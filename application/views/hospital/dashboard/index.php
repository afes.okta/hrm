<?php $session = $this->session->userdata('hospital_name'); ?>

<?php $hospitalinfo = $this->Clients_model->read_hospital_info($session['hospital_id']); ?>



<div class="box-widget widget-user-2">

  <div class="widget-user-header">

      <div class="row">
          <div class="col-md-6">
              <h4 class="widget-user-username welcome-hrsale-user">Welcome back, <?php echo $hospitalinfo[0]->hospital_name;?>!</h4>

              <h5 class="widget-user-desc welcome-hrsale-user-text">Today is <?php echo date('l, j F Y');?></h5>
          </div>

        
      </div>

    
 
  </div>

</div>

<div class="row animated fadeInRight">

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="#">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-primary"><i class="fa fa-users"></i></span>

      <div class="info-box-content"> <span class="info-box-number"> 0</span> <span class="info-box-number client-hr-invoice">Total Clients & Dependants</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="#">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-green"><i class="fa fa-hospital-o"></i></span>

      <div class="info-box-content"> <span class="info-box-number">0</span> <span class="info-box-number client-hr-invoice">Total Authorizations</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="#">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-purple"><i class="fa fa-medkit"></i></span>

      <div class="info-box-content"> <span class="info-box-number">0</span> <span class="info-box-number client-hr-invoice">Total Bills</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

  

    <!-- /.info-box --> 

  </div>

</div>

<div class="row animated fadeInRight">

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="#">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-yellow"><i class="fa fa-ambulance"></i></span>

      <div class="info-box-content"> <span class="info-box-number"> 0</span> <span class="info-box-number client-hr-invoice">Total Encounters</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="#">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-red"><i class="fa fa-table"></i></span>

      <div class="info-box-content"> <span class="info-box-number">0</span> <span class="info-box-number client-hr-invoice">Total Complains</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="#">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-green"><i class="fa fa-tasks"></i></span>

      <div class="info-box-content"> <span class="info-box-number">0</span> <span class="info-box-number client-hr-invoice">Total Appointments</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>
  </div>



<style type="text/css">

.box-body {

    padding: 0 !important;

}

.info-box-number {

	font-size:16px !important;

	font-weight:300 !important;

}

</style>

