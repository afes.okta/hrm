<?php

/* Dependant List view

*/
?>

<?php $session = $this->session->userdata('client_username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>



<div class="row <?php echo $get_animate;?>">

  
   
  <!-- fix for small devices only -->

  <div class="clearfix visible-sm-block"></div>

  
 

</div>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> <?php //echo $this->lang->line('xin_list_all');?>Diagnose Clients</h3>
    <button class="btn btn-info pull-right" id="diagnose_client"><i class="fa fa fa-plus"></i>&nbsp;Diagnose Client</button>
  </div>

    <div class="current-tab <?php echo $get_animate;?>" aria-expanded="false">

        <div class="box" id="diagnose_client_form">

        <div class="box-body">

            <div class="card-block">
              <?php echo form_open_multipart('hospital/clients/insert_clients_diagnose');?>
                    <div class="form-body">

                        <div class="row">
                            <div class="col-md-12">
                                <?php if ($this->session->flashdata('success')): ?>
                                    <div class="alert alert-success">
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                <?php endif ?>


                                <?php if ($this->session->flashdata('error')): ?>
                                    <div class="alert alert-warning">
                                        <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                <?php endif ?>
                            </div>
                            <div class="col-md-11" style="width: 98.5%;">
                                <div class="form-group">

                                  <label for="company_name">Search Client</label>
                                  <select class="form-control col-md-6 select2" name="diagnose_client">
                                     <option>Select Client</option> 
                        <?php   
                            if(!empty($clients))
                            { 
                                //print_r($clients);
                                foreach ($clients as $key => $value)
                                {          
                        ?>  
                                      <option value="<?php echo $value->client_id; ?>"><?php echo isset($value->name) ? ucfirst($value->name) : '';?> <?php echo isset($value->last_name) ? ucfirst($value->last_name) : ''; ?> (<?php echo isset($value->client_id) ? ucfirst("LTM/".date("Y",strtotime($value->created_at))."/".$value->client_id) : ''; ?>)</option>
                        <?php
                                }
                            }
                        ?> 
                                  </select>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">

                                  <label for="company_name">Client ID</label>
                                  <input class="form-control" name="diagnose_client_id" placeholder="Client ID"   type="text" value="">

                                </div>
                            </div>
                            <div class="col-md-6 form-group">

                                <label for="last_name">Date</label>

                                <input class="form-control" name="diagnose_date" type="date" value="">

                            </div>
                            <div class="col-md-6 form-group" style="width: 49.3%;">

                                <label for="last_name">Select Services</label>
                                <select class="form-control select2" name="diagnose_services[]" data-plugin="select_hrm" data-placeholder="Select Services" multiple> 
                                    <?php /*if (isset($all_locations) and !empty($all_locations)): ?>
                                        <?php foreach ($all_locations as  $value): ?>

                                            <?php echo "<option  ".($value->location_id == $state ? "selected" : "")."      value='".$value->location_id."'    >".$value->location_name."</option> " ?>
                                        <?php endforeach ?>
                                        
                                    <?php endif */ ?>
                                    <option value="">Select Services</option>

                      <?php if (isset($xin_services_hospital) and !empty($xin_services_hospital)) { ?>
                         
                     
                      <?php foreach ($xin_services_hospital as $key => $value): ?>
                                    <option value="<?php echo isset($value->id  ) ? $value->id   : ''; ?> [<?php echo isset($value->service_price) ? $value->service_price : ''; ?>]"><?php echo isset($value->service_name) ? $value->service_name : ''; ?> (<?php echo isset($value->service_price) ? $value->service_price : ''; ?>)</option>
                      <?php 
                            endforeach;
                        }
                      ?>
                                </select>
                            </div>
                            <div class="col-md-6 form-group pull-right" style="width: 550px;">

                                <label for="last_name">Select Drugs</label>
                                <select class="form-control select2" name="diagnose_drugs[]" data-plugin="select_hrm" data-placeholder="Select Drugs" multiple> 
                                    <?php /*if (isset($all_locations) and !empty($all_locations)): ?>
                                        <?php foreach ($all_locations as  $value): ?>

                                            <?php echo "<option  ".($value->location_id == $state ? "selected" : "")."      value='".$value->location_id."'    >".$value->location_name."</option> " ?>
                                        <?php endforeach ?>
                                        
                                    <?php endif */ ?>
                                    <option value="">Select Drugs</option>
                      <?php if (isset($all_hospital_drugs) and !empty($all_hospital_drugs)) { ?>
                         
                     
                      <?php foreach ($all_hospital_drugs as $key => $value): ?>
                                    <option value="<?php echo isset($value->drug_id) ? $value->drug_id : ''; ?> [<?php echo isset($value->drug_price) ? $value->drug_price : ''; ?>]"><?php echo isset($value->drug_name) ? $value->drug_name : ''; ?> (<?php echo isset($value->drug_price) ? $value->drug_price : ''; ?>)</option>
                      <?php 
                            endforeach;
                        }
                      ?>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="last_name">Diagnose</label>
                                <textarea class="form-control" placeholder="Diagnose" rows="5" name="diagnose_diagnose" type="text"></textarea>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="last_name">Procedure</label>
                                <textarea class="form-control" placeholder="Procedure" rows="5" name="diagnose_procedure" type="text"></textarea>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="last_name">Investigation</label>
                                <textarea class="form-control" placeholder="Investigation" rows="5" name="diagnose_investigation" type="text"></textarea>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="last_name">Medical Personnel</label>
                                <textarea class="form-control" placeholder="Medical Personnel" rows="5" name="diagnose_medical" type="text"></textarea>
                            </div>

                    </div>



                    <?php if (empty($hospital_id)): ?> 
                    <div class="form-actions"> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => "pull-right ".$this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '."Request For Authorization Code")); ?> 
                    </div>
                    <?php endif ?>

            </div>
                <?php echo form_close(); ?> 

        </div>
    </div>
</div>
  <div class="box-body">

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-bordered" id="xin_table_new">

        <thead>

          <tr>

            <th>Status</th>
            <th>G. Code</th>
            <th>Transaction ID</th>
            <th>Client Name</th>
            <th>Date</th>
            <th>Total Bill</th>

          </tr>

        </thead>

        <tbody>
        <?php 
          if (!empty($xin_diagnose_clients)) { 
          foreach ($xin_diagnose_clients as $key => $value):

            $ci=& get_instance();
            $ci->load->model('Clients_model'); 

            $services = $ci->Clients_model->read_individual_hospital_diagnose_services($value->diagnose_id);
            $drugs = $ci->Clients_model->read_individual_hospital_diagnose_drugs($value->diagnose_id);
              
        ?>
               <tr>
                  <td>
                    <!--  data-toggle="modal" data-target="#myModal" onclick="return loadModalView(<?php echo $value->diagnose_id; ?>);" -->
                      <a class="btn btn-default" data-toggle="modal" data-target="#myModal" onclick="return loadModalView(<?php echo $value->diagnose_id; ?>);"><i class="fa fa-eye"></i></a> 
                      <?php if (($value->diagnose_status == '2') && ($value->diagnose_bill_status == '')){ ?>
                          <p class="btn btn-success">Code Generated</p> 
                          <a href="<?php echo base_url(); ?>hospital/clients/index?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&bill_request=yes&id=<?php echo $value->diagnose_id; ?>"  class="btn btn-info">Send Bill Request</a>
                      <?php }elseif($value->diagnose_bill_status == '3') { ?>
                          <p class="btn btn-success">Code Generated</p> 
                          <p class="btn btn-success"> Bill Approved</p> 
                      <?php }elseif($value->diagnose_status == '3') { ?>
                          <p class="btn btn-danger"> Rejected</p> 
                      <?php }elseif($value->diagnose_bill_status == '4') { ?>
                          <p class="btn btn-success">Code Generated</p> 
                          <p class="btn btn-danger"> Bill Rejected</p> 
                      <?php }elseif(($value->diagnose_status == '2') && (($value->diagnose_bill_status == '1') || ($value->diagnose_bill_status == '2'))) { ?>
                          <p class="btn btn-success">Code Generated</p> 
                          <p class="btn btn-warning"> Bill Requested</p> 
                      <?php }else { ?>
                          <p class="btn btn-warning"> Requested For Code</p> 

                      <?php } ?>
                  </td>
                  <td><?php if(empty($value->diagnose_generated_code)) echo "-----"; else echo $value->diagnose_generated_code; ?></td>
                  <td><?php if(empty($value->diagnose_transaction_id)) echo "-----"; else echo $value->diagnose_transaction_id; ?></td>
                  <td><?php echo $value->name." ".$value->last_name." ".$value->other_name; ?></td>
                  <td><?php echo $value->diagnose_date; ?></td>
                  <td>₦<?php echo $value->diagnose_total_sum.".00"; ?></td>
               </tr>
          <?php endforeach;

          } ?>
        </tbody>

      </table>
    </div>

  </div>

</div>

<style type="text/css">

.info-box-number {

    font-size:16px !important;

    font-weight:300 !important;

}

</style>

<script type="text/javascript">
  $('.select2').select2();

      function loadModalView(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_data").html(response);
            }
          });

      }
  $(document).ready(function() {

      // alert("yess its wokringg wahidddd");

      $("#diagnose_client_form").hide();
      $("#diagnose_client").click(function(){
          $("#diagnose_client_form").slideToggle();
      });
  });

  document.addEventListener('DOMContentLoaded', function(){ 
      var xin_table_new = $('#xin_table_new').dataTable();  
  }, false);
</script>