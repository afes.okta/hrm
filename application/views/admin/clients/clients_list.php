<?php

/* Client view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php if(in_array('323',$role_resources_ids)) {?>



<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"><?php echo $this->lang->line('xin_add_new');?> <?php echo $this->lang->line('xin_project_client');?></h3>

      <div class="box-tools pull-right"> <a class="text-dark collapsed" data-toggle="collapse" href="#add_form" aria-expanded="false">

        <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> <?php echo $this->lang->line('xin_add_new');?></button>

        </a> </div>

    </div>

    <div id="add_form" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <?php $attributes = array('name' => 'add_client', 'id' => 'xin-form', 'autocomplete' => 'off');?>

        <?php $hidden = array('user_id' => $session['user_id']);?>

        <?php echo form_open('admin/clients/add_client', $attributes, $hidden);?>

        <div class="form-body">

        <div class="row">

            <div class="col-md-6">

                <div class="form-group">

                    <label for="client_name">Contact Name</label>

                    <input class="form-control" placeholder="Contact Name" name="name" type="text">

                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="company_name">Select organization</label>

                  

                    <select class="form-control" name="company_name" data-plugin="xin_select" data-placeholder="Select Organization">

                        <option value="">Select Organization</option>

                        <?php foreach($all_organizations as $organization) {?>

                        <option value="<?php echo $organization->id;?>"> <?php echo $organization->name;?></option>

                        <?php } ?>

                    </select> 
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="contact_number">Phone Number</label>

                    <input class="form-control" placeholder="Phone Number" name="contact_number" type="text">
                </div>
            </div>

               

            <div class="col-md-6">
                <div class="form-group">
                    <label for="website"><?php echo $this->lang->line('xin_employee_password');?></label>

                    <input class="form-control" placeholder="<?php echo $this->lang->line('xin_employee_password');?>" name="password" type="text">
                </div>
            </div>
        </div>

        <div class="row"> 
            <div class="col-md-6">
                <div class="form-group">

                    <label for="email"><?php echo $this->lang->line('xin_email');?></label>

                    <input class="form-control" placeholder="<?php echo $this->lang->line('xin_email');?>" name="email" type="email">
                </div>
            </div>

                 

            <div class="col-md-6">
                <div class="form-group">
                    <label for="subscription_ids">Select Subscription</label>

                  

                    <select   class="form-control" name="subscription_ids[]" data-plugin="xin_select" data-placeholder="Select Subscription">

                        <option value="">Select Subscription</option>

                        <?php foreach($all_subscriptions as $subscription) {?>

                        <option value="<?php echo $subscription->subscription_id;?>"> <?php echo $subscription->plan_name;?></option>

                        <?php } ?>

                    </select> 
                </div>
            </div>     
        </div>



        <div class="row"> 

            <div class="col-md-6">
                <div class="form-group">
                    <label for="subscription_ids">Select Individual or Family</label> 

                    <select   class="form-control" name="ind_family" data-plugin="xin_select" data-placeholder="Select Individual or Family">

                        <option value="">Select Individual or Family</option> 

                        <option value="individual">Individual  </option>
                        <option value="family">Family  </option>
 
                    </select> 
                </div>
            </div> 



            <div class="col-md-3">

                <fieldset class="form-group">

                    <label for="logo"><?php echo $this->lang->line('xin_project_client_photo');?></label>

                    <input type="file" class="form-control-file" id="client_photo" name="client_photo">

                    <small><?php echo $this->lang->line('xin_company_file_type');?></small>

                </fieldset>

            </div>

        </div>
 
 
        </div>

        <div class="form-actions box-footer">

            <button type="submit" class="btn btn-primary"> <i class="fa fa-check-square-o"></i> <?php echo $this->lang->line('xin_save');?> </button>

        </div>

        <?php echo form_close(); ?> </div>

    </div>

  </div>

</div>

<?php } ?>

<div class="box <?php echo $get_animate;?>">

    <div class="box-header with-border">

        

        <a href="<?php echo base_url(); ?>/uploads/csv/sample-csv-clients.csv" class="btn btn-primary"> <i class="fa fa-download"></i> Download sample File </a> 

        <?php $attributes = array('name' => 'import_attendance', 'id' => 'xin-form', 'autocomplete' => 'off');?>

             

            <?php echo form_open_multipart('admin/clients/import_clients', $attributes, $hidden);?>

            <div class="row">
                <?php if ($this->session->flashdata('success')): ?>
                    <div class="alert alert-success  alert-dismissible" style="margin: 20px 12px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif ?>

                 <?php if ($this->session->flashdata('error')): ?>
                    <div class="alert alert-warning  alert-dismissible" style="margin: 20px 12px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif ?>

                <div class="col-md-4 form-group"> 

                    <fieldset class="form-group">

                        <label for="logo"><?php echo $this->lang->line('xin_employee_upload_file');?></label>

                        <input type="file" class="form-control-file" id="file" name="file">

                        <small><?php echo $this->lang->line('xin_employee_imp_allowed_size');?></small>

                    </fieldset> 
                </div>

            </div>

            <div class="mt-1">

              <div class="form-actions box-footer"> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => $this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '.$this->lang->line('xin_save'))); ?> </div>

            </div>

            <?php echo form_close(); ?>

    
    <h3 class="box-title"> <?php echo $this->lang->line('xin_list_all');?> <?php echo $this->lang->line('xin_project_clients');?> </h3>
    </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table">

        <thead>

            <tr> 
                <th><?php echo $this->lang->line('xin_action');?></th>

                <th> Name</th>

                <th>Organization</th>

                <th>Subscriptions</th>

                <th>Family/Individual</th> 

                <th><?php echo $this->lang->line('xin_email');?></th>

                <th>Phone Number</th> 

                
            </tr>

        </thead>

      </table>

    </div>

  </div>

</div>













<div class="modal fadeInRight edit-modal-data2 animated" id="edit-modal-data" role="dialog" aria-labelledby="edit-modal-data" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="ajax_modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                <h4 class="modal-title" id="edit-modal-data"><i class="icon-pencil7"></i> Edit Client</h4>
            </div>
            <form action="<?php echo base_url() ?>admin/clients/update" name="edit_client" id="edit_client" autocomplete="off" class="m-b-1" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="EDIT">
                <input type="hidden" name="_token"  id="_token">
                <input type="hidden" name="ext_name" value="Shaleena">
                <input type="hidden" name="csrf_hrsale" value="467ba7724bc88bcd9af14ffeb6201eff">                  



                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company_name">Contact Name</label>
                                <input class="form-control" placeholder="Contact Name" name="name" type="text"  id="c_name">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company_name">Organization Name</label>
                                <select class="form-control" id="c_orga" name="company_name" data-plugin="xin_select" data-placeholder="Select Organization">

                                    <option value="">Select Organization</option>

                                    <?php foreach($all_organizations as $organization) {?>

                                    <option value="<?php echo $organization->id;?>"> <?php echo $organization->name;?></option>

                                    <?php } ?>

                                </select> 
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="contact_number">Phone Number</label>
                                <input class="form-control" placeholder="Phone Number" name="contact_number" type="text" id="c_phone" value="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group"> 
                                <label for="email">Email</label>
                                <input class="form-control" id="c_email" placeholder="Email" name="email" type="email" > 
                            </div>
                        </div> 
                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subscription_ids">Select Subscription</label> 

                                <select id="subscription_ids" class="form-control" name="subscription_ids[]" data-plugin="xin_select" data-placeholder="Select Subscription">

                                    <option value="">Select Subscription</option>

                                    <?php foreach($all_subscriptions as $subscription) {?>

                                    <option value="<?php echo $subscription->subscription_id;?>"> <?php echo $subscription->plan_name;?></option>

                                    <?php } ?>

                                </select> 
                            </div>
                        </div>  
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="status" class="control-label">Status</label>
                                <select class="form-control  select2" name="status" id="c_status"  data-placeholder="Status" >
                                    <option value="0">Inactive</option>
                                    <option value="1" >Active</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                          
                            <div class="form-group">
                                <label for="subscription_ids">Select Individual or Family</label> 

                                <select   class="form-control" id="ind_family" name="ind_family" data-plugin="xin_select" data-placeholder="Select Individual or Family">

                                    <option value="">Select Individual or Family</option> 

                                    <option value="individual">Individual  </option>
                                    <option value="family">Family  </option>
             
                                </select> 
                            </div>
                            
                        </div> 
                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                 
                            
                                <label for="logo">Profile Photo</label>
                                <small>Upload files only: gif,png,jpg,jpeg</small> 
                                <input type="file" class="form-control-file" id="client_photo" name="client_photo">
                           
                                <span class="avatar box-48 mr-0-5"> <img class="user-image-hr46 ui-w-100 rounded-circle" src="https://hmo.liontech.com.ng/uploads/clients/" alt=""> </span>
                              
                            </fieldset>
                             
                        </div> 
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save">Update</button>
                </div>
            </form>


            <script type="text/javascript">

                document.addEventListener('DOMContentLoaded', function(){ 

                    $('[data-plugin="select_hrm"]').select2($(this).attr('data-options'));
                    $('[data-plugin="select_hrm"]').select2({ width:'100%' });   

                    /* Edit data */
                    $("#edit_client").submit(function(e){
                        var fd = new FormData(this);
                        var obj = $(this), action = obj.attr('name');
                        fd.append("is_ajax", 2);
                        fd.append("edit_type", 'client');
                        fd.append("form", action);
                        e.preventDefault();
                        $('.save').prop('disabled', true);
                        $.ajax({
                            url: e.target.action,
                            type: "POST",
                            data:  fd,
                            contentType: false,
                            cache: false,
                            processData:false,
                            success: function(JSON)
                            {
                                if (JSON.error != '') {
                                    toastr.error(JSON.error);
                                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                                    $('.save').prop('disabled', false);
                                } else {
                                    // On page load: datatable
                                    var xin_table = $('#xin_table').dataTable({
                                        "bDestroy": true,
                                        "ajax": {
                                            url : "<?php  echo base_url(); ?>admin/clients/clients_list",
                                            type : 'GET'
                                        },
                                        dom: 'lBfrtip',
                                        "buttons": ['csv', 'excel', 'pdf', 'print'], // colvis > if needed
                                        "fnDrawCallback": function(settings){
                                        $('[data-toggle="tooltip"]').tooltip();          
                                        }
                                    });
                                    xin_table.api().ajax.reload(function(){ 
                                        toastr.success(JSON.result);
                                    }, true);
                                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                                    $('.edit-modal-data2').modal('toggle');
                                    $('.save').prop('disabled', false);
                                }
                            },
                            error: function() 
                            {
                                toastr.error(JSON.error);
                                $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                                $('.save').prop('disabled', false);
                            }           
                       });
                    });





                    $('body').on('click', '.edit_client_detail', function() { 
  
                        var c_name     = $(this).attr('data-c_name');
                        var c_orga     = $(this).attr('data-c_orga'); 
                        var c_email    = $(this).attr('data-email');
                        var c_phone    = $(this).attr('data-phone');
                        var c_status   = $(this).attr('data-status');
                        var _token     = $(this).attr('data-client_id');
                        var detail_pkg_edit     = $(this).attr('data-detail_pkg_edit');
                        var ind_family     = $(this).attr('data-ind_family');
                        
                        var subs_array = detail_pkg_edit.split("--------")
                        $('#subscription_ids').val(0).trigger('change');
                        for (var i = 1; i < subs_array.length; i++) 
                        {
                            // $("#band_edit option[value="+bnd_pkg_array[i]+"]").prop("selected", false).parent().trigger("change");
                            $('#subscription_ids option[value="'+subs_array[i]+'"]').attr("selected", "selected");
                            // $('#band_edit').attr(bnd_pkg_array[i]).trigger('change')
                                 
                        }

                        $('#c_name').val(c_name); 
                        $('#c_orga').val(c_orga).trigger('change'); 
                        $('#c_status').val(c_status).trigger('change'); 
                        $('#ind_family').val(ind_family).trigger('change'); 
                        $('#c_email').val(c_email);
                        $('#c_phone').val(c_phone);
                        $('#_token').val(_token);
                        
                    }) 
                }, false);
             
            </script>
        </div>
    </div>
</div>