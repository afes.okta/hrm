<?php

/* Subscription view

*/
 
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php if(in_array('341',$role_resources_ids)) {?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>


<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

     

   

  </div>

</div>

<?php } ?>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> List All Clients   </h3>

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                    <th><?php echo $this->lang->line('xin_action');?></th>

                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Other Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Location</th>
                    <th>Date of Birth</th>
                    <th>Sex</th>
                    <th>Marital Status</th>
                    <th>Family/Individual</th>
                    <th>Diseases</th>
                    <th>Disease Comment</th> 
                    <th>Profile</th> 
                </tr>

                <tbody> 
                    <?php   
                    if(!empty($clients))
                    { 
                        foreach ($clients as $key => $value)
                        {              
                             
                         

                            $ci=& get_instance();
                            $ci->load->model('Training_model'); 


                            $diseases = explode(',', $value->diseases);
                            $disease_list = '';
                            foreach ($diseases as $disease) {
                               $disease_list .= '<p>'.ucfirst( $disease).'</p>';
                            }


                            // $bands    = $ci->Training_model->getAll2('xin_bands', ' band_id='. $value->band_id.' ');

                            $location = $ci->Training_model->getAll2('xin_location', ' location_id= "'. $value->state.'" ');
                            
                                
                            ?>  
                            <tr>
                                <td>
                                    <!-- <button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_detect"  ><span class="fa fa-pencil"></span></button> -->


                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="View Details"><a href="<?php echo base_url(); ?>admin/ClientAccount/detailAccount/<?php echo $value->client_id; ?>"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"><span class="fa fa-arrow-circle-right"></span></button></a></span>

                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Details"><a href="<?php echo base_url(); ?>admin/ClientAccount/editAccount/<?php echo $value->client_id; ?>"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"><span class="fa fa-pencil"></span></button></a></span>

                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                        <button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal2"   ><span class="fa fa-trash"><a href="<?php echo base_url(); ?>admin/ClientAccount/deleteAccount/<?php echo $value->client_id; ?>"></span></button>
                                    </span>  
                                </td>

                                <td><?php echo isset($value->name) ? ucfirst($value->name) : ''; ?> </td>

                                <td><?php echo isset($value->last_name) ? ucfirst($value->last_name) : ''; ?> </td>

                                <td><?php echo isset($value->other_name) ? $value->other_name : ''; ?> </td>

                                <td><?php echo isset($value->email) ? $value->email : ''; ?></td> 

                                <td><?php echo isset($value->contact_number) ? $value->contact_number : ''; ?></td> 

                                <td><?php echo isset($value->address_1) ? $value->address_1 : ''; ?></td>


                                <td><?php echo isset($location[0]->location_name) ? $location[0]->location_name : ''; ?></td>


                                <td><?php echo isset($value->dob) ? $value->dob : ''; ?></td> 

                                <td><?php echo isset($value->sex) ? ucfirst($value->sex) : ''; ?></td> 

                                <td><?php echo isset($value->marital_status) ? ucfirst($value->marital_status) : ''; ?></td> 

                                <td><?php echo isset($value->ind_family) ? ucfirst($value->ind_family) : ''; ?></td> 
                                <td><?php echo isset($disease_list) ? $disease_list : ''; ?></td> 

                                <td><?php echo isset($value->disease_comment) ? ucfirst($value->disease_comment) : ''; ?></td>    
                                 
                                <td>
                                    <?php if (isset($value->client_profile  ) and !empty($value->client_profile )): ?>
                                        <?php
                                            echo '<a style="cursor:pointer" download href=" '.base_url().'uploads/clients/'.$value->client_profile  .' ">View </a>';
                                        ?>
                                    <?php endif ?>  
                                </td>
                                
                               
                            </tr> 

                            <?php 
                        }
                    }
                    ?>
                </tbody>

            </thead>

        </table>

    </div>

  </div>

</div>
 

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable();
 
    }, false);

     
</script>
 






 