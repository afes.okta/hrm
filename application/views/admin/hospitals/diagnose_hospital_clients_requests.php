<?php

/* Subscription view

*/
 
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php if(in_array('341',$role_resources_ids)) {?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>


<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

   
</div>

<?php } ?>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> Diagnose Hospital Clients </h3>

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                    <th><?php echo $this->lang->line('xin_action');?></th>
                    <th>Approve/Rejected By</th>
                    <th>G. Code</th>
                    <th>Transaction ID</th>
                    <th>Client Name</th>
                    <th>Date</th>
                    <th>Total Bill</th>
                </tr>

                <tbody> 
                    <?php   
                    if(!empty($xin_diagnose_clients))
                    { 
                        foreach ($xin_diagnose_clients as $key => $value)
                        {              
                             
                            $ci=& get_instance();
                            $ci->load->model('Clients_model'); 

                            $services = $ci->Clients_model->read_individual_hospital_diagnose_services($value->diagnose_id);
                            $drugs = $ci->Clients_model->read_individual_hospital_diagnose_drugs($value->diagnose_id);
                            $admin_info = $ci->Clients_model->read_individual_admin_info($value->diagnose_status_approve_by);
                                
                            ?>  
                            <tr>
                                <td>
                                    <a class="btn btn-default" data-toggle="modal" data-target="#myModal" onclick="return loadModalView(<?php echo $value->diagnose_id; ?>);"><i class="fa fa-eye"></i></a>
                                    <?php if ($value->diagnose_status == '2'){ ?>
                                        <p class="btn btn-success">Code Generated</p> 

                                    <?php }elseif($value->diagnose_status == '3') { ?>
                                        <p class="btn btn-danger"> Rejected</p> 
                                    <?php }else { ?>
                                        <a href="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&approve=yes&id=<?php echo $value->diagnose_id; ?>"  class="btn btn-info">Approve</a>

                                        <a href="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&reject=yes&id=<?php echo $value->diagnose_id; ?>" class="btn btn-warning">Reject</a>

                                    <?php } ?>
                                </td>
                                <td><?php if($admin_info) echo $admin_info->first_name." ".$admin_info->last_name; else echo "-----"; ?></td>
                                <td><?php if(empty($value->diagnose_generated_code)) echo "-----"; else echo $value->diagnose_generated_code; ?></td>
                                <td><?php if(empty($value->diagnose_transaction_id)) echo "-----"; else echo $value->diagnose_transaction_id; ?></td>
                                  <td><?php echo $value->name." ".$value->last_name." ".$value->other_name; ?></td>
                                  <td><?php echo $value->diagnose_date; ?></td>
                                  <td><?php echo $value->diagnose_total_sum.".00"; ?></td>
                            </tr> 

                            <?php 
                        }
                    }
                    ?>
                </tbody>

            </thead>

        </table>

    </div>

  </div>

</div>
 

<script type="text/javascript">

      function loadModalView(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_data").html(response);
            }
          });

      }

    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable(); 
    }, false);

     
</script>
 






 