<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyModel extends CI_Model {

    var $client_service = "frontend-client";
    var $auth_key       = "hmorestapi";

    public function check_auth_client(){
        $client_service = $this->input->get_request_header('Client-Service', TRUE);
        $auth_key  = $this->input->get_request_header('Auth-Key', TRUE);
        if($client_service == $this->client_service && $auth_key == $this->auth_key){
            return true;
        } else {
            return json_output(401,array('status' => 401,'message' => 'Unauthorized.'));
        }
    }

    public function login($username,$password)
    {
        $q  = $this->db->from('xin_hospital')->where('email',$username)->get()->row();

        if(empty($q)){
            return array('status' => 204,'message' => 'Username not found.');
        } else {
            $hashed_password = $q->password;
            $id              = $q->hospital_id;
            if ($password == $hashed_password) {
                $last_login = date('Y-m-d H:i:s');
                try {
                    $string = random_bytes(32);
                } catch (TypeError $e) {
                    // Well, it's an integer, so this IS unexpected.
                    die("An unexpected error has occurred");
                } catch (Error $e) {
                    // This is also unexpected because 32 is a reasonable integer.
                    die("An unexpected error has occurred");
                } catch (Exception $e) {
                    // If you get this message, the CSPRNG failed hard.
                    die("Could not generate a random string. Is our OS secure?");
                }
                $token = bin2hex($string);
//				$token = md5(rand());
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->trans_start();
                $this->db->where('hospital_id',$id)->update('xin_hospital',array('last_login' => $last_login));
                $this->db->insert('xin_users_authentication',array('users_id' => $id,'token' => $token,'expired_at' => $expired_at));
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    return array('status' => 500,'message' => 'Internal server error.');
                } else {
                    $this->db->trans_commit();
                    return array('status' => 200,'message' => 'Successfully login.','hospital_id' => $id, 'token' => $token);
                }
            } else {
               return array('status' => 204,'message' => 'Wrong password.');
            }
        }
    }

    public function login_client($username,$password)
    {
        $q  = $this->db->from('xin_clients')->where('email',$username)->get()->row();

        if(empty($q)){
            return array('status' => 204,'message' => 'Username not found.');
        } else {
            $hashed_password = $q->client_password;
            $id              = $q->client_id;
            $password_hash   = hash_equals($hashed_password, crypt($password, $hashed_password));
            if ($password_hash) {
                $last_login = date('Y-m-d H:i:s');
                try {
                    $string = random_bytes(32);
                } catch (TypeError $e) {
                    // Well, it's an integer, so this IS unexpected.
                    die("An unexpected error has occurred");
                } catch (Error $e) {
                    // This is also unexpected because 32 is a reasonable integer.
                    die("An unexpected error has occurred");
                } catch (Exception $e) {
                    // If you get this message, the CSPRNG failed hard.
                    die("Could not generate a random string. Is our OS secure?");
                }
                $token = bin2hex($string);
//                $token = md5(rand());
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->trans_start();
                $this->db->where('client_id',$id)->update('xin_clients',array('last_login_date' => $last_login));
                $this->db->insert('xin_users_authentication',array('users_id' => $id,'token' => $token,'expired_at' => $expired_at));
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    return array('status' => 500,'message' => 'Internal server error.');
                } else {
                    $this->db->trans_commit();
                    return array('status' => 200,'message' => 'Successfully login.','client_id' => $id, 'token' => $token);
                }
            } else {
                return array('status' => 204,'message' => 'Wrong password.');
            }
        }
    }

    public function logout()
    {
        $users_id  = $this->input->get_request_header('User-ID', TRUE);
        $token     = $this->input->get_request_header('token', TRUE);
        $this->db->where('users_id',$users_id)->where('token',$token)->delete('xin_users_authentication');
        return array('status' => 200,'message' => 'Successfully logout.');
    }

    public function auth()
    {
        $users_id  = $this->input->get_request_header('User-ID', TRUE);
        $token     = $this->input->get_request_header('token', TRUE);
		$q  = $this->db->from('xin_users_authentication')->where('users_id',$users_id)->where('token',$token)->get()->row();
        if(empty($q)){
            return json_output(401,array('status' => 401,'message' => 'Unauthorized.'));
        } else {
            if($q->expired_at < date('Y-m-d H:i:s')){
                return json_output(401,array('status' => 401,'message' => 'Your session has been expired.'));
            } else {
                $updated_at = date('Y-m-d H:i:s');
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->where('users_id',$users_id)->where('token',$token)->update('xin_users_authentication',array('expired_at' => $expired_at,'updated_at' => $updated_at));
                return array('status' => 200,'message' => 'Authorized.');
            }
        }
    }

    public function hospital_all()
    {
        return $this->db->from('xin_hospital')->order_by('hospital_id','desc')->get()->result();
    }

    public function hospital_detail_data($id)
    {
        return $this->db->from('xin_hospital')->where('hospital_id',$id)->order_by('hospital_id','desc')->get()->row();
    }

    public function client_all() {
        return $this->db->from('xin_clients')->order_by('client_id','desc')->get()->result();
    }

    public function client_by_hospital_all($id) {
        return $this->db->from('xin_clients')->where('hospital_id',$id)->order_by('client_id','desc')->get()->result();
    }

    public function client_detail_data($id) {
        return $this->db->from('xin_clients')->where('client_id',$id)->order_by('client_id','desc')->get()->row();
    }
    
    public function client_fammily_detail_data($id) {
        return $this->db->from('xin_clients_family')->where('client_id',$id)->order_by('client_id','desc')->get()->row();
    }
    
    public function client_dependant_data($id) {
        return $this->db->from('xin_vdependant')->where('client_id',$id)->order_by('client_id','desc')->get()->result();
    }
    
    public function service_hospital_all() {
        return $this->db->from('xin_services_hospital')->order_by('id','desc')->get()->result();
    }

    public function drugs_hospital_all($id) {
        return $this->db->from('xin_services_hospital')->where('hospital_id',$id)->order_by('drug_id','desc')->get()->result();
    }

    public function drugs_detail_hospital_all($id,$drugs_id) {
        return $this->db->from('xin_services_hospital')->where("hospital_id='$id' and drug_id='$drugs_id'")->order_by('drug_id','desc')->get()->result();
    }

    public function hospital_request_create_data($data)
    {
        $this->db->insert('xin_clients_diagnose',$data);
        return array('status' => 200,'message' => 'Data has been created.');
    }

    public function hospital_request_client_get_data($hospital_id)
    {
        return $this->db->from('xin_clients_diagnose')->where('diagnose_hospital_id',$hospital_id)->order_by('diagnose_id','desc')->get()->result();
    }

    public function count_finance_by_hospital() {
        return $this->db->from('xin_finance_transaction')->where("transaction_type='income'")->get()->num_rows();
    }

    public function hospital_update_data($id,$data)
    {
        $this->db->where('hospital_id',$id)->update('xin_hospital',$data);
        return array('status' => 200,'message' => 'Data has been updated.');
    }
    
    public function client_dependant_update_data($id,$data)
    {
        $this->db->where('client_id',$id)->update('xin_clients_family',$data);
        return array('status' => 200,'message' => 'Data has been updated.');
    }
    
    public function client_dependant_new_data($data)
    {
        $this->db->insert('xin_clients_family',$data);
        return array('status' => 200,'message' => 'Data has been updated.');
    }
    
    public function client_request_change_hospital($data)
    {
        $this->db->insert('xin_change_hospital_request',$data);
        return array('status' => 200,'message' => 'Data has been created.');
    }

    public function count_authorization_dependant_by_hospital($id) {
        return $this->db->from('xin_clients_diagnose')->where('diagnose_hospital_id',$id)->get()->num_rows();
    }

	public function count_client_dependant_by_hospital($id) {
		return $this->db->from('xin_vdependant')->where('hospital_id',$id)->get()->num_rows();
	}
    
    public function count_client_request_hospital($id) {
        return $this->db->from('xin_change_hospital_request')->where('client_id',$id)->get()->num_rows();
    }

	public function count_client_familly_request_hospital($id) {
		return $this->db->from('xin_clients_family_req')->where('client_id',$id)->get()->num_rows();
	}

	public function get_xin_subscription($id) {
		return $this->db->from('xin_subscription')->where('subscription_id',$id)->get()->num_rows();
	}
//    public function book_delete_data($id)
//    {
//        $this->db->where('id',$id)->delete('books');
//        return array('status' => 200,'message' => 'Data has been deleted.');
//    }

	public function get_bill_status($id) {
		return $this->db->from('xin_clients_diagnose')->where('diagnose_hospital_id',$id)->where('diagnose_bill_status =3 or diagnose_bill_status=4')->get()->num_rows();
	}
}
