<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the HRSALE License
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.hrsale.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to hrsalesoft@gmail.com so we can send you a copy immediately.
 *
 * @author   HRSALE
 * @author-email  hrsalesoft@gmail.com
 * @copyright  Copyright © hrsale.com. All Rights Reserved
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class ClientAccount extends MY_Controller {
	
	 public function __construct() {
        parent::__construct();
		//load the model
		$this->load->model("Training_model");
		$this->load->model("Xin_model");
		$this->load->model("Trainers_model");
		$this->load->model("Designation_model");
		$this->load->model("Department_model");
		$this->load->model("Custom_fields_model");
	}
	
	/*Function to set JSON output*/
	public function output($Return=array()){
		/*Set response header*/
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
		/*Final JSON response*/
		exit(json_encode($Return));
	}
	
	public function index() 
    {
		$session = $this->session->userdata('username');
		if(empty($session)){  
			redirect('admin/'); 
		}
		$system = $this->Xin_model->read_setting_info(1);
		if($system[0]->module_training!='true'){
			redirect('admin/dashboard');
		}
		$data['title'] =  'Clients | '.$this->Xin_model->site_title();
 
		$data['clients']   =  $this->Training_model->getAll2('xin_clients',' 1 order by client_id desc');
		
		$data['breadcrumbs'] = 'Clients';
		$data['path_url'] = 'training';
 
		$role_resources_ids = $this->Xin_model->user_role_resource();
		if(in_array('54',$role_resources_ids)) {
			if(!empty($session)){ 
				$data['subview'] = $this->load->view("admin/client_account/client_account_list", $data, TRUE);
				$this->load->view('admin/layout/layout_main', $data); //page load
			} else {
				redirect('admin/');
			}
		} else {
			redirect('admin/dashboard');
		}
    }
  
	public function update_hospital() 
	{ 
		 
		if($this->input->post('hospital_name')) 
		{		
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
		 		
			/* Server side PHP input validation */
			$hospital_name    = $this->input->post('hospital_name');
			$location_id      = $this->input->post('location_id');
			$band_id          = $this->input->post('band_id');
			// $tarrif           = $this->input->post('tarrif');
			$email            = $this->input->post('email');
			$password         = $this->input->post('password');
			$phone            = $this->input->post('phone');
			$id               = $this->input->post('hospital_id');
			$created_on       = date("Y-m-d h:i:s");
	
			if($this->input->post('hospital_name')==='') {
	        	$Return['error'] = 'Hospital name is required';
			} else if($this->input->post('location_id')==='') {
	        	$Return['error'] = 'Location is required';
			} else if($this->input->post('band_id')==='') {
	        	$Return['error'] = 'Band is required';
			} else if($this->input->post('email')==='') {
	        	$Return['error'] = 'Email is required';
			} else if($this->input->post('password')==='') {
	        	$Return['error'] = 'Password is required';
			} else if($this->input->post('phone')==='') {
	        	$Return['error'] = 'Phone is required';
			}  	
				if($_FILES['logo']['size'] != 0) 
				{
					if(is_uploaded_file($_FILES['logo']['tmp_name'])) {
						//checking image type
						$allowed =  array('png','jpg','jpeg','gif');
						$filename = $_FILES['logo']['name'];
						$ext = pathinfo($filename, PATHINFO_EXTENSION);
						
						if(in_array($ext,$allowed)){
							$tmp_name = $_FILES["logo"]["tmp_name"];
							$bill_copy = "uploads/hospital/logo/";
							if (!file_exists($bill_copy)) 
							{
							    mkdir($bill_copy, 0777, true);
							}
							// basename() may prevent filesystem traversal attacks;
							// further validation/sanitation of the filename may be appropriate
							$lname = basename($_FILES["logo"]["name"]);
							$newfilename = 'logo_'.round(microtime(true)).'.'.$ext;
							move_uploaded_file($tmp_name, $bill_copy.$newfilename);
							$fname = $newfilename;
						} else {
							$Return['error'] = $this->lang->line('xin_error_attatchment_type');
						}
					}
				}
		
				
			if($Return['error']!=''){
	       		$this->output($Return);
	    	}
 			
 			if(isset($fname) and !empty($fname))
 			{
 				$data = array( 
					'hospital_name' => $hospital_name,
					'location_id'   => $location_id,
					'band_id'       => $band_id,
					// 'tarrif'        => $tarrif, 
					'email'         => $email, 
					'password'      => $password, 
					'phone'         => $phone, 
					'created_on'    => $created_on,  
					'logo_img'      => $fname,  
				);	
 			}else {
 				$data = array( 
					'hospital_name' => $hospital_name,
					'location_id'   => $location_id,
					'band_id'       => $band_id,
					// 'tarrif'        => $tarrif, 
					'email'         => $email, 
					'password'      => $password, 
					'phone'         => $phone, 
					'created_on'    => $created_on,  
				);
 			}
			
			$iresult = $this->Training_model->update2('xin_hospital',' hospital_id='.$id.' ',$data);
			if ($iresult) { 
				$Return['result'] = 'Hospital has been updated successfully.';	 
			} else {
				$Return['error'] = $this->lang->line('xin_error_msg');
			}
			$this->output($Return);
			exit;
		}
	}
	
 	public function delete_hospital() 
 	{
		/* Define return | here result is used to return user data and error for error message */
		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();
		$result = $this->Training_model->delete2('xin_hospital',' hospital_id = '.$id.' ');
		 
		if(isset($id)) {
			$Return['result'] = 'Hospital been deleted successfully';
		} else {
			$Return['error'] = $this->lang->line('xin_error_msg');
		}
		$this->output($Return);
	}
	 
	
	// training details
	public function detailAccount($id = null) 
 	{
 		 
		$session = $this->session->userdata('username');
		if(empty($session)){ 
			redirect('admin/');
		}
		if($id == '')
		{
			redirect('admin/Hospital');
		}else{
			$data['title'] = $this->Xin_model->site_title();
			$data['clients']   =  $this->Training_model->getAll2('xin_clients',' client_id ='.$id.'    ');
			$data['dependents_list']   =  $this->Training_model->getAll2('xin_clients_family',' client_id ='.$id.'    ');
			// $data['xin_services_hospital']   =  $this->Training_model->getAll2('xin_services_hospital',' hospital_id ='.$id.'    ');
			$data['breadcrumbs'] = 'Client Profile';
			$data['path_url'] = 'training_details';
			$role_resources_ids = $this->Xin_model->user_role_resource();
			if(in_array('54',$role_resources_ids)) 
			{
				if(!empty($session)){ 
					$data['subview'] = $this->load->view("admin/client_account/client_detail", $data,TRUE);
					$this->load->view('admin/layout/layout_main', $data); //page load
				} else {
					redirect('admin/');
				}
			}else {
				redirect('admin/dashboard');
			} 
		}    
     }

     public function editAccount($id = null) 
 	{
 		 
		$session = $this->session->userdata('username');
		if(empty($session)){ 
			redirect('admin/');
		}
		if($id == '')
		{
			redirect('admin/Hospital');
		}else{
			$data['title'] = $this->Xin_model->site_title();
			$data['clients']   =  $this->Training_model->getAll2('xin_clients',' client_id ='.$id.'    ');

			$data['dependents_list']   =  $this->Training_model->getAll2('xin_clients_family',' client_id ='.$id.'    ');
			// $data['xin_services_hospital']   =  $this->Training_model->getAll2('xin_services_hospital',' hospital_id ='.$id.'    ');
			$data['breadcrumbs'] = 'Client Profile';
			$data['path_url'] = 'training_details';
			$role_resources_ids = $this->Xin_model->user_role_resource();
			if(in_array('54',$role_resources_ids)) 
			{
				if(!empty($session)){ 
					$data['subview'] = $this->load->view("admin/client_account/client_edit", $data,TRUE);
					$this->load->view('admin/layout/layout_main', $data); //page load
				} else {
					redirect('admin/');
				}
			}else {
				redirect('admin/dashboard');
			} 
		}    
     }
	 
	 // Validate and update info in database
	public function update_status() {
	
		if($this->input->post('edit_type')=='update_status') {
			
			$id = $this->input->post('token_status');
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
		
			$data = array(
			'performance' => $this->input->post('performance'),
			'training_status' => $this->input->post('status'),
			'remarks' => $this->input->post('remarks')
			);
			
			$result = $this->Training_model->update_status($data,$id);		
			
			if ($result == TRUE) {
				$Return['result'] = $this->lang->line('xin_success_training_status_updated');
			} else {
				$Return['error'] = $this->lang->line('xin_error_msg');
			}
			$this->output($Return);
			exit;
		}
	}
	
	public function delete() {
		/* Define return | here result is used to return user data and error for error message */
		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
		$id = $this->uri->segment(4);
		$Return['csrf_hash'] = $this->security->get_csrf_hash();
		$result = $this->Training_model->delete_record($id);
		if(isset($id)) {
			$Return['result'] = $this->lang->line('xin_success_training_deleted');
		} else {
			$Return['error'] = $this->lang->line('xin_error_msg');
		}
		$this->output($Return);
	}
	public function bands() 
    {
		$session = $this->session->userdata('username');
		if(empty($session)){  
			redirect('admin/'); 
		}
		$system = $this->Xin_model->read_setting_info(1);
		if($system[0]->module_training!='true'){ 
			redirect('admin/dashboard'); 
		}
		$data['title']       =  'Bands | '.$this->Xin_model->site_title();
		$data['all_bands']   =  $this->Training_model->getAll2('xin_bands',' 1 order by band_id desc');
		$data['breadcrumbs'] = 'Band';
		$data['path_url']    = 'training';
 
		$role_resources_ids = $this->Xin_model->user_role_resource();
		if(in_array('54',$role_resources_ids)) {
			if(!empty($session)){ 
				$data['subview'] = $this->load->view("admin/hospitals/bands/bands_list", $data, TRUE);
				$this->load->view('admin/layout/layout_main', $data); //page load
			} else {
				redirect('admin/');
			}
		} else {
			redirect('admin/dashboard');
		}
    }
    public function add_band()
    { 
    	 
	 	 
		if($this->input->post('add_type')) 
		{		
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
		 		
			/* Server side PHP input validation */
			$band_name    = $this->input->post('band_name');
			$created_on   = date("Y-m-d h:i:s");
	
			if($this->input->post('band_name')==='') {
	        	$Return['error'] = 'Brand name is required';
			}  
				
			if($Return['error']!=''){
	       		$this->output($Return);
	    	}
 
			$data = array(
				'band_name' => $band_name,
				'created_on' => $created_on, 
			);
			$iresult = $this->Training_model->insertDataTB('xin_bands',$data);
			if ($iresult) { 
				$Return['result'] = 'Band has been added successfully.';	 
			} else {
				$Return['error'] = $this->lang->line('xin_error_msg');
			}
			$this->output($Return);
			exit;
		}
	}
	 public function update_band()
    { 
    	 
	 	 
		if($this->input->post('band_name')) 
		{		
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
		 		
			/* Server side PHP input validation */
			$band_name    = $this->input->post('band_name');
			$band_id      = $this->input->post('band_id');
			$created_on   = date("Y-m-d h:i:s");
	
			if($this->input->post('band_name')==='') {
	        	$Return['error'] = 'Brand name is required';
			}  
				
			if($Return['error']!=''){
	       		$this->output($Return);
	    	}
 
			$data = array(
				'band_name' => $band_name,
				'created_on' => $created_on, 
			);
			$iresult = $this->Training_model->update2('xin_bands','band_id = '.$band_id.' ',$data);
			if ($iresult) { 
				$Return['result'] = 'Band has been updated successfully.';	 
			} else {
				$Return['error'] = $this->lang->line('xin_error_msg');
			}
			$this->output($Return);
			exit;
		}
	}
	public function delete_bands() {
		/* Define return | here result is used to return user data and error for error message */
		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();
		$result = $this->Training_model->delete2('xin_bands',' band_id = '.$id.' ');
		 
		if(isset($id)) {
			$Return['result'] = 'Band has been deleted successfully';
		} else {
			$Return['error'] = $this->lang->line('xin_error_msg');
		}
		$this->output($Return);
	}
	public function locations() 
    {
		$session = $this->session->userdata('username');
		if(empty($session)){  
			redirect('admin/'); 
		}
		$system = $this->Xin_model->read_setting_info(1);
		if($system[0]->module_training!='true'){
			redirect('admin/dashboard');
		}
		$data['title'] =  'Location | '.$this->Xin_model->site_title();
		$data['all_locations']  =  $this->Training_model->getAll2('xin_location',' 1 order by location_id desc');
		$data['breadcrumbs'] = 'Location';
		$data['path_url'] = 'training'; 
		 
		$role_resources_ids = $this->Xin_model->user_role_resource();
		if(in_array('54',$role_resources_ids)) {
			if(!empty($session)){ 
				$data['subview'] = $this->load->view("admin/hospitals/locations/location_list", $data, TRUE);
				$this->load->view('admin/layout/layout_main', $data); //page load
			} else {
				redirect('admin/');
			}
		} else {
			redirect('admin/dashboard');
		}
    }
	public function add_location()
    { 
     
		if($this->input->post('add_type')) 
		{		
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
		 		
			/* Server side PHP input validation */
			$location_name    = $this->input->post('location_name');
			$created_on       = date("Y-m-d h:i:s");
	
			if($this->input->post('band_name')==='') {
	        	$Return['error'] = 'location name is required';
			}  
				
			if($Return['error']!=''){
	       		$this->output($Return);
	    	}
 
			$data = array(
				'location_name' => $location_name,
				'created_on' => $created_on, 
			);
			$iresult = $this->Training_model->insertDataTB('xin_location',$data);
			if ($iresult) { 
				$Return['result'] = 'Location has been added successfully.';	 
			} else {
				$Return['error'] = $this->lang->line('xin_error_msg');
			}
			$this->output($Return);
			exit;
		}
	}
	public function update_location()
    { 
     
		if($this->input->post('location_name')) 
		{		
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
		 		
			/* Server side PHP input validation */
			$location_name    = $this->input->post('location_name');
			$id               = $this->input->post('location_id');
			$created_on       = date("Y-m-d h:i:s");
	
			if($this->input->post('band_name')==='') {
	        	$Return['error'] = 'location name is required';
			}  
				
			if($Return['error']!=''){
	       		$this->output($Return);
	    	}
 
			$data = array(
				'location_name' => $location_name,
				'created_on' => $created_on, 
			);
			$iresult = $this->Training_model->update2('xin_location',' location_id ='.$id.' ',$data);
			if ($iresult) { 
				$Return['result'] = 'Location has been updated successfully.';	 
			} else {
				$Return['error'] = $this->lang->line('xin_error_msg');
			}
			$this->output($Return);
			exit;
		}
	}
	public function delete_location() {
		/* Define return | here result is used to return user data and error for error message */
		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();
		$result = $this->Training_model->delete2('xin_location',' location_id = '.$id.' ');
		 
		if(isset($id)) {
			$Return['result'] = 'Location has been deleted successfully';
		} else {
			$Return['error'] = $this->lang->line('xin_error_msg');
		}
		$this->output($Return);
	}
	public function organizations() 
    {
		$session = $this->session->userdata('username');
		if(empty($session)){  
			redirect('admin/'); 
		}
		$system = $this->Xin_model->read_setting_info(1);
		if($system[0]->module_training!='true'){
			redirect('admin/dashboard');
		}
		$data['title'] =  'Organization | '.$this->Xin_model->site_title();
		$data['all_organizations']  =  $this->Training_model->getAll2('xin_organization',' 1 order by id desc');
		$data['all_business']  =  $this->Training_model->getAll2('business_type',' 1 order by business_id desc');
		$data['all_locations']  =  $this->Training_model->getAll2('xin_location',' 1 order by location_id desc');
		$data['breadcrumbs'] = 'Organization';
		$data['path_url'] = 'training'; 
		 
		$role_resources_ids = $this->Xin_model->user_role_resource();
		if(in_array('54',$role_resources_ids)) {
			if(!empty($session)){ 
				$data['subview'] = $this->load->view("admin/hospitals/organization/organization_list", $data, TRUE);
				$this->load->view('admin/layout/layout_main', $data); //page load
			} else {
				redirect('admin/');
			}
		} else {
			redirect('admin/dashboard');
		}
    }
	public function add_organization()
    { 
     
		if($this->input->post('add_type')) 
		{		
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
		 		
			/* Server side PHP input validation */
			$name              = $this->input->post('name');
			$location_id       = $this->input->post('location_id');
			$contact_person    = $this->input->post('contact_person');
			$rc_number         = $this->input->post('rc_number');
			$type_business     = $this->input->post('type_business'); 
			$created_on        = date("Y-m-d h:i:s");
	
			if($this->input->post('name')==='') {
	        	$Return['error'] = 'Organization name is required';
			} else if($this->input->post('location_id')==='') {
	        	$Return['error'] = 'location is required';
			}else if($this->input->post('contact_person')==='') {
	        	$Return['error'] = 'Contact person is required';
			}else if($this->input->post('rc_number')==='') {
	        	$Return['error'] = 'RC number is required';
			}else if($this->input->post('type_business')==='') {
	        	$Return['error'] = 'Type of business is required';
			} /* Check if file uploaded..*/
			else if($_FILES['logo']['size'] == 0) {
				$fname = 'no file';
				$Return['error'] = $this->lang->line('xin_error_logo_field');
			} else {
				if(is_uploaded_file($_FILES['logo']['tmp_name'])) {
					//checking image type
					$allowed =  array('png','jpg','jpeg','gif');
					$filename = $_FILES['logo']['name'];
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					
					if(in_array($ext,$allowed)){
						$tmp_name = $_FILES["logo"]["tmp_name"];
						$bill_copy = "uploads/organization/logo/";
						if (!file_exists($bill_copy)) 
						{
						    mkdir($bill_copy, 0777, true);
						}
						// basename() may prevent filesystem traversal attacks;
						// further validation/sanitation of the filename may be appropriate
						$lname = basename($_FILES["logo"]["name"]);
						$newfilename = 'logo_'.round(microtime(true)).'.'.$ext;
						move_uploaded_file($tmp_name, $bill_copy.$newfilename);
						$fname = $newfilename;
					} else {
						$Return['error'] = $this->lang->line('xin_error_attatchment_type');
					}
				}
			}
				
			if($Return['error']!=''){
	       		$this->output($Return);
	    	}
 
			$data = array(
				'name'           => $name,
				'location_id'    => $location_id,
				'rc_number'      => $rc_number,
				'contact_person' => $contact_person,
				'type_business'  => $type_business,
				'logo_name'      => $fname,
				'created_on' => $created_on, 
			);
			$iresult = $this->Training_model->insertDataTB('xin_organization',$data);
			if ($iresult) { 
				$Return['result'] = 'Organization has been added successfully.';	 
			} else {
				$Return['error'] = $this->lang->line('xin_error_msg');
			}
			$this->output($Return);
			exit;
		}
	}
	public function update_organization()
    { 
     
		if($this->input->post('name')) 
		{		
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
		 		
			/* Server side PHP input validation */
			$organization_id   = $this->input->post('organization_id');
			$name              = $this->input->post('name');
			$location_id       = $this->input->post('location_id');
			$contact_person    = $this->input->post('contact_person');
			$rc_number         = $this->input->post('rc_number');
			$type_business     = $this->input->post('type_business'); 
			$created_on        = date("Y-m-d h:i:s");
	
			if($this->input->post('name')==='') {
	        	$Return['error'] = 'Organization name is required';
			} else if($this->input->post('location_id')==='') {
	        	$Return['error'] = 'location is required';
			}else if($this->input->post('contact_person')==='') {
	        	$Return['error'] = 'Contact person is required';
			}else if($this->input->post('rc_number')==='') {
	        	$Return['error'] = 'RC number is required';
			}else if($this->input->post('type_business')==='') {
	        	$Return['error'] = 'Type of business is required';
			} /* Check if file uploaded..*/
		  	if($_FILES['logo']['size'] != 0)  
		  	{
				if(is_uploaded_file($_FILES['logo']['tmp_name'])) 
				{
					//checking image type
					$allowed =  array('png','jpg','jpeg','gif');
					$filename = $_FILES['logo']['name'];
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					
					if(in_array($ext,$allowed)){
						$tmp_name = $_FILES["logo"]["tmp_name"];
						$bill_copy = "uploads/organization/logo/";
						if (!file_exists($bill_copy)) 
						{
						    mkdir($bill_copy, 0777, true);
						}
						// basename() may prevent filesystem traversal attacks;
						// further validation/sanitation of the filename may be appropriate
						$lname = basename($_FILES["logo"]["name"]);
						$newfilename = 'logo_'.round(microtime(true)).'.'.$ext;
						move_uploaded_file($tmp_name, $bill_copy.$newfilename);
						$fname = $newfilename;
					} else {
						$Return['error'] = $this->lang->line('xin_error_attatchment_type');
					}
				}
			}
				
			if($Return['error']!=''){
	       		$this->output($Return);
	    	}
 
	    	if(isset($fname) and !empty($fname))
	    	{
	    		$data = array( 
					'name'           => $name,
					'location_id'    => $location_id,
					'rc_number'      => $rc_number,
					'contact_person' => $contact_person,
					'type_business'  => $type_business,
					'logo_name'      => $fname, 
					'created_on'     => $created_on,  
				);
	    	}else {
	    		$data = array( 
					'name'           => $name,
					'location_id'    => $location_id,
					'rc_number'      => $rc_number,
					'contact_person' => $contact_person,
					'type_business'  => $type_business, 
					'created_on'     => $created_on,  
				);
	    	}
			
			$iresult = $this->Training_model->update2('xin_organization',' id ='.$organization_id.' ',$data);
			if ($iresult) { 
				$Return['result'] = 'Organization has been updated successfully.';	 
			} else {
				$Return['error'] = $this->lang->line('xin_error_msg');
			}
			$this->output($Return);
			exit;
		}
	}
	public function delete_organization() {
		/* Define return | here result is used to return user data and error for error message */
		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();
		$result = $this->Training_model->delete2('xin_organization',' id = '.$id.' ');
		 
		if(isset($id)) {
			$Return['result'] = 'Organization been deleted successfully';
		} else {
			$Return['error'] = $this->lang->line('xin_error_msg');
		}
		$this->output($Return);
	}
	public function business_type() 
    {
		$session = $this->session->userdata('username');
		if(empty($session)){  
			redirect('admin/'); 
		}
		$system = $this->Xin_model->read_setting_info(1);
		if($system[0]->module_training!='true'){
			redirect('admin/dashboard');
		}
		$data['title'] =  'Business | '.$this->Xin_model->site_title();
		$data['all_business']  =  $this->Training_model->getAll2('business_type',' 1 order by business_id desc');
		$data['breadcrumbs'] = 'Business';
		$data['path_url'] = 'training'; 
		 
		$role_resources_ids = $this->Xin_model->user_role_resource();
		if(in_array('54',$role_resources_ids)) {
			if(!empty($session)){ 
				$data['subview'] = $this->load->view("admin/hospitals/business/business_list", $data, TRUE);
				$this->load->view('admin/layout/layout_main', $data); //page load
			} else {
				redirect('admin/');
			}
		} else {
			redirect('admin/dashboard');
		}
    }
	public function add_business_type()
    { 
     
		if($this->input->post('add_type')) 
		{		
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
		 		
			/* Server side PHP input validation */
			$business_name    = $this->input->post('business_name');
			$created_on       = date("Y-m-d h:i:s");
	
			if($this->input->post('band_name')==='') {
	        	$Return['error'] = 'Business type is required';
			}  
				
			if($Return['error']!=''){
	       		$this->output($Return);
	    	}
 
			$data = array(
				'business_name' => $business_name,
				'created_on' => $created_on, 
			);
			$iresult = $this->Training_model->insertDataTB('business_type',$data);
			if ($iresult) { 
				$Return['result'] = 'Business type has been added successfully.';	 
			} else {
				$Return['error'] = $this->lang->line('xin_error_msg');
			}
			$this->output($Return);
			exit;
		}
	}
	public function update_business()
    { 
     
		if($this->input->post('business_name')) 
		{		
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
		 		
			/* Server side PHP input validation */
			$business_name    = $this->input->post('business_name');
			$business_id      = $this->input->post('business_id');
			$created_on       = date("Y-m-d h:i:s");
	
			if($this->input->post('band_name')==='') {
	        	$Return['error'] = 'Business type is required';
			}  
				
			if($Return['error']!=''){
	       		$this->output($Return);
	    	}
 
			$data = array(
				'business_name' => $business_name,
				'created_on' => $created_on, 
			);
			$iresult = $this->Training_model->update2('business_type','business_id = '.$business_id.' ',$data);
			if ($iresult) { 
				$Return['result'] = 'Business type has been updated successfully.';	 
			} else {
				$Return['error'] = $this->lang->line('xin_error_msg');
			}
			$this->output($Return);
			exit;
		}
	}
	public function delete_business() {
		/* Define return | here result is used to return user data and error for error message */
		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();
		$result = $this->Training_model->delete2('business_type',' business_id = '.$id.' ');
		 
		if(isset($id)) {
			$Return['result'] = 'Business type has been deleted successfully';
		} else {
			$Return['error'] = $this->lang->line('xin_error_msg');
		}
		$this->output($Return);
	}
	public function import_drugs()
	{
		if($this->input->post('hospital_id')) 
		{		
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
			//validate whether uploaded file is a csv file
	   		$csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
			$hospital_id = $this->input->post('hospital_id');
			if($_FILES['file']['name']==='') 
			{
				$this->session->set_flashdata('error','Error! Allowed file size is 2MB. ');
				redirect($_SERVER['HTTP_REFERER']);
			} else 
			{
				if(in_array($_FILES['file']['type'],$csvMimes))
				{
					if(is_uploaded_file($_FILES['file']['tmp_name']))
					{ 
						// check file size
						if(filesize($_FILES['file']['tmp_name']) > 2000000) {
							$this->session->set_flashdata('error','Error! Allowed file size is 2MB. ');
							redirect($_SERVER['HTTP_REFERER']);
							 
						} else 
						{ 
							//open uploaded csv file with read only mode
							$csvFile = fopen($_FILES['file']['tmp_name'], 'r');
							
							//skip first line 
							fgetcsv($csvFile);
							
							//parse data from csv file line by line
							while(($line = fgetcsv($csvFile)) !== FALSE)
							{
							 	if (isset($line[0]) and isset($line[1])) 
							 	{
							 		$data = array(
										'drug_name' => htmlentities($line[0]),
										'drug_price' => htmlentities($line[1]), 
										'hospital_id' => $hospital_id,  
										'created_on' => date('Y-m-d h:i:s')
									);
									$last_insert_id = $this->Training_model->insertDataTB('xin_hospital_drugs',$data); 
							 	}
								 
							}					
							//close opened csv file
							fclose($csvFile);
			
							$this->session->set_flashdata('success','Success! Data has been imported successfully');
							redirect($_SERVER['HTTP_REFERER']);
						}
					}else{
						$this->session->set_flashdata('error','Error! unable to upload file.');
						redirect($_SERVER['HTTP_REFERER']);
					}
				}else{
					$this->session->set_flashdata('error','Error! Invalid file format.');
					redirect($_SERVER['HTTP_REFERER']);
					 
				}
			}  
		}
	}
	public function import_hospital_services()
	{
		if($this->input->post('hospital_id')) 
		{		
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
			//validate whether uploaded file is a csv file
	   		$csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
			$hospital_id = $this->input->post('hospital_id');
			if($_FILES['file']['name']==='') 
			{
				$this->session->set_flashdata('error','Error! Allowed file size is 2MB. ');
				redirect($_SERVER['HTTP_REFERER']);
			} else 
			{
				if(in_array($_FILES['file']['type'],$csvMimes))
				{
					if(is_uploaded_file($_FILES['file']['tmp_name']))
					{ 
						// check file size
						if(filesize($_FILES['file']['tmp_name']) > 2000000) {
							$this->session->set_flashdata('error','Error! Allowed file size is 2MB. ');
							redirect($_SERVER['HTTP_REFERER']);
							 
						} else 
						{ 
							//open uploaded csv file with read only mode
							$csvFile = fopen($_FILES['file']['tmp_name'], 'r');
							
							//skip first line 
							fgetcsv($csvFile);
							
							//parse data from csv file line by line
							while(($line = fgetcsv($csvFile)) !== FALSE)
							{
							
								$data = array(
									'service_name' => $line[0],
									'service_price' => $line[1], 
									'hospital_id' => $hospital_id,  
									'created_on' => date('Y-m-d h:i:s')
								);
								$last_insert_id = $this->Training_model->insertDataTB('xin_services_hospital',$data);  
							}					
							//close opened csv file
							fclose($csvFile);
			
							$this->session->set_flashdata('success','Success! Data has been imported successfully');
							redirect($_SERVER['HTTP_REFERER']);
						}
					}else{
						$this->session->set_flashdata('error','Error! unable to upload file.');
						redirect($_SERVER['HTTP_REFERER']);
					}
				}else{
					$this->session->set_flashdata('error','Error! Invalid file format.');
					redirect($_SERVER['HTTP_REFERER']);
					 
				}
			}  
		}
	}
	public function update_services()
    { 
    	 
	 	 
		if($this->input->post('service_name')) 
		{		
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
		 		
			/* Server side PHP input validation */
			$service_name       = $this->input->post('service_name');
			$service_price      = $this->input->post('service_price');
			$service_id_edit    = $this->input->post('service_id_edit');
			 
	
			if($this->input->post('service_name')==='') {
	        	$Return['error'] = 'Service name is required';
			} else if($this->input->post('service_price')==='') {
	        	$Return['error'] = 'Service price is required';
			}  
				
			if($Return['error']!=''){
	       		$this->output($Return);
	    	}
 
			$data = array(
				'service_name'  => $service_name,
				'service_price' => $service_price, 
			);
			$iresult = $this->Training_model->update2('xin_services_hospital','id = '.$service_id_edit.' ',$data);
			if ($iresult) { 
				$Return['result'] = 'Service has been updated successfully.';	
				$this->session->set_flashdata('success','Service has been updated successfully.');
				redirect($_SERVER['HTTP_REFERER']); 
			} else {
				$this->session->set_flashdata('error',$this->lang->line('xin_error_msg'));
				redirect($_SERVER['HTTP_REFERER']); 
				$Return['error'] = $this->lang->line('xin_error_msg');
			}
			$this->output($Return);
			exit;
		}
	}
	public function update_drug()
    { 
    	 
	 	 
		if($this->input->post('drug_nam')) 
		{		
			/* Define return | here result is used to return user data and error for error message */
			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
			$Return['csrf_hash'] = $this->security->get_csrf_hash();
		 		
			/* Server side PHP input validation */
			$drug_nam       = $this->input->post('drug_nam');
			$drug_price     = $this->input->post('drug_price');
			$drug_id_edit   = $this->input->post('drug_id_edit');
			 
	
			if($this->input->post('drug_nam')==='') {
	        	$Return['error'] = 'Drug name is required';
			} else if($this->input->post('drug_price')==='') {
	        	$Return['error'] = 'Drug price is required';
			}  
				
			if($Return['error']!=''){
	       		$this->output($Return);
	    	}
 
			$data = array(
				'drug_name' => $drug_nam,
				'drug_price' => $drug_price, 
			);
			$iresult = $this->Training_model->update2('xin_hospital_drugs','drug_id = '.$drug_id_edit.' ',$data);
			if ($iresult) { 
				$Return['result'] = 'Drug has been updated successfully.';	 
			} else {
				$Return['error'] = $this->lang->line('xin_error_msg');
			}
			$this->output($Return);
			exit;
		}
	}
	public function delete_service() 
 	{
		/* Define return | here result is used to return user data and error for error message */
		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');
		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();
		$result = $this->Training_model->delete2('xin_services_hospital',' id = '.$id.' ');
		 
		if(isset($id)) {
			$Return['result'] = 'Service been deleted successfully'; 
			$this->session->set_flashdata('success','Service has been deleted successfully.');
			redirect($_SERVER['HTTP_REFERER']); 
		} else {
			$Return['result'] = $this->lang->line('xin_error_msg');	
			$this->session->set_flashdata('error',$this->lang->line('xin_error_msg'));
			redirect($_SERVER['HTTP_REFERER']); 
		}
		$this->output($Return);
	}
}
