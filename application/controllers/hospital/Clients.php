<?php

 /**

 * NOTICE OF LICENSE

 *

 * This source file is subject to the HRSALE License

 * that is bundled with this package in the file license.txt.

 * It is also available through the world-wide-web at this URL:

 * http://www.hrsale.com/license.txt

 * If you did not receive a copy of the license and are unable to

 * obtain it through the world-wide-web, please send an email

 * to hrsalesoft@gmail.com so we can send you a copy immediately.

 *

 * @author   HRSALE

 * @author-email  hrsalesoft@gmail.com

 * @copyright  Copyright © hrsale.com. All Rights Reserved

 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Clients extends MY_Controller

{

	

	public function __construct()

     {

          parent::__construct();

			//load the model		

			$this->load->model('Login_model');

			$this->load->model('Employees_model');

			$this->load->model('Users_model');

			$this->load->library('email');

			$this->load->model("Xin_model");

			$this->load->model("Designation_model");

			$this->load->model("Department_model");

			$this->load->model("Location_model");

			$this->load->model("Clients_model");
			$this->load->model("Training_model");

     }

	 

	 /*Function to set JSON output*/

	public function output($Return=array()){

		/*Set response header*/

		header("Access-Control-Allow-Origin: *");

		header("Content-Type: application/json; charset=UTF-8");

		/*Final JSON response*/

		exit(json_encode($Return));

	}

	

	public function index()

	{		

		if(isset($_GET['bill_request']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];
			$code = "";

			$iresult = $this->Clients_model->update_diagnose_status_record('1',$code,$id);
			redirect($_SERVER['HTTP_REFERER']);
		}


		$data['title'] = 'HR Software';
		$var = $this->session->userdata;

		// $this->load->view('hospital/clients/dependant_list', $data);	
		$data['all_hospital_drugs']   =  $this->Training_model->getAll2('xin_hospital_drugs',' hospital_id ='.$var['hospital_id']['hospital_id'].'    ');

		$data['xin_services_hospital']   =  $this->Training_model->getAll2('xin_services_hospital',' hospital_id ='.$var['hospital_id']['hospital_id'].'    ');
		$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients($var['hospital_id']['hospital_id'],'');
		$data['query'] = $this->db->last_query();
		$data['clients']   =  $this->Training_model->getAll2('xin_clients',' 1 order by client_id asc');
		$data['subview'] = $this->load->view("hospital/client/index", $data, TRUE);

		$this->load->view('hospital/layout/layout_main', $data); //page load

	}

	public function insert_clients_diagnose()
	{
		// echo "Wahid Akram here data<br />";

		$type = "";
		$services_ids = array();
		$drugs_ids = array();
		$total = 0;
		$var = $this->session->userdata;

		$services = $this->input->post('diagnose_services');
		$drugs = $this->input->post('diagnose_drugs');

		foreach ($services as $key => $value) {
			preg_match('/^(.*\[)(.*)(\])/', $value, $match);
			$arr = preg_split('/\[.*?\]/',$value);
			// echo "Clienttt = ".$arr[0]."<br />";
			array_push($services_ids,$arr[0]);
			$total = $total + $match[2];
			// echo "Clienttt = ".$match[2]."<br />";
			// print_r($arr);
		}
		foreach ($drugs as $key => $value) {
			preg_match('/^(.*\[)(.*)(\])/', $value, $match);
			$arr = preg_split('/\[.*?\]/',$value);
			// echo "Clienttt = ".$arr[0]."<br />";
			array_push($drugs_ids,$arr[0]);
			$total = $total + $match[2];
			// echo "Clienttt = ".$match[2]."<br />";
		}

		// print_r($services_ids);
		// print_r($drugs_ids);
		// echo "Total ww= ".$total."<br />";
		// die;
		$arr = explode(":", $this->input->post('diagnose_client'));
		$arr2 = explode("-", $arr[0]);
		$n = count($arr2);
		if($n == 3) { $type = "C"; } else { $type = "D"; }

		$data = array(
			'diagnose_hospital_id'         => $var['hospital_id']['hospital_id'],
			'diagnose_client_id'         => $arr[1],
			'diagnose_user_type'         => $type,
			'diagnose_date'         => Date('Y-m-d'),
			'diagnose_diagnose'         => $this->input->post('diagnose_diagnose'),
			'diagnose_procedure'         => $this->input->post('diagnose_procedure'),
			'diagnose_investigation'         => $this->input->post('diagnose_investigation'),
			'diagnose_medical'         => $this->input->post('diagnose_medical'),
			'diagnose_total_sum'         => $total,
			'diagnose_status'         => '1'
		);

		$result = $this->Clients_model->add_diagnose_client($data,$services_ids,$drugs_ids);
		redirect($_SERVER['HTTP_REFERER']);
		// die;
	}

	public function edit_diagnose()
	{		

		if($this->input->post("diagnose_client")) {

			$type = "";
			$services_ids = array();
			$drugs_ids = array();
			$total = 0;
			$var = $this->session->userdata;

			$services = $this->input->post('diagnose_services');
			$drugs = $this->input->post('diagnose_drugs');

			foreach ($services as $key => $value) {
				preg_match('/^(.*\[)(.*)(\])/', $value, $match);
				$arr = preg_split('/\[.*?\]/',$value);
				// echo "Clienttt = ".$arr[0]."<br />";
				array_push($services_ids,$arr[0]);
				$total = $total + $match[2];
				// echo "Clienttt = ".$match[2]."<br />";
				// print_r($arr);
			}
			foreach ($drugs as $key => $value) {
				preg_match('/^(.*\[)(.*)(\])/', $value, $match);
				$arr = preg_split('/\[.*?\]/',$value);
				// echo "Clienttt = ".$arr[0]."<br />";
				array_push($drugs_ids,$arr[0]);
				$total = $total + $match[2];
				// echo "Clienttt = ".$match[2]."<br />";
			}

			// print_r($services_ids);
			// print_r($drugs_ids);
		// echo "Total ww= ".$total."<br />";
		// die;
			$arr = explode(":", $this->input->post('diagnose_client'));
			$arr2 = explode("-", $arr[0]);
			$n = count($arr2);
			if($n == 3) { $type = "C"; } else { $type = "D"; }

			if($this->input->post("diagnose_rejection") == "bill") {
				$data = array(
					'diagnose_hospital_id'         => $var['hospital_id']['hospital_id'],
					'diagnose_client_id'         => $arr[1],
					'diagnose_user_type'         => $type,
					'diagnose_date'         => Date('Y-m-d'),
					'diagnose_diagnose'         => $this->input->post('diagnose_diagnose'),
					'diagnose_procedure'         => $this->input->post('diagnose_procedure'),
					'diagnose_investigation'         => $this->input->post('diagnose_investigation'),
					'diagnose_medical'         => $this->input->post('diagnose_medical'),
					'diagnose_total_sum'         => $total,
					'diagnose_bill_status'         => '1',
		    		'diagnose_bill_approve_by' => NULL
				);
			} else {
				$data = array(
					'diagnose_hospital_id'         => $var['hospital_id']['hospital_id'],
					'diagnose_client_id'         => $arr[1],
					'diagnose_user_type'         => $type,
					'diagnose_date'         => Date('Y-m-d'),
					'diagnose_diagnose'         => $this->input->post('diagnose_diagnose'),
					'diagnose_procedure'         => $this->input->post('diagnose_procedure'),
					'diagnose_investigation'         => $this->input->post('diagnose_investigation'),
					'diagnose_medical'         => $this->input->post('diagnose_medical'),
					'diagnose_total_sum'         => $total,
					'diagnose_status'         => '1',
		    		'diagnose_status_approve_by' => NULL
				);
			}

			// print_r($data); echo "<br />";
			// print_r($services_ids); echo "<br />";
			// print_r($drugs_ids); echo "<br />";
			// die;
			// $iresult = $this->Clients_model->update_auth_status_again('1',$_GET['eid']);
			$result = $this->Clients_model->edit_diagnose_client($data,$services_ids,$drugs_ids,$_GET['eid']);
			redirect('hospital/clients/index');
		}
		// die;
		if($_GET['id']) {
		// echo "wahidd page accesseddd..";
			$data['title'] = 'HR Software';
			$var = $this->session->userdata;

			// $this->load->view('hospital/clients/dependant_list', $data);	
			$data['all_hospital_drugs']   =  $this->Training_model->getAll2('xin_hospital_drugs',' hospital_id ='.$var['hospital_id']['hospital_id'].'    ');

			$data['xin_services_hospital']   =  $this->Training_model->getAll2('xin_services_hospital',' hospital_id ='.$var['hospital_id']['hospital_id'].'    ');
			$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients($var['hospital_id']['hospital_id'],$_GET['id']);
			$data['query'] = $this->db->last_query();
			$data['clients']   =  $this->Training_model->getAll2('xin_clients',' 1 order by client_id asc');
			$data['subview'] = $this->load->view("hospital/client/edit_diagnose", $data, TRUE);

			$this->load->view('hospital/layout/layout_main', $data); //page load
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	public function fetch_diagnose_id()
	{
		$fetched_result = array();
		$did = $this->input->post('id');

		$fetched_result = $this->Clients_model->view_individual_hospital_diagnose_clients($did);
		// echo $this->db->last_query();

        $services = $this->Clients_model->read_individual_hospital_diagnose_services($fetched_result["diagnose_id"]);
        $drugs = $this->Clients_model->read_individual_hospital_diagnose_drugs($fetched_result["diagnose_id"]);

	    $hcp = preg_replace('/\s+/', '', $fetched_result["hospital_name"]);
	    $date = date("Ymd", strtotime($fetched_result["diagnose_date_time"]));

		echo '
          <div class="col-lg-12">
                <div class="col-lg-4 text-center">
                    <h5 class="text-info"><b>Generated Code:</b> '; if(empty($fetched_result["diagnose_generated_code"])) echo "-----"; else echo "HCP-".$hcp."-".$fetched_result["diagnose_generated_code"]."-".$date."-".$fetched_result["diagnose_generated_code"]; 
               echo'</h5>
                </div>
                <div class="col-lg-4 text-center">
                    <h5 class="text-info"><b>Hospital:</b> '; if(empty($fetched_result["hospital_name"])) echo "-----"; else echo $fetched_result["hospital_name"]; echo'</h5>
                </div>
                <div class="col-lg-4 text-center">
                    <h5 class="text-info"><b>Date:</b> '.$fetched_result["diagnose_date"].'</h5>
                </div>
                <div class="col-lg-12">
                    <table class="table" style="padding: 100px; border: 0px;">
                      <tbody>
                        <tr>
                          <td class="col-lg-2" align="right"><h5><b>Client Name:</b></h5></td>
                          <td align="left">'; if($fetched_result["diagnose_user_type"] == 'C') { echo $fetched_result["cname"]." ".$fetched_result["clname"]." ".$fetched_result["coname"]; } else { echo $fetched_result["dname"]." ".$fetched_result["dlname"]." ".$fetched_result["doname"]; } echo '</td>
                          <td class="col-lg-2" align="right"><h5><b>Diagnose:</b></h5></td>
                          <td align="left">'.$fetched_result["diagnose_diagnose"].'</td>
                        </tr>
                        <tr>
                          <td class="col-lg-2" align="right"><h5><b>Procedure:</b></h5></td>
                          <td align="left">'.$fetched_result["diagnose_procedure"].'</td>
                          <td class="col-lg-2" align="right"><h5><b>Investigation:</b></h5></td>
                          <td align="left">'.$fetched_result["diagnose_investigation"].'</td>
                        </tr>
                        <tr>
                          <td class="col-lg-2" align="right"><h5><b>Medical:</b></h5></td>
                          <td colspan="3" align="left">'.$fetched_result["diagnose_medical"].'</td>
                        </tr>
                        <tr>
                          <th colspan="2" class="bg-dark text-center" style="background-color: #343a40; color: #FFF;">DRUGS</th>
                          <th colspan="2" class="text-center bg-dark" style="background-color: #343a40; color: #FFF;">SERVICES</th>
                        </tr>
                        <tr>
                          <td colspan="2">
                            <table class="table table-bordered col-lg-6">
                              <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Price</th>
                                </tr>
                              </thead>
                              <tbody>';
                            if(!empty($drugs)) {
                              foreach ($drugs as $key => $dvalue) {
                              echo '
                                <tr>
                                  <td>'.$dvalue->drug_name.'</td>
                                  <td>'.number_format($dvalue->drug_price).'</td>
                                </tr>';
                                }
                            }
                            echo '
                              </tbody>
                            </table>
                          </td>
                          <td colspan="2">
                            <table class="table table-bordered col-lg-6">
                              <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Price</th>
                                </tr>
                              </thead>
                              <tbody>';
                            if(!empty($services)) {
                              foreach ($services as $key => $svalue) {
                              	echo '
                                <tr>
                                  <td>'.$svalue->service_name.'</td>
                                  <td>'.number_format($svalue->service_price).'</td>
                                </tr>';
                              }
                            }
                            echo '
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" align="right"><h3 class="text-danger">Total Bill: </h3></td>
                          <td colspan="1" align="left"><h3 class="text-danger">'.number_format($fetched_result["diagnose_total_sum"]).'.00</h3></td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>';
	}

	public function fetch_reason()
	{
		$fetched_result = array();
		$did = $this->input->post('id');
		$for = $this->input->post('type');

		$fetched_result = $this->Clients_model->view_individual_hospital_diagnose_clients($did);
		// echo $this->db->last_query();
		if($for == 1)
			echo '<p>'.$fetched_result["diagnose_reject_reason"].'</p>';
		else 
			echo '<p>'.$fetched_result["diagnose_bill_reject_reason"].'</p>';
	}
}