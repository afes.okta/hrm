<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Client
 *
 * @author root
 */
class Client extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api/MyModel');
    }
    
    public function index()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET'){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                if($response['status'] == 200){
                    $resp = $this->MyModel->client_all();
                    json_output($response['status'],$resp);
                }
            }
        }
    }
    
    public function detail($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET'){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                if($response['status'] == 200){
                    $resp = $this->MyModel->client_detail_data($id);
                    json_output($response['status'],$resp);
                }
            }
        }
    }
    
    public function client_familly($id){
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET'){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                if($response['status'] == 200){
                    $resp = $this->MyModel->client_fammily_detail_data($id);
                    json_output($response['status'],$resp);
                }
            }
        }
    }
    
    public function client_dependant($id){
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET'){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                if($response['status'] == 200){
                    $resp = $this->MyModel->client_dependant_data($id);
                    json_output($response['status'],$resp);
                }
            }
        }
    }
       
    public function update_dependant($id) {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'PUT' || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if($response['status'] == 200){
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['last_modified'] = date('Y-m-d H:i:s');
                        
                    $from = new DateTime($params['dob']);
                    $to   = new DateTime('today');
                    $age  = $from->diff($to)->y; 
                    if( ( $age == 21 or $age > 21)  and $params['relation']  != 'wife' )
                    {
                        $respStatus = 400;
                        $resp = array('status' => 400,'message' =>  'WE ARE SORRY YOU CAN NOT ADD THIS DEPENDANT AS HE/SHE HAS EXCEEDED AGE LIMITS AS A DEPENDANT KINDLY APPLY HERE TO GET AN INDIVIDUAL PLAN FOR HIM/HER');                                               
                    }  
                    if ($params['name'] == "" || $params['contact_number'] == "" || $params['email'] == "" || $params['username'] == "" || $params['relation'] == "" || 
                           $params['dob'] == "" || $params['hospital_id'] == "" || $params['last_name'] == "" || $params['sex'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400,'message' =>  'Name or Email can\'t empty');
                    } else {
                        $resp = $this->MyModel->client_dependant_update_data($id,$params);
                    }
                    json_output($respStatus,$resp);
                }
            }
        }
    }
    
    public function new_dependant($id) {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST' || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if($response['status'] == 200){
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['last_modified'] = date('Y-m-d H:i:s');
                    $params['client_id'] = $id;
                    $from = new DateTime($params['dob']);
                    $to   = new DateTime('today');
                    $age  = $from->diff($to)->y; 
                    if( ( $age == 21 or $age > 21)  and $params['relation']  != 'wife' )
                    {
                        $respStatus = 400;
                        $resp = array('status' => 400,'message' =>  'WE ARE SORRY YOU CAN NOT ADD THIS DEPENDANT AS HE/SHE HAS EXCEEDED AGE LIMITS AS A DEPENDANT KINDLY APPLY HERE TO GET AN INDIVIDUAL PLAN FOR HIM/HER');                                               
                    }  
                    if ($params['name'] == "" || $params['contact_number'] == "" || $params['email'] == "" || $params['username'] == "" || $params['relation'] == "" || 
                           $params['dob'] == "" || $params['hospital_id'] == "" || $params['last_name'] == "" || $params['sex'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400,'message' =>  'Name or Email can\'t empty');
                    } else {
                        $resp = $this->MyModel->client_dependant_update_data($id,$params);
                    }
                    json_output($respStatus,$resp);
                }
            }
        }
    }
    
    public function update_password($id) {
        $method = $_SERVER['REQUEST_METHOD'];
        $options = array('cost' => 12);
        if($method != 'PUT' || $this->uri->segment(4) == '' ){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if($response['status'] == 200){
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['last_modified'] = date('Y-m-d H:i:s');                    
                    if ($params['client_password'] == "" ) {
                        $respStatus = 400;
                        $resp = array('status' => 400,'message' =>  'Password can\'t empty');
                    } else {
                        $password_hash = password_hash($params['client_password'], PASSWORD_BCRYPT, $options);
                        $params['client_password'] = $password_hash;
                        $resp = $this->MyModel->client_dependant_update_data($id,$params);
                    }
                    json_output($respStatus,$resp);
                }
            }
        }
    }
    
    public function request_change_hospital() {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if($response['status'] == 200){
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['created_on'] = date('Y-m-d H:i:s');
                    if ($params['client_id'] == "" || $params['hospital_id']) {
                        $respStatus = 400;
                        $resp = array('status' => 400,'message' =>  'Client and Hospital can\'t empty');
                    } else {
                        $resp = $this->MyModel->client_request_change_hospital($params);
                    }
                    json_output($respStatus,$resp);
                }
            }
        }
    }

	/**
	 * use for dashboard api
	 * get total dependant
	 * @param $client_id
	 */
    public function total_dependant($client_id) {
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(4) == ''){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
				$response = $this->MyModel->auth();
				$respStatus = $response['status'];
				if($response['status'] == 200){
					$resp = $this->MyModel->client_fammily_detail_data($client_id);
					if (empty($resp)) {
						json_output(400,array('status' => 400,'message' => 'Bad request.'));
					}else {
						json_output($respStatus, $resp);
					}
				}
			}
		}
	}

	/**
	 * use for dashboard api
	 * get total total hospital
	 * @param $client_id
	 */
	public function total_hospital($client_id) {
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(4) == ''){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
				$response = $this->MyModel->auth();
				$respStatus = $response['status'];
				$total_hospital = 0;
				if($response['status'] == 200){
					$clientinfo = $this->MyModel->client_detail_data($client_id);
					if (empty($clientinfo)) {
						json_output(201,array('status' => 201,'message' => 'Data Missing.'));
					}else {
						if(isset($clientinfo->subscription_ids) and !empty($clientinfo->subscription_ids) )
						{
							$subs = $this->MyModel->get_xin_subscription($clientinfo->subscription_ids);
							if (isset($subs->band_types) AND !empty($subs->band_types))
							{
								$subs_pkg = $subs->plan_name;
								$bands  = explode(',', $subs->band_types);
								foreach ($bands as $key2 => $band)
								{
									$hos_data = $this->MyModel->hospital_detail_data($band);
									if(isset($hos_data->hospital_name))
									{
										$total_hospital++;
									}
								}
							}
						}
						if ($total_hospital == 0) {
							json_output(201, array('status'=>201,'Total Hospital'=>$total_hospital));
						}else {
							json_output(200, array('status'=>200,'Total Hospital'=>$total_hospital));
						}
					}
				}
			}
		}
	}

	/**
	 * use for dashboard api
	 * get total request
	 * @param $client_id
	 */
	public function total_hospital_request($client_id) {
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(6) == ''){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
				$response = $this->MyModel->auth();
				$respStatus = $response['status'];
				if($response['status'] == 200){
					$resp = $this->MyModel->count_client_request_hospital($client_id);
					json_output($respStatus,$resp);
				}
			}
		}
	}

	/**
	 * use for dashboard api
	 * get total dependent request
	 * @param $client_id
	 */
	public function total_dependant_request($client_id) {
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(6) == ''){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
				$response = $this->MyModel->auth();
				$respStatus = $response['status'];
				if($response['status'] == 200){
					$resp = $this->MyModel->count_client_familly_request_hospital($client_id);
					json_output($respStatus,$resp);
				}
			}
		}
	}

}
