<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * Created_at: 2019-12-10
 * Created_by: Afes Oktavianus
 * Class Hospital_api
 */
class Hospital_api extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('api/Hospital_api_model');
    }

//	index method to display all data hospital using the ge method
    public function index_get() {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            $this->response(array('status' => 400, 'message' => 'Bad request.'), 400);
        } else {
            $response = $this->Hospital_api_model->all_hospital();
            $this->response($response);
        }
    }
    
    public function count_get() {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            $this->response(array('status' => 400, 'message' => 'Bad request.'), 400);
        } else {
            $resp = $this->Hospital_api_model->count();
            $this->response($resp);
        }
    }
    
    public function countby_get($id) {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE) {
            $this->response(array('status' => 400, 'message' => 'Bad request.'), 400);
        } else {
            $resp = $this->Hospital_api_model->count_where(['hospital_id' => $id]);
            $this->response($resp);
        }
    }

    public function detail_get($id) {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE) {
            $this->response(array('status' => 400, 'message' => 'Bad request.'), 400);
        } else {
            $resp = $this->Hospital_api_model->find(['hospital_id' => $id]);
            $this->response($resp);
        }
    }

    public function create_post() {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            $this->response(array('status' => 400, 'message' => 'Bad request.'), 400);
        } else {            
            $params = json_decode(file_get_contents('php://input'), TRUE);
            if ($params['title'] == "" || $params['author'] == "") {
                $respStatus = 400;
                $resp = array('status' => 400, 'message' => 'Title & Author can\'t empty');
            } else {
                $resp = $this->Hospital_api_model->add_hospital($params);
            }
            json_output($respStatus, $resp);            
        }
    }

    public function update_put($id) {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'PUT' || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE) {
            $this->response(array('status' => 400, 'message' => 'Bad request.'), 400);
        } else {
            $params = json_decode(file_get_contents('php://input'), TRUE);
            $params['updated_at'] = date('Y-m-d H:i:s');
            if ($params['title'] == "" || $params['author'] == "") {
                $respStatus = 400;
                $resp = array('status' => 400, 'message' => 'Title & Author can\'t empty');
            } else {
                $resp = $this->Hospital_api_model->update_hospital($id, $params);
            }
            $this->response($resp);            
        }
    }

    public function destroy($id) {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'DELETE' || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE) {
            $this->response(array('status' => 400, 'message' => 'Bad request.'), 400);
        } else {
            $resp = $this->Hospital_api_model->delete_hospital($this->uri->segment(4));
            $this->response($resp);        
        }
    }
    
    public function sum_dependent_hospital_get($id) {        
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE) {
            $this->response(array('status' => 400, 'message' => 'Bad request.'), 400);
        } else {
            $resp = $this->Hospital_api_model->sum('service_price',['hospital_id'=>$this->uri->segment(4)]);
            $this->response($resp);
        }
    }        

}
