-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 09, 2019 at 08:39 AM
-- Server version: 5.5.62
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hmo`
--

-- --------------------------------------------------------

--
-- Table structure for table `business_type`
--

CREATE TABLE `business_type` (
  `business_id` int(11) NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_type`
--

INSERT INTO `business_type` (`business_id`, `business_name`, `created_on`) VALUES
(2, 'IT Office', '2019-11-01 05:48:41'),
(3, 'Insurance', '2019-11-01 11:48:17'),
(4, 'Finance and Banking', '2019-11-01 11:48:35'),
(5, 'School', '2019-11-01 11:49:01'),
(6, 'Record Label2', '2019-11-02 06:05:44');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_advance_salaries`
--

CREATE TABLE `xin_advance_salaries` (
  `advance_salary_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `month_year` varchar(255) NOT NULL,
  `advance_amount` varchar(255) NOT NULL,
  `one_time_deduct` varchar(50) NOT NULL,
  `monthly_installment` varchar(255) NOT NULL,
  `total_paid` varchar(255) NOT NULL,
  `reason` text NOT NULL,
  `status` int(11) DEFAULT NULL,
  `is_deducted_from_salary` int(11) DEFAULT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_announcements`
--

CREATE TABLE `xin_announcements` (
  `announcement_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `start_date` varchar(200) NOT NULL,
  `end_date` varchar(200) NOT NULL,
  `company_id` int(111) NOT NULL,
  `department_id` int(111) NOT NULL,
  `published_by` int(111) NOT NULL,
  `summary` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_notify` int(11) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_assets`
--

CREATE TABLE `xin_assets` (
  `assets_id` int(111) NOT NULL,
  `assets_category_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `company_asset_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `purchase_date` varchar(255) NOT NULL,
  `invoice_number` varchar(255) NOT NULL,
  `manufacturer` varchar(255) NOT NULL,
  `serial_number` varchar(255) NOT NULL,
  `warranty_end_date` varchar(255) NOT NULL,
  `asset_note` text NOT NULL,
  `asset_image` varchar(255) NOT NULL,
  `is_working` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_assets_categories`
--

CREATE TABLE `xin_assets_categories` (
  `assets_category_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_assets_categories`
--

INSERT INTO `xin_assets_categories` (`assets_category_id`, `company_id`, `category_name`, `created_at`) VALUES
(1, 1, 'Laptop', '05-04-2018 03:03:31');

-- --------------------------------------------------------

--
-- Table structure for table `xin_attendance_time`
--

CREATE TABLE `xin_attendance_time` (
  `time_attendance_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `attendance_date` varchar(255) NOT NULL,
  `clock_in` varchar(255) NOT NULL,
  `clock_in_ip_address` varchar(255) NOT NULL,
  `clock_out` varchar(255) NOT NULL,
  `clock_out_ip_address` varchar(255) NOT NULL,
  `clock_in_out` varchar(255) NOT NULL,
  `clock_in_latitude` varchar(150) NOT NULL,
  `clock_in_longitude` varchar(150) NOT NULL,
  `clock_out_latitude` varchar(150) NOT NULL,
  `clock_out_longitude` varchar(150) NOT NULL,
  `time_late` varchar(255) NOT NULL,
  `early_leaving` varchar(255) NOT NULL,
  `overtime` varchar(255) NOT NULL,
  `total_work` varchar(255) NOT NULL,
  `total_rest` varchar(255) NOT NULL,
  `attendance_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_attendance_time`
--

INSERT INTO `xin_attendance_time` (`time_attendance_id`, `employee_id`, `attendance_date`, `clock_in`, `clock_in_ip_address`, `clock_out`, `clock_out_ip_address`, `clock_in_out`, `clock_in_latitude`, `clock_in_longitude`, `clock_out_latitude`, `clock_out_longitude`, `time_late`, `early_leaving`, `overtime`, `total_work`, `total_rest`, `attendance_status`) VALUES
(1, 5, '2019-04-17', '2019-04-17 10:36:38', '::1', '2019-04-17 10:37:36', '::1', '0', '31.450726399999997', '74.2940672', '31.450726399999997', '74.2940672', '2019-04-17 10:36:38', '2019-04-17 10:37:36', '2019-04-17 10:37:36', '0:0', '', 'Present');

-- --------------------------------------------------------

--
-- Table structure for table `xin_attendance_time_request`
--

CREATE TABLE `xin_attendance_time_request` (
  `time_request_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `request_date_request` varchar(255) NOT NULL,
  `request_clock_in` varchar(200) NOT NULL,
  `request_clock_out` varchar(200) NOT NULL,
  `total_hours` varchar(255) NOT NULL,
  `request_reason` text NOT NULL,
  `is_approved` tinyint(1) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `xin_awards`
--

CREATE TABLE `xin_awards` (
  `award_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(200) NOT NULL,
  `award_type_id` int(200) NOT NULL,
  `gift_item` varchar(200) NOT NULL,
  `cash_price` varchar(200) NOT NULL,
  `award_photo` varchar(255) NOT NULL,
  `award_month_year` varchar(200) NOT NULL,
  `award_information` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_awards`
--

INSERT INTO `xin_awards` (`award_id`, `company_id`, `employee_id`, `award_type_id`, `gift_item`, `cash_price`, `award_photo`, `award_month_year`, `award_information`, `description`, `created_at`) VALUES
(1, 1, 5, 1, 'Car', '200000', 'award_1569174762.jpg', '2019-09', 'to be added', '', '2019-09-22');

-- --------------------------------------------------------

--
-- Table structure for table `xin_award_type`
--

CREATE TABLE `xin_award_type` (
  `award_type_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `award_type` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_award_type`
--

INSERT INTO `xin_award_type` (`award_type_id`, `company_id`, `award_type`, `created_at`) VALUES
(1, 1, 'Performer of the Year', '22-03-2018 01:33:57');

-- --------------------------------------------------------

--
-- Table structure for table `xin_bands`
--

CREATE TABLE `xin_bands` (
  `band_id` int(11) NOT NULL,
  `band_name` varchar(255) NOT NULL,
  `created_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_bands`
--

INSERT INTO `xin_bands` (`band_id`, `band_name`, `created_on`) VALUES
(7, 'Band A', '2019-11-01 11:51:02'),
(8, 'Band B', '2019-11-01 11:51:20'),
(9, 'Band C', '2019-11-01 11:51:37');

-- --------------------------------------------------------

--
-- Table structure for table `xin_change_hospital_request`
--

CREATE TABLE `xin_change_hospital_request` (
  `change_hospital_request_id` int(11) NOT NULL,
  `hospital_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `created_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_change_hospital_request`
--

INSERT INTO `xin_change_hospital_request` (`change_hospital_request_id`, `hospital_id`, `client_id`, `status`, `created_on`) VALUES
(1, 4, 5, 'pending', '2019-11-11 03:46:38'),
(2, 4, 5, 'rejected', '2019-11-11 03:48:02'),
(3, 4, 5, 'rejected', '2019-11-11 03:49:22'),
(4, 6, 5, 'approved', '2019-11-11 04:05:26'),
(5, 3, 2, 'approved', '2019-11-11 05:34:58'),
(6, 6, 2, 'pending', '2019-11-12 06:53:43'),
(7, 4, 2, 'approved', '2019-11-12 06:53:48'),
(10, 6, 2, 'pending', '2019-11-20 10:50:50'),
(11, 3, 2, 'approved', '2019-11-20 07:59:47'),
(12, 3, 10, 'pending', '2019-11-21 11:05:15'),
(13, 6, 2, 'approved', '2019-11-21 03:22:15'),
(14, 6, 10, 'pending', '2019-11-21 09:42:49'),
(15, 6, 10, 'pending', '2019-11-21 09:42:54'),
(16, 3, 10, 'pending', '2019-12-02 10:24:35'),
(17, 3, 10, 'pending', '2019-12-02 10:24:40'),
(18, 3, 10, 'pending', '2019-12-02 11:38:44');

-- --------------------------------------------------------

--
-- Table structure for table `xin_chat_messages`
--

CREATE TABLE `xin_chat_messages` (
  `message_id` int(11) UNSIGNED NOT NULL,
  `from_id` varchar(40) NOT NULL DEFAULT '',
  `to_id` varchar(50) NOT NULL DEFAULT '',
  `message_frm` varchar(255) NOT NULL,
  `is_read` int(11) NOT NULL DEFAULT '0',
  `message_content` longtext NOT NULL,
  `message_date` varchar(255) DEFAULT NULL,
  `recd` tinyint(1) NOT NULL DEFAULT '0',
  `message_type` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_chat_messages`
--

INSERT INTO `xin_chat_messages` (`message_id`, `from_id`, `to_id`, `message_frm`, `is_read`, `message_content`, `message_date`, `recd`, `message_type`) VALUES
(1, '1', '5', '5', 0, 'hi', '2019-09-22 18:26:34', 0, ''),
(2, '1', '5', '5', 0, 'hi', '2019-09-22 18:26:35', 0, ''),
(3, '1', '6', '6', 0, 'Testing', '2019-11-02 00:29:21', 0, ''),
(4, '1', '5', '5', 0, 'How are you doing', '2019-11-05 11:45:48', 0, ''),
(5, '1', '5', '5', 0, 'hi', '2019-11-11 13:24:52', 0, ''),
(6, '1', '5', '5', 0, 'hi', '2019-11-11 13:24:53', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `xin_clients`
--

CREATE TABLE `xin_clients` (
  `client_id` int(111) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `client_username` varchar(255) NOT NULL,
  `client_password` varchar(255) NOT NULL,
  `client_profile` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `website_url` varchar(255) NOT NULL,
  `address_1` mediumtext NOT NULL,
  `address_2` mediumtext NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `country` int(111) NOT NULL,
  `is_active` int(11) NOT NULL,
  `subscription_ids` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `other_name` varchar(255) DEFAULT NULL,
  `last_logout_date` varchar(255) NOT NULL,
  `last_login_date` varchar(255) NOT NULL,
  `last_login_ip` varchar(255) NOT NULL,
  `is_logged_in` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `ind_family` varchar(255) DEFAULT NULL,
  `hospital_id` int(11) DEFAULT NULL,
  `diseases` text,
  `disease_comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_clients`
--

INSERT INTO `xin_clients` (`client_id`, `name`, `email`, `client_username`, `client_password`, `client_profile`, `contact_number`, `company_name`, `gender`, `website_url`, `address_1`, `address_2`, `city`, `state`, `zipcode`, `country`, `is_active`, `subscription_ids`, `dob`, `sex`, `marital_status`, `last_name`, `other_name`, `last_logout_date`, `last_login_date`, `last_login_ip`, `is_logged_in`, `created_at`, `ind_family`, `hospital_id`, `diseases`, `disease_comment`) VALUES
(1, 'Muhammad Zeeshan', 'John1122@gmail.com', '', '$2y$12$wGATpsG6S/IAIwobUZMeDeYyxpirjXVaVLo79ta2PLlRJjBNIsfFa', '', '32434343', '4', '', '', 'Pakistan', '', '', '18', '', 0, 0, '14', '2019-11-06', 'male', 'single', 'khan', '', '04-12-2019 23:07:43', '04-12-2019 17:05:30', '39.52.127.48', 0, '2019-03-20 22:05:02', 'family', 3, '', ''),
(2, 'Vivian Bruce', 'kennedyjob@yahoo.com', '', '$2y$12$INWfTz/I6yRNyfBc/kbBvOYi1DniZ20vSTm6sh05lWDlpPcqW/y.G', 'client_photo_1572692867.png', '7031806085', '4', '', '', '34 gana street maitama Abuja', '34', 'Abuja', 'Abuja', '900217', 161, 1, '13', NULL, NULL, NULL, NULL, NULL, '05-12-2019 18:24:13', '05-12-2019 12:23:55', '129.56.105.108', 0, '2019-11-02 12:07:47', 'family', 6, NULL, NULL),
(4, 'Muhammad', 'John1122@gmail.com', '', '$2y$12$ZAN0VkuRoE1poCi46xBENOhiWsMyGwdM93u48v0s3OX0Zx5q6a9Ne', 'client_photo_1572692867.png', '32434343', '5', '', '', 'sss', '', '', '18', '', 0, 1, '11', '2019-11-14', 'male', 'single', 'Zeeshan', '', '', '12-11-2019 04:11:26', '39.52.126.28', 1, '2019-11-08 15:29:07', 'family', 6, '', ''),
(5, 'zain', 'John1122@gmail.com', '', '$2y$12$FIYD1t6fXiNwKfIezoTCRutpu2gCR8g4Vx.QX6Thhwldl6qhSiYJa', 'client_photo_1572692867.png', '32434343', '5', '', '', 'RT Nagar, Bengaluru, Karnataka, India', '', '', '18', '', 0, 1, '14', '2019-11-11', 'male', 'married', 'Zeeshan', '', '', '11-11-2019 14:55:26', '39.52.126.28', 1, '2019-11-08 16:12:04', 'family', 6, '', ''),
(6, 'Zain', 'John1122@gmail.com', '', '$2y$12$T53erBy5ZAZBwt4K1sPF9O5mBuVqjJe.5YVrr03XWqhVPs6g5cby2', 'client_photo_1572692867.png', '32434343', '5', '', '', 'RT Nagar, Bengaluru, Karnataka, India', '', '', '12', '', 0, 1, '13', '2019-11-10', 'male', 'married', 'Zeeshan', '', '', '10-11-2019 14:08:08', '39.34.196.174', 1, '2019-11-08 17:42:49', 'family', 6, 'Sickle Cell Disease,Sickle Cell Disease', ''),
(7, 'Yarima ALbashir', 'test@test.com.ng', '', '$2y$12$xPrLm4NNITfY2/fYrscXLOANbmeL0iwXg2rJC4Vn64Lhv9lU5VmDi', 'client_photo_1573476926.png', '7031806085', '6', '', '', '', '', '', '', '', 0, 1, '11', NULL, NULL, NULL, NULL, NULL, '11-11-2019 20:50:47', '21-11-2019 22:37:48', '105.112.122.187', 1, '2019-11-11 13:55:26', 'family', NULL, NULL, NULL),
(8, 'Juliet Kennedy', 'kennedyjob@myweb.ng', '', '$2y$12$z7hgcmzJmHHwmP/D9HMpYO20YozEIoyc12m6jw.Pc3CwdnCN3Hlwe', 'client_photo_1572692867.png', '7031806085', '5', '', '', '', '', '', '', '', 0, 1, '14', NULL, NULL, NULL, NULL, NULL, '05-12-2019 05:49:03', '04-12-2019 22:15:48', '197.210.45.239', 0, '2019-11-12 14:05:54', 'individual', NULL, NULL, NULL),
(9, 'Musa', 'link2musa@gmail.com', '', '$2y$12$o2.ySMmzTbgXGaXofwM8uu46Vg5zCZVI.pXHC8Iqxi961q0Eyvj9C', '', '2323232323', '5', '', '', '', '', '', '', '', 0, 1, '14', NULL, NULL, NULL, NULL, NULL, '', '18-11-2019 07:15:04', '39.34.225.122', 1, '2019-11-16 21:29:48', 'family', NULL, NULL, NULL),
(10, 'Test Client', 'client@liontech.com.ng', '', '$2y$12$D428s8S5wgE7ihFnSL0tDe/12lD5Kp2nT4zoSvUM2JEh2dutZmvD2', 'client_photo_1574117092.png', '445865456', '5', '', '', '', '', '', '', '', 0, 1, '15', NULL, NULL, NULL, NULL, NULL, '03-12-2019 16:31:42', '05-12-2019 12:37:35', '182.186.81.135', 1, '2019-11-18 23:44:51', 'family', NULL, '', ''),
(11, 'India Man', 'india@indiatech.com', '', '$2y$12$rCaseP2DNf..tQIElXtLeej2ceQ/mSXJ31Kd2E/.IYLE5UsHYlL7S', '', '7031806085', '7', '', '', '', '', '', '', '', 0, 1, '14', NULL, NULL, NULL, NULL, NULL, '', '21-11-2019 12:28:04', '105.112.122.187', 1, '2019-11-21 12:27:46', 'family', NULL, NULL, NULL),
(12, 'Zeeshan', 'zeeshan722awan@gmail.com', '', '$2y$12$HOjRyTalJ7YC.fGvtV7gD.iyk0Fl.4UaZd.OOOPSo8LuaclsWD69K', '', '32434343', '5', '', '', '', '', '', '', '', 0, 1, '14', NULL, NULL, NULL, NULL, NULL, '', '03-12-2019 17:29:50', '37.111.139.229', 1, '2019-12-03 17:29:43', 'family', NULL, NULL, NULL),
(23, 'Joy Job', 'joy@liontech.com.ng', '', '$2y$12$p4/XHEudoQuOG9bpW9ETRu0PKHnTAHjBctDTYteRIbcNub5nDp9Iy', '', '1223', '5', '', '', '', '', '', '', '', 0, 1, '14', NULL, NULL, NULL, NULL, NULL, '', '05-12-2019 00:25:21', '197.210.45.239', 1, '2019-12-05 00:22:35', 'family', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `xin_clients_diagnose`
--

CREATE TABLE `xin_clients_diagnose` (
  `diagnose_id` int(11) NOT NULL,
  `diagnose_hospital_id` int(11) NOT NULL,
  `diagnose_client_id` int(11) NOT NULL,
  `diagnose_date` date NOT NULL,
  `diagnose_diagnose` text NOT NULL,
  `diagnose_procedure` text NOT NULL,
  `diagnose_investigation` text NOT NULL,
  `diagnose_medical` text NOT NULL,
  `diagnose_total_sum` int(11) NOT NULL,
  `diagnose_generated_code` text,
  `diagnose_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_clients_diagnose`
--

INSERT INTO `xin_clients_diagnose` (`diagnose_id`, `diagnose_hospital_id`, `diagnose_client_id`, `diagnose_date`, `diagnose_diagnose`, `diagnose_procedure`, `diagnose_investigation`, `diagnose_medical`, `diagnose_total_sum`, `diagnose_generated_code`, `diagnose_status`) VALUES
(6, 10, 5, '2019-12-18', 'aaa', 'bbb', 'cc', 'dd', 2569, '79448838', 3);

-- --------------------------------------------------------

--
-- Table structure for table `xin_clients_diagnose_drugs`
--

CREATE TABLE `xin_clients_diagnose_drugs` (
  `diagnose_autod_id` int(11) NOT NULL,
  `diagnose_maind_id` int(11) NOT NULL,
  `diagnose_drugs_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_clients_diagnose_drugs`
--

INSERT INTO `xin_clients_diagnose_drugs` (`diagnose_autod_id`, `diagnose_maind_id`, `diagnose_drugs_id`) VALUES
(5, 6, 14647),
(6, 6, 14649),
(7, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `xin_clients_diagnose_services`
--

CREATE TABLE `xin_clients_diagnose_services` (
  `diagnose_autos_id` int(11) NOT NULL,
  `diagnose_mains_id` int(11) NOT NULL,
  `diagnose_services_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_clients_diagnose_services`
--

INSERT INTO `xin_clients_diagnose_services` (`diagnose_autos_id`, `diagnose_mains_id`, `diagnose_services_id`) VALUES
(6, 6, 17),
(7, 6, 16),
(8, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `xin_clients_family`
--

CREATE TABLE `xin_clients_family` (
  `clients_family_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `other_name` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `address_1` text,
  `location` int(11) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `hospital_id` int(11) DEFAULT NULL,
  `client_profile` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_clients_family`
--

INSERT INTO `xin_clients_family` (`clients_family_id`, `name`, `last_name`, `other_name`, `dob`, `contact_number`, `address_1`, `location`, `sex`, `relation`, `client_id`, `hospital_id`, `client_profile`, `created_on`) VALUES
(1, 'Muhammad Zeeshan', 'Zeeshan', 'tt', '2019-11-09', '32434343', 'RT Nagar, Bengaluru, Karnataka, India', 17, 'male', 'daughter', 5, 3, 'client_photo_1573308801.jpg', '0000-00-00 00:00:00'),
(2, 'Muhammad Zeeshan', 'Zeeshan', 'tt', '2019-11-02', '32434343', 'RT Nagar, Bengaluru, Karnataka, India', 15, 'male', 'son', 6, 3, 'client_photo_1573391879.jpg', '2019-11-10 02:17:58'),
(3, 'Kennedy Job', 'Job', 'Akor', '2012-06-12', '7031806085', '34 gana street maitama Abuja', 15, 'male', 'son', 2, 3, 'client_photo_1573392263.jpg', '2019-11-10 02:24:22'),
(4, 'Muhammad Zeeshan', 'Zeeshan', '', '2019-11-01', '32434343', 'RT Nagar, Bengaluru, Karnataka, India', 15, 'male', 'son', 6, 3, 'client_photo_1573392474.jpg', '2019-11-10 02:27:54'),
(5, 'Juliet', 'Kennedy', 'Sochima', '2014-06-17', '7031806085', '34 gana street maitama Abuja', 15, 'female', 'wife', 2, 3, 'client_photo_1573395621.jpg', '2019-11-10 03:20:20'),
(6, 'Muhammad Zeeshan', 'Zeeshan', '', '1990-11-10', '32434343', 'RT Nagar, Bengaluru, Karnataka, India', 18, 'male', 'wife', 6, 3, 'client_photo_1573396002.jpg', '2019-11-10 03:26:41'),
(7, 'Mariam', 'Yarima', '', '1989-05-15', '7031806085', '34 gana street maitama Abuja', 15, 'female', 'wife', 7, 6, 'client_photo_1573478145.jpg', '2019-11-11 02:15:45'),
(8, 'Muhammad Zeeshan', 'Zeeshan', '', '2019-11-01', '32434343', 'Lahore', 17, 'male', 'son', 4, 4, 'client_photo_1573530394.jpg', '2019-11-12 04:46:34'),
(9, 'Zain', 'Ali', '', '2019-11-13', '32434343', NULL, NULL, 'male', 'son', 4, 6, 'client_photo_1573530769.jpg', '2019-11-12 04:52:48'),
(10, 'sss', 'Zeeshan', '', '2019-11-13', '32434343', NULL, NULL, 'male', 'son', 4, 4, 'client_photo_1573530874.jpg', '2019-11-12 04:54:34'),
(11, 'zain', 'khan', '', '2019-11-01', '32434343', NULL, NULL, 'male', 'son', 1, 6, 'client_photo_1573569953.jpg', '2019-11-12 03:45:52'),
(12, 'My', 'Wife', '', '1989-01-31', '123456789', NULL, NULL, 'female', 'wife', 11, 6, 'client_photo_1574336019.png', '2019-11-21 12:33:38'),
(13, 'Aisha', 'Yarima', 'Mariam', '2015-06-16', '445865456', NULL, NULL, 'female', 'daughter', 7, 6, 'client_photo_1574373655.png', '2019-11-21 11:00:55'),
(14, 'Mohammad', 'Yarima', 'Usman', '2004-06-16', '445865456', NULL, NULL, 'male', 'son', 7, 4, 'client_photo_1574373813.jpg', '2019-11-21 11:03:33'),
(15, 'Mohammad', 'Yarima', 'Usman', '2004-06-16', '445865456', NULL, NULL, 'male', 'son', 7, 4, 'client_photo_1574373817.jpg', '2019-11-21 11:03:37'),
(16, 'Example', 'Example', 'Example', '2018-11-30', '000000000', NULL, NULL, 'male', 'wife', 10, 3, 'client_photo_1574518906.jpg', '2019-11-23 03:21:45');

-- --------------------------------------------------------

--
-- Table structure for table `xin_clients_family_req`
--

CREATE TABLE `xin_clients_family_req` (
  `clients_family_id` int(11) NOT NULL,
  `requester_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `other_name` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `address_1` text,
  `location` int(11) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `hospital_id` int(11) DEFAULT NULL,
  `client_profile` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'approve'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_clients_family_req`
--

INSERT INTO `xin_clients_family_req` (`clients_family_id`, `requester_id`, `name`, `last_name`, `other_name`, `dob`, `contact_number`, `address_1`, `location`, `sex`, `relation`, `client_id`, `hospital_id`, `client_profile`, `created_on`, `status`) VALUES
(16, 8, 'Muhammad Zeeshan', 'Zeeshan', '', '2019-11-01', '32434343', 'Lahore', 17, 'male', 'son', 4, 4, NULL, '2019-11-12 04:47:04', 'approved'),
(17, 9, 'Zain', 'Ali', '', '2019-11-13', '32434343', NULL, NULL, 'male', 'son', 4, 6, NULL, '2019-11-12 05:02:01', 'approve'),
(18, 9, 'Zain', 'Ali', '', '2019-11-13', '32434343', NULL, NULL, 'male', 'son', 4, 6, NULL, '2019-11-12 05:02:02', 'approved'),
(19, 11, 'zain', 'khan', '', '2019-11-01', '32434343', NULL, NULL, 'male', 'son', 1, 6, NULL, '2019-11-12 04:20:09', 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `xin_companies`
--

CREATE TABLE `xin_companies` (
  `company_id` int(111) NOT NULL,
  `type_id` int(111) NOT NULL,
  `name` varchar(255) NOT NULL,
  `trading_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `registration_no` varchar(255) NOT NULL,
  `government_tax` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `website_url` varchar(255) NOT NULL,
  `address_1` mediumtext NOT NULL,
  `address_2` mediumtext NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `country` int(111) NOT NULL,
  `is_active` int(11) NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_companies`
--

INSERT INTO `xin_companies` (`company_id`, `type_id`, `name`, `trading_name`, `username`, `password`, `registration_no`, `government_tax`, `email`, `logo`, `contact_number`, `website_url`, `address_1`, `address_2`, `city`, `state`, `zipcode`, `country`, `is_active`, `added_by`, `created_at`) VALUES
(1, 5, 'LionTech', 'LionTech Group', 'test123', '', 'RC-1360902', '', 'info@liontech.com.ng', 'logo_1568650365.png', '07031806085', 'liontech.com.ng', 'Test', 'Test2', 'Abuja', 'FCT', '11461', 161, 0, 1, '22-05-2018');

-- --------------------------------------------------------

--
-- Table structure for table `xin_company_documents`
--

CREATE TABLE `xin_company_documents` (
  `document_id` int(11) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `license_name` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `license_number` varchar(255) NOT NULL,
  `license_notification` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `document` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `xin_company_info`
--

CREATE TABLE `xin_company_info` (
  `company_info_id` int(111) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `logo_second` varchar(255) NOT NULL,
  `sign_in_logo` varchar(255) NOT NULL,
  `favicon` varchar(255) NOT NULL,
  `website_url` mediumtext NOT NULL,
  `starting_year` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_contact` varchar(255) NOT NULL,
  `contact_person` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address_1` mediumtext NOT NULL,
  `address_2` mediumtext NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `country` int(111) NOT NULL,
  `updated_at` varchar(255) NOT NULL,
  `location` text NOT NULL,
  `rc_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_company_info`
--

INSERT INTO `xin_company_info` (`company_info_id`, `logo`, `logo_second`, `sign_in_logo`, `favicon`, `website_url`, `starting_year`, `company_name`, `company_email`, `company_contact`, `contact_person`, `email`, `phone`, `address_1`, `address_2`, `city`, `state`, `zipcode`, `country`, `updated_at`, `location`, `rc_number`) VALUES
(1, 'logo_1573568921.png', 'logo2_1520609223.png', 'signin_logo_1573568882.png', 'favicon_1573568874.png', '', '', 'LionTech', '', '', 'Kennedy Job', 'info@liontech.com.ng', '123456789', 'Address Line 1', 'Address Line 2', 'Abuja', 'FCT', '11461', 161, '2017-05-20 12:05:53', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `xin_company_policy`
--

CREATE TABLE `xin_company_policy` (
  `policy_id` int(111) NOT NULL,
  `company_id` int(111) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_company_policy`
--

INSERT INTO `xin_company_policy` (`policy_id`, `company_id`, `title`, `description`, `added_by`, `created_at`) VALUES
(1, 1, 'Smoke-Free Work', '&lt;p&gt;Smoke-Free Work Environment Policy Close&lt;/p&gt;', 1, '28-02-2018');

-- --------------------------------------------------------

--
-- Table structure for table `xin_company_type`
--

CREATE TABLE `xin_company_type` (
  `type_id` int(111) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_company_type`
--

INSERT INTO `xin_company_type` (`type_id`, `name`, `created_at`) VALUES
(1, 'Corporation', ''),
(2, 'Exempt Organization', ''),
(3, 'Partnership', ''),
(4, 'Private Foundation', ''),
(5, 'Limited Liability Company', '');

-- --------------------------------------------------------

--
-- Table structure for table `xin_contract_type`
--

CREATE TABLE `xin_contract_type` (
  `contract_type_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_contract_type`
--

INSERT INTO `xin_contract_type` (`contract_type_id`, `company_id`, `name`, `created_at`) VALUES
(1, 1, 'Permanent', '05-04-2018 06:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `xin_countries`
--

CREATE TABLE `xin_countries` (
  `country_id` int(11) NOT NULL,
  `country_code` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL,
  `country_flag` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_countries`
--

INSERT INTO `xin_countries` (`country_id`, `country_code`, `country_name`, `country_flag`) VALUES
(1, '+93', 'Afghanistan', 'flag_1500831780.gif'),
(2, '+355', 'Albania', 'flag_1500831815.gif'),
(3, 'DZ', 'Algeria', ''),
(4, 'DS', 'American Samoa', ''),
(5, 'AD', 'Andorra', ''),
(6, 'AO', 'Angola', ''),
(7, 'AI', 'Anguilla', ''),
(8, 'AQ', 'Antarctica', ''),
(9, 'AG', 'Antigua and Barbuda', ''),
(10, 'AR', 'Argentina', ''),
(11, 'AM', 'Armenia', ''),
(12, 'AW', 'Aruba', ''),
(13, 'AU', 'Australia', ''),
(14, 'AT', 'Austria', ''),
(15, 'AZ', 'Azerbaijan', ''),
(16, 'BS', 'Bahamas', ''),
(17, 'BH', 'Bahrain', ''),
(18, 'BD', 'Bangladesh', ''),
(19, 'BB', 'Barbados', ''),
(20, 'BY', 'Belarus', ''),
(21, 'BE', 'Belgium', ''),
(22, 'BZ', 'Belize', ''),
(23, 'BJ', 'Benin', ''),
(24, 'BM', 'Bermuda', ''),
(25, 'BT', 'Bhutan', ''),
(26, 'BO', 'Bolivia', ''),
(27, 'BA', 'Bosnia and Herzegovina', ''),
(28, 'BW', 'Botswana', ''),
(29, 'BV', 'Bouvet Island', ''),
(30, 'BR', 'Brazil', ''),
(31, 'IO', 'British Indian Ocean Territory', ''),
(32, 'BN', 'Brunei Darussalam', ''),
(33, 'BG', 'Bulgaria', ''),
(34, 'BF', 'Burkina Faso', ''),
(35, 'BI', 'Burundi', ''),
(36, 'KH', 'Cambodia', ''),
(37, 'CM', 'Cameroon', ''),
(38, 'CA', 'Canada', ''),
(39, 'CV', 'Cape Verde', ''),
(40, 'KY', 'Cayman Islands', ''),
(41, 'CF', 'Central African Republic', ''),
(42, 'TD', 'Chad', ''),
(43, 'CL', 'Chile', ''),
(44, 'CN', 'China', ''),
(45, 'CX', 'Christmas Island', ''),
(46, 'CC', 'Cocos (Keeling) Islands', ''),
(47, 'CO', 'Colombia', ''),
(48, 'KM', 'Comoros', ''),
(49, 'CG', 'Congo', ''),
(50, 'CK', 'Cook Islands', ''),
(51, 'CR', 'Costa Rica', ''),
(52, 'HR', 'Croatia (Hrvatska)', ''),
(53, 'CU', 'Cuba', ''),
(54, 'CY', 'Cyprus', ''),
(55, 'CZ', 'Czech Republic', ''),
(56, 'DK', 'Denmark', ''),
(57, 'DJ', 'Djibouti', ''),
(58, 'DM', 'Dominica', ''),
(59, 'DO', 'Dominican Republic', ''),
(60, 'TP', 'East Timor', ''),
(61, 'EC', 'Ecuador', ''),
(62, 'EG', 'Egypt', ''),
(63, 'SV', 'El Salvador', ''),
(64, 'GQ', 'Equatorial Guinea', ''),
(65, 'ER', 'Eritrea', ''),
(66, 'EE', 'Estonia', ''),
(67, 'ET', 'Ethiopia', ''),
(68, 'FK', 'Falkland Islands (Malvinas)', ''),
(69, 'FO', 'Faroe Islands', ''),
(70, 'FJ', 'Fiji', ''),
(71, 'FI', 'Finland', ''),
(72, 'FR', 'France', ''),
(73, 'FX', 'France, Metropolitan', ''),
(74, 'GF', 'French Guiana', ''),
(75, 'PF', 'French Polynesia', ''),
(76, 'TF', 'French Southern Territories', ''),
(77, 'GA', 'Gabon', ''),
(78, 'GM', 'Gambia', ''),
(79, 'GE', 'Georgia', ''),
(80, 'DE', 'Germany', ''),
(81, 'GH', 'Ghana', ''),
(82, 'GI', 'Gibraltar', ''),
(83, 'GK', 'Guernsey', ''),
(84, 'GR', 'Greece', ''),
(85, 'GL', 'Greenland', ''),
(86, 'GD', 'Grenada', ''),
(87, 'GP', 'Guadeloupe', ''),
(88, 'GU', 'Guam', ''),
(89, 'GT', 'Guatemala', ''),
(90, 'GN', 'Guinea', ''),
(91, 'GW', 'Guinea-Bissau', ''),
(92, 'GY', 'Guyana', ''),
(93, 'HT', 'Haiti', ''),
(94, 'HM', 'Heard and Mc Donald Islands', ''),
(95, 'HN', 'Honduras', ''),
(96, 'HK', 'Hong Kong', ''),
(97, 'HU', 'Hungary', ''),
(98, 'IS', 'Iceland', ''),
(99, 'IN', 'India', ''),
(100, 'IM', 'Isle of Man', ''),
(101, 'ID', 'Indonesia', ''),
(102, 'IR', 'Iran (Islamic Republic of)', ''),
(103, 'IQ', 'Iraq', ''),
(104, 'IE', 'Ireland', ''),
(105, 'IL', 'Israel', ''),
(106, 'IT', 'Italy', ''),
(107, 'CI', 'Ivory Coast', ''),
(108, 'JE', 'Jersey', ''),
(109, 'JM', 'Jamaica', ''),
(110, 'JP', 'Japan', ''),
(111, 'JO', 'Jordan', ''),
(112, 'KZ', 'Kazakhstan', ''),
(113, 'KE', 'Kenya', ''),
(114, 'KI', 'Kiribati', ''),
(115, 'KP', 'Korea, Democratic People\'s Republic of', ''),
(116, 'KR', 'Korea, Republic of', ''),
(117, 'XK', 'Kosovo', ''),
(118, 'KW', 'Kuwait', ''),
(119, 'KG', 'Kyrgyzstan', ''),
(120, 'LA', 'Lao People\'s Democratic Republic', ''),
(121, 'LV', 'Latvia', ''),
(122, 'LB', 'Lebanon', ''),
(123, 'LS', 'Lesotho', ''),
(124, 'LR', 'Liberia', ''),
(125, 'LY', 'Libyan Arab Jamahiriya', ''),
(126, 'LI', 'Liechtenstein', ''),
(127, 'LT', 'Lithuania', ''),
(128, 'LU', 'Luxembourg', ''),
(129, 'MO', 'Macau', ''),
(130, 'MK', 'Macedonia', ''),
(131, 'MG', 'Madagascar', ''),
(132, 'MW', 'Malawi', ''),
(133, 'MY', 'Malaysia', ''),
(134, 'MV', 'Maldives', ''),
(135, 'ML', 'Mali', ''),
(136, 'MT', 'Malta', ''),
(137, 'MH', 'Marshall Islands', ''),
(138, 'MQ', 'Martinique', ''),
(139, 'MR', 'Mauritania', ''),
(140, 'MU', 'Mauritius', ''),
(141, 'TY', 'Mayotte', ''),
(142, 'MX', 'Mexico', ''),
(143, 'FM', 'Micronesia, Federated States of', ''),
(144, 'MD', 'Moldova, Republic of', ''),
(145, 'MC', 'Monaco', ''),
(146, 'MN', 'Mongolia', ''),
(147, 'ME', 'Montenegro', ''),
(148, 'MS', 'Montserrat', ''),
(149, 'MA', 'Morocco', ''),
(150, 'MZ', 'Mozambique', ''),
(151, 'MM', 'Myanmar', ''),
(152, 'NA', 'Namibia', ''),
(153, 'NR', 'Nauru', ''),
(154, 'NP', 'Nepal', ''),
(155, 'NL', 'Netherlands', ''),
(156, 'AN', 'Netherlands Antilles', ''),
(157, 'NC', 'New Caledonia', ''),
(158, 'NZ', 'New Zealand', ''),
(159, 'NI', 'Nicaragua', ''),
(160, 'NE', 'Niger', ''),
(161, 'NG', 'Nigeria', ''),
(162, 'NU', 'Niue', ''),
(163, 'NF', 'Norfolk Island', ''),
(164, 'MP', 'Northern Mariana Islands', ''),
(165, 'NO', 'Norway', ''),
(166, 'OM', 'Oman', ''),
(167, 'PK', 'Pakistan', ''),
(168, 'PW', 'Palau', ''),
(169, 'PS', 'Palestine', ''),
(170, 'PA', 'Panama', ''),
(171, 'PG', 'Papua New Guinea', ''),
(172, 'PY', 'Paraguay', ''),
(173, 'PE', 'Peru', ''),
(174, 'PH', 'Philippines', ''),
(175, 'PN', 'Pitcairn', ''),
(176, 'PL', 'Poland', ''),
(177, 'PT', 'Portugal', ''),
(178, 'PR', 'Puerto Rico', ''),
(179, 'QA', 'Qatar', ''),
(180, 'RE', 'Reunion', ''),
(181, 'RO', 'Romania', ''),
(182, 'RU', 'Russian Federation', ''),
(183, 'RW', 'Rwanda', ''),
(184, 'KN', 'Saint Kitts and Nevis', ''),
(185, 'LC', 'Saint Lucia', ''),
(186, 'VC', 'Saint Vincent and the Grenadines', ''),
(187, 'WS', 'Samoa', ''),
(188, 'SM', 'San Marino', ''),
(189, 'ST', 'Sao Tome and Principe', ''),
(190, 'SA', 'Saudi Arabia', ''),
(191, 'SN', 'Senegal', ''),
(192, 'RS', 'Serbia', ''),
(193, 'SC', 'Seychelles', ''),
(194, 'SL', 'Sierra Leone', ''),
(195, 'SG', 'Singapore', ''),
(196, 'SK', 'Slovakia', ''),
(197, 'SI', 'Slovenia', ''),
(198, 'SB', 'Solomon Islands', ''),
(199, 'SO', 'Somalia', ''),
(200, 'ZA', 'South Africa', ''),
(201, 'GS', 'South Georgia South Sandwich Islands', ''),
(202, 'ES', 'Spain', ''),
(203, 'LK', 'Sri Lanka', ''),
(204, 'SH', 'St. Helena', ''),
(205, 'PM', 'St. Pierre and Miquelon', ''),
(206, 'SD', 'Sudan', ''),
(207, 'SR', 'Suriname', ''),
(208, 'SJ', 'Svalbard and Jan Mayen Islands', ''),
(209, 'SZ', 'Swaziland', ''),
(210, 'SE', 'Sweden', ''),
(211, 'CH', 'Switzerland', ''),
(212, 'SY', 'Syrian Arab Republic', ''),
(213, 'TW', 'Taiwan', ''),
(214, 'TJ', 'Tajikistan', ''),
(215, 'TZ', 'Tanzania, United Republic of', ''),
(216, 'TH', 'Thailand', ''),
(217, 'TG', 'Togo', ''),
(218, 'TK', 'Tokelau', ''),
(219, 'TO', 'Tonga', ''),
(220, 'TT', 'Trinidad and Tobago', ''),
(221, 'TN', 'Tunisia', ''),
(222, 'TR', 'Turkey', ''),
(223, 'TM', 'Turkmenistan', ''),
(224, 'TC', 'Turks and Caicos Islands', ''),
(225, 'TV', 'Tuvalu', ''),
(226, 'UG', 'Uganda', ''),
(227, 'UA', 'Ukraine', ''),
(228, 'AE', 'United Arab Emirates', ''),
(229, 'GB', 'United Kingdom', ''),
(230, 'US', 'United States', ''),
(231, 'UM', 'United States minor outlying islands', ''),
(232, 'UY', 'Uruguay', ''),
(233, 'UZ', 'Uzbekistan', ''),
(234, 'VU', 'Vanuatu', ''),
(235, 'VA', 'Vatican City State', ''),
(236, 'VE', 'Venezuela', ''),
(237, 'VN', 'Vietnam', ''),
(238, 'VG', 'Virgin Islands (British)', ''),
(239, 'VI', 'Virgin Islands (U.S.)', ''),
(240, 'WF', 'Wallis and Futuna Islands', ''),
(241, 'EH', 'Western Sahara', ''),
(242, 'YE', 'Yemen', ''),
(243, 'ZR', 'Zaire', ''),
(244, 'ZM', 'Zambia', ''),
(245, 'ZW', 'Zimbabwe', '');

-- --------------------------------------------------------

--
-- Table structure for table `xin_currencies`
--

CREATE TABLE `xin_currencies` (
  `currency_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `symbol` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_currencies`
--

INSERT INTO `xin_currencies` (`currency_id`, `company_id`, `name`, `code`, `symbol`) VALUES
(1, 1, 'Dollars', 'USD', '$'),
(2, 0, 'Naira', 'NGN', '₦');

-- --------------------------------------------------------

--
-- Table structure for table `xin_currency_converter`
--

CREATE TABLE `xin_currency_converter` (
  `currency_converter_id` int(11) NOT NULL,
  `usd_currency` varchar(11) NOT NULL DEFAULT '1',
  `to_currency_title` varchar(200) NOT NULL,
  `to_currency_rate` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_currency_converter`
--

INSERT INTO `xin_currency_converter` (`currency_converter_id`, `usd_currency`, `to_currency_title`, `to_currency_rate`, `created_at`) VALUES
(1, '1', 'MYR', '4.11', '17-08-2018 03:29:58');

-- --------------------------------------------------------

--
-- Table structure for table `xin_database_backup`
--

CREATE TABLE `xin_database_backup` (
  `backup_id` int(111) NOT NULL,
  `backup_file` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_departments`
--

CREATE TABLE `xin_departments` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(200) NOT NULL,
  `company_id` int(11) NOT NULL,
  `location_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_departments`
--

INSERT INTO `xin_departments` (`department_id`, `department_name`, `company_id`, `location_id`, `employee_id`, `added_by`, `created_at`, `status`) VALUES
(1, 'MD Office', 1, 2, 5, 0, '06-03-2018', 1),
(2, 'Accounts and  Finances', 1, 3, 5, 1, '17-03-2018', 1),
(3, 'Client Services', 1, 2, 5, 1, '2019-11-02 00:48:45', 1),
(4, 'Claims', 1, 2, 5, 1, '2019-11-02 00:49:26', 1),
(5, 'Business Development', 1, 2, 5, 1, '2019-11-02 00:49:45', 1),
(6, 'Underwriting', 1, 2, 5, 1, '2019-11-02 00:50:09', 1),
(7, 'Call Center', 1, 2, 5, 1, '2019-11-02 00:50:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `xin_designations`
--

CREATE TABLE `xin_designations` (
  `designation_id` int(11) NOT NULL,
  `top_designation_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(200) NOT NULL,
  `sub_department_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `designation_name` varchar(200) NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_designations`
--

INSERT INTO `xin_designations` (`designation_id`, `top_designation_id`, `department_id`, `sub_department_id`, `company_id`, `designation_name`, `added_by`, `created_at`, `status`) VALUES
(9, 0, 1, 8, 1, 'Software Developer', 1, '06-03-2018', 1),
(10, 0, 2, 10, 1, 'Finance', 1, '18-03-2018', 1);

-- --------------------------------------------------------

--
-- Table structure for table `xin_document_type`
--

CREATE TABLE `xin_document_type` (
  `document_type_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `document_type` varchar(255) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_document_type`
--

INSERT INTO `xin_document_type` (`document_type_id`, `company_id`, `document_type`, `created_at`) VALUES
(1, 1, 'Driving License', '09-05-2018 12:34:55');

-- --------------------------------------------------------

--
-- Table structure for table `xin_email_template`
--

CREATE TABLE `xin_email_template` (
  `template_id` int(111) NOT NULL,
  `template_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` longtext NOT NULL,
  `status` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_email_template`
--

INSERT INTO `xin_email_template` (`template_id`, `template_code`, `name`, `subject`, `message`, `status`) VALUES
(2, 'code1', 'Forgot Password', 'Forgot Password', '&lt;p&gt;There was recently a request for password for your Â {var site_name}Â account.&lt;/p&gt;&lt;p&gt;Please, keep it in your records so you don\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&#039;t forget it.&lt;/p&gt;&lt;p&gt;Your username: {var username}&lt;br&gt;Your email address: {var email}&lt;br&gt;Your password: {var password}&lt;/p&gt;&lt;p&gt;Thank you,&lt;br&gt;The {var site_name} Team&lt;/p&gt;', 1),
(3, 'code2', 'New Project', 'New Project', '&lt;p&gt;Dear {var name},&lt;/p&gt;&lt;p&gt;New project has been assigned to you.&lt;/p&gt;&lt;p&gt;Project Name: {var project_name}&lt;/p&gt;&lt;p&gt;Project Start Date:&amp;nbsp;&lt;span 1rem;\\\\\\&quot;=\\&quot;\\&quot;&gt;{var project_start_date}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span 1rem;\\\\\\&quot;=\\&quot;\\&quot;&gt;Thank you,&lt;/span&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;The {var site_name} Team&lt;/p&gt;', 1),
(5, 'code3', 'Leave Request ', 'A Leave Request from you', '&lt;p&gt;Dear Admin,&lt;/p&gt;&lt;p&gt;{var employee_name}Â wants a leave from you.&lt;/p&gt;&lt;p&gt;You can view this leave request by logging in to the portal using the link below.&lt;/p&gt;&lt;p&gt;{var site_url}admin/&lt;br&gt;&lt;br&gt;Regards&lt;/p&gt;&lt;p&gt;The {var site_name} Team&lt;/p&gt;', 1),
(6, 'code4', 'Leave Approve', 'Your leave request has been approved', '&lt;p&gt;Your leave request has been approved&lt;/p&gt;&lt;p&gt;&lt;span style=\\&quot;font-size: 1rem;\\&quot;&gt;Congratulations! Your leave request from&lt;/span&gt;&lt;font color=\\&quot;#333333\\&quot; face=\\&quot;sans-serif, Arial, Verdana, Trebuchet MS\\&quot;&gt;&amp;nbsp;&lt;/font&gt;{var leave_start_date}&amp;nbsp;to&amp;nbsp;{var leave_end_date}&amp;nbsp;has been approved by your company management.&lt;/p&gt;&lt;p&gt;Check here&lt;/p&gt;&lt;p&gt;{var site_url}hr/user/leave/&lt;br&gt;&lt;/p&gt;&lt;p&gt;Regards&lt;br&gt;The {var site_name} Team&lt;/p&gt;', 1),
(7, 'code5', 'Leave Reject', 'Your leave request has been Rejected', '&lt;p&gt;Your leave request has been Rejected&lt;/p&gt;&lt;p&gt;Unfortunately !Â Your leave request fromÂ {var leave_start_date}Â toÂ {var leave_end_date}Â has been Rejected by your company management.&lt;/p&gt;&lt;p&gt;Check here&lt;/p&gt;&lt;p&gt;{var site_url}hr/user/leave/&lt;/p&gt;&lt;p&gt;Regards&lt;/p&gt;&lt;p&gt;The {var site_name} Team&lt;/p&gt;', 1),
(8, 'code6', 'Welcome Email ', 'Welcome Email ', '&lt;p&gt;Hello&amp;nbsp;{var employee_name},&lt;/p&gt;&lt;p&gt;Welcome to&amp;nbsp;{var site_name}&amp;nbsp;.Thanks for joining&amp;nbsp;{var site_name}. We listed your sign in details below, make sure you keep them safe.&lt;/p&gt;&lt;p&gt;Your Username: {var username}&lt;/p&gt;&lt;p&gt;Your Employee ID: {var employee_id}&lt;br&gt;Your Email Address: {var email}&lt;br&gt;Your Password: {var password}&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=\\&quot;{var site_url}\\&quot;&gt;&lt;a href=\\&quot;{var site_url}/hr/\\&quot;&gt;Login Panel&lt;/a&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Link doesn\\&#039;t work? Copy the following link to your browser address bar:&lt;/p&gt;&lt;p&gt;{var site_url}/hr/&lt;/p&gt;&lt;p&gt;Have fun!&lt;/p&gt;&lt;p&gt;The&amp;nbsp;{var site_name}&amp;nbsp;Team.&lt;/p&gt;', 1),
(9, 'code7', 'Transfer', 'New Transfer', '&lt;p&gt;Hello&amp;nbsp;{var employee_name},&lt;/p&gt;&lt;p&gt;You have been&amp;nbsp;transfered to another department and location.&lt;/p&gt;&lt;p&gt;You can view the transfer details by logging in to the portal using the link below.&lt;/p&gt;&lt;p&gt;{var site_url}hr/user/transfer/&lt;/p&gt;&lt;p&gt;Regards&lt;/p&gt;&lt;p&gt;The {var site_name} Team&lt;/p&gt;', 1),
(10, 'code8', 'Award', 'Award Received', '&lt;p&gt;Hello&amp;nbsp;{var employee_name},&lt;/p&gt;&lt;p&gt;You have been&amp;nbsp;awarded&amp;nbsp;{var award_name}.&lt;/p&gt;&lt;p&gt;You can view this award by logging in to the portal using the link below.&lt;/p&gt;&lt;p&gt;&lt;span style=\\&quot;font-size: 1rem;\\&quot;&gt;{var site_url}hr/&lt;/span&gt;&lt;span style=\\&quot;font-size: 1rem;\\&quot;&gt;user/awards/&lt;/span&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;Regards&lt;/p&gt;&lt;p&gt;The {var site_name} Team&lt;/p&gt;', 1),
(14, 'code9', 'New Task', 'Task assigned', '&lt;p&gt;Dear Employee,&lt;/p&gt;&lt;p&gt;A new task&amp;nbsp;&lt;span style=\\&quot;\\\\&amp;quot;font-weight:\\&quot; bolder;\\\\\\&quot;=\\&quot;\\&quot;&gt;{var task_name}&lt;/span&gt;&amp;nbsp;has been assigned to you by&amp;nbsp;&lt;span style=\\&quot;\\\\&amp;quot;font-weight:\\&quot; bolder;\\\\\\&quot;=\\&quot;\\&quot;&gt;{var task_assigned_by}&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;You can view this task by logging in to the portal using the link below.&lt;/p&gt;&lt;p&gt;{var site_url}hr/user/tasks/&lt;br&gt;&lt;/p&gt;&lt;p&gt;Link doesn\\\\\\&#039;t work? Copy the following link to your browser address bar:&lt;/p&gt;&lt;p&gt;{var site_url}&lt;/p&gt;&lt;p&gt;Regards,&lt;/p&gt;&lt;p&gt;The {var site_name} Team&lt;/p&gt;', 1),
(15, 'code10', 'New Inquiry', 'New Inquiry [#{var ticket_code}]', '&lt;p xss=removed rgb(51,=\\&quot;\\\\\\&quot; font-family:=\\&quot;\\\\\\&quot; sans-serif,=\\&quot;\\\\\\&quot; arial,=\\&quot;\\\\\\&quot; verdana,=\\&quot;\\\\\\&quot; trebuchet=\\&quot;\\\\\\\\\\&quot;&gt;&lt;span xss=removed&gt;Dear Admin,&lt;/span&gt;&lt;br&gt;&lt;/p&gt;&lt;p xss=removed rgb(51,=\\&quot;\\\\\\&quot; font-family:=\\&quot;\\\\\\&quot; sans-serif,=\\&quot;\\\\\\&quot; arial,=\\&quot;\\\\\\&quot; verdana,=\\&quot;\\\\\\&quot; trebuchet=\\&quot;\\\\\\\\\\&quot;&gt;&lt;span xss=removed&gt;Your received a new inquiry.&lt;/span&gt;&lt;/p&gt;&lt;p xss=removed rgb(51,=\\&quot;\\\\\\&quot; font-family:=\\&quot;\\\\\\&quot; sans-serif,=\\&quot;\\\\\\&quot; arial,=\\&quot;\\\\\\&quot; verdana,=\\&quot;\\\\\\&quot; trebuchet=\\&quot;\\\\\\\\\\&quot;&gt;&lt;span xss=removed&gt;Inquiry Code: #{var ticket_code}&lt;/span&gt;&lt;/p&gt;&lt;p xss=removed rgb(51,=\\&quot;\\\\\\&quot; font-family:=\\&quot;\\\\\\&quot; sans-serif,=\\&quot;\\\\\\&quot; arial,=\\&quot;\\\\\\&quot; verdana,=\\&quot;\\\\\\&quot; trebuchet=\\&quot;\\\\\\\\\\&quot;&gt;Status : Open&lt;br&gt;&lt;br&gt;Click on the below link to see the inquiry details and post additional comments.&lt;/p&gt;&lt;p xss=removed rgb(51,=\\&quot;\\\\\\&quot; font-family:=\\&quot;\\\\\\&quot; sans-serif,=\\&quot;\\\\\\&quot; arial,=\\&quot;\\\\\\&quot; verdana,=\\&quot;\\\\\\&quot; trebuchet=\\&quot;\\\\\\\\\\&quot;&gt;{var site_url}admin/tickets/&lt;br&gt;&lt;br&gt;Regards&lt;/p&gt;&lt;p xss=removed rgb(51,=\\&quot;\\\\\\&quot; font-family:=\\&quot;\\\\\\&quot; sans-serif,=\\&quot;\\\\\\&quot; arial,=\\&quot;\\\\\\&quot; verdana,=\\&quot;\\\\\\&quot; trebuchet=\\&quot;\\\\\\\\\\&quot;&gt;The {var site_name} Team&lt;/p&gt;', 1),
(16, 'code11', 'Enrollee Welcome Email', 'Enrollee Welcome Email', '&lt;p&gt;HelloÂ {var client_name},&lt;/p&gt;&lt;p&gt;Welcome toÂ {var site_name}Â .Thanks for joiningÂ {var site_name}. We listed your sign in details below, make sure you keep them safe. You can login to your panel using email and password.&lt;/p&gt;&lt;p&gt;Your Username: {var username}&lt;/p&gt;&lt;p&gt;&lt;span xss=\\&quot;\\\\\\\\\\&quot;&gt;Your Email Address: {var email}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Your Password: {var password}&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=\\&quot;\\\\\\\\\\&quot;&gt;&lt;/a&gt;&lt;a href=\\&quot;\\\\\\\\\\&quot; hr=\\&quot;\\\\\\\\\\&quot;&gt;&lt;/a&gt;&lt;a href=\\&quot;\\\\\\\\\\&quot; client=\\&quot;\\\\\\\\\\&quot;&gt;Login Panel&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Link doesn\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&#039;t work? Copy the following link to your browser address bar:&lt;/p&gt;&lt;p&gt;{var site_url}/client/&lt;/p&gt;&lt;p&gt;Have fun!&lt;/p&gt;&lt;p&gt;Â {var site_name}Â Team.&lt;/p&gt;', 1);

-- --------------------------------------------------------

--
-- Table structure for table `xin_employees`
--

CREATE TABLE `xin_employees` (
  `user_id` int(11) NOT NULL,
  `employee_id` varchar(200) NOT NULL,
  `office_shift_id` int(111) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `date_of_birth` varchar(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `e_status` int(11) NOT NULL,
  `user_role_id` int(100) NOT NULL,
  `department_id` int(100) NOT NULL,
  `sub_department_id` int(11) NOT NULL,
  `designation_id` int(100) NOT NULL,
  `company_id` int(111) DEFAULT NULL,
  `location_id` int(11) NOT NULL,
  `view_companies_id` varchar(255) NOT NULL,
  `salary_template` varchar(255) NOT NULL,
  `hourly_grade_id` int(111) NOT NULL,
  `monthly_grade_id` int(111) NOT NULL,
  `date_of_joining` varchar(200) NOT NULL,
  `date_of_leaving` varchar(255) NOT NULL,
  `marital_status` varchar(255) NOT NULL,
  `salary` varchar(200) NOT NULL,
  `wages_type` int(11) NOT NULL,
  `basic_salary` varchar(200) NOT NULL DEFAULT '0',
  `daily_wages` varchar(200) NOT NULL DEFAULT '0',
  `salary_ssempee` varchar(200) NOT NULL DEFAULT '0',
  `salary_ssempeer` varchar(200) DEFAULT '0',
  `salary_income_tax` varchar(200) NOT NULL DEFAULT '0',
  `salary_overtime` varchar(200) NOT NULL DEFAULT '0',
  `salary_commission` varchar(200) NOT NULL DEFAULT '0',
  `salary_claims` varchar(200) NOT NULL DEFAULT '0',
  `salary_paid_leave` varchar(200) NOT NULL DEFAULT '0',
  `salary_director_fees` varchar(200) NOT NULL DEFAULT '0',
  `salary_bonus` varchar(200) NOT NULL DEFAULT '0',
  `salary_advance_paid` varchar(200) NOT NULL DEFAULT '0',
  `address` mediumtext NOT NULL,
  `state` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `zipcode` varchar(200) NOT NULL,
  `profile_picture` mediumtext NOT NULL,
  `profile_background` mediumtext NOT NULL,
  `resume` mediumtext NOT NULL,
  `skype_id` varchar(200) NOT NULL,
  `contact_no` varchar(200) NOT NULL,
  `facebook_link` mediumtext NOT NULL,
  `twitter_link` mediumtext NOT NULL,
  `blogger_link` mediumtext NOT NULL,
  `linkdedin_link` mediumtext NOT NULL,
  `google_plus_link` mediumtext NOT NULL,
  `instagram_link` varchar(255) NOT NULL,
  `pinterest_link` varchar(255) NOT NULL,
  `youtube_link` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_login_date` varchar(255) NOT NULL,
  `last_logout_date` varchar(255) NOT NULL,
  `last_login_ip` varchar(255) NOT NULL,
  `is_logged_in` int(111) NOT NULL,
  `online_status` int(111) NOT NULL,
  `fixed_header` varchar(150) NOT NULL,
  `compact_sidebar` varchar(150) NOT NULL,
  `boxed_wrapper` varchar(150) NOT NULL,
  `leave_categories` varchar(255) NOT NULL DEFAULT '0',
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_employees`
--

INSERT INTO `xin_employees` (`user_id`, `employee_id`, `office_shift_id`, `first_name`, `last_name`, `username`, `email`, `password`, `date_of_birth`, `gender`, `e_status`, `user_role_id`, `department_id`, `sub_department_id`, `designation_id`, `company_id`, `location_id`, `view_companies_id`, `salary_template`, `hourly_grade_id`, `monthly_grade_id`, `date_of_joining`, `date_of_leaving`, `marital_status`, `salary`, `wages_type`, `basic_salary`, `daily_wages`, `salary_ssempee`, `salary_ssempeer`, `salary_income_tax`, `salary_overtime`, `salary_commission`, `salary_claims`, `salary_paid_leave`, `salary_director_fees`, `salary_bonus`, `salary_advance_paid`, `address`, `state`, `city`, `zipcode`, `profile_picture`, `profile_background`, `resume`, `skype_id`, `contact_no`, `facebook_link`, `twitter_link`, `blogger_link`, `linkdedin_link`, `google_plus_link`, `instagram_link`, `pinterest_link`, `youtube_link`, `is_active`, `last_login_date`, `last_logout_date`, `last_login_ip`, `is_logged_in`, `online_status`, `fixed_header`, `compact_sidebar`, `boxed_wrapper`, `leave_categories`, `created_at`) VALUES
(1, 'LTG001', 1, 'Kennedy', 'Job', 'kenny9', 'kennedyjob1992@gmail.com', '$2y$12$Ze7Temq6UqKx1FAjOnCDBOt4k.BYqOeNeu5OzuDRWj6jxaGdE4OIG', '1992-09-29', 'Male', 0, 1, 2, 10, 10, 1, 1, '0', 'monthly', 0, 0, '2018-02-01', '', 'Single', '', 1, '1000', '0', '8', '17', '10', '0', '1', '2', '3', '0', '0', '0', 'Test Address', '', '', '', 'profile_1568646433.png', 'profile_background_1519924152.jpg', '', '', '07031806085', '', '', '', '', '', '', '', '', 1, '06-12-2019 13:08:48', '26-11-2019 21:32:59', '105.112.120.17', 1, 1, '', '', '', '0,1,2', '2018-02-28 05:30:44'),
(5, '00343', 1, 'Patience', 'Onoja', 'patience', 'patience.onoja@liontech.com', '$2y$12$zjBiQwIQG7vmgGeq935iqOCDiQVREZgA3VsN44YderDI5YoXKkWdi', '2018-11-22', 'Female', 0, 2, 1, 0, 9, 1, 2, '0,1', 'monthly', 0, 0, '2018-03-02', '', 'Single', '', 1, '1000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'jsmt12', '', '', '', '', '', '', '', '1232', '', '', '', '', '', '', '', '', 1, '17-04-2019 08:42:32', '26-03-2019 07:31:01', '::1', 1, 1, '', '', '', '0,1,2', '2018-03-18 01:10:04'),
(6, '00033', 1, 'Muhammad', 'Awan', 'muhammad', 'hmo@liontech.com.ng', '$2y$12$VlUbTFTocOmjmqls2Ld1VOavt8rOUsIjYPBcLXE35YDtzrwUt6m7m', '2019-11-01', 'Male', 0, 1, 1, 8, 9, 1, 1, '', '', 0, 0, '2019-10-31', '', '', '', 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'sdsdsdsdsdsdsd', '', '', '', '', '', '', '', '4554545', '', '', '', '', '', '', '', '', 1, '04-12-2019 15:48:57', '02-11-2019 00:18:43', '39.52.127.48', 1, 0, '', '', '', '0', '2019-10-31 10:50:24'),
(7, '05557', 1, 'Musa', 'Muhammad', 'musa', 'musa@musa.musa', '$2y$12$tPvkZo2mdZlh66TEH8jvlOc/XWxSK9Ch/lxJYoiRDNuQKXUE.zu2.', '2019-11-16', 'Male', 0, 1, 1, 0, 9, 1, 2, '', '', 0, 0, '2019-11-16', '', '', '', 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Address 123, Address 456', '', '', '', '', '', '', '', '445865456', '', '', '', '', '', '', '', '', 1, '18-11-2019 07:15:32', '', '39.34.225.122', 1, 0, '', '', '', '0', '2019-11-16 03:01:13'),
(8, '3332', 1, 'Fiverr', 'Fiverr', 'fiverr', 'hello@liontech.com.ng', '$2y$12$hXqd4Pvunejckr2IowpkPed1nHjwCSBRYGI1J5zhU9meUfWjmhkjK', '2003-11-17', 'Male', 0, 1, 1, 0, 9, 1, 2, '', '', 0, 0, '2019-11-18', '', '', '', 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'No 2 kayode close Johannesburg, Soweto', '', '', '', '', '', '', '', '114074531', '', '', '', '', '', '', '', '', 1, '06-12-2019 11:27:42', '21-11-2019 17:02:05', '116.206.166.100', 1, 0, '', '', '', '0,1', '2019-11-18 10:42:34');

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_bankaccount`
--

CREATE TABLE `xin_employee_bankaccount` (
  `bankaccount_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `is_primary` int(11) NOT NULL,
  `account_title` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_code` varchar(255) NOT NULL,
  `bank_branch` mediumtext NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_complaints`
--

CREATE TABLE `xin_employee_complaints` (
  `complaint_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `complaint_from` int(111) NOT NULL,
  `title` varchar(255) NOT NULL,
  `complaint_date` varchar(255) NOT NULL,
  `complaint_against` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `status` tinyint(2) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_contacts`
--

CREATE TABLE `xin_employee_contacts` (
  `contact_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `relation` varchar(255) NOT NULL,
  `is_primary` int(111) NOT NULL,
  `is_dependent` int(111) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `work_phone` varchar(255) NOT NULL,
  `work_phone_extension` varchar(255) NOT NULL,
  `mobile_phone` varchar(255) NOT NULL,
  `home_phone` varchar(255) NOT NULL,
  `work_email` varchar(255) NOT NULL,
  `personal_email` varchar(255) NOT NULL,
  `address_1` mediumtext NOT NULL,
  `address_2` mediumtext NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_contract`
--

CREATE TABLE `xin_employee_contract` (
  `contract_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `contract_type_id` int(111) NOT NULL,
  `from_date` varchar(255) NOT NULL,
  `designation_id` int(111) NOT NULL,
  `title` varchar(255) NOT NULL,
  `to_date` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_documents`
--

CREATE TABLE `xin_employee_documents` (
  `document_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `document_type_id` int(111) NOT NULL,
  `date_of_expiry` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `notification_email` varchar(255) NOT NULL,
  `is_alert` tinyint(1) NOT NULL,
  `description` mediumtext NOT NULL,
  `document_file` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_exit`
--

CREATE TABLE `xin_employee_exit` (
  `exit_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `exit_date` varchar(255) NOT NULL,
  `exit_type_id` int(111) NOT NULL,
  `exit_interview` int(111) NOT NULL,
  `is_inactivate_account` int(111) NOT NULL,
  `reason` mediumtext NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_exit_type`
--

CREATE TABLE `xin_employee_exit_type` (
  `exit_type_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_employee_exit_type`
--

INSERT INTO `xin_employee_exit_type` (`exit_type_id`, `company_id`, `type`, `created_at`) VALUES
(1, 1, 'Test', '');

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_immigration`
--

CREATE TABLE `xin_employee_immigration` (
  `immigration_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `document_type_id` int(111) NOT NULL,
  `document_number` varchar(255) NOT NULL,
  `document_file` varchar(255) NOT NULL,
  `issue_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `eligible_review_date` varchar(255) NOT NULL,
  `comments` mediumtext NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_leave`
--

CREATE TABLE `xin_employee_leave` (
  `leave_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `contract_id` int(111) NOT NULL,
  `casual_leave` varchar(255) NOT NULL,
  `medical_leave` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_location`
--

CREATE TABLE `xin_employee_location` (
  `office_location_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `location_id` int(111) NOT NULL,
  `from_date` varchar(255) NOT NULL,
  `to_date` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_promotions`
--

CREATE TABLE `xin_employee_promotions` (
  `promotion_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `title` varchar(255) NOT NULL,
  `promotion_date` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_qualification`
--

CREATE TABLE `xin_employee_qualification` (
  `qualification_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `name` varchar(255) NOT NULL,
  `education_level_id` int(111) NOT NULL,
  `from_year` varchar(255) NOT NULL,
  `language_id` int(111) NOT NULL,
  `to_year` varchar(255) NOT NULL,
  `skill_id` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_resignations`
--

CREATE TABLE `xin_employee_resignations` (
  `resignation_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `notice_date` varchar(255) NOT NULL,
  `resignation_date` varchar(255) NOT NULL,
  `reason` mediumtext NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_shift`
--

CREATE TABLE `xin_employee_shift` (
  `emp_shift_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `shift_id` int(111) NOT NULL,
  `from_date` varchar(255) NOT NULL,
  `to_date` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_terminations`
--

CREATE TABLE `xin_employee_terminations` (
  `termination_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `terminated_by` int(111) NOT NULL,
  `termination_type_id` int(111) NOT NULL,
  `termination_date` varchar(255) NOT NULL,
  `notice_date` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `status` tinyint(2) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_transfer`
--

CREATE TABLE `xin_employee_transfer` (
  `transfer_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `transfer_date` varchar(255) NOT NULL,
  `transfer_department` int(111) NOT NULL,
  `transfer_location` int(111) NOT NULL,
  `description` mediumtext NOT NULL,
  `status` tinyint(2) NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_travels`
--

CREATE TABLE `xin_employee_travels` (
  `travel_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `visit_purpose` varchar(255) NOT NULL,
  `visit_place` varchar(255) NOT NULL,
  `travel_mode` int(111) DEFAULT NULL,
  `arrangement_type` int(111) DEFAULT NULL,
  `expected_budget` varchar(255) NOT NULL,
  `actual_budget` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `status` tinyint(2) NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_warnings`
--

CREATE TABLE `xin_employee_warnings` (
  `warning_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `warning_to` int(111) NOT NULL,
  `warning_by` int(111) NOT NULL,
  `warning_date` varchar(255) NOT NULL,
  `warning_type_id` int(111) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `status` tinyint(2) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_employee_work_experience`
--

CREATE TABLE `xin_employee_work_experience` (
  `work_experience_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `from_date` varchar(255) NOT NULL,
  `to_date` varchar(255) NOT NULL,
  `post` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_events`
--

CREATE TABLE `xin_events` (
  `event_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `event_title` varchar(255) NOT NULL,
  `event_date` varchar(255) NOT NULL,
  `event_time` varchar(255) NOT NULL,
  `event_note` mediumtext NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_events`
--

INSERT INTO `xin_events` (`event_id`, `company_id`, `employee_id`, `event_title`, `event_date`, `event_time`, `event_note`, `created_at`) VALUES
(1, 1, 5, 'HMO Training', '2019-11-03', '19:35', 'No data available in table\n', '2019-11-02');

-- --------------------------------------------------------

--
-- Table structure for table `xin_expenses`
--

CREATE TABLE `xin_expenses` (
  `expense_id` int(11) NOT NULL,
  `employee_id` int(200) NOT NULL,
  `company_id` int(11) NOT NULL,
  `expense_type_id` int(200) NOT NULL,
  `billcopy_file` mediumtext NOT NULL,
  `amount` varchar(200) NOT NULL,
  `purchase_date` varchar(200) NOT NULL,
  `remarks` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `status_remarks` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_expense_type`
--

CREATE TABLE `xin_expense_type` (
  `expense_type_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_expense_type`
--

INSERT INTO `xin_expense_type` (`expense_type_id`, `company_id`, `name`, `status`, `created_at`) VALUES
(1, 1, 'Supplies', 1, '22-03-2018 01:17:42'),
(2, 1, 'Utility', 1, '22-03-2018 01:17:48');

-- --------------------------------------------------------

--
-- Table structure for table `xin_file_manager`
--

CREATE TABLE `xin_file_manager` (
  `file_id` int(111) NOT NULL,
  `user_id` int(111) NOT NULL,
  `department_id` int(111) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_size` varchar(255) NOT NULL,
  `file_extension` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_file_manager_settings`
--

CREATE TABLE `xin_file_manager_settings` (
  `setting_id` int(111) NOT NULL,
  `allowed_extensions` mediumtext NOT NULL,
  `maximum_file_size` varchar(255) NOT NULL,
  `is_enable_all_files` varchar(255) NOT NULL,
  `updated_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_file_manager_settings`
--

INSERT INTO `xin_file_manager_settings` (`setting_id`, `allowed_extensions`, `maximum_file_size`, `is_enable_all_files`, `updated_at`) VALUES
(1, 'gif,png,pdf,txt,doc,docx', '10', 'yes', '2019-03-17 08:57:59');

-- --------------------------------------------------------

--
-- Table structure for table `xin_finance_bankcash`
--

CREATE TABLE `xin_finance_bankcash` (
  `bankcash_id` int(111) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `account_balance` varchar(255) NOT NULL,
  `account_opening_balance` varchar(200) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `branch_code` varchar(255) NOT NULL,
  `bank_branch` text NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_finance_bankcash`
--

INSERT INTO `xin_finance_bankcash` (`bankcash_id`, `account_name`, `account_balance`, `account_opening_balance`, `account_number`, `branch_code`, `bank_branch`, `created_at`) VALUES
(1, 'Ecobank', '100000', '100000', '3223223', 'WS', '', '20-11-2019 04:04:57'),
(2, 'FCMB', '0', '0', '55533', 'FC', '', '20-11-2019 04:05:16'),
(3, 'GT Bank', '200000', '200000', '3232323', '232', '', '20-11-2019 08:25:52');

-- --------------------------------------------------------

--
-- Table structure for table `xin_finance_deposit`
--

CREATE TABLE `xin_finance_deposit` (
  `deposit_id` int(111) NOT NULL,
  `account_type_id` int(111) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `deposit_date` varchar(255) NOT NULL,
  `category_id` int(111) NOT NULL,
  `payer_id` int(111) NOT NULL,
  `payment_method` int(111) NOT NULL,
  `deposit_reference` varchar(255) NOT NULL,
  `deposit_file` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_finance_expense`
--

CREATE TABLE `xin_finance_expense` (
  `expense_id` int(111) NOT NULL,
  `account_type_id` int(111) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `expense_date` varchar(255) NOT NULL,
  `category_id` int(111) NOT NULL,
  `payee_id` int(111) NOT NULL,
  `payment_method` int(111) NOT NULL,
  `expense_reference` varchar(255) NOT NULL,
  `expense_file` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_finance_payees`
--

CREATE TABLE `xin_finance_payees` (
  `payee_id` int(11) NOT NULL,
  `payee_name` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_finance_payers`
--

CREATE TABLE `xin_finance_payers` (
  `payer_id` int(11) NOT NULL,
  `payer_name` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_finance_transaction`
--

CREATE TABLE `xin_finance_transaction` (
  `transaction_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `transaction_date` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `amount` float NOT NULL,
  `transaction_type` varchar(100) NOT NULL,
  `dr_cr` enum('dr','cr') NOT NULL,
  `transaction_cat_id` int(11) NOT NULL,
  `payer_payee_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `reference` varchar(100) NOT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `invoice_type` varchar(100) DEFAULT NULL,
  `attachment_file` varchar(100) DEFAULT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `xin_finance_transactions`
--

CREATE TABLE `xin_finance_transactions` (
  `transaction_id` int(111) NOT NULL,
  `account_type_id` int(111) NOT NULL,
  `deposit_id` int(111) NOT NULL,
  `expense_id` int(111) NOT NULL,
  `transfer_id` int(111) NOT NULL,
  `transaction_type` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `transaction_debit` varchar(255) NOT NULL,
  `transaction_credit` varchar(255) NOT NULL,
  `transaction_date` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_finance_transfer`
--

CREATE TABLE `xin_finance_transfer` (
  `transfer_id` int(111) NOT NULL,
  `from_account_id` int(111) NOT NULL,
  `to_account_id` int(111) NOT NULL,
  `transfer_date` varchar(255) NOT NULL,
  `transfer_amount` varchar(255) NOT NULL,
  `payment_method` varchar(111) NOT NULL,
  `transfer_reference` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_goal_tracking`
--

CREATE TABLE `xin_goal_tracking` (
  `tracking_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `tracking_type_id` int(200) NOT NULL,
  `start_date` varchar(200) NOT NULL,
  `end_date` varchar(200) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `target_achiement` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `goal_progress` varchar(200) NOT NULL,
  `goal_status` int(11) NOT NULL DEFAULT '0',
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_goal_tracking_type`
--

CREATE TABLE `xin_goal_tracking_type` (
  `tracking_type_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_goal_tracking_type`
--

INSERT INTO `xin_goal_tracking_type` (`tracking_type_id`, `company_id`, `type_name`, `created_at`) VALUES
(1, 1, 'Invoice Goal', '31-08-2018 01:29:44'),
(4, 1, 'Event Goal', '31-08-2018 01:29:47');

-- --------------------------------------------------------

--
-- Table structure for table `xin_holidays`
--

CREATE TABLE `xin_holidays` (
  `holiday_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `event_name` varchar(200) NOT NULL,
  `description` mediumtext NOT NULL,
  `start_date` varchar(200) NOT NULL,
  `end_date` varchar(200) NOT NULL,
  `is_publish` tinyint(1) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_hospital`
--

CREATE TABLE `xin_hospital` (
  `hospital_id` int(11) NOT NULL,
  `hospital_name` varchar(255) NOT NULL,
  `location_id` int(11) NOT NULL,
  `band_id` int(11) NOT NULL,
  `tarrif` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `created_on` datetime DEFAULT NULL,
  `logo_img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_hospital`
--

INSERT INTO `xin_hospital` (`hospital_id`, `hospital_name`, `location_id`, `band_id`, `tarrif`, `email`, `password`, `phone`, `created_on`, `logo_img`) VALUES
(3, 'Limi Hospital', 15, 9, '2000', 'limih@gmail.com', '123456', '813854264', '2019-11-01 11:57:07', ''),
(4, 'Maitama Hospital', 15, 7, '4500', 'testing@this.com', '', '445865456', '2019-11-11 12:34:48', ''),
(5, 'Enugu Teaching Hospital', 18, 9, '1300', 'kennedyjob@myweb.ng', '12345', '7031806085', '2019-11-02 12:02:36', ''),
(6, 'Rave Way Clinics', 13, 8, '1600', 'kennedyjob@yahoo.com', '123456', '7031806085', '2019-11-02 12:03:59', ''),
(7, 'Wuse General Hospital', 15, 9, '1450', 'testing@this.com', '', '445865456', '2019-11-11 12:35:04', 'logo_1572767456.jpg'),
(8, 'test2sdf', 17, 8, '66', 'John1122@gmail.com', '', '32434343', '2019-11-05 04:17:45', 'logo_1572708333.jpg'),
(9, 'Jos Clinics', 16, 8, '', 'kennedyjob@myweb.ng', '123456', '7031806085', '2019-11-16 04:59:26', 'logo_1573919966.png'),
(10, 'Test Hospital', 15, 9, '', 'hospital@liontech.com.ng', '123456', '813854264', '2019-11-18 11:47:01', 'logo_1574117221.jpg'),
(11, 'zz', 17, 8, '', 'zeeshan72awan@gmail.com', '1122', '32434343', '2019-12-04 05:04:03', 'logo_1575475443.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `xin_hospital_drugs`
--

CREATE TABLE `xin_hospital_drugs` (
  `drug_id` int(11) NOT NULL,
  `drug_name` varchar(255) NOT NULL,
  `drug_price` decimal(10,2) NOT NULL,
  `hospital_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_hospital_drugs`
--

INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(2, 'test', 2323.00, 8, '2019-11-05 04:03:59'),
(3, 'testing', 222.00, 8, '2019-11-05 04:03:59'),
(4, 'test', 12.00, 8, '2019-11-05 04:13:57'),
(5, 'test', 2323.00, 8, '2019-11-05 04:13:57'),
(6, 'tt', 222.00, 8, '2019-11-05 04:13:57'),
(7, 'test', 12.00, 8, '2019-11-05 04:18:04'),
(8, 'test', 2323.00, 8, '2019-11-05 04:18:04'),
(9, 'ttd', 4222.00, 8, '2019-11-05 04:18:04'),
(10, 'test', 12.00, 8, '2019-11-05 04:19:18'),
(12, 'tt', 222.00, 8, '2019-11-05 04:19:18'),
(14, 'test', 2323.00, 8, '2019-11-05 04:19:48'),
(15, 'tt', 222.00, 8, '2019-11-05 04:19:48'),
(16, 'test', 12.00, 8, '2019-11-05 05:07:48'),
(17, 'test', 2323.00, 8, '2019-11-05 05:07:48'),
(18, 'tt', 222.00, 8, '2019-11-05 05:07:48'),
(19, '\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 \0\0\0!\0\0\0\"\0\0\0#\0\0\0$\0\0\0%\0\0\0&\0\0\0\'\0\0\0(\0\0\0)\0\0\0*\0\0\0+\0\0\0', 0.00, 4, '2019-11-11 12:37:02'),
(22, '\0\0Pelox', 0.00, 4, '2019-11-11 12:37:02'),
(23, '\0\0Supracin', 0.00, 4, '2019-11-11 12:37:02'),
(24, '\0\0Tetacid ', 0.00, 4, '2019-11-11 12:37:02'),
(25, '\0\0\0	\0\0\0?\r?\0\0\0\0\0\0\0\0\0d\0\0\0\0\0\0\0\0\0\0????MbP?_\0\0\0?\0\0\0\0\0\0\0\0\0\0%\0\0\0', 0.00, 4, '2019-11-11 12:37:02'),
(26, '\0\0\0\0', 0.00, 4, '2019-11-11 12:37:02'),
(27, '\0\0Ixime', 0.00, 6, '2019-11-12 06:27:31'),
(28, '\0\0Manix', 0.00, 6, '2019-11-12 06:27:32'),
(29, '\0\0Pelox', 0.00, 6, '2019-11-12 06:27:32'),
(30, '\0\0Supracin', 0.00, 6, '2019-11-12 06:27:32'),
(31, '\0\0Tetacid ', 0.00, 6, '2019-11-12 06:27:32'),
(32, '\0\0\0	\0\0\0?\r?\0\0\0\0\0\0\0\0\0d\0\0\0\0\0\0\0\0\0\0????MbP?_\0\0\0?\0\0\0\0\0\0\0\0\0\0%\0\0\0', 0.00, 6, '2019-11-12 06:27:32'),
(33, '\0\0\0\0', 0.00, 6, '2019-11-12 06:27:32'),
(34, '\0\0Ixime', 0.00, 6, '2019-11-12 06:33:44'),
(35, '\0\0Manix', 0.00, 6, '2019-11-12 06:33:44'),
(36, '\0\0Pelox', 0.00, 6, '2019-11-12 06:33:44'),
(37, '\0\0Supracin', 0.00, 6, '2019-11-12 06:33:44'),
(38, '\0\0Tetacid ', 0.00, 6, '2019-11-12 06:33:45'),
(39, '', 0.00, 6, '2019-11-12 06:33:45'),
(40, '\0\0\0\0', 0.00, 6, '2019-11-12 06:33:45'),
(41, '', 0.00, 6, '2019-11-12 06:33:45'),
(42, '', 0.00, 6, '2019-11-12 06:33:45'),
(43, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:45'),
(44, '', 0.00, 6, '2019-11-12 06:33:45'),
(45, '', 0.00, 6, '2019-11-12 06:33:45'),
(46, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:45'),
(47, '', 0.00, 6, '2019-11-12 06:33:45'),
(48, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:45'),
(49, '', 0.00, 6, '2019-11-12 06:33:45'),
(50, '', 0.00, 6, '2019-11-12 06:33:45'),
(51, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:45'),
(52, '', 0.00, 6, '2019-11-12 06:33:45'),
(53, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:45'),
(54, '', 0.00, 6, '2019-11-12 06:33:46'),
(55, '\0\0\0\0\0	\0\0\0~', 0.00, 6, '2019-11-12 06:33:46'),
(56, '', 0.00, 6, '2019-11-12 06:33:46'),
(57, '\0	\0\0\0\0', 0.00, 6, '2019-11-12 06:33:46'),
(58, '\0\0\0~', 0.00, 6, '2019-11-12 06:33:46'),
(59, '', 0.00, 6, '2019-11-12 06:33:46'),
(60, '\0', 0.00, 6, '2019-11-12 06:33:46'),
(61, '\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:46'),
(62, '\0', 0.00, 6, '2019-11-12 06:33:46'),
(63, '', 0.00, 6, '2019-11-12 06:33:46'),
(64, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:46'),
(65, '\0\0\0\0', 0.00, 6, '2019-11-12 06:33:46'),
(66, '', 0.00, 6, '2019-11-12 06:33:46'),
(67, '\0\0\0\0\0\r\0\0\0~', 0.00, 6, '2019-11-12 06:33:46'),
(68, '', 0.00, 6, '2019-11-12 06:33:46'),
(69, '\0\r\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:46'),
(70, '', 0.00, 6, '2019-11-12 06:33:47'),
(71, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:47'),
(72, '', 0.00, 6, '2019-11-12 06:33:47'),
(73, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:47'),
(74, '', 0.00, 6, '2019-11-12 06:33:47'),
(75, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:47'),
(76, '', 0.00, 6, '2019-11-12 06:33:47'),
(77, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:47'),
(78, '\0\0\0\0P', 0.00, 6, '2019-11-12 06:33:47'),
(79, '', 0.00, 6, '2019-11-12 06:33:47'),
(80, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:47'),
(81, '', 0.00, 6, '2019-11-12 06:33:47'),
(82, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:47'),
(83, '', 0.00, 6, '2019-11-12 06:33:47'),
(84, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:47'),
(85, '\0\0\0\0', 0.00, 6, '2019-11-12 06:33:47'),
(86, '', 0.00, 6, '2019-11-12 06:33:47'),
(87, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:47'),
(88, '', 0.00, 6, '2019-11-12 06:33:47'),
(89, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:47'),
(90, '', 0.00, 6, '2019-11-12 06:33:47'),
(91, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:47'),
(92, '', 0.00, 6, '2019-11-12 06:33:48'),
(93, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:48'),
(94, '', 0.00, 6, '2019-11-12 06:33:48'),
(95, '\0\0\0\0\0\Z\0\0\0~', 0.00, 6, '2019-11-12 06:33:48'),
(96, '', 0.00, 6, '2019-11-12 06:33:48'),
(97, '\0\Z\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:48'),
(98, '', 0.00, 6, '2019-11-12 06:33:48'),
(99, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:48'),
(100, '', 0.00, 6, '2019-11-12 06:33:48'),
(101, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:48'),
(102, '', 0.00, 6, '2019-11-12 06:33:48'),
(103, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:48'),
(104, '', 0.00, 6, '2019-11-12 06:33:48'),
(105, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:33:48'),
(106, '', 0.00, 6, '2019-11-12 06:33:48'),
(107, '\0\0\0\0\0 \0\0\0~', 0.00, 6, '2019-11-12 06:33:48'),
(108, '\0\0\0\0#F\0\0\0 \0\0\0\0', 0.00, 6, '2019-11-12 06:33:48'),
(109, '\0 \0\0\0\0!\0\0\0~', 0.00, 6, '2019-11-12 06:33:48'),
(110, '', 0.00, 6, '2019-11-12 06:33:49'),
(111, '\0!\0\0\0\0&quot;\0\0\0~', 0.00, 6, '2019-11-12 06:33:49'),
(112, '', 0.00, 6, '2019-11-12 06:33:49'),
(113, '\0&quot;\0\0\0\0#\0\0\0~', 0.00, 6, '2019-11-12 06:33:49'),
(114, '', 0.00, 6, '2019-11-12 06:33:49'),
(115, '\0#\0\0\0\0$\0\0\0~', 0.00, 6, '2019-11-12 06:33:49'),
(116, '', 0.00, 6, '2019-11-12 06:33:49'),
(117, '\0$\0\0\0\0%\0\0\0~', 0.00, 6, '2019-11-12 06:33:49'),
(118, '', 0.00, 6, '2019-11-12 06:33:49'),
(119, '\0%\0\0\0\0&amp;\0\0\0~', 0.00, 6, '2019-11-12 06:33:49'),
(120, '', 0.00, 6, '2019-11-12 06:33:49'),
(121, '\0&amp;\0\0\0\0\'\0\0\0~', 0.00, 6, '2019-11-12 06:33:50'),
(122, '', 0.00, 6, '2019-11-12 06:33:50'),
(123, '\0\'\0\0\0\0(\0\0\0~', 0.00, 6, '2019-11-12 06:33:50'),
(124, '', 0.00, 6, '2019-11-12 06:33:50'),
(125, '', 0.00, 6, '2019-11-12 06:33:50'),
(126, '\0)\0\0\0\0*\0\0\0~', 0.00, 6, '2019-11-12 06:33:50'),
(127, '', 0.00, 6, '2019-11-12 06:33:50'),
(128, '\0*\0\0\0\0+\0\0\0~', 0.00, 6, '2019-11-12 06:33:50'),
(129, '', 0.00, 6, '2019-11-12 06:33:50'),
(130, '\0+\0\0\0\0', 0.00, 6, '2019-11-12 06:33:50'),
(131, '', 0.00, 6, '2019-11-12 06:33:50'),
(132, '\0', 0.00, 6, '2019-11-12 06:33:50'),
(133, '\0', 0.00, 6, '2019-11-12 06:33:50'),
(134, '\0-\0\0\0\0.\0\0\0~', 0.00, 6, '2019-11-12 06:33:51'),
(135, '', 0.00, 6, '2019-11-12 06:33:51'),
(136, '\0.\0\0\0\0/\0\0\0~', 0.00, 6, '2019-11-12 06:33:51'),
(137, '', 0.00, 6, '2019-11-12 06:33:51'),
(138, '\0/\0\0\0\00\0\0\0~', 0.00, 6, '2019-11-12 06:33:51'),
(139, '', 0.00, 6, '2019-11-12 06:33:51'),
(140, '\00\0\0\0\01\0\0\0~', 0.00, 6, '2019-11-12 06:33:51'),
(141, '', 0.00, 6, '2019-11-12 06:33:51'),
(142, '\01\0\0\0\02\0\0\0~', 0.00, 6, '2019-11-12 06:33:51'),
(143, '', 0.00, 6, '2019-11-12 06:33:51'),
(144, '\02\0\0\0\03\0\0\0~', 0.00, 6, '2019-11-12 06:33:51'),
(145, '', 0.00, 6, '2019-11-12 06:33:51'),
(146, '\03\0\0\0\04\0\0\0~', 0.00, 6, '2019-11-12 06:33:51'),
(147, '\03\0\0\0', 0.00, 6, '2019-11-12 06:33:51'),
(148, '', 0.00, 6, '2019-11-12 06:33:51'),
(149, '\04\0\0\0\05\0\0\0~', 0.00, 6, '2019-11-12 06:33:52'),
(150, '', 0.00, 6, '2019-11-12 06:33:52'),
(151, '\05\0\0\0\06\0\0\0~', 0.00, 6, '2019-11-12 06:33:52'),
(152, '', 0.00, 6, '2019-11-12 06:33:52'),
(153, '\06\0\0\0\07\0\0\0~', 0.00, 6, '2019-11-12 06:33:52'),
(154, '', 0.00, 6, '2019-11-12 06:33:52'),
(155, '\07\0\0\0\08\0\0\0~', 0.00, 6, '2019-11-12 06:33:52'),
(156, '', 0.00, 6, '2019-11-12 06:33:52'),
(157, '', 0.00, 6, '2019-11-12 06:33:52'),
(158, '\09\0\0\0\0:\0\0\0~', 0.00, 6, '2019-11-12 06:33:52'),
(159, '', 0.00, 6, '2019-11-12 06:33:52'),
(160, '\0:\0\0\0\0;\0\0\0~', 0.00, 6, '2019-11-12 06:33:52'),
(161, '', 0.00, 6, '2019-11-12 06:33:52'),
(162, '\0;\0\0\0\0&lt;\0\0\0~', 0.00, 6, '2019-11-12 06:33:52'),
(163, '', 0.00, 6, '2019-11-12 06:33:52'),
(164, '\0&lt;\0\0\0\0=\0\0\0~', 0.00, 6, '2019-11-12 06:33:52'),
(165, '', 0.00, 6, '2019-11-12 06:33:53'),
(166, '\0=\0\0\0\0&gt;\0\0\0~', 0.00, 6, '2019-11-12 06:33:53'),
(167, '', 0.00, 6, '2019-11-12 06:33:53'),
(168, '', 0.00, 6, '2019-11-12 06:33:53'),
(169, '\0?\0\0\0\0@\0\0\0~', 0.00, 6, '2019-11-12 06:33:53'),
(170, '', 0.00, 6, '2019-11-12 06:33:53'),
(171, '\0@\0\0\0\0A\0\0\0~', 0.00, 6, '2019-11-12 06:33:53'),
(172, '', 0.00, 6, '2019-11-12 06:33:53'),
(173, '\0A\0\0\0\0B\0\0\0~', 0.00, 6, '2019-11-12 06:33:53'),
(174, '', 0.00, 6, '2019-11-12 06:33:53'),
(175, '\0B\0\0\0\0C\0\0\0~', 0.00, 6, '2019-11-12 06:33:53'),
(176, '', 0.00, 6, '2019-11-12 06:33:53'),
(177, '\0C\0\0\0\0D\0\0\0~', 0.00, 6, '2019-11-12 06:33:53'),
(178, '', 0.00, 6, '2019-11-12 06:33:53'),
(179, '\0D\0\0\0\0E\0\0\0~', 0.00, 6, '2019-11-12 06:33:53'),
(180, '', 0.00, 6, '2019-11-12 06:33:53'),
(181, '', 0.00, 6, '2019-11-12 06:33:53'),
(182, '\0F\0\0\0\0G\0\0\0~', 0.00, 6, '2019-11-12 06:33:53'),
(183, '', 0.00, 6, '2019-11-12 06:33:53'),
(184, '\0G\0\0\0\0H\0\0\0~', 0.00, 6, '2019-11-12 06:33:54'),
(185, '', 0.00, 6, '2019-11-12 06:33:54'),
(186, '', 0.00, 6, '2019-11-12 06:33:54'),
(187, '\0I\0\0\0\0J\0\0\0~', 0.00, 6, '2019-11-12 06:33:54'),
(188, '', 0.00, 6, '2019-11-12 06:33:54'),
(189, '\0J\0\0\0\0K\0\0\0~', 0.00, 6, '2019-11-12 06:33:54'),
(190, '', 0.00, 6, '2019-11-12 06:33:54'),
(191, '\0K\0\0\0\0L\0\0\0~', 0.00, 6, '2019-11-12 06:33:54'),
(192, '', 0.00, 6, '2019-11-12 06:33:54'),
(193, '\0L\0\0\0\0M\0\0\0~', 0.00, 6, '2019-11-12 06:33:55'),
(194, '', 0.00, 6, '2019-11-12 06:33:55'),
(195, '\0M\0\0\0\0N\0\0\0~', 0.00, 6, '2019-11-12 06:33:55'),
(196, '', 0.00, 6, '2019-11-12 06:33:55'),
(197, '\0N\0\0\0\0O\0\0\0~', 0.00, 6, '2019-11-12 06:33:55'),
(198, '', 0.00, 6, '2019-11-12 06:33:55'),
(199, '\0O\0\0\0\0P\0\0\0~', 0.00, 6, '2019-11-12 06:33:55'),
(200, '', 0.00, 6, '2019-11-12 06:33:55'),
(201, '\0P\0\0\0\0Q\0\0\0~', 0.00, 6, '2019-11-12 06:33:55'),
(202, '', 0.00, 6, '2019-11-12 06:33:56'),
(203, '\0Q\0\0\0\0R\0\0\0~', 0.00, 6, '2019-11-12 06:33:56'),
(204, '', 0.00, 6, '2019-11-12 06:33:56'),
(205, '\0R\0\0\0\0S\0\0\0~', 0.00, 6, '2019-11-12 06:33:56'),
(206, '', 0.00, 6, '2019-11-12 06:33:56'),
(207, '\0S\0\0\0\0T\0\0\0~', 0.00, 6, '2019-11-12 06:33:56'),
(208, '', 0.00, 6, '2019-11-12 06:33:56'),
(209, '\0T\0\0\0\0U\0\0\0~', 0.00, 6, '2019-11-12 06:33:56'),
(210, '', 0.00, 6, '2019-11-12 06:33:56'),
(211, '\0U\0\0\0\0V\0\0\0~', 0.00, 6, '2019-11-12 06:33:56'),
(212, '', 0.00, 6, '2019-11-12 06:33:56'),
(213, '\0V\0\0\0\0W\0\0\0~', 0.00, 6, '2019-11-12 06:33:56'),
(214, '', 0.00, 6, '2019-11-12 06:33:56'),
(215, '\0W\0\0\0\0X\0\0\0~', 0.00, 6, '2019-11-12 06:33:56'),
(216, '', 0.00, 6, '2019-11-12 06:33:56'),
(217, '\0X\0\0\0\0Y\0\0\0~', 0.00, 6, '2019-11-12 06:33:56'),
(218, '', 0.00, 6, '2019-11-12 06:33:56'),
(219, '\0Y\0\0\0\0Z\0\0\0~', 0.00, 6, '2019-11-12 06:33:56'),
(220, '', 0.00, 6, '2019-11-12 06:33:56'),
(221, '\0Z\0\0\0\0[\0\0\0~', 0.00, 6, '2019-11-12 06:33:56'),
(222, '', 0.00, 6, '2019-11-12 06:33:57'),
(223, '\0[\0\0\0\0\\\0\0\0~', 0.00, 6, '2019-11-12 06:33:57'),
(224, '', 0.00, 6, '2019-11-12 06:33:57'),
(225, '\0\\\0\0\0\0]\0\0\0~', 0.00, 6, '2019-11-12 06:33:57'),
(226, '', 0.00, 6, '2019-11-12 06:33:57'),
(227, '\0]\0\0\0\0^\0\0\0~', 0.00, 6, '2019-11-12 06:33:57'),
(228, '', 0.00, 6, '2019-11-12 06:33:57'),
(229, '\0^\0\0\0\0_\0\0\0~', 0.00, 6, '2019-11-12 06:33:57'),
(230, '', 0.00, 6, '2019-11-12 06:33:57'),
(231, '\0_\0\0\0\0`\0\0\0~', 0.00, 6, '2019-11-12 06:33:57'),
(232, '\0_\0\0\0j\r\0\0\0`\0\0\0\0', 0.00, 6, '2019-11-12 06:33:57'),
(233, '\0`\0\0\0\0a\0\0\0~', 0.00, 6, '2019-11-12 06:33:57'),
(234, '', 0.00, 6, '2019-11-12 06:33:57'),
(235, '\0a\0\0\0\0b\0\0\0~', 0.00, 6, '2019-11-12 06:33:57'),
(236, '\0a\0\0\0S', 0.00, 6, '2019-11-12 06:33:58'),
(237, '', 0.00, 6, '2019-11-12 06:33:58'),
(238, '\0b\0\0\0\0c\0\0\0~', 0.00, 6, '2019-11-12 06:33:58'),
(239, '', 0.00, 6, '2019-11-12 06:33:58'),
(240, '\0c\0\0\0\0d\0\0\0~', 0.00, 6, '2019-11-12 06:33:58'),
(241, '', 0.00, 6, '2019-11-12 06:33:58'),
(242, '', 0.00, 6, '2019-11-12 06:33:58'),
(243, '\0e\0\0\0\0f\0\0\0~', 0.00, 6, '2019-11-12 06:33:58'),
(244, '', 0.00, 6, '2019-11-12 06:33:58'),
(245, '\0f\0\0\0\0g\0\0\0~', 0.00, 6, '2019-11-12 06:33:58'),
(246, '', 0.00, 6, '2019-11-12 06:33:58'),
(247, '\0g\0\0\0\0h\0\0\0~', 0.00, 6, '2019-11-12 06:33:58'),
(248, '', 0.00, 6, '2019-11-12 06:33:58'),
(249, '\0h\0\0\0\0i\0\0\0~', 0.00, 6, '2019-11-12 06:33:58'),
(250, '', 0.00, 6, '2019-11-12 06:33:58'),
(251, '\0i\0\0\0\0j\0\0\0~', 0.00, 6, '2019-11-12 06:33:58'),
(252, '', 0.00, 6, '2019-11-12 06:33:58'),
(253, '\0j\0\0\0\0k\0\0\0~', 0.00, 6, '2019-11-12 06:33:58'),
(254, '', 0.00, 6, '2019-11-12 06:33:59'),
(255, '\0k\0\0\0\0l\0\0\0~', 0.00, 6, '2019-11-12 06:33:59'),
(256, '', 0.00, 6, '2019-11-12 06:33:59'),
(257, '\0l\0\0\0\0m\0\0\0~', 0.00, 6, '2019-11-12 06:33:59'),
(258, '', 0.00, 6, '2019-11-12 06:33:59'),
(259, '\0m\0\0\0\0n\0\0\0~', 0.00, 6, '2019-11-12 06:33:59'),
(260, '', 0.00, 6, '2019-11-12 06:33:59'),
(261, '', 0.00, 6, '2019-11-12 06:33:59'),
(262, '\0o\0\0\0\0p\0\0\0~', 0.00, 6, '2019-11-12 06:33:59'),
(263, '', 0.00, 6, '2019-11-12 06:33:59'),
(264, '\0p\0\0\0\0q\0\0\0~', 0.00, 6, '2019-11-12 06:33:59'),
(265, '', 0.00, 6, '2019-11-12 06:33:59'),
(266, '\0q\0\0\0\0r\0\0\0~', 0.00, 6, '2019-11-12 06:33:59'),
(267, '\0q\0\0\0', 0.00, 6, '2019-11-12 06:33:59'),
(268, '', 0.00, 6, '2019-11-12 06:33:59'),
(269, '\0r\0\0\0\0s\0\0\0~', 0.00, 6, '2019-11-12 06:33:59'),
(270, '', 0.00, 6, '2019-11-12 06:33:59'),
(271, '\0s\0\0\0\0t\0\0\0~', 0.00, 6, '2019-11-12 06:33:59'),
(272, '', 0.00, 6, '2019-11-12 06:33:59'),
(273, '\0t\0\0\0\0u\0\0\0~', 0.00, 6, '2019-11-12 06:33:59'),
(274, '', 0.00, 6, '2019-11-12 06:34:00'),
(275, '\0u\0\0\0\0v\0\0\0~', 0.00, 6, '2019-11-12 06:34:00'),
(276, '', 0.00, 6, '2019-11-12 06:34:00'),
(277, '\0v\0\0\0\0w\0\0\0~', 0.00, 6, '2019-11-12 06:34:00'),
(278, '', 0.00, 6, '2019-11-12 06:34:00'),
(279, '\0w\0\0\0\0x\0\0\0~', 0.00, 6, '2019-11-12 06:34:00'),
(280, '', 0.00, 6, '2019-11-12 06:34:00'),
(281, '\0x\0\0\0\0y\0\0\0~', 0.00, 6, '2019-11-12 06:34:00'),
(282, '', 0.00, 6, '2019-11-12 06:34:00'),
(283, '\0y\0\0\0\0z\0\0\0~', 0.00, 6, '2019-11-12 06:34:00'),
(284, '', 0.00, 6, '2019-11-12 06:34:00'),
(285, '', 0.00, 6, '2019-11-12 06:34:00'),
(286, '', 0.00, 6, '2019-11-12 06:34:00'),
(287, '', 0.00, 6, '2019-11-12 06:34:00'),
(288, '\0}\0\0\0\0~\0\0\0~', 0.00, 6, '2019-11-12 06:34:00'),
(289, '', 0.00, 6, '2019-11-12 06:34:00'),
(290, '', 0.00, 6, '2019-11-12 06:34:00'),
(291, '', 0.00, 6, '2019-11-12 06:34:00'),
(292, '', 0.00, 6, '2019-11-12 06:34:00'),
(293, '', 0.00, 6, '2019-11-12 06:34:00'),
(294, '', 0.00, 6, '2019-11-12 06:34:00'),
(295, '', 0.00, 6, '2019-11-12 06:34:01'),
(296, '', 0.00, 6, '2019-11-12 06:34:01'),
(297, '', 0.00, 6, '2019-11-12 06:34:01'),
(298, '', 0.00, 6, '2019-11-12 06:34:01'),
(299, '', 0.00, 6, '2019-11-12 06:34:01'),
(300, '', 0.00, 6, '2019-11-12 06:34:01'),
(301, '', 0.00, 6, '2019-11-12 06:34:01'),
(302, '', 0.00, 6, '2019-11-12 06:34:01'),
(303, '', 0.00, 6, '2019-11-12 06:34:01'),
(304, '', 0.00, 6, '2019-11-12 06:34:02'),
(305, '', 0.00, 6, '2019-11-12 06:34:02'),
(306, '', 0.00, 6, '2019-11-12 06:34:02'),
(307, '', 0.00, 6, '2019-11-12 06:34:02'),
(308, '', 0.00, 6, '2019-11-12 06:34:02'),
(309, '', 0.00, 6, '2019-11-12 06:34:03'),
(310, '', 0.00, 6, '2019-11-12 06:34:03'),
(311, '', 0.00, 6, '2019-11-12 06:34:03'),
(312, '', 0.00, 6, '2019-11-12 06:34:03'),
(313, '', 0.00, 6, '2019-11-12 06:34:03'),
(314, '', 0.00, 6, '2019-11-12 06:34:03'),
(315, '', 0.00, 6, '2019-11-12 06:34:03'),
(316, '', 0.00, 6, '2019-11-12 06:34:03'),
(317, '', 0.00, 6, '2019-11-12 06:34:03'),
(318, '', 0.00, 6, '2019-11-12 06:34:03'),
(319, '', 0.00, 6, '2019-11-12 06:34:03'),
(320, '', 0.00, 6, '2019-11-12 06:34:03'),
(321, '', 0.00, 6, '2019-11-12 06:34:03'),
(322, '', 0.00, 6, '2019-11-12 06:34:03'),
(323, '', 0.00, 6, '2019-11-12 06:34:03'),
(324, '', 0.00, 6, '2019-11-12 06:34:04'),
(325, '', 0.00, 6, '2019-11-12 06:34:04'),
(326, '', 0.00, 6, '2019-11-12 06:34:04'),
(327, '', 0.00, 6, '2019-11-12 06:34:04'),
(328, '', 0.00, 6, '2019-11-12 06:34:04'),
(329, '', 0.00, 6, '2019-11-12 06:34:04'),
(330, '', 0.00, 6, '2019-11-12 06:34:04'),
(331, '', 0.00, 6, '2019-11-12 06:34:04'),
(332, '', 0.00, 6, '2019-11-12 06:34:04'),
(333, '', 0.00, 6, '2019-11-12 06:34:04'),
(334, '', 0.00, 6, '2019-11-12 06:34:04'),
(335, '', 0.00, 6, '2019-11-12 06:34:04'),
(336, '', 0.00, 6, '2019-11-12 06:34:04'),
(337, '', 0.00, 6, '2019-11-12 06:34:04'),
(338, '', 0.00, 6, '2019-11-12 06:34:04'),
(339, '', 0.00, 6, '2019-11-12 06:34:04'),
(340, '', 0.00, 6, '2019-11-12 06:34:04'),
(341, '', 0.00, 6, '2019-11-12 06:34:04'),
(342, '', 0.00, 6, '2019-11-12 06:34:05'),
(343, '', 0.00, 6, '2019-11-12 06:34:05'),
(344, '', 0.00, 6, '2019-11-12 06:34:06'),
(345, '', 0.00, 6, '2019-11-12 06:34:06'),
(346, '', 0.00, 6, '2019-11-12 06:34:06'),
(347, '', 0.00, 6, '2019-11-12 06:34:06'),
(348, '', 0.00, 6, '2019-11-12 06:34:06'),
(349, '', 0.00, 6, '2019-11-12 06:34:06'),
(350, '', 0.00, 6, '2019-11-12 06:34:06'),
(351, '', 0.00, 6, '2019-11-12 06:34:06'),
(352, '', 0.00, 6, '2019-11-12 06:34:06'),
(353, '', 0.00, 6, '2019-11-12 06:34:06'),
(354, '', 0.00, 6, '2019-11-12 06:34:06'),
(355, '', 0.00, 6, '2019-11-12 06:34:06'),
(356, '', 0.00, 6, '2019-11-12 06:34:07'),
(357, '', 0.00, 6, '2019-11-12 06:34:07'),
(358, '', 0.00, 6, '2019-11-12 06:34:07'),
(359, '', 0.00, 6, '2019-11-12 06:34:07'),
(360, '', 0.00, 6, '2019-11-12 06:34:07'),
(361, '', 0.00, 6, '2019-11-12 06:34:07'),
(362, '', 0.00, 6, '2019-11-12 06:34:07'),
(363, '', 0.00, 6, '2019-11-12 06:34:07'),
(364, '', 0.00, 6, '2019-11-12 06:34:07'),
(365, '', 0.00, 6, '2019-11-12 06:34:07'),
(366, '', 0.00, 6, '2019-11-12 06:34:07'),
(367, '', 0.00, 6, '2019-11-12 06:34:07'),
(368, '', 0.00, 6, '2019-11-12 06:34:07'),
(369, '', 0.00, 6, '2019-11-12 06:34:07'),
(370, '', 0.00, 6, '2019-11-12 06:34:08'),
(371, '', 0.00, 6, '2019-11-12 06:34:08'),
(372, '', 0.00, 6, '2019-11-12 06:34:08'),
(373, '', 0.00, 6, '2019-11-12 06:34:08'),
(374, '', 0.00, 6, '2019-11-12 06:34:08'),
(375, '', 0.00, 6, '2019-11-12 06:34:08'),
(376, '', 0.00, 6, '2019-11-12 06:34:08'),
(377, '', 0.00, 6, '2019-11-12 06:34:08'),
(378, '', 0.00, 6, '2019-11-12 06:34:08'),
(379, '', 0.00, 6, '2019-11-12 06:34:08'),
(380, '', 0.00, 6, '2019-11-12 06:34:08'),
(381, '', 0.00, 6, '2019-11-12 06:34:08'),
(382, '', 0.00, 6, '2019-11-12 06:34:08'),
(383, '', 0.00, 6, '2019-11-12 06:34:08'),
(384, '', 0.00, 6, '2019-11-12 06:34:08'),
(385, '', 0.00, 6, '2019-11-12 06:34:08'),
(386, '', 0.00, 6, '2019-11-12 06:34:09'),
(387, '', 0.00, 6, '2019-11-12 06:34:09'),
(388, '', 0.00, 6, '2019-11-12 06:34:09'),
(389, '', 0.00, 6, '2019-11-12 06:34:09'),
(390, '', 0.00, 6, '2019-11-12 06:34:09'),
(391, '', 0.00, 6, '2019-11-12 06:34:09'),
(392, '', 0.00, 6, '2019-11-12 06:34:09'),
(393, '', 0.00, 6, '2019-11-12 06:34:09'),
(394, '', 0.00, 6, '2019-11-12 06:34:09'),
(395, '', 0.00, 6, '2019-11-12 06:34:09'),
(396, '', 0.00, 6, '2019-11-12 06:34:09'),
(397, '', 0.00, 6, '2019-11-12 06:34:09'),
(398, '', 0.00, 6, '2019-11-12 06:34:09'),
(399, '', 0.00, 6, '2019-11-12 06:34:09'),
(400, '', 0.00, 6, '2019-11-12 06:34:09'),
(401, '', 0.00, 6, '2019-11-12 06:34:10'),
(402, '', 0.00, 6, '2019-11-12 06:34:10'),
(403, '', 0.00, 6, '2019-11-12 06:34:10'),
(404, '', 0.00, 6, '2019-11-12 06:34:10'),
(405, '', 0.00, 6, '2019-11-12 06:34:10'),
(406, '', 0.00, 6, '2019-11-12 06:34:10'),
(407, '', 0.00, 6, '2019-11-12 06:34:10'),
(408, '', 0.00, 6, '2019-11-12 06:34:10'),
(409, '', 0.00, 6, '2019-11-12 06:34:10'),
(410, '', 0.00, 6, '2019-11-12 06:34:10'),
(411, '', 0.00, 6, '2019-11-12 06:34:10'),
(412, '', 0.00, 6, '2019-11-12 06:34:10'),
(413, '', 0.00, 6, '2019-11-12 06:34:10'),
(414, '', 0.00, 6, '2019-11-12 06:34:10'),
(415, '', 0.00, 6, '2019-11-12 06:34:10'),
(416, '', 0.00, 6, '2019-11-12 06:34:10'),
(417, '', 0.00, 6, '2019-11-12 06:34:10'),
(418, '', 0.00, 6, '2019-11-12 06:34:10'),
(419, '', 0.00, 6, '2019-11-12 06:34:10'),
(420, '', 0.00, 6, '2019-11-12 06:34:10'),
(421, '', 0.00, 6, '2019-11-12 06:34:10'),
(422, '', 0.00, 6, '2019-11-12 06:34:10'),
(423, '', 0.00, 6, '2019-11-12 06:34:11'),
(424, '', 0.00, 6, '2019-11-12 06:34:11'),
(425, '', 0.00, 6, '2019-11-12 06:34:11'),
(426, '', 0.00, 6, '2019-11-12 06:34:11'),
(427, '', 0.00, 6, '2019-11-12 06:34:11'),
(428, '', 0.00, 6, '2019-11-12 06:34:11'),
(429, '', 0.00, 6, '2019-11-12 06:34:11'),
(430, '', 0.00, 6, '2019-11-12 06:34:11'),
(431, '', 0.00, 6, '2019-11-12 06:34:11'),
(432, '', 0.00, 6, '2019-11-12 06:34:11'),
(433, '', 0.00, 6, '2019-11-12 06:34:11'),
(434, '', 0.00, 6, '2019-11-12 06:34:11'),
(435, '', 0.00, 6, '2019-11-12 06:34:11'),
(436, '', 0.00, 6, '2019-11-12 06:34:11'),
(437, '', 0.00, 6, '2019-11-12 06:34:12'),
(438, '', 0.00, 6, '2019-11-12 06:34:12'),
(439, '', 0.00, 6, '2019-11-12 06:34:12'),
(440, '', 0.00, 6, '2019-11-12 06:34:12'),
(441, '', 0.00, 6, '2019-11-12 06:34:12'),
(442, '', 0.00, 6, '2019-11-12 06:34:12'),
(443, '', 0.00, 6, '2019-11-12 06:34:12'),
(444, '', 0.00, 6, '2019-11-12 06:34:12'),
(445, '', 0.00, 6, '2019-11-12 06:34:12'),
(446, '', 0.00, 6, '2019-11-12 06:34:12'),
(447, '', 0.00, 6, '2019-11-12 06:34:12'),
(448, '', 0.00, 6, '2019-11-12 06:34:12'),
(449, '', 0.00, 6, '2019-11-12 06:34:12'),
(450, '', 0.00, 6, '2019-11-12 06:34:13'),
(451, '', 0.00, 6, '2019-11-12 06:34:13'),
(452, '', 0.00, 6, '2019-11-12 06:34:13'),
(453, '', 0.00, 6, '2019-11-12 06:34:13'),
(454, '', 0.00, 6, '2019-11-12 06:34:13'),
(455, '', 0.00, 6, '2019-11-12 06:34:13'),
(456, '', 0.00, 6, '2019-11-12 06:34:13'),
(457, '', 0.00, 6, '2019-11-12 06:34:13'),
(458, '', 0.00, 6, '2019-11-12 06:34:13'),
(459, '', 0.00, 6, '2019-11-12 06:34:13'),
(460, '', 0.00, 6, '2019-11-12 06:34:13'),
(461, '', 0.00, 6, '2019-11-12 06:34:13'),
(462, '', 0.00, 6, '2019-11-12 06:34:13'),
(463, '', 0.00, 6, '2019-11-12 06:34:13'),
(464, '', 0.00, 6, '2019-11-12 06:34:13'),
(465, '', 0.00, 6, '2019-11-12 06:34:13'),
(466, '', 0.00, 6, '2019-11-12 06:34:13'),
(467, '', 0.00, 6, '2019-11-12 06:34:13'),
(468, '', 0.00, 6, '2019-11-12 06:34:13'),
(469, '', 0.00, 6, '2019-11-12 06:34:13'),
(470, '', 0.00, 6, '2019-11-12 06:34:14'),
(471, '', 0.00, 6, '2019-11-12 06:34:14'),
(472, '', 0.00, 6, '2019-11-12 06:34:14'),
(473, '', 0.00, 6, '2019-11-12 06:34:14'),
(474, '', 0.00, 6, '2019-11-12 06:34:14'),
(475, '', 0.00, 6, '2019-11-12 06:34:14'),
(476, '', 0.00, 6, '2019-11-12 06:34:14'),
(477, '', 0.00, 6, '2019-11-12 06:34:14'),
(478, '', 0.00, 6, '2019-11-12 06:34:14'),
(479, '', 0.00, 6, '2019-11-12 06:34:14'),
(480, '', 0.00, 6, '2019-11-12 06:34:14'),
(481, '', 0.00, 6, '2019-11-12 06:34:14'),
(482, '', 0.00, 6, '2019-11-12 06:34:14'),
(483, '', 0.00, 6, '2019-11-12 06:34:14'),
(484, '', 0.00, 6, '2019-11-12 06:34:14'),
(485, '', 0.00, 6, '2019-11-12 06:34:14'),
(486, '', 0.00, 6, '2019-11-12 06:34:14'),
(487, '', 0.00, 6, '2019-11-12 06:34:14'),
(488, '', 0.00, 6, '2019-11-12 06:34:14'),
(489, '', 0.00, 6, '2019-11-12 06:34:14'),
(490, '', 0.00, 6, '2019-11-12 06:34:14'),
(491, '', 0.00, 6, '2019-11-12 06:34:14'),
(492, '', 0.00, 6, '2019-11-12 06:34:14'),
(493, '', 0.00, 6, '2019-11-12 06:34:14'),
(494, '', 0.00, 6, '2019-11-12 06:34:15'),
(495, '', 0.00, 6, '2019-11-12 06:34:15'),
(496, '', 0.00, 6, '2019-11-12 06:34:15'),
(497, '', 0.00, 6, '2019-11-12 06:34:15'),
(498, '', 0.00, 6, '2019-11-12 06:34:15'),
(499, '', 0.00, 6, '2019-11-12 06:34:15'),
(500, '', 0.00, 6, '2019-11-12 06:34:15'),
(501, '', 0.00, 6, '2019-11-12 06:34:15'),
(502, '', 0.00, 6, '2019-11-12 06:34:15'),
(503, '', 0.00, 6, '2019-11-12 06:34:16'),
(504, '', 0.00, 6, '2019-11-12 06:34:16'),
(505, '', 0.00, 6, '2019-11-12 06:34:16'),
(506, '', 0.00, 6, '2019-11-12 06:34:17'),
(507, '', 0.00, 6, '2019-11-12 06:34:17'),
(508, '', 0.00, 6, '2019-11-12 06:34:17'),
(509, '', 0.00, 6, '2019-11-12 06:34:17'),
(510, '', 0.00, 6, '2019-11-12 06:34:17'),
(511, '', 0.00, 6, '2019-11-12 06:34:17'),
(512, '', 0.00, 6, '2019-11-12 06:34:17'),
(513, '', 0.00, 6, '2019-11-12 06:34:17'),
(514, '', 0.00, 6, '2019-11-12 06:34:17'),
(515, '', 0.00, 6, '2019-11-12 06:34:17'),
(516, '', 0.00, 6, '2019-11-12 06:34:17'),
(517, '', 0.00, 6, '2019-11-12 06:34:17'),
(518, '', 0.00, 6, '2019-11-12 06:34:17'),
(519, '', 0.00, 6, '2019-11-12 06:34:17'),
(520, '', 0.00, 6, '2019-11-12 06:34:18'),
(521, '', 0.00, 6, '2019-11-12 06:34:18'),
(522, '', 0.00, 6, '2019-11-12 06:34:18'),
(523, '', 0.00, 6, '2019-11-12 06:34:18'),
(524, '', 0.00, 6, '2019-11-12 06:34:18'),
(525, '', 0.00, 6, '2019-11-12 06:34:18'),
(526, '', 0.00, 6, '2019-11-12 06:34:18'),
(527, '', 0.00, 6, '2019-11-12 06:34:18'),
(528, '', 0.00, 6, '2019-11-12 06:34:18'),
(529, '', 0.00, 6, '2019-11-12 06:34:18'),
(530, '', 0.00, 6, '2019-11-12 06:34:18'),
(531, '', 0.00, 6, '2019-11-12 06:34:19'),
(532, '', 0.00, 6, '2019-11-12 06:34:19'),
(533, '', 0.00, 6, '2019-11-12 06:34:19'),
(534, '', 0.00, 6, '2019-11-12 06:34:19'),
(535, '', 0.00, 6, '2019-11-12 06:34:19'),
(536, '', 0.00, 6, '2019-11-12 06:34:19'),
(537, '', 0.00, 6, '2019-11-12 06:34:19'),
(538, '', 0.00, 6, '2019-11-12 06:34:19'),
(539, '', 0.00, 6, '2019-11-12 06:34:19'),
(540, '', 0.00, 6, '2019-11-12 06:34:19'),
(541, '', 0.00, 6, '2019-11-12 06:34:19'),
(542, '', 0.00, 6, '2019-11-12 06:34:19'),
(543, '', 0.00, 6, '2019-11-12 06:34:19'),
(544, '\0\0\0', 0.00, 6, '2019-11-12 06:34:19'),
(545, '\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:19'),
(546, '', 0.00, 6, '2019-11-12 06:34:19'),
(547, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:19'),
(548, '', 0.00, 6, '2019-11-12 06:34:19'),
(549, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:19'),
(550, '\0\0\0', 0.00, 6, '2019-11-12 06:34:19'),
(551, '', 0.00, 6, '2019-11-12 06:34:19'),
(552, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:19'),
(553, '', 0.00, 6, '2019-11-12 06:34:20'),
(554, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:20'),
(555, '', 0.00, 6, '2019-11-12 06:34:20'),
(556, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:20'),
(557, '', 0.00, 6, '2019-11-12 06:34:20'),
(558, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:20'),
(559, '', 0.00, 6, '2019-11-12 06:34:20'),
(560, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:20'),
(561, '', 0.00, 6, '2019-11-12 06:34:20'),
(562, '\0\0\0\0	\0\0~', 0.00, 6, '2019-11-12 06:34:20'),
(563, '', 0.00, 6, '2019-11-12 06:34:20'),
(564, '\0	\0\0\0', 0.00, 6, '2019-11-12 06:34:20'),
(565, '\0\0~', 0.00, 6, '2019-11-12 06:34:20'),
(566, '', 0.00, 6, '2019-11-12 06:34:20'),
(567, '\0', 0.00, 6, '2019-11-12 06:34:20'),
(568, '\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:20'),
(569, '\0', 0.00, 6, '2019-11-12 06:34:21'),
(570, '', 0.00, 6, '2019-11-12 06:34:21'),
(571, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:21'),
(572, '', 0.00, 6, '2019-11-12 06:34:21'),
(573, '\0\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:34:21'),
(574, '', 0.00, 6, '2019-11-12 06:34:21'),
(575, '\0\r\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:21'),
(576, '', 0.00, 6, '2019-11-12 06:34:21'),
(577, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:21'),
(578, '', 0.00, 6, '2019-11-12 06:34:21'),
(579, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:21'),
(580, '', 0.00, 6, '2019-11-12 06:34:21'),
(581, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:21'),
(582, '', 0.00, 6, '2019-11-12 06:34:21'),
(583, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:21'),
(584, '', 0.00, 6, '2019-11-12 06:34:21'),
(585, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:21'),
(586, '', 0.00, 6, '2019-11-12 06:34:21'),
(587, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:21'),
(588, '', 0.00, 6, '2019-11-12 06:34:21'),
(589, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:22'),
(590, '\0\0\0R', 0.00, 6, '2019-11-12 06:34:22'),
(591, '', 0.00, 6, '2019-11-12 06:34:22'),
(592, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:22'),
(593, '', 0.00, 6, '2019-11-12 06:34:22'),
(594, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:22'),
(595, '', 0.00, 6, '2019-11-12 06:34:22'),
(596, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:22'),
(597, '', 0.00, 6, '2019-11-12 06:34:22'),
(598, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:22'),
(599, '', 0.00, 6, '2019-11-12 06:34:22'),
(600, '\0\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:34:22'),
(601, '', 0.00, 6, '2019-11-12 06:34:22'),
(602, '\0\Z\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:22'),
(603, '\0\Z\0\0R', 0.00, 6, '2019-11-12 06:34:22'),
(604, '', 0.00, 6, '2019-11-12 06:34:22'),
(605, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:22'),
(606, '', 0.00, 6, '2019-11-12 06:34:22'),
(607, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:22'),
(608, '', 0.00, 6, '2019-11-12 06:34:22'),
(609, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:23'),
(610, '', 0.00, 6, '2019-11-12 06:34:23'),
(611, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:23'),
(612, '', 0.00, 6, '2019-11-12 06:34:23'),
(613, '\0\0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:34:23'),
(614, '\0\0\0#\0\0\0 \0\0\0', 0.00, 6, '2019-11-12 06:34:23'),
(615, '\0 \0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:34:23'),
(616, '', 0.00, 6, '2019-11-12 06:34:23'),
(617, '', 0.00, 6, '2019-11-12 06:34:23'),
(618, '\0&quot;\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:34:23'),
(619, '', 0.00, 6, '2019-11-12 06:34:23'),
(620, '\0#\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:34:24'),
(621, '\0#\0\0', 0.00, 6, '2019-11-12 06:34:24'),
(622, '', 0.00, 6, '2019-11-12 06:34:24'),
(623, '\0$\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:34:24'),
(624, '', 0.00, 6, '2019-11-12 06:34:24'),
(625, '', 0.00, 6, '2019-11-12 06:34:24'),
(626, '\0&amp;\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:34:24'),
(627, '', 0.00, 6, '2019-11-12 06:34:24'),
(628, '\0\'\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:34:24'),
(629, '', 0.00, 6, '2019-11-12 06:34:24'),
(630, '\0(\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:34:24'),
(631, '', 0.00, 6, '2019-11-12 06:34:24'),
(632, '\0)\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:34:24'),
(633, '', 0.00, 6, '2019-11-12 06:34:24'),
(634, '\0*\0\0\0+\0\0~', 0.00, 6, '2019-11-12 06:34:25'),
(635, '', 0.00, 6, '2019-11-12 06:34:25'),
(636, '\0+\0\0\0', 0.00, 6, '2019-11-12 06:34:25'),
(637, '', 0.00, 6, '2019-11-12 06:34:25'),
(638, '\0', 0.00, 6, '2019-11-12 06:34:25'),
(639, '\0', 0.00, 6, '2019-11-12 06:34:25'),
(640, '\0-\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:34:25'),
(641, '', 0.00, 6, '2019-11-12 06:34:25'),
(642, '\0.\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:34:25'),
(643, '', 0.00, 6, '2019-11-12 06:34:25'),
(644, '\0/\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:34:25'),
(645, '', 0.00, 6, '2019-11-12 06:34:25'),
(646, '', 0.00, 6, '2019-11-12 06:34:25'),
(647, '\01\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:34:25'),
(648, '', 0.00, 6, '2019-11-12 06:34:25'),
(649, '\02\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:34:25'),
(650, '', 0.00, 6, '2019-11-12 06:34:25'),
(651, '\03\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:34:25'),
(652, '', 0.00, 6, '2019-11-12 06:34:25'),
(653, '\04\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:34:26'),
(654, '', 0.00, 6, '2019-11-12 06:34:26'),
(655, '\05\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:34:26'),
(656, '', 0.00, 6, '2019-11-12 06:34:26'),
(657, '\06\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:34:26'),
(658, '', 0.00, 6, '2019-11-12 06:34:26'),
(659, '\07\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:34:26'),
(660, '', 0.00, 6, '2019-11-12 06:34:26'),
(661, '\08\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:34:26'),
(662, '', 0.00, 6, '2019-11-12 06:34:26'),
(663, '\09\0\0\0:\0\0~', 0.00, 6, '2019-11-12 06:34:26'),
(664, '', 0.00, 6, '2019-11-12 06:34:26'),
(665, '\0:\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:34:27'),
(666, '', 0.00, 6, '2019-11-12 06:34:27'),
(667, '\0;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:34:27'),
(668, '', 0.00, 6, '2019-11-12 06:34:27'),
(669, '\0&lt;\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:34:27'),
(670, '', 0.00, 6, '2019-11-12 06:34:27'),
(671, '\0=\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:34:27'),
(672, '', 0.00, 6, '2019-11-12 06:34:27'),
(673, '\0&gt;\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:34:27'),
(674, '', 0.00, 6, '2019-11-12 06:34:27'),
(675, '\0?\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:34:27'),
(676, '\0?\0\02\0\0\0@\0\0\0', 0.00, 6, '2019-11-12 06:34:27'),
(677, '\0@\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:34:28'),
(678, '', 0.00, 6, '2019-11-12 06:34:28'),
(679, '\0A\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:34:28'),
(680, '', 0.00, 6, '2019-11-12 06:34:28'),
(681, '\0B\0\0\0C\0\0~', 0.00, 6, '2019-11-12 06:34:28'),
(682, '\0B\0\0R', 0.00, 6, '2019-11-12 06:34:29'),
(683, '', 0.00, 6, '2019-11-12 06:34:29'),
(684, '\0C\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:34:29'),
(685, '', 0.00, 6, '2019-11-12 06:34:29'),
(686, '\0D\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:34:29'),
(687, '', 0.00, 6, '2019-11-12 06:34:29'),
(688, '\0E\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:34:29'),
(689, '', 0.00, 6, '2019-11-12 06:34:29'),
(690, '\0F\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:34:29'),
(691, '', 0.00, 6, '2019-11-12 06:34:29'),
(692, '\0G\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:34:29'),
(693, '', 0.00, 6, '2019-11-12 06:34:29'),
(694, '\0H\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:34:29'),
(695, '', 0.00, 6, '2019-11-12 06:34:29'),
(696, '\0I\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:34:29'),
(697, '', 0.00, 6, '2019-11-12 06:34:29'),
(698, '\0J\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:34:29'),
(699, '', 0.00, 6, '2019-11-12 06:34:29'),
(700, '\0K\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:34:29'),
(701, '\0K\0\0S', 0.00, 6, '2019-11-12 06:34:30'),
(702, '', 0.00, 6, '2019-11-12 06:34:30'),
(703, '\0L\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:34:31'),
(704, '', 0.00, 6, '2019-11-12 06:34:31'),
(705, '\0M\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:34:31'),
(706, '', 0.00, 6, '2019-11-12 06:34:31'),
(707, '\0N\0\0\0O\0\0~', 0.00, 6, '2019-11-12 06:34:31'),
(708, '', 0.00, 6, '2019-11-12 06:34:31'),
(709, '\0O\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:34:32'),
(710, '', 0.00, 6, '2019-11-12 06:34:32'),
(711, '\0P\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:34:32'),
(712, '\0P\0\0', 0.00, 6, '2019-11-12 06:34:32'),
(713, '', 0.00, 6, '2019-11-12 06:34:32'),
(714, '\0Q\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:34:32'),
(715, '', 0.00, 6, '2019-11-12 06:34:32'),
(716, '\0R\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:34:32'),
(717, '', 0.00, 6, '2019-11-12 06:34:32'),
(718, '\0S\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:34:33'),
(719, '', 0.00, 6, '2019-11-12 06:34:33'),
(720, '\0T\0\0\0U\0\0~', 0.00, 6, '2019-11-12 06:34:33'),
(721, '', 0.00, 6, '2019-11-12 06:34:33'),
(722, '\0U\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:34:33'),
(723, '', 0.00, 6, '2019-11-12 06:34:33'),
(724, '\0V\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:34:33'),
(725, '', 0.00, 6, '2019-11-12 06:34:33'),
(726, '\0W\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:34:33'),
(727, '', 0.00, 6, '2019-11-12 06:34:33'),
(728, '\0X\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:34:33'),
(729, '', 0.00, 6, '2019-11-12 06:34:33'),
(730, '\0Y\0\0\0Z\0\0~', 0.00, 6, '2019-11-12 06:34:33'),
(731, '', 0.00, 6, '2019-11-12 06:34:33'),
(732, '\0Z\0\0\0[\0\0~', 0.00, 6, '2019-11-12 06:34:34'),
(733, '', 0.00, 6, '2019-11-12 06:34:34'),
(734, '\0[\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:34:34'),
(735, '', 0.00, 6, '2019-11-12 06:34:34'),
(736, '\0\\\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:34:34'),
(737, '', 0.00, 6, '2019-11-12 06:34:34'),
(738, '\0]\0\0\0^\0\0~', 0.00, 6, '2019-11-12 06:34:34'),
(739, '', 0.00, 6, '2019-11-12 06:34:34'),
(740, '\0^\0\0\0_\0\0~', 0.00, 6, '2019-11-12 06:34:34'),
(741, '', 0.00, 6, '2019-11-12 06:34:34'),
(742, '\0_\0\0\0`\0\0\0_\0\0fffff&amp;r@\0`\0\0\0', 0.00, 6, '2019-11-12 06:34:34'),
(743, '\0`\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:34:34'),
(744, '', 0.00, 6, '2019-11-12 06:34:34'),
(745, '\0a\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:34:34'),
(746, '', 0.00, 6, '2019-11-12 06:34:34'),
(747, '\0b\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:34:34'),
(748, '', 0.00, 6, '2019-11-12 06:34:34'),
(749, '\0c\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:34:34'),
(750, '', 0.00, 6, '2019-11-12 06:34:35'),
(751, '\0d\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:34:35'),
(752, '', 0.00, 6, '2019-11-12 06:34:35'),
(753, '\0e\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:34:35'),
(754, '', 0.00, 6, '2019-11-12 06:34:35'),
(755, '\0f\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:34:35'),
(756, '', 0.00, 6, '2019-11-12 06:34:35'),
(757, '\0g\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:34:35'),
(758, '', 0.00, 6, '2019-11-12 06:34:35'),
(759, '\0h\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:34:35'),
(760, '', 0.00, 6, '2019-11-12 06:34:35'),
(761, '\0i\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:34:35'),
(762, '', 0.00, 6, '2019-11-12 06:34:35'),
(763, '\0j\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:34:36'),
(764, '', 0.00, 6, '2019-11-12 06:34:36'),
(765, '\0k\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:34:36'),
(766, '', 0.00, 6, '2019-11-12 06:34:36'),
(767, '\0l\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:34:36'),
(768, '', 0.00, 6, '2019-11-12 06:34:36'),
(769, '\0m\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:34:36'),
(770, '', 0.00, 6, '2019-11-12 06:34:36'),
(771, '\0n\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:34:36'),
(772, '', 0.00, 6, '2019-11-12 06:34:37'),
(773, '\0o\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:34:37'),
(774, '', 0.00, 6, '2019-11-12 06:34:37'),
(775, '\0p\0\0\0q\0\0~', 0.00, 6, '2019-11-12 06:34:37'),
(776, '\0p\0\0R', 0.00, 6, '2019-11-12 06:34:37'),
(777, '', 0.00, 6, '2019-11-12 06:34:38'),
(778, '\0q\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:34:38'),
(779, '', 0.00, 6, '2019-11-12 06:34:38'),
(780, '\0r\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:34:38'),
(781, '', 0.00, 6, '2019-11-12 06:34:38'),
(782, '\0s\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:34:38'),
(783, '', 0.00, 6, '2019-11-12 06:34:38'),
(784, '\0t\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:34:38'),
(785, '', 0.00, 6, '2019-11-12 06:34:38'),
(786, '\0u\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:34:38'),
(787, '', 0.00, 6, '2019-11-12 06:34:38'),
(788, '\0v\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:34:38'),
(789, '', 0.00, 6, '2019-11-12 06:34:38'),
(790, '\0w\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:34:38'),
(791, '\0w\0\0', 0.00, 6, '2019-11-12 06:34:38'),
(792, '', 0.00, 6, '2019-11-12 06:34:38'),
(793, '\0x\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:34:39'),
(794, '', 0.00, 6, '2019-11-12 06:34:39'),
(795, '\0y\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:34:39'),
(796, '', 0.00, 6, '2019-11-12 06:34:39'),
(797, '\0z\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:34:39'),
(798, '', 0.00, 6, '2019-11-12 06:34:39'),
(799, '\0{\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:34:39'),
(800, '', 0.00, 6, '2019-11-12 06:34:39'),
(801, '\0|\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:34:39'),
(802, '', 0.00, 6, '2019-11-12 06:34:40'),
(803, '\0}\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:34:40'),
(804, '', 0.00, 6, '2019-11-12 06:34:40'),
(805, '\0~\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:40'),
(806, '', 0.00, 6, '2019-11-12 06:34:40'),
(807, '', 0.00, 6, '2019-11-12 06:34:40'),
(808, '', 0.00, 6, '2019-11-12 06:34:40'),
(809, '', 0.00, 6, '2019-11-12 06:34:40'),
(810, '', 0.00, 6, '2019-11-12 06:34:40'),
(811, '', 0.00, 6, '2019-11-12 06:34:40'),
(812, '', 0.00, 6, '2019-11-12 06:34:40'),
(813, '', 0.00, 6, '2019-11-12 06:34:40'),
(814, '', 0.00, 6, '2019-11-12 06:34:40'),
(815, '', 0.00, 6, '2019-11-12 06:34:40'),
(816, '', 0.00, 6, '2019-11-12 06:34:40'),
(817, '', 0.00, 6, '2019-11-12 06:34:40'),
(818, '', 0.00, 6, '2019-11-12 06:34:40'),
(819, '', 0.00, 6, '2019-11-12 06:34:41'),
(820, '', 0.00, 6, '2019-11-12 06:34:41'),
(821, '', 0.00, 6, '2019-11-12 06:34:41'),
(822, '', 0.00, 6, '2019-11-12 06:34:41'),
(823, '', 0.00, 6, '2019-11-12 06:34:41'),
(824, '', 0.00, 6, '2019-11-12 06:34:41'),
(825, '', 0.00, 6, '2019-11-12 06:34:41'),
(826, '', 0.00, 6, '2019-11-12 06:34:41'),
(827, '', 0.00, 6, '2019-11-12 06:34:41'),
(828, '', 0.00, 6, '2019-11-12 06:34:41'),
(829, '', 0.00, 6, '2019-11-12 06:34:41'),
(830, '', 0.00, 6, '2019-11-12 06:34:41'),
(831, '', 0.00, 6, '2019-11-12 06:34:41'),
(832, '', 0.00, 6, '2019-11-12 06:34:41'),
(833, '', 0.00, 6, '2019-11-12 06:34:41'),
(834, '', 0.00, 6, '2019-11-12 06:34:41'),
(835, '', 0.00, 6, '2019-11-12 06:34:42'),
(836, '', 0.00, 6, '2019-11-12 06:34:42'),
(837, '', 0.00, 6, '2019-11-12 06:34:42'),
(838, '', 0.00, 6, '2019-11-12 06:34:42'),
(839, '', 0.00, 6, '2019-11-12 06:34:42'),
(840, '', 0.00, 6, '2019-11-12 06:34:42'),
(841, '', 0.00, 6, '2019-11-12 06:34:43'),
(842, '', 0.00, 6, '2019-11-12 06:34:43'),
(843, '', 0.00, 6, '2019-11-12 06:34:43'),
(844, '', 0.00, 6, '2019-11-12 06:34:43'),
(845, '', 0.00, 6, '2019-11-12 06:34:43'),
(846, '', 0.00, 6, '2019-11-12 06:34:43'),
(847, '', 0.00, 6, '2019-11-12 06:34:43'),
(848, '', 0.00, 6, '2019-11-12 06:34:43'),
(849, '', 0.00, 6, '2019-11-12 06:34:43'),
(850, '', 0.00, 6, '2019-11-12 06:34:43'),
(851, '', 0.00, 6, '2019-11-12 06:34:43'),
(852, '', 0.00, 6, '2019-11-12 06:34:43'),
(853, '', 0.00, 6, '2019-11-12 06:34:43'),
(854, '', 0.00, 6, '2019-11-12 06:34:43'),
(855, '', 0.00, 6, '2019-11-12 06:34:43'),
(856, '', 0.00, 6, '2019-11-12 06:34:43'),
(857, '', 0.00, 6, '2019-11-12 06:34:43'),
(858, '', 0.00, 6, '2019-11-12 06:34:43'),
(859, '', 0.00, 6, '2019-11-12 06:34:43'),
(860, '', 0.00, 6, '2019-11-12 06:34:43'),
(861, '', 0.00, 6, '2019-11-12 06:34:43'),
(862, '', 0.00, 6, '2019-11-12 06:34:44'),
(863, '', 0.00, 6, '2019-11-12 06:34:44'),
(864, '', 0.00, 6, '2019-11-12 06:34:44'),
(865, '', 0.00, 6, '2019-11-12 06:34:44'),
(866, '', 0.00, 6, '2019-11-12 06:34:44'),
(867, '', 0.00, 6, '2019-11-12 06:34:44'),
(868, '', 0.00, 6, '2019-11-12 06:34:44'),
(869, '', 0.00, 6, '2019-11-12 06:34:44'),
(870, '', 0.00, 6, '2019-11-12 06:34:44'),
(871, '', 0.00, 6, '2019-11-12 06:34:44'),
(872, '', 0.00, 6, '2019-11-12 06:34:44'),
(873, '', 0.00, 6, '2019-11-12 06:34:44'),
(874, '', 0.00, 6, '2019-11-12 06:34:44'),
(875, '', 0.00, 6, '2019-11-12 06:34:44'),
(876, '', 0.00, 6, '2019-11-12 06:34:44'),
(877, '', 0.00, 6, '2019-11-12 06:34:44'),
(878, '', 0.00, 6, '2019-11-12 06:34:44'),
(879, '', 0.00, 6, '2019-11-12 06:34:44'),
(880, '', 0.00, 6, '2019-11-12 06:34:44'),
(881, '', 0.00, 6, '2019-11-12 06:34:44'),
(882, '', 0.00, 6, '2019-11-12 06:34:45'),
(883, '', 0.00, 6, '2019-11-12 06:34:45'),
(884, '', 0.00, 6, '2019-11-12 06:34:45'),
(885, '', 0.00, 6, '2019-11-12 06:34:45'),
(886, '', 0.00, 6, '2019-11-12 06:34:45'),
(887, '', 0.00, 6, '2019-11-12 06:34:45'),
(888, '', 0.00, 6, '2019-11-12 06:34:45'),
(889, '', 0.00, 6, '2019-11-12 06:34:45'),
(890, '', 0.00, 6, '2019-11-12 06:34:45'),
(891, '', 0.00, 6, '2019-11-12 06:34:45'),
(892, '', 0.00, 6, '2019-11-12 06:34:45'),
(893, '', 0.00, 6, '2019-11-12 06:34:45'),
(894, '', 0.00, 6, '2019-11-12 06:34:45'),
(895, '', 0.00, 6, '2019-11-12 06:34:46'),
(896, '', 0.00, 6, '2019-11-12 06:34:46'),
(897, '', 0.00, 6, '2019-11-12 06:34:46'),
(898, '', 0.00, 6, '2019-11-12 06:34:46'),
(899, '', 0.00, 6, '2019-11-12 06:34:46'),
(900, '', 0.00, 6, '2019-11-12 06:34:46'),
(901, '', 0.00, 6, '2019-11-12 06:34:46'),
(902, '', 0.00, 6, '2019-11-12 06:34:46'),
(903, '', 0.00, 6, '2019-11-12 06:34:46'),
(904, '', 0.00, 6, '2019-11-12 06:34:46'),
(905, '', 0.00, 6, '2019-11-12 06:34:46'),
(906, '', 0.00, 6, '2019-11-12 06:34:46'),
(907, '', 0.00, 6, '2019-11-12 06:34:46'),
(908, '', 0.00, 6, '2019-11-12 06:34:46'),
(909, '', 0.00, 6, '2019-11-12 06:34:46'),
(910, '', 0.00, 6, '2019-11-12 06:34:46'),
(911, '', 0.00, 6, '2019-11-12 06:34:46'),
(912, '', 0.00, 6, '2019-11-12 06:34:46'),
(913, '', 0.00, 6, '2019-11-12 06:34:47'),
(914, '', 0.00, 6, '2019-11-12 06:34:47'),
(915, '', 0.00, 6, '2019-11-12 06:34:47'),
(916, '', 0.00, 6, '2019-11-12 06:34:47'),
(917, '', 0.00, 6, '2019-11-12 06:34:47'),
(918, '', 0.00, 6, '2019-11-12 06:34:47'),
(919, '', 0.00, 6, '2019-11-12 06:34:47'),
(920, '', 0.00, 6, '2019-11-12 06:34:47'),
(921, '', 0.00, 6, '2019-11-12 06:34:47'),
(922, '', 0.00, 6, '2019-11-12 06:34:47'),
(923, '', 0.00, 6, '2019-11-12 06:34:47'),
(924, '', 0.00, 6, '2019-11-12 06:34:47'),
(925, '', 0.00, 6, '2019-11-12 06:34:47'),
(926, '', 0.00, 6, '2019-11-12 06:34:47'),
(927, '', 0.00, 6, '2019-11-12 06:34:47'),
(928, '', 0.00, 6, '2019-11-12 06:34:47'),
(929, '', 0.00, 6, '2019-11-12 06:34:47'),
(930, '', 0.00, 6, '2019-11-12 06:34:47'),
(931, '', 0.00, 6, '2019-11-12 06:34:48'),
(932, '', 0.00, 6, '2019-11-12 06:34:48'),
(933, '', 0.00, 6, '2019-11-12 06:34:48'),
(934, '', 0.00, 6, '2019-11-12 06:34:48'),
(935, '', 0.00, 6, '2019-11-12 06:34:48'),
(936, '', 0.00, 6, '2019-11-12 06:34:48'),
(937, '', 0.00, 6, '2019-11-12 06:34:48'),
(938, '', 0.00, 6, '2019-11-12 06:34:48'),
(939, '', 0.00, 6, '2019-11-12 06:34:48'),
(940, '', 0.00, 6, '2019-11-12 06:34:48'),
(941, '', 0.00, 6, '2019-11-12 06:34:48'),
(942, '', 0.00, 6, '2019-11-12 06:34:48'),
(943, '', 0.00, 6, '2019-11-12 06:34:48'),
(944, '', 0.00, 6, '2019-11-12 06:34:48'),
(945, '', 0.00, 6, '2019-11-12 06:34:49'),
(946, '', 0.00, 6, '2019-11-12 06:34:49'),
(947, '', 0.00, 6, '2019-11-12 06:34:49'),
(948, '', 0.00, 6, '2019-11-12 06:34:49'),
(949, '', 0.00, 6, '2019-11-12 06:34:49'),
(950, '', 0.00, 6, '2019-11-12 06:34:49'),
(951, '', 0.00, 6, '2019-11-12 06:34:49'),
(952, '', 0.00, 6, '2019-11-12 06:34:49'),
(953, '', 0.00, 6, '2019-11-12 06:34:49'),
(954, '', 0.00, 6, '2019-11-12 06:34:49'),
(955, '', 0.00, 6, '2019-11-12 06:34:49'),
(956, '', 0.00, 6, '2019-11-12 06:34:49'),
(957, '', 0.00, 6, '2019-11-12 06:34:49'),
(958, '', 0.00, 6, '2019-11-12 06:34:49'),
(959, '', 0.00, 6, '2019-11-12 06:34:49'),
(960, '', 0.00, 6, '2019-11-12 06:34:50'),
(961, '', 0.00, 6, '2019-11-12 06:34:50'),
(962, '', 0.00, 6, '2019-11-12 06:34:50'),
(963, '', 0.00, 6, '2019-11-12 06:34:50'),
(964, '', 0.00, 6, '2019-11-12 06:34:50'),
(965, '', 0.00, 6, '2019-11-12 06:34:50'),
(966, '', 0.00, 6, '2019-11-12 06:34:50'),
(967, '', 0.00, 6, '2019-11-12 06:34:50'),
(968, '', 0.00, 6, '2019-11-12 06:34:50'),
(969, '', 0.00, 6, '2019-11-12 06:34:50'),
(970, '', 0.00, 6, '2019-11-12 06:34:50'),
(971, '', 0.00, 6, '2019-11-12 06:34:50'),
(972, '', 0.00, 6, '2019-11-12 06:34:50'),
(973, '', 0.00, 6, '2019-11-12 06:34:50'),
(974, '', 0.00, 6, '2019-11-12 06:34:51'),
(975, '', 0.00, 6, '2019-11-12 06:34:51'),
(976, '', 0.00, 6, '2019-11-12 06:34:51'),
(977, '', 0.00, 6, '2019-11-12 06:34:51'),
(978, '', 0.00, 6, '2019-11-12 06:34:51'),
(979, '', 0.00, 6, '2019-11-12 06:34:51'),
(980, '', 0.00, 6, '2019-11-12 06:34:51'),
(981, '', 0.00, 6, '2019-11-12 06:34:51'),
(982, '', 0.00, 6, '2019-11-12 06:34:51'),
(983, '', 0.00, 6, '2019-11-12 06:34:51'),
(984, '', 0.00, 6, '2019-11-12 06:34:51'),
(985, '', 0.00, 6, '2019-11-12 06:34:51'),
(986, '', 0.00, 6, '2019-11-12 06:34:51'),
(987, '', 0.00, 6, '2019-11-12 06:34:51'),
(988, '', 0.00, 6, '2019-11-12 06:34:51'),
(989, '', 0.00, 6, '2019-11-12 06:34:51'),
(990, '', 0.00, 6, '2019-11-12 06:34:52'),
(991, '', 0.00, 6, '2019-11-12 06:34:52'),
(992, '', 0.00, 6, '2019-11-12 06:34:52'),
(993, '', 0.00, 6, '2019-11-12 06:34:52'),
(994, '', 0.00, 6, '2019-11-12 06:34:52'),
(995, '', 0.00, 6, '2019-11-12 06:34:52'),
(996, '', 0.00, 6, '2019-11-12 06:34:52'),
(997, '', 0.00, 6, '2019-11-12 06:34:52'),
(998, '', 0.00, 6, '2019-11-12 06:34:52'),
(999, '', 0.00, 6, '2019-11-12 06:34:52'),
(1000, '', 0.00, 6, '2019-11-12 06:34:52'),
(1001, '', 0.00, 6, '2019-11-12 06:34:52'),
(1002, '', 0.00, 6, '2019-11-12 06:34:52'),
(1003, '', 0.00, 6, '2019-11-12 06:34:52'),
(1004, '', 0.00, 6, '2019-11-12 06:34:52'),
(1005, '', 0.00, 6, '2019-11-12 06:34:52'),
(1006, '', 0.00, 6, '2019-11-12 06:34:52'),
(1007, '', 0.00, 6, '2019-11-12 06:34:52'),
(1008, '', 0.00, 6, '2019-11-12 06:34:52'),
(1009, '', 0.00, 6, '2019-11-12 06:34:53'),
(1010, '', 0.00, 6, '2019-11-12 06:34:53'),
(1011, '', 0.00, 6, '2019-11-12 06:34:53'),
(1012, '', 0.00, 6, '2019-11-12 06:34:53'),
(1013, '', 0.00, 6, '2019-11-12 06:34:53'),
(1014, '', 0.00, 6, '2019-11-12 06:34:53'),
(1015, '', 0.00, 6, '2019-11-12 06:34:53'),
(1016, '', 0.00, 6, '2019-11-12 06:34:53'),
(1017, '', 0.00, 6, '2019-11-12 06:34:53'),
(1018, '', 0.00, 6, '2019-11-12 06:34:53'),
(1019, '', 0.00, 6, '2019-11-12 06:34:53'),
(1020, '', 0.00, 6, '2019-11-12 06:34:53'),
(1021, '', 0.00, 6, '2019-11-12 06:34:53'),
(1022, '', 0.00, 6, '2019-11-12 06:34:53'),
(1023, '', 0.00, 6, '2019-11-12 06:34:53'),
(1024, '', 0.00, 6, '2019-11-12 06:34:53'),
(1025, '', 0.00, 6, '2019-11-12 06:34:53'),
(1026, '', 0.00, 6, '2019-11-12 06:34:53'),
(1027, '', 0.00, 6, '2019-11-12 06:34:53'),
(1028, '', 0.00, 6, '2019-11-12 06:34:54'),
(1029, '', 0.00, 6, '2019-11-12 06:34:54'),
(1030, '', 0.00, 6, '2019-11-12 06:34:54'),
(1031, '', 0.00, 6, '2019-11-12 06:34:54'),
(1032, '', 0.00, 6, '2019-11-12 06:34:54'),
(1033, '', 0.00, 6, '2019-11-12 06:34:54'),
(1034, '', 0.00, 6, '2019-11-12 06:34:54'),
(1035, '', 0.00, 6, '2019-11-12 06:34:54'),
(1036, '', 0.00, 6, '2019-11-12 06:34:54'),
(1037, '', 0.00, 6, '2019-11-12 06:34:54'),
(1038, '', 0.00, 6, '2019-11-12 06:34:54'),
(1039, '', 0.00, 6, '2019-11-12 06:34:54'),
(1040, '', 0.00, 6, '2019-11-12 06:34:54'),
(1041, '', 0.00, 6, '2019-11-12 06:34:54'),
(1042, '', 0.00, 6, '2019-11-12 06:34:54'),
(1043, '', 0.00, 6, '2019-11-12 06:34:54'),
(1044, '', 0.00, 6, '2019-11-12 06:34:54'),
(1045, '', 0.00, 6, '2019-11-12 06:34:54'),
(1046, '', 0.00, 6, '2019-11-12 06:34:54'),
(1047, '', 0.00, 6, '2019-11-12 06:34:54'),
(1048, '', 0.00, 6, '2019-11-12 06:34:54'),
(1049, '', 0.00, 6, '2019-11-12 06:34:55'),
(1050, '', 0.00, 6, '2019-11-12 06:34:55'),
(1051, '', 0.00, 6, '2019-11-12 06:34:55'),
(1052, '', 0.00, 6, '2019-11-12 06:34:55'),
(1053, '', 0.00, 6, '2019-11-12 06:34:55'),
(1054, '', 0.00, 6, '2019-11-12 06:34:55'),
(1055, '', 0.00, 6, '2019-11-12 06:34:55'),
(1056, '', 0.00, 6, '2019-11-12 06:34:55'),
(1057, '', 0.00, 6, '2019-11-12 06:34:55'),
(1058, '', 0.00, 6, '2019-11-12 06:34:55'),
(1059, '', 0.00, 6, '2019-11-12 06:34:55'),
(1060, '', 0.00, 6, '2019-11-12 06:34:55'),
(1061, '', 0.00, 6, '2019-11-12 06:34:55'),
(1062, '', 0.00, 6, '2019-11-12 06:34:55'),
(1063, '', 0.00, 6, '2019-11-12 06:34:56'),
(1064, '', 0.00, 6, '2019-11-12 06:34:56'),
(1065, '', 0.00, 6, '2019-11-12 06:34:56'),
(1066, '', 0.00, 6, '2019-11-12 06:34:56'),
(1067, '', 0.00, 6, '2019-11-12 06:34:56'),
(1068, '', 0.00, 6, '2019-11-12 06:34:56'),
(1069, '\0\0\0', 0.00, 6, '2019-11-12 06:34:56'),
(1070, '\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:56'),
(1071, '\0\0\0\0S', 0.00, 6, '2019-11-12 06:34:56'),
(1072, '', 0.00, 6, '2019-11-12 06:34:57'),
(1073, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:57'),
(1074, '', 0.00, 6, '2019-11-12 06:34:57'),
(1075, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:57'),
(1076, '', 0.00, 6, '2019-11-12 06:34:57'),
(1077, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:57'),
(1078, '', 0.00, 6, '2019-11-12 06:34:57');
INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(1079, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:57'),
(1080, '', 0.00, 6, '2019-11-12 06:34:57'),
(1081, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:58'),
(1082, '', 0.00, 6, '2019-11-12 06:34:58'),
(1083, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:58'),
(1084, '\0\0\0R', 0.00, 6, '2019-11-12 06:34:58'),
(1085, '', 0.00, 6, '2019-11-12 06:34:58'),
(1086, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:58'),
(1087, '\0\0\0R', 0.00, 6, '2019-11-12 06:34:58'),
(1088, '', 0.00, 6, '2019-11-12 06:34:58'),
(1089, '\0\0\0\0	\0\0~', 0.00, 6, '2019-11-12 06:34:58'),
(1090, '', 0.00, 6, '2019-11-12 06:34:58'),
(1091, '\0	\0\0\0', 0.00, 6, '2019-11-12 06:34:58'),
(1092, '\0\0~', 0.00, 6, '2019-11-12 06:34:59'),
(1093, '', 0.00, 6, '2019-11-12 06:34:59'),
(1094, '\0', 0.00, 6, '2019-11-12 06:34:59'),
(1095, '\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:59'),
(1096, '\0', 0.00, 6, '2019-11-12 06:34:59'),
(1097, '', 0.00, 6, '2019-11-12 06:34:59'),
(1098, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:59'),
(1099, '', 0.00, 6, '2019-11-12 06:34:59'),
(1100, '\0\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:34:59'),
(1101, '', 0.00, 6, '2019-11-12 06:34:59'),
(1102, '\0\r\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:59'),
(1103, '', 0.00, 6, '2019-11-12 06:34:59'),
(1104, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:59'),
(1105, '', 0.00, 6, '2019-11-12 06:34:59'),
(1106, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:34:59'),
(1107, '', 0.00, 6, '2019-11-12 06:34:59'),
(1108, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:00'),
(1109, '', 0.00, 6, '2019-11-12 06:35:00'),
(1110, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:00'),
(1111, '', 0.00, 6, '2019-11-12 06:35:00'),
(1112, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:00'),
(1113, '', 0.00, 6, '2019-11-12 06:35:00'),
(1114, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:01'),
(1115, '', 0.00, 6, '2019-11-12 06:35:01'),
(1116, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:01'),
(1117, '', 0.00, 6, '2019-11-12 06:35:01'),
(1118, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:01'),
(1119, '', 0.00, 6, '2019-11-12 06:35:02'),
(1120, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:02'),
(1121, '', 0.00, 6, '2019-11-12 06:35:02'),
(1122, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:02'),
(1123, '', 0.00, 6, '2019-11-12 06:35:03'),
(1124, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:03'),
(1125, '', 0.00, 6, '2019-11-12 06:35:03'),
(1126, '\0\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:35:03'),
(1127, '', 0.00, 6, '2019-11-12 06:35:03'),
(1128, '\0\Z\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:03'),
(1129, '', 0.00, 6, '2019-11-12 06:35:03'),
(1130, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:03'),
(1131, '', 0.00, 6, '2019-11-12 06:35:03'),
(1132, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:03'),
(1133, '', 0.00, 6, '2019-11-12 06:35:03'),
(1134, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:03'),
(1135, '', 0.00, 6, '2019-11-12 06:35:03'),
(1136, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:03'),
(1137, '', 0.00, 6, '2019-11-12 06:35:03'),
(1138, '\0\0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:35:04'),
(1139, '', 0.00, 6, '2019-11-12 06:35:04'),
(1140, '\0 \0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:35:04'),
(1141, '', 0.00, 6, '2019-11-12 06:35:04'),
(1142, '\0!\0\0\0&quot;\0\0~', 0.00, 6, '2019-11-12 06:35:04'),
(1143, '', 0.00, 6, '2019-11-12 06:35:04'),
(1144, '\0&quot;\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:35:04'),
(1145, '', 0.00, 6, '2019-11-12 06:35:04'),
(1146, '\0#\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:35:04'),
(1147, '', 0.00, 6, '2019-11-12 06:35:04'),
(1148, '\0$\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:35:04'),
(1149, '', 0.00, 6, '2019-11-12 06:35:04'),
(1150, '\0%\0\0\0&amp;\0\0~', 0.00, 6, '2019-11-12 06:35:04'),
(1151, '', 0.00, 6, '2019-11-12 06:35:04'),
(1152, '\0&amp;\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:35:04'),
(1153, '', 0.00, 6, '2019-11-12 06:35:04'),
(1154, '\0\'\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:35:04'),
(1155, '\0\'\0\0R', 0.00, 6, '2019-11-12 06:35:05'),
(1156, '', 0.00, 6, '2019-11-12 06:35:05'),
(1157, '\0(\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:35:05'),
(1158, '', 0.00, 6, '2019-11-12 06:35:05'),
(1159, '\0)\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:35:05'),
(1160, '', 0.00, 6, '2019-11-12 06:35:05'),
(1161, '', 0.00, 6, '2019-11-12 06:35:05'),
(1162, '\0+\0\0\0', 0.00, 6, '2019-11-12 06:35:05'),
(1163, '', 0.00, 6, '2019-11-12 06:35:05'),
(1164, '\0', 0.00, 6, '2019-11-12 06:35:06'),
(1165, '\0', 0.00, 6, '2019-11-12 06:35:06'),
(1166, '\0-\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:35:06'),
(1167, '', 0.00, 6, '2019-11-12 06:35:06'),
(1168, '\0.\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:35:06'),
(1169, '', 0.00, 6, '2019-11-12 06:35:06'),
(1170, '\0/\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:35:06'),
(1171, '', 0.00, 6, '2019-11-12 06:35:06'),
(1172, '\00\0\0\01\0\0~', 0.00, 6, '2019-11-12 06:35:06'),
(1173, '', 0.00, 6, '2019-11-12 06:35:06'),
(1174, '\01\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:35:06'),
(1175, '', 0.00, 6, '2019-11-12 06:35:06'),
(1176, '\02\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:35:06'),
(1177, '', 0.00, 6, '2019-11-12 06:35:06'),
(1178, '\03\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:35:06'),
(1179, '', 0.00, 6, '2019-11-12 06:35:06'),
(1180, '\04\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:35:06'),
(1181, '', 0.00, 6, '2019-11-12 06:35:06'),
(1182, '\05\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:35:07'),
(1183, '', 0.00, 6, '2019-11-12 06:35:07'),
(1184, '\06\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:35:07'),
(1185, '', 0.00, 6, '2019-11-12 06:35:07'),
(1186, '\07\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:35:07'),
(1187, '', 0.00, 6, '2019-11-12 06:35:07'),
(1188, '\08\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:35:07'),
(1189, '', 0.00, 6, '2019-11-12 06:35:07'),
(1190, '\09\0\0\0:\0\0~', 0.00, 6, '2019-11-12 06:35:08'),
(1191, '', 0.00, 6, '2019-11-12 06:35:08'),
(1192, '\0:\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:35:08'),
(1193, '', 0.00, 6, '2019-11-12 06:35:08'),
(1194, '\0;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:35:08'),
(1195, '\0;\0\0', 0.00, 6, '2019-11-12 06:35:08'),
(1196, '', 0.00, 6, '2019-11-12 06:35:08'),
(1197, '\0&lt;\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:35:08'),
(1198, '', 0.00, 6, '2019-11-12 06:35:09'),
(1199, '\0=\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:35:09'),
(1200, '', 0.00, 6, '2019-11-12 06:35:09'),
(1201, '\0&gt;\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:35:09'),
(1202, '', 0.00, 6, '2019-11-12 06:35:09'),
(1203, '\0?\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:35:09'),
(1204, '', 0.00, 6, '2019-11-12 06:35:09'),
(1205, '\0@\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:35:09'),
(1206, '', 0.00, 6, '2019-11-12 06:35:09'),
(1207, '\0A\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:35:09'),
(1208, '', 0.00, 6, '2019-11-12 06:35:09'),
(1209, '\0B\0\0\0C\0\0~', 0.00, 6, '2019-11-12 06:35:09'),
(1210, '', 0.00, 6, '2019-11-12 06:35:10'),
(1211, '\0C\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:35:10'),
(1212, '', 0.00, 6, '2019-11-12 06:35:10'),
(1213, '\0D\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:35:10'),
(1214, '', 0.00, 6, '2019-11-12 06:35:10'),
(1215, '\0E\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:35:10'),
(1216, '', 0.00, 6, '2019-11-12 06:35:10'),
(1217, '\0F\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:35:10'),
(1218, '', 0.00, 6, '2019-11-12 06:35:10'),
(1219, '\0G\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:35:10'),
(1220, '', 0.00, 6, '2019-11-12 06:35:10'),
(1221, '\0H\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:35:10'),
(1222, '', 0.00, 6, '2019-11-12 06:35:10'),
(1223, '\0I\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:35:10'),
(1224, '', 0.00, 6, '2019-11-12 06:35:10'),
(1225, '\0J\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:35:10'),
(1226, '', 0.00, 6, '2019-11-12 06:35:10'),
(1227, '\0K\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:35:10'),
(1228, '', 0.00, 6, '2019-11-12 06:35:10'),
(1229, '\0L\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:35:10'),
(1230, '', 0.00, 6, '2019-11-12 06:35:11'),
(1231, '\0M\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:35:11'),
(1232, '', 0.00, 6, '2019-11-12 06:35:11'),
(1233, '', 0.00, 6, '2019-11-12 06:35:11'),
(1234, '\0O\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:35:11'),
(1235, '', 0.00, 6, '2019-11-12 06:35:11'),
(1236, '\0P\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:35:11'),
(1237, '', 0.00, 6, '2019-11-12 06:35:11'),
(1238, '\0Q\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:35:12'),
(1239, '', 0.00, 6, '2019-11-12 06:35:13'),
(1240, '\0R\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:35:13'),
(1241, '', 0.00, 6, '2019-11-12 06:35:13'),
(1242, '\0S\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:35:13'),
(1243, '', 0.00, 6, '2019-11-12 06:35:13'),
(1244, '\0T\0\0\0U\0\0~', 0.00, 6, '2019-11-12 06:35:13'),
(1245, '', 0.00, 6, '2019-11-12 06:35:13'),
(1246, '\0U\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:35:13'),
(1247, '', 0.00, 6, '2019-11-12 06:35:13'),
(1248, '\0V\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:35:13'),
(1249, '', 0.00, 6, '2019-11-12 06:35:13'),
(1250, '\0W\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:35:13'),
(1251, '', 0.00, 6, '2019-11-12 06:35:13'),
(1252, '\0X\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:35:13'),
(1253, '', 0.00, 6, '2019-11-12 06:35:13'),
(1254, '\0Y\0\0\0Z\0\0~', 0.00, 6, '2019-11-12 06:35:13'),
(1255, '', 0.00, 6, '2019-11-12 06:35:13'),
(1256, '\0Z\0\0\0[\0\0~', 0.00, 6, '2019-11-12 06:35:13'),
(1257, '', 0.00, 6, '2019-11-12 06:35:13'),
(1258, '\0[\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:35:13'),
(1259, '', 0.00, 6, '2019-11-12 06:35:14'),
(1260, '\0\\\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:35:14'),
(1261, '\0\\\0\0', 0.00, 6, '2019-11-12 06:35:14'),
(1262, '', 0.00, 6, '2019-11-12 06:35:14'),
(1263, '\0]\0\0\0^\0\0~', 0.00, 6, '2019-11-12 06:35:14'),
(1264, '', 0.00, 6, '2019-11-12 06:35:14'),
(1265, '\0^\0\0\0_\0\0~', 0.00, 6, '2019-11-12 06:35:14'),
(1266, '', 0.00, 6, '2019-11-12 06:35:14'),
(1267, '\0_\0\0\0`\0\0~', 0.00, 6, '2019-11-12 06:35:14'),
(1268, '\0_\0\0B)\0\0\0`\0\0\0', 0.00, 6, '2019-11-12 06:35:14'),
(1269, '\0`\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:35:14'),
(1270, '', 0.00, 6, '2019-11-12 06:35:14'),
(1271, '\0a\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:35:14'),
(1272, '', 0.00, 6, '2019-11-12 06:35:14'),
(1273, '\0b\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:35:14'),
(1274, '', 0.00, 6, '2019-11-12 06:35:14'),
(1275, '\0c\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:35:14'),
(1276, '', 0.00, 6, '2019-11-12 06:35:14'),
(1277, '\0d\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:35:15'),
(1278, '', 0.00, 6, '2019-11-12 06:35:15'),
(1279, '\0e\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:35:15'),
(1280, '', 0.00, 6, '2019-11-12 06:35:15'),
(1281, '\0f\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:35:15'),
(1282, '', 0.00, 6, '2019-11-12 06:35:15'),
(1283, '\0g\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:35:15'),
(1284, '', 0.00, 6, '2019-11-12 06:35:15'),
(1285, '\0h\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:35:15'),
(1286, '', 0.00, 6, '2019-11-12 06:35:15'),
(1287, '\0i\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:35:15'),
(1288, '', 0.00, 6, '2019-11-12 06:35:15'),
(1289, '\0j\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:35:15'),
(1290, '', 0.00, 6, '2019-11-12 06:35:15'),
(1291, '\0k\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:35:16'),
(1292, '', 0.00, 6, '2019-11-12 06:35:16'),
(1293, '\0l\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:35:16'),
(1294, '', 0.00, 6, '2019-11-12 06:35:16'),
(1295, '\0m\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:35:16'),
(1296, '', 0.00, 6, '2019-11-12 06:35:16'),
(1297, '\0n\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:35:16'),
(1298, '', 0.00, 6, '2019-11-12 06:35:16'),
(1299, '\0o\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:35:16'),
(1300, '', 0.00, 6, '2019-11-12 06:35:16'),
(1301, '\0p\0\0\0q\0\0~', 0.00, 6, '2019-11-12 06:35:16'),
(1302, '', 0.00, 6, '2019-11-12 06:35:16'),
(1303, '\0q\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:35:16'),
(1304, '', 0.00, 6, '2019-11-12 06:35:16'),
(1305, '\0r\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:35:16'),
(1306, '', 0.00, 6, '2019-11-12 06:35:16'),
(1307, '\0s\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:35:16'),
(1308, '', 0.00, 6, '2019-11-12 06:35:16'),
(1309, '\0t\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:35:16'),
(1310, '', 0.00, 6, '2019-11-12 06:35:17'),
(1311, '\0u\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:35:17'),
(1312, '', 0.00, 6, '2019-11-12 06:35:17'),
(1313, '\0v\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:35:17'),
(1314, '', 0.00, 6, '2019-11-12 06:35:17'),
(1315, '\0w\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:35:17'),
(1316, '', 0.00, 6, '2019-11-12 06:35:17'),
(1317, '\0x\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:35:17'),
(1318, '', 0.00, 6, '2019-11-12 06:35:17'),
(1319, '\0y\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:35:17'),
(1320, '', 0.00, 6, '2019-11-12 06:35:17'),
(1321, '\0z\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:35:18'),
(1322, '', 0.00, 6, '2019-11-12 06:35:18'),
(1323, '\0{\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:35:18'),
(1324, '\0{\0\0', 0.00, 6, '2019-11-12 06:35:18'),
(1325, '', 0.00, 6, '2019-11-12 06:35:18'),
(1326, '\0|\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:35:18'),
(1327, '', 0.00, 6, '2019-11-12 06:35:18'),
(1328, '\0}\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:35:18'),
(1329, '', 0.00, 6, '2019-11-12 06:35:18'),
(1330, '\0~\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:18'),
(1331, '', 0.00, 6, '2019-11-12 06:35:18'),
(1332, '', 0.00, 6, '2019-11-12 06:35:19'),
(1333, '', 0.00, 6, '2019-11-12 06:35:19'),
(1334, '', 0.00, 6, '2019-11-12 06:35:19'),
(1335, '', 0.00, 6, '2019-11-12 06:35:19'),
(1336, '', 0.00, 6, '2019-11-12 06:35:19'),
(1337, '', 0.00, 6, '2019-11-12 06:35:19'),
(1338, '', 0.00, 6, '2019-11-12 06:35:19'),
(1339, '', 0.00, 6, '2019-11-12 06:35:19'),
(1340, '', 0.00, 6, '2019-11-12 06:35:19'),
(1341, '', 0.00, 6, '2019-11-12 06:35:19'),
(1342, '', 0.00, 6, '2019-11-12 06:35:19'),
(1343, '', 0.00, 6, '2019-11-12 06:35:19'),
(1344, '', 0.00, 6, '2019-11-12 06:35:19'),
(1345, '', 0.00, 6, '2019-11-12 06:35:19'),
(1346, '', 0.00, 6, '2019-11-12 06:35:20'),
(1347, '', 0.00, 6, '2019-11-12 06:35:20'),
(1348, '', 0.00, 6, '2019-11-12 06:35:20'),
(1349, '', 0.00, 6, '2019-11-12 06:35:20'),
(1350, '', 0.00, 6, '2019-11-12 06:35:20'),
(1351, '', 0.00, 6, '2019-11-12 06:35:20'),
(1352, '', 0.00, 6, '2019-11-12 06:35:20'),
(1353, '', 0.00, 6, '2019-11-12 06:35:20'),
(1354, '', 0.00, 6, '2019-11-12 06:35:20'),
(1355, '', 0.00, 6, '2019-11-12 06:35:20'),
(1356, '', 0.00, 6, '2019-11-12 06:35:21'),
(1357, '', 0.00, 6, '2019-11-12 06:35:21'),
(1358, '', 0.00, 6, '2019-11-12 06:35:21'),
(1359, '', 0.00, 6, '2019-11-12 06:35:21'),
(1360, '', 0.00, 6, '2019-11-12 06:35:21'),
(1361, '', 0.00, 6, '2019-11-12 06:35:21'),
(1362, '', 0.00, 6, '2019-11-12 06:35:21'),
(1363, '', 0.00, 6, '2019-11-12 06:35:21'),
(1364, '', 0.00, 6, '2019-11-12 06:35:21'),
(1365, '', 0.00, 6, '2019-11-12 06:35:21'),
(1366, '', 0.00, 6, '2019-11-12 06:35:21'),
(1367, '', 0.00, 6, '2019-11-12 06:35:21'),
(1368, '', 0.00, 6, '2019-11-12 06:35:21'),
(1369, '', 0.00, 6, '2019-11-12 06:35:21'),
(1370, '', 0.00, 6, '2019-11-12 06:35:21'),
(1371, '', 0.00, 6, '2019-11-12 06:35:21'),
(1372, '', 0.00, 6, '2019-11-12 06:35:21'),
(1373, '', 0.00, 6, '2019-11-12 06:35:21'),
(1374, '', 0.00, 6, '2019-11-12 06:35:21'),
(1375, '', 0.00, 6, '2019-11-12 06:35:22'),
(1376, '', 0.00, 6, '2019-11-12 06:35:22'),
(1377, '', 0.00, 6, '2019-11-12 06:35:22'),
(1378, '', 0.00, 6, '2019-11-12 06:35:22'),
(1379, '', 0.00, 6, '2019-11-12 06:35:22'),
(1380, '', 0.00, 6, '2019-11-12 06:35:22'),
(1381, '', 0.00, 6, '2019-11-12 06:35:22'),
(1382, '', 0.00, 6, '2019-11-12 06:35:22'),
(1383, '', 0.00, 6, '2019-11-12 06:35:22'),
(1384, '', 0.00, 6, '2019-11-12 06:35:22'),
(1385, '', 0.00, 6, '2019-11-12 06:35:22'),
(1386, '', 0.00, 6, '2019-11-12 06:35:22'),
(1387, '', 0.00, 6, '2019-11-12 06:35:22'),
(1388, '', 0.00, 6, '2019-11-12 06:35:22'),
(1389, '', 0.00, 6, '2019-11-12 06:35:22'),
(1390, '', 0.00, 6, '2019-11-12 06:35:23'),
(1391, '', 0.00, 6, '2019-11-12 06:35:23'),
(1392, '', 0.00, 6, '2019-11-12 06:35:23'),
(1393, '', 0.00, 6, '2019-11-12 06:35:23'),
(1394, '', 0.00, 6, '2019-11-12 06:35:23'),
(1395, '', 0.00, 6, '2019-11-12 06:35:23'),
(1396, '', 0.00, 6, '2019-11-12 06:35:23'),
(1397, '', 0.00, 6, '2019-11-12 06:35:24'),
(1398, '', 0.00, 6, '2019-11-12 06:35:24'),
(1399, '', 0.00, 6, '2019-11-12 06:35:24'),
(1400, '', 0.00, 6, '2019-11-12 06:35:24'),
(1401, '', 0.00, 6, '2019-11-12 06:35:24'),
(1402, '', 0.00, 6, '2019-11-12 06:35:24'),
(1403, '', 0.00, 6, '2019-11-12 06:35:24'),
(1404, '', 0.00, 6, '2019-11-12 06:35:24'),
(1405, '', 0.00, 6, '2019-11-12 06:35:24'),
(1406, '', 0.00, 6, '2019-11-12 06:35:24'),
(1407, '', 0.00, 6, '2019-11-12 06:35:24'),
(1408, '', 0.00, 6, '2019-11-12 06:35:24'),
(1409, '', 0.00, 6, '2019-11-12 06:35:24'),
(1410, '', 0.00, 6, '2019-11-12 06:35:24'),
(1411, '', 0.00, 6, '2019-11-12 06:35:24'),
(1412, '', 0.00, 6, '2019-11-12 06:35:25'),
(1413, '', 0.00, 6, '2019-11-12 06:35:25'),
(1414, '', 0.00, 6, '2019-11-12 06:35:25'),
(1415, '', 0.00, 6, '2019-11-12 06:35:25'),
(1416, '', 0.00, 6, '2019-11-12 06:35:25'),
(1417, '', 0.00, 6, '2019-11-12 06:35:25'),
(1418, '', 0.00, 6, '2019-11-12 06:35:25'),
(1419, '', 0.00, 6, '2019-11-12 06:35:25'),
(1420, '', 0.00, 6, '2019-11-12 06:35:26'),
(1421, '', 0.00, 6, '2019-11-12 06:35:26'),
(1422, '', 0.00, 6, '2019-11-12 06:35:26'),
(1423, '', 0.00, 6, '2019-11-12 06:35:26'),
(1424, '', 0.00, 6, '2019-11-12 06:35:26'),
(1425, '', 0.00, 6, '2019-11-12 06:35:26'),
(1426, '', 0.00, 6, '2019-11-12 06:35:26'),
(1427, '', 0.00, 6, '2019-11-12 06:35:26'),
(1428, '', 0.00, 6, '2019-11-12 06:35:26'),
(1429, '', 0.00, 6, '2019-11-12 06:35:26'),
(1430, '', 0.00, 6, '2019-11-12 06:35:27'),
(1431, '', 0.00, 6, '2019-11-12 06:35:27'),
(1432, '', 0.00, 6, '2019-11-12 06:35:27'),
(1433, '', 0.00, 6, '2019-11-12 06:35:27'),
(1434, '', 0.00, 6, '2019-11-12 06:35:27'),
(1435, '', 0.00, 6, '2019-11-12 06:35:27'),
(1436, '', 0.00, 6, '2019-11-12 06:35:27'),
(1437, '', 0.00, 6, '2019-11-12 06:35:27'),
(1438, '', 0.00, 6, '2019-11-12 06:35:28'),
(1439, '', 0.00, 6, '2019-11-12 06:35:28'),
(1440, '', 0.00, 6, '2019-11-12 06:35:28'),
(1441, '', 0.00, 6, '2019-11-12 06:35:28'),
(1442, '', 0.00, 6, '2019-11-12 06:35:28'),
(1443, '', 0.00, 6, '2019-11-12 06:35:28'),
(1444, '', 0.00, 6, '2019-11-12 06:35:28'),
(1445, '', 0.00, 6, '2019-11-12 06:35:28'),
(1446, '', 0.00, 6, '2019-11-12 06:35:28'),
(1447, '', 0.00, 6, '2019-11-12 06:35:28'),
(1448, '', 0.00, 6, '2019-11-12 06:35:28'),
(1449, '', 0.00, 6, '2019-11-12 06:35:28'),
(1450, '', 0.00, 6, '2019-11-12 06:35:28'),
(1451, '', 0.00, 6, '2019-11-12 06:35:28'),
(1452, '', 0.00, 6, '2019-11-12 06:35:28'),
(1453, '', 0.00, 6, '2019-11-12 06:35:28'),
(1454, '', 0.00, 6, '2019-11-12 06:35:28'),
(1455, '', 0.00, 6, '2019-11-12 06:35:29'),
(1456, '', 0.00, 6, '2019-11-12 06:35:29'),
(1457, '', 0.00, 6, '2019-11-12 06:35:29'),
(1458, '', 0.00, 6, '2019-11-12 06:35:29'),
(1459, '', 0.00, 6, '2019-11-12 06:35:29'),
(1460, '', 0.00, 6, '2019-11-12 06:35:29'),
(1461, '', 0.00, 6, '2019-11-12 06:35:29'),
(1462, '', 0.00, 6, '2019-11-12 06:35:29'),
(1463, '', 0.00, 6, '2019-11-12 06:35:29'),
(1464, '', 0.00, 6, '2019-11-12 06:35:29'),
(1465, '', 0.00, 6, '2019-11-12 06:35:29'),
(1466, '', 0.00, 6, '2019-11-12 06:35:29'),
(1467, '', 0.00, 6, '2019-11-12 06:35:29'),
(1468, '', 0.00, 6, '2019-11-12 06:35:29'),
(1469, '', 0.00, 6, '2019-11-12 06:35:30'),
(1470, '', 0.00, 6, '2019-11-12 06:35:30'),
(1471, '', 0.00, 6, '2019-11-12 06:35:30'),
(1472, '', 0.00, 6, '2019-11-12 06:35:30'),
(1473, '', 0.00, 6, '2019-11-12 06:35:30'),
(1474, '', 0.00, 6, '2019-11-12 06:35:30'),
(1475, '', 0.00, 6, '2019-11-12 06:35:30'),
(1476, '', 0.00, 6, '2019-11-12 06:35:30'),
(1477, '', 0.00, 6, '2019-11-12 06:35:30'),
(1478, '', 0.00, 6, '2019-11-12 06:35:30'),
(1479, '', 0.00, 6, '2019-11-12 06:35:30'),
(1480, '', 0.00, 6, '2019-11-12 06:35:30'),
(1481, '', 0.00, 6, '2019-11-12 06:35:30'),
(1482, '', 0.00, 6, '2019-11-12 06:35:30'),
(1483, '', 0.00, 6, '2019-11-12 06:35:30'),
(1484, '', 0.00, 6, '2019-11-12 06:35:30'),
(1485, '', 0.00, 6, '2019-11-12 06:35:30'),
(1486, '', 0.00, 6, '2019-11-12 06:35:31'),
(1487, '', 0.00, 6, '2019-11-12 06:35:31'),
(1488, '', 0.00, 6, '2019-11-12 06:35:31'),
(1489, '', 0.00, 6, '2019-11-12 06:35:31'),
(1490, '', 0.00, 6, '2019-11-12 06:35:31'),
(1491, '', 0.00, 6, '2019-11-12 06:35:31'),
(1492, '', 0.00, 6, '2019-11-12 06:35:31'),
(1493, '', 0.00, 6, '2019-11-12 06:35:31'),
(1494, '', 0.00, 6, '2019-11-12 06:35:31'),
(1495, '', 0.00, 6, '2019-11-12 06:35:31'),
(1496, '', 0.00, 6, '2019-11-12 06:35:31'),
(1497, '', 0.00, 6, '2019-11-12 06:35:31'),
(1498, '', 0.00, 6, '2019-11-12 06:35:31'),
(1499, '', 0.00, 6, '2019-11-12 06:35:31'),
(1500, '', 0.00, 6, '2019-11-12 06:35:32'),
(1501, '', 0.00, 6, '2019-11-12 06:35:32'),
(1502, '', 0.00, 6, '2019-11-12 06:35:32'),
(1503, '', 0.00, 6, '2019-11-12 06:35:32'),
(1504, '', 0.00, 6, '2019-11-12 06:35:32'),
(1505, '', 0.00, 6, '2019-11-12 06:35:32'),
(1506, '', 0.00, 6, '2019-11-12 06:35:32'),
(1507, '', 0.00, 6, '2019-11-12 06:35:32'),
(1508, '', 0.00, 6, '2019-11-12 06:35:32'),
(1509, '', 0.00, 6, '2019-11-12 06:35:32'),
(1510, '', 0.00, 6, '2019-11-12 06:35:32'),
(1511, '', 0.00, 6, '2019-11-12 06:35:33'),
(1512, '', 0.00, 6, '2019-11-12 06:35:33'),
(1513, '', 0.00, 6, '2019-11-12 06:35:33'),
(1514, '', 0.00, 6, '2019-11-12 06:35:33'),
(1515, '', 0.00, 6, '2019-11-12 06:35:33'),
(1516, '', 0.00, 6, '2019-11-12 06:35:33'),
(1517, '', 0.00, 6, '2019-11-12 06:35:33'),
(1518, '', 0.00, 6, '2019-11-12 06:35:33'),
(1519, '', 0.00, 6, '2019-11-12 06:35:33'),
(1520, '', 0.00, 6, '2019-11-12 06:35:33'),
(1521, '', 0.00, 6, '2019-11-12 06:35:33'),
(1522, '', 0.00, 6, '2019-11-12 06:35:33'),
(1523, '', 0.00, 6, '2019-11-12 06:35:33'),
(1524, '', 0.00, 6, '2019-11-12 06:35:34'),
(1525, '', 0.00, 6, '2019-11-12 06:35:34'),
(1526, '', 0.00, 6, '2019-11-12 06:35:35'),
(1527, '', 0.00, 6, '2019-11-12 06:35:35'),
(1528, '', 0.00, 6, '2019-11-12 06:35:35'),
(1529, '', 0.00, 6, '2019-11-12 06:35:35'),
(1530, '', 0.00, 6, '2019-11-12 06:35:35'),
(1531, '', 0.00, 6, '2019-11-12 06:35:35'),
(1532, '', 0.00, 6, '2019-11-12 06:35:36'),
(1533, '', 0.00, 6, '2019-11-12 06:35:36'),
(1534, '', 0.00, 6, '2019-11-12 06:35:36'),
(1535, '', 0.00, 6, '2019-11-12 06:35:36'),
(1536, '', 0.00, 6, '2019-11-12 06:35:36'),
(1537, '', 0.00, 6, '2019-11-12 06:35:37'),
(1538, '', 0.00, 6, '2019-11-12 06:35:37'),
(1539, '', 0.00, 6, '2019-11-12 06:35:37'),
(1540, '', 0.00, 6, '2019-11-12 06:35:37'),
(1541, '', 0.00, 6, '2019-11-12 06:35:37'),
(1542, '', 0.00, 6, '2019-11-12 06:35:37'),
(1543, '', 0.00, 6, '2019-11-12 06:35:37'),
(1544, '', 0.00, 6, '2019-11-12 06:35:37'),
(1545, '', 0.00, 6, '2019-11-12 06:35:37'),
(1546, '', 0.00, 6, '2019-11-12 06:35:37'),
(1547, '', 0.00, 6, '2019-11-12 06:35:38'),
(1548, '', 0.00, 6, '2019-11-12 06:35:38'),
(1549, '', 0.00, 6, '2019-11-12 06:35:38'),
(1550, '', 0.00, 6, '2019-11-12 06:35:38'),
(1551, '', 0.00, 6, '2019-11-12 06:35:38'),
(1552, '', 0.00, 6, '2019-11-12 06:35:38'),
(1553, '', 0.00, 6, '2019-11-12 06:35:38'),
(1554, '', 0.00, 6, '2019-11-12 06:35:38'),
(1555, '', 0.00, 6, '2019-11-12 06:35:38'),
(1556, '', 0.00, 6, '2019-11-12 06:35:38'),
(1557, '', 0.00, 6, '2019-11-12 06:35:38'),
(1558, '', 0.00, 6, '2019-11-12 06:35:38'),
(1559, '', 0.00, 6, '2019-11-12 06:35:38'),
(1560, '', 0.00, 6, '2019-11-12 06:35:38'),
(1561, '', 0.00, 6, '2019-11-12 06:35:38'),
(1562, '', 0.00, 6, '2019-11-12 06:35:38'),
(1563, '', 0.00, 6, '2019-11-12 06:35:38'),
(1564, '', 0.00, 6, '2019-11-12 06:35:38'),
(1565, '', 0.00, 6, '2019-11-12 06:35:38'),
(1566, '', 0.00, 6, '2019-11-12 06:35:39'),
(1567, '', 0.00, 6, '2019-11-12 06:35:39'),
(1568, '', 0.00, 6, '2019-11-12 06:35:39'),
(1569, '', 0.00, 6, '2019-11-12 06:35:39'),
(1570, '', 0.00, 6, '2019-11-12 06:35:39'),
(1571, '', 0.00, 6, '2019-11-12 06:35:39'),
(1572, '', 0.00, 6, '2019-11-12 06:35:39'),
(1573, '', 0.00, 6, '2019-11-12 06:35:39'),
(1574, '', 0.00, 6, '2019-11-12 06:35:39'),
(1575, '', 0.00, 6, '2019-11-12 06:35:39'),
(1576, '', 0.00, 6, '2019-11-12 06:35:39'),
(1577, '', 0.00, 6, '2019-11-12 06:35:39'),
(1578, '', 0.00, 6, '2019-11-12 06:35:39'),
(1579, '', 0.00, 6, '2019-11-12 06:35:39'),
(1580, '', 0.00, 6, '2019-11-12 06:35:39'),
(1581, '', 0.00, 6, '2019-11-12 06:35:39'),
(1582, '', 0.00, 6, '2019-11-12 06:35:40'),
(1583, '', 0.00, 6, '2019-11-12 06:35:40'),
(1584, '', 0.00, 6, '2019-11-12 06:35:40'),
(1585, '', 0.00, 6, '2019-11-12 06:35:40'),
(1586, '', 0.00, 6, '2019-11-12 06:35:40'),
(1587, '', 0.00, 6, '2019-11-12 06:35:40'),
(1588, '', 0.00, 6, '2019-11-12 06:35:40'),
(1589, '', 0.00, 6, '2019-11-12 06:35:40'),
(1590, '\0\0\0', 0.00, 6, '2019-11-12 06:35:40'),
(1591, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:40'),
(1592, '', 0.00, 6, '2019-11-12 06:35:40'),
(1593, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:40'),
(1594, '', 0.00, 6, '2019-11-12 06:35:40'),
(1595, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:40'),
(1596, '', 0.00, 6, '2019-11-12 06:35:40'),
(1597, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:40'),
(1598, '', 0.00, 6, '2019-11-12 06:35:40'),
(1599, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:40'),
(1600, '', 0.00, 6, '2019-11-12 06:35:41'),
(1601, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:41'),
(1602, '', 0.00, 6, '2019-11-12 06:35:41'),
(1603, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:41'),
(1604, '', 0.00, 6, '2019-11-12 06:35:41'),
(1605, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:41'),
(1606, '', 0.00, 6, '2019-11-12 06:35:41'),
(1607, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:41'),
(1608, '', 0.00, 6, '2019-11-12 06:35:41'),
(1609, '\0	\0\0\0	\0\0~', 0.00, 6, '2019-11-12 06:35:41'),
(1610, '', 0.00, 6, '2019-11-12 06:35:41'),
(1611, '\0', 0.00, 6, '2019-11-12 06:35:41'),
(1612, '\0\0\0', 0.00, 6, '2019-11-12 06:35:42'),
(1613, '\0\0~', 0.00, 6, '2019-11-12 06:35:42'),
(1614, '\0', 0.00, 6, '2019-11-12 06:35:42'),
(1615, '', 0.00, 6, '2019-11-12 06:35:42'),
(1616, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:42'),
(1617, '', 0.00, 6, '2019-11-12 06:35:42'),
(1618, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:42'),
(1619, '', 0.00, 6, '2019-11-12 06:35:43'),
(1620, '\0\r\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:35:43'),
(1621, '', 0.00, 6, '2019-11-12 06:35:43'),
(1622, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:43'),
(1623, '', 0.00, 6, '2019-11-12 06:35:43'),
(1624, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:43'),
(1625, '', 0.00, 6, '2019-11-12 06:35:43'),
(1626, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:43'),
(1627, '', 0.00, 6, '2019-11-12 06:35:43'),
(1628, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:43'),
(1629, '', 0.00, 6, '2019-11-12 06:35:43'),
(1630, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:43'),
(1631, '\0\0\0', 0.00, 6, '2019-11-12 06:35:43'),
(1632, '', 0.00, 6, '2019-11-12 06:35:43'),
(1633, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:44'),
(1634, '', 0.00, 6, '2019-11-12 06:35:44'),
(1635, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:44'),
(1636, '', 0.00, 6, '2019-11-12 06:35:44'),
(1637, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:44'),
(1638, '', 0.00, 6, '2019-11-12 06:35:44'),
(1639, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:44'),
(1640, '', 0.00, 6, '2019-11-12 06:35:44'),
(1641, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:44'),
(1642, '', 0.00, 6, '2019-11-12 06:35:44'),
(1643, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:44'),
(1644, '', 0.00, 6, '2019-11-12 06:35:44'),
(1645, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:44'),
(1646, '', 0.00, 6, '2019-11-12 06:35:44'),
(1647, '\0\Z\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:35:44'),
(1648, '', 0.00, 6, '2019-11-12 06:35:44'),
(1649, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:44'),
(1650, '', 0.00, 6, '2019-11-12 06:35:44'),
(1651, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:45'),
(1652, '', 0.00, 6, '2019-11-12 06:35:45'),
(1653, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:45'),
(1654, '', 0.00, 6, '2019-11-12 06:35:45'),
(1655, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:45'),
(1656, '\0\0\0S', 0.00, 6, '2019-11-12 06:35:45'),
(1657, '', 0.00, 6, '2019-11-12 06:35:45'),
(1658, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:35:46'),
(1659, '\0\0\0S', 0.00, 6, '2019-11-12 06:35:46'),
(1660, '\0\0\0 \0\0\0', 0.00, 6, '2019-11-12 06:35:46'),
(1661, '\0 \0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:35:46'),
(1662, '', 0.00, 6, '2019-11-12 06:35:46'),
(1663, '\0!\0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:35:46'),
(1664, '', 0.00, 6, '2019-11-12 06:35:46'),
(1665, '\0&quot;\0\0\0&quot;\0\0~', 0.00, 6, '2019-11-12 06:35:46'),
(1666, '', 0.00, 6, '2019-11-12 06:35:46'),
(1667, '\0#\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:35:46'),
(1668, '', 0.00, 6, '2019-11-12 06:35:46'),
(1669, '\0$\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:35:46'),
(1670, '', 0.00, 6, '2019-11-12 06:35:46'),
(1671, '\0%\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:35:47'),
(1672, '', 0.00, 6, '2019-11-12 06:35:47'),
(1673, '\0&amp;\0\0\0&amp;\0\0~', 0.00, 6, '2019-11-12 06:35:47'),
(1674, '', 0.00, 6, '2019-11-12 06:35:47'),
(1675, '\0\'\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:35:47'),
(1676, '', 0.00, 6, '2019-11-12 06:35:47'),
(1677, '\0(\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:35:47'),
(1678, '', 0.00, 6, '2019-11-12 06:35:47'),
(1679, '\0)\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:35:47'),
(1680, '', 0.00, 6, '2019-11-12 06:35:47'),
(1681, '\0*\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:35:47'),
(1682, '', 0.00, 6, '2019-11-12 06:35:47'),
(1683, '\0+\0\0\0+\0\0~', 0.00, 6, '2019-11-12 06:35:47'),
(1684, '', 0.00, 6, '2019-11-12 06:35:47'),
(1685, '\0', 0.00, 6, '2019-11-12 06:35:47'),
(1686, '\0', 0.00, 6, '2019-11-12 06:35:48'),
(1687, '\0-\0\0\0-\0\0~', 0.00, 6, '2019-11-12 06:35:48'),
(1688, '', 0.00, 6, '2019-11-12 06:35:48'),
(1689, '\0.\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:35:48'),
(1690, '', 0.00, 6, '2019-11-12 06:35:48'),
(1691, '\0/\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:35:48'),
(1692, '', 0.00, 6, '2019-11-12 06:35:48'),
(1693, '\00\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:35:48'),
(1694, '', 0.00, 6, '2019-11-12 06:35:48'),
(1695, '\01\0\0\01\0\0~', 0.00, 6, '2019-11-12 06:35:48'),
(1696, '', 0.00, 6, '2019-11-12 06:35:48'),
(1697, '\02\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:35:48'),
(1698, '', 0.00, 6, '2019-11-12 06:35:48'),
(1699, '\03\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:35:48'),
(1700, '', 0.00, 6, '2019-11-12 06:35:48'),
(1701, '\04\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:35:48'),
(1702, '', 0.00, 6, '2019-11-12 06:35:49'),
(1703, '\05\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:35:49'),
(1704, '', 0.00, 6, '2019-11-12 06:35:49'),
(1705, '\06\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:35:49'),
(1706, '', 0.00, 6, '2019-11-12 06:35:49'),
(1707, '\07\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:35:49'),
(1708, '', 0.00, 6, '2019-11-12 06:35:49'),
(1709, '\08\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:35:49'),
(1710, '', 0.00, 6, '2019-11-12 06:35:49'),
(1711, '\09\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:35:49'),
(1712, '', 0.00, 6, '2019-11-12 06:35:49'),
(1713, '', 0.00, 6, '2019-11-12 06:35:49'),
(1714, '\0;\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:35:49'),
(1715, '', 0.00, 6, '2019-11-12 06:35:49'),
(1716, '\0&lt;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:35:49'),
(1717, '', 0.00, 6, '2019-11-12 06:35:49'),
(1718, '\0=\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:35:49'),
(1719, '\0=\0\0', 0.00, 6, '2019-11-12 06:35:50'),
(1720, '', 0.00, 6, '2019-11-12 06:35:50'),
(1721, '\0&gt;\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:35:50'),
(1722, '', 0.00, 6, '2019-11-12 06:35:50'),
(1723, '\0?\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:35:50'),
(1724, '\0?\0\0\0\0\0\0@\0\0\0', 0.00, 6, '2019-11-12 06:35:50'),
(1725, '\0@\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:35:50'),
(1726, '', 0.00, 6, '2019-11-12 06:35:50'),
(1727, '\0A\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:35:51'),
(1728, '', 0.00, 6, '2019-11-12 06:35:51'),
(1729, '\0B\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:35:51'),
(1730, '', 0.00, 6, '2019-11-12 06:35:51'),
(1731, '', 0.00, 6, '2019-11-12 06:35:51'),
(1732, '\0D\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:35:51'),
(1733, '', 0.00, 6, '2019-11-12 06:35:51'),
(1734, '\0E\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:35:51'),
(1735, '', 0.00, 6, '2019-11-12 06:35:51'),
(1736, '\0F\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:35:51'),
(1737, '', 0.00, 6, '2019-11-12 06:35:52'),
(1738, '\0G\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:35:52'),
(1739, '', 0.00, 6, '2019-11-12 06:35:52'),
(1740, '\0H\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:35:52'),
(1741, '', 0.00, 6, '2019-11-12 06:35:52'),
(1742, '\0I\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:35:52'),
(1743, '', 0.00, 6, '2019-11-12 06:35:52'),
(1744, '\0J\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:35:52'),
(1745, '', 0.00, 6, '2019-11-12 06:35:52'),
(1746, '\0K\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:35:52'),
(1747, '', 0.00, 6, '2019-11-12 06:35:53'),
(1748, '\0L\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:35:53'),
(1749, '', 0.00, 6, '2019-11-12 06:35:53'),
(1750, '\0M\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:35:53'),
(1751, '', 0.00, 6, '2019-11-12 06:35:53'),
(1752, '\0N\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:35:53'),
(1753, '', 0.00, 6, '2019-11-12 06:35:53'),
(1754, '\0O\0\0\0O\0\0~', 0.00, 6, '2019-11-12 06:35:53'),
(1755, '\0O\0\0', 0.00, 6, '2019-11-12 06:35:54'),
(1756, '', 0.00, 6, '2019-11-12 06:35:54'),
(1757, '\0P\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:35:54'),
(1758, '', 0.00, 6, '2019-11-12 06:35:54'),
(1759, '\0Q\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:35:54'),
(1760, '', 0.00, 6, '2019-11-12 06:35:54'),
(1761, '\0R\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:35:54'),
(1762, '', 0.00, 6, '2019-11-12 06:35:54'),
(1763, '\0S\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:35:54'),
(1764, '', 0.00, 6, '2019-11-12 06:35:54'),
(1765, '\0T\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:35:54'),
(1766, '', 0.00, 6, '2019-11-12 06:35:54'),
(1767, '', 0.00, 6, '2019-11-12 06:35:54'),
(1768, '\0V\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:35:55'),
(1769, '', 0.00, 6, '2019-11-12 06:35:55'),
(1770, '\0W\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:35:55'),
(1771, '', 0.00, 6, '2019-11-12 06:35:55'),
(1772, '\0X\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:35:55'),
(1773, '', 0.00, 6, '2019-11-12 06:35:55'),
(1774, '\0Y\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:35:55'),
(1775, '', 0.00, 6, '2019-11-12 06:35:55'),
(1776, '', 0.00, 6, '2019-11-12 06:35:55'),
(1777, '\0[\0\0\0[\0\0~', 0.00, 6, '2019-11-12 06:35:55'),
(1778, '', 0.00, 6, '2019-11-12 06:35:55'),
(1779, '\0\\\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:35:55'),
(1780, '', 0.00, 6, '2019-11-12 06:35:55'),
(1781, '\0]\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:35:56'),
(1782, '', 0.00, 6, '2019-11-12 06:35:56'),
(1783, '\0^\0\0\0^\0\0~', 0.00, 6, '2019-11-12 06:35:56'),
(1784, '', 0.00, 6, '2019-11-12 06:35:56'),
(1785, '', 0.00, 6, '2019-11-12 06:35:56'),
(1786, '\0`\0\0\0`\0\0~', 0.00, 6, '2019-11-12 06:35:56'),
(1787, '', 0.00, 6, '2019-11-12 06:35:57'),
(1788, '\0a\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:35:57'),
(1789, '', 0.00, 6, '2019-11-12 06:35:57'),
(1790, '\0b\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:35:57'),
(1791, '\0b\0\0', 0.00, 6, '2019-11-12 06:35:57'),
(1792, '', 0.00, 6, '2019-11-12 06:35:57'),
(1793, '\0c\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:35:58'),
(1794, '', 0.00, 6, '2019-11-12 06:35:58'),
(1795, '\0d\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:35:58'),
(1796, '', 0.00, 6, '2019-11-12 06:35:58'),
(1797, '\0e\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:35:58'),
(1798, '', 0.00, 6, '2019-11-12 06:35:59'),
(1799, '\0f\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:35:59'),
(1800, '', 0.00, 6, '2019-11-12 06:35:59'),
(1801, '\0g\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:35:59'),
(1802, '', 0.00, 6, '2019-11-12 06:35:59'),
(1803, '\0h\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:35:59'),
(1804, '', 0.00, 6, '2019-11-12 06:35:59'),
(1805, '\0i\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:35:59'),
(1806, '', 0.00, 6, '2019-11-12 06:36:00'),
(1807, '\0j\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:36:00'),
(1808, '', 0.00, 6, '2019-11-12 06:36:00'),
(1809, '\0k\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:36:00'),
(1810, '\0k\0\0', 0.00, 6, '2019-11-12 06:36:00'),
(1811, '', 0.00, 6, '2019-11-12 06:36:00'),
(1812, '\0l\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:36:00'),
(1813, '', 0.00, 6, '2019-11-12 06:36:00'),
(1814, '\0m\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:36:00'),
(1815, '', 0.00, 6, '2019-11-12 06:36:00'),
(1816, '\0n\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:36:00'),
(1817, '', 0.00, 6, '2019-11-12 06:36:01'),
(1818, '\0o\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:36:01'),
(1819, '', 0.00, 6, '2019-11-12 06:36:01'),
(1820, '\0p\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:36:01'),
(1821, '', 0.00, 6, '2019-11-12 06:36:01'),
(1822, '', 0.00, 6, '2019-11-12 06:36:01'),
(1823, '\0r\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:36:01'),
(1824, '', 0.00, 6, '2019-11-12 06:36:02'),
(1825, '\0s\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:36:02'),
(1826, '', 0.00, 6, '2019-11-12 06:36:02'),
(1827, '\0t\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:36:02'),
(1828, '', 0.00, 6, '2019-11-12 06:36:03'),
(1829, '\0u\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:36:03'),
(1830, '', 0.00, 6, '2019-11-12 06:36:03'),
(1831, '\0v\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:36:03'),
(1832, '', 0.00, 6, '2019-11-12 06:36:03'),
(1833, '\0w\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:36:03'),
(1834, '', 0.00, 6, '2019-11-12 06:36:03'),
(1835, '\0x\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:36:03'),
(1836, '', 0.00, 6, '2019-11-12 06:36:03'),
(1837, '\0y\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:36:04'),
(1838, '', 0.00, 6, '2019-11-12 06:36:04'),
(1839, '\0z\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:36:04'),
(1840, '', 0.00, 6, '2019-11-12 06:36:04'),
(1841, '\0{\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:36:04'),
(1842, '', 0.00, 6, '2019-11-12 06:36:04'),
(1843, '\0|\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:36:04'),
(1844, '', 0.00, 6, '2019-11-12 06:36:04'),
(1845, '\0}\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:36:04'),
(1846, '\0}\0\0S', 0.00, 6, '2019-11-12 06:36:04'),
(1847, '', 0.00, 6, '2019-11-12 06:36:04'),
(1848, '\0~\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:36:04'),
(1849, '', 0.00, 6, '2019-11-12 06:36:04'),
(1850, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:04'),
(1851, '', 0.00, 6, '2019-11-12 06:36:05'),
(1852, '', 0.00, 6, '2019-11-12 06:36:05'),
(1853, '', 0.00, 6, '2019-11-12 06:36:05'),
(1854, '', 0.00, 6, '2019-11-12 06:36:05'),
(1855, '', 0.00, 6, '2019-11-12 06:36:05'),
(1856, '', 0.00, 6, '2019-11-12 06:36:05'),
(1857, '', 0.00, 6, '2019-11-12 06:36:05'),
(1858, '', 0.00, 6, '2019-11-12 06:36:06'),
(1859, '', 0.00, 6, '2019-11-12 06:36:06'),
(1860, '', 0.00, 6, '2019-11-12 06:36:06'),
(1861, '', 0.00, 6, '2019-11-12 06:36:06'),
(1862, '', 0.00, 6, '2019-11-12 06:36:06'),
(1863, '', 0.00, 6, '2019-11-12 06:36:06'),
(1864, '', 0.00, 6, '2019-11-12 06:36:06'),
(1865, '', 0.00, 6, '2019-11-12 06:36:06'),
(1866, '', 0.00, 6, '2019-11-12 06:36:07'),
(1867, '', 0.00, 6, '2019-11-12 06:36:07'),
(1868, '', 0.00, 6, '2019-11-12 06:36:07'),
(1869, '', 0.00, 6, '2019-11-12 06:36:07'),
(1870, '', 0.00, 6, '2019-11-12 06:36:07'),
(1871, '', 0.00, 6, '2019-11-12 06:36:08'),
(1872, '', 0.00, 6, '2019-11-12 06:36:08'),
(1873, '', 0.00, 6, '2019-11-12 06:36:08'),
(1874, '', 0.00, 6, '2019-11-12 06:36:08'),
(1875, '', 0.00, 6, '2019-11-12 06:36:08'),
(1876, '', 0.00, 6, '2019-11-12 06:36:08'),
(1877, '', 0.00, 6, '2019-11-12 06:36:08'),
(1878, '', 0.00, 6, '2019-11-12 06:36:08'),
(1879, '', 0.00, 6, '2019-11-12 06:36:08'),
(1880, '', 0.00, 6, '2019-11-12 06:36:08'),
(1881, '', 0.00, 6, '2019-11-12 06:36:08'),
(1882, '', 0.00, 6, '2019-11-12 06:36:08'),
(1883, '', 0.00, 6, '2019-11-12 06:36:08'),
(1884, '', 0.00, 6, '2019-11-12 06:36:09'),
(1885, '', 0.00, 6, '2019-11-12 06:36:09'),
(1886, '', 0.00, 6, '2019-11-12 06:36:09'),
(1887, '', 0.00, 6, '2019-11-12 06:36:09'),
(1888, '', 0.00, 6, '2019-11-12 06:36:09'),
(1889, '', 0.00, 6, '2019-11-12 06:36:09'),
(1890, '', 0.00, 6, '2019-11-12 06:36:09'),
(1891, '', 0.00, 6, '2019-11-12 06:36:09'),
(1892, '', 0.00, 6, '2019-11-12 06:36:09'),
(1893, '', 0.00, 6, '2019-11-12 06:36:09'),
(1894, '', 0.00, 6, '2019-11-12 06:36:09'),
(1895, '', 0.00, 6, '2019-11-12 06:36:09'),
(1896, '', 0.00, 6, '2019-11-12 06:36:09'),
(1897, '', 0.00, 6, '2019-11-12 06:36:09'),
(1898, '', 0.00, 6, '2019-11-12 06:36:09'),
(1899, '', 0.00, 6, '2019-11-12 06:36:09'),
(1900, '', 0.00, 6, '2019-11-12 06:36:09'),
(1901, '', 0.00, 6, '2019-11-12 06:36:09'),
(1902, '', 0.00, 6, '2019-11-12 06:36:09'),
(1903, '', 0.00, 6, '2019-11-12 06:36:09'),
(1904, '', 0.00, 6, '2019-11-12 06:36:10'),
(1905, '', 0.00, 6, '2019-11-12 06:36:10'),
(1906, '', 0.00, 6, '2019-11-12 06:36:10'),
(1907, '', 0.00, 6, '2019-11-12 06:36:10'),
(1908, '', 0.00, 6, '2019-11-12 06:36:10'),
(1909, '', 0.00, 6, '2019-11-12 06:36:10'),
(1910, '', 0.00, 6, '2019-11-12 06:36:10'),
(1911, '', 0.00, 6, '2019-11-12 06:36:10'),
(1912, '', 0.00, 6, '2019-11-12 06:36:10'),
(1913, '', 0.00, 6, '2019-11-12 06:36:10'),
(1914, '', 0.00, 6, '2019-11-12 06:36:10'),
(1915, '', 0.00, 6, '2019-11-12 06:36:10'),
(1916, '', 0.00, 6, '2019-11-12 06:36:10'),
(1917, '', 0.00, 6, '2019-11-12 06:36:10'),
(1918, '', 0.00, 6, '2019-11-12 06:36:10'),
(1919, '', 0.00, 6, '2019-11-12 06:36:10'),
(1920, '', 0.00, 6, '2019-11-12 06:36:10'),
(1921, '', 0.00, 6, '2019-11-12 06:36:10'),
(1922, '', 0.00, 6, '2019-11-12 06:36:10'),
(1923, '', 0.00, 6, '2019-11-12 06:36:10'),
(1924, '', 0.00, 6, '2019-11-12 06:36:10'),
(1925, '', 0.00, 6, '2019-11-12 06:36:11'),
(1926, '', 0.00, 6, '2019-11-12 06:36:11'),
(1927, '', 0.00, 6, '2019-11-12 06:36:11'),
(1928, '', 0.00, 6, '2019-11-12 06:36:11'),
(1929, '', 0.00, 6, '2019-11-12 06:36:11'),
(1930, '', 0.00, 6, '2019-11-12 06:36:11'),
(1931, '', 0.00, 6, '2019-11-12 06:36:11'),
(1932, '', 0.00, 6, '2019-11-12 06:36:11'),
(1933, '', 0.00, 6, '2019-11-12 06:36:11'),
(1934, '', 0.00, 6, '2019-11-12 06:36:11'),
(1935, '', 0.00, 6, '2019-11-12 06:36:11'),
(1936, '', 0.00, 6, '2019-11-12 06:36:11'),
(1937, '', 0.00, 6, '2019-11-12 06:36:11'),
(1938, '', 0.00, 6, '2019-11-12 06:36:11'),
(1939, '', 0.00, 6, '2019-11-12 06:36:11'),
(1940, '', 0.00, 6, '2019-11-12 06:36:11'),
(1941, '', 0.00, 6, '2019-11-12 06:36:11'),
(1942, '', 0.00, 6, '2019-11-12 06:36:11'),
(1943, '', 0.00, 6, '2019-11-12 06:36:11'),
(1944, '', 0.00, 6, '2019-11-12 06:36:12'),
(1945, '', 0.00, 6, '2019-11-12 06:36:12'),
(1946, '', 0.00, 6, '2019-11-12 06:36:12'),
(1947, '', 0.00, 6, '2019-11-12 06:36:12'),
(1948, '', 0.00, 6, '2019-11-12 06:36:12'),
(1949, '', 0.00, 6, '2019-11-12 06:36:12'),
(1950, '', 0.00, 6, '2019-11-12 06:36:12'),
(1951, '', 0.00, 6, '2019-11-12 06:36:12'),
(1952, '', 0.00, 6, '2019-11-12 06:36:12'),
(1953, '', 0.00, 6, '2019-11-12 06:36:12'),
(1954, '', 0.00, 6, '2019-11-12 06:36:12'),
(1955, '', 0.00, 6, '2019-11-12 06:36:12'),
(1956, '', 0.00, 6, '2019-11-12 06:36:12'),
(1957, '', 0.00, 6, '2019-11-12 06:36:12'),
(1958, '', 0.00, 6, '2019-11-12 06:36:12'),
(1959, '', 0.00, 6, '2019-11-12 06:36:12'),
(1960, '', 0.00, 6, '2019-11-12 06:36:13'),
(1961, '', 0.00, 6, '2019-11-12 06:36:13'),
(1962, '', 0.00, 6, '2019-11-12 06:36:13'),
(1963, '', 0.00, 6, '2019-11-12 06:36:13'),
(1964, '', 0.00, 6, '2019-11-12 06:36:13'),
(1965, '', 0.00, 6, '2019-11-12 06:36:13'),
(1966, '', 0.00, 6, '2019-11-12 06:36:13'),
(1967, '', 0.00, 6, '2019-11-12 06:36:13'),
(1968, '', 0.00, 6, '2019-11-12 06:36:13'),
(1969, '', 0.00, 6, '2019-11-12 06:36:13'),
(1970, '', 0.00, 6, '2019-11-12 06:36:13'),
(1971, '', 0.00, 6, '2019-11-12 06:36:13'),
(1972, '', 0.00, 6, '2019-11-12 06:36:13'),
(1973, '', 0.00, 6, '2019-11-12 06:36:13'),
(1974, '', 0.00, 6, '2019-11-12 06:36:13'),
(1975, '', 0.00, 6, '2019-11-12 06:36:13'),
(1976, '', 0.00, 6, '2019-11-12 06:36:13'),
(1977, '', 0.00, 6, '2019-11-12 06:36:13'),
(1978, '', 0.00, 6, '2019-11-12 06:36:13'),
(1979, '', 0.00, 6, '2019-11-12 06:36:13'),
(1980, '', 0.00, 6, '2019-11-12 06:36:14'),
(1981, '', 0.00, 6, '2019-11-12 06:36:14'),
(1982, '', 0.00, 6, '2019-11-12 06:36:14'),
(1983, '', 0.00, 6, '2019-11-12 06:36:14'),
(1984, '', 0.00, 6, '2019-11-12 06:36:14'),
(1985, '', 0.00, 6, '2019-11-12 06:36:14'),
(1986, '', 0.00, 6, '2019-11-12 06:36:14'),
(1987, '', 0.00, 6, '2019-11-12 06:36:14'),
(1988, '', 0.00, 6, '2019-11-12 06:36:14'),
(1989, '', 0.00, 6, '2019-11-12 06:36:14'),
(1990, '', 0.00, 6, '2019-11-12 06:36:14'),
(1991, '', 0.00, 6, '2019-11-12 06:36:14'),
(1992, '', 0.00, 6, '2019-11-12 06:36:14'),
(1993, '', 0.00, 6, '2019-11-12 06:36:14'),
(1994, '', 0.00, 6, '2019-11-12 06:36:15'),
(1995, '', 0.00, 6, '2019-11-12 06:36:15'),
(1996, '', 0.00, 6, '2019-11-12 06:36:15'),
(1997, '', 0.00, 6, '2019-11-12 06:36:15'),
(1998, '', 0.00, 6, '2019-11-12 06:36:15'),
(1999, '', 0.00, 6, '2019-11-12 06:36:15'),
(2000, '', 0.00, 6, '2019-11-12 06:36:15'),
(2001, '', 0.00, 6, '2019-11-12 06:36:15'),
(2002, '', 0.00, 6, '2019-11-12 06:36:15'),
(2003, '', 0.00, 6, '2019-11-12 06:36:15'),
(2004, '', 0.00, 6, '2019-11-12 06:36:15'),
(2005, '', 0.00, 6, '2019-11-12 06:36:15'),
(2006, '', 0.00, 6, '2019-11-12 06:36:15'),
(2007, '', 0.00, 6, '2019-11-12 06:36:15'),
(2008, '', 0.00, 6, '2019-11-12 06:36:15'),
(2009, '', 0.00, 6, '2019-11-12 06:36:15'),
(2010, '', 0.00, 6, '2019-11-12 06:36:15'),
(2011, '', 0.00, 6, '2019-11-12 06:36:16'),
(2012, '', 0.00, 6, '2019-11-12 06:36:16'),
(2013, '', 0.00, 6, '2019-11-12 06:36:16'),
(2014, '', 0.00, 6, '2019-11-12 06:36:16'),
(2015, '', 0.00, 6, '2019-11-12 06:36:16'),
(2016, '', 0.00, 6, '2019-11-12 06:36:16'),
(2017, '', 0.00, 6, '2019-11-12 06:36:16'),
(2018, '', 0.00, 6, '2019-11-12 06:36:16'),
(2019, '', 0.00, 6, '2019-11-12 06:36:16'),
(2020, '', 0.00, 6, '2019-11-12 06:36:16'),
(2021, '', 0.00, 6, '2019-11-12 06:36:16'),
(2022, '', 0.00, 6, '2019-11-12 06:36:16'),
(2023, '', 0.00, 6, '2019-11-12 06:36:16'),
(2024, '', 0.00, 6, '2019-11-12 06:36:16'),
(2025, '', 0.00, 6, '2019-11-12 06:36:16'),
(2026, '', 0.00, 6, '2019-11-12 06:36:16'),
(2027, '', 0.00, 6, '2019-11-12 06:36:16'),
(2028, '', 0.00, 6, '2019-11-12 06:36:16'),
(2029, '', 0.00, 6, '2019-11-12 06:36:16'),
(2030, '', 0.00, 6, '2019-11-12 06:36:17'),
(2031, '', 0.00, 6, '2019-11-12 06:36:17'),
(2032, '', 0.00, 6, '2019-11-12 06:36:17'),
(2033, '', 0.00, 6, '2019-11-12 06:36:17'),
(2034, '', 0.00, 6, '2019-11-12 06:36:18'),
(2035, '', 0.00, 6, '2019-11-12 06:36:18'),
(2036, '', 0.00, 6, '2019-11-12 06:36:18'),
(2037, '', 0.00, 6, '2019-11-12 06:36:18'),
(2038, '', 0.00, 6, '2019-11-12 06:36:18'),
(2039, '', 0.00, 6, '2019-11-12 06:36:18'),
(2040, '', 0.00, 6, '2019-11-12 06:36:18'),
(2041, '', 0.00, 6, '2019-11-12 06:36:19'),
(2042, '', 0.00, 6, '2019-11-12 06:36:19'),
(2043, '', 0.00, 6, '2019-11-12 06:36:19'),
(2044, '', 0.00, 6, '2019-11-12 06:36:19'),
(2045, '', 0.00, 6, '2019-11-12 06:36:19'),
(2046, '', 0.00, 6, '2019-11-12 06:36:19'),
(2047, '', 0.00, 6, '2019-11-12 06:36:19'),
(2048, '', 0.00, 6, '2019-11-12 06:36:19'),
(2049, '', 0.00, 6, '2019-11-12 06:36:19'),
(2050, '', 0.00, 6, '2019-11-12 06:36:19'),
(2051, '', 0.00, 6, '2019-11-12 06:36:19'),
(2052, '', 0.00, 6, '2019-11-12 06:36:19'),
(2053, '', 0.00, 6, '2019-11-12 06:36:19'),
(2054, '', 0.00, 6, '2019-11-12 06:36:19'),
(2055, '', 0.00, 6, '2019-11-12 06:36:19'),
(2056, '', 0.00, 6, '2019-11-12 06:36:20'),
(2057, '', 0.00, 6, '2019-11-12 06:36:20'),
(2058, '', 0.00, 6, '2019-11-12 06:36:20'),
(2059, '', 0.00, 6, '2019-11-12 06:36:20'),
(2060, '', 0.00, 6, '2019-11-12 06:36:20'),
(2061, '', 0.00, 6, '2019-11-12 06:36:20'),
(2062, '', 0.00, 6, '2019-11-12 06:36:20'),
(2063, '', 0.00, 6, '2019-11-12 06:36:20'),
(2064, '', 0.00, 6, '2019-11-12 06:36:20'),
(2065, '', 0.00, 6, '2019-11-12 06:36:20'),
(2066, '', 0.00, 6, '2019-11-12 06:36:20'),
(2067, '', 0.00, 6, '2019-11-12 06:36:20'),
(2068, '', 0.00, 6, '2019-11-12 06:36:20'),
(2069, '', 0.00, 6, '2019-11-12 06:36:20'),
(2070, '', 0.00, 6, '2019-11-12 06:36:20'),
(2071, '', 0.00, 6, '2019-11-12 06:36:20'),
(2072, '', 0.00, 6, '2019-11-12 06:36:21'),
(2073, '', 0.00, 6, '2019-11-12 06:36:21'),
(2074, '', 0.00, 6, '2019-11-12 06:36:21'),
(2075, '', 0.00, 6, '2019-11-12 06:36:21'),
(2076, '', 0.00, 6, '2019-11-12 06:36:21'),
(2077, '', 0.00, 6, '2019-11-12 06:36:21'),
(2078, '', 0.00, 6, '2019-11-12 06:36:21'),
(2079, '', 0.00, 6, '2019-11-12 06:36:21'),
(2080, '', 0.00, 6, '2019-11-12 06:36:21'),
(2081, '', 0.00, 6, '2019-11-12 06:36:21'),
(2082, '', 0.00, 6, '2019-11-12 06:36:21'),
(2083, '', 0.00, 6, '2019-11-12 06:36:21'),
(2084, '', 0.00, 6, '2019-11-12 06:36:21'),
(2085, '', 0.00, 6, '2019-11-12 06:36:21'),
(2086, '', 0.00, 6, '2019-11-12 06:36:21'),
(2087, '', 0.00, 6, '2019-11-12 06:36:21'),
(2088, '', 0.00, 6, '2019-11-12 06:36:21'),
(2089, '', 0.00, 6, '2019-11-12 06:36:21'),
(2090, '', 0.00, 6, '2019-11-12 06:36:21'),
(2091, '', 0.00, 6, '2019-11-12 06:36:21'),
(2092, '', 0.00, 6, '2019-11-12 06:36:21'),
(2093, '', 0.00, 6, '2019-11-12 06:36:21'),
(2094, '', 0.00, 6, '2019-11-12 06:36:22'),
(2095, '', 0.00, 6, '2019-11-12 06:36:22'),
(2096, '', 0.00, 6, '2019-11-12 06:36:22'),
(2097, '', 0.00, 6, '2019-11-12 06:36:22'),
(2098, '', 0.00, 6, '2019-11-12 06:36:22'),
(2099, '', 0.00, 6, '2019-11-12 06:36:22'),
(2100, '', 0.00, 6, '2019-11-12 06:36:22'),
(2101, '', 0.00, 6, '2019-11-12 06:36:22'),
(2102, '\0\0\0', 0.00, 6, '2019-11-12 06:36:22'),
(2103, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:22'),
(2104, '', 0.00, 6, '2019-11-12 06:36:22'),
(2105, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:23'),
(2106, '', 0.00, 6, '2019-11-12 06:36:23'),
(2107, '', 0.00, 6, '2019-11-12 06:36:23'),
(2108, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:23'),
(2109, '', 0.00, 6, '2019-11-12 06:36:23'),
(2110, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:23'),
(2111, '', 0.00, 6, '2019-11-12 06:36:23'),
(2112, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:23'),
(2113, '', 0.00, 6, '2019-11-12 06:36:23'),
(2114, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:23'),
(2115, '', 0.00, 6, '2019-11-12 06:36:23'),
(2116, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:23'),
(2117, '', 0.00, 6, '2019-11-12 06:36:23'),
(2118, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:23'),
(2119, '', 0.00, 6, '2019-11-12 06:36:23'),
(2120, '', 0.00, 6, '2019-11-12 06:36:23'),
(2121, '\0', 0.00, 6, '2019-11-12 06:36:23'),
(2122, '\0\0\0', 0.00, 6, '2019-11-12 06:36:24'),
(2123, '\0\0~', 0.00, 6, '2019-11-12 06:36:24'),
(2124, '\0', 0.00, 6, '2019-11-12 06:36:24'),
(2125, '', 0.00, 6, '2019-11-12 06:36:24'),
(2126, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:24'),
(2127, '\0\0\0', 0.00, 6, '2019-11-12 06:36:24'),
(2128, '', 0.00, 6, '2019-11-12 06:36:25'),
(2129, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:25'),
(2130, '', 0.00, 6, '2019-11-12 06:36:25'),
(2131, '\0\r\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:36:25'),
(2132, '', 0.00, 6, '2019-11-12 06:36:25'),
(2133, '', 0.00, 6, '2019-11-12 06:36:25'),
(2134, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:26'),
(2135, '', 0.00, 6, '2019-11-12 06:36:26'),
(2136, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:26'),
(2137, '', 0.00, 6, '2019-11-12 06:36:26'),
(2138, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:26'),
(2139, '', 0.00, 6, '2019-11-12 06:36:26'),
(2140, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:26'),
(2141, '', 0.00, 6, '2019-11-12 06:36:26');
INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(2142, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:26'),
(2143, '', 0.00, 6, '2019-11-12 06:36:26'),
(2144, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:26'),
(2145, '', 0.00, 6, '2019-11-12 06:36:27'),
(2146, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:27'),
(2147, '', 0.00, 6, '2019-11-12 06:36:27'),
(2148, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:27'),
(2149, '', 0.00, 6, '2019-11-12 06:36:28'),
(2150, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:28'),
(2151, '', 0.00, 6, '2019-11-12 06:36:28'),
(2152, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:28'),
(2153, '\0\0\0', 0.00, 6, '2019-11-12 06:36:28'),
(2154, '', 0.00, 6, '2019-11-12 06:36:28'),
(2155, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:28'),
(2156, '', 0.00, 6, '2019-11-12 06:36:28'),
(2157, '\0\Z\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:36:28'),
(2158, '', 0.00, 6, '2019-11-12 06:36:28'),
(2159, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:28'),
(2160, '', 0.00, 6, '2019-11-12 06:36:28'),
(2161, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:28'),
(2162, '', 0.00, 6, '2019-11-12 06:36:28'),
(2163, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:28'),
(2164, '', 0.00, 6, '2019-11-12 06:36:28'),
(2165, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:29'),
(2166, '', 0.00, 6, '2019-11-12 06:36:29'),
(2167, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:29'),
(2168, '', 0.00, 6, '2019-11-12 06:36:29'),
(2169, '\0 \0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:36:29'),
(2170, '', 0.00, 6, '2019-11-12 06:36:29'),
(2171, '\0!\0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:36:29'),
(2172, '', 0.00, 6, '2019-11-12 06:36:30'),
(2173, '\0&quot;\0\0\0&quot;\0\0~', 0.00, 6, '2019-11-12 06:36:30'),
(2174, '', 0.00, 6, '2019-11-12 06:36:30'),
(2175, '\0#\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:36:30'),
(2176, '', 0.00, 6, '2019-11-12 06:36:30'),
(2177, '\0$\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:36:30'),
(2178, '', 0.00, 6, '2019-11-12 06:36:30'),
(2179, '\0%\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:36:30'),
(2180, '', 0.00, 6, '2019-11-12 06:36:30'),
(2181, '\0&amp;\0\0\0&amp;\0\0~', 0.00, 6, '2019-11-12 06:36:31'),
(2182, '', 0.00, 6, '2019-11-12 06:36:31'),
(2183, '\0\'\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:36:31'),
(2184, '', 0.00, 6, '2019-11-12 06:36:31'),
(2185, '\0(\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:36:31'),
(2186, '', 0.00, 6, '2019-11-12 06:36:31'),
(2187, '\0)\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:36:31'),
(2188, '', 0.00, 6, '2019-11-12 06:36:31'),
(2189, '\0*\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:36:31'),
(2190, '', 0.00, 6, '2019-11-12 06:36:31'),
(2191, '\0+\0\0\0+\0\0~', 0.00, 6, '2019-11-12 06:36:32'),
(2192, '', 0.00, 6, '2019-11-12 06:36:32'),
(2193, '\0', 0.00, 6, '2019-11-12 06:36:32'),
(2194, '\0', 0.00, 6, '2019-11-12 06:36:32'),
(2195, '\0-\0\0\0-\0\0~', 0.00, 6, '2019-11-12 06:36:32'),
(2196, '', 0.00, 6, '2019-11-12 06:36:32'),
(2197, '\0.\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:36:32'),
(2198, '', 0.00, 6, '2019-11-12 06:36:32'),
(2199, '\0/\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:36:32'),
(2200, '', 0.00, 6, '2019-11-12 06:36:32'),
(2201, '\00\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:36:32'),
(2202, '', 0.00, 6, '2019-11-12 06:36:33'),
(2203, '', 0.00, 6, '2019-11-12 06:36:33'),
(2204, '\02\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:36:33'),
(2205, '', 0.00, 6, '2019-11-12 06:36:33'),
(2206, '\03\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:36:33'),
(2207, '', 0.00, 6, '2019-11-12 06:36:33'),
(2208, '\04\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:36:33'),
(2209, '', 0.00, 6, '2019-11-12 06:36:33'),
(2210, '\05\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:36:33'),
(2211, '', 0.00, 6, '2019-11-12 06:36:33'),
(2212, '\06\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:36:33'),
(2213, '\06\0\0', 0.00, 6, '2019-11-12 06:36:33'),
(2214, '', 0.00, 6, '2019-11-12 06:36:33'),
(2215, '\07\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:36:33'),
(2216, '', 0.00, 6, '2019-11-12 06:36:33'),
(2217, '\08\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:36:33'),
(2218, '', 0.00, 6, '2019-11-12 06:36:33'),
(2219, '\09\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:36:34'),
(2220, '', 0.00, 6, '2019-11-12 06:36:34'),
(2221, '\0:\0\0\0:\0\0~', 0.00, 6, '2019-11-12 06:36:34'),
(2222, '', 0.00, 6, '2019-11-12 06:36:34'),
(2223, '\0;\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:36:34'),
(2224, '', 0.00, 6, '2019-11-12 06:36:34'),
(2225, '\0&lt;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:36:34'),
(2226, '', 0.00, 6, '2019-11-12 06:36:34'),
(2227, '\0=\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:36:34'),
(2228, '', 0.00, 6, '2019-11-12 06:36:34'),
(2229, '\0&gt;\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:36:34'),
(2230, '', 0.00, 6, '2019-11-12 06:36:34'),
(2231, '\0?\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:36:34'),
(2232, '\0?\0\0!\0\0\0@\0\0\0', 0.00, 6, '2019-11-12 06:36:34'),
(2233, '\0@\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:36:34'),
(2234, '', 0.00, 6, '2019-11-12 06:36:34'),
(2235, '\0A\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:36:34'),
(2236, '', 0.00, 6, '2019-11-12 06:36:35'),
(2237, '\0B\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:36:35'),
(2238, '', 0.00, 6, '2019-11-12 06:36:35'),
(2239, '\0C\0\0\0C\0\0~', 0.00, 6, '2019-11-12 06:36:35'),
(2240, '', 0.00, 6, '2019-11-12 06:36:35'),
(2241, '\0D\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:36:35'),
(2242, '', 0.00, 6, '2019-11-12 06:36:35'),
(2243, '\0E\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:36:35'),
(2244, '', 0.00, 6, '2019-11-12 06:36:35'),
(2245, '\0F\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:36:36'),
(2246, '', 0.00, 6, '2019-11-12 06:36:36'),
(2247, '\0G\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:36:36'),
(2248, '', 0.00, 6, '2019-11-12 06:36:36'),
(2249, '\0H\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:36:36'),
(2250, '', 0.00, 6, '2019-11-12 06:36:36'),
(2251, '\0I\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:36:36'),
(2252, '', 0.00, 6, '2019-11-12 06:36:36'),
(2253, '\0J\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:36:36'),
(2254, '', 0.00, 6, '2019-11-12 06:36:36'),
(2255, '\0K\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:36:36'),
(2256, '', 0.00, 6, '2019-11-12 06:36:36'),
(2257, '\0L\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:36:36'),
(2258, '', 0.00, 6, '2019-11-12 06:36:36'),
(2259, '\0M\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:36:36'),
(2260, '', 0.00, 6, '2019-11-12 06:36:36'),
(2261, '\0N\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:36:36'),
(2262, '', 0.00, 6, '2019-11-12 06:36:37'),
(2263, '\0O\0\0\0O\0\0~', 0.00, 6, '2019-11-12 06:36:37'),
(2264, '', 0.00, 6, '2019-11-12 06:36:37'),
(2265, '\0P\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:36:37'),
(2266, '', 0.00, 6, '2019-11-12 06:36:37'),
(2267, '\0Q\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:36:37'),
(2268, '', 0.00, 6, '2019-11-12 06:36:37'),
(2269, '\0R\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:36:38'),
(2270, '', 0.00, 6, '2019-11-12 06:36:38'),
(2271, '\0S\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:36:38'),
(2272, '', 0.00, 6, '2019-11-12 06:36:38'),
(2273, '\0T\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:36:38'),
(2274, '\0T\0\0R', 0.00, 6, '2019-11-12 06:36:38'),
(2275, '', 0.00, 6, '2019-11-12 06:36:38'),
(2276, '\0U\0\0\0U\0\0~', 0.00, 6, '2019-11-12 06:36:38'),
(2277, '', 0.00, 6, '2019-11-12 06:36:38'),
(2278, '\0V\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:36:38'),
(2279, '', 0.00, 6, '2019-11-12 06:36:38'),
(2280, '\0W\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:36:38'),
(2281, '', 0.00, 6, '2019-11-12 06:36:38'),
(2282, '\0X\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:36:38'),
(2283, '', 0.00, 6, '2019-11-12 06:36:38'),
(2284, '\0Y\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:36:38'),
(2285, '', 0.00, 6, '2019-11-12 06:36:38'),
(2286, '\0Z\0\0\0Z\0\0~', 0.00, 6, '2019-11-12 06:36:38'),
(2287, '', 0.00, 6, '2019-11-12 06:36:38'),
(2288, '', 0.00, 6, '2019-11-12 06:36:39'),
(2289, '\0\\\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:36:39'),
(2290, '', 0.00, 6, '2019-11-12 06:36:39'),
(2291, '\0]\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:36:39'),
(2292, '', 0.00, 6, '2019-11-12 06:36:39'),
(2293, '', 0.00, 6, '2019-11-12 06:36:39'),
(2294, '\0_\0\0\0_\0\0~', 0.00, 6, '2019-11-12 06:36:39'),
(2295, '\0_\0\0*\0\0\0`\0\0\0', 0.00, 6, '2019-11-12 06:36:39'),
(2296, '\0`\0\0\0`\0\0~', 0.00, 6, '2019-11-12 06:36:39'),
(2297, '', 0.00, 6, '2019-11-12 06:36:39'),
(2298, '\0a\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:36:39'),
(2299, '\0a\0\0', 0.00, 6, '2019-11-12 06:36:39'),
(2300, '', 0.00, 6, '2019-11-12 06:36:39'),
(2301, '\0b\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:36:39'),
(2302, '', 0.00, 6, '2019-11-12 06:36:39'),
(2303, '\0c\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:36:40'),
(2304, '', 0.00, 6, '2019-11-12 06:36:40'),
(2305, '\0d\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:36:40'),
(2306, '', 0.00, 6, '2019-11-12 06:36:40'),
(2307, '\0e\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:36:40'),
(2308, '', 0.00, 6, '2019-11-12 06:36:40'),
(2309, '\0f\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:36:40'),
(2310, '', 0.00, 6, '2019-11-12 06:36:40'),
(2311, '\0g\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:36:40'),
(2312, '', 0.00, 6, '2019-11-12 06:36:40'),
(2313, '\0h\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:36:41'),
(2314, '', 0.00, 6, '2019-11-12 06:36:41'),
(2315, '\0i\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:36:41'),
(2316, '', 0.00, 6, '2019-11-12 06:36:41'),
(2317, '\0j\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:36:41'),
(2318, '', 0.00, 6, '2019-11-12 06:36:41'),
(2319, '\0k\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:36:41'),
(2320, '', 0.00, 6, '2019-11-12 06:36:41'),
(2321, '\0l\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:36:41'),
(2322, '', 0.00, 6, '2019-11-12 06:36:41'),
(2323, '\0m\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:36:42'),
(2324, '', 0.00, 6, '2019-11-12 06:36:42'),
(2325, '\0n\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:36:42'),
(2326, '', 0.00, 6, '2019-11-12 06:36:42'),
(2327, '\0o\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:36:42'),
(2328, '', 0.00, 6, '2019-11-12 06:36:42'),
(2329, '\0p\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:36:42'),
(2330, '', 0.00, 6, '2019-11-12 06:36:42'),
(2331, '', 0.00, 6, '2019-11-12 06:36:42'),
(2332, '\0r\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:36:42'),
(2333, '', 0.00, 6, '2019-11-12 06:36:42'),
(2334, '\0s\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:36:42'),
(2335, '', 0.00, 6, '2019-11-12 06:36:42'),
(2336, '\0t\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:36:42'),
(2337, '', 0.00, 6, '2019-11-12 06:36:42'),
(2338, '\0u\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:36:43'),
(2339, '', 0.00, 6, '2019-11-12 06:36:43'),
(2340, '\0v\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:36:43'),
(2341, '', 0.00, 6, '2019-11-12 06:36:43'),
(2342, '\0w\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:36:43'),
(2343, '\0w\0\0', 0.00, 6, '2019-11-12 06:36:43'),
(2344, '', 0.00, 6, '2019-11-12 06:36:43'),
(2345, '\0x\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:36:43'),
(2346, '', 0.00, 6, '2019-11-12 06:36:43'),
(2347, '\0y\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:36:43'),
(2348, '', 0.00, 6, '2019-11-12 06:36:43'),
(2349, '\0z\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:36:43'),
(2350, '', 0.00, 6, '2019-11-12 06:36:44'),
(2351, '\0{\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:36:44'),
(2352, '', 0.00, 6, '2019-11-12 06:36:44'),
(2353, '\0|\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:36:44'),
(2354, '', 0.00, 6, '2019-11-12 06:36:44'),
(2355, '\0}\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:36:44'),
(2356, '', 0.00, 6, '2019-11-12 06:36:44'),
(2357, '\0~\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:36:44'),
(2358, '', 0.00, 6, '2019-11-12 06:36:44'),
(2359, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:44'),
(2360, '', 0.00, 6, '2019-11-12 06:36:44'),
(2361, '', 0.00, 6, '2019-11-12 06:36:44'),
(2362, '', 0.00, 6, '2019-11-12 06:36:44'),
(2363, '', 0.00, 6, '2019-11-12 06:36:44'),
(2364, '', 0.00, 6, '2019-11-12 06:36:44'),
(2365, '', 0.00, 6, '2019-11-12 06:36:44'),
(2366, '', 0.00, 6, '2019-11-12 06:36:44'),
(2367, '', 0.00, 6, '2019-11-12 06:36:44'),
(2368, '', 0.00, 6, '2019-11-12 06:36:44'),
(2369, '', 0.00, 6, '2019-11-12 06:36:44'),
(2370, '', 0.00, 6, '2019-11-12 06:36:44'),
(2371, '', 0.00, 6, '2019-11-12 06:36:45'),
(2372, '', 0.00, 6, '2019-11-12 06:36:45'),
(2373, '', 0.00, 6, '2019-11-12 06:36:45'),
(2374, '', 0.00, 6, '2019-11-12 06:36:45'),
(2375, '', 0.00, 6, '2019-11-12 06:36:45'),
(2376, '', 0.00, 6, '2019-11-12 06:36:45'),
(2377, '', 0.00, 6, '2019-11-12 06:36:45'),
(2378, '', 0.00, 6, '2019-11-12 06:36:45'),
(2379, '', 0.00, 6, '2019-11-12 06:36:45'),
(2380, '', 0.00, 6, '2019-11-12 06:36:45'),
(2381, '', 0.00, 6, '2019-11-12 06:36:45'),
(2382, '', 0.00, 6, '2019-11-12 06:36:45'),
(2383, '', 0.00, 6, '2019-11-12 06:36:46'),
(2384, '', 0.00, 6, '2019-11-12 06:36:46'),
(2385, '', 0.00, 6, '2019-11-12 06:36:46'),
(2386, '', 0.00, 6, '2019-11-12 06:36:46'),
(2387, '', 0.00, 6, '2019-11-12 06:36:46'),
(2388, '', 0.00, 6, '2019-11-12 06:36:46'),
(2389, '', 0.00, 6, '2019-11-12 06:36:46'),
(2390, '', 0.00, 6, '2019-11-12 06:36:46'),
(2391, '', 0.00, 6, '2019-11-12 06:36:46'),
(2392, '', 0.00, 6, '2019-11-12 06:36:46'),
(2393, '', 0.00, 6, '2019-11-12 06:36:46'),
(2394, '', 0.00, 6, '2019-11-12 06:36:46'),
(2395, '', 0.00, 6, '2019-11-12 06:36:46'),
(2396, '', 0.00, 6, '2019-11-12 06:36:46'),
(2397, '', 0.00, 6, '2019-11-12 06:36:46'),
(2398, '', 0.00, 6, '2019-11-12 06:36:46'),
(2399, '', 0.00, 6, '2019-11-12 06:36:46'),
(2400, '', 0.00, 6, '2019-11-12 06:36:46'),
(2401, '', 0.00, 6, '2019-11-12 06:36:46'),
(2402, '', 0.00, 6, '2019-11-12 06:36:47'),
(2403, '', 0.00, 6, '2019-11-12 06:36:47'),
(2404, '', 0.00, 6, '2019-11-12 06:36:47'),
(2405, '', 0.00, 6, '2019-11-12 06:36:47'),
(2406, '', 0.00, 6, '2019-11-12 06:36:47'),
(2407, '', 0.00, 6, '2019-11-12 06:36:47'),
(2408, '', 0.00, 6, '2019-11-12 06:36:47'),
(2409, '', 0.00, 6, '2019-11-12 06:36:47'),
(2410, '', 0.00, 6, '2019-11-12 06:36:47'),
(2411, '', 0.00, 6, '2019-11-12 06:36:48'),
(2412, '', 0.00, 6, '2019-11-12 06:36:48'),
(2413, '', 0.00, 6, '2019-11-12 06:36:48'),
(2414, '', 0.00, 6, '2019-11-12 06:36:48'),
(2415, '', 0.00, 6, '2019-11-12 06:36:48'),
(2416, '', 0.00, 6, '2019-11-12 06:36:48'),
(2417, '', 0.00, 6, '2019-11-12 06:36:48'),
(2418, '', 0.00, 6, '2019-11-12 06:36:48'),
(2419, '', 0.00, 6, '2019-11-12 06:36:48'),
(2420, '', 0.00, 6, '2019-11-12 06:36:48'),
(2421, '', 0.00, 6, '2019-11-12 06:36:48'),
(2422, '', 0.00, 6, '2019-11-12 06:36:48'),
(2423, '', 0.00, 6, '2019-11-12 06:36:48'),
(2424, '', 0.00, 6, '2019-11-12 06:36:48'),
(2425, '', 0.00, 6, '2019-11-12 06:36:48'),
(2426, '', 0.00, 6, '2019-11-12 06:36:48'),
(2427, '', 0.00, 6, '2019-11-12 06:36:49'),
(2428, '', 0.00, 6, '2019-11-12 06:36:49'),
(2429, '', 0.00, 6, '2019-11-12 06:36:49'),
(2430, '', 0.00, 6, '2019-11-12 06:36:49'),
(2431, '', 0.00, 6, '2019-11-12 06:36:49'),
(2432, '', 0.00, 6, '2019-11-12 06:36:49'),
(2433, '', 0.00, 6, '2019-11-12 06:36:49'),
(2434, '', 0.00, 6, '2019-11-12 06:36:49'),
(2435, '', 0.00, 6, '2019-11-12 06:36:49'),
(2436, '', 0.00, 6, '2019-11-12 06:36:49'),
(2437, '', 0.00, 6, '2019-11-12 06:36:49'),
(2438, '', 0.00, 6, '2019-11-12 06:36:49'),
(2439, '', 0.00, 6, '2019-11-12 06:36:49'),
(2440, '', 0.00, 6, '2019-11-12 06:36:49'),
(2441, '', 0.00, 6, '2019-11-12 06:36:49'),
(2442, '', 0.00, 6, '2019-11-12 06:36:49'),
(2443, '', 0.00, 6, '2019-11-12 06:36:49'),
(2444, '', 0.00, 6, '2019-11-12 06:36:49'),
(2445, '', 0.00, 6, '2019-11-12 06:36:50'),
(2446, '', 0.00, 6, '2019-11-12 06:36:50'),
(2447, '', 0.00, 6, '2019-11-12 06:36:50'),
(2448, '', 0.00, 6, '2019-11-12 06:36:50'),
(2449, '', 0.00, 6, '2019-11-12 06:36:50'),
(2450, '', 0.00, 6, '2019-11-12 06:36:50'),
(2451, '', 0.00, 6, '2019-11-12 06:36:50'),
(2452, '', 0.00, 6, '2019-11-12 06:36:50'),
(2453, '', 0.00, 6, '2019-11-12 06:36:50'),
(2454, '', 0.00, 6, '2019-11-12 06:36:50'),
(2455, '', 0.00, 6, '2019-11-12 06:36:50'),
(2456, '', 0.00, 6, '2019-11-12 06:36:50'),
(2457, '', 0.00, 6, '2019-11-12 06:36:50'),
(2458, '', 0.00, 6, '2019-11-12 06:36:50'),
(2459, '', 0.00, 6, '2019-11-12 06:36:50'),
(2460, '', 0.00, 6, '2019-11-12 06:36:50'),
(2461, '', 0.00, 6, '2019-11-12 06:36:50'),
(2462, '', 0.00, 6, '2019-11-12 06:36:50'),
(2463, '', 0.00, 6, '2019-11-12 06:36:50'),
(2464, '', 0.00, 6, '2019-11-12 06:36:50'),
(2465, '', 0.00, 6, '2019-11-12 06:36:50'),
(2466, '', 0.00, 6, '2019-11-12 06:36:50'),
(2467, '', 0.00, 6, '2019-11-12 06:36:51'),
(2468, '', 0.00, 6, '2019-11-12 06:36:51'),
(2469, '', 0.00, 6, '2019-11-12 06:36:51'),
(2470, '', 0.00, 6, '2019-11-12 06:36:51'),
(2471, '\0\0Ixime', 0.00, 6, '2019-11-12 06:36:51'),
(2472, '\0\0Manix', 0.00, 6, '2019-11-12 06:36:51'),
(2473, '\0\0Pelox', 0.00, 6, '2019-11-12 06:36:51'),
(2474, '\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0 \0\0!\0\0&quot;\0\0#\0\0$\0\0%\0\0&amp;\0\0\'\0\0(\0\0)\0\0*\0\0+\0\0', 0.00, 6, '2019-11-12 06:36:51'),
(2475, '\0\0Supracin', 0.00, 6, '2019-11-12 06:36:52'),
(2476, '\0\0Tetacid ', 0.00, 6, '2019-11-12 06:36:53'),
(2477, '', 0.00, 6, '2019-11-12 06:36:53'),
(2478, '\0\0\0\0', 0.00, 6, '2019-11-12 06:36:53'),
(2479, '', 0.00, 6, '2019-11-12 06:36:53'),
(2480, '', 0.00, 6, '2019-11-12 06:36:53'),
(2481, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:53'),
(2482, '', 0.00, 6, '2019-11-12 06:36:53'),
(2483, '', 0.00, 6, '2019-11-12 06:36:53'),
(2484, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:53'),
(2485, '', 0.00, 6, '2019-11-12 06:36:53'),
(2486, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:53'),
(2487, '', 0.00, 6, '2019-11-12 06:36:53'),
(2488, '', 0.00, 6, '2019-11-12 06:36:53'),
(2489, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:53'),
(2490, '', 0.00, 6, '2019-11-12 06:36:53'),
(2491, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:53'),
(2492, '', 0.00, 6, '2019-11-12 06:36:54'),
(2493, '\0\0\0\0\0	\0\0\0~', 0.00, 6, '2019-11-12 06:36:54'),
(2494, '', 0.00, 6, '2019-11-12 06:36:54'),
(2495, '\0	\0\0\0\0', 0.00, 6, '2019-11-12 06:36:54'),
(2496, '\0\0\0~', 0.00, 6, '2019-11-12 06:36:54'),
(2497, '', 0.00, 6, '2019-11-12 06:36:54'),
(2498, '\0', 0.00, 6, '2019-11-12 06:36:54'),
(2499, '\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:54'),
(2500, '\0', 0.00, 6, '2019-11-12 06:36:54'),
(2501, '', 0.00, 6, '2019-11-12 06:36:54'),
(2502, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:54'),
(2503, '\0\0\0\0', 0.00, 6, '2019-11-12 06:36:54'),
(2504, '', 0.00, 6, '2019-11-12 06:36:54'),
(2505, '\0\0\0\0\0\r\0\0\0~', 0.00, 6, '2019-11-12 06:36:54'),
(2506, '', 0.00, 6, '2019-11-12 06:36:54'),
(2507, '\0\r\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:54'),
(2508, '', 0.00, 6, '2019-11-12 06:36:54'),
(2509, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:54'),
(2510, '', 0.00, 6, '2019-11-12 06:36:54'),
(2511, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:54'),
(2512, '', 0.00, 6, '2019-11-12 06:36:54'),
(2513, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:54'),
(2514, '', 0.00, 6, '2019-11-12 06:36:54'),
(2515, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:54'),
(2516, '\0\0\0\0P', 0.00, 6, '2019-11-12 06:36:54'),
(2517, '', 0.00, 6, '2019-11-12 06:36:55'),
(2518, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:55'),
(2519, '', 0.00, 6, '2019-11-12 06:36:55'),
(2520, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:55'),
(2521, '', 0.00, 6, '2019-11-12 06:36:55'),
(2522, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:55'),
(2523, '\0\0\0\0', 0.00, 6, '2019-11-12 06:36:55'),
(2524, '', 0.00, 6, '2019-11-12 06:36:55'),
(2525, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:55'),
(2526, '', 0.00, 6, '2019-11-12 06:36:55'),
(2527, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:55'),
(2528, '', 0.00, 6, '2019-11-12 06:36:55'),
(2529, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:55'),
(2530, '', 0.00, 6, '2019-11-12 06:36:55'),
(2531, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:56'),
(2532, '', 0.00, 6, '2019-11-12 06:36:56'),
(2533, '\0\0\0\0\0\Z\0\0\0~', 0.00, 6, '2019-11-12 06:36:56'),
(2534, '', 0.00, 6, '2019-11-12 06:36:56'),
(2535, '\0\Z\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:56'),
(2536, '', 0.00, 6, '2019-11-12 06:36:56'),
(2537, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:56'),
(2538, '', 0.00, 6, '2019-11-12 06:36:56'),
(2539, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:56'),
(2540, '', 0.00, 6, '2019-11-12 06:36:56'),
(2541, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:56'),
(2542, '', 0.00, 6, '2019-11-12 06:36:57'),
(2543, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:36:57'),
(2544, '', 0.00, 6, '2019-11-12 06:36:57'),
(2545, '\0\0\0\0\0 \0\0\0~', 0.00, 6, '2019-11-12 06:36:57'),
(2546, '\0\0\0\0#F\0\0\0 \0\0\0\0', 0.00, 6, '2019-11-12 06:36:57'),
(2547, '\0 \0\0\0\0!\0\0\0~', 0.00, 6, '2019-11-12 06:36:57'),
(2548, '', 0.00, 6, '2019-11-12 06:36:57'),
(2549, '\0!\0\0\0\0&quot;\0\0\0~', 0.00, 6, '2019-11-12 06:36:57'),
(2550, '', 0.00, 6, '2019-11-12 06:36:58'),
(2551, '\0&quot;\0\0\0\0#\0\0\0~', 0.00, 6, '2019-11-12 06:36:58'),
(2552, '', 0.00, 6, '2019-11-12 06:36:58'),
(2553, '\0#\0\0\0\0$\0\0\0~', 0.00, 6, '2019-11-12 06:36:58'),
(2554, '', 0.00, 6, '2019-11-12 06:36:58'),
(2555, '\0$\0\0\0\0%\0\0\0~', 0.00, 6, '2019-11-12 06:36:58'),
(2556, '', 0.00, 6, '2019-11-12 06:36:58'),
(2557, '\0%\0\0\0\0&amp;\0\0\0~', 0.00, 6, '2019-11-12 06:36:58'),
(2558, '', 0.00, 6, '2019-11-12 06:36:58'),
(2559, '\0&amp;\0\0\0\0\'\0\0\0~', 0.00, 6, '2019-11-12 06:36:58'),
(2560, '', 0.00, 6, '2019-11-12 06:36:58'),
(2561, '\0\'\0\0\0\0(\0\0\0~', 0.00, 6, '2019-11-12 06:36:58'),
(2562, '', 0.00, 6, '2019-11-12 06:36:58'),
(2563, '', 0.00, 6, '2019-11-12 06:36:58'),
(2564, '\0)\0\0\0\0*\0\0\0~', 0.00, 6, '2019-11-12 06:36:58'),
(2565, '', 0.00, 6, '2019-11-12 06:36:58'),
(2566, '\0*\0\0\0\0+\0\0\0~', 0.00, 6, '2019-11-12 06:36:59'),
(2567, '', 0.00, 6, '2019-11-12 06:36:59'),
(2568, '\0+\0\0\0\0', 0.00, 6, '2019-11-12 06:36:59'),
(2569, '', 0.00, 6, '2019-11-12 06:36:59'),
(2570, '\0', 0.00, 6, '2019-11-12 06:36:59'),
(2571, '\0', 0.00, 6, '2019-11-12 06:36:59'),
(2572, '\0-\0\0\0\0.\0\0\0~', 0.00, 6, '2019-11-12 06:36:59'),
(2573, '', 0.00, 6, '2019-11-12 06:36:59'),
(2574, '\0.\0\0\0\0/\0\0\0~', 0.00, 6, '2019-11-12 06:36:59'),
(2575, '', 0.00, 6, '2019-11-12 06:36:59'),
(2576, '\0/\0\0\0\00\0\0\0~', 0.00, 6, '2019-11-12 06:36:59'),
(2577, '', 0.00, 6, '2019-11-12 06:36:59'),
(2578, '\00\0\0\0\01\0\0\0~', 0.00, 6, '2019-11-12 06:36:59'),
(2579, '', 0.00, 6, '2019-11-12 06:36:59'),
(2580, '\01\0\0\0\02\0\0\0~', 0.00, 6, '2019-11-12 06:36:59'),
(2581, '', 0.00, 6, '2019-11-12 06:36:59'),
(2582, '\02\0\0\0\03\0\0\0~', 0.00, 6, '2019-11-12 06:36:59'),
(2583, '', 0.00, 6, '2019-11-12 06:36:59'),
(2584, '\03\0\0\0\04\0\0\0~', 0.00, 6, '2019-11-12 06:37:00'),
(2585, '\03\0\0\0', 0.00, 6, '2019-11-12 06:37:00'),
(2586, '', 0.00, 6, '2019-11-12 06:37:00'),
(2587, '\04\0\0\0\05\0\0\0~', 0.00, 6, '2019-11-12 06:37:00'),
(2588, '', 0.00, 6, '2019-11-12 06:37:00'),
(2589, '\05\0\0\0\06\0\0\0~', 0.00, 6, '2019-11-12 06:37:00'),
(2590, '', 0.00, 6, '2019-11-12 06:37:00'),
(2591, '\06\0\0\0\07\0\0\0~', 0.00, 6, '2019-11-12 06:37:00'),
(2592, '', 0.00, 6, '2019-11-12 06:37:00'),
(2593, '\07\0\0\0\08\0\0\0~', 0.00, 6, '2019-11-12 06:37:00'),
(2594, '', 0.00, 6, '2019-11-12 06:37:00'),
(2595, '', 0.00, 6, '2019-11-12 06:37:00'),
(2596, '\09\0\0\0\0:\0\0\0~', 0.00, 6, '2019-11-12 06:37:00'),
(2597, '', 0.00, 6, '2019-11-12 06:37:00'),
(2598, '\0:\0\0\0\0;\0\0\0~', 0.00, 6, '2019-11-12 06:37:01'),
(2599, '', 0.00, 6, '2019-11-12 06:37:01'),
(2600, '\0;\0\0\0\0&lt;\0\0\0~', 0.00, 6, '2019-11-12 06:37:01'),
(2601, '', 0.00, 6, '2019-11-12 06:37:01'),
(2602, '\0&lt;\0\0\0\0=\0\0\0~', 0.00, 6, '2019-11-12 06:37:01'),
(2603, '', 0.00, 6, '2019-11-12 06:37:01'),
(2604, '\0=\0\0\0\0&gt;\0\0\0~', 0.00, 6, '2019-11-12 06:37:01'),
(2605, '', 0.00, 6, '2019-11-12 06:37:01'),
(2606, '', 0.00, 6, '2019-11-12 06:37:01'),
(2607, '\0?\0\0\0\0@\0\0\0~', 0.00, 6, '2019-11-12 06:37:01'),
(2608, '', 0.00, 6, '2019-11-12 06:37:01'),
(2609, '\0@\0\0\0\0A\0\0\0~', 0.00, 6, '2019-11-12 06:37:01'),
(2610, '', 0.00, 6, '2019-11-12 06:37:01'),
(2611, '\0A\0\0\0\0B\0\0\0~', 0.00, 6, '2019-11-12 06:37:01'),
(2612, '', 0.00, 6, '2019-11-12 06:37:01'),
(2613, '\0B\0\0\0\0C\0\0\0~', 0.00, 6, '2019-11-12 06:37:01'),
(2614, '', 0.00, 6, '2019-11-12 06:37:01'),
(2615, '\0C\0\0\0\0D\0\0\0~', 0.00, 6, '2019-11-12 06:37:01'),
(2616, '', 0.00, 6, '2019-11-12 06:37:02'),
(2617, '\0D\0\0\0\0E\0\0\0~', 0.00, 6, '2019-11-12 06:37:02'),
(2618, '', 0.00, 6, '2019-11-12 06:37:02'),
(2619, '', 0.00, 6, '2019-11-12 06:37:02'),
(2620, '\0F\0\0\0\0G\0\0\0~', 0.00, 6, '2019-11-12 06:37:02'),
(2621, '', 0.00, 6, '2019-11-12 06:37:02'),
(2622, '\0G\0\0\0\0H\0\0\0~', 0.00, 6, '2019-11-12 06:37:03'),
(2623, '', 0.00, 6, '2019-11-12 06:37:03'),
(2624, '', 0.00, 6, '2019-11-12 06:37:03'),
(2625, '\0I\0\0\0\0J\0\0\0~', 0.00, 6, '2019-11-12 06:37:03'),
(2626, '', 0.00, 6, '2019-11-12 06:37:04'),
(2627, '\0J\0\0\0\0K\0\0\0~', 0.00, 6, '2019-11-12 06:37:04'),
(2628, '', 0.00, 6, '2019-11-12 06:37:04'),
(2629, '\0K\0\0\0\0L\0\0\0~', 0.00, 6, '2019-11-12 06:37:04'),
(2630, '', 0.00, 6, '2019-11-12 06:37:04'),
(2631, '\0L\0\0\0\0M\0\0\0~', 0.00, 6, '2019-11-12 06:37:04'),
(2632, '', 0.00, 6, '2019-11-12 06:37:04'),
(2633, '\0M\0\0\0\0N\0\0\0~', 0.00, 6, '2019-11-12 06:37:04'),
(2634, '', 0.00, 6, '2019-11-12 06:37:04'),
(2635, '\0N\0\0\0\0O\0\0\0~', 0.00, 6, '2019-11-12 06:37:04'),
(2636, '', 0.00, 6, '2019-11-12 06:37:04'),
(2637, '\0O\0\0\0\0P\0\0\0~', 0.00, 6, '2019-11-12 06:37:04'),
(2638, '', 0.00, 6, '2019-11-12 06:37:04'),
(2639, '\0P\0\0\0\0Q\0\0\0~', 0.00, 6, '2019-11-12 06:37:04'),
(2640, '', 0.00, 6, '2019-11-12 06:37:04'),
(2641, '\0Q\0\0\0\0R\0\0\0~', 0.00, 6, '2019-11-12 06:37:05'),
(2642, '', 0.00, 6, '2019-11-12 06:37:05'),
(2643, '\0R\0\0\0\0S\0\0\0~', 0.00, 6, '2019-11-12 06:37:05'),
(2644, '', 0.00, 6, '2019-11-12 06:37:05'),
(2645, '\0S\0\0\0\0T\0\0\0~', 0.00, 6, '2019-11-12 06:37:05'),
(2646, '', 0.00, 6, '2019-11-12 06:37:05'),
(2647, '\0T\0\0\0\0U\0\0\0~', 0.00, 6, '2019-11-12 06:37:05'),
(2648, '', 0.00, 6, '2019-11-12 06:37:05'),
(2649, '\0U\0\0\0\0V\0\0\0~', 0.00, 6, '2019-11-12 06:37:05'),
(2650, '', 0.00, 6, '2019-11-12 06:37:06'),
(2651, '\0V\0\0\0\0W\0\0\0~', 0.00, 6, '2019-11-12 06:37:06'),
(2652, '', 0.00, 6, '2019-11-12 06:37:06'),
(2653, '\0W\0\0\0\0X\0\0\0~', 0.00, 6, '2019-11-12 06:37:06'),
(2654, '', 0.00, 6, '2019-11-12 06:37:06'),
(2655, '\0X\0\0\0\0Y\0\0\0~', 0.00, 6, '2019-11-12 06:37:06'),
(2656, '', 0.00, 6, '2019-11-12 06:37:06'),
(2657, '\0Y\0\0\0\0Z\0\0\0~', 0.00, 6, '2019-11-12 06:37:06'),
(2658, '', 0.00, 6, '2019-11-12 06:37:06'),
(2659, '\0Z\0\0\0\0[\0\0\0~', 0.00, 6, '2019-11-12 06:37:06'),
(2660, '', 0.00, 6, '2019-11-12 06:37:07'),
(2661, '\0[\0\0\0\0\\\0\0\0~', 0.00, 6, '2019-11-12 06:37:07'),
(2662, '', 0.00, 6, '2019-11-12 06:37:07'),
(2663, '\0\\\0\0\0\0]\0\0\0~', 0.00, 6, '2019-11-12 06:37:07'),
(2664, '', 0.00, 6, '2019-11-12 06:37:07'),
(2665, '\0]\0\0\0\0^\0\0\0~', 0.00, 6, '2019-11-12 06:37:07'),
(2666, '', 0.00, 6, '2019-11-12 06:37:07'),
(2667, '\0^\0\0\0\0_\0\0\0~', 0.00, 6, '2019-11-12 06:37:07'),
(2668, '', 0.00, 6, '2019-11-12 06:37:07'),
(2669, '\0_\0\0\0\0`\0\0\0~', 0.00, 6, '2019-11-12 06:37:07'),
(2670, '\0_\0\0\0j\r\0\0\0`\0\0\0\0', 0.00, 6, '2019-11-12 06:37:08'),
(2671, '\0`\0\0\0\0a\0\0\0~', 0.00, 6, '2019-11-12 06:37:08'),
(2672, '', 0.00, 6, '2019-11-12 06:37:08'),
(2673, '\0a\0\0\0\0b\0\0\0~', 0.00, 6, '2019-11-12 06:37:08'),
(2674, '\0a\0\0\0S', 0.00, 6, '2019-11-12 06:37:08'),
(2675, '', 0.00, 6, '2019-11-12 06:37:08'),
(2676, '\0b\0\0\0\0c\0\0\0~', 0.00, 6, '2019-11-12 06:37:08'),
(2677, '', 0.00, 6, '2019-11-12 06:37:08'),
(2678, '\0c\0\0\0\0d\0\0\0~', 0.00, 6, '2019-11-12 06:37:09'),
(2679, '', 0.00, 6, '2019-11-12 06:37:09'),
(2680, '', 0.00, 6, '2019-11-12 06:37:09'),
(2681, '\0e\0\0\0\0f\0\0\0~', 0.00, 6, '2019-11-12 06:37:09'),
(2682, '', 0.00, 6, '2019-11-12 06:37:09'),
(2683, '\0f\0\0\0\0g\0\0\0~', 0.00, 6, '2019-11-12 06:37:09'),
(2684, '', 0.00, 6, '2019-11-12 06:37:09'),
(2685, '\0g\0\0\0\0h\0\0\0~', 0.00, 6, '2019-11-12 06:37:09'),
(2686, '', 0.00, 6, '2019-11-12 06:37:09'),
(2687, '\0h\0\0\0\0i\0\0\0~', 0.00, 6, '2019-11-12 06:37:09'),
(2688, '', 0.00, 6, '2019-11-12 06:37:09'),
(2689, '\0i\0\0\0\0j\0\0\0~', 0.00, 6, '2019-11-12 06:37:09'),
(2690, '', 0.00, 6, '2019-11-12 06:37:09'),
(2691, '\0j\0\0\0\0k\0\0\0~', 0.00, 6, '2019-11-12 06:37:09'),
(2692, '', 0.00, 6, '2019-11-12 06:37:10'),
(2693, '\0k\0\0\0\0l\0\0\0~', 0.00, 6, '2019-11-12 06:37:10'),
(2694, '', 0.00, 6, '2019-11-12 06:37:10'),
(2695, '\0l\0\0\0\0m\0\0\0~', 0.00, 6, '2019-11-12 06:37:10'),
(2696, '', 0.00, 6, '2019-11-12 06:37:10'),
(2697, '\0m\0\0\0\0n\0\0\0~', 0.00, 6, '2019-11-12 06:37:10'),
(2698, '', 0.00, 6, '2019-11-12 06:37:10'),
(2699, '', 0.00, 6, '2019-11-12 06:37:10'),
(2700, '\0o\0\0\0\0p\0\0\0~', 0.00, 6, '2019-11-12 06:37:10'),
(2701, '', 0.00, 6, '2019-11-12 06:37:10'),
(2702, '\0p\0\0\0\0q\0\0\0~', 0.00, 6, '2019-11-12 06:37:10'),
(2703, '', 0.00, 6, '2019-11-12 06:37:10'),
(2704, '\0q\0\0\0\0r\0\0\0~', 0.00, 6, '2019-11-12 06:37:10'),
(2705, '\0q\0\0\0', 0.00, 6, '2019-11-12 06:37:10'),
(2706, '', 0.00, 6, '2019-11-12 06:37:11'),
(2707, '\0r\0\0\0\0s\0\0\0~', 0.00, 6, '2019-11-12 06:37:11'),
(2708, '', 0.00, 6, '2019-11-12 06:37:11'),
(2709, '\0s\0\0\0\0t\0\0\0~', 0.00, 6, '2019-11-12 06:37:11'),
(2710, '', 0.00, 6, '2019-11-12 06:37:11'),
(2711, '\0t\0\0\0\0u\0\0\0~', 0.00, 6, '2019-11-12 06:37:11'),
(2712, '', 0.00, 6, '2019-11-12 06:37:11'),
(2713, '\0u\0\0\0\0v\0\0\0~', 0.00, 6, '2019-11-12 06:37:11'),
(2714, '', 0.00, 6, '2019-11-12 06:37:11'),
(2715, '\0v\0\0\0\0w\0\0\0~', 0.00, 6, '2019-11-12 06:37:11'),
(2716, '', 0.00, 6, '2019-11-12 06:37:11'),
(2717, '\0w\0\0\0\0x\0\0\0~', 0.00, 6, '2019-11-12 06:37:11'),
(2718, '', 0.00, 6, '2019-11-12 06:37:11'),
(2719, '\0x\0\0\0\0y\0\0\0~', 0.00, 6, '2019-11-12 06:37:11'),
(2720, '', 0.00, 6, '2019-11-12 06:37:11'),
(2721, '\0y\0\0\0\0z\0\0\0~', 0.00, 6, '2019-11-12 06:37:11'),
(2722, '', 0.00, 6, '2019-11-12 06:37:11'),
(2723, '', 0.00, 6, '2019-11-12 06:37:11'),
(2724, '', 0.00, 6, '2019-11-12 06:37:11'),
(2725, '', 0.00, 6, '2019-11-12 06:37:11'),
(2726, '\0}\0\0\0\0~\0\0\0~', 0.00, 6, '2019-11-12 06:37:12'),
(2727, '', 0.00, 6, '2019-11-12 06:37:12'),
(2728, '', 0.00, 6, '2019-11-12 06:37:12'),
(2729, '', 0.00, 6, '2019-11-12 06:37:12'),
(2730, '', 0.00, 6, '2019-11-12 06:37:12'),
(2731, '', 0.00, 6, '2019-11-12 06:37:12'),
(2732, '', 0.00, 6, '2019-11-12 06:37:12'),
(2733, '', 0.00, 6, '2019-11-12 06:37:12'),
(2734, '', 0.00, 6, '2019-11-12 06:37:12'),
(2735, '', 0.00, 6, '2019-11-12 06:37:12'),
(2736, '', 0.00, 6, '2019-11-12 06:37:12'),
(2737, '', 0.00, 6, '2019-11-12 06:37:12'),
(2738, '', 0.00, 6, '2019-11-12 06:37:12'),
(2739, '', 0.00, 6, '2019-11-12 06:37:12'),
(2740, '', 0.00, 6, '2019-11-12 06:37:12'),
(2741, '', 0.00, 6, '2019-11-12 06:37:12'),
(2742, '', 0.00, 6, '2019-11-12 06:37:13'),
(2743, '', 0.00, 6, '2019-11-12 06:37:13'),
(2744, '', 0.00, 6, '2019-11-12 06:37:13'),
(2745, '', 0.00, 6, '2019-11-12 06:37:13'),
(2746, '', 0.00, 6, '2019-11-12 06:37:13'),
(2747, '', 0.00, 6, '2019-11-12 06:37:13'),
(2748, '', 0.00, 6, '2019-11-12 06:37:13'),
(2749, '', 0.00, 6, '2019-11-12 06:37:13'),
(2750, '', 0.00, 6, '2019-11-12 06:37:13'),
(2751, '', 0.00, 6, '2019-11-12 06:37:13'),
(2752, '', 0.00, 6, '2019-11-12 06:37:14'),
(2753, '', 0.00, 6, '2019-11-12 06:37:14'),
(2754, '', 0.00, 6, '2019-11-12 06:37:14'),
(2755, '', 0.00, 6, '2019-11-12 06:37:14'),
(2756, '', 0.00, 6, '2019-11-12 06:37:14'),
(2757, '', 0.00, 6, '2019-11-12 06:37:14'),
(2758, '', 0.00, 6, '2019-11-12 06:37:14'),
(2759, '', 0.00, 6, '2019-11-12 06:37:14'),
(2760, '', 0.00, 6, '2019-11-12 06:37:14'),
(2761, '', 0.00, 6, '2019-11-12 06:37:14'),
(2762, '', 0.00, 6, '2019-11-12 06:37:14'),
(2763, '', 0.00, 6, '2019-11-12 06:37:14'),
(2764, '', 0.00, 6, '2019-11-12 06:37:14'),
(2765, '', 0.00, 6, '2019-11-12 06:37:15'),
(2766, '', 0.00, 6, '2019-11-12 06:37:15'),
(2767, '', 0.00, 6, '2019-11-12 06:37:15'),
(2768, '', 0.00, 6, '2019-11-12 06:37:15'),
(2769, '', 0.00, 6, '2019-11-12 06:37:15'),
(2770, '', 0.00, 6, '2019-11-12 06:37:15'),
(2771, '', 0.00, 6, '2019-11-12 06:37:15'),
(2772, '', 0.00, 6, '2019-11-12 06:37:15'),
(2773, '', 0.00, 6, '2019-11-12 06:37:15'),
(2774, '', 0.00, 6, '2019-11-12 06:37:15'),
(2775, '', 0.00, 6, '2019-11-12 06:37:15'),
(2776, '', 0.00, 6, '2019-11-12 06:37:15'),
(2777, '', 0.00, 6, '2019-11-12 06:37:15'),
(2778, '', 0.00, 6, '2019-11-12 06:37:15'),
(2779, '', 0.00, 6, '2019-11-12 06:37:16'),
(2780, '', 0.00, 6, '2019-11-12 06:37:16'),
(2781, '', 0.00, 6, '2019-11-12 06:37:16'),
(2782, '', 0.00, 6, '2019-11-12 06:37:16'),
(2783, '', 0.00, 6, '2019-11-12 06:37:16'),
(2784, '', 0.00, 6, '2019-11-12 06:37:16'),
(2785, '', 0.00, 6, '2019-11-12 06:37:16'),
(2786, '', 0.00, 6, '2019-11-12 06:37:17'),
(2787, '', 0.00, 6, '2019-11-12 06:37:17'),
(2788, '', 0.00, 6, '2019-11-12 06:37:17'),
(2789, '', 0.00, 6, '2019-11-12 06:37:17'),
(2790, '', 0.00, 6, '2019-11-12 06:37:17'),
(2791, '', 0.00, 6, '2019-11-12 06:37:17'),
(2792, '', 0.00, 6, '2019-11-12 06:37:18'),
(2793, '', 0.00, 6, '2019-11-12 06:37:18'),
(2794, '', 0.00, 6, '2019-11-12 06:37:18'),
(2795, '', 0.00, 6, '2019-11-12 06:37:18'),
(2796, '', 0.00, 6, '2019-11-12 06:37:18'),
(2797, '', 0.00, 6, '2019-11-12 06:37:18'),
(2798, '', 0.00, 6, '2019-11-12 06:37:18'),
(2799, '', 0.00, 6, '2019-11-12 06:37:18'),
(2800, '', 0.00, 6, '2019-11-12 06:37:18'),
(2801, '', 0.00, 6, '2019-11-12 06:37:18'),
(2802, '', 0.00, 6, '2019-11-12 06:37:18'),
(2803, '', 0.00, 6, '2019-11-12 06:37:19'),
(2804, '', 0.00, 6, '2019-11-12 06:37:19'),
(2805, '', 0.00, 6, '2019-11-12 06:37:19'),
(2806, '', 0.00, 6, '2019-11-12 06:37:19'),
(2807, '', 0.00, 6, '2019-11-12 06:37:19'),
(2808, '', 0.00, 6, '2019-11-12 06:37:19'),
(2809, '', 0.00, 6, '2019-11-12 06:37:19'),
(2810, '', 0.00, 6, '2019-11-12 06:37:19'),
(2811, '', 0.00, 6, '2019-11-12 06:37:19'),
(2812, '', 0.00, 6, '2019-11-12 06:37:19'),
(2813, '', 0.00, 6, '2019-11-12 06:37:19'),
(2814, '', 0.00, 6, '2019-11-12 06:37:19'),
(2815, '', 0.00, 6, '2019-11-12 06:37:19'),
(2816, '', 0.00, 6, '2019-11-12 06:37:19'),
(2817, '', 0.00, 6, '2019-11-12 06:37:19'),
(2818, '', 0.00, 6, '2019-11-12 06:37:19'),
(2819, '', 0.00, 6, '2019-11-12 06:37:19'),
(2820, '', 0.00, 6, '2019-11-12 06:37:19'),
(2821, '', 0.00, 6, '2019-11-12 06:37:19'),
(2822, '', 0.00, 6, '2019-11-12 06:37:19'),
(2823, '', 0.00, 6, '2019-11-12 06:37:19'),
(2824, '', 0.00, 6, '2019-11-12 06:37:20'),
(2825, '', 0.00, 6, '2019-11-12 06:37:20'),
(2826, '', 0.00, 6, '2019-11-12 06:37:20'),
(2827, '', 0.00, 6, '2019-11-12 06:37:20'),
(2828, '', 0.00, 6, '2019-11-12 06:37:20'),
(2829, '', 0.00, 6, '2019-11-12 06:37:20'),
(2830, '', 0.00, 6, '2019-11-12 06:37:20'),
(2831, '', 0.00, 6, '2019-11-12 06:37:20'),
(2832, '', 0.00, 6, '2019-11-12 06:37:20'),
(2833, '', 0.00, 6, '2019-11-12 06:37:20'),
(2834, '', 0.00, 6, '2019-11-12 06:37:20'),
(2835, '', 0.00, 6, '2019-11-12 06:37:20'),
(2836, '', 0.00, 6, '2019-11-12 06:37:20'),
(2837, '', 0.00, 6, '2019-11-12 06:37:20'),
(2838, '', 0.00, 6, '2019-11-12 06:37:20'),
(2839, '', 0.00, 6, '2019-11-12 06:37:20'),
(2840, '', 0.00, 6, '2019-11-12 06:37:21'),
(2841, '', 0.00, 6, '2019-11-12 06:37:21'),
(2842, '', 0.00, 6, '2019-11-12 06:37:21'),
(2843, '', 0.00, 6, '2019-11-12 06:37:21'),
(2844, '', 0.00, 6, '2019-11-12 06:37:21'),
(2845, '', 0.00, 6, '2019-11-12 06:37:21'),
(2846, '', 0.00, 6, '2019-11-12 06:37:21'),
(2847, '', 0.00, 6, '2019-11-12 06:37:21'),
(2848, '', 0.00, 6, '2019-11-12 06:37:21'),
(2849, '', 0.00, 6, '2019-11-12 06:37:21'),
(2850, '', 0.00, 6, '2019-11-12 06:37:21'),
(2851, '', 0.00, 6, '2019-11-12 06:37:21'),
(2852, '', 0.00, 6, '2019-11-12 06:37:21'),
(2853, '', 0.00, 6, '2019-11-12 06:37:21'),
(2854, '', 0.00, 6, '2019-11-12 06:37:21'),
(2855, '', 0.00, 6, '2019-11-12 06:37:21'),
(2856, '', 0.00, 6, '2019-11-12 06:37:21'),
(2857, '', 0.00, 6, '2019-11-12 06:37:21'),
(2858, '', 0.00, 6, '2019-11-12 06:37:21'),
(2859, '', 0.00, 6, '2019-11-12 06:37:21'),
(2860, '', 0.00, 6, '2019-11-12 06:37:21'),
(2861, '', 0.00, 6, '2019-11-12 06:37:21'),
(2862, '', 0.00, 6, '2019-11-12 06:37:22'),
(2863, '', 0.00, 6, '2019-11-12 06:37:22'),
(2864, '', 0.00, 6, '2019-11-12 06:37:22'),
(2865, '', 0.00, 6, '2019-11-12 06:37:22'),
(2866, '', 0.00, 6, '2019-11-12 06:37:22'),
(2867, '', 0.00, 6, '2019-11-12 06:37:22'),
(2868, '', 0.00, 6, '2019-11-12 06:37:23'),
(2869, '', 0.00, 6, '2019-11-12 06:37:23'),
(2870, '', 0.00, 6, '2019-11-12 06:37:23'),
(2871, '', 0.00, 6, '2019-11-12 06:37:24'),
(2872, '', 0.00, 6, '2019-11-12 06:37:24'),
(2873, '', 0.00, 6, '2019-11-12 06:37:24'),
(2874, '', 0.00, 6, '2019-11-12 06:37:24'),
(2875, '', 0.00, 6, '2019-11-12 06:37:24'),
(2876, '', 0.00, 6, '2019-11-12 06:37:24'),
(2877, '', 0.00, 6, '2019-11-12 06:37:24'),
(2878, '', 0.00, 6, '2019-11-12 06:37:24'),
(2879, '', 0.00, 6, '2019-11-12 06:37:24'),
(2880, '', 0.00, 6, '2019-11-12 06:37:24'),
(2881, '', 0.00, 6, '2019-11-12 06:37:24'),
(2882, '', 0.00, 6, '2019-11-12 06:37:24'),
(2883, '', 0.00, 6, '2019-11-12 06:37:24'),
(2884, '', 0.00, 6, '2019-11-12 06:37:25'),
(2885, '', 0.00, 6, '2019-11-12 06:37:25'),
(2886, '', 0.00, 6, '2019-11-12 06:37:25'),
(2887, '', 0.00, 6, '2019-11-12 06:37:25'),
(2888, '', 0.00, 6, '2019-11-12 06:37:25'),
(2889, '', 0.00, 6, '2019-11-12 06:37:25'),
(2890, '', 0.00, 6, '2019-11-12 06:37:26'),
(2891, '', 0.00, 6, '2019-11-12 06:37:26'),
(2892, '', 0.00, 6, '2019-11-12 06:37:26'),
(2893, '', 0.00, 6, '2019-11-12 06:37:26'),
(2894, '', 0.00, 6, '2019-11-12 06:37:26'),
(2895, '', 0.00, 6, '2019-11-12 06:37:26'),
(2896, '', 0.00, 6, '2019-11-12 06:37:26'),
(2897, '', 0.00, 6, '2019-11-12 06:37:26'),
(2898, '', 0.00, 6, '2019-11-12 06:37:27'),
(2899, '', 0.00, 6, '2019-11-12 06:37:27'),
(2900, '', 0.00, 6, '2019-11-12 06:37:27'),
(2901, '', 0.00, 6, '2019-11-12 06:37:27'),
(2902, '', 0.00, 6, '2019-11-12 06:37:27'),
(2903, '', 0.00, 6, '2019-11-12 06:37:27'),
(2904, '', 0.00, 6, '2019-11-12 06:37:27'),
(2905, '', 0.00, 6, '2019-11-12 06:37:27'),
(2906, '', 0.00, 6, '2019-11-12 06:37:27'),
(2907, '', 0.00, 6, '2019-11-12 06:37:27'),
(2908, '', 0.00, 6, '2019-11-12 06:37:28'),
(2909, '', 0.00, 6, '2019-11-12 06:37:28'),
(2910, '', 0.00, 6, '2019-11-12 06:37:28'),
(2911, '', 0.00, 6, '2019-11-12 06:37:28'),
(2912, '', 0.00, 6, '2019-11-12 06:37:28'),
(2913, '', 0.00, 6, '2019-11-12 06:37:28'),
(2914, '', 0.00, 6, '2019-11-12 06:37:28'),
(2915, '', 0.00, 6, '2019-11-12 06:37:28'),
(2916, '', 0.00, 6, '2019-11-12 06:37:28'),
(2917, '', 0.00, 6, '2019-11-12 06:37:28'),
(2918, '', 0.00, 6, '2019-11-12 06:37:28'),
(2919, '', 0.00, 6, '2019-11-12 06:37:28'),
(2920, '', 0.00, 6, '2019-11-12 06:37:28'),
(2921, '', 0.00, 6, '2019-11-12 06:37:29'),
(2922, '', 0.00, 6, '2019-11-12 06:37:29'),
(2923, '', 0.00, 6, '2019-11-12 06:37:29'),
(2924, '', 0.00, 6, '2019-11-12 06:37:29'),
(2925, '', 0.00, 6, '2019-11-12 06:37:29'),
(2926, '', 0.00, 6, '2019-11-12 06:37:29'),
(2927, '', 0.00, 6, '2019-11-12 06:37:29'),
(2928, '', 0.00, 6, '2019-11-12 06:37:29'),
(2929, '', 0.00, 6, '2019-11-12 06:37:29'),
(2930, '', 0.00, 6, '2019-11-12 06:37:29'),
(2931, '', 0.00, 6, '2019-11-12 06:37:29'),
(2932, '', 0.00, 6, '2019-11-12 06:37:29'),
(2933, '', 0.00, 6, '2019-11-12 06:37:30'),
(2934, '', 0.00, 6, '2019-11-12 06:37:30'),
(2935, '', 0.00, 6, '2019-11-12 06:37:30'),
(2936, '', 0.00, 6, '2019-11-12 06:37:30'),
(2937, '', 0.00, 6, '2019-11-12 06:37:30'),
(2938, '', 0.00, 6, '2019-11-12 06:37:30'),
(2939, '', 0.00, 6, '2019-11-12 06:37:30'),
(2940, '', 0.00, 6, '2019-11-12 06:37:30'),
(2941, '', 0.00, 6, '2019-11-12 06:37:30'),
(2942, '', 0.00, 6, '2019-11-12 06:37:30'),
(2943, '', 0.00, 6, '2019-11-12 06:37:30'),
(2944, '', 0.00, 6, '2019-11-12 06:37:30'),
(2945, '', 0.00, 6, '2019-11-12 06:37:30'),
(2946, '', 0.00, 6, '2019-11-12 06:37:30'),
(2947, '', 0.00, 6, '2019-11-12 06:37:30'),
(2948, '', 0.00, 6, '2019-11-12 06:37:30'),
(2949, '', 0.00, 6, '2019-11-12 06:37:31'),
(2950, '', 0.00, 6, '2019-11-12 06:37:31'),
(2951, '', 0.00, 6, '2019-11-12 06:37:31'),
(2952, '', 0.00, 6, '2019-11-12 06:37:31'),
(2953, '', 0.00, 6, '2019-11-12 06:37:31'),
(2954, '', 0.00, 6, '2019-11-12 06:37:31'),
(2955, '', 0.00, 6, '2019-11-12 06:37:31'),
(2956, '', 0.00, 6, '2019-11-12 06:37:31'),
(2957, '', 0.00, 6, '2019-11-12 06:37:31'),
(2958, '', 0.00, 6, '2019-11-12 06:37:31'),
(2959, '', 0.00, 6, '2019-11-12 06:37:31'),
(2960, '', 0.00, 6, '2019-11-12 06:37:31'),
(2961, '', 0.00, 6, '2019-11-12 06:37:31'),
(2962, '', 0.00, 6, '2019-11-12 06:37:31'),
(2963, '', 0.00, 6, '2019-11-12 06:37:32'),
(2964, '', 0.00, 6, '2019-11-12 06:37:32'),
(2965, '', 0.00, 6, '2019-11-12 06:37:32'),
(2966, '', 0.00, 6, '2019-11-12 06:37:32'),
(2967, '', 0.00, 6, '2019-11-12 06:37:32'),
(2968, '', 0.00, 6, '2019-11-12 06:37:32'),
(2969, '', 0.00, 6, '2019-11-12 06:37:32'),
(2970, '', 0.00, 6, '2019-11-12 06:37:32'),
(2971, '', 0.00, 6, '2019-11-12 06:37:32'),
(2972, '', 0.00, 6, '2019-11-12 06:37:32'),
(2973, '', 0.00, 6, '2019-11-12 06:37:32'),
(2974, '', 0.00, 6, '2019-11-12 06:37:33'),
(2975, '', 0.00, 6, '2019-11-12 06:37:33'),
(2976, '', 0.00, 6, '2019-11-12 06:37:33'),
(2977, '', 0.00, 6, '2019-11-12 06:37:33'),
(2978, '', 0.00, 6, '2019-11-12 06:37:33'),
(2979, '', 0.00, 6, '2019-11-12 06:37:33'),
(2980, '', 0.00, 6, '2019-11-12 06:37:33'),
(2981, '', 0.00, 6, '2019-11-12 06:37:33'),
(2982, '\0\0\0', 0.00, 6, '2019-11-12 06:37:33'),
(2983, '\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:33'),
(2984, '', 0.00, 6, '2019-11-12 06:37:33'),
(2985, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:33'),
(2986, '', 0.00, 6, '2019-11-12 06:37:33'),
(2987, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:33'),
(2988, '\0\0\0', 0.00, 6, '2019-11-12 06:37:33'),
(2989, '', 0.00, 6, '2019-11-12 06:37:33'),
(2990, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:33'),
(2991, '', 0.00, 6, '2019-11-12 06:37:33'),
(2992, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:33'),
(2993, '', 0.00, 6, '2019-11-12 06:37:33'),
(2994, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:34'),
(2995, '', 0.00, 6, '2019-11-12 06:37:34'),
(2996, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:34'),
(2997, '', 0.00, 6, '2019-11-12 06:37:34'),
(2998, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:34'),
(2999, '', 0.00, 6, '2019-11-12 06:37:34'),
(3000, '\0\0\0\0	\0\0~', 0.00, 6, '2019-11-12 06:37:34'),
(3001, '', 0.00, 6, '2019-11-12 06:37:34'),
(3002, '\0	\0\0\0', 0.00, 6, '2019-11-12 06:37:34'),
(3003, '\0\0~', 0.00, 6, '2019-11-12 06:37:34'),
(3004, '', 0.00, 6, '2019-11-12 06:37:35'),
(3005, '\0', 0.00, 6, '2019-11-12 06:37:35'),
(3006, '\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:35'),
(3007, '\0', 0.00, 6, '2019-11-12 06:37:35'),
(3008, '', 0.00, 6, '2019-11-12 06:37:35'),
(3009, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:35'),
(3010, '', 0.00, 6, '2019-11-12 06:37:35'),
(3011, '\0\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:37:36'),
(3012, '', 0.00, 6, '2019-11-12 06:37:36'),
(3013, '\0\r\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:36'),
(3014, '', 0.00, 6, '2019-11-12 06:37:36'),
(3015, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:37'),
(3016, '', 0.00, 6, '2019-11-12 06:37:37'),
(3017, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:37'),
(3018, '', 0.00, 6, '2019-11-12 06:37:37'),
(3019, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:37'),
(3020, '', 0.00, 6, '2019-11-12 06:37:37'),
(3021, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:37'),
(3022, '', 0.00, 6, '2019-11-12 06:37:37'),
(3023, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:37'),
(3024, '', 0.00, 6, '2019-11-12 06:37:38'),
(3025, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:38'),
(3026, '', 0.00, 6, '2019-11-12 06:37:38'),
(3027, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:38'),
(3028, '\0\0\0R', 0.00, 6, '2019-11-12 06:37:39'),
(3029, '', 0.00, 6, '2019-11-12 06:37:39'),
(3030, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:39'),
(3031, '', 0.00, 6, '2019-11-12 06:37:39'),
(3032, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:39'),
(3033, '', 0.00, 6, '2019-11-12 06:37:39'),
(3034, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:39'),
(3035, '', 0.00, 6, '2019-11-12 06:37:40'),
(3036, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:40'),
(3037, '', 0.00, 6, '2019-11-12 06:37:40'),
(3038, '\0\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:37:41'),
(3039, '', 0.00, 6, '2019-11-12 06:37:41'),
(3040, '\0\Z\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:41'),
(3041, '\0\Z\0\0R', 0.00, 6, '2019-11-12 06:37:41'),
(3042, '', 0.00, 6, '2019-11-12 06:37:41'),
(3043, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:42'),
(3044, '', 0.00, 6, '2019-11-12 06:37:42'),
(3045, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:42'),
(3046, '', 0.00, 6, '2019-11-12 06:37:42'),
(3047, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:42'),
(3048, '', 0.00, 6, '2019-11-12 06:37:42'),
(3049, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:42'),
(3050, '', 0.00, 6, '2019-11-12 06:37:42'),
(3051, '\0\0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:37:42'),
(3052, '\0\0\0#\0\0\0 \0\0\0', 0.00, 6, '2019-11-12 06:37:42'),
(3053, '\0 \0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:37:42'),
(3054, '', 0.00, 6, '2019-11-12 06:37:42'),
(3055, '', 0.00, 6, '2019-11-12 06:37:43'),
(3056, '\0&quot;\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:37:43'),
(3057, '', 0.00, 6, '2019-11-12 06:37:43'),
(3058, '\0#\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:37:43'),
(3059, '\0#\0\0', 0.00, 6, '2019-11-12 06:37:43'),
(3060, '', 0.00, 6, '2019-11-12 06:37:43'),
(3061, '\0$\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:37:43'),
(3062, '', 0.00, 6, '2019-11-12 06:37:43'),
(3063, '', 0.00, 6, '2019-11-12 06:37:43'),
(3064, '\0&amp;\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:37:43'),
(3065, '', 0.00, 6, '2019-11-12 06:37:43'),
(3066, '\0\'\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:37:43'),
(3067, '', 0.00, 6, '2019-11-12 06:37:44'),
(3068, '\0(\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:37:44'),
(3069, '', 0.00, 6, '2019-11-12 06:37:44'),
(3070, '\0)\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:37:44'),
(3071, '', 0.00, 6, '2019-11-12 06:37:44'),
(3072, '\0*\0\0\0+\0\0~', 0.00, 6, '2019-11-12 06:37:44'),
(3073, '', 0.00, 6, '2019-11-12 06:37:44'),
(3074, '\0+\0\0\0', 0.00, 6, '2019-11-12 06:37:44'),
(3075, '', 0.00, 6, '2019-11-12 06:37:44'),
(3076, '\0', 0.00, 6, '2019-11-12 06:37:44'),
(3077, '\0', 0.00, 6, '2019-11-12 06:37:44'),
(3078, '\0-\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:37:44'),
(3079, '', 0.00, 6, '2019-11-12 06:37:44'),
(3080, '\0.\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:37:44'),
(3081, '', 0.00, 6, '2019-11-12 06:37:44'),
(3082, '\0/\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:37:44'),
(3083, '', 0.00, 6, '2019-11-12 06:37:44'),
(3084, '', 0.00, 6, '2019-11-12 06:37:44'),
(3085, '\01\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:37:44'),
(3086, '', 0.00, 6, '2019-11-12 06:37:45'),
(3087, '\02\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:37:45'),
(3088, '', 0.00, 6, '2019-11-12 06:37:45'),
(3089, '\03\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:37:45'),
(3090, '', 0.00, 6, '2019-11-12 06:37:45'),
(3091, '\04\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:37:45'),
(3092, '', 0.00, 6, '2019-11-12 06:37:45'),
(3093, '\05\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:37:45'),
(3094, '', 0.00, 6, '2019-11-12 06:37:45'),
(3095, '\06\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:37:45'),
(3096, '', 0.00, 6, '2019-11-12 06:37:46'),
(3097, '\07\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:37:46'),
(3098, '', 0.00, 6, '2019-11-12 06:37:46'),
(3099, '\08\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:37:46'),
(3100, '', 0.00, 6, '2019-11-12 06:37:46'),
(3101, '\09\0\0\0:\0\0~', 0.00, 6, '2019-11-12 06:37:46'),
(3102, '', 0.00, 6, '2019-11-12 06:37:46'),
(3103, '\0:\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:37:46'),
(3104, '', 0.00, 6, '2019-11-12 06:37:46'),
(3105, '\0;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:37:47'),
(3106, '', 0.00, 6, '2019-11-12 06:37:47'),
(3107, '\0&lt;\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:37:47'),
(3108, '', 0.00, 6, '2019-11-12 06:37:47'),
(3109, '\0=\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:37:47'),
(3110, '', 0.00, 6, '2019-11-12 06:37:48'),
(3111, '\0&gt;\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:37:48'),
(3112, '', 0.00, 6, '2019-11-12 06:37:48'),
(3113, '\0?\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:37:48'),
(3114, '\0?\0\02\0\0\0@\0\0\0', 0.00, 6, '2019-11-12 06:37:48'),
(3115, '\0@\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:37:48'),
(3116, '', 0.00, 6, '2019-11-12 06:37:48'),
(3117, '\0A\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:37:48'),
(3118, '', 0.00, 6, '2019-11-12 06:37:48'),
(3119, '\0B\0\0\0C\0\0~', 0.00, 6, '2019-11-12 06:37:48'),
(3120, '\0B\0\0R', 0.00, 6, '2019-11-12 06:37:48'),
(3121, '', 0.00, 6, '2019-11-12 06:37:49'),
(3122, '\0C\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:37:49'),
(3123, '', 0.00, 6, '2019-11-12 06:37:49'),
(3124, '\0D\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:37:49'),
(3125, '', 0.00, 6, '2019-11-12 06:37:49'),
(3126, '\0E\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:37:49'),
(3127, '', 0.00, 6, '2019-11-12 06:37:49'),
(3128, '\0F\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:37:49'),
(3129, '', 0.00, 6, '2019-11-12 06:37:49'),
(3130, '\0G\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:37:49'),
(3131, '', 0.00, 6, '2019-11-12 06:37:49'),
(3132, '\0H\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:37:49'),
(3133, '', 0.00, 6, '2019-11-12 06:37:49'),
(3134, '\0I\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:37:49'),
(3135, '', 0.00, 6, '2019-11-12 06:37:49'),
(3136, '\0J\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:37:50'),
(3137, '', 0.00, 6, '2019-11-12 06:37:50'),
(3138, '\0K\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:37:50'),
(3139, '\0K\0\0S', 0.00, 6, '2019-11-12 06:37:50'),
(3140, '', 0.00, 6, '2019-11-12 06:37:50'),
(3141, '\0L\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:37:50'),
(3142, '', 0.00, 6, '2019-11-12 06:37:50'),
(3143, '\0M\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:37:50'),
(3144, '', 0.00, 6, '2019-11-12 06:37:50'),
(3145, '\0N\0\0\0O\0\0~', 0.00, 6, '2019-11-12 06:37:50'),
(3146, '', 0.00, 6, '2019-11-12 06:37:50'),
(3147, '\0O\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:37:50'),
(3148, '', 0.00, 6, '2019-11-12 06:37:50'),
(3149, '\0P\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:37:50'),
(3150, '\0P\0\0', 0.00, 6, '2019-11-12 06:37:50'),
(3151, '', 0.00, 6, '2019-11-12 06:37:50'),
(3152, '\0Q\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:37:50'),
(3153, '', 0.00, 6, '2019-11-12 06:37:51'),
(3154, '\0R\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:37:51'),
(3155, '', 0.00, 6, '2019-11-12 06:37:51'),
(3156, '\0S\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:37:51'),
(3157, '', 0.00, 6, '2019-11-12 06:37:51'),
(3158, '\0T\0\0\0U\0\0~', 0.00, 6, '2019-11-12 06:37:51'),
(3159, '', 0.00, 6, '2019-11-12 06:37:51'),
(3160, '\0U\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:37:51'),
(3161, '', 0.00, 6, '2019-11-12 06:37:51'),
(3162, '\0V\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:37:51'),
(3163, '', 0.00, 6, '2019-11-12 06:37:51'),
(3164, '\0W\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:37:51'),
(3165, '', 0.00, 6, '2019-11-12 06:37:51'),
(3166, '\0X\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:37:51'),
(3167, '', 0.00, 6, '2019-11-12 06:37:51'),
(3168, '\0Y\0\0\0Z\0\0~', 0.00, 6, '2019-11-12 06:37:51');
INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(3169, '', 0.00, 6, '2019-11-12 06:37:51'),
(3170, '\0Z\0\0\0[\0\0~', 0.00, 6, '2019-11-12 06:37:51'),
(3171, '', 0.00, 6, '2019-11-12 06:37:51'),
(3172, '\0[\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:37:52'),
(3173, '', 0.00, 6, '2019-11-12 06:37:52'),
(3174, '\0\\\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:37:52'),
(3175, '', 0.00, 6, '2019-11-12 06:37:52'),
(3176, '\0]\0\0\0^\0\0~', 0.00, 6, '2019-11-12 06:37:52'),
(3177, '', 0.00, 6, '2019-11-12 06:37:52'),
(3178, '\0^\0\0\0_\0\0~', 0.00, 6, '2019-11-12 06:37:52'),
(3179, '', 0.00, 6, '2019-11-12 06:37:52'),
(3180, '\0_\0\0\0`\0\0\0_\0\0fffff&amp;r@\0`\0\0\0', 0.00, 6, '2019-11-12 06:37:52'),
(3181, '\0`\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:37:52'),
(3182, '', 0.00, 6, '2019-11-12 06:37:52'),
(3183, '\0a\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:37:52'),
(3184, '', 0.00, 6, '2019-11-12 06:37:52'),
(3185, '\0b\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:37:52'),
(3186, '', 0.00, 6, '2019-11-12 06:37:52'),
(3187, '\0c\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:37:53'),
(3188, '', 0.00, 6, '2019-11-12 06:37:53'),
(3189, '\0d\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:37:53'),
(3190, '', 0.00, 6, '2019-11-12 06:37:53'),
(3191, '\0e\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:37:53'),
(3192, '', 0.00, 6, '2019-11-12 06:37:53'),
(3193, '\0f\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:37:53'),
(3194, '', 0.00, 6, '2019-11-12 06:37:53'),
(3195, '\0g\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:37:53'),
(3196, '', 0.00, 6, '2019-11-12 06:37:53'),
(3197, '\0h\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:37:53'),
(3198, '', 0.00, 6, '2019-11-12 06:37:54'),
(3199, '\0i\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:37:54'),
(3200, '', 0.00, 6, '2019-11-12 06:37:54'),
(3201, '\0j\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:37:54'),
(3202, '', 0.00, 6, '2019-11-12 06:37:54'),
(3203, '\0k\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:37:54'),
(3204, '', 0.00, 6, '2019-11-12 06:37:54'),
(3205, '\0l\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:37:54'),
(3206, '', 0.00, 6, '2019-11-12 06:37:54'),
(3207, '\0m\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:37:54'),
(3208, '', 0.00, 6, '2019-11-12 06:37:54'),
(3209, '\0n\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:37:54'),
(3210, '', 0.00, 6, '2019-11-12 06:37:54'),
(3211, '\0o\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:37:54'),
(3212, '', 0.00, 6, '2019-11-12 06:37:54'),
(3213, '\0p\0\0\0q\0\0~', 0.00, 6, '2019-11-12 06:37:54'),
(3214, '\0p\0\0R', 0.00, 6, '2019-11-12 06:37:54'),
(3215, '', 0.00, 6, '2019-11-12 06:37:54'),
(3216, '\0q\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:37:54'),
(3217, '', 0.00, 6, '2019-11-12 06:37:54'),
(3218, '\0r\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:37:54'),
(3219, '', 0.00, 6, '2019-11-12 06:37:54'),
(3220, '\0s\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:37:55'),
(3221, '', 0.00, 6, '2019-11-12 06:37:55'),
(3222, '\0t\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:37:55'),
(3223, '', 0.00, 6, '2019-11-12 06:37:55'),
(3224, '\0u\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:37:55'),
(3225, '', 0.00, 6, '2019-11-12 06:37:55'),
(3226, '\0v\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:37:55'),
(3227, '', 0.00, 6, '2019-11-12 06:37:55'),
(3228, '\0w\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:37:55'),
(3229, '\0w\0\0', 0.00, 6, '2019-11-12 06:37:55'),
(3230, '', 0.00, 6, '2019-11-12 06:37:55'),
(3231, '\0x\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:37:55'),
(3232, '', 0.00, 6, '2019-11-12 06:37:55'),
(3233, '\0y\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:37:55'),
(3234, '', 0.00, 6, '2019-11-12 06:37:55'),
(3235, '\0z\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:37:55'),
(3236, '', 0.00, 6, '2019-11-12 06:37:55'),
(3237, '\0{\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:37:55'),
(3238, '', 0.00, 6, '2019-11-12 06:37:56'),
(3239, '\0|\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:37:56'),
(3240, '', 0.00, 6, '2019-11-12 06:37:56'),
(3241, '\0}\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:37:56'),
(3242, '', 0.00, 6, '2019-11-12 06:37:56'),
(3243, '\0~\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:37:56'),
(3244, '', 0.00, 6, '2019-11-12 06:37:56'),
(3245, '', 0.00, 6, '2019-11-12 06:37:56'),
(3246, '', 0.00, 6, '2019-11-12 06:37:56'),
(3247, '', 0.00, 6, '2019-11-12 06:37:56'),
(3248, '', 0.00, 6, '2019-11-12 06:37:56'),
(3249, '', 0.00, 6, '2019-11-12 06:37:56'),
(3250, '', 0.00, 6, '2019-11-12 06:37:56'),
(3251, '', 0.00, 6, '2019-11-12 06:37:56'),
(3252, '', 0.00, 6, '2019-11-12 06:37:56'),
(3253, '', 0.00, 6, '2019-11-12 06:37:56'),
(3254, '', 0.00, 6, '2019-11-12 06:37:57'),
(3255, '', 0.00, 6, '2019-11-12 06:37:57'),
(3256, '', 0.00, 6, '2019-11-12 06:37:57'),
(3257, '', 0.00, 6, '2019-11-12 06:37:57'),
(3258, '', 0.00, 6, '2019-11-12 06:37:57'),
(3259, '', 0.00, 6, '2019-11-12 06:37:57'),
(3260, '', 0.00, 6, '2019-11-12 06:37:57'),
(3261, '', 0.00, 6, '2019-11-12 06:37:57'),
(3262, '', 0.00, 6, '2019-11-12 06:37:57'),
(3263, '', 0.00, 6, '2019-11-12 06:37:57'),
(3264, '', 0.00, 6, '2019-11-12 06:37:57'),
(3265, '', 0.00, 6, '2019-11-12 06:37:57'),
(3266, '', 0.00, 6, '2019-11-12 06:37:57'),
(3267, '', 0.00, 6, '2019-11-12 06:37:57'),
(3268, '', 0.00, 6, '2019-11-12 06:37:57'),
(3269, '', 0.00, 6, '2019-11-12 06:37:58'),
(3270, '', 0.00, 6, '2019-11-12 06:37:58'),
(3271, '', 0.00, 6, '2019-11-12 06:37:58'),
(3272, '', 0.00, 6, '2019-11-12 06:37:59'),
(3273, '', 0.00, 6, '2019-11-12 06:37:59'),
(3274, '', 0.00, 6, '2019-11-12 06:38:00'),
(3275, '', 0.00, 6, '2019-11-12 06:38:00'),
(3276, '', 0.00, 6, '2019-11-12 06:38:00'),
(3277, '', 0.00, 6, '2019-11-12 06:38:00'),
(3278, '', 0.00, 6, '2019-11-12 06:38:00'),
(3279, '', 0.00, 6, '2019-11-12 06:38:00'),
(3280, '', 0.00, 6, '2019-11-12 06:38:00'),
(3281, '', 0.00, 6, '2019-11-12 06:38:00'),
(3282, '', 0.00, 6, '2019-11-12 06:38:00'),
(3283, '', 0.00, 6, '2019-11-12 06:38:00'),
(3284, '', 0.00, 6, '2019-11-12 06:38:00'),
(3285, '', 0.00, 6, '2019-11-12 06:38:00'),
(3286, '', 0.00, 6, '2019-11-12 06:38:01'),
(3287, '', 0.00, 6, '2019-11-12 06:38:01'),
(3288, '', 0.00, 6, '2019-11-12 06:38:01'),
(3289, '', 0.00, 6, '2019-11-12 06:38:01'),
(3290, '', 0.00, 6, '2019-11-12 06:38:01'),
(3291, '', 0.00, 6, '2019-11-12 06:38:01'),
(3292, '', 0.00, 6, '2019-11-12 06:38:01'),
(3293, '', 0.00, 6, '2019-11-12 06:38:01'),
(3294, '', 0.00, 6, '2019-11-12 06:38:01'),
(3295, '', 0.00, 6, '2019-11-12 06:38:01'),
(3296, '', 0.00, 6, '2019-11-12 06:38:01'),
(3297, '', 0.00, 6, '2019-11-12 06:38:01'),
(3298, '', 0.00, 6, '2019-11-12 06:38:02'),
(3299, '', 0.00, 6, '2019-11-12 06:38:02'),
(3300, '', 0.00, 6, '2019-11-12 06:38:02'),
(3301, '', 0.00, 6, '2019-11-12 06:38:02'),
(3302, '', 0.00, 6, '2019-11-12 06:38:02'),
(3303, '', 0.00, 6, '2019-11-12 06:38:02'),
(3304, '', 0.00, 6, '2019-11-12 06:38:02'),
(3305, '', 0.00, 6, '2019-11-12 06:38:02'),
(3306, '', 0.00, 6, '2019-11-12 06:38:02'),
(3307, '', 0.00, 6, '2019-11-12 06:38:02'),
(3308, '', 0.00, 6, '2019-11-12 06:38:02'),
(3309, '', 0.00, 6, '2019-11-12 06:38:02'),
(3310, '', 0.00, 6, '2019-11-12 06:38:02'),
(3311, '', 0.00, 6, '2019-11-12 06:38:03'),
(3312, '', 0.00, 6, '2019-11-12 06:38:03'),
(3313, '', 0.00, 6, '2019-11-12 06:38:03'),
(3314, '', 0.00, 6, '2019-11-12 06:38:03'),
(3315, '', 0.00, 6, '2019-11-12 06:38:03'),
(3316, '', 0.00, 6, '2019-11-12 06:38:03'),
(3317, '', 0.00, 6, '2019-11-12 06:38:03'),
(3318, '', 0.00, 6, '2019-11-12 06:38:03'),
(3319, '', 0.00, 6, '2019-11-12 06:38:03'),
(3320, '', 0.00, 6, '2019-11-12 06:38:03'),
(3321, '', 0.00, 6, '2019-11-12 06:38:03'),
(3322, '', 0.00, 6, '2019-11-12 06:38:03'),
(3323, '', 0.00, 6, '2019-11-12 06:38:03'),
(3324, '', 0.00, 6, '2019-11-12 06:38:03'),
(3325, '', 0.00, 6, '2019-11-12 06:38:03'),
(3326, '', 0.00, 6, '2019-11-12 06:38:04'),
(3327, '', 0.00, 6, '2019-11-12 06:38:04'),
(3328, '', 0.00, 6, '2019-11-12 06:38:04'),
(3329, '', 0.00, 6, '2019-11-12 06:38:04'),
(3330, '', 0.00, 6, '2019-11-12 06:38:04'),
(3331, '', 0.00, 6, '2019-11-12 06:38:04'),
(3332, '', 0.00, 6, '2019-11-12 06:38:04'),
(3333, '', 0.00, 6, '2019-11-12 06:38:04'),
(3334, '', 0.00, 6, '2019-11-12 06:38:04'),
(3335, '', 0.00, 6, '2019-11-12 06:38:04'),
(3336, '', 0.00, 6, '2019-11-12 06:38:04'),
(3337, '', 0.00, 6, '2019-11-12 06:38:04'),
(3338, '', 0.00, 6, '2019-11-12 06:38:04'),
(3339, '', 0.00, 6, '2019-11-12 06:38:05'),
(3340, '', 0.00, 6, '2019-11-12 06:38:05'),
(3341, '', 0.00, 6, '2019-11-12 06:38:05'),
(3342, '', 0.00, 6, '2019-11-12 06:38:05'),
(3343, '', 0.00, 6, '2019-11-12 06:38:05'),
(3344, '', 0.00, 6, '2019-11-12 06:38:06'),
(3345, '', 0.00, 6, '2019-11-12 06:38:06'),
(3346, '', 0.00, 6, '2019-11-12 06:38:06'),
(3347, '', 0.00, 6, '2019-11-12 06:38:06'),
(3348, '', 0.00, 6, '2019-11-12 06:38:06'),
(3349, '', 0.00, 6, '2019-11-12 06:38:06'),
(3350, '', 0.00, 6, '2019-11-12 06:38:06'),
(3351, '', 0.00, 6, '2019-11-12 06:38:06'),
(3352, '', 0.00, 6, '2019-11-12 06:38:06'),
(3353, '', 0.00, 6, '2019-11-12 06:38:06'),
(3354, '', 0.00, 6, '2019-11-12 06:38:06'),
(3355, '', 0.00, 6, '2019-11-12 06:38:06'),
(3356, '', 0.00, 6, '2019-11-12 06:38:06'),
(3357, '', 0.00, 6, '2019-11-12 06:38:06'),
(3358, '', 0.00, 6, '2019-11-12 06:38:06'),
(3359, '', 0.00, 6, '2019-11-12 06:38:06'),
(3360, '', 0.00, 6, '2019-11-12 06:38:06'),
(3361, '', 0.00, 6, '2019-11-12 06:38:06'),
(3362, '', 0.00, 6, '2019-11-12 06:38:07'),
(3363, '', 0.00, 6, '2019-11-12 06:38:07'),
(3364, '', 0.00, 6, '2019-11-12 06:38:07'),
(3365, '', 0.00, 6, '2019-11-12 06:38:07'),
(3366, '', 0.00, 6, '2019-11-12 06:38:07'),
(3367, '', 0.00, 6, '2019-11-12 06:38:07'),
(3368, '', 0.00, 6, '2019-11-12 06:38:07'),
(3369, '', 0.00, 6, '2019-11-12 06:38:07'),
(3370, '', 0.00, 6, '2019-11-12 06:38:07'),
(3371, '', 0.00, 6, '2019-11-12 06:38:07'),
(3372, '', 0.00, 6, '2019-11-12 06:38:07'),
(3373, '', 0.00, 6, '2019-11-12 06:38:07'),
(3374, '', 0.00, 6, '2019-11-12 06:38:07'),
(3375, '', 0.00, 6, '2019-11-12 06:38:07'),
(3376, '', 0.00, 6, '2019-11-12 06:38:07'),
(3377, '', 0.00, 6, '2019-11-12 06:38:08'),
(3378, '', 0.00, 6, '2019-11-12 06:38:08'),
(3379, '', 0.00, 6, '2019-11-12 06:38:08'),
(3380, '', 0.00, 6, '2019-11-12 06:38:08'),
(3381, '', 0.00, 6, '2019-11-12 06:38:08'),
(3382, '', 0.00, 6, '2019-11-12 06:38:08'),
(3383, '', 0.00, 6, '2019-11-12 06:38:08'),
(3384, '', 0.00, 6, '2019-11-12 06:38:08'),
(3385, '', 0.00, 6, '2019-11-12 06:38:08'),
(3386, '', 0.00, 6, '2019-11-12 06:38:08'),
(3387, '', 0.00, 6, '2019-11-12 06:38:08'),
(3388, '', 0.00, 6, '2019-11-12 06:38:08'),
(3389, '', 0.00, 6, '2019-11-12 06:38:08'),
(3390, '', 0.00, 6, '2019-11-12 06:38:08'),
(3391, '', 0.00, 6, '2019-11-12 06:38:08'),
(3392, '', 0.00, 6, '2019-11-12 06:38:08'),
(3393, '', 0.00, 6, '2019-11-12 06:38:08'),
(3394, '', 0.00, 6, '2019-11-12 06:38:09'),
(3395, '', 0.00, 6, '2019-11-12 06:38:09'),
(3396, '', 0.00, 6, '2019-11-12 06:38:09'),
(3397, '', 0.00, 6, '2019-11-12 06:38:09'),
(3398, '', 0.00, 6, '2019-11-12 06:38:09'),
(3399, '', 0.00, 6, '2019-11-12 06:38:09'),
(3400, '', 0.00, 6, '2019-11-12 06:38:09'),
(3401, '', 0.00, 6, '2019-11-12 06:38:09'),
(3402, '', 0.00, 6, '2019-11-12 06:38:09'),
(3403, '', 0.00, 6, '2019-11-12 06:38:09'),
(3404, '', 0.00, 6, '2019-11-12 06:38:09'),
(3405, '', 0.00, 6, '2019-11-12 06:38:09'),
(3406, '', 0.00, 6, '2019-11-12 06:38:09'),
(3407, '', 0.00, 6, '2019-11-12 06:38:09'),
(3408, '', 0.00, 6, '2019-11-12 06:38:09'),
(3409, '', 0.00, 6, '2019-11-12 06:38:09'),
(3410, '', 0.00, 6, '2019-11-12 06:38:10'),
(3411, '', 0.00, 6, '2019-11-12 06:38:10'),
(3412, '', 0.00, 6, '2019-11-12 06:38:11'),
(3413, '', 0.00, 6, '2019-11-12 06:38:11'),
(3414, '', 0.00, 6, '2019-11-12 06:38:11'),
(3415, '', 0.00, 6, '2019-11-12 06:38:11'),
(3416, '', 0.00, 6, '2019-11-12 06:38:11'),
(3417, '', 0.00, 6, '2019-11-12 06:38:12'),
(3418, '', 0.00, 6, '2019-11-12 06:38:12'),
(3419, '', 0.00, 6, '2019-11-12 06:38:12'),
(3420, '', 0.00, 6, '2019-11-12 06:38:12'),
(3421, '', 0.00, 6, '2019-11-12 06:38:12'),
(3422, '', 0.00, 6, '2019-11-12 06:38:12'),
(3423, '', 0.00, 6, '2019-11-12 06:38:12'),
(3424, '', 0.00, 6, '2019-11-12 06:38:12'),
(3425, '', 0.00, 6, '2019-11-12 06:38:12'),
(3426, '', 0.00, 6, '2019-11-12 06:38:12'),
(3427, '', 0.00, 6, '2019-11-12 06:38:12'),
(3428, '', 0.00, 6, '2019-11-12 06:38:12'),
(3429, '', 0.00, 6, '2019-11-12 06:38:12'),
(3430, '', 0.00, 6, '2019-11-12 06:38:12'),
(3431, '', 0.00, 6, '2019-11-12 06:38:12'),
(3432, '', 0.00, 6, '2019-11-12 06:38:12'),
(3433, '', 0.00, 6, '2019-11-12 06:38:13'),
(3434, '', 0.00, 6, '2019-11-12 06:38:13'),
(3435, '', 0.00, 6, '2019-11-12 06:38:13'),
(3436, '', 0.00, 6, '2019-11-12 06:38:13'),
(3437, '', 0.00, 6, '2019-11-12 06:38:13'),
(3438, '', 0.00, 6, '2019-11-12 06:38:13'),
(3439, '', 0.00, 6, '2019-11-12 06:38:13'),
(3440, '', 0.00, 6, '2019-11-12 06:38:13'),
(3441, '', 0.00, 6, '2019-11-12 06:38:13'),
(3442, '', 0.00, 6, '2019-11-12 06:38:13'),
(3443, '', 0.00, 6, '2019-11-12 06:38:13'),
(3444, '', 0.00, 6, '2019-11-12 06:38:13'),
(3445, '', 0.00, 6, '2019-11-12 06:38:13'),
(3446, '', 0.00, 6, '2019-11-12 06:38:13'),
(3447, '', 0.00, 6, '2019-11-12 06:38:13'),
(3448, '', 0.00, 6, '2019-11-12 06:38:13'),
(3449, '', 0.00, 6, '2019-11-12 06:38:13'),
(3450, '', 0.00, 6, '2019-11-12 06:38:14'),
(3451, '', 0.00, 6, '2019-11-12 06:38:14'),
(3452, '', 0.00, 6, '2019-11-12 06:38:14'),
(3453, '', 0.00, 6, '2019-11-12 06:38:14'),
(3454, '', 0.00, 6, '2019-11-12 06:38:14'),
(3455, '', 0.00, 6, '2019-11-12 06:38:14'),
(3456, '', 0.00, 6, '2019-11-12 06:38:14'),
(3457, '', 0.00, 6, '2019-11-12 06:38:14'),
(3458, '', 0.00, 6, '2019-11-12 06:38:14'),
(3459, '', 0.00, 6, '2019-11-12 06:38:14'),
(3460, '', 0.00, 6, '2019-11-12 06:38:15'),
(3461, '', 0.00, 6, '2019-11-12 06:38:15'),
(3462, '', 0.00, 6, '2019-11-12 06:38:15'),
(3463, '', 0.00, 6, '2019-11-12 06:38:15'),
(3464, '', 0.00, 6, '2019-11-12 06:38:15'),
(3465, '', 0.00, 6, '2019-11-12 06:38:15'),
(3466, '', 0.00, 6, '2019-11-12 06:38:15'),
(3467, '', 0.00, 6, '2019-11-12 06:38:15'),
(3468, '', 0.00, 6, '2019-11-12 06:38:15'),
(3469, '', 0.00, 6, '2019-11-12 06:38:15'),
(3470, '', 0.00, 6, '2019-11-12 06:38:15'),
(3471, '', 0.00, 6, '2019-11-12 06:38:15'),
(3472, '', 0.00, 6, '2019-11-12 06:38:15'),
(3473, '', 0.00, 6, '2019-11-12 06:38:15'),
(3474, '', 0.00, 6, '2019-11-12 06:38:15'),
(3475, '', 0.00, 6, '2019-11-12 06:38:15'),
(3476, '', 0.00, 6, '2019-11-12 06:38:15'),
(3477, '', 0.00, 6, '2019-11-12 06:38:15'),
(3478, '', 0.00, 6, '2019-11-12 06:38:15'),
(3479, '', 0.00, 6, '2019-11-12 06:38:16'),
(3480, '', 0.00, 6, '2019-11-12 06:38:16'),
(3481, '', 0.00, 6, '2019-11-12 06:38:16'),
(3482, '', 0.00, 6, '2019-11-12 06:38:16'),
(3483, '', 0.00, 6, '2019-11-12 06:38:16'),
(3484, '', 0.00, 6, '2019-11-12 06:38:16'),
(3485, '', 0.00, 6, '2019-11-12 06:38:16'),
(3486, '', 0.00, 6, '2019-11-12 06:38:16'),
(3487, '', 0.00, 6, '2019-11-12 06:38:16'),
(3488, '', 0.00, 6, '2019-11-12 06:38:16'),
(3489, '', 0.00, 6, '2019-11-12 06:38:16'),
(3490, '', 0.00, 6, '2019-11-12 06:38:16'),
(3491, '', 0.00, 6, '2019-11-12 06:38:16'),
(3492, '', 0.00, 6, '2019-11-12 06:38:16'),
(3493, '', 0.00, 6, '2019-11-12 06:38:17'),
(3494, '', 0.00, 6, '2019-11-12 06:38:17'),
(3495, '', 0.00, 6, '2019-11-12 06:38:17'),
(3496, '', 0.00, 6, '2019-11-12 06:38:17'),
(3497, '', 0.00, 6, '2019-11-12 06:38:17'),
(3498, '', 0.00, 6, '2019-11-12 06:38:17'),
(3499, '', 0.00, 6, '2019-11-12 06:38:17'),
(3500, '', 0.00, 6, '2019-11-12 06:38:17'),
(3501, '', 0.00, 6, '2019-11-12 06:38:17'),
(3502, '', 0.00, 6, '2019-11-12 06:38:17'),
(3503, '', 0.00, 6, '2019-11-12 06:38:17'),
(3504, '', 0.00, 6, '2019-11-12 06:38:17'),
(3505, '', 0.00, 6, '2019-11-12 06:38:17'),
(3506, '', 0.00, 6, '2019-11-12 06:38:17'),
(3507, '\0\0\0', 0.00, 6, '2019-11-12 06:38:17'),
(3508, '\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:17'),
(3509, '\0\0\0\0S', 0.00, 6, '2019-11-12 06:38:17'),
(3510, '', 0.00, 6, '2019-11-12 06:38:17'),
(3511, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:17'),
(3512, '', 0.00, 6, '2019-11-12 06:38:17'),
(3513, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:18'),
(3514, '', 0.00, 6, '2019-11-12 06:38:18'),
(3515, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:18'),
(3516, '', 0.00, 6, '2019-11-12 06:38:18'),
(3517, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:18'),
(3518, '', 0.00, 6, '2019-11-12 06:38:18'),
(3519, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:18'),
(3520, '', 0.00, 6, '2019-11-12 06:38:18'),
(3521, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:18'),
(3522, '\0\0\0R', 0.00, 6, '2019-11-12 06:38:18'),
(3523, '', 0.00, 6, '2019-11-12 06:38:18'),
(3524, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:18'),
(3525, '\0\0\0R', 0.00, 6, '2019-11-12 06:38:18'),
(3526, '', 0.00, 6, '2019-11-12 06:38:18'),
(3527, '\0\0\0\0	\0\0~', 0.00, 6, '2019-11-12 06:38:18'),
(3528, '', 0.00, 6, '2019-11-12 06:38:18'),
(3529, '\0	\0\0\0', 0.00, 6, '2019-11-12 06:38:18'),
(3530, '\0\0~', 0.00, 6, '2019-11-12 06:38:18'),
(3531, '', 0.00, 6, '2019-11-12 06:38:18'),
(3532, '\0', 0.00, 6, '2019-11-12 06:38:18'),
(3533, '\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:18'),
(3534, '\0', 0.00, 6, '2019-11-12 06:38:19'),
(3535, '', 0.00, 6, '2019-11-12 06:38:19'),
(3536, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:19'),
(3537, '', 0.00, 6, '2019-11-12 06:38:19'),
(3538, '\0\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:38:19'),
(3539, '', 0.00, 6, '2019-11-12 06:38:19'),
(3540, '\0\r\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:19'),
(3541, '', 0.00, 6, '2019-11-12 06:38:19'),
(3542, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:19'),
(3543, '', 0.00, 6, '2019-11-12 06:38:19'),
(3544, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:19'),
(3545, '', 0.00, 6, '2019-11-12 06:38:19'),
(3546, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:19'),
(3547, '', 0.00, 6, '2019-11-12 06:38:19'),
(3548, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:19'),
(3549, '', 0.00, 6, '2019-11-12 06:38:20'),
(3550, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:20'),
(3551, '', 0.00, 6, '2019-11-12 06:38:20'),
(3552, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:20'),
(3553, '', 0.00, 6, '2019-11-12 06:38:20'),
(3554, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:20'),
(3555, '', 0.00, 6, '2019-11-12 06:38:20'),
(3556, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:20'),
(3557, '', 0.00, 6, '2019-11-12 06:38:20'),
(3558, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:20'),
(3559, '', 0.00, 6, '2019-11-12 06:38:20'),
(3560, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:21'),
(3561, '', 0.00, 6, '2019-11-12 06:38:21'),
(3562, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:21'),
(3563, '', 0.00, 6, '2019-11-12 06:38:21'),
(3564, '\0\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:38:22'),
(3565, '', 0.00, 6, '2019-11-12 06:38:22'),
(3566, '\0\Z\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:22'),
(3567, '', 0.00, 6, '2019-11-12 06:38:22'),
(3568, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:22'),
(3569, '', 0.00, 6, '2019-11-12 06:38:22'),
(3570, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:22'),
(3571, '', 0.00, 6, '2019-11-12 06:38:22'),
(3572, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:22'),
(3573, '', 0.00, 6, '2019-11-12 06:38:22'),
(3574, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:23'),
(3575, '', 0.00, 6, '2019-11-12 06:38:23'),
(3576, '\0\0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:38:23'),
(3577, '', 0.00, 6, '2019-11-12 06:38:23'),
(3578, '\0 \0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:38:23'),
(3579, '', 0.00, 6, '2019-11-12 06:38:23'),
(3580, '\0!\0\0\0&quot;\0\0~', 0.00, 6, '2019-11-12 06:38:23'),
(3581, '', 0.00, 6, '2019-11-12 06:38:23'),
(3582, '\0&quot;\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:38:23'),
(3583, '', 0.00, 6, '2019-11-12 06:38:23'),
(3584, '\0#\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:38:23'),
(3585, '', 0.00, 6, '2019-11-12 06:38:23'),
(3586, '\0$\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:38:23'),
(3587, '', 0.00, 6, '2019-11-12 06:38:23'),
(3588, '\0%\0\0\0&amp;\0\0~', 0.00, 6, '2019-11-12 06:38:23'),
(3589, '', 0.00, 6, '2019-11-12 06:38:23'),
(3590, '\0&amp;\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:38:23'),
(3591, '', 0.00, 6, '2019-11-12 06:38:23'),
(3592, '\0\'\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:38:23'),
(3593, '\0\'\0\0R', 0.00, 6, '2019-11-12 06:38:23'),
(3594, '', 0.00, 6, '2019-11-12 06:38:23'),
(3595, '\0(\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:38:23'),
(3596, '', 0.00, 6, '2019-11-12 06:38:23'),
(3597, '\0)\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:38:23'),
(3598, '', 0.00, 6, '2019-11-12 06:38:23'),
(3599, '', 0.00, 6, '2019-11-12 06:38:23'),
(3600, '\0+\0\0\0', 0.00, 6, '2019-11-12 06:38:23'),
(3601, '', 0.00, 6, '2019-11-12 06:38:23'),
(3602, '\0', 0.00, 6, '2019-11-12 06:38:23'),
(3603, '\0', 0.00, 6, '2019-11-12 06:38:24'),
(3604, '\0-\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:38:24'),
(3605, '', 0.00, 6, '2019-11-12 06:38:24'),
(3606, '\0.\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:38:24'),
(3607, '', 0.00, 6, '2019-11-12 06:38:24'),
(3608, '\0/\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:38:24'),
(3609, '', 0.00, 6, '2019-11-12 06:38:24'),
(3610, '\00\0\0\01\0\0~', 0.00, 6, '2019-11-12 06:38:24'),
(3611, '', 0.00, 6, '2019-11-12 06:38:24'),
(3612, '\01\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:38:24'),
(3613, '', 0.00, 6, '2019-11-12 06:38:24'),
(3614, '\02\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:38:24'),
(3615, '', 0.00, 6, '2019-11-12 06:38:24'),
(3616, '\03\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:38:24'),
(3617, '', 0.00, 6, '2019-11-12 06:38:24'),
(3618, '\04\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:38:24'),
(3619, '', 0.00, 6, '2019-11-12 06:38:24'),
(3620, '\05\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:38:25'),
(3621, '', 0.00, 6, '2019-11-12 06:38:25'),
(3622, '\06\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:38:25'),
(3623, '', 0.00, 6, '2019-11-12 06:38:25'),
(3624, '\07\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:38:25'),
(3625, '', 0.00, 6, '2019-11-12 06:38:25'),
(3626, '\08\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:38:25'),
(3627, '', 0.00, 6, '2019-11-12 06:38:25'),
(3628, '\09\0\0\0:\0\0~', 0.00, 6, '2019-11-12 06:38:25'),
(3629, '', 0.00, 6, '2019-11-12 06:38:25'),
(3630, '\0:\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:38:25'),
(3631, '', 0.00, 6, '2019-11-12 06:38:26'),
(3632, '\0;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:38:26'),
(3633, '\0;\0\0', 0.00, 6, '2019-11-12 06:38:26'),
(3634, '', 0.00, 6, '2019-11-12 06:38:26'),
(3635, '\0&lt;\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:38:26'),
(3636, '', 0.00, 6, '2019-11-12 06:38:26'),
(3637, '\0=\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:38:26'),
(3638, '', 0.00, 6, '2019-11-12 06:38:26'),
(3639, '\0&gt;\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:38:26'),
(3640, '', 0.00, 6, '2019-11-12 06:38:26'),
(3641, '\0?\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:38:26'),
(3642, '', 0.00, 6, '2019-11-12 06:38:26'),
(3643, '\0@\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:38:26'),
(3644, '', 0.00, 6, '2019-11-12 06:38:26'),
(3645, '\0A\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:38:26'),
(3646, '', 0.00, 6, '2019-11-12 06:38:27'),
(3647, '\0B\0\0\0C\0\0~', 0.00, 6, '2019-11-12 06:38:27'),
(3648, '', 0.00, 6, '2019-11-12 06:38:27'),
(3649, '\0C\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:38:27'),
(3650, '', 0.00, 6, '2019-11-12 06:38:27'),
(3651, '\0D\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:38:27'),
(3652, '', 0.00, 6, '2019-11-12 06:38:27'),
(3653, '\0E\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:38:27'),
(3654, '', 0.00, 6, '2019-11-12 06:38:28'),
(3655, '\0F\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:38:28'),
(3656, '', 0.00, 6, '2019-11-12 06:38:28'),
(3657, '\0G\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:38:28'),
(3658, '', 0.00, 6, '2019-11-12 06:38:28'),
(3659, '\0H\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:38:28'),
(3660, '', 0.00, 6, '2019-11-12 06:38:29'),
(3661, '\0I\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:38:29'),
(3662, '', 0.00, 6, '2019-11-12 06:38:29'),
(3663, '\0J\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:38:29'),
(3664, '', 0.00, 6, '2019-11-12 06:38:29'),
(3665, '\0K\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:38:29'),
(3666, '', 0.00, 6, '2019-11-12 06:38:29'),
(3667, '\0L\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:38:29'),
(3668, '', 0.00, 6, '2019-11-12 06:38:29'),
(3669, '\0M\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:38:29'),
(3670, '', 0.00, 6, '2019-11-12 06:38:29'),
(3671, '', 0.00, 6, '2019-11-12 06:38:29'),
(3672, '\0O\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:38:29'),
(3673, '', 0.00, 6, '2019-11-12 06:38:30'),
(3674, '\0P\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:38:30'),
(3675, '', 0.00, 6, '2019-11-12 06:38:30'),
(3676, '\0Q\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:38:31'),
(3677, '', 0.00, 6, '2019-11-12 06:38:31'),
(3678, '\0R\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:38:31'),
(3679, '', 0.00, 6, '2019-11-12 06:38:31'),
(3680, '\0S\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:38:31'),
(3681, '', 0.00, 6, '2019-11-12 06:38:31'),
(3682, '\0T\0\0\0U\0\0~', 0.00, 6, '2019-11-12 06:38:31'),
(3683, '', 0.00, 6, '2019-11-12 06:38:31'),
(3684, '\0U\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:38:31'),
(3685, '', 0.00, 6, '2019-11-12 06:38:31'),
(3686, '\0V\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:38:31'),
(3687, '', 0.00, 6, '2019-11-12 06:38:31'),
(3688, '\0W\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:38:31'),
(3689, '', 0.00, 6, '2019-11-12 06:38:31'),
(3690, '\0X\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:38:31'),
(3691, '', 0.00, 6, '2019-11-12 06:38:32'),
(3692, '\0Y\0\0\0Z\0\0~', 0.00, 6, '2019-11-12 06:38:32'),
(3693, '', 0.00, 6, '2019-11-12 06:38:32'),
(3694, '\0Z\0\0\0[\0\0~', 0.00, 6, '2019-11-12 06:38:32'),
(3695, '', 0.00, 6, '2019-11-12 06:38:32'),
(3696, '\0[\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:38:32'),
(3697, '', 0.00, 6, '2019-11-12 06:38:32'),
(3698, '\0\\\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:38:32'),
(3699, '\0\\\0\0', 0.00, 6, '2019-11-12 06:38:33'),
(3700, '', 0.00, 6, '2019-11-12 06:38:34'),
(3701, '\0]\0\0\0^\0\0~', 0.00, 6, '2019-11-12 06:38:34'),
(3702, '', 0.00, 6, '2019-11-12 06:38:34'),
(3703, '\0^\0\0\0_\0\0~', 0.00, 6, '2019-11-12 06:38:34'),
(3704, '', 0.00, 6, '2019-11-12 06:38:34'),
(3705, '\0_\0\0\0`\0\0~', 0.00, 6, '2019-11-12 06:38:35'),
(3706, '\0_\0\0B)\0\0\0`\0\0\0', 0.00, 6, '2019-11-12 06:38:35'),
(3707, '\0`\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:38:35'),
(3708, '', 0.00, 6, '2019-11-12 06:38:35'),
(3709, '\0a\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:38:35'),
(3710, '', 0.00, 6, '2019-11-12 06:38:35'),
(3711, '\0b\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:38:35'),
(3712, '', 0.00, 6, '2019-11-12 06:38:35'),
(3713, '\0c\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:38:35'),
(3714, '', 0.00, 6, '2019-11-12 06:38:35'),
(3715, '\0d\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:38:35'),
(3716, '', 0.00, 6, '2019-11-12 06:38:35'),
(3717, '\0e\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:38:35'),
(3718, '', 0.00, 6, '2019-11-12 06:38:35'),
(3719, '\0f\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:38:35'),
(3720, '', 0.00, 6, '2019-11-12 06:38:35'),
(3721, '\0g\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:38:35'),
(3722, '', 0.00, 6, '2019-11-12 06:38:35'),
(3723, '\0h\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:38:35'),
(3724, '', 0.00, 6, '2019-11-12 06:38:35'),
(3725, '\0i\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:38:36'),
(3726, '', 0.00, 6, '2019-11-12 06:38:36'),
(3727, '\0j\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:38:36'),
(3728, '', 0.00, 6, '2019-11-12 06:38:36'),
(3729, '\0k\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:38:36'),
(3730, '', 0.00, 6, '2019-11-12 06:38:36'),
(3731, '\0l\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:38:36'),
(3732, '', 0.00, 6, '2019-11-12 06:38:36'),
(3733, '\0m\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:38:36'),
(3734, '', 0.00, 6, '2019-11-12 06:38:36'),
(3735, '\0n\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:38:36'),
(3736, '', 0.00, 6, '2019-11-12 06:38:36'),
(3737, '\0o\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:38:37'),
(3738, '', 0.00, 6, '2019-11-12 06:38:37'),
(3739, '\0p\0\0\0q\0\0~', 0.00, 6, '2019-11-12 06:38:37'),
(3740, '', 0.00, 6, '2019-11-12 06:38:37'),
(3741, '\0q\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:38:37'),
(3742, '', 0.00, 6, '2019-11-12 06:38:37'),
(3743, '\0r\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:38:37'),
(3744, '', 0.00, 6, '2019-11-12 06:38:37'),
(3745, '\0s\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:38:37'),
(3746, '', 0.00, 6, '2019-11-12 06:38:37'),
(3747, '\0t\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:38:37'),
(3748, '', 0.00, 6, '2019-11-12 06:38:37'),
(3749, '\0u\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:38:37'),
(3750, '', 0.00, 6, '2019-11-12 06:38:37'),
(3751, '\0v\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:38:37'),
(3752, '', 0.00, 6, '2019-11-12 06:38:37'),
(3753, '\0w\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:38:37'),
(3754, '', 0.00, 6, '2019-11-12 06:38:37'),
(3755, '\0x\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:38:37'),
(3756, '', 0.00, 6, '2019-11-12 06:38:38'),
(3757, '\0y\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:38:38'),
(3758, '', 0.00, 6, '2019-11-12 06:38:38'),
(3759, '\0z\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:38:38'),
(3760, '', 0.00, 6, '2019-11-12 06:38:38'),
(3761, '\0{\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:38:38'),
(3762, '\0{\0\0', 0.00, 6, '2019-11-12 06:38:38'),
(3763, '', 0.00, 6, '2019-11-12 06:38:38'),
(3764, '\0|\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:38:38'),
(3765, '', 0.00, 6, '2019-11-12 06:38:38'),
(3766, '\0}\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:38:38'),
(3767, '', 0.00, 6, '2019-11-12 06:38:39'),
(3768, '\0~\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:39'),
(3769, '', 0.00, 6, '2019-11-12 06:38:39'),
(3770, '', 0.00, 6, '2019-11-12 06:38:39'),
(3771, '', 0.00, 6, '2019-11-12 06:38:39'),
(3772, '', 0.00, 6, '2019-11-12 06:38:39'),
(3773, '', 0.00, 6, '2019-11-12 06:38:39'),
(3774, '', 0.00, 6, '2019-11-12 06:38:39'),
(3775, '', 0.00, 6, '2019-11-12 06:38:39'),
(3776, '', 0.00, 6, '2019-11-12 06:38:39'),
(3777, '', 0.00, 6, '2019-11-12 06:38:39'),
(3778, '', 0.00, 6, '2019-11-12 06:38:39'),
(3779, '', 0.00, 6, '2019-11-12 06:38:40'),
(3780, '', 0.00, 6, '2019-11-12 06:38:40'),
(3781, '', 0.00, 6, '2019-11-12 06:38:40'),
(3782, '', 0.00, 6, '2019-11-12 06:38:40'),
(3783, '', 0.00, 6, '2019-11-12 06:38:40'),
(3784, '', 0.00, 6, '2019-11-12 06:38:40'),
(3785, '', 0.00, 6, '2019-11-12 06:38:40'),
(3786, '', 0.00, 6, '2019-11-12 06:38:40'),
(3787, '', 0.00, 6, '2019-11-12 06:38:40'),
(3788, '', 0.00, 6, '2019-11-12 06:38:40'),
(3789, '', 0.00, 6, '2019-11-12 06:38:40'),
(3790, '', 0.00, 6, '2019-11-12 06:38:40'),
(3791, '', 0.00, 6, '2019-11-12 06:38:40'),
(3792, '', 0.00, 6, '2019-11-12 06:38:40'),
(3793, '', 0.00, 6, '2019-11-12 06:38:40'),
(3794, '', 0.00, 6, '2019-11-12 06:38:40'),
(3795, '', 0.00, 6, '2019-11-12 06:38:40'),
(3796, '', 0.00, 6, '2019-11-12 06:38:40'),
(3797, '', 0.00, 6, '2019-11-12 06:38:41'),
(3798, '', 0.00, 6, '2019-11-12 06:38:41'),
(3799, '', 0.00, 6, '2019-11-12 06:38:41'),
(3800, '', 0.00, 6, '2019-11-12 06:38:41'),
(3801, '', 0.00, 6, '2019-11-12 06:38:41'),
(3802, '', 0.00, 6, '2019-11-12 06:38:41'),
(3803, '', 0.00, 6, '2019-11-12 06:38:41'),
(3804, '', 0.00, 6, '2019-11-12 06:38:41'),
(3805, '', 0.00, 6, '2019-11-12 06:38:41'),
(3806, '', 0.00, 6, '2019-11-12 06:38:41'),
(3807, '', 0.00, 6, '2019-11-12 06:38:41'),
(3808, '', 0.00, 6, '2019-11-12 06:38:41'),
(3809, '', 0.00, 6, '2019-11-12 06:38:41'),
(3810, '', 0.00, 6, '2019-11-12 06:38:41'),
(3811, '', 0.00, 6, '2019-11-12 06:38:41'),
(3812, '', 0.00, 6, '2019-11-12 06:38:41'),
(3813, '', 0.00, 6, '2019-11-12 06:38:41'),
(3814, '', 0.00, 6, '2019-11-12 06:38:41'),
(3815, '', 0.00, 6, '2019-11-12 06:38:42'),
(3816, '', 0.00, 6, '2019-11-12 06:38:42'),
(3817, '', 0.00, 6, '2019-11-12 06:38:42'),
(3818, '', 0.00, 6, '2019-11-12 06:38:42'),
(3819, '', 0.00, 6, '2019-11-12 06:38:42'),
(3820, '', 0.00, 6, '2019-11-12 06:38:42'),
(3821, '', 0.00, 6, '2019-11-12 06:38:42'),
(3822, '', 0.00, 6, '2019-11-12 06:38:42'),
(3823, '', 0.00, 6, '2019-11-12 06:38:42'),
(3824, '', 0.00, 6, '2019-11-12 06:38:42'),
(3825, '', 0.00, 6, '2019-11-12 06:38:42'),
(3826, '', 0.00, 6, '2019-11-12 06:38:42'),
(3827, '', 0.00, 6, '2019-11-12 06:38:42'),
(3828, '', 0.00, 6, '2019-11-12 06:38:42'),
(3829, '', 0.00, 6, '2019-11-12 06:38:42'),
(3830, '', 0.00, 6, '2019-11-12 06:38:42'),
(3831, '', 0.00, 6, '2019-11-12 06:38:42'),
(3832, '', 0.00, 6, '2019-11-12 06:38:43'),
(3833, '', 0.00, 6, '2019-11-12 06:38:43'),
(3834, '', 0.00, 6, '2019-11-12 06:38:43'),
(3835, '', 0.00, 6, '2019-11-12 06:38:43'),
(3836, '', 0.00, 6, '2019-11-12 06:38:43'),
(3837, '', 0.00, 6, '2019-11-12 06:38:43'),
(3838, '', 0.00, 6, '2019-11-12 06:38:43'),
(3839, '', 0.00, 6, '2019-11-12 06:38:43'),
(3840, '', 0.00, 6, '2019-11-12 06:38:43'),
(3841, '', 0.00, 6, '2019-11-12 06:38:43'),
(3842, '', 0.00, 6, '2019-11-12 06:38:43'),
(3843, '', 0.00, 6, '2019-11-12 06:38:43'),
(3844, '', 0.00, 6, '2019-11-12 06:38:43'),
(3845, '', 0.00, 6, '2019-11-12 06:38:43'),
(3846, '', 0.00, 6, '2019-11-12 06:38:43'),
(3847, '', 0.00, 6, '2019-11-12 06:38:43'),
(3848, '', 0.00, 6, '2019-11-12 06:38:43'),
(3849, '', 0.00, 6, '2019-11-12 06:38:43'),
(3850, '', 0.00, 6, '2019-11-12 06:38:43'),
(3851, '', 0.00, 6, '2019-11-12 06:38:43'),
(3852, '', 0.00, 6, '2019-11-12 06:38:44'),
(3853, '', 0.00, 6, '2019-11-12 06:38:44'),
(3854, '', 0.00, 6, '2019-11-12 06:38:44'),
(3855, '', 0.00, 6, '2019-11-12 06:38:44'),
(3856, '', 0.00, 6, '2019-11-12 06:38:44'),
(3857, '', 0.00, 6, '2019-11-12 06:38:44'),
(3858, '', 0.00, 6, '2019-11-12 06:38:44'),
(3859, '', 0.00, 6, '2019-11-12 06:38:44'),
(3860, '', 0.00, 6, '2019-11-12 06:38:44'),
(3861, '', 0.00, 6, '2019-11-12 06:38:44'),
(3862, '', 0.00, 6, '2019-11-12 06:38:44'),
(3863, '', 0.00, 6, '2019-11-12 06:38:44'),
(3864, '', 0.00, 6, '2019-11-12 06:38:44'),
(3865, '', 0.00, 6, '2019-11-12 06:38:44'),
(3866, '', 0.00, 6, '2019-11-12 06:38:44'),
(3867, '', 0.00, 6, '2019-11-12 06:38:44'),
(3868, '', 0.00, 6, '2019-11-12 06:38:45'),
(3869, '', 0.00, 6, '2019-11-12 06:38:45'),
(3870, '', 0.00, 6, '2019-11-12 06:38:45'),
(3871, '', 0.00, 6, '2019-11-12 06:38:45'),
(3872, '', 0.00, 6, '2019-11-12 06:38:45'),
(3873, '', 0.00, 6, '2019-11-12 06:38:46'),
(3874, '', 0.00, 6, '2019-11-12 06:38:46'),
(3875, '', 0.00, 6, '2019-11-12 06:38:46'),
(3876, '', 0.00, 6, '2019-11-12 06:38:46'),
(3877, '', 0.00, 6, '2019-11-12 06:38:46'),
(3878, '', 0.00, 6, '2019-11-12 06:38:46'),
(3879, '', 0.00, 6, '2019-11-12 06:38:46'),
(3880, '', 0.00, 6, '2019-11-12 06:38:46'),
(3881, '', 0.00, 6, '2019-11-12 06:38:46'),
(3882, '', 0.00, 6, '2019-11-12 06:38:46'),
(3883, '', 0.00, 6, '2019-11-12 06:38:46'),
(3884, '', 0.00, 6, '2019-11-12 06:38:46'),
(3885, '', 0.00, 6, '2019-11-12 06:38:46'),
(3886, '', 0.00, 6, '2019-11-12 06:38:46'),
(3887, '', 0.00, 6, '2019-11-12 06:38:46'),
(3888, '', 0.00, 6, '2019-11-12 06:38:46'),
(3889, '', 0.00, 6, '2019-11-12 06:38:46'),
(3890, '', 0.00, 6, '2019-11-12 06:38:46'),
(3891, '', 0.00, 6, '2019-11-12 06:38:47'),
(3892, '', 0.00, 6, '2019-11-12 06:38:47'),
(3893, '', 0.00, 6, '2019-11-12 06:38:47'),
(3894, '', 0.00, 6, '2019-11-12 06:38:47'),
(3895, '', 0.00, 6, '2019-11-12 06:38:47'),
(3896, '', 0.00, 6, '2019-11-12 06:38:47'),
(3897, '', 0.00, 6, '2019-11-12 06:38:47'),
(3898, '', 0.00, 6, '2019-11-12 06:38:47'),
(3899, '', 0.00, 6, '2019-11-12 06:38:47'),
(3900, '', 0.00, 6, '2019-11-12 06:38:47'),
(3901, '', 0.00, 6, '2019-11-12 06:38:47'),
(3902, '', 0.00, 6, '2019-11-12 06:38:47'),
(3903, '', 0.00, 6, '2019-11-12 06:38:47'),
(3904, '', 0.00, 6, '2019-11-12 06:38:47'),
(3905, '', 0.00, 6, '2019-11-12 06:38:47'),
(3906, '', 0.00, 6, '2019-11-12 06:38:47'),
(3907, '', 0.00, 6, '2019-11-12 06:38:48'),
(3908, '', 0.00, 6, '2019-11-12 06:38:48'),
(3909, '', 0.00, 6, '2019-11-12 06:38:48'),
(3910, '', 0.00, 6, '2019-11-12 06:38:48'),
(3911, '', 0.00, 6, '2019-11-12 06:38:48'),
(3912, '', 0.00, 6, '2019-11-12 06:38:48'),
(3913, '', 0.00, 6, '2019-11-12 06:38:48'),
(3914, '', 0.00, 6, '2019-11-12 06:38:48'),
(3915, '', 0.00, 6, '2019-11-12 06:38:48'),
(3916, '', 0.00, 6, '2019-11-12 06:38:48'),
(3917, '', 0.00, 6, '2019-11-12 06:38:48'),
(3918, '', 0.00, 6, '2019-11-12 06:38:48'),
(3919, '', 0.00, 6, '2019-11-12 06:38:48'),
(3920, '', 0.00, 6, '2019-11-12 06:38:48'),
(3921, '', 0.00, 6, '2019-11-12 06:38:48'),
(3922, '', 0.00, 6, '2019-11-12 06:38:48'),
(3923, '', 0.00, 6, '2019-11-12 06:38:48'),
(3924, '', 0.00, 6, '2019-11-12 06:38:49'),
(3925, '', 0.00, 6, '2019-11-12 06:38:49'),
(3926, '', 0.00, 6, '2019-11-12 06:38:49'),
(3927, '', 0.00, 6, '2019-11-12 06:38:49'),
(3928, '', 0.00, 6, '2019-11-12 06:38:49'),
(3929, '', 0.00, 6, '2019-11-12 06:38:49'),
(3930, '', 0.00, 6, '2019-11-12 06:38:49'),
(3931, '', 0.00, 6, '2019-11-12 06:38:49'),
(3932, '', 0.00, 6, '2019-11-12 06:38:49'),
(3933, '', 0.00, 6, '2019-11-12 06:38:49'),
(3934, '', 0.00, 6, '2019-11-12 06:38:49'),
(3935, '', 0.00, 6, '2019-11-12 06:38:49'),
(3936, '', 0.00, 6, '2019-11-12 06:38:49'),
(3937, '', 0.00, 6, '2019-11-12 06:38:49'),
(3938, '', 0.00, 6, '2019-11-12 06:38:49'),
(3939, '', 0.00, 6, '2019-11-12 06:38:49'),
(3940, '', 0.00, 6, '2019-11-12 06:38:49'),
(3941, '', 0.00, 6, '2019-11-12 06:38:49'),
(3942, '', 0.00, 6, '2019-11-12 06:38:49'),
(3943, '', 0.00, 6, '2019-11-12 06:38:49'),
(3944, '', 0.00, 6, '2019-11-12 06:38:49'),
(3945, '', 0.00, 6, '2019-11-12 06:38:49'),
(3946, '', 0.00, 6, '2019-11-12 06:38:49'),
(3947, '', 0.00, 6, '2019-11-12 06:38:50'),
(3948, '', 0.00, 6, '2019-11-12 06:38:50'),
(3949, '', 0.00, 6, '2019-11-12 06:38:50'),
(3950, '', 0.00, 6, '2019-11-12 06:38:50'),
(3951, '', 0.00, 6, '2019-11-12 06:38:50'),
(3952, '', 0.00, 6, '2019-11-12 06:38:50'),
(3953, '', 0.00, 6, '2019-11-12 06:38:50'),
(3954, '', 0.00, 6, '2019-11-12 06:38:50'),
(3955, '', 0.00, 6, '2019-11-12 06:38:51'),
(3956, '', 0.00, 6, '2019-11-12 06:38:51'),
(3957, '', 0.00, 6, '2019-11-12 06:38:51'),
(3958, '', 0.00, 6, '2019-11-12 06:38:51'),
(3959, '', 0.00, 6, '2019-11-12 06:38:51'),
(3960, '', 0.00, 6, '2019-11-12 06:38:51'),
(3961, '', 0.00, 6, '2019-11-12 06:38:51'),
(3962, '', 0.00, 6, '2019-11-12 06:38:51'),
(3963, '', 0.00, 6, '2019-11-12 06:38:51'),
(3964, '', 0.00, 6, '2019-11-12 06:38:51'),
(3965, '', 0.00, 6, '2019-11-12 06:38:52'),
(3966, '', 0.00, 6, '2019-11-12 06:38:52'),
(3967, '', 0.00, 6, '2019-11-12 06:38:52'),
(3968, '', 0.00, 6, '2019-11-12 06:38:52'),
(3969, '', 0.00, 6, '2019-11-12 06:38:52'),
(3970, '', 0.00, 6, '2019-11-12 06:38:52'),
(3971, '', 0.00, 6, '2019-11-12 06:38:52'),
(3972, '', 0.00, 6, '2019-11-12 06:38:52'),
(3973, '', 0.00, 6, '2019-11-12 06:38:52'),
(3974, '', 0.00, 6, '2019-11-12 06:38:53'),
(3975, '', 0.00, 6, '2019-11-12 06:38:53'),
(3976, '', 0.00, 6, '2019-11-12 06:38:53'),
(3977, '', 0.00, 6, '2019-11-12 06:38:53'),
(3978, '', 0.00, 6, '2019-11-12 06:38:53'),
(3979, '', 0.00, 6, '2019-11-12 06:38:53'),
(3980, '', 0.00, 6, '2019-11-12 06:38:53'),
(3981, '', 0.00, 6, '2019-11-12 06:38:53'),
(3982, '', 0.00, 6, '2019-11-12 06:38:53'),
(3983, '', 0.00, 6, '2019-11-12 06:38:53'),
(3984, '', 0.00, 6, '2019-11-12 06:38:53'),
(3985, '', 0.00, 6, '2019-11-12 06:38:54'),
(3986, '', 0.00, 6, '2019-11-12 06:38:54'),
(3987, '', 0.00, 6, '2019-11-12 06:38:54'),
(3988, '', 0.00, 6, '2019-11-12 06:38:54'),
(3989, '', 0.00, 6, '2019-11-12 06:38:54'),
(3990, '', 0.00, 6, '2019-11-12 06:38:54'),
(3991, '', 0.00, 6, '2019-11-12 06:38:54'),
(3992, '', 0.00, 6, '2019-11-12 06:38:54'),
(3993, '', 0.00, 6, '2019-11-12 06:38:54'),
(3994, '', 0.00, 6, '2019-11-12 06:38:54'),
(3995, '', 0.00, 6, '2019-11-12 06:38:54'),
(3996, '', 0.00, 6, '2019-11-12 06:38:54'),
(3997, '', 0.00, 6, '2019-11-12 06:38:54'),
(3998, '', 0.00, 6, '2019-11-12 06:38:54'),
(3999, '', 0.00, 6, '2019-11-12 06:38:54'),
(4000, '', 0.00, 6, '2019-11-12 06:38:54'),
(4001, '', 0.00, 6, '2019-11-12 06:38:54'),
(4002, '', 0.00, 6, '2019-11-12 06:38:54'),
(4003, '', 0.00, 6, '2019-11-12 06:38:54'),
(4004, '', 0.00, 6, '2019-11-12 06:38:54'),
(4005, '', 0.00, 6, '2019-11-12 06:38:55'),
(4006, '', 0.00, 6, '2019-11-12 06:38:55'),
(4007, '', 0.00, 6, '2019-11-12 06:38:55'),
(4008, '', 0.00, 6, '2019-11-12 06:38:55'),
(4009, '', 0.00, 6, '2019-11-12 06:38:55'),
(4010, '', 0.00, 6, '2019-11-12 06:38:55'),
(4011, '', 0.00, 6, '2019-11-12 06:38:55'),
(4012, '', 0.00, 6, '2019-11-12 06:38:55'),
(4013, '', 0.00, 6, '2019-11-12 06:38:55'),
(4014, '', 0.00, 6, '2019-11-12 06:38:55'),
(4015, '', 0.00, 6, '2019-11-12 06:38:55'),
(4016, '', 0.00, 6, '2019-11-12 06:38:55'),
(4017, '', 0.00, 6, '2019-11-12 06:38:55'),
(4018, '', 0.00, 6, '2019-11-12 06:38:55'),
(4019, '', 0.00, 6, '2019-11-12 06:38:56'),
(4020, '', 0.00, 6, '2019-11-12 06:38:56'),
(4021, '', 0.00, 6, '2019-11-12 06:38:56'),
(4022, '', 0.00, 6, '2019-11-12 06:38:56'),
(4023, '', 0.00, 6, '2019-11-12 06:38:57'),
(4024, '', 0.00, 6, '2019-11-12 06:38:57'),
(4025, '', 0.00, 6, '2019-11-12 06:38:57'),
(4026, '', 0.00, 6, '2019-11-12 06:38:57'),
(4027, '', 0.00, 6, '2019-11-12 06:38:57'),
(4028, '\0\0\0', 0.00, 6, '2019-11-12 06:38:57'),
(4029, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:57'),
(4030, '', 0.00, 6, '2019-11-12 06:38:57'),
(4031, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:57'),
(4032, '', 0.00, 6, '2019-11-12 06:38:57'),
(4033, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:57'),
(4034, '', 0.00, 6, '2019-11-12 06:38:57'),
(4035, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:57'),
(4036, '', 0.00, 6, '2019-11-12 06:38:57'),
(4037, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:58'),
(4038, '', 0.00, 6, '2019-11-12 06:38:58'),
(4039, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:58'),
(4040, '', 0.00, 6, '2019-11-12 06:38:58'),
(4041, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:58'),
(4042, '', 0.00, 6, '2019-11-12 06:38:58'),
(4043, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:58'),
(4044, '', 0.00, 6, '2019-11-12 06:38:58'),
(4045, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:58'),
(4046, '', 0.00, 6, '2019-11-12 06:38:58'),
(4047, '\0	\0\0\0	\0\0~', 0.00, 6, '2019-11-12 06:38:58'),
(4048, '', 0.00, 6, '2019-11-12 06:38:59'),
(4049, '\0', 0.00, 6, '2019-11-12 06:38:59'),
(4050, '\0\0\0', 0.00, 6, '2019-11-12 06:38:59'),
(4051, '\0\0~', 0.00, 6, '2019-11-12 06:38:59'),
(4052, '\0', 0.00, 6, '2019-11-12 06:38:59'),
(4053, '', 0.00, 6, '2019-11-12 06:38:59'),
(4054, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:59'),
(4055, '', 0.00, 6, '2019-11-12 06:38:59'),
(4056, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:38:59'),
(4057, '', 0.00, 6, '2019-11-12 06:38:59'),
(4058, '\0\r\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:38:59'),
(4059, '', 0.00, 6, '2019-11-12 06:39:00'),
(4060, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:00'),
(4061, '', 0.00, 6, '2019-11-12 06:39:00'),
(4062, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:00'),
(4063, '', 0.00, 6, '2019-11-12 06:39:00'),
(4064, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:00'),
(4065, '', 0.00, 6, '2019-11-12 06:39:00'),
(4066, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:00'),
(4067, '', 0.00, 6, '2019-11-12 06:39:00'),
(4068, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:00'),
(4069, '\0\0\0', 0.00, 6, '2019-11-12 06:39:00'),
(4070, '', 0.00, 6, '2019-11-12 06:39:01'),
(4071, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:01'),
(4072, '', 0.00, 6, '2019-11-12 06:39:01'),
(4073, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:01'),
(4074, '', 0.00, 6, '2019-11-12 06:39:01'),
(4075, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:01'),
(4076, '', 0.00, 6, '2019-11-12 06:39:01'),
(4077, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:01'),
(4078, '', 0.00, 6, '2019-11-12 06:39:01'),
(4079, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:01'),
(4080, '', 0.00, 6, '2019-11-12 06:39:01'),
(4081, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:01'),
(4082, '', 0.00, 6, '2019-11-12 06:39:01'),
(4083, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:01'),
(4084, '', 0.00, 6, '2019-11-12 06:39:01'),
(4085, '\0\Z\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:39:01'),
(4086, '', 0.00, 6, '2019-11-12 06:39:01'),
(4087, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:01'),
(4088, '', 0.00, 6, '2019-11-12 06:39:01'),
(4089, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:01'),
(4090, '', 0.00, 6, '2019-11-12 06:39:01'),
(4091, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:01'),
(4092, '', 0.00, 6, '2019-11-12 06:39:01'),
(4093, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:02'),
(4094, '\0\0\0S', 0.00, 6, '2019-11-12 06:39:02'),
(4095, '', 0.00, 6, '2019-11-12 06:39:02'),
(4096, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:02'),
(4097, '\0\0\0S', 0.00, 6, '2019-11-12 06:39:02'),
(4098, '\0\0\0 \0\0\0', 0.00, 6, '2019-11-12 06:39:02'),
(4099, '\0 \0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:39:02'),
(4100, '', 0.00, 6, '2019-11-12 06:39:02'),
(4101, '\0!\0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:39:03'),
(4102, '', 0.00, 6, '2019-11-12 06:39:03'),
(4103, '\0&quot;\0\0\0&quot;\0\0~', 0.00, 6, '2019-11-12 06:39:03'),
(4104, '', 0.00, 6, '2019-11-12 06:39:03'),
(4105, '\0#\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:39:03'),
(4106, '', 0.00, 6, '2019-11-12 06:39:03'),
(4107, '\0$\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:39:03'),
(4108, '', 0.00, 6, '2019-11-12 06:39:03'),
(4109, '\0%\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:39:04'),
(4110, '', 0.00, 6, '2019-11-12 06:39:04'),
(4111, '\0&amp;\0\0\0&amp;\0\0~', 0.00, 6, '2019-11-12 06:39:04'),
(4112, '', 0.00, 6, '2019-11-12 06:39:04'),
(4113, '\0\'\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:39:04'),
(4114, '', 0.00, 6, '2019-11-12 06:39:04'),
(4115, '\0(\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:39:04'),
(4116, '', 0.00, 6, '2019-11-12 06:39:04'),
(4117, '\0)\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:39:04'),
(4118, '', 0.00, 6, '2019-11-12 06:39:04'),
(4119, '\0*\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:39:04'),
(4120, '', 0.00, 6, '2019-11-12 06:39:04'),
(4121, '\0+\0\0\0+\0\0~', 0.00, 6, '2019-11-12 06:39:04'),
(4122, '', 0.00, 6, '2019-11-12 06:39:04'),
(4123, '\0', 0.00, 6, '2019-11-12 06:39:04'),
(4124, '\0', 0.00, 6, '2019-11-12 06:39:04'),
(4125, '\0-\0\0\0-\0\0~', 0.00, 6, '2019-11-12 06:39:05'),
(4126, '', 0.00, 6, '2019-11-12 06:39:05'),
(4127, '\0.\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:39:05'),
(4128, '', 0.00, 6, '2019-11-12 06:39:06'),
(4129, '\0/\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:39:06'),
(4130, '', 0.00, 6, '2019-11-12 06:39:06'),
(4131, '\00\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:39:06'),
(4132, '', 0.00, 6, '2019-11-12 06:39:06'),
(4133, '\01\0\0\01\0\0~', 0.00, 6, '2019-11-12 06:39:06'),
(4134, '', 0.00, 6, '2019-11-12 06:39:07'),
(4135, '\02\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:39:07'),
(4136, '', 0.00, 6, '2019-11-12 06:39:07'),
(4137, '\03\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:39:07'),
(4138, '', 0.00, 6, '2019-11-12 06:39:07'),
(4139, '\04\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:39:07'),
(4140, '', 0.00, 6, '2019-11-12 06:39:07'),
(4141, '\05\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:39:07'),
(4142, '', 0.00, 6, '2019-11-12 06:39:07'),
(4143, '\06\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:39:07'),
(4144, '', 0.00, 6, '2019-11-12 06:39:08'),
(4145, '\07\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:39:08'),
(4146, '', 0.00, 6, '2019-11-12 06:39:08'),
(4147, '\08\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:39:08'),
(4148, '', 0.00, 6, '2019-11-12 06:39:08'),
(4149, '\09\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:39:08'),
(4150, '', 0.00, 6, '2019-11-12 06:39:08'),
(4151, '', 0.00, 6, '2019-11-12 06:39:08'),
(4152, '\0;\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:39:08'),
(4153, '', 0.00, 6, '2019-11-12 06:39:08'),
(4154, '\0&lt;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:39:08'),
(4155, '', 0.00, 6, '2019-11-12 06:39:08'),
(4156, '\0=\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:39:08'),
(4157, '\0=\0\0', 0.00, 6, '2019-11-12 06:39:08'),
(4158, '', 0.00, 6, '2019-11-12 06:39:08'),
(4159, '\0&gt;\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:39:08'),
(4160, '', 0.00, 6, '2019-11-12 06:39:09'),
(4161, '\0?\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:39:09'),
(4162, '\0?\0\0\0\0\0\0@\0\0\0', 0.00, 6, '2019-11-12 06:39:09'),
(4163, '\0@\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:39:09'),
(4164, '', 0.00, 6, '2019-11-12 06:39:09'),
(4165, '\0A\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:39:09'),
(4166, '', 0.00, 6, '2019-11-12 06:39:09'),
(4167, '\0B\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:39:09'),
(4168, '', 0.00, 6, '2019-11-12 06:39:09'),
(4169, '', 0.00, 6, '2019-11-12 06:39:09'),
(4170, '\0D\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:39:09'),
(4171, '', 0.00, 6, '2019-11-12 06:39:09'),
(4172, '\0E\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:39:09'),
(4173, '', 0.00, 6, '2019-11-12 06:39:09'),
(4174, '\0F\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:39:09'),
(4175, '', 0.00, 6, '2019-11-12 06:39:09'),
(4176, '\0G\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:39:09'),
(4177, '', 0.00, 6, '2019-11-12 06:39:10'),
(4178, '\0H\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:39:10'),
(4179, '', 0.00, 6, '2019-11-12 06:39:10'),
(4180, '\0I\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:39:10'),
(4181, '', 0.00, 6, '2019-11-12 06:39:10'),
(4182, '\0J\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:39:10'),
(4183, '', 0.00, 6, '2019-11-12 06:39:10'),
(4184, '\0K\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:39:10'),
(4185, '', 0.00, 6, '2019-11-12 06:39:10'),
(4186, '\0L\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:39:10'),
(4187, '', 0.00, 6, '2019-11-12 06:39:10'),
(4188, '\0M\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:39:10'),
(4189, '', 0.00, 6, '2019-11-12 06:39:10'),
(4190, '\0N\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:39:10'),
(4191, '', 0.00, 6, '2019-11-12 06:39:10'),
(4192, '\0O\0\0\0O\0\0~', 0.00, 6, '2019-11-12 06:39:10'),
(4193, '\0O\0\0', 0.00, 6, '2019-11-12 06:39:10'),
(4194, '', 0.00, 6, '2019-11-12 06:39:10'),
(4195, '\0P\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:39:11'),
(4196, '', 0.00, 6, '2019-11-12 06:39:11'),
(4197, '\0Q\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:39:11'),
(4198, '', 0.00, 6, '2019-11-12 06:39:11'),
(4199, '\0R\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:39:11'),
(4200, '', 0.00, 6, '2019-11-12 06:39:11'),
(4201, '\0S\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:39:11'),
(4202, '', 0.00, 6, '2019-11-12 06:39:11'),
(4203, '\0T\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:39:11'),
(4204, '', 0.00, 6, '2019-11-12 06:39:11'),
(4205, '', 0.00, 6, '2019-11-12 06:39:11'),
(4206, '\0V\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:39:11'),
(4207, '', 0.00, 6, '2019-11-12 06:39:11'),
(4208, '\0W\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:39:11'),
(4209, '', 0.00, 6, '2019-11-12 06:39:11'),
(4210, '\0X\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:39:12'),
(4211, '', 0.00, 6, '2019-11-12 06:39:12'),
(4212, '\0Y\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:39:12'),
(4213, '', 0.00, 6, '2019-11-12 06:39:12'),
(4214, '', 0.00, 6, '2019-11-12 06:39:12'),
(4215, '\0[\0\0\0[\0\0~', 0.00, 6, '2019-11-12 06:39:12'),
(4216, '', 0.00, 6, '2019-11-12 06:39:12'),
(4217, '\0\\\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:39:12'),
(4218, '', 0.00, 6, '2019-11-12 06:39:12'),
(4219, '\0]\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:39:12'),
(4220, '', 0.00, 6, '2019-11-12 06:39:12'),
(4221, '\0^\0\0\0^\0\0~', 0.00, 6, '2019-11-12 06:39:12'),
(4222, '', 0.00, 6, '2019-11-12 06:39:12'),
(4223, '', 0.00, 6, '2019-11-12 06:39:12'),
(4224, '\0`\0\0\0`\0\0~', 0.00, 6, '2019-11-12 06:39:12'),
(4225, '', 0.00, 6, '2019-11-12 06:39:12'),
(4226, '\0a\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:39:12'),
(4227, '', 0.00, 6, '2019-11-12 06:39:13'),
(4228, '\0b\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:39:13'),
(4229, '\0b\0\0', 0.00, 6, '2019-11-12 06:39:13'),
(4230, '', 0.00, 6, '2019-11-12 06:39:13'),
(4231, '\0c\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:39:13');
INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(4232, '', 0.00, 6, '2019-11-12 06:39:13'),
(4233, '\0d\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:39:13'),
(4234, '', 0.00, 6, '2019-11-12 06:39:13'),
(4235, '\0e\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:39:13'),
(4236, '', 0.00, 6, '2019-11-12 06:39:14'),
(4237, '\0f\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:39:14'),
(4238, '', 0.00, 6, '2019-11-12 06:39:14'),
(4239, '\0g\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:39:14'),
(4240, '', 0.00, 6, '2019-11-12 06:39:14'),
(4241, '\0h\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:39:14'),
(4242, '', 0.00, 6, '2019-11-12 06:39:14'),
(4243, '\0i\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:39:14'),
(4244, '', 0.00, 6, '2019-11-12 06:39:14'),
(4245, '\0j\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:39:14'),
(4246, '', 0.00, 6, '2019-11-12 06:39:15'),
(4247, '\0k\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:39:15'),
(4248, '\0k\0\0', 0.00, 6, '2019-11-12 06:39:15'),
(4249, '', 0.00, 6, '2019-11-12 06:39:15'),
(4250, '\0l\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:39:15'),
(4251, '', 0.00, 6, '2019-11-12 06:39:15'),
(4252, '\0m\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:39:15'),
(4253, '', 0.00, 6, '2019-11-12 06:39:15'),
(4254, '\0n\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:39:15'),
(4255, '', 0.00, 6, '2019-11-12 06:39:15'),
(4256, '\0o\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:39:15'),
(4257, '', 0.00, 6, '2019-11-12 06:39:15'),
(4258, '\0p\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:39:15'),
(4259, '', 0.00, 6, '2019-11-12 06:39:15'),
(4260, '', 0.00, 6, '2019-11-12 06:39:15'),
(4261, '\0r\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:39:15'),
(4262, '', 0.00, 6, '2019-11-12 06:39:15'),
(4263, '\0s\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:39:15'),
(4264, '', 0.00, 6, '2019-11-12 06:39:15'),
(4265, '\0t\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:39:15'),
(4266, '', 0.00, 6, '2019-11-12 06:39:16'),
(4267, '\0u\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:39:16'),
(4268, '', 0.00, 6, '2019-11-12 06:39:16'),
(4269, '\0v\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:39:16'),
(4270, '', 0.00, 6, '2019-11-12 06:39:16'),
(4271, '\0w\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:39:16'),
(4272, '', 0.00, 6, '2019-11-12 06:39:16'),
(4273, '\0x\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:39:16'),
(4274, '', 0.00, 6, '2019-11-12 06:39:16'),
(4275, '\0y\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:39:16'),
(4276, '', 0.00, 6, '2019-11-12 06:39:16'),
(4277, '\0z\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:39:16'),
(4278, '', 0.00, 6, '2019-11-12 06:39:16'),
(4279, '\0{\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:39:17'),
(4280, '', 0.00, 6, '2019-11-12 06:39:17'),
(4281, '\0|\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:39:17'),
(4282, '', 0.00, 6, '2019-11-12 06:39:17'),
(4283, '\0}\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:39:17'),
(4284, '\0}\0\0S', 0.00, 6, '2019-11-12 06:39:17'),
(4285, '', 0.00, 6, '2019-11-12 06:39:17'),
(4286, '\0~\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:39:17'),
(4287, '', 0.00, 6, '2019-11-12 06:39:17'),
(4288, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:18'),
(4289, '', 0.00, 6, '2019-11-12 06:39:18'),
(4290, '', 0.00, 6, '2019-11-12 06:39:18'),
(4291, '', 0.00, 6, '2019-11-12 06:39:18'),
(4292, '', 0.00, 6, '2019-11-12 06:39:18'),
(4293, '', 0.00, 6, '2019-11-12 06:39:18'),
(4294, '', 0.00, 6, '2019-11-12 06:39:19'),
(4295, '', 0.00, 6, '2019-11-12 06:39:19'),
(4296, '', 0.00, 6, '2019-11-12 06:39:19'),
(4297, '', 0.00, 6, '2019-11-12 06:39:19'),
(4298, '', 0.00, 6, '2019-11-12 06:39:19'),
(4299, '', 0.00, 6, '2019-11-12 06:39:19'),
(4300, '', 0.00, 6, '2019-11-12 06:39:19'),
(4301, '', 0.00, 6, '2019-11-12 06:39:19'),
(4302, '', 0.00, 6, '2019-11-12 06:39:19'),
(4303, '', 0.00, 6, '2019-11-12 06:39:19'),
(4304, '', 0.00, 6, '2019-11-12 06:39:19'),
(4305, '', 0.00, 6, '2019-11-12 06:39:19'),
(4306, '', 0.00, 6, '2019-11-12 06:39:19'),
(4307, '', 0.00, 6, '2019-11-12 06:39:19'),
(4308, '', 0.00, 6, '2019-11-12 06:39:19'),
(4309, '', 0.00, 6, '2019-11-12 06:39:19'),
(4310, '', 0.00, 6, '2019-11-12 06:39:19'),
(4311, '', 0.00, 6, '2019-11-12 06:39:19'),
(4312, '', 0.00, 6, '2019-11-12 06:39:19'),
(4313, '', 0.00, 6, '2019-11-12 06:39:19'),
(4314, '', 0.00, 6, '2019-11-12 06:39:19'),
(4315, '', 0.00, 6, '2019-11-12 06:39:19'),
(4316, '', 0.00, 6, '2019-11-12 06:39:19'),
(4317, '', 0.00, 6, '2019-11-12 06:39:19'),
(4318, '', 0.00, 6, '2019-11-12 06:39:20'),
(4319, '', 0.00, 6, '2019-11-12 06:39:20'),
(4320, '', 0.00, 6, '2019-11-12 06:39:20'),
(4321, '', 0.00, 6, '2019-11-12 06:39:20'),
(4322, '', 0.00, 6, '2019-11-12 06:39:20'),
(4323, '', 0.00, 6, '2019-11-12 06:39:20'),
(4324, '', 0.00, 6, '2019-11-12 06:39:20'),
(4325, '', 0.00, 6, '2019-11-12 06:39:20'),
(4326, '', 0.00, 6, '2019-11-12 06:39:20'),
(4327, '', 0.00, 6, '2019-11-12 06:39:20'),
(4328, '', 0.00, 6, '2019-11-12 06:39:20'),
(4329, '', 0.00, 6, '2019-11-12 06:39:20'),
(4330, '', 0.00, 6, '2019-11-12 06:39:20'),
(4331, '', 0.00, 6, '2019-11-12 06:39:20'),
(4332, '', 0.00, 6, '2019-11-12 06:39:20'),
(4333, '', 0.00, 6, '2019-11-12 06:39:21'),
(4334, '', 0.00, 6, '2019-11-12 06:39:21'),
(4335, '', 0.00, 6, '2019-11-12 06:39:21'),
(4336, '', 0.00, 6, '2019-11-12 06:39:21'),
(4337, '', 0.00, 6, '2019-11-12 06:39:21'),
(4338, '', 0.00, 6, '2019-11-12 06:39:21'),
(4339, '', 0.00, 6, '2019-11-12 06:39:21'),
(4340, '', 0.00, 6, '2019-11-12 06:39:21'),
(4341, '', 0.00, 6, '2019-11-12 06:39:21'),
(4342, '', 0.00, 6, '2019-11-12 06:39:21'),
(4343, '', 0.00, 6, '2019-11-12 06:39:21'),
(4344, '', 0.00, 6, '2019-11-12 06:39:21'),
(4345, '', 0.00, 6, '2019-11-12 06:39:21'),
(4346, '', 0.00, 6, '2019-11-12 06:39:21'),
(4347, '', 0.00, 6, '2019-11-12 06:39:21'),
(4348, '', 0.00, 6, '2019-11-12 06:39:21'),
(4349, '', 0.00, 6, '2019-11-12 06:39:22'),
(4350, '', 0.00, 6, '2019-11-12 06:39:22'),
(4351, '', 0.00, 6, '2019-11-12 06:39:22'),
(4352, '', 0.00, 6, '2019-11-12 06:39:22'),
(4353, '', 0.00, 6, '2019-11-12 06:39:22'),
(4354, '', 0.00, 6, '2019-11-12 06:39:22'),
(4355, '', 0.00, 6, '2019-11-12 06:39:22'),
(4356, '', 0.00, 6, '2019-11-12 06:39:22'),
(4357, '', 0.00, 6, '2019-11-12 06:39:22'),
(4358, '', 0.00, 6, '2019-11-12 06:39:22'),
(4359, '', 0.00, 6, '2019-11-12 06:39:22'),
(4360, '', 0.00, 6, '2019-11-12 06:39:22'),
(4361, '', 0.00, 6, '2019-11-12 06:39:22'),
(4362, '', 0.00, 6, '2019-11-12 06:39:22'),
(4363, '', 0.00, 6, '2019-11-12 06:39:22'),
(4364, '', 0.00, 6, '2019-11-12 06:39:22'),
(4365, '', 0.00, 6, '2019-11-12 06:39:22'),
(4366, '', 0.00, 6, '2019-11-12 06:39:22'),
(4367, '', 0.00, 6, '2019-11-12 06:39:23'),
(4368, '', 0.00, 6, '2019-11-12 06:39:23'),
(4369, '', 0.00, 6, '2019-11-12 06:39:23'),
(4370, '', 0.00, 6, '2019-11-12 06:39:23'),
(4371, '', 0.00, 6, '2019-11-12 06:39:23'),
(4372, '', 0.00, 6, '2019-11-12 06:39:23'),
(4373, '', 0.00, 6, '2019-11-12 06:39:23'),
(4374, '', 0.00, 6, '2019-11-12 06:39:23'),
(4375, '', 0.00, 6, '2019-11-12 06:39:23'),
(4376, '', 0.00, 6, '2019-11-12 06:39:23'),
(4377, '', 0.00, 6, '2019-11-12 06:39:23'),
(4378, '', 0.00, 6, '2019-11-12 06:39:23'),
(4379, '', 0.00, 6, '2019-11-12 06:39:23'),
(4380, '', 0.00, 6, '2019-11-12 06:39:23'),
(4381, '', 0.00, 6, '2019-11-12 06:39:23'),
(4382, '', 0.00, 6, '2019-11-12 06:39:24'),
(4383, '', 0.00, 6, '2019-11-12 06:39:24'),
(4384, '', 0.00, 6, '2019-11-12 06:39:24'),
(4385, '', 0.00, 6, '2019-11-12 06:39:24'),
(4386, '', 0.00, 6, '2019-11-12 06:39:24'),
(4387, '', 0.00, 6, '2019-11-12 06:39:24'),
(4388, '', 0.00, 6, '2019-11-12 06:39:24'),
(4389, '', 0.00, 6, '2019-11-12 06:39:24'),
(4390, '', 0.00, 6, '2019-11-12 06:39:24'),
(4391, '', 0.00, 6, '2019-11-12 06:39:24'),
(4392, '', 0.00, 6, '2019-11-12 06:39:24'),
(4393, '', 0.00, 6, '2019-11-12 06:39:24'),
(4394, '', 0.00, 6, '2019-11-12 06:39:24'),
(4395, '', 0.00, 6, '2019-11-12 06:39:24'),
(4396, '', 0.00, 6, '2019-11-12 06:39:24'),
(4397, '', 0.00, 6, '2019-11-12 06:39:24'),
(4398, '', 0.00, 6, '2019-11-12 06:39:24'),
(4399, '', 0.00, 6, '2019-11-12 06:39:24'),
(4400, '', 0.00, 6, '2019-11-12 06:39:24'),
(4401, '', 0.00, 6, '2019-11-12 06:39:24'),
(4402, '', 0.00, 6, '2019-11-12 06:39:24'),
(4403, '', 0.00, 6, '2019-11-12 06:39:25'),
(4404, '', 0.00, 6, '2019-11-12 06:39:25'),
(4405, '', 0.00, 6, '2019-11-12 06:39:25'),
(4406, '', 0.00, 6, '2019-11-12 06:39:25'),
(4407, '', 0.00, 6, '2019-11-12 06:39:25'),
(4408, '', 0.00, 6, '2019-11-12 06:39:25'),
(4409, '', 0.00, 6, '2019-11-12 06:39:25'),
(4410, '', 0.00, 6, '2019-11-12 06:39:25'),
(4411, '', 0.00, 6, '2019-11-12 06:39:25'),
(4412, '', 0.00, 6, '2019-11-12 06:39:25'),
(4413, '', 0.00, 6, '2019-11-12 06:39:25'),
(4414, '', 0.00, 6, '2019-11-12 06:39:25'),
(4415, '', 0.00, 6, '2019-11-12 06:39:25'),
(4416, '', 0.00, 6, '2019-11-12 06:39:26'),
(4417, '', 0.00, 6, '2019-11-12 06:39:26'),
(4418, '', 0.00, 6, '2019-11-12 06:39:26'),
(4419, '', 0.00, 6, '2019-11-12 06:39:26'),
(4420, '', 0.00, 6, '2019-11-12 06:39:26'),
(4421, '', 0.00, 6, '2019-11-12 06:39:26'),
(4422, '', 0.00, 6, '2019-11-12 06:39:26'),
(4423, '', 0.00, 6, '2019-11-12 06:39:26'),
(4424, '', 0.00, 6, '2019-11-12 06:39:26'),
(4425, '', 0.00, 6, '2019-11-12 06:39:26'),
(4426, '', 0.00, 6, '2019-11-12 06:39:26'),
(4427, '', 0.00, 6, '2019-11-12 06:39:26'),
(4428, '', 0.00, 6, '2019-11-12 06:39:26'),
(4429, '', 0.00, 6, '2019-11-12 06:39:26'),
(4430, '', 0.00, 6, '2019-11-12 06:39:27'),
(4431, '', 0.00, 6, '2019-11-12 06:39:27'),
(4432, '', 0.00, 6, '2019-11-12 06:39:27'),
(4433, '', 0.00, 6, '2019-11-12 06:39:27'),
(4434, '', 0.00, 6, '2019-11-12 06:39:27'),
(4435, '', 0.00, 6, '2019-11-12 06:39:27'),
(4436, '', 0.00, 6, '2019-11-12 06:39:27'),
(4437, '', 0.00, 6, '2019-11-12 06:39:27'),
(4438, '', 0.00, 6, '2019-11-12 06:39:27'),
(4439, '', 0.00, 6, '2019-11-12 06:39:27'),
(4440, '', 0.00, 6, '2019-11-12 06:39:27'),
(4441, '', 0.00, 6, '2019-11-12 06:39:27'),
(4442, '', 0.00, 6, '2019-11-12 06:39:28'),
(4443, '', 0.00, 6, '2019-11-12 06:39:28'),
(4444, '', 0.00, 6, '2019-11-12 06:39:28'),
(4445, '', 0.00, 6, '2019-11-12 06:39:28'),
(4446, '', 0.00, 6, '2019-11-12 06:39:28'),
(4447, '', 0.00, 6, '2019-11-12 06:39:28'),
(4448, '', 0.00, 6, '2019-11-12 06:39:28'),
(4449, '', 0.00, 6, '2019-11-12 06:39:29'),
(4450, '', 0.00, 6, '2019-11-12 06:39:29'),
(4451, '', 0.00, 6, '2019-11-12 06:39:29'),
(4452, '', 0.00, 6, '2019-11-12 06:39:29'),
(4453, '', 0.00, 6, '2019-11-12 06:39:29'),
(4454, '', 0.00, 6, '2019-11-12 06:39:29'),
(4455, '', 0.00, 6, '2019-11-12 06:39:29'),
(4456, '', 0.00, 6, '2019-11-12 06:39:30'),
(4457, '', 0.00, 6, '2019-11-12 06:39:30'),
(4458, '', 0.00, 6, '2019-11-12 06:39:30'),
(4459, '', 0.00, 6, '2019-11-12 06:39:30'),
(4460, '', 0.00, 6, '2019-11-12 06:39:30'),
(4461, '', 0.00, 6, '2019-11-12 06:39:30'),
(4462, '', 0.00, 6, '2019-11-12 06:39:30'),
(4463, '', 0.00, 6, '2019-11-12 06:39:30'),
(4464, '', 0.00, 6, '2019-11-12 06:39:30'),
(4465, '', 0.00, 6, '2019-11-12 06:39:30'),
(4466, '', 0.00, 6, '2019-11-12 06:39:31'),
(4467, '', 0.00, 6, '2019-11-12 06:39:31'),
(4468, '', 0.00, 6, '2019-11-12 06:39:31'),
(4469, '', 0.00, 6, '2019-11-12 06:39:31'),
(4470, '', 0.00, 6, '2019-11-12 06:39:31'),
(4471, '', 0.00, 6, '2019-11-12 06:39:31'),
(4472, '', 0.00, 6, '2019-11-12 06:39:31'),
(4473, '', 0.00, 6, '2019-11-12 06:39:31'),
(4474, '', 0.00, 6, '2019-11-12 06:39:31'),
(4475, '', 0.00, 6, '2019-11-12 06:39:31'),
(4476, '', 0.00, 6, '2019-11-12 06:39:31'),
(4477, '', 0.00, 6, '2019-11-12 06:39:31'),
(4478, '', 0.00, 6, '2019-11-12 06:39:32'),
(4479, '', 0.00, 6, '2019-11-12 06:39:32'),
(4480, '', 0.00, 6, '2019-11-12 06:39:32'),
(4481, '', 0.00, 6, '2019-11-12 06:39:32'),
(4482, '', 0.00, 6, '2019-11-12 06:39:32'),
(4483, '', 0.00, 6, '2019-11-12 06:39:32'),
(4484, '', 0.00, 6, '2019-11-12 06:39:32'),
(4485, '', 0.00, 6, '2019-11-12 06:39:32'),
(4486, '', 0.00, 6, '2019-11-12 06:39:32'),
(4487, '', 0.00, 6, '2019-11-12 06:39:32'),
(4488, '', 0.00, 6, '2019-11-12 06:39:32'),
(4489, '', 0.00, 6, '2019-11-12 06:39:32'),
(4490, '', 0.00, 6, '2019-11-12 06:39:32'),
(4491, '', 0.00, 6, '2019-11-12 06:39:32'),
(4492, '', 0.00, 6, '2019-11-12 06:39:32'),
(4493, '', 0.00, 6, '2019-11-12 06:39:32'),
(4494, '', 0.00, 6, '2019-11-12 06:39:32'),
(4495, '', 0.00, 6, '2019-11-12 06:39:32'),
(4496, '', 0.00, 6, '2019-11-12 06:39:32'),
(4497, '', 0.00, 6, '2019-11-12 06:39:32'),
(4498, '', 0.00, 6, '2019-11-12 06:39:33'),
(4499, '', 0.00, 6, '2019-11-12 06:39:33'),
(4500, '', 0.00, 6, '2019-11-12 06:39:33'),
(4501, '', 0.00, 6, '2019-11-12 06:39:33'),
(4502, '', 0.00, 6, '2019-11-12 06:39:33'),
(4503, '', 0.00, 6, '2019-11-12 06:39:33'),
(4504, '', 0.00, 6, '2019-11-12 06:39:33'),
(4505, '', 0.00, 6, '2019-11-12 06:39:33'),
(4506, '', 0.00, 6, '2019-11-12 06:39:33'),
(4507, '', 0.00, 6, '2019-11-12 06:39:33'),
(4508, '', 0.00, 6, '2019-11-12 06:39:33'),
(4509, '', 0.00, 6, '2019-11-12 06:39:33'),
(4510, '', 0.00, 6, '2019-11-12 06:39:33'),
(4511, '', 0.00, 6, '2019-11-12 06:39:33'),
(4512, '', 0.00, 6, '2019-11-12 06:39:33'),
(4513, '', 0.00, 6, '2019-11-12 06:39:33'),
(4514, '', 0.00, 6, '2019-11-12 06:39:33'),
(4515, '', 0.00, 6, '2019-11-12 06:39:33'),
(4516, '', 0.00, 6, '2019-11-12 06:39:33'),
(4517, '', 0.00, 6, '2019-11-12 06:39:34'),
(4518, '', 0.00, 6, '2019-11-12 06:39:34'),
(4519, '', 0.00, 6, '2019-11-12 06:39:34'),
(4520, '', 0.00, 6, '2019-11-12 06:39:34'),
(4521, '', 0.00, 6, '2019-11-12 06:39:34'),
(4522, '', 0.00, 6, '2019-11-12 06:39:34'),
(4523, '', 0.00, 6, '2019-11-12 06:39:34'),
(4524, '', 0.00, 6, '2019-11-12 06:39:34'),
(4525, '', 0.00, 6, '2019-11-12 06:39:34'),
(4526, '', 0.00, 6, '2019-11-12 06:39:34'),
(4527, '', 0.00, 6, '2019-11-12 06:39:34'),
(4528, '', 0.00, 6, '2019-11-12 06:39:34'),
(4529, '', 0.00, 6, '2019-11-12 06:39:34'),
(4530, '', 0.00, 6, '2019-11-12 06:39:34'),
(4531, '', 0.00, 6, '2019-11-12 06:39:34'),
(4532, '', 0.00, 6, '2019-11-12 06:39:34'),
(4533, '', 0.00, 6, '2019-11-12 06:39:34'),
(4534, '', 0.00, 6, '2019-11-12 06:39:34'),
(4535, '', 0.00, 6, '2019-11-12 06:39:35'),
(4536, '', 0.00, 6, '2019-11-12 06:39:35'),
(4537, '', 0.00, 6, '2019-11-12 06:39:35'),
(4538, '', 0.00, 6, '2019-11-12 06:39:35'),
(4539, '', 0.00, 6, '2019-11-12 06:39:35'),
(4540, '\0\0\0', 0.00, 6, '2019-11-12 06:39:35'),
(4541, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:35'),
(4542, '', 0.00, 6, '2019-11-12 06:39:35'),
(4543, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:35'),
(4544, '', 0.00, 6, '2019-11-12 06:39:35'),
(4545, '', 0.00, 6, '2019-11-12 06:39:36'),
(4546, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:36'),
(4547, '', 0.00, 6, '2019-11-12 06:39:36'),
(4548, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:37'),
(4549, '', 0.00, 6, '2019-11-12 06:39:37'),
(4550, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:37'),
(4551, '', 0.00, 6, '2019-11-12 06:39:37'),
(4552, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:37'),
(4553, '', 0.00, 6, '2019-11-12 06:39:37'),
(4554, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:37'),
(4555, '', 0.00, 6, '2019-11-12 06:39:37'),
(4556, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:37'),
(4557, '', 0.00, 6, '2019-11-12 06:39:37'),
(4558, '', 0.00, 6, '2019-11-12 06:39:37'),
(4559, '\0', 0.00, 6, '2019-11-12 06:39:37'),
(4560, '\0\0\0', 0.00, 6, '2019-11-12 06:39:37'),
(4561, '\0\0~', 0.00, 6, '2019-11-12 06:39:37'),
(4562, '\0', 0.00, 6, '2019-11-12 06:39:37'),
(4563, '', 0.00, 6, '2019-11-12 06:39:38'),
(4564, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:38'),
(4565, '\0\0\0', 0.00, 6, '2019-11-12 06:39:38'),
(4566, '', 0.00, 6, '2019-11-12 06:39:38'),
(4567, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:38'),
(4568, '', 0.00, 6, '2019-11-12 06:39:38'),
(4569, '\0\r\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:39:38'),
(4570, '', 0.00, 6, '2019-11-12 06:39:38'),
(4571, '', 0.00, 6, '2019-11-12 06:39:38'),
(4572, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:38'),
(4573, '', 0.00, 6, '2019-11-12 06:39:38'),
(4574, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:38'),
(4575, '', 0.00, 6, '2019-11-12 06:39:38'),
(4576, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:38'),
(4577, '', 0.00, 6, '2019-11-12 06:39:38'),
(4578, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:38'),
(4579, '', 0.00, 6, '2019-11-12 06:39:38'),
(4580, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:38'),
(4581, '', 0.00, 6, '2019-11-12 06:39:38'),
(4582, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:38'),
(4583, '', 0.00, 6, '2019-11-12 06:39:38'),
(4584, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:38'),
(4585, '', 0.00, 6, '2019-11-12 06:39:39'),
(4586, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:39'),
(4587, '', 0.00, 6, '2019-11-12 06:39:39'),
(4588, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:39'),
(4589, '', 0.00, 6, '2019-11-12 06:39:39'),
(4590, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:39'),
(4591, '\0\0\0', 0.00, 6, '2019-11-12 06:39:39'),
(4592, '', 0.00, 6, '2019-11-12 06:39:40'),
(4593, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:40'),
(4594, '', 0.00, 6, '2019-11-12 06:39:40'),
(4595, '\0\Z\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:39:40'),
(4596, '', 0.00, 6, '2019-11-12 06:39:40'),
(4597, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:40'),
(4598, '', 0.00, 6, '2019-11-12 06:39:41'),
(4599, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:41'),
(4600, '', 0.00, 6, '2019-11-12 06:39:41'),
(4601, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:41'),
(4602, '', 0.00, 6, '2019-11-12 06:39:41'),
(4603, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:41'),
(4604, '', 0.00, 6, '2019-11-12 06:39:41'),
(4605, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:41'),
(4606, '', 0.00, 6, '2019-11-12 06:39:41'),
(4607, '\0 \0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:39:41'),
(4608, '', 0.00, 6, '2019-11-12 06:39:41'),
(4609, '\0!\0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:39:41'),
(4610, '', 0.00, 6, '2019-11-12 06:39:41'),
(4611, '\0&quot;\0\0\0&quot;\0\0~', 0.00, 6, '2019-11-12 06:39:41'),
(4612, '', 0.00, 6, '2019-11-12 06:39:41'),
(4613, '\0#\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:39:41'),
(4614, '', 0.00, 6, '2019-11-12 06:39:41'),
(4615, '\0$\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:39:41'),
(4616, '', 0.00, 6, '2019-11-12 06:39:41'),
(4617, '\0%\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:39:41'),
(4618, '', 0.00, 6, '2019-11-12 06:39:42'),
(4619, '\0&amp;\0\0\0&amp;\0\0~', 0.00, 6, '2019-11-12 06:39:42'),
(4620, '', 0.00, 6, '2019-11-12 06:39:42'),
(4621, '\0\'\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:39:42'),
(4622, '', 0.00, 6, '2019-11-12 06:39:42'),
(4623, '\0(\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:39:42'),
(4624, '', 0.00, 6, '2019-11-12 06:39:42'),
(4625, '\0)\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:39:42'),
(4626, '', 0.00, 6, '2019-11-12 06:39:42'),
(4627, '\0*\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:39:42'),
(4628, '', 0.00, 6, '2019-11-12 06:39:42'),
(4629, '\0+\0\0\0+\0\0~', 0.00, 6, '2019-11-12 06:39:42'),
(4630, '', 0.00, 6, '2019-11-12 06:39:42'),
(4631, '\0', 0.00, 6, '2019-11-12 06:39:42'),
(4632, '\0', 0.00, 6, '2019-11-12 06:39:42'),
(4633, '\0-\0\0\0-\0\0~', 0.00, 6, '2019-11-12 06:39:43'),
(4634, '', 0.00, 6, '2019-11-12 06:39:43'),
(4635, '\0.\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:39:43'),
(4636, '', 0.00, 6, '2019-11-12 06:39:43'),
(4637, '\0/\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:39:43'),
(4638, '', 0.00, 6, '2019-11-12 06:39:43'),
(4639, '\00\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:39:43'),
(4640, '', 0.00, 6, '2019-11-12 06:39:43'),
(4641, '', 0.00, 6, '2019-11-12 06:39:43'),
(4642, '\02\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:39:43'),
(4643, '', 0.00, 6, '2019-11-12 06:39:43'),
(4644, '\03\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:39:43'),
(4645, '', 0.00, 6, '2019-11-12 06:39:44'),
(4646, '\04\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:39:44'),
(4647, '', 0.00, 6, '2019-11-12 06:39:44'),
(4648, '\05\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:39:44'),
(4649, '', 0.00, 6, '2019-11-12 06:39:44'),
(4650, '\06\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:39:44'),
(4651, '\06\0\0', 0.00, 6, '2019-11-12 06:39:44'),
(4652, '', 0.00, 6, '2019-11-12 06:39:44'),
(4653, '\07\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:39:44'),
(4654, '', 0.00, 6, '2019-11-12 06:39:44'),
(4655, '\08\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:39:45'),
(4656, '', 0.00, 6, '2019-11-12 06:39:45'),
(4657, '\09\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:39:45'),
(4658, '', 0.00, 6, '2019-11-12 06:39:45'),
(4659, '\0:\0\0\0:\0\0~', 0.00, 6, '2019-11-12 06:39:45'),
(4660, '', 0.00, 6, '2019-11-12 06:39:45'),
(4661, '\0;\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:39:45'),
(4662, '', 0.00, 6, '2019-11-12 06:39:45'),
(4663, '\0&lt;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:39:45'),
(4664, '', 0.00, 6, '2019-11-12 06:39:45'),
(4665, '\0=\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:39:45'),
(4666, '', 0.00, 6, '2019-11-12 06:39:45'),
(4667, '\0&gt;\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:39:45'),
(4668, '', 0.00, 6, '2019-11-12 06:39:45'),
(4669, '\0?\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:39:45'),
(4670, '\0?\0\0!\0\0\0@\0\0\0', 0.00, 6, '2019-11-12 06:39:45'),
(4671, '\0@\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:39:46'),
(4672, '', 0.00, 6, '2019-11-12 06:39:46'),
(4673, '\0A\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:39:46'),
(4674, '', 0.00, 6, '2019-11-12 06:39:46'),
(4675, '\0B\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:39:46'),
(4676, '', 0.00, 6, '2019-11-12 06:39:46'),
(4677, '\0C\0\0\0C\0\0~', 0.00, 6, '2019-11-12 06:39:46'),
(4678, '', 0.00, 6, '2019-11-12 06:39:46'),
(4679, '\0D\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:39:46'),
(4680, '', 0.00, 6, '2019-11-12 06:39:46'),
(4681, '\0E\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:39:46'),
(4682, '', 0.00, 6, '2019-11-12 06:39:46'),
(4683, '\0F\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:39:46'),
(4684, '', 0.00, 6, '2019-11-12 06:39:46'),
(4685, '\0G\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:39:46'),
(4686, '', 0.00, 6, '2019-11-12 06:39:47'),
(4687, '\0H\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:39:47'),
(4688, '', 0.00, 6, '2019-11-12 06:39:47'),
(4689, '\0I\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:39:47'),
(4690, '', 0.00, 6, '2019-11-12 06:39:47'),
(4691, '\0J\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:39:47'),
(4692, '', 0.00, 6, '2019-11-12 06:39:47'),
(4693, '\0K\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:39:47'),
(4694, '', 0.00, 6, '2019-11-12 06:39:47'),
(4695, '\0L\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:39:47'),
(4696, '', 0.00, 6, '2019-11-12 06:39:47'),
(4697, '\0M\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:39:47'),
(4698, '', 0.00, 6, '2019-11-12 06:39:47'),
(4699, '\0N\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:39:47'),
(4700, '', 0.00, 6, '2019-11-12 06:39:47'),
(4701, '\0O\0\0\0O\0\0~', 0.00, 6, '2019-11-12 06:39:47'),
(4702, '', 0.00, 6, '2019-11-12 06:39:47'),
(4703, '\0P\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:39:47'),
(4704, '', 0.00, 6, '2019-11-12 06:39:47'),
(4705, '\0Q\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:39:47'),
(4706, '', 0.00, 6, '2019-11-12 06:39:48'),
(4707, '\0R\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:39:48'),
(4708, '', 0.00, 6, '2019-11-12 06:39:48'),
(4709, '\0S\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:39:48'),
(4710, '', 0.00, 6, '2019-11-12 06:39:48'),
(4711, '\0T\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:39:48'),
(4712, '\0T\0\0R', 0.00, 6, '2019-11-12 06:39:48'),
(4713, '', 0.00, 6, '2019-11-12 06:39:48'),
(4714, '\0U\0\0\0U\0\0~', 0.00, 6, '2019-11-12 06:39:48'),
(4715, '', 0.00, 6, '2019-11-12 06:39:48'),
(4716, '\0V\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:39:48'),
(4717, '', 0.00, 6, '2019-11-12 06:39:48'),
(4718, '\0W\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:39:48'),
(4719, '', 0.00, 6, '2019-11-12 06:39:48'),
(4720, '\0X\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:39:48'),
(4721, '', 0.00, 6, '2019-11-12 06:39:48'),
(4722, '\0Y\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:39:48'),
(4723, '', 0.00, 6, '2019-11-12 06:39:49'),
(4724, '\0Z\0\0\0Z\0\0~', 0.00, 6, '2019-11-12 06:39:49'),
(4725, '', 0.00, 6, '2019-11-12 06:39:49'),
(4726, '', 0.00, 6, '2019-11-12 06:39:49'),
(4727, '\0\\\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:39:49'),
(4728, '', 0.00, 6, '2019-11-12 06:39:49'),
(4729, '\0]\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:39:49'),
(4730, '', 0.00, 6, '2019-11-12 06:39:49'),
(4731, '', 0.00, 6, '2019-11-12 06:39:49'),
(4732, '\0_\0\0\0_\0\0~', 0.00, 6, '2019-11-12 06:39:49'),
(4733, '\0_\0\0*\0\0\0`\0\0\0', 0.00, 6, '2019-11-12 06:39:49'),
(4734, '\0`\0\0\0`\0\0~', 0.00, 6, '2019-11-12 06:39:49'),
(4735, '', 0.00, 6, '2019-11-12 06:39:49'),
(4736, '\0a\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:39:49'),
(4737, '\0a\0\0', 0.00, 6, '2019-11-12 06:39:49'),
(4738, '', 0.00, 6, '2019-11-12 06:39:49'),
(4739, '\0b\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:39:49'),
(4740, '', 0.00, 6, '2019-11-12 06:39:49'),
(4741, '\0c\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:39:49'),
(4742, '', 0.00, 6, '2019-11-12 06:39:49'),
(4743, '\0d\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:39:49'),
(4744, '', 0.00, 6, '2019-11-12 06:39:49'),
(4745, '\0e\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:39:49'),
(4746, '', 0.00, 6, '2019-11-12 06:39:50'),
(4747, '\0f\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:39:50'),
(4748, '', 0.00, 6, '2019-11-12 06:39:50'),
(4749, '\0g\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:39:50'),
(4750, '', 0.00, 6, '2019-11-12 06:39:50'),
(4751, '\0h\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:39:50'),
(4752, '', 0.00, 6, '2019-11-12 06:39:50'),
(4753, '\0i\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:39:50'),
(4754, '', 0.00, 6, '2019-11-12 06:39:50'),
(4755, '\0j\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:39:50'),
(4756, '', 0.00, 6, '2019-11-12 06:39:50'),
(4757, '\0k\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:39:50'),
(4758, '', 0.00, 6, '2019-11-12 06:39:50'),
(4759, '\0l\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:39:50'),
(4760, '', 0.00, 6, '2019-11-12 06:39:51'),
(4761, '\0m\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:39:51'),
(4762, '', 0.00, 6, '2019-11-12 06:39:51'),
(4763, '\0n\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:39:51'),
(4764, '', 0.00, 6, '2019-11-12 06:39:51'),
(4765, '\0o\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:39:51'),
(4766, '', 0.00, 6, '2019-11-12 06:39:51'),
(4767, '\0p\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:39:51'),
(4768, '', 0.00, 6, '2019-11-12 06:39:51'),
(4769, '', 0.00, 6, '2019-11-12 06:39:52'),
(4770, '\0r\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:39:52'),
(4771, '', 0.00, 6, '2019-11-12 06:39:52'),
(4772, '\0s\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:39:52'),
(4773, '', 0.00, 6, '2019-11-12 06:39:52'),
(4774, '\0t\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:39:52'),
(4775, '', 0.00, 6, '2019-11-12 06:39:52'),
(4776, '\0u\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:39:52'),
(4777, '', 0.00, 6, '2019-11-12 06:39:52'),
(4778, '\0v\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:39:52'),
(4779, '', 0.00, 6, '2019-11-12 06:39:52'),
(4780, '\0w\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:39:52'),
(4781, '\0w\0\0', 0.00, 6, '2019-11-12 06:39:52'),
(4782, '', 0.00, 6, '2019-11-12 06:39:52'),
(4783, '\0x\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:39:52'),
(4784, '', 0.00, 6, '2019-11-12 06:39:52'),
(4785, '\0y\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:39:52'),
(4786, '', 0.00, 6, '2019-11-12 06:39:52'),
(4787, '\0z\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:39:52'),
(4788, '', 0.00, 6, '2019-11-12 06:39:52'),
(4789, '\0{\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:39:52'),
(4790, '', 0.00, 6, '2019-11-12 06:39:53'),
(4791, '\0|\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:39:53'),
(4792, '', 0.00, 6, '2019-11-12 06:39:53'),
(4793, '\0}\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:39:53'),
(4794, '', 0.00, 6, '2019-11-12 06:39:53'),
(4795, '', 0.00, 6, '2019-11-12 06:39:53'),
(4796, '', 0.00, 6, '2019-11-12 06:39:53'),
(4797, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:39:53'),
(4798, '', 0.00, 6, '2019-11-12 06:39:53'),
(4799, '', 0.00, 6, '2019-11-12 06:39:53'),
(4800, '', 0.00, 6, '2019-11-12 06:39:53'),
(4801, '', 0.00, 6, '2019-11-12 06:39:53'),
(4802, '', 0.00, 6, '2019-11-12 06:39:53'),
(4803, '', 0.00, 6, '2019-11-12 06:39:53'),
(4804, '', 0.00, 6, '2019-11-12 06:39:53'),
(4805, '', 0.00, 6, '2019-11-12 06:39:53'),
(4806, '', 0.00, 6, '2019-11-12 06:39:53'),
(4807, '', 0.00, 6, '2019-11-12 06:39:53'),
(4808, '', 0.00, 6, '2019-11-12 06:39:53'),
(4809, '', 0.00, 6, '2019-11-12 06:39:54'),
(4810, '', 0.00, 6, '2019-11-12 06:39:54'),
(4811, '', 0.00, 6, '2019-11-12 06:39:54'),
(4812, '', 0.00, 6, '2019-11-12 06:39:54'),
(4813, '', 0.00, 6, '2019-11-12 06:39:54'),
(4814, '', 0.00, 6, '2019-11-12 06:39:54'),
(4815, '', 0.00, 6, '2019-11-12 06:39:54'),
(4816, '', 0.00, 6, '2019-11-12 06:39:54'),
(4817, '', 0.00, 6, '2019-11-12 06:39:54'),
(4818, '', 0.00, 6, '2019-11-12 06:39:54'),
(4819, '', 0.00, 6, '2019-11-12 06:39:54'),
(4820, '', 0.00, 6, '2019-11-12 06:39:54'),
(4821, '', 0.00, 6, '2019-11-12 06:39:54'),
(4822, '', 0.00, 6, '2019-11-12 06:39:54'),
(4823, '', 0.00, 6, '2019-11-12 06:39:54'),
(4824, '', 0.00, 6, '2019-11-12 06:39:54'),
(4825, '', 0.00, 6, '2019-11-12 06:39:54'),
(4826, '', 0.00, 6, '2019-11-12 06:39:54'),
(4827, '', 0.00, 6, '2019-11-12 06:39:54'),
(4828, '', 0.00, 6, '2019-11-12 06:39:54'),
(4829, '', 0.00, 6, '2019-11-12 06:39:55'),
(4830, '', 0.00, 6, '2019-11-12 06:39:55'),
(4831, '', 0.00, 6, '2019-11-12 06:39:55'),
(4832, '', 0.00, 6, '2019-11-12 06:39:55'),
(4833, '', 0.00, 6, '2019-11-12 06:39:55'),
(4834, '', 0.00, 6, '2019-11-12 06:39:55'),
(4835, '', 0.00, 6, '2019-11-12 06:39:55'),
(4836, '', 0.00, 6, '2019-11-12 06:39:55'),
(4837, '', 0.00, 6, '2019-11-12 06:39:55'),
(4838, '', 0.00, 6, '2019-11-12 06:39:55'),
(4839, '', 0.00, 6, '2019-11-12 06:39:55'),
(4840, '', 0.00, 6, '2019-11-12 06:39:55'),
(4841, '', 0.00, 6, '2019-11-12 06:39:55'),
(4842, '', 0.00, 6, '2019-11-12 06:39:55'),
(4843, '', 0.00, 6, '2019-11-12 06:39:55'),
(4844, '', 0.00, 6, '2019-11-12 06:39:55'),
(4845, '', 0.00, 6, '2019-11-12 06:39:55'),
(4846, '', 0.00, 6, '2019-11-12 06:39:55'),
(4847, '', 0.00, 6, '2019-11-12 06:39:55'),
(4848, '', 0.00, 6, '2019-11-12 06:39:55'),
(4849, '', 0.00, 6, '2019-11-12 06:39:55'),
(4850, '', 0.00, 6, '2019-11-12 06:39:55'),
(4851, '', 0.00, 6, '2019-11-12 06:39:56'),
(4852, '', 0.00, 6, '2019-11-12 06:39:56'),
(4853, '', 0.00, 6, '2019-11-12 06:39:56'),
(4854, '', 0.00, 6, '2019-11-12 06:39:56'),
(4855, '', 0.00, 6, '2019-11-12 06:39:56'),
(4856, '', 0.00, 6, '2019-11-12 06:39:56'),
(4857, '', 0.00, 6, '2019-11-12 06:39:56'),
(4858, '', 0.00, 6, '2019-11-12 06:39:56'),
(4859, '', 0.00, 6, '2019-11-12 06:39:56'),
(4860, '', 0.00, 6, '2019-11-12 06:39:56'),
(4861, '', 0.00, 6, '2019-11-12 06:39:56'),
(4862, '', 0.00, 6, '2019-11-12 06:39:57'),
(4863, '', 0.00, 6, '2019-11-12 06:39:57'),
(4864, '', 0.00, 6, '2019-11-12 06:39:57'),
(4865, '', 0.00, 6, '2019-11-12 06:39:57'),
(4866, '', 0.00, 6, '2019-11-12 06:39:57'),
(4867, '', 0.00, 6, '2019-11-12 06:39:57'),
(4868, '', 0.00, 6, '2019-11-12 06:39:57'),
(4869, '', 0.00, 6, '2019-11-12 06:39:57'),
(4870, '', 0.00, 6, '2019-11-12 06:39:57'),
(4871, '', 0.00, 6, '2019-11-12 06:39:57'),
(4872, '', 0.00, 6, '2019-11-12 06:39:57'),
(4873, '', 0.00, 6, '2019-11-12 06:39:57'),
(4874, '', 0.00, 6, '2019-11-12 06:39:57'),
(4875, '', 0.00, 6, '2019-11-12 06:39:57'),
(4876, '', 0.00, 6, '2019-11-12 06:39:58'),
(4877, '', 0.00, 6, '2019-11-12 06:39:58'),
(4878, '', 0.00, 6, '2019-11-12 06:39:58'),
(4879, '', 0.00, 6, '2019-11-12 06:39:58'),
(4880, '', 0.00, 6, '2019-11-12 06:39:58'),
(4881, '', 0.00, 6, '2019-11-12 06:39:58'),
(4882, '', 0.00, 6, '2019-11-12 06:39:58'),
(4883, '', 0.00, 6, '2019-11-12 06:39:58'),
(4884, '', 0.00, 6, '2019-11-12 06:39:58'),
(4885, '', 0.00, 6, '2019-11-12 06:39:58'),
(4886, '', 0.00, 6, '2019-11-12 06:39:58'),
(4887, '', 0.00, 6, '2019-11-12 06:39:58'),
(4888, '', 0.00, 6, '2019-11-12 06:39:58'),
(4889, '', 0.00, 6, '2019-11-12 06:39:58'),
(4890, '', 0.00, 6, '2019-11-12 06:39:58'),
(4891, '', 0.00, 6, '2019-11-12 06:39:58'),
(4892, '', 0.00, 6, '2019-11-12 06:39:58'),
(4893, '', 0.00, 6, '2019-11-12 06:39:59'),
(4894, '', 0.00, 6, '2019-11-12 06:39:59'),
(4895, '', 0.00, 6, '2019-11-12 06:39:59'),
(4896, '', 0.00, 6, '2019-11-12 06:39:59'),
(4897, '', 0.00, 6, '2019-11-12 06:39:59'),
(4898, '', 0.00, 6, '2019-11-12 06:39:59'),
(4899, '', 0.00, 6, '2019-11-12 06:39:59'),
(4900, '', 0.00, 6, '2019-11-12 06:39:59'),
(4901, '', 0.00, 6, '2019-11-12 06:39:59'),
(4902, '', 0.00, 6, '2019-11-12 06:39:59'),
(4903, '', 0.00, 6, '2019-11-12 06:39:59'),
(4904, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', 0.00, 6, '2019-11-12 06:39:59'),
(4905, '\0\0Ixime', 0.00, 6, '2019-11-12 06:40:00'),
(4906, '\0\0Manix', 0.00, 6, '2019-11-12 06:40:00'),
(4907, '\0\0Pelox', 0.00, 6, '2019-11-12 06:40:00'),
(4908, '\0\0Supracin', 0.00, 6, '2019-11-12 06:40:00'),
(4909, '\0\0Tetacid ', 0.00, 6, '2019-11-12 06:40:00'),
(4910, '', 0.00, 6, '2019-11-12 06:40:00'),
(4911, '\0\0\0\0', 0.00, 6, '2019-11-12 06:40:00'),
(4912, '', 0.00, 6, '2019-11-12 06:40:00'),
(4913, '', 0.00, 6, '2019-11-12 06:40:00'),
(4914, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:00'),
(4915, '', 0.00, 6, '2019-11-12 06:40:00'),
(4916, '', 0.00, 6, '2019-11-12 06:40:00'),
(4917, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:01'),
(4918, '', 0.00, 6, '2019-11-12 06:40:01'),
(4919, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:01'),
(4920, '', 0.00, 6, '2019-11-12 06:40:01'),
(4921, '', 0.00, 6, '2019-11-12 06:40:01'),
(4922, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:01'),
(4923, '', 0.00, 6, '2019-11-12 06:40:01'),
(4924, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:01'),
(4925, '', 0.00, 6, '2019-11-12 06:40:01'),
(4926, '\0\0\0\0\0	\0\0\0~', 0.00, 6, '2019-11-12 06:40:02'),
(4927, '', 0.00, 6, '2019-11-12 06:40:02'),
(4928, '\0	\0\0\0\0', 0.00, 6, '2019-11-12 06:40:02'),
(4929, '\0\0\0~', 0.00, 6, '2019-11-12 06:40:03'),
(4930, '', 0.00, 6, '2019-11-12 06:40:03'),
(4931, '\0', 0.00, 6, '2019-11-12 06:40:03'),
(4932, '\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:03'),
(4933, '\0', 0.00, 6, '2019-11-12 06:40:03'),
(4934, '', 0.00, 6, '2019-11-12 06:40:03'),
(4935, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:03'),
(4936, '\0\0\0\0', 0.00, 6, '2019-11-12 06:40:03'),
(4937, '', 0.00, 6, '2019-11-12 06:40:03'),
(4938, '\0\0\0\0\0\r\0\0\0~', 0.00, 6, '2019-11-12 06:40:03'),
(4939, '', 0.00, 6, '2019-11-12 06:40:03'),
(4940, '\0\r\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:03'),
(4941, '', 0.00, 6, '2019-11-12 06:40:03'),
(4942, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:03'),
(4943, '', 0.00, 6, '2019-11-12 06:40:03'),
(4944, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:03'),
(4945, '', 0.00, 6, '2019-11-12 06:40:03'),
(4946, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:04'),
(4947, '', 0.00, 6, '2019-11-12 06:40:04'),
(4948, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:04'),
(4949, '\0\0\0\0P', 0.00, 6, '2019-11-12 06:40:04'),
(4950, '', 0.00, 6, '2019-11-12 06:40:04'),
(4951, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:04'),
(4952, '', 0.00, 6, '2019-11-12 06:40:04'),
(4953, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:04'),
(4954, '', 0.00, 6, '2019-11-12 06:40:04'),
(4955, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:04'),
(4956, '\0\0\0\0', 0.00, 6, '2019-11-12 06:40:04'),
(4957, '', 0.00, 6, '2019-11-12 06:40:04'),
(4958, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:04'),
(4959, '', 0.00, 6, '2019-11-12 06:40:04'),
(4960, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:04'),
(4961, '', 0.00, 6, '2019-11-12 06:40:04'),
(4962, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:04'),
(4963, '', 0.00, 6, '2019-11-12 06:40:04'),
(4964, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:05'),
(4965, '', 0.00, 6, '2019-11-12 06:40:05'),
(4966, '\0\0\0\0\0\Z\0\0\0~', 0.00, 6, '2019-11-12 06:40:05'),
(4967, '', 0.00, 6, '2019-11-12 06:40:05'),
(4968, '\0\Z\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:05'),
(4969, '', 0.00, 6, '2019-11-12 06:40:05'),
(4970, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:05'),
(4971, '', 0.00, 6, '2019-11-12 06:40:05'),
(4972, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:05'),
(4973, '', 0.00, 6, '2019-11-12 06:40:05'),
(4974, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:05'),
(4975, '', 0.00, 6, '2019-11-12 06:40:05'),
(4976, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:05'),
(4977, '', 0.00, 6, '2019-11-12 06:40:05'),
(4978, '\0\0\0\0\0 \0\0\0~', 0.00, 6, '2019-11-12 06:40:05'),
(4979, '\0\0\0\0#F\0\0\0 \0\0\0\0', 0.00, 6, '2019-11-12 06:40:06'),
(4980, '\0 \0\0\0\0!\0\0\0~', 0.00, 6, '2019-11-12 06:40:06'),
(4981, '', 0.00, 6, '2019-11-12 06:40:06'),
(4982, '\0!\0\0\0\0&quot;\0\0\0~', 0.00, 6, '2019-11-12 06:40:06'),
(4983, '', 0.00, 6, '2019-11-12 06:40:06'),
(4984, '\0&quot;\0\0\0\0#\0\0\0~', 0.00, 6, '2019-11-12 06:40:06'),
(4985, '', 0.00, 6, '2019-11-12 06:40:06'),
(4986, '\0#\0\0\0\0$\0\0\0~', 0.00, 6, '2019-11-12 06:40:06'),
(4987, '', 0.00, 6, '2019-11-12 06:40:07'),
(4988, '\0$\0\0\0\0%\0\0\0~', 0.00, 6, '2019-11-12 06:40:07'),
(4989, '', 0.00, 6, '2019-11-12 06:40:07'),
(4990, '\0%\0\0\0\0&amp;\0\0\0~', 0.00, 6, '2019-11-12 06:40:07'),
(4991, '', 0.00, 6, '2019-11-12 06:40:07'),
(4992, '\0&amp;\0\0\0\0\'\0\0\0~', 0.00, 6, '2019-11-12 06:40:07'),
(4993, '', 0.00, 6, '2019-11-12 06:40:07'),
(4994, '\0\'\0\0\0\0(\0\0\0~', 0.00, 6, '2019-11-12 06:40:07'),
(4995, '', 0.00, 6, '2019-11-12 06:40:07'),
(4996, '', 0.00, 6, '2019-11-12 06:40:07'),
(4997, '\0)\0\0\0\0*\0\0\0~', 0.00, 6, '2019-11-12 06:40:07'),
(4998, '', 0.00, 6, '2019-11-12 06:40:07'),
(4999, '\0*\0\0\0\0+\0\0\0~', 0.00, 6, '2019-11-12 06:40:07'),
(5000, '', 0.00, 6, '2019-11-12 06:40:07'),
(5001, '\0+\0\0\0\0', 0.00, 6, '2019-11-12 06:40:07'),
(5002, '', 0.00, 6, '2019-11-12 06:40:07'),
(5003, '\0', 0.00, 6, '2019-11-12 06:40:07'),
(5004, '\0', 0.00, 6, '2019-11-12 06:40:07'),
(5005, '\0-\0\0\0\0.\0\0\0~', 0.00, 6, '2019-11-12 06:40:07'),
(5006, '', 0.00, 6, '2019-11-12 06:40:07'),
(5007, '\0.\0\0\0\0/\0\0\0~', 0.00, 6, '2019-11-12 06:40:08'),
(5008, '', 0.00, 6, '2019-11-12 06:40:08'),
(5009, '\0/\0\0\0\00\0\0\0~', 0.00, 6, '2019-11-12 06:40:08'),
(5010, '', 0.00, 6, '2019-11-12 06:40:08'),
(5011, '\00\0\0\0\01\0\0\0~', 0.00, 6, '2019-11-12 06:40:08'),
(5012, '', 0.00, 6, '2019-11-12 06:40:08'),
(5013, '\01\0\0\0\02\0\0\0~', 0.00, 6, '2019-11-12 06:40:08'),
(5014, '', 0.00, 6, '2019-11-12 06:40:08'),
(5015, '\02\0\0\0\03\0\0\0~', 0.00, 6, '2019-11-12 06:40:08'),
(5016, '', 0.00, 6, '2019-11-12 06:40:08'),
(5017, '\03\0\0\0\04\0\0\0~', 0.00, 6, '2019-11-12 06:40:08'),
(5018, '\03\0\0\0', 0.00, 6, '2019-11-12 06:40:08'),
(5019, '', 0.00, 6, '2019-11-12 06:40:08'),
(5020, '\04\0\0\0\05\0\0\0~', 0.00, 6, '2019-11-12 06:40:08'),
(5021, '', 0.00, 6, '2019-11-12 06:40:09'),
(5022, '\05\0\0\0\06\0\0\0~', 0.00, 6, '2019-11-12 06:40:09'),
(5023, '', 0.00, 6, '2019-11-12 06:40:09'),
(5024, '\06\0\0\0\07\0\0\0~', 0.00, 6, '2019-11-12 06:40:09'),
(5025, '', 0.00, 6, '2019-11-12 06:40:09'),
(5026, '\07\0\0\0\08\0\0\0~', 0.00, 6, '2019-11-12 06:40:09'),
(5027, '', 0.00, 6, '2019-11-12 06:40:09'),
(5028, '', 0.00, 6, '2019-11-12 06:40:09'),
(5029, '\09\0\0\0\0:\0\0\0~', 0.00, 6, '2019-11-12 06:40:09'),
(5030, '', 0.00, 6, '2019-11-12 06:40:09'),
(5031, '\0:\0\0\0\0;\0\0\0~', 0.00, 6, '2019-11-12 06:40:09'),
(5032, '', 0.00, 6, '2019-11-12 06:40:09'),
(5033, '\0;\0\0\0\0&lt;\0\0\0~', 0.00, 6, '2019-11-12 06:40:09'),
(5034, '', 0.00, 6, '2019-11-12 06:40:09'),
(5035, '\0&lt;\0\0\0\0=\0\0\0~', 0.00, 6, '2019-11-12 06:40:09'),
(5036, '', 0.00, 6, '2019-11-12 06:40:09'),
(5037, '\0=\0\0\0\0&gt;\0\0\0~', 0.00, 6, '2019-11-12 06:40:09'),
(5038, '', 0.00, 6, '2019-11-12 06:40:09'),
(5039, '', 0.00, 6, '2019-11-12 06:40:09'),
(5040, '\0?\0\0\0\0@\0\0\0~', 0.00, 6, '2019-11-12 06:40:09'),
(5041, '', 0.00, 6, '2019-11-12 06:40:10'),
(5042, '\0@\0\0\0\0A\0\0\0~', 0.00, 6, '2019-11-12 06:40:10'),
(5043, '', 0.00, 6, '2019-11-12 06:40:10'),
(5044, '\0A\0\0\0\0B\0\0\0~', 0.00, 6, '2019-11-12 06:40:10'),
(5045, '', 0.00, 6, '2019-11-12 06:40:10'),
(5046, '\0B\0\0\0\0C\0\0\0~', 0.00, 6, '2019-11-12 06:40:10'),
(5047, '', 0.00, 6, '2019-11-12 06:40:10'),
(5048, '\0C\0\0\0\0D\0\0\0~', 0.00, 6, '2019-11-12 06:40:10'),
(5049, '', 0.00, 6, '2019-11-12 06:40:10'),
(5050, '\0D\0\0\0\0E\0\0\0~', 0.00, 6, '2019-11-12 06:40:10'),
(5051, '', 0.00, 6, '2019-11-12 06:40:10'),
(5052, '', 0.00, 6, '2019-11-12 06:40:10'),
(5053, '\0F\0\0\0\0G\0\0\0~', 0.00, 6, '2019-11-12 06:40:10'),
(5054, '', 0.00, 6, '2019-11-12 06:40:10'),
(5055, '\0G\0\0\0\0H\0\0\0~', 0.00, 6, '2019-11-12 06:40:10'),
(5056, '', 0.00, 6, '2019-11-12 06:40:10'),
(5057, '', 0.00, 6, '2019-11-12 06:40:11'),
(5058, '\0I\0\0\0\0J\0\0\0~', 0.00, 6, '2019-11-12 06:40:11'),
(5059, '', 0.00, 6, '2019-11-12 06:40:11'),
(5060, '\0J\0\0\0\0K\0\0\0~', 0.00, 6, '2019-11-12 06:40:11'),
(5061, '', 0.00, 6, '2019-11-12 06:40:11'),
(5062, '\0K\0\0\0\0L\0\0\0~', 0.00, 6, '2019-11-12 06:40:11'),
(5063, '', 0.00, 6, '2019-11-12 06:40:11'),
(5064, '\0L\0\0\0\0M\0\0\0~', 0.00, 6, '2019-11-12 06:40:11'),
(5065, '', 0.00, 6, '2019-11-12 06:40:11'),
(5066, '\0M\0\0\0\0N\0\0\0~', 0.00, 6, '2019-11-12 06:40:11'),
(5067, '', 0.00, 6, '2019-11-12 06:40:12'),
(5068, '\0N\0\0\0\0O\0\0\0~', 0.00, 6, '2019-11-12 06:40:12'),
(5069, '', 0.00, 6, '2019-11-12 06:40:12'),
(5070, '\0O\0\0\0\0P\0\0\0~', 0.00, 6, '2019-11-12 06:40:12'),
(5071, '', 0.00, 6, '2019-11-12 06:40:12'),
(5072, '\0P\0\0\0\0Q\0\0\0~', 0.00, 6, '2019-11-12 06:40:12'),
(5073, '', 0.00, 6, '2019-11-12 06:40:12'),
(5074, '\0Q\0\0\0\0R\0\0\0~', 0.00, 6, '2019-11-12 06:40:12'),
(5075, '', 0.00, 6, '2019-11-12 06:40:12'),
(5076, '\0R\0\0\0\0S\0\0\0~', 0.00, 6, '2019-11-12 06:40:12'),
(5077, '', 0.00, 6, '2019-11-12 06:40:12'),
(5078, '\0S\0\0\0\0T\0\0\0~', 0.00, 6, '2019-11-12 06:40:12'),
(5079, '', 0.00, 6, '2019-11-12 06:40:12'),
(5080, '\0T\0\0\0\0U\0\0\0~', 0.00, 6, '2019-11-12 06:40:12'),
(5081, '', 0.00, 6, '2019-11-12 06:40:12'),
(5082, '\0U\0\0\0\0V\0\0\0~', 0.00, 6, '2019-11-12 06:40:12'),
(5083, '', 0.00, 6, '2019-11-12 06:40:12'),
(5084, '\0V\0\0\0\0W\0\0\0~', 0.00, 6, '2019-11-12 06:40:12'),
(5085, '', 0.00, 6, '2019-11-12 06:40:12'),
(5086, '\0W\0\0\0\0X\0\0\0~', 0.00, 6, '2019-11-12 06:40:12'),
(5087, '', 0.00, 6, '2019-11-12 06:40:13'),
(5088, '\0X\0\0\0\0Y\0\0\0~', 0.00, 6, '2019-11-12 06:40:13'),
(5089, '', 0.00, 6, '2019-11-12 06:40:13'),
(5090, '\0Y\0\0\0\0Z\0\0\0~', 0.00, 6, '2019-11-12 06:40:13'),
(5091, '', 0.00, 6, '2019-11-12 06:40:13'),
(5092, '\0Z\0\0\0\0[\0\0\0~', 0.00, 6, '2019-11-12 06:40:13'),
(5093, '', 0.00, 6, '2019-11-12 06:40:13'),
(5094, '\0[\0\0\0\0\\\0\0\0~', 0.00, 6, '2019-11-12 06:40:13'),
(5095, '', 0.00, 6, '2019-11-12 06:40:13'),
(5096, '\0\\\0\0\0\0]\0\0\0~', 0.00, 6, '2019-11-12 06:40:13'),
(5097, '', 0.00, 6, '2019-11-12 06:40:14'),
(5098, '\0]\0\0\0\0^\0\0\0~', 0.00, 6, '2019-11-12 06:40:14'),
(5099, '', 0.00, 6, '2019-11-12 06:40:14'),
(5100, '\0^\0\0\0\0_\0\0\0~', 0.00, 6, '2019-11-12 06:40:14'),
(5101, '', 0.00, 6, '2019-11-12 06:40:14'),
(5102, '\0_\0\0\0\0`\0\0\0~', 0.00, 6, '2019-11-12 06:40:14'),
(5103, '\0_\0\0\0j\r\0\0\0`\0\0\0\0', 0.00, 6, '2019-11-12 06:40:14'),
(5104, '\0`\0\0\0\0a\0\0\0~', 0.00, 6, '2019-11-12 06:40:14'),
(5105, '', 0.00, 6, '2019-11-12 06:40:14'),
(5106, '\0a\0\0\0\0b\0\0\0~', 0.00, 6, '2019-11-12 06:40:14'),
(5107, '\0a\0\0\0S', 0.00, 6, '2019-11-12 06:40:14'),
(5108, '', 0.00, 6, '2019-11-12 06:40:14'),
(5109, '\0b\0\0\0\0c\0\0\0~', 0.00, 6, '2019-11-12 06:40:14'),
(5110, '', 0.00, 6, '2019-11-12 06:40:15'),
(5111, '\0c\0\0\0\0d\0\0\0~', 0.00, 6, '2019-11-12 06:40:15'),
(5112, '', 0.00, 6, '2019-11-12 06:40:15'),
(5113, '', 0.00, 6, '2019-11-12 06:40:15'),
(5114, '\0e\0\0\0\0f\0\0\0~', 0.00, 6, '2019-11-12 06:40:15'),
(5115, '', 0.00, 6, '2019-11-12 06:40:15'),
(5116, '\0f\0\0\0\0g\0\0\0~', 0.00, 6, '2019-11-12 06:40:15'),
(5117, '', 0.00, 6, '2019-11-12 06:40:15'),
(5118, '\0g\0\0\0\0h\0\0\0~', 0.00, 6, '2019-11-12 06:40:15'),
(5119, '', 0.00, 6, '2019-11-12 06:40:15'),
(5120, '\0h\0\0\0\0i\0\0\0~', 0.00, 6, '2019-11-12 06:40:15'),
(5121, '', 0.00, 6, '2019-11-12 06:40:15'),
(5122, '\0i\0\0\0\0j\0\0\0~', 0.00, 6, '2019-11-12 06:40:16'),
(5123, '', 0.00, 6, '2019-11-12 06:40:16'),
(5124, '\0j\0\0\0\0k\0\0\0~', 0.00, 6, '2019-11-12 06:40:16'),
(5125, '', 0.00, 6, '2019-11-12 06:40:16'),
(5126, '\0k\0\0\0\0l\0\0\0~', 0.00, 6, '2019-11-12 06:40:16'),
(5127, '', 0.00, 6, '2019-11-12 06:40:16'),
(5128, '\0l\0\0\0\0m\0\0\0~', 0.00, 6, '2019-11-12 06:40:16'),
(5129, '', 0.00, 6, '2019-11-12 06:40:16'),
(5130, '\0m\0\0\0\0n\0\0\0~', 0.00, 6, '2019-11-12 06:40:16'),
(5131, '', 0.00, 6, '2019-11-12 06:40:16'),
(5132, '', 0.00, 6, '2019-11-12 06:40:16'),
(5133, '\0o\0\0\0\0p\0\0\0~', 0.00, 6, '2019-11-12 06:40:16'),
(5134, '', 0.00, 6, '2019-11-12 06:40:16'),
(5135, '\0p\0\0\0\0q\0\0\0~', 0.00, 6, '2019-11-12 06:40:16'),
(5136, '', 0.00, 6, '2019-11-12 06:40:17'),
(5137, '\0q\0\0\0\0r\0\0\0~', 0.00, 6, '2019-11-12 06:40:17'),
(5138, '\0q\0\0\0', 0.00, 6, '2019-11-12 06:40:17'),
(5139, '', 0.00, 6, '2019-11-12 06:40:17'),
(5140, '\0r\0\0\0\0s\0\0\0~', 0.00, 6, '2019-11-12 06:40:17'),
(5141, '', 0.00, 6, '2019-11-12 06:40:17'),
(5142, '\0s\0\0\0\0t\0\0\0~', 0.00, 6, '2019-11-12 06:40:17'),
(5143, '', 0.00, 6, '2019-11-12 06:40:17'),
(5144, '\0t\0\0\0\0u\0\0\0~', 0.00, 6, '2019-11-12 06:40:17'),
(5145, '', 0.00, 6, '2019-11-12 06:40:17'),
(5146, '\0u\0\0\0\0v\0\0\0~', 0.00, 6, '2019-11-12 06:40:17'),
(5147, '', 0.00, 6, '2019-11-12 06:40:17'),
(5148, '\0v\0\0\0\0w\0\0\0~', 0.00, 6, '2019-11-12 06:40:17'),
(5149, '', 0.00, 6, '2019-11-12 06:40:17'),
(5150, '\0w\0\0\0\0x\0\0\0~', 0.00, 6, '2019-11-12 06:40:17'),
(5151, '', 0.00, 6, '2019-11-12 06:40:17'),
(5152, '\0x\0\0\0\0y\0\0\0~', 0.00, 6, '2019-11-12 06:40:18'),
(5153, '', 0.00, 6, '2019-11-12 06:40:18'),
(5154, '\0y\0\0\0\0z\0\0\0~', 0.00, 6, '2019-11-12 06:40:18'),
(5155, '', 0.00, 6, '2019-11-12 06:40:18'),
(5156, '', 0.00, 6, '2019-11-12 06:40:18'),
(5157, '', 0.00, 6, '2019-11-12 06:40:18'),
(5158, '', 0.00, 6, '2019-11-12 06:40:18'),
(5159, '\0}\0\0\0\0~\0\0\0~', 0.00, 6, '2019-11-12 06:40:18'),
(5160, '', 0.00, 6, '2019-11-12 06:40:18'),
(5161, '', 0.00, 6, '2019-11-12 06:40:18'),
(5162, '', 0.00, 6, '2019-11-12 06:40:18'),
(5163, '', 0.00, 6, '2019-11-12 06:40:18'),
(5164, '', 0.00, 6, '2019-11-12 06:40:18'),
(5165, '', 0.00, 6, '2019-11-12 06:40:18'),
(5166, '', 0.00, 6, '2019-11-12 06:40:19'),
(5167, '', 0.00, 6, '2019-11-12 06:40:19'),
(5168, '', 0.00, 6, '2019-11-12 06:40:19'),
(5169, '', 0.00, 6, '2019-11-12 06:40:19'),
(5170, '', 0.00, 6, '2019-11-12 06:40:19'),
(5171, '', 0.00, 6, '2019-11-12 06:40:19'),
(5172, '', 0.00, 6, '2019-11-12 06:40:19'),
(5173, '', 0.00, 6, '2019-11-12 06:40:19'),
(5174, '', 0.00, 6, '2019-11-12 06:40:19'),
(5175, '', 0.00, 6, '2019-11-12 06:40:19'),
(5176, '', 0.00, 6, '2019-11-12 06:40:19'),
(5177, '', 0.00, 6, '2019-11-12 06:40:19'),
(5178, '', 0.00, 6, '2019-11-12 06:40:19'),
(5179, '', 0.00, 6, '2019-11-12 06:40:19'),
(5180, '', 0.00, 6, '2019-11-12 06:40:19'),
(5181, '', 0.00, 6, '2019-11-12 06:40:19'),
(5182, '', 0.00, 6, '2019-11-12 06:40:19'),
(5183, '', 0.00, 6, '2019-11-12 06:40:19'),
(5184, '', 0.00, 6, '2019-11-12 06:40:19'),
(5185, '', 0.00, 6, '2019-11-12 06:40:19'),
(5186, '', 0.00, 6, '2019-11-12 06:40:19'),
(5187, '', 0.00, 6, '2019-11-12 06:40:19'),
(5188, '', 0.00, 6, '2019-11-12 06:40:20'),
(5189, '', 0.00, 6, '2019-11-12 06:40:20'),
(5190, '', 0.00, 6, '2019-11-12 06:40:20'),
(5191, '', 0.00, 6, '2019-11-12 06:40:20'),
(5192, '', 0.00, 6, '2019-11-12 06:40:20'),
(5193, '', 0.00, 6, '2019-11-12 06:40:20'),
(5194, '', 0.00, 6, '2019-11-12 06:40:20'),
(5195, '', 0.00, 6, '2019-11-12 06:40:20'),
(5196, '', 0.00, 6, '2019-11-12 06:40:20'),
(5197, '', 0.00, 6, '2019-11-12 06:40:20'),
(5198, '', 0.00, 6, '2019-11-12 06:40:20'),
(5199, '', 0.00, 6, '2019-11-12 06:40:20'),
(5200, '', 0.00, 6, '2019-11-12 06:40:20'),
(5201, '', 0.00, 6, '2019-11-12 06:40:20'),
(5202, '', 0.00, 6, '2019-11-12 06:40:20'),
(5203, '', 0.00, 6, '2019-11-12 06:40:20'),
(5204, '', 0.00, 6, '2019-11-12 06:40:20'),
(5205, '', 0.00, 6, '2019-11-12 06:40:20'),
(5206, '', 0.00, 6, '2019-11-12 06:40:21'),
(5207, '', 0.00, 6, '2019-11-12 06:40:21'),
(5208, '', 0.00, 6, '2019-11-12 06:40:21'),
(5209, '', 0.00, 6, '2019-11-12 06:40:21'),
(5210, '', 0.00, 6, '2019-11-12 06:40:21'),
(5211, '', 0.00, 6, '2019-11-12 06:40:21'),
(5212, '', 0.00, 6, '2019-11-12 06:40:21'),
(5213, '', 0.00, 6, '2019-11-12 06:40:21'),
(5214, '', 0.00, 6, '2019-11-12 06:40:21'),
(5215, '', 0.00, 6, '2019-11-12 06:40:21'),
(5216, '', 0.00, 6, '2019-11-12 06:40:21'),
(5217, '', 0.00, 6, '2019-11-12 06:40:21'),
(5218, '', 0.00, 6, '2019-11-12 06:40:21'),
(5219, '', 0.00, 6, '2019-11-12 06:40:21'),
(5220, '', 0.00, 6, '2019-11-12 06:40:21'),
(5221, '', 0.00, 6, '2019-11-12 06:40:22'),
(5222, '', 0.00, 6, '2019-11-12 06:40:22'),
(5223, '', 0.00, 6, '2019-11-12 06:40:22'),
(5224, '', 0.00, 6, '2019-11-12 06:40:22'),
(5225, '', 0.00, 6, '2019-11-12 06:40:22'),
(5226, '', 0.00, 6, '2019-11-12 06:40:22'),
(5227, '', 0.00, 6, '2019-11-12 06:40:22'),
(5228, '', 0.00, 6, '2019-11-12 06:40:22'),
(5229, '', 0.00, 6, '2019-11-12 06:40:22'),
(5230, '', 0.00, 6, '2019-11-12 06:40:22'),
(5231, '', 0.00, 6, '2019-11-12 06:40:22'),
(5232, '', 0.00, 6, '2019-11-12 06:40:22'),
(5233, '', 0.00, 6, '2019-11-12 06:40:22'),
(5234, '', 0.00, 6, '2019-11-12 06:40:22'),
(5235, '', 0.00, 6, '2019-11-12 06:40:22'),
(5236, '', 0.00, 6, '2019-11-12 06:40:22'),
(5237, '', 0.00, 6, '2019-11-12 06:40:22'),
(5238, '', 0.00, 6, '2019-11-12 06:40:23'),
(5239, '', 0.00, 6, '2019-11-12 06:40:23'),
(5240, '', 0.00, 6, '2019-11-12 06:40:23'),
(5241, '', 0.00, 6, '2019-11-12 06:40:23'),
(5242, '', 0.00, 6, '2019-11-12 06:40:23'),
(5243, '', 0.00, 6, '2019-11-12 06:40:23'),
(5244, '', 0.00, 6, '2019-11-12 06:40:23'),
(5245, '', 0.00, 6, '2019-11-12 06:40:23'),
(5246, '', 0.00, 6, '2019-11-12 06:40:23'),
(5247, '', 0.00, 6, '2019-11-12 06:40:23'),
(5248, '', 0.00, 6, '2019-11-12 06:40:23'),
(5249, '', 0.00, 6, '2019-11-12 06:40:23'),
(5250, '', 0.00, 6, '2019-11-12 06:40:23'),
(5251, '', 0.00, 6, '2019-11-12 06:40:23'),
(5252, '', 0.00, 6, '2019-11-12 06:40:23'),
(5253, '', 0.00, 6, '2019-11-12 06:40:24'),
(5254, '', 0.00, 6, '2019-11-12 06:40:24'),
(5255, '', 0.00, 6, '2019-11-12 06:40:24'),
(5256, '', 0.00, 6, '2019-11-12 06:40:24'),
(5257, '', 0.00, 6, '2019-11-12 06:40:24'),
(5258, '', 0.00, 6, '2019-11-12 06:40:24'),
(5259, '', 0.00, 6, '2019-11-12 06:40:24'),
(5260, '', 0.00, 6, '2019-11-12 06:40:24'),
(5261, '', 0.00, 6, '2019-11-12 06:40:24'),
(5262, '', 0.00, 6, '2019-11-12 06:40:24'),
(5263, '', 0.00, 6, '2019-11-12 06:40:24'),
(5264, '', 0.00, 6, '2019-11-12 06:40:25'),
(5265, '', 0.00, 6, '2019-11-12 06:40:25'),
(5266, '', 0.00, 6, '2019-11-12 06:40:25'),
(5267, '', 0.00, 6, '2019-11-12 06:40:25'),
(5268, '', 0.00, 6, '2019-11-12 06:40:25'),
(5269, '', 0.00, 6, '2019-11-12 06:40:25'),
(5270, '', 0.00, 6, '2019-11-12 06:40:25'),
(5271, '', 0.00, 6, '2019-11-12 06:40:25'),
(5272, '', 0.00, 6, '2019-11-12 06:40:25'),
(5273, '', 0.00, 6, '2019-11-12 06:40:25'),
(5274, '', 0.00, 6, '2019-11-12 06:40:25');
INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(5275, '', 0.00, 6, '2019-11-12 06:40:25'),
(5276, '', 0.00, 6, '2019-11-12 06:40:25'),
(5277, '', 0.00, 6, '2019-11-12 06:40:25'),
(5278, '', 0.00, 6, '2019-11-12 06:40:25'),
(5279, '', 0.00, 6, '2019-11-12 06:40:25'),
(5280, '', 0.00, 6, '2019-11-12 06:40:25'),
(5281, '', 0.00, 6, '2019-11-12 06:40:25'),
(5282, '', 0.00, 6, '2019-11-12 06:40:25'),
(5283, '', 0.00, 6, '2019-11-12 06:40:25'),
(5284, '', 0.00, 6, '2019-11-12 06:40:25'),
(5285, '', 0.00, 6, '2019-11-12 06:40:26'),
(5286, '', 0.00, 6, '2019-11-12 06:40:26'),
(5287, '', 0.00, 6, '2019-11-12 06:40:26'),
(5288, '', 0.00, 6, '2019-11-12 06:40:26'),
(5289, '', 0.00, 6, '2019-11-12 06:40:26'),
(5290, '', 0.00, 6, '2019-11-12 06:40:26'),
(5291, '', 0.00, 6, '2019-11-12 06:40:26'),
(5292, '', 0.00, 6, '2019-11-12 06:40:26'),
(5293, '', 0.00, 6, '2019-11-12 06:40:26'),
(5294, '', 0.00, 6, '2019-11-12 06:40:26'),
(5295, '', 0.00, 6, '2019-11-12 06:40:26'),
(5296, '', 0.00, 6, '2019-11-12 06:40:26'),
(5297, '', 0.00, 6, '2019-11-12 06:40:26'),
(5298, '', 0.00, 6, '2019-11-12 06:40:26'),
(5299, '', 0.00, 6, '2019-11-12 06:40:26'),
(5300, '', 0.00, 6, '2019-11-12 06:40:26'),
(5301, '', 0.00, 6, '2019-11-12 06:40:26'),
(5302, '', 0.00, 6, '2019-11-12 06:40:26'),
(5303, '', 0.00, 6, '2019-11-12 06:40:26'),
(5304, '', 0.00, 6, '2019-11-12 06:40:26'),
(5305, '', 0.00, 6, '2019-11-12 06:40:27'),
(5306, '', 0.00, 6, '2019-11-12 06:40:27'),
(5307, '', 0.00, 6, '2019-11-12 06:40:27'),
(5308, '', 0.00, 6, '2019-11-12 06:40:27'),
(5309, '', 0.00, 6, '2019-11-12 06:40:27'),
(5310, '', 0.00, 6, '2019-11-12 06:40:27'),
(5311, '', 0.00, 6, '2019-11-12 06:40:27'),
(5312, '', 0.00, 6, '2019-11-12 06:40:27'),
(5313, '', 0.00, 6, '2019-11-12 06:40:27'),
(5314, '', 0.00, 6, '2019-11-12 06:40:27'),
(5315, '', 0.00, 6, '2019-11-12 06:40:27'),
(5316, '', 0.00, 6, '2019-11-12 06:40:28'),
(5317, '', 0.00, 6, '2019-11-12 06:40:28'),
(5318, '', 0.00, 6, '2019-11-12 06:40:28'),
(5319, '', 0.00, 6, '2019-11-12 06:40:28'),
(5320, '', 0.00, 6, '2019-11-12 06:40:28'),
(5321, '', 0.00, 6, '2019-11-12 06:40:28'),
(5322, '', 0.00, 6, '2019-11-12 06:40:28'),
(5323, '', 0.00, 6, '2019-11-12 06:40:28'),
(5324, '', 0.00, 6, '2019-11-12 06:40:28'),
(5325, '', 0.00, 6, '2019-11-12 06:40:28'),
(5326, '', 0.00, 6, '2019-11-12 06:40:28'),
(5327, '', 0.00, 6, '2019-11-12 06:40:28'),
(5328, '', 0.00, 6, '2019-11-12 06:40:28'),
(5329, '', 0.00, 6, '2019-11-12 06:40:29'),
(5330, '', 0.00, 6, '2019-11-12 06:40:29'),
(5331, '', 0.00, 6, '2019-11-12 06:40:29'),
(5332, '', 0.00, 6, '2019-11-12 06:40:29'),
(5333, '', 0.00, 6, '2019-11-12 06:40:29'),
(5334, '', 0.00, 6, '2019-11-12 06:40:29'),
(5335, '', 0.00, 6, '2019-11-12 06:40:29'),
(5336, '', 0.00, 6, '2019-11-12 06:40:29'),
(5337, '', 0.00, 6, '2019-11-12 06:40:29'),
(5338, '', 0.00, 6, '2019-11-12 06:40:29'),
(5339, '', 0.00, 6, '2019-11-12 06:40:29'),
(5340, '', 0.00, 6, '2019-11-12 06:40:29'),
(5341, '', 0.00, 6, '2019-11-12 06:40:29'),
(5342, '', 0.00, 6, '2019-11-12 06:40:29'),
(5343, '', 0.00, 6, '2019-11-12 06:40:29'),
(5344, '', 0.00, 6, '2019-11-12 06:40:29'),
(5345, '', 0.00, 6, '2019-11-12 06:40:29'),
(5346, '', 0.00, 6, '2019-11-12 06:40:29'),
(5347, '', 0.00, 6, '2019-11-12 06:40:29'),
(5348, '', 0.00, 6, '2019-11-12 06:40:30'),
(5349, '', 0.00, 6, '2019-11-12 06:40:30'),
(5350, '', 0.00, 6, '2019-11-12 06:40:30'),
(5351, '', 0.00, 6, '2019-11-12 06:40:30'),
(5352, '', 0.00, 6, '2019-11-12 06:40:30'),
(5353, '', 0.00, 6, '2019-11-12 06:40:30'),
(5354, '', 0.00, 6, '2019-11-12 06:40:30'),
(5355, '', 0.00, 6, '2019-11-12 06:40:30'),
(5356, '', 0.00, 6, '2019-11-12 06:40:30'),
(5357, '', 0.00, 6, '2019-11-12 06:40:30'),
(5358, '', 0.00, 6, '2019-11-12 06:40:30'),
(5359, '', 0.00, 6, '2019-11-12 06:40:30'),
(5360, '', 0.00, 6, '2019-11-12 06:40:30'),
(5361, '', 0.00, 6, '2019-11-12 06:40:30'),
(5362, '', 0.00, 6, '2019-11-12 06:40:30'),
(5363, '', 0.00, 6, '2019-11-12 06:40:30'),
(5364, '', 0.00, 6, '2019-11-12 06:40:30'),
(5365, '', 0.00, 6, '2019-11-12 06:40:30'),
(5366, '', 0.00, 6, '2019-11-12 06:40:30'),
(5367, '', 0.00, 6, '2019-11-12 06:40:31'),
(5368, '', 0.00, 6, '2019-11-12 06:40:31'),
(5369, '', 0.00, 6, '2019-11-12 06:40:31'),
(5370, '', 0.00, 6, '2019-11-12 06:40:31'),
(5371, '', 0.00, 6, '2019-11-12 06:40:31'),
(5372, '', 0.00, 6, '2019-11-12 06:40:31'),
(5373, '', 0.00, 6, '2019-11-12 06:40:31'),
(5374, '', 0.00, 6, '2019-11-12 06:40:31'),
(5375, '', 0.00, 6, '2019-11-12 06:40:31'),
(5376, '', 0.00, 6, '2019-11-12 06:40:31'),
(5377, '', 0.00, 6, '2019-11-12 06:40:31'),
(5378, '', 0.00, 6, '2019-11-12 06:40:31'),
(5379, '', 0.00, 6, '2019-11-12 06:40:31'),
(5380, '', 0.00, 6, '2019-11-12 06:40:31'),
(5381, '', 0.00, 6, '2019-11-12 06:40:31'),
(5382, '', 0.00, 6, '2019-11-12 06:40:32'),
(5383, '', 0.00, 6, '2019-11-12 06:40:32'),
(5384, '', 0.00, 6, '2019-11-12 06:40:32'),
(5385, '', 0.00, 6, '2019-11-12 06:40:32'),
(5386, '', 0.00, 6, '2019-11-12 06:40:32'),
(5387, '', 0.00, 6, '2019-11-12 06:40:32'),
(5388, '', 0.00, 6, '2019-11-12 06:40:32'),
(5389, '', 0.00, 6, '2019-11-12 06:40:32'),
(5390, '', 0.00, 6, '2019-11-12 06:40:32'),
(5391, '', 0.00, 6, '2019-11-12 06:40:32'),
(5392, '', 0.00, 6, '2019-11-12 06:40:32'),
(5393, '', 0.00, 6, '2019-11-12 06:40:32'),
(5394, '', 0.00, 6, '2019-11-12 06:40:32'),
(5395, '', 0.00, 6, '2019-11-12 06:40:32'),
(5396, '', 0.00, 6, '2019-11-12 06:40:32'),
(5397, '', 0.00, 6, '2019-11-12 06:40:33'),
(5398, '', 0.00, 6, '2019-11-12 06:40:33'),
(5399, '', 0.00, 6, '2019-11-12 06:40:33'),
(5400, '', 0.00, 6, '2019-11-12 06:40:33'),
(5401, '', 0.00, 6, '2019-11-12 06:40:33'),
(5402, '', 0.00, 6, '2019-11-12 06:40:33'),
(5403, '', 0.00, 6, '2019-11-12 06:40:33'),
(5404, '', 0.00, 6, '2019-11-12 06:40:33'),
(5405, '', 0.00, 6, '2019-11-12 06:40:33'),
(5406, '', 0.00, 6, '2019-11-12 06:40:33'),
(5407, '', 0.00, 6, '2019-11-12 06:40:33'),
(5408, '', 0.00, 6, '2019-11-12 06:40:33'),
(5409, '', 0.00, 6, '2019-11-12 06:40:33'),
(5410, '', 0.00, 6, '2019-11-12 06:40:33'),
(5411, '', 0.00, 6, '2019-11-12 06:40:33'),
(5412, '', 0.00, 6, '2019-11-12 06:40:33'),
(5413, '', 0.00, 6, '2019-11-12 06:40:33'),
(5414, '', 0.00, 6, '2019-11-12 06:40:33'),
(5415, '\0\0\0', 0.00, 6, '2019-11-12 06:40:34'),
(5416, '\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:34'),
(5417, '', 0.00, 6, '2019-11-12 06:40:34'),
(5418, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:34'),
(5419, '', 0.00, 6, '2019-11-12 06:40:34'),
(5420, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:34'),
(5421, '\0\0\0', 0.00, 6, '2019-11-12 06:40:34'),
(5422, '', 0.00, 6, '2019-11-12 06:40:34'),
(5423, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:34'),
(5424, '', 0.00, 6, '2019-11-12 06:40:34'),
(5425, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:34'),
(5426, '', 0.00, 6, '2019-11-12 06:40:34'),
(5427, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:34'),
(5428, '', 0.00, 6, '2019-11-12 06:40:34'),
(5429, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:34'),
(5430, '', 0.00, 6, '2019-11-12 06:40:35'),
(5431, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:35'),
(5432, '', 0.00, 6, '2019-11-12 06:40:35'),
(5433, '\0\0\0\0	\0\0~', 0.00, 6, '2019-11-12 06:40:35'),
(5434, '', 0.00, 6, '2019-11-12 06:40:35'),
(5435, '\0	\0\0\0', 0.00, 6, '2019-11-12 06:40:36'),
(5436, '\0\0~', 0.00, 6, '2019-11-12 06:40:36'),
(5437, '', 0.00, 6, '2019-11-12 06:40:37'),
(5438, '\0', 0.00, 6, '2019-11-12 06:40:37'),
(5439, '\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:37'),
(5440, '\0', 0.00, 6, '2019-11-12 06:40:37'),
(5441, '', 0.00, 6, '2019-11-12 06:40:37'),
(5442, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:37'),
(5443, '', 0.00, 6, '2019-11-12 06:40:37'),
(5444, '\0\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:40:38'),
(5445, '', 0.00, 6, '2019-11-12 06:40:38'),
(5446, '\0\r\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:38'),
(5447, '', 0.00, 6, '2019-11-12 06:40:38'),
(5448, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:38'),
(5449, '', 0.00, 6, '2019-11-12 06:40:38'),
(5450, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:38'),
(5451, '', 0.00, 6, '2019-11-12 06:40:38'),
(5452, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:38'),
(5453, '', 0.00, 6, '2019-11-12 06:40:38'),
(5454, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:38'),
(5455, '', 0.00, 6, '2019-11-12 06:40:38'),
(5456, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:38'),
(5457, '', 0.00, 6, '2019-11-12 06:40:38'),
(5458, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:38'),
(5459, '', 0.00, 6, '2019-11-12 06:40:39'),
(5460, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:39'),
(5461, '\0\0\0R', 0.00, 6, '2019-11-12 06:40:39'),
(5462, '', 0.00, 6, '2019-11-12 06:40:39'),
(5463, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:39'),
(5464, '', 0.00, 6, '2019-11-12 06:40:39'),
(5465, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:39'),
(5466, '', 0.00, 6, '2019-11-12 06:40:39'),
(5467, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:39'),
(5468, '', 0.00, 6, '2019-11-12 06:40:39'),
(5469, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:39'),
(5470, '', 0.00, 6, '2019-11-12 06:40:39'),
(5471, '\0\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:40:39'),
(5472, '', 0.00, 6, '2019-11-12 06:40:39'),
(5473, '\0\Z\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:39'),
(5474, '\0\Z\0\0R', 0.00, 6, '2019-11-12 06:40:39'),
(5475, '', 0.00, 6, '2019-11-12 06:40:39'),
(5476, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:39'),
(5477, '', 0.00, 6, '2019-11-12 06:40:39'),
(5478, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:39'),
(5479, '', 0.00, 6, '2019-11-12 06:40:39'),
(5480, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:39'),
(5481, '', 0.00, 6, '2019-11-12 06:40:40'),
(5482, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:40'),
(5483, '', 0.00, 6, '2019-11-12 06:40:40'),
(5484, '\0\0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:40:40'),
(5485, '\0\0\0#\0\0\0 \0\0\0', 0.00, 6, '2019-11-12 06:40:40'),
(5486, '\0 \0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:40:40'),
(5487, '', 0.00, 6, '2019-11-12 06:40:40'),
(5488, '', 0.00, 6, '2019-11-12 06:40:40'),
(5489, '\0&quot;\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:40:40'),
(5490, '', 0.00, 6, '2019-11-12 06:40:40'),
(5491, '\0#\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:40:40'),
(5492, '\0#\0\0', 0.00, 6, '2019-11-12 06:40:40'),
(5493, '', 0.00, 6, '2019-11-12 06:40:40'),
(5494, '\0$\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:40:40'),
(5495, '', 0.00, 6, '2019-11-12 06:40:40'),
(5496, '', 0.00, 6, '2019-11-12 06:40:40'),
(5497, '\0&amp;\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:40:40'),
(5498, '', 0.00, 6, '2019-11-12 06:40:40'),
(5499, '\0\'\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:40:40'),
(5500, '', 0.00, 6, '2019-11-12 06:40:40'),
(5501, '\0(\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:40:40'),
(5502, '', 0.00, 6, '2019-11-12 06:40:40'),
(5503, '\0)\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:40:41'),
(5504, '', 0.00, 6, '2019-11-12 06:40:41'),
(5505, '\0*\0\0\0+\0\0~', 0.00, 6, '2019-11-12 06:40:41'),
(5506, '', 0.00, 6, '2019-11-12 06:40:41'),
(5507, '\0+\0\0\0', 0.00, 6, '2019-11-12 06:40:41'),
(5508, '', 0.00, 6, '2019-11-12 06:40:41'),
(5509, '\0', 0.00, 6, '2019-11-12 06:40:41'),
(5510, '\0', 0.00, 6, '2019-11-12 06:40:41'),
(5511, '\0-\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:40:41'),
(5512, '', 0.00, 6, '2019-11-12 06:40:41'),
(5513, '\0.\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:40:42'),
(5514, '', 0.00, 6, '2019-11-12 06:40:42'),
(5515, '\0/\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:40:42'),
(5516, '', 0.00, 6, '2019-11-12 06:40:42'),
(5517, '', 0.00, 6, '2019-11-12 06:40:42'),
(5518, '\01\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:40:42'),
(5519, '', 0.00, 6, '2019-11-12 06:40:42'),
(5520, '\02\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:40:42'),
(5521, '', 0.00, 6, '2019-11-12 06:40:42'),
(5522, '\03\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:40:42'),
(5523, '', 0.00, 6, '2019-11-12 06:40:42'),
(5524, '\04\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:40:42'),
(5525, '', 0.00, 6, '2019-11-12 06:40:42'),
(5526, '\05\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:40:42'),
(5527, '', 0.00, 6, '2019-11-12 06:40:42'),
(5528, '\06\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:40:42'),
(5529, '', 0.00, 6, '2019-11-12 06:40:42'),
(5530, '\07\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:40:42'),
(5531, '', 0.00, 6, '2019-11-12 06:40:43'),
(5532, '\08\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:40:43'),
(5533, '', 0.00, 6, '2019-11-12 06:40:43'),
(5534, '\09\0\0\0:\0\0~', 0.00, 6, '2019-11-12 06:40:43'),
(5535, '', 0.00, 6, '2019-11-12 06:40:43'),
(5536, '\0:\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:40:43'),
(5537, '', 0.00, 6, '2019-11-12 06:40:43'),
(5538, '\0;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:40:43'),
(5539, '', 0.00, 6, '2019-11-12 06:40:43'),
(5540, '\0&lt;\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:40:43'),
(5541, '', 0.00, 6, '2019-11-12 06:40:43'),
(5542, '\0=\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:40:43'),
(5543, '', 0.00, 6, '2019-11-12 06:40:43'),
(5544, '\0&gt;\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:40:43'),
(5545, '', 0.00, 6, '2019-11-12 06:40:43'),
(5546, '\0?\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:40:43'),
(5547, '\0?\0\02\0\0\0@\0\0\0', 0.00, 6, '2019-11-12 06:40:43'),
(5548, '\0@\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:40:43'),
(5549, '', 0.00, 6, '2019-11-12 06:40:43'),
(5550, '\0A\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:40:43'),
(5551, '', 0.00, 6, '2019-11-12 06:40:44'),
(5552, '\0B\0\0\0C\0\0~', 0.00, 6, '2019-11-12 06:40:44'),
(5553, '\0B\0\0R', 0.00, 6, '2019-11-12 06:40:44'),
(5554, '', 0.00, 6, '2019-11-12 06:40:44'),
(5555, '\0C\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:40:44'),
(5556, '', 0.00, 6, '2019-11-12 06:40:44'),
(5557, '\0D\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:40:44'),
(5558, '', 0.00, 6, '2019-11-12 06:40:44'),
(5559, '\0E\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:40:44'),
(5560, '', 0.00, 6, '2019-11-12 06:40:44'),
(5561, '\0F\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:40:44'),
(5562, '', 0.00, 6, '2019-11-12 06:40:44'),
(5563, '\0G\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:40:44'),
(5564, '', 0.00, 6, '2019-11-12 06:40:44'),
(5565, '\0H\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:40:44'),
(5566, '', 0.00, 6, '2019-11-12 06:40:44'),
(5567, '\0I\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:40:44'),
(5568, '', 0.00, 6, '2019-11-12 06:40:44'),
(5569, '\0J\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:40:44'),
(5570, '', 0.00, 6, '2019-11-12 06:40:44'),
(5571, '\0K\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:40:44'),
(5572, '\0K\0\0S', 0.00, 6, '2019-11-12 06:40:44'),
(5573, '', 0.00, 6, '2019-11-12 06:40:44'),
(5574, '\0L\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:40:45'),
(5575, '', 0.00, 6, '2019-11-12 06:40:45'),
(5576, '\0M\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:40:45'),
(5577, '', 0.00, 6, '2019-11-12 06:40:45'),
(5578, '\0N\0\0\0O\0\0~', 0.00, 6, '2019-11-12 06:40:45'),
(5579, '', 0.00, 6, '2019-11-12 06:40:45'),
(5580, '\0O\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:40:45'),
(5581, '', 0.00, 6, '2019-11-12 06:40:45'),
(5582, '\0P\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:40:45'),
(5583, '\0P\0\0', 0.00, 6, '2019-11-12 06:40:45'),
(5584, '', 0.00, 6, '2019-11-12 06:40:45'),
(5585, '\0Q\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:40:45'),
(5586, '', 0.00, 6, '2019-11-12 06:40:45'),
(5587, '\0R\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:40:45'),
(5588, '', 0.00, 6, '2019-11-12 06:40:45'),
(5589, '\0S\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:40:45'),
(5590, '', 0.00, 6, '2019-11-12 06:40:45'),
(5591, '\0T\0\0\0U\0\0~', 0.00, 6, '2019-11-12 06:40:46'),
(5592, '', 0.00, 6, '2019-11-12 06:40:46'),
(5593, '\0U\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:40:46'),
(5594, '', 0.00, 6, '2019-11-12 06:40:46'),
(5595, '\0V\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:40:46'),
(5596, '', 0.00, 6, '2019-11-12 06:40:46'),
(5597, '\0W\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:40:46'),
(5598, '', 0.00, 6, '2019-11-12 06:40:46'),
(5599, '\0X\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:40:47'),
(5600, '\0\0Ixime', 0.00, 7, '2019-11-12 06:40:47'),
(5601, '', 0.00, 6, '2019-11-12 06:40:47'),
(5602, '\0\0Manix', 0.00, 7, '2019-11-12 06:40:47'),
(5603, '\0Y\0\0\0Z\0\0~', 0.00, 6, '2019-11-12 06:40:47'),
(5604, '\0\0Pelox', 0.00, 7, '2019-11-12 06:40:47'),
(5605, '', 0.00, 6, '2019-11-12 06:40:47'),
(5606, '\0\0Supracin', 0.00, 7, '2019-11-12 06:40:47'),
(5607, '\0Z\0\0\0[\0\0~', 0.00, 6, '2019-11-12 06:40:47'),
(5608, '\0\0Tetacid ', 0.00, 7, '2019-11-12 06:40:47'),
(5609, '', 0.00, 6, '2019-11-12 06:40:47'),
(5610, '', 0.00, 7, '2019-11-12 06:40:47'),
(5611, '\0[\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:40:47'),
(5612, '\0\0\0\0', 0.00, 7, '2019-11-12 06:40:48'),
(5613, '', 0.00, 6, '2019-11-12 06:40:48'),
(5614, '', 0.00, 7, '2019-11-12 06:40:48'),
(5615, '\0\\\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:40:48'),
(5616, '', 0.00, 7, '2019-11-12 06:40:48'),
(5617, '', 0.00, 6, '2019-11-12 06:40:48'),
(5618, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:48'),
(5619, '\0]\0\0\0^\0\0~', 0.00, 6, '2019-11-12 06:40:48'),
(5620, '', 0.00, 7, '2019-11-12 06:40:48'),
(5621, '', 0.00, 6, '2019-11-12 06:40:48'),
(5622, '', 0.00, 7, '2019-11-12 06:40:48'),
(5623, '\0^\0\0\0_\0\0~', 0.00, 6, '2019-11-12 06:40:48'),
(5624, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:48'),
(5625, '', 0.00, 6, '2019-11-12 06:40:48'),
(5626, '', 0.00, 7, '2019-11-12 06:40:48'),
(5627, '\0_\0\0\0`\0\0\0_\0\0fffff&amp;r@\0`\0\0\0', 0.00, 6, '2019-11-12 06:40:48'),
(5628, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:48'),
(5629, '\0`\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:40:48'),
(5630, '', 0.00, 7, '2019-11-12 06:40:48'),
(5631, '', 0.00, 6, '2019-11-12 06:40:48'),
(5632, '', 0.00, 7, '2019-11-12 06:40:48'),
(5633, '\0a\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:40:48'),
(5634, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:48'),
(5635, '', 0.00, 6, '2019-11-12 06:40:48'),
(5636, '', 0.00, 7, '2019-11-12 06:40:48'),
(5637, '\0b\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:40:48'),
(5638, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:49'),
(5639, '', 0.00, 6, '2019-11-12 06:40:49'),
(5640, '', 0.00, 7, '2019-11-12 06:40:49'),
(5641, '\0c\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:40:49'),
(5642, '\0\0\0\0\0	\0\0\0~', 0.00, 7, '2019-11-12 06:40:49'),
(5643, '', 0.00, 6, '2019-11-12 06:40:49'),
(5644, '', 0.00, 7, '2019-11-12 06:40:49'),
(5645, '\0d\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:40:49'),
(5646, '\0	\0\0\0\0', 0.00, 7, '2019-11-12 06:40:49'),
(5647, '', 0.00, 6, '2019-11-12 06:40:49'),
(5648, '\0\0\0~', 0.00, 7, '2019-11-12 06:40:49'),
(5649, '\0e\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:40:49'),
(5650, '', 0.00, 7, '2019-11-12 06:40:49'),
(5651, '', 0.00, 6, '2019-11-12 06:40:49'),
(5652, '\0', 0.00, 7, '2019-11-12 06:40:49'),
(5653, '\0f\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:40:49'),
(5654, '\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:49'),
(5655, '', 0.00, 6, '2019-11-12 06:40:49'),
(5656, '\0', 0.00, 7, '2019-11-12 06:40:49'),
(5657, '\0g\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:40:49'),
(5658, '', 0.00, 7, '2019-11-12 06:40:49'),
(5659, '', 0.00, 6, '2019-11-12 06:40:49'),
(5660, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:50'),
(5661, '\0h\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:40:50'),
(5662, '\0\0\0\0', 0.00, 7, '2019-11-12 06:40:51'),
(5663, '', 0.00, 6, '2019-11-12 06:40:51'),
(5664, '', 0.00, 7, '2019-11-12 06:40:51'),
(5665, '\0i\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:40:51'),
(5666, '\0\0\0\0\0\r\0\0\0~', 0.00, 7, '2019-11-12 06:40:51'),
(5667, '', 0.00, 6, '2019-11-12 06:40:51'),
(5668, '', 0.00, 7, '2019-11-12 06:40:51'),
(5669, '\0j\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:40:51'),
(5670, '\0\r\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:52'),
(5671, '', 0.00, 6, '2019-11-12 06:40:52'),
(5672, '', 0.00, 7, '2019-11-12 06:40:52'),
(5673, '\0k\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:40:52'),
(5674, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:52'),
(5675, '', 0.00, 6, '2019-11-12 06:40:52'),
(5676, '', 0.00, 7, '2019-11-12 06:40:52'),
(5677, '\0l\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:40:52'),
(5678, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:52'),
(5679, '', 0.00, 6, '2019-11-12 06:40:52'),
(5680, '', 0.00, 7, '2019-11-12 06:40:53'),
(5681, '\0m\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:40:53'),
(5682, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:53'),
(5683, '', 0.00, 6, '2019-11-12 06:40:53'),
(5684, '', 0.00, 7, '2019-11-12 06:40:53'),
(5685, '\0n\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:40:53'),
(5686, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:53'),
(5687, '', 0.00, 6, '2019-11-12 06:40:53'),
(5688, '\0\0\0\0P', 0.00, 7, '2019-11-12 06:40:53'),
(5689, '\0o\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:40:53'),
(5690, '', 0.00, 7, '2019-11-12 06:40:53'),
(5691, '', 0.00, 6, '2019-11-12 06:40:53'),
(5692, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:54'),
(5693, '\0p\0\0\0q\0\0~', 0.00, 6, '2019-11-12 06:40:54'),
(5694, '', 0.00, 7, '2019-11-12 06:40:54'),
(5695, '\0p\0\0R', 0.00, 6, '2019-11-12 06:40:54'),
(5696, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:54'),
(5697, '', 0.00, 6, '2019-11-12 06:40:54'),
(5698, '', 0.00, 7, '2019-11-12 06:40:54'),
(5699, '\0q\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:40:54'),
(5700, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:54'),
(5701, '', 0.00, 6, '2019-11-12 06:40:54'),
(5702, '\0\0\0\0', 0.00, 7, '2019-11-12 06:40:54'),
(5703, '\0r\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:40:54'),
(5704, '', 0.00, 7, '2019-11-12 06:40:54'),
(5705, '', 0.00, 6, '2019-11-12 06:40:54'),
(5706, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:54'),
(5707, '\0s\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:40:54'),
(5708, '', 0.00, 7, '2019-11-12 06:40:54'),
(5709, '', 0.00, 6, '2019-11-12 06:40:54'),
(5710, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:55'),
(5711, '\0t\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:40:55'),
(5712, '', 0.00, 7, '2019-11-12 06:40:55'),
(5713, '', 0.00, 6, '2019-11-12 06:40:55'),
(5714, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:55'),
(5715, '\0u\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:40:55'),
(5716, '', 0.00, 7, '2019-11-12 06:40:55'),
(5717, '', 0.00, 6, '2019-11-12 06:40:55'),
(5718, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:55'),
(5719, '\0v\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:40:55'),
(5720, '', 0.00, 7, '2019-11-12 06:40:55'),
(5721, '', 0.00, 6, '2019-11-12 06:40:56'),
(5722, '\0\0\0\0\0\Z\0\0\0~', 0.00, 7, '2019-11-12 06:40:56'),
(5723, '\0w\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:40:56'),
(5724, '', 0.00, 7, '2019-11-12 06:40:56'),
(5725, '\0w\0\0', 0.00, 6, '2019-11-12 06:40:56'),
(5726, '\0\Z\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:56'),
(5727, '', 0.00, 6, '2019-11-12 06:40:56'),
(5728, '', 0.00, 7, '2019-11-12 06:40:56'),
(5729, '\0x\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:40:56'),
(5730, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:56'),
(5731, '', 0.00, 6, '2019-11-12 06:40:56'),
(5732, '', 0.00, 7, '2019-11-12 06:40:56'),
(5733, '\0y\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:40:56'),
(5734, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:56'),
(5735, '', 0.00, 6, '2019-11-12 06:40:57'),
(5736, '', 0.00, 7, '2019-11-12 06:40:57'),
(5737, '\0z\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:40:57'),
(5738, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:57'),
(5739, '', 0.00, 6, '2019-11-12 06:40:57'),
(5740, '', 0.00, 7, '2019-11-12 06:40:57'),
(5741, '\0{\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:40:57'),
(5742, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:40:57'),
(5743, '', 0.00, 6, '2019-11-12 06:40:57'),
(5744, '', 0.00, 7, '2019-11-12 06:40:57'),
(5745, '\0|\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:40:57'),
(5746, '\0\0\0\0\0 \0\0\0~', 0.00, 7, '2019-11-12 06:40:57'),
(5747, '', 0.00, 6, '2019-11-12 06:40:58'),
(5748, '\0\0\0\0#F\0\0\0 \0\0\0\0', 0.00, 7, '2019-11-12 06:40:58'),
(5749, '\0}\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:40:58'),
(5750, '\0 \0\0\0\0!\0\0\0~', 0.00, 7, '2019-11-12 06:40:58'),
(5751, '', 0.00, 6, '2019-11-12 06:40:59'),
(5752, '', 0.00, 7, '2019-11-12 06:40:59'),
(5753, '\0~\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:40:59'),
(5754, '\0!\0\0\0\0&quot;\0\0\0~', 0.00, 7, '2019-11-12 06:40:59'),
(5755, '', 0.00, 6, '2019-11-12 06:40:59'),
(5756, '', 0.00, 7, '2019-11-12 06:40:59'),
(5757, '', 0.00, 6, '2019-11-12 06:40:59'),
(5758, '\0&quot;\0\0\0\0#\0\0\0~', 0.00, 7, '2019-11-12 06:40:59'),
(5759, '', 0.00, 6, '2019-11-12 06:40:59'),
(5760, '', 0.00, 7, '2019-11-12 06:40:59'),
(5761, '', 0.00, 6, '2019-11-12 06:40:59'),
(5762, '\0#\0\0\0\0$\0\0\0~', 0.00, 7, '2019-11-12 06:40:59'),
(5763, '', 0.00, 6, '2019-11-12 06:40:59'),
(5764, '', 0.00, 7, '2019-11-12 06:40:59'),
(5765, '', 0.00, 6, '2019-11-12 06:40:59'),
(5766, '\0$\0\0\0\0%\0\0\0~', 0.00, 7, '2019-11-12 06:40:59'),
(5767, '', 0.00, 6, '2019-11-12 06:40:59'),
(5768, '', 0.00, 7, '2019-11-12 06:40:59'),
(5769, '', 0.00, 6, '2019-11-12 06:40:59'),
(5770, '\0%\0\0\0\0&amp;\0\0\0~', 0.00, 7, '2019-11-12 06:40:59'),
(5771, '', 0.00, 6, '2019-11-12 06:40:59'),
(5772, '', 0.00, 7, '2019-11-12 06:40:59'),
(5773, '', 0.00, 6, '2019-11-12 06:40:59'),
(5774, '\0&amp;\0\0\0\0\'\0\0\0~', 0.00, 7, '2019-11-12 06:40:59'),
(5775, '', 0.00, 6, '2019-11-12 06:41:00'),
(5776, '', 0.00, 7, '2019-11-12 06:41:00'),
(5777, '', 0.00, 6, '2019-11-12 06:41:00'),
(5778, '\0\'\0\0\0\0(\0\0\0~', 0.00, 7, '2019-11-12 06:41:00'),
(5779, '', 0.00, 6, '2019-11-12 06:41:00'),
(5780, '', 0.00, 7, '2019-11-12 06:41:00'),
(5781, '', 0.00, 6, '2019-11-12 06:41:00'),
(5782, '', 0.00, 7, '2019-11-12 06:41:00'),
(5783, '', 0.00, 6, '2019-11-12 06:41:00'),
(5784, '\0)\0\0\0\0*\0\0\0~', 0.00, 7, '2019-11-12 06:41:00'),
(5785, '', 0.00, 6, '2019-11-12 06:41:00'),
(5786, '', 0.00, 7, '2019-11-12 06:41:00'),
(5787, '', 0.00, 6, '2019-11-12 06:41:00'),
(5788, '\0*\0\0\0\0+\0\0\0~', 0.00, 7, '2019-11-12 06:41:00'),
(5789, '', 0.00, 6, '2019-11-12 06:41:00'),
(5790, '', 0.00, 7, '2019-11-12 06:41:00'),
(5791, '', 0.00, 6, '2019-11-12 06:41:00'),
(5792, '\0+\0\0\0\0', 0.00, 7, '2019-11-12 06:41:00'),
(5793, '', 0.00, 6, '2019-11-12 06:41:01'),
(5794, '', 0.00, 7, '2019-11-12 06:41:01'),
(5795, '', 0.00, 6, '2019-11-12 06:41:01'),
(5796, '\0', 0.00, 7, '2019-11-12 06:41:01'),
(5797, '', 0.00, 6, '2019-11-12 06:41:01'),
(5798, '\0', 0.00, 7, '2019-11-12 06:41:01'),
(5799, '', 0.00, 6, '2019-11-12 06:41:01'),
(5800, '\0-\0\0\0\0.\0\0\0~', 0.00, 7, '2019-11-12 06:41:01'),
(5801, '', 0.00, 6, '2019-11-12 06:41:01'),
(5802, '', 0.00, 7, '2019-11-12 06:41:01'),
(5803, '', 0.00, 6, '2019-11-12 06:41:01'),
(5804, '\0.\0\0\0\0/\0\0\0~', 0.00, 7, '2019-11-12 06:41:01'),
(5805, '', 0.00, 6, '2019-11-12 06:41:01'),
(5806, '', 0.00, 7, '2019-11-12 06:41:01'),
(5807, '', 0.00, 6, '2019-11-12 06:41:01'),
(5808, '\0/\0\0\0\00\0\0\0~', 0.00, 7, '2019-11-12 06:41:01'),
(5809, '', 0.00, 6, '2019-11-12 06:41:01'),
(5810, '', 0.00, 7, '2019-11-12 06:41:01'),
(5811, '', 0.00, 6, '2019-11-12 06:41:02'),
(5812, '\00\0\0\0\01\0\0\0~', 0.00, 7, '2019-11-12 06:41:02'),
(5813, '', 0.00, 6, '2019-11-12 06:41:02'),
(5814, '', 0.00, 7, '2019-11-12 06:41:02'),
(5815, '', 0.00, 6, '2019-11-12 06:41:02'),
(5816, '\01\0\0\0\02\0\0\0~', 0.00, 7, '2019-11-12 06:41:02'),
(5817, '', 0.00, 6, '2019-11-12 06:41:02'),
(5818, '', 0.00, 7, '2019-11-12 06:41:02'),
(5819, '', 0.00, 6, '2019-11-12 06:41:02'),
(5820, '\02\0\0\0\03\0\0\0~', 0.00, 7, '2019-11-12 06:41:02'),
(5821, '', 0.00, 6, '2019-11-12 06:41:03'),
(5822, '', 0.00, 7, '2019-11-12 06:41:03'),
(5823, '', 0.00, 6, '2019-11-12 06:41:03'),
(5824, '\03\0\0\0\04\0\0\0~', 0.00, 7, '2019-11-12 06:41:03'),
(5825, '', 0.00, 6, '2019-11-12 06:41:03'),
(5826, '\03\0\0\0', 0.00, 7, '2019-11-12 06:41:03'),
(5827, '', 0.00, 6, '2019-11-12 06:41:03'),
(5828, '', 0.00, 7, '2019-11-12 06:41:03'),
(5829, '', 0.00, 6, '2019-11-12 06:41:03'),
(5830, '\04\0\0\0\05\0\0\0~', 0.00, 7, '2019-11-12 06:41:03'),
(5831, '', 0.00, 6, '2019-11-12 06:41:03'),
(5832, '', 0.00, 7, '2019-11-12 06:41:03'),
(5833, '', 0.00, 6, '2019-11-12 06:41:03'),
(5834, '\05\0\0\0\06\0\0\0~', 0.00, 7, '2019-11-12 06:41:03'),
(5835, '', 0.00, 6, '2019-11-12 06:41:03'),
(5836, '', 0.00, 7, '2019-11-12 06:41:03'),
(5837, '', 0.00, 6, '2019-11-12 06:41:03'),
(5838, '\06\0\0\0\07\0\0\0~', 0.00, 7, '2019-11-12 06:41:03'),
(5839, '', 0.00, 6, '2019-11-12 06:41:03'),
(5840, '', 0.00, 7, '2019-11-12 06:41:04'),
(5841, '', 0.00, 6, '2019-11-12 06:41:04'),
(5842, '\07\0\0\0\08\0\0\0~', 0.00, 7, '2019-11-12 06:41:04'),
(5843, '', 0.00, 6, '2019-11-12 06:41:04'),
(5844, '', 0.00, 7, '2019-11-12 06:41:04'),
(5845, '', 0.00, 6, '2019-11-12 06:41:04'),
(5846, '', 0.00, 7, '2019-11-12 06:41:04'),
(5847, '', 0.00, 6, '2019-11-12 06:41:04'),
(5848, '\09\0\0\0\0:\0\0\0~', 0.00, 7, '2019-11-12 06:41:04'),
(5849, '', 0.00, 6, '2019-11-12 06:41:04'),
(5850, '', 0.00, 7, '2019-11-12 06:41:04'),
(5851, '', 0.00, 6, '2019-11-12 06:41:04'),
(5852, '\0:\0\0\0\0;\0\0\0~', 0.00, 7, '2019-11-12 06:41:04'),
(5853, '', 0.00, 6, '2019-11-12 06:41:04'),
(5854, '', 0.00, 7, '2019-11-12 06:41:04'),
(5855, '', 0.00, 6, '2019-11-12 06:41:04'),
(5856, '\0;\0\0\0\0&lt;\0\0\0~', 0.00, 7, '2019-11-12 06:41:04'),
(5857, '', 0.00, 6, '2019-11-12 06:41:05'),
(5858, '', 0.00, 7, '2019-11-12 06:41:05'),
(5859, '', 0.00, 6, '2019-11-12 06:41:05'),
(5860, '\0&lt;\0\0\0\0=\0\0\0~', 0.00, 7, '2019-11-12 06:41:05'),
(5861, '', 0.00, 6, '2019-11-12 06:41:05'),
(5862, '', 0.00, 7, '2019-11-12 06:41:05'),
(5863, '', 0.00, 6, '2019-11-12 06:41:05'),
(5864, '\0=\0\0\0\0&gt;\0\0\0~', 0.00, 7, '2019-11-12 06:41:05'),
(5865, '', 0.00, 6, '2019-11-12 06:41:05'),
(5866, '', 0.00, 7, '2019-11-12 06:41:05'),
(5867, '', 0.00, 6, '2019-11-12 06:41:05'),
(5868, '', 0.00, 7, '2019-11-12 06:41:05'),
(5869, '', 0.00, 6, '2019-11-12 06:41:05'),
(5870, '\0?\0\0\0\0@\0\0\0~', 0.00, 7, '2019-11-12 06:41:06'),
(5871, '', 0.00, 6, '2019-11-12 06:41:06'),
(5872, '', 0.00, 7, '2019-11-12 06:41:06'),
(5873, '', 0.00, 6, '2019-11-12 06:41:06'),
(5874, '\0@\0\0\0\0A\0\0\0~', 0.00, 7, '2019-11-12 06:41:06'),
(5875, '', 0.00, 6, '2019-11-12 06:41:06'),
(5876, '', 0.00, 7, '2019-11-12 06:41:06'),
(5877, '', 0.00, 6, '2019-11-12 06:41:06'),
(5878, '\0A\0\0\0\0B\0\0\0~', 0.00, 7, '2019-11-12 06:41:06'),
(5879, '', 0.00, 6, '2019-11-12 06:41:06'),
(5880, '', 0.00, 7, '2019-11-12 06:41:06'),
(5881, '', 0.00, 6, '2019-11-12 06:41:06'),
(5882, '\0B\0\0\0\0C\0\0\0~', 0.00, 7, '2019-11-12 06:41:06'),
(5883, '', 0.00, 6, '2019-11-12 06:41:07'),
(5884, '', 0.00, 7, '2019-11-12 06:41:07'),
(5885, '', 0.00, 6, '2019-11-12 06:41:07'),
(5886, '\0C\0\0\0\0D\0\0\0~', 0.00, 7, '2019-11-12 06:41:07'),
(5887, '', 0.00, 6, '2019-11-12 06:41:07'),
(5888, '', 0.00, 7, '2019-11-12 06:41:07'),
(5889, '', 0.00, 6, '2019-11-12 06:41:07'),
(5890, '\0D\0\0\0\0E\0\0\0~', 0.00, 7, '2019-11-12 06:41:07'),
(5891, '', 0.00, 6, '2019-11-12 06:41:07'),
(5892, '', 0.00, 7, '2019-11-12 06:41:07'),
(5893, '', 0.00, 6, '2019-11-12 06:41:07'),
(5894, '', 0.00, 7, '2019-11-12 06:41:07'),
(5895, '', 0.00, 6, '2019-11-12 06:41:07'),
(5896, '\0F\0\0\0\0G\0\0\0~', 0.00, 7, '2019-11-12 06:41:07'),
(5897, '', 0.00, 6, '2019-11-12 06:41:08'),
(5898, '', 0.00, 7, '2019-11-12 06:41:08'),
(5899, '', 0.00, 6, '2019-11-12 06:41:08'),
(5900, '\0G\0\0\0\0H\0\0\0~', 0.00, 7, '2019-11-12 06:41:08'),
(5901, '', 0.00, 6, '2019-11-12 06:41:08'),
(5902, '', 0.00, 7, '2019-11-12 06:41:08'),
(5903, '', 0.00, 6, '2019-11-12 06:41:08'),
(5904, '', 0.00, 7, '2019-11-12 06:41:08'),
(5905, '', 0.00, 6, '2019-11-12 06:41:08'),
(5906, '\0I\0\0\0\0J\0\0\0~', 0.00, 7, '2019-11-12 06:41:08'),
(5907, '', 0.00, 6, '2019-11-12 06:41:08'),
(5908, '', 0.00, 7, '2019-11-12 06:41:08'),
(5909, '', 0.00, 6, '2019-11-12 06:41:08'),
(5910, '\0J\0\0\0\0K\0\0\0~', 0.00, 7, '2019-11-12 06:41:08'),
(5911, '', 0.00, 6, '2019-11-12 06:41:08'),
(5912, '', 0.00, 7, '2019-11-12 06:41:08'),
(5913, '', 0.00, 6, '2019-11-12 06:41:08'),
(5914, '\0K\0\0\0\0L\0\0\0~', 0.00, 7, '2019-11-12 06:41:08'),
(5915, '', 0.00, 6, '2019-11-12 06:41:08'),
(5916, '', 0.00, 7, '2019-11-12 06:41:08'),
(5917, '', 0.00, 6, '2019-11-12 06:41:08'),
(5918, '\0L\0\0\0\0M\0\0\0~', 0.00, 7, '2019-11-12 06:41:08'),
(5919, '', 0.00, 6, '2019-11-12 06:41:09'),
(5920, '', 0.00, 7, '2019-11-12 06:41:09'),
(5921, '', 0.00, 6, '2019-11-12 06:41:09'),
(5922, '\0M\0\0\0\0N\0\0\0~', 0.00, 7, '2019-11-12 06:41:09'),
(5923, '', 0.00, 6, '2019-11-12 06:41:09'),
(5924, '', 0.00, 7, '2019-11-12 06:41:09'),
(5925, '', 0.00, 6, '2019-11-12 06:41:10'),
(5926, '\0N\0\0\0\0O\0\0\0~', 0.00, 7, '2019-11-12 06:41:10'),
(5927, '', 0.00, 6, '2019-11-12 06:41:10'),
(5928, '', 0.00, 7, '2019-11-12 06:41:10'),
(5929, '', 0.00, 6, '2019-11-12 06:41:10'),
(5930, '\0O\0\0\0\0P\0\0\0~', 0.00, 7, '2019-11-12 06:41:10'),
(5931, '', 0.00, 6, '2019-11-12 06:41:10'),
(5932, '', 0.00, 7, '2019-11-12 06:41:10'),
(5933, '', 0.00, 6, '2019-11-12 06:41:10'),
(5934, '\0P\0\0\0\0Q\0\0\0~', 0.00, 7, '2019-11-12 06:41:10'),
(5935, '', 0.00, 6, '2019-11-12 06:41:10'),
(5936, '', 0.00, 7, '2019-11-12 06:41:10'),
(5937, '', 0.00, 6, '2019-11-12 06:41:10'),
(5938, '\0Q\0\0\0\0R\0\0\0~', 0.00, 7, '2019-11-12 06:41:10'),
(5939, '', 0.00, 6, '2019-11-12 06:41:10'),
(5940, '', 0.00, 7, '2019-11-12 06:41:10'),
(5941, '', 0.00, 6, '2019-11-12 06:41:10'),
(5942, '\0R\0\0\0\0S\0\0\0~', 0.00, 7, '2019-11-12 06:41:10'),
(5943, '', 0.00, 6, '2019-11-12 06:41:10'),
(5944, '', 0.00, 7, '2019-11-12 06:41:10'),
(5945, '', 0.00, 6, '2019-11-12 06:41:10'),
(5946, '\0S\0\0\0\0T\0\0\0~', 0.00, 7, '2019-11-12 06:41:11'),
(5947, '', 0.00, 6, '2019-11-12 06:41:11'),
(5948, '', 0.00, 7, '2019-11-12 06:41:11'),
(5949, '', 0.00, 6, '2019-11-12 06:41:11'),
(5950, '\0T\0\0\0\0U\0\0\0~', 0.00, 7, '2019-11-12 06:41:11'),
(5951, '', 0.00, 6, '2019-11-12 06:41:11'),
(5952, '', 0.00, 7, '2019-11-12 06:41:11'),
(5953, '', 0.00, 6, '2019-11-12 06:41:11'),
(5954, '\0U\0\0\0\0V\0\0\0~', 0.00, 7, '2019-11-12 06:41:11'),
(5955, '', 0.00, 6, '2019-11-12 06:41:11'),
(5956, '', 0.00, 7, '2019-11-12 06:41:11'),
(5957, '', 0.00, 6, '2019-11-12 06:41:11'),
(5958, '\0V\0\0\0\0W\0\0\0~', 0.00, 7, '2019-11-12 06:41:11'),
(5959, '', 0.00, 6, '2019-11-12 06:41:11'),
(5960, '', 0.00, 7, '2019-11-12 06:41:12'),
(5961, '', 0.00, 6, '2019-11-12 06:41:12'),
(5962, '\0W\0\0\0\0X\0\0\0~', 0.00, 7, '2019-11-12 06:41:12'),
(5963, '', 0.00, 6, '2019-11-12 06:41:12'),
(5964, '', 0.00, 7, '2019-11-12 06:41:12'),
(5965, '', 0.00, 6, '2019-11-12 06:41:12'),
(5966, '\0X\0\0\0\0Y\0\0\0~', 0.00, 7, '2019-11-12 06:41:12'),
(5967, '', 0.00, 6, '2019-11-12 06:41:12'),
(5968, '', 0.00, 7, '2019-11-12 06:41:12'),
(5969, '', 0.00, 6, '2019-11-12 06:41:12'),
(5970, '\0Y\0\0\0\0Z\0\0\0~', 0.00, 7, '2019-11-12 06:41:12'),
(5971, '', 0.00, 6, '2019-11-12 06:41:12'),
(5972, '', 0.00, 7, '2019-11-12 06:41:12'),
(5973, '', 0.00, 6, '2019-11-12 06:41:12'),
(5974, '\0Z\0\0\0\0[\0\0\0~', 0.00, 7, '2019-11-12 06:41:12'),
(5975, '', 0.00, 6, '2019-11-12 06:41:12'),
(5976, '', 0.00, 7, '2019-11-12 06:41:12'),
(5977, '', 0.00, 6, '2019-11-12 06:41:12'),
(5978, '\0[\0\0\0\0\\\0\0\0~', 0.00, 7, '2019-11-12 06:41:12'),
(5979, '', 0.00, 6, '2019-11-12 06:41:12'),
(5980, '', 0.00, 7, '2019-11-12 06:41:13'),
(5981, '', 0.00, 6, '2019-11-12 06:41:13'),
(5982, '\0\\\0\0\0\0]\0\0\0~', 0.00, 7, '2019-11-12 06:41:13'),
(5983, '', 0.00, 6, '2019-11-12 06:41:13'),
(5984, '', 0.00, 7, '2019-11-12 06:41:13'),
(5985, '', 0.00, 6, '2019-11-12 06:41:13'),
(5986, '\0]\0\0\0\0^\0\0\0~', 0.00, 7, '2019-11-12 06:41:13'),
(5987, '', 0.00, 6, '2019-11-12 06:41:13'),
(5988, '', 0.00, 7, '2019-11-12 06:41:13'),
(5989, '', 0.00, 6, '2019-11-12 06:41:13'),
(5990, '\0^\0\0\0\0_\0\0\0~', 0.00, 7, '2019-11-12 06:41:13'),
(5991, '', 0.00, 6, '2019-11-12 06:41:13'),
(5992, '', 0.00, 7, '2019-11-12 06:41:13'),
(5993, '', 0.00, 6, '2019-11-12 06:41:13'),
(5994, '\0_\0\0\0\0`\0\0\0~', 0.00, 7, '2019-11-12 06:41:13'),
(5995, '', 0.00, 6, '2019-11-12 06:41:13'),
(5996, '\0_\0\0\0j\r\0\0\0`\0\0\0\0', 0.00, 7, '2019-11-12 06:41:13'),
(5997, '', 0.00, 6, '2019-11-12 06:41:13'),
(5998, '\0`\0\0\0\0a\0\0\0~', 0.00, 7, '2019-11-12 06:41:13'),
(5999, '', 0.00, 6, '2019-11-12 06:41:14'),
(6000, '', 0.00, 7, '2019-11-12 06:41:14'),
(6001, '', 0.00, 6, '2019-11-12 06:41:14'),
(6002, '\0a\0\0\0\0b\0\0\0~', 0.00, 7, '2019-11-12 06:41:14'),
(6003, '', 0.00, 6, '2019-11-12 06:41:14'),
(6004, '\0a\0\0\0S', 0.00, 7, '2019-11-12 06:41:14'),
(6005, '', 0.00, 6, '2019-11-12 06:41:14'),
(6006, '', 0.00, 7, '2019-11-12 06:41:14'),
(6007, '', 0.00, 6, '2019-11-12 06:41:14'),
(6008, '\0b\0\0\0\0c\0\0\0~', 0.00, 7, '2019-11-12 06:41:14'),
(6009, '', 0.00, 6, '2019-11-12 06:41:14'),
(6010, '', 0.00, 7, '2019-11-12 06:41:14'),
(6011, '', 0.00, 6, '2019-11-12 06:41:14'),
(6012, '\0c\0\0\0\0d\0\0\0~', 0.00, 7, '2019-11-12 06:41:14'),
(6013, '', 0.00, 6, '2019-11-12 06:41:14'),
(6014, '', 0.00, 7, '2019-11-12 06:41:14'),
(6015, '', 0.00, 6, '2019-11-12 06:41:14'),
(6016, '', 0.00, 7, '2019-11-12 06:41:14'),
(6017, '', 0.00, 6, '2019-11-12 06:41:14'),
(6018, '\0e\0\0\0\0f\0\0\0~', 0.00, 7, '2019-11-12 06:41:14'),
(6019, '', 0.00, 6, '2019-11-12 06:41:14'),
(6020, '', 0.00, 7, '2019-11-12 06:41:14'),
(6021, '', 0.00, 6, '2019-11-12 06:41:15'),
(6022, '\0f\0\0\0\0g\0\0\0~', 0.00, 7, '2019-11-12 06:41:15'),
(6023, '', 0.00, 6, '2019-11-12 06:41:15'),
(6024, '', 0.00, 7, '2019-11-12 06:41:15'),
(6025, '', 0.00, 6, '2019-11-12 06:41:15'),
(6026, '\0g\0\0\0\0h\0\0\0~', 0.00, 7, '2019-11-12 06:41:15'),
(6027, '', 0.00, 6, '2019-11-12 06:41:15'),
(6028, '', 0.00, 7, '2019-11-12 06:41:15'),
(6029, '', 0.00, 6, '2019-11-12 06:41:15'),
(6030, '\0h\0\0\0\0i\0\0\0~', 0.00, 7, '2019-11-12 06:41:15'),
(6031, '', 0.00, 6, '2019-11-12 06:41:15'),
(6032, '', 0.00, 7, '2019-11-12 06:41:15'),
(6033, '', 0.00, 6, '2019-11-12 06:41:15'),
(6034, '\0i\0\0\0\0j\0\0\0~', 0.00, 7, '2019-11-12 06:41:15'),
(6035, '', 0.00, 6, '2019-11-12 06:41:15'),
(6036, '', 0.00, 7, '2019-11-12 06:41:15'),
(6037, '', 0.00, 6, '2019-11-12 06:41:15'),
(6038, '\0j\0\0\0\0k\0\0\0~', 0.00, 7, '2019-11-12 06:41:15'),
(6039, '', 0.00, 6, '2019-11-12 06:41:16'),
(6040, '', 0.00, 7, '2019-11-12 06:41:16'),
(6041, '', 0.00, 6, '2019-11-12 06:41:16'),
(6042, '\0k\0\0\0\0l\0\0\0~', 0.00, 7, '2019-11-12 06:41:16'),
(6043, '', 0.00, 6, '2019-11-12 06:41:17'),
(6044, '', 0.00, 7, '2019-11-12 06:41:17'),
(6045, '', 0.00, 6, '2019-11-12 06:41:17'),
(6046, '\0l\0\0\0\0m\0\0\0~', 0.00, 7, '2019-11-12 06:41:17'),
(6047, '', 0.00, 6, '2019-11-12 06:41:17'),
(6048, '', 0.00, 7, '2019-11-12 06:41:17'),
(6049, '', 0.00, 6, '2019-11-12 06:41:17'),
(6050, '\0m\0\0\0\0n\0\0\0~', 0.00, 7, '2019-11-12 06:41:17'),
(6051, '', 0.00, 6, '2019-11-12 06:41:17'),
(6052, '', 0.00, 7, '2019-11-12 06:41:17'),
(6053, '', 0.00, 6, '2019-11-12 06:41:17'),
(6054, '', 0.00, 7, '2019-11-12 06:41:17'),
(6055, '', 0.00, 6, '2019-11-12 06:41:17'),
(6056, '\0o\0\0\0\0p\0\0\0~', 0.00, 7, '2019-11-12 06:41:17'),
(6057, '', 0.00, 6, '2019-11-12 06:41:17'),
(6058, '', 0.00, 7, '2019-11-12 06:41:17'),
(6059, '', 0.00, 6, '2019-11-12 06:41:17'),
(6060, '\0p\0\0\0\0q\0\0\0~', 0.00, 7, '2019-11-12 06:41:17'),
(6061, '', 0.00, 6, '2019-11-12 06:41:18'),
(6062, '', 0.00, 7, '2019-11-12 06:41:18'),
(6063, '', 0.00, 6, '2019-11-12 06:41:18'),
(6064, '\0q\0\0\0\0r\0\0\0~', 0.00, 7, '2019-11-12 06:41:18'),
(6065, '', 0.00, 6, '2019-11-12 06:41:18'),
(6066, '\0q\0\0\0', 0.00, 7, '2019-11-12 06:41:18'),
(6067, '', 0.00, 6, '2019-11-12 06:41:18'),
(6068, '', 0.00, 7, '2019-11-12 06:41:18'),
(6069, '', 0.00, 6, '2019-11-12 06:41:18'),
(6070, '\0r\0\0\0\0s\0\0\0~', 0.00, 7, '2019-11-12 06:41:18'),
(6071, '', 0.00, 6, '2019-11-12 06:41:18'),
(6072, '', 0.00, 7, '2019-11-12 06:41:18'),
(6073, '', 0.00, 6, '2019-11-12 06:41:18'),
(6074, '\0s\0\0\0\0t\0\0\0~', 0.00, 7, '2019-11-12 06:41:18'),
(6075, '', 0.00, 6, '2019-11-12 06:41:18'),
(6076, '', 0.00, 7, '2019-11-12 06:41:19'),
(6077, '', 0.00, 6, '2019-11-12 06:41:19'),
(6078, '\0t\0\0\0\0u\0\0\0~', 0.00, 7, '2019-11-12 06:41:19'),
(6079, '', 0.00, 6, '2019-11-12 06:41:19'),
(6080, '', 0.00, 7, '2019-11-12 06:41:19'),
(6081, '', 0.00, 6, '2019-11-12 06:41:19'),
(6082, '\0u\0\0\0\0v\0\0\0~', 0.00, 7, '2019-11-12 06:41:19'),
(6083, '', 0.00, 6, '2019-11-12 06:41:19'),
(6084, '', 0.00, 7, '2019-11-12 06:41:19'),
(6085, '', 0.00, 6, '2019-11-12 06:41:19'),
(6086, '\0v\0\0\0\0w\0\0\0~', 0.00, 7, '2019-11-12 06:41:19'),
(6087, '', 0.00, 6, '2019-11-12 06:41:19'),
(6088, '', 0.00, 7, '2019-11-12 06:41:19'),
(6089, '', 0.00, 6, '2019-11-12 06:41:19'),
(6090, '\0w\0\0\0\0x\0\0\0~', 0.00, 7, '2019-11-12 06:41:19'),
(6091, '', 0.00, 6, '2019-11-12 06:41:19'),
(6092, '', 0.00, 7, '2019-11-12 06:41:19'),
(6093, '', 0.00, 6, '2019-11-12 06:41:19'),
(6094, '\0x\0\0\0\0y\0\0\0~', 0.00, 7, '2019-11-12 06:41:19'),
(6095, '', 0.00, 6, '2019-11-12 06:41:19'),
(6096, '', 0.00, 7, '2019-11-12 06:41:19'),
(6097, '', 0.00, 6, '2019-11-12 06:41:19'),
(6098, '\0y\0\0\0\0z\0\0\0~', 0.00, 7, '2019-11-12 06:41:19'),
(6099, '', 0.00, 6, '2019-11-12 06:41:19'),
(6100, '', 0.00, 7, '2019-11-12 06:41:20'),
(6101, '', 0.00, 6, '2019-11-12 06:41:20'),
(6102, '', 0.00, 7, '2019-11-12 06:41:20'),
(6103, '', 0.00, 6, '2019-11-12 06:41:20'),
(6104, '', 0.00, 7, '2019-11-12 06:41:20'),
(6105, '', 0.00, 6, '2019-11-12 06:41:20'),
(6106, '', 0.00, 7, '2019-11-12 06:41:20'),
(6107, '', 0.00, 6, '2019-11-12 06:41:20'),
(6108, '\0}\0\0\0\0~\0\0\0~', 0.00, 7, '2019-11-12 06:41:20'),
(6109, '', 0.00, 6, '2019-11-12 06:41:21'),
(6110, '', 0.00, 7, '2019-11-12 06:41:21'),
(6111, '', 0.00, 6, '2019-11-12 06:41:21'),
(6112, '', 0.00, 7, '2019-11-12 06:41:21'),
(6113, '', 0.00, 6, '2019-11-12 06:41:21'),
(6114, '', 0.00, 7, '2019-11-12 06:41:21'),
(6115, '', 0.00, 6, '2019-11-12 06:41:21'),
(6116, '', 0.00, 7, '2019-11-12 06:41:21'),
(6117, '', 0.00, 6, '2019-11-12 06:41:21'),
(6118, '', 0.00, 7, '2019-11-12 06:41:22'),
(6119, '', 0.00, 6, '2019-11-12 06:41:22'),
(6120, '', 0.00, 7, '2019-11-12 06:41:22'),
(6121, '', 0.00, 6, '2019-11-12 06:41:22'),
(6122, '', 0.00, 7, '2019-11-12 06:41:22'),
(6123, '', 0.00, 6, '2019-11-12 06:41:22'),
(6124, '', 0.00, 7, '2019-11-12 06:41:22'),
(6125, '', 0.00, 6, '2019-11-12 06:41:22'),
(6126, '', 0.00, 7, '2019-11-12 06:41:22'),
(6127, '', 0.00, 6, '2019-11-12 06:41:22'),
(6128, '', 0.00, 7, '2019-11-12 06:41:23'),
(6129, '', 0.00, 6, '2019-11-12 06:41:23'),
(6130, '', 0.00, 7, '2019-11-12 06:41:23'),
(6131, '', 0.00, 6, '2019-11-12 06:41:23'),
(6132, '', 0.00, 7, '2019-11-12 06:41:23'),
(6133, '', 0.00, 6, '2019-11-12 06:41:23'),
(6134, '', 0.00, 7, '2019-11-12 06:41:23'),
(6135, '', 0.00, 6, '2019-11-12 06:41:23'),
(6136, '', 0.00, 7, '2019-11-12 06:41:23'),
(6137, '', 0.00, 6, '2019-11-12 06:41:23'),
(6138, '', 0.00, 7, '2019-11-12 06:41:23'),
(6139, '', 0.00, 6, '2019-11-12 06:41:23'),
(6140, '', 0.00, 7, '2019-11-12 06:41:23'),
(6141, '', 0.00, 6, '2019-11-12 06:41:24'),
(6142, '', 0.00, 7, '2019-11-12 06:41:24'),
(6143, '', 0.00, 6, '2019-11-12 06:41:24'),
(6144, '', 0.00, 7, '2019-11-12 06:41:24'),
(6145, '', 0.00, 6, '2019-11-12 06:41:24'),
(6146, '', 0.00, 7, '2019-11-12 06:41:24'),
(6147, '', 0.00, 6, '2019-11-12 06:41:24'),
(6148, '', 0.00, 7, '2019-11-12 06:41:24'),
(6149, '', 0.00, 6, '2019-11-12 06:41:24'),
(6150, '', 0.00, 7, '2019-11-12 06:41:24'),
(6151, '', 0.00, 6, '2019-11-12 06:41:24'),
(6152, '', 0.00, 7, '2019-11-12 06:41:24'),
(6153, '', 0.00, 6, '2019-11-12 06:41:24'),
(6154, '', 0.00, 7, '2019-11-12 06:41:24'),
(6155, '', 0.00, 6, '2019-11-12 06:41:24'),
(6156, '', 0.00, 7, '2019-11-12 06:41:24'),
(6157, '', 0.00, 6, '2019-11-12 06:41:24'),
(6158, '', 0.00, 7, '2019-11-12 06:41:25'),
(6159, '', 0.00, 6, '2019-11-12 06:41:25'),
(6160, '', 0.00, 7, '2019-11-12 06:41:25'),
(6161, '', 0.00, 6, '2019-11-12 06:41:25'),
(6162, '', 0.00, 7, '2019-11-12 06:41:25'),
(6163, '', 0.00, 6, '2019-11-12 06:41:25'),
(6164, '', 0.00, 7, '2019-11-12 06:41:25'),
(6165, '', 0.00, 6, '2019-11-12 06:41:25'),
(6166, '', 0.00, 7, '2019-11-12 06:41:25'),
(6167, '', 0.00, 6, '2019-11-12 06:41:25'),
(6168, '', 0.00, 7, '2019-11-12 06:41:25'),
(6169, '', 0.00, 6, '2019-11-12 06:41:25'),
(6170, '', 0.00, 7, '2019-11-12 06:41:25'),
(6171, '', 0.00, 6, '2019-11-12 06:41:25'),
(6172, '', 0.00, 7, '2019-11-12 06:41:25'),
(6173, '', 0.00, 6, '2019-11-12 06:41:25'),
(6174, '', 0.00, 7, '2019-11-12 06:41:25'),
(6175, '', 0.00, 6, '2019-11-12 06:41:25'),
(6176, '', 0.00, 7, '2019-11-12 06:41:25'),
(6177, '', 0.00, 6, '2019-11-12 06:41:25'),
(6178, '', 0.00, 7, '2019-11-12 06:41:25'),
(6179, '', 0.00, 6, '2019-11-12 06:41:25'),
(6180, '', 0.00, 7, '2019-11-12 06:41:25'),
(6181, '', 0.00, 6, '2019-11-12 06:41:26'),
(6182, '', 0.00, 7, '2019-11-12 06:41:26'),
(6183, '', 0.00, 6, '2019-11-12 06:41:26'),
(6184, '', 0.00, 7, '2019-11-12 06:41:26'),
(6185, '', 0.00, 6, '2019-11-12 06:41:26'),
(6186, '', 0.00, 7, '2019-11-12 06:41:26'),
(6187, '', 0.00, 6, '2019-11-12 06:41:26'),
(6188, '', 0.00, 7, '2019-11-12 06:41:26'),
(6189, '', 0.00, 6, '2019-11-12 06:41:26'),
(6190, '', 0.00, 7, '2019-11-12 06:41:26'),
(6191, '', 0.00, 6, '2019-11-12 06:41:27'),
(6192, '', 0.00, 7, '2019-11-12 06:41:27'),
(6193, '', 0.00, 6, '2019-11-12 06:41:27'),
(6194, '', 0.00, 7, '2019-11-12 06:41:27'),
(6195, '', 0.00, 6, '2019-11-12 06:41:27'),
(6196, '', 0.00, 7, '2019-11-12 06:41:27'),
(6197, '', 0.00, 6, '2019-11-12 06:41:27'),
(6198, '', 0.00, 7, '2019-11-12 06:41:27'),
(6199, '', 0.00, 6, '2019-11-12 06:41:27'),
(6200, '', 0.00, 7, '2019-11-12 06:41:27'),
(6201, '', 0.00, 6, '2019-11-12 06:41:27'),
(6202, '', 0.00, 7, '2019-11-12 06:41:27'),
(6203, '', 0.00, 6, '2019-11-12 06:41:27'),
(6204, '', 0.00, 7, '2019-11-12 06:41:27'),
(6205, '', 0.00, 6, '2019-11-12 06:41:27'),
(6206, '', 0.00, 7, '2019-11-12 06:41:27'),
(6207, '', 0.00, 6, '2019-11-12 06:41:27'),
(6208, '', 0.00, 7, '2019-11-12 06:41:27'),
(6209, '', 0.00, 6, '2019-11-12 06:41:27'),
(6210, '', 0.00, 7, '2019-11-12 06:41:27'),
(6211, '', 0.00, 6, '2019-11-12 06:41:27'),
(6212, '', 0.00, 7, '2019-11-12 06:41:28'),
(6213, '', 0.00, 6, '2019-11-12 06:41:28'),
(6214, '', 0.00, 7, '2019-11-12 06:41:28'),
(6215, '', 0.00, 6, '2019-11-12 06:41:28'),
(6216, '', 0.00, 7, '2019-11-12 06:41:28'),
(6217, '', 0.00, 6, '2019-11-12 06:41:28'),
(6218, '', 0.00, 7, '2019-11-12 06:41:28'),
(6219, '', 0.00, 6, '2019-11-12 06:41:28'),
(6220, '', 0.00, 7, '2019-11-12 06:41:28'),
(6221, '', 0.00, 6, '2019-11-12 06:41:28'),
(6222, '', 0.00, 7, '2019-11-12 06:41:29'),
(6223, '', 0.00, 6, '2019-11-12 06:41:29'),
(6224, '', 0.00, 7, '2019-11-12 06:41:29'),
(6225, '', 0.00, 6, '2019-11-12 06:41:29'),
(6226, '', 0.00, 7, '2019-11-12 06:41:29'),
(6227, '', 0.00, 6, '2019-11-12 06:41:29'),
(6228, '', 0.00, 7, '2019-11-12 06:41:29'),
(6229, '', 0.00, 6, '2019-11-12 06:41:29'),
(6230, '', 0.00, 7, '2019-11-12 06:41:29'),
(6231, '', 0.00, 6, '2019-11-12 06:41:29'),
(6232, '', 0.00, 7, '2019-11-12 06:41:29'),
(6233, '', 0.00, 6, '2019-11-12 06:41:29'),
(6234, '', 0.00, 7, '2019-11-12 06:41:29'),
(6235, '', 0.00, 6, '2019-11-12 06:41:29'),
(6236, '', 0.00, 7, '2019-11-12 06:41:29'),
(6237, '', 0.00, 6, '2019-11-12 06:41:29'),
(6238, '', 0.00, 7, '2019-11-12 06:41:29'),
(6239, '', 0.00, 6, '2019-11-12 06:41:29'),
(6240, '', 0.00, 7, '2019-11-12 06:41:29'),
(6241, '', 0.00, 6, '2019-11-12 06:41:29'),
(6242, '', 0.00, 7, '2019-11-12 06:41:29'),
(6243, '', 0.00, 6, '2019-11-12 06:41:29'),
(6244, '', 0.00, 7, '2019-11-12 06:41:30'),
(6245, '', 0.00, 6, '2019-11-12 06:41:30'),
(6246, '', 0.00, 7, '2019-11-12 06:41:30'),
(6247, '', 0.00, 6, '2019-11-12 06:41:30'),
(6248, '', 0.00, 7, '2019-11-12 06:41:30'),
(6249, '', 0.00, 6, '2019-11-12 06:41:30'),
(6250, '', 0.00, 7, '2019-11-12 06:41:30'),
(6251, '', 0.00, 6, '2019-11-12 06:41:30'),
(6252, '', 0.00, 7, '2019-11-12 06:41:30'),
(6253, '', 0.00, 6, '2019-11-12 06:41:30'),
(6254, '', 0.00, 7, '2019-11-12 06:41:30'),
(6255, '', 0.00, 6, '2019-11-12 06:41:30'),
(6256, '', 0.00, 7, '2019-11-12 06:41:30'),
(6257, '', 0.00, 6, '2019-11-12 06:41:30'),
(6258, '', 0.00, 7, '2019-11-12 06:41:30'),
(6259, '', 0.00, 6, '2019-11-12 06:41:30'),
(6260, '', 0.00, 7, '2019-11-12 06:41:30'),
(6261, '', 0.00, 6, '2019-11-12 06:41:30'),
(6262, '', 0.00, 7, '2019-11-12 06:41:30'),
(6263, '', 0.00, 6, '2019-11-12 06:41:30'),
(6264, '', 0.00, 7, '2019-11-12 06:41:30'),
(6265, '', 0.00, 6, '2019-11-12 06:41:30'),
(6266, '', 0.00, 7, '2019-11-12 06:41:31'),
(6267, '', 0.00, 6, '2019-11-12 06:41:31'),
(6268, '', 0.00, 7, '2019-11-12 06:41:31'),
(6269, '', 0.00, 6, '2019-11-12 06:41:31'),
(6270, '', 0.00, 7, '2019-11-12 06:41:31'),
(6271, '', 0.00, 6, '2019-11-12 06:41:31'),
(6272, '', 0.00, 7, '2019-11-12 06:41:32'),
(6273, '', 0.00, 6, '2019-11-12 06:41:32'),
(6274, '', 0.00, 7, '2019-11-12 06:41:32'),
(6275, '', 0.00, 6, '2019-11-12 06:41:32'),
(6276, '', 0.00, 7, '2019-11-12 06:41:32'),
(6277, '', 0.00, 6, '2019-11-12 06:41:32'),
(6278, '', 0.00, 7, '2019-11-12 06:41:32'),
(6279, '', 0.00, 6, '2019-11-12 06:41:32'),
(6280, '', 0.00, 7, '2019-11-12 06:41:32'),
(6281, '\0\0\0', 0.00, 6, '2019-11-12 06:41:32'),
(6282, '', 0.00, 7, '2019-11-12 06:41:32'),
(6283, '\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:32'),
(6284, '', 0.00, 7, '2019-11-12 06:41:32'),
(6285, '\0\0\0\0S', 0.00, 6, '2019-11-12 06:41:32'),
(6286, '', 0.00, 7, '2019-11-12 06:41:33'),
(6287, '', 0.00, 6, '2019-11-12 06:41:33'),
(6288, '', 0.00, 7, '2019-11-12 06:41:33'),
(6289, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:33'),
(6290, '', 0.00, 7, '2019-11-12 06:41:33'),
(6291, '', 0.00, 6, '2019-11-12 06:41:33'),
(6292, '', 0.00, 7, '2019-11-12 06:41:33'),
(6293, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:33'),
(6294, '', 0.00, 7, '2019-11-12 06:41:33'),
(6295, '', 0.00, 6, '2019-11-12 06:41:33'),
(6296, '', 0.00, 7, '2019-11-12 06:41:33'),
(6297, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:33'),
(6298, '', 0.00, 7, '2019-11-12 06:41:33'),
(6299, '', 0.00, 6, '2019-11-12 06:41:33'),
(6300, '', 0.00, 7, '2019-11-12 06:41:33'),
(6301, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:33'),
(6302, '', 0.00, 7, '2019-11-12 06:41:33'),
(6303, '', 0.00, 6, '2019-11-12 06:41:34'),
(6304, '', 0.00, 7, '2019-11-12 06:41:34'),
(6305, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:34'),
(6306, '', 0.00, 7, '2019-11-12 06:41:34'),
(6307, '', 0.00, 6, '2019-11-12 06:41:34'),
(6308, '', 0.00, 7, '2019-11-12 06:41:34'),
(6309, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:34'),
(6310, '', 0.00, 7, '2019-11-12 06:41:34'),
(6311, '\0\0\0R', 0.00, 6, '2019-11-12 06:41:34'),
(6312, '', 0.00, 7, '2019-11-12 06:41:34'),
(6313, '', 0.00, 6, '2019-11-12 06:41:34'),
(6314, '', 0.00, 7, '2019-11-12 06:41:34'),
(6315, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:34'),
(6316, '', 0.00, 7, '2019-11-12 06:41:34'),
(6317, '\0\0\0R', 0.00, 6, '2019-11-12 06:41:34'),
(6318, '', 0.00, 7, '2019-11-12 06:41:34'),
(6319, '', 0.00, 6, '2019-11-12 06:41:34'),
(6320, '', 0.00, 7, '2019-11-12 06:41:34'),
(6321, '\0\0\0\0	\0\0~', 0.00, 6, '2019-11-12 06:41:34'),
(6322, '', 0.00, 7, '2019-11-12 06:41:34'),
(6323, '', 0.00, 6, '2019-11-12 06:41:34'),
(6324, '', 0.00, 7, '2019-11-12 06:41:35'),
(6325, '\0	\0\0\0', 0.00, 6, '2019-11-12 06:41:35'),
(6326, '', 0.00, 7, '2019-11-12 06:41:35'),
(6327, '\0\0~', 0.00, 6, '2019-11-12 06:41:35'),
(6328, '', 0.00, 7, '2019-11-12 06:41:35'),
(6329, '', 0.00, 6, '2019-11-12 06:41:35'),
(6330, '', 0.00, 7, '2019-11-12 06:41:35'),
(6331, '\0', 0.00, 6, '2019-11-12 06:41:35'),
(6332, '', 0.00, 7, '2019-11-12 06:41:35'),
(6333, '\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:35');
INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(6334, '', 0.00, 7, '2019-11-12 06:41:35'),
(6335, '\0', 0.00, 6, '2019-11-12 06:41:35'),
(6336, '', 0.00, 7, '2019-11-12 06:41:35'),
(6337, '', 0.00, 6, '2019-11-12 06:41:35'),
(6338, '', 0.00, 7, '2019-11-12 06:41:35'),
(6339, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:35'),
(6340, '', 0.00, 7, '2019-11-12 06:41:35'),
(6341, '', 0.00, 6, '2019-11-12 06:41:35'),
(6342, '', 0.00, 7, '2019-11-12 06:41:35'),
(6343, '\0\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:41:35'),
(6344, '', 0.00, 7, '2019-11-12 06:41:35'),
(6345, '', 0.00, 6, '2019-11-12 06:41:35'),
(6346, '', 0.00, 7, '2019-11-12 06:41:35'),
(6347, '\0\r\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:36'),
(6348, '', 0.00, 7, '2019-11-12 06:41:36'),
(6349, '', 0.00, 6, '2019-11-12 06:41:36'),
(6350, '', 0.00, 7, '2019-11-12 06:41:36'),
(6351, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:36'),
(6352, '', 0.00, 7, '2019-11-12 06:41:36'),
(6353, '', 0.00, 6, '2019-11-12 06:41:36'),
(6354, '', 0.00, 7, '2019-11-12 06:41:36'),
(6355, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:36'),
(6356, '', 0.00, 7, '2019-11-12 06:41:37'),
(6357, '', 0.00, 6, '2019-11-12 06:41:37'),
(6358, '', 0.00, 7, '2019-11-12 06:41:37'),
(6359, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:37'),
(6360, '', 0.00, 7, '2019-11-12 06:41:37'),
(6361, '', 0.00, 6, '2019-11-12 06:41:37'),
(6362, '', 0.00, 7, '2019-11-12 06:41:37'),
(6363, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:37'),
(6364, '', 0.00, 7, '2019-11-12 06:41:37'),
(6365, '', 0.00, 6, '2019-11-12 06:41:37'),
(6366, '', 0.00, 7, '2019-11-12 06:41:37'),
(6367, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:37'),
(6368, '', 0.00, 7, '2019-11-12 06:41:37'),
(6369, '', 0.00, 6, '2019-11-12 06:41:37'),
(6370, '', 0.00, 7, '2019-11-12 06:41:37'),
(6371, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:37'),
(6372, '', 0.00, 7, '2019-11-12 06:41:37'),
(6373, '', 0.00, 6, '2019-11-12 06:41:37'),
(6374, '', 0.00, 7, '2019-11-12 06:41:37'),
(6375, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:37'),
(6376, '', 0.00, 7, '2019-11-12 06:41:37'),
(6377, '', 0.00, 6, '2019-11-12 06:41:38'),
(6378, '', 0.00, 7, '2019-11-12 06:41:38'),
(6379, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:38'),
(6380, '', 0.00, 7, '2019-11-12 06:41:38'),
(6381, '', 0.00, 6, '2019-11-12 06:41:38'),
(6382, '', 0.00, 7, '2019-11-12 06:41:38'),
(6383, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:38'),
(6384, '', 0.00, 7, '2019-11-12 06:41:38'),
(6385, '', 0.00, 6, '2019-11-12 06:41:38'),
(6386, '', 0.00, 7, '2019-11-12 06:41:38'),
(6387, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:38'),
(6388, '', 0.00, 7, '2019-11-12 06:41:38'),
(6389, '', 0.00, 6, '2019-11-12 06:41:38'),
(6390, '', 0.00, 7, '2019-11-12 06:41:38'),
(6391, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:38'),
(6392, '', 0.00, 7, '2019-11-12 06:41:38'),
(6393, '', 0.00, 6, '2019-11-12 06:41:38'),
(6394, '', 0.00, 7, '2019-11-12 06:41:38'),
(6395, '\0\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:41:38'),
(6396, '', 0.00, 7, '2019-11-12 06:41:39'),
(6397, '', 0.00, 6, '2019-11-12 06:41:39'),
(6398, '', 0.00, 7, '2019-11-12 06:41:39'),
(6399, '\0\Z\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:39'),
(6400, '', 0.00, 7, '2019-11-12 06:41:39'),
(6401, '', 0.00, 6, '2019-11-12 06:41:39'),
(6402, '', 0.00, 7, '2019-11-12 06:41:39'),
(6403, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:39'),
(6404, '', 0.00, 7, '2019-11-12 06:41:39'),
(6405, '', 0.00, 6, '2019-11-12 06:41:39'),
(6406, '', 0.00, 7, '2019-11-12 06:41:39'),
(6407, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:39'),
(6408, '', 0.00, 7, '2019-11-12 06:41:39'),
(6409, '', 0.00, 6, '2019-11-12 06:41:39'),
(6410, '', 0.00, 7, '2019-11-12 06:41:39'),
(6411, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:39'),
(6412, '', 0.00, 7, '2019-11-12 06:41:39'),
(6413, '', 0.00, 6, '2019-11-12 06:41:39'),
(6414, '', 0.00, 7, '2019-11-12 06:41:39'),
(6415, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:41:39'),
(6416, '', 0.00, 7, '2019-11-12 06:41:39'),
(6417, '', 0.00, 6, '2019-11-12 06:41:40'),
(6418, '', 0.00, 7, '2019-11-12 06:41:40'),
(6419, '\0\0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:41:40'),
(6420, '', 0.00, 7, '2019-11-12 06:41:40'),
(6421, '', 0.00, 6, '2019-11-12 06:41:40'),
(6422, '', 0.00, 7, '2019-11-12 06:41:40'),
(6423, '\0 \0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:41:40'),
(6424, '', 0.00, 7, '2019-11-12 06:41:40'),
(6425, '', 0.00, 6, '2019-11-12 06:41:40'),
(6426, '', 0.00, 7, '2019-11-12 06:41:40'),
(6427, '\0!\0\0\0&quot;\0\0~', 0.00, 6, '2019-11-12 06:41:40'),
(6428, '', 0.00, 7, '2019-11-12 06:41:40'),
(6429, '', 0.00, 6, '2019-11-12 06:41:40'),
(6430, '', 0.00, 7, '2019-11-12 06:41:40'),
(6431, '\0&quot;\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:41:40'),
(6432, '', 0.00, 7, '2019-11-12 06:41:40'),
(6433, '', 0.00, 6, '2019-11-12 06:41:40'),
(6434, '', 0.00, 7, '2019-11-12 06:41:40'),
(6435, '\0#\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:41:40'),
(6436, '', 0.00, 7, '2019-11-12 06:41:40'),
(6437, '', 0.00, 6, '2019-11-12 06:41:40'),
(6438, '', 0.00, 7, '2019-11-12 06:41:40'),
(6439, '\0$\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:41:40'),
(6440, '', 0.00, 7, '2019-11-12 06:41:40'),
(6441, '', 0.00, 6, '2019-11-12 06:41:40'),
(6442, '', 0.00, 7, '2019-11-12 06:41:40'),
(6443, '\0%\0\0\0&amp;\0\0~', 0.00, 6, '2019-11-12 06:41:41'),
(6444, '', 0.00, 7, '2019-11-12 06:41:41'),
(6445, '', 0.00, 6, '2019-11-12 06:41:41'),
(6446, '', 0.00, 7, '2019-11-12 06:41:41'),
(6447, '\0&amp;\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:41:41'),
(6448, '', 0.00, 7, '2019-11-12 06:41:41'),
(6449, '', 0.00, 6, '2019-11-12 06:41:41'),
(6450, '', 0.00, 7, '2019-11-12 06:41:41'),
(6451, '\0\'\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:41:41'),
(6452, '', 0.00, 7, '2019-11-12 06:41:41'),
(6453, '\0\'\0\0R', 0.00, 6, '2019-11-12 06:41:41'),
(6454, '', 0.00, 7, '2019-11-12 06:41:41'),
(6455, '', 0.00, 6, '2019-11-12 06:41:41'),
(6456, '', 0.00, 7, '2019-11-12 06:41:41'),
(6457, '\0(\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:41:41'),
(6458, '', 0.00, 7, '2019-11-12 06:41:41'),
(6459, '', 0.00, 6, '2019-11-12 06:41:42'),
(6460, '', 0.00, 7, '2019-11-12 06:41:42'),
(6461, '\0)\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:41:42'),
(6462, '', 0.00, 7, '2019-11-12 06:41:42'),
(6463, '', 0.00, 6, '2019-11-12 06:41:43'),
(6464, '', 0.00, 7, '2019-11-12 06:41:43'),
(6465, '', 0.00, 6, '2019-11-12 06:41:43'),
(6466, '', 0.00, 7, '2019-11-12 06:41:43'),
(6467, '\0+\0\0\0', 0.00, 6, '2019-11-12 06:41:43'),
(6468, '', 0.00, 7, '2019-11-12 06:41:43'),
(6469, '', 0.00, 6, '2019-11-12 06:41:43'),
(6470, '', 0.00, 7, '2019-11-12 06:41:43'),
(6471, '\0', 0.00, 6, '2019-11-12 06:41:43'),
(6472, '', 0.00, 7, '2019-11-12 06:41:43'),
(6473, '\0', 0.00, 6, '2019-11-12 06:41:43'),
(6474, '', 0.00, 7, '2019-11-12 06:41:43'),
(6475, '\0-\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:41:43'),
(6476, '', 0.00, 7, '2019-11-12 06:41:43'),
(6477, '', 0.00, 6, '2019-11-12 06:41:43'),
(6478, '', 0.00, 7, '2019-11-12 06:41:43'),
(6479, '\0.\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:41:43'),
(6480, '', 0.00, 7, '2019-11-12 06:41:43'),
(6481, '', 0.00, 6, '2019-11-12 06:41:43'),
(6482, '', 0.00, 7, '2019-11-12 06:41:44'),
(6483, '\0/\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:41:44'),
(6484, '', 0.00, 7, '2019-11-12 06:41:44'),
(6485, '', 0.00, 6, '2019-11-12 06:41:44'),
(6486, '', 0.00, 7, '2019-11-12 06:41:44'),
(6487, '\00\0\0\01\0\0~', 0.00, 6, '2019-11-12 06:41:44'),
(6488, '', 0.00, 7, '2019-11-12 06:41:44'),
(6489, '', 0.00, 6, '2019-11-12 06:41:44'),
(6490, '', 0.00, 7, '2019-11-12 06:41:44'),
(6491, '\01\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:41:44'),
(6492, '', 0.00, 7, '2019-11-12 06:41:44'),
(6493, '', 0.00, 6, '2019-11-12 06:41:44'),
(6494, '', 0.00, 7, '2019-11-12 06:41:44'),
(6495, '\02\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:41:44'),
(6496, '', 0.00, 7, '2019-11-12 06:41:44'),
(6497, '', 0.00, 6, '2019-11-12 06:41:44'),
(6498, '', 0.00, 7, '2019-11-12 06:41:44'),
(6499, '\03\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:41:44'),
(6500, '', 0.00, 7, '2019-11-12 06:41:44'),
(6501, '', 0.00, 6, '2019-11-12 06:41:44'),
(6502, '', 0.00, 7, '2019-11-12 06:41:44'),
(6503, '\04\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:41:45'),
(6504, '', 0.00, 7, '2019-11-12 06:41:45'),
(6505, '', 0.00, 6, '2019-11-12 06:41:45'),
(6506, '', 0.00, 7, '2019-11-12 06:41:45'),
(6507, '\05\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:41:45'),
(6508, '', 0.00, 7, '2019-11-12 06:41:45'),
(6509, '', 0.00, 6, '2019-11-12 06:41:45'),
(6510, '', 0.00, 7, '2019-11-12 06:41:45'),
(6511, '\06\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:41:45'),
(6512, '', 0.00, 7, '2019-11-12 06:41:45'),
(6513, '', 0.00, 6, '2019-11-12 06:41:45'),
(6514, '', 0.00, 7, '2019-11-12 06:41:45'),
(6515, '\07\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:41:45'),
(6516, '', 0.00, 7, '2019-11-12 06:41:45'),
(6517, '', 0.00, 6, '2019-11-12 06:41:45'),
(6518, '', 0.00, 7, '2019-11-12 06:41:45'),
(6519, '\08\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:41:45'),
(6520, '', 0.00, 7, '2019-11-12 06:41:46'),
(6521, '', 0.00, 6, '2019-11-12 06:41:46'),
(6522, '', 0.00, 7, '2019-11-12 06:41:46'),
(6523, '\09\0\0\0:\0\0~', 0.00, 6, '2019-11-12 06:41:46'),
(6524, '', 0.00, 7, '2019-11-12 06:41:46'),
(6525, '', 0.00, 6, '2019-11-12 06:41:46'),
(6526, '', 0.00, 7, '2019-11-12 06:41:46'),
(6527, '\0:\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:41:46'),
(6528, '', 0.00, 7, '2019-11-12 06:41:46'),
(6529, '', 0.00, 6, '2019-11-12 06:41:46'),
(6530, '', 0.00, 7, '2019-11-12 06:41:46'),
(6531, '\0;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:41:46'),
(6532, '', 0.00, 7, '2019-11-12 06:41:46'),
(6533, '\0;\0\0', 0.00, 6, '2019-11-12 06:41:46'),
(6534, '', 0.00, 7, '2019-11-12 06:41:46'),
(6535, '', 0.00, 6, '2019-11-12 06:41:46'),
(6536, '', 0.00, 7, '2019-11-12 06:41:46'),
(6537, '\0&lt;\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:41:46'),
(6538, '', 0.00, 7, '2019-11-12 06:41:47'),
(6539, '', 0.00, 6, '2019-11-12 06:41:47'),
(6540, '', 0.00, 7, '2019-11-12 06:41:47'),
(6541, '\0=\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:41:47'),
(6542, '', 0.00, 7, '2019-11-12 06:41:47'),
(6543, '', 0.00, 6, '2019-11-12 06:41:47'),
(6544, '', 0.00, 7, '2019-11-12 06:41:47'),
(6545, '\0&gt;\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:41:47'),
(6546, '', 0.00, 7, '2019-11-12 06:41:47'),
(6547, '', 0.00, 6, '2019-11-12 06:41:47'),
(6548, '', 0.00, 7, '2019-11-12 06:41:48'),
(6549, '\0?\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:41:48'),
(6550, '', 0.00, 7, '2019-11-12 06:41:48'),
(6551, '', 0.00, 6, '2019-11-12 06:41:48'),
(6552, '', 0.00, 7, '2019-11-12 06:41:48'),
(6553, '\0@\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:41:48'),
(6554, '', 0.00, 7, '2019-11-12 06:41:48'),
(6555, '', 0.00, 6, '2019-11-12 06:41:48'),
(6556, '', 0.00, 7, '2019-11-12 06:41:48'),
(6557, '\0A\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:41:48'),
(6558, '', 0.00, 7, '2019-11-12 06:41:48'),
(6559, '', 0.00, 6, '2019-11-12 06:41:48'),
(6560, '', 0.00, 7, '2019-11-12 06:41:48'),
(6561, '\0B\0\0\0C\0\0~', 0.00, 6, '2019-11-12 06:41:48'),
(6562, '', 0.00, 7, '2019-11-12 06:41:48'),
(6563, '', 0.00, 6, '2019-11-12 06:41:48'),
(6564, '', 0.00, 7, '2019-11-12 06:41:48'),
(6565, '\0C\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:41:48'),
(6566, '', 0.00, 7, '2019-11-12 06:41:48'),
(6567, '', 0.00, 6, '2019-11-12 06:41:48'),
(6568, '', 0.00, 7, '2019-11-12 06:41:48'),
(6569, '\0D\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:41:48'),
(6570, '', 0.00, 7, '2019-11-12 06:41:48'),
(6571, '', 0.00, 6, '2019-11-12 06:41:48'),
(6572, '', 0.00, 7, '2019-11-12 06:41:48'),
(6573, '\0E\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:41:49'),
(6574, '', 0.00, 7, '2019-11-12 06:41:49'),
(6575, '', 0.00, 6, '2019-11-12 06:41:49'),
(6576, '', 0.00, 7, '2019-11-12 06:41:49'),
(6577, '\0F\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:41:49'),
(6578, '', 0.00, 7, '2019-11-12 06:41:49'),
(6579, '', 0.00, 6, '2019-11-12 06:41:49'),
(6580, '', 0.00, 7, '2019-11-12 06:41:49'),
(6581, '\0G\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:41:49'),
(6582, '', 0.00, 7, '2019-11-12 06:41:49'),
(6583, '', 0.00, 6, '2019-11-12 06:41:49'),
(6584, '', 0.00, 7, '2019-11-12 06:41:49'),
(6585, '\0H\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:41:49'),
(6586, '', 0.00, 7, '2019-11-12 06:41:49'),
(6587, '', 0.00, 6, '2019-11-12 06:41:49'),
(6588, '', 0.00, 7, '2019-11-12 06:41:49'),
(6589, '\0I\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:41:49'),
(6590, '', 0.00, 7, '2019-11-12 06:41:49'),
(6591, '', 0.00, 6, '2019-11-12 06:41:49'),
(6592, '', 0.00, 7, '2019-11-12 06:41:49'),
(6593, '\0J\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:41:50'),
(6594, '', 0.00, 7, '2019-11-12 06:41:50'),
(6595, '', 0.00, 6, '2019-11-12 06:41:50'),
(6596, '', 0.00, 7, '2019-11-12 06:41:50'),
(6597, '\0K\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:41:50'),
(6598, '', 0.00, 7, '2019-11-12 06:41:50'),
(6599, '', 0.00, 6, '2019-11-12 06:41:50'),
(6600, '', 0.00, 7, '2019-11-12 06:41:50'),
(6601, '\0L\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:41:50'),
(6602, '', 0.00, 7, '2019-11-12 06:41:50'),
(6603, '', 0.00, 6, '2019-11-12 06:41:50'),
(6604, '', 0.00, 7, '2019-11-12 06:41:50'),
(6605, '\0M\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:41:50'),
(6606, '', 0.00, 7, '2019-11-12 06:41:50'),
(6607, '', 0.00, 6, '2019-11-12 06:41:50'),
(6608, '', 0.00, 7, '2019-11-12 06:41:50'),
(6609, '', 0.00, 6, '2019-11-12 06:41:50'),
(6610, '', 0.00, 7, '2019-11-12 06:41:50'),
(6611, '\0O\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:41:50'),
(6612, '', 0.00, 7, '2019-11-12 06:41:50'),
(6613, '', 0.00, 6, '2019-11-12 06:41:50'),
(6614, '', 0.00, 7, '2019-11-12 06:41:50'),
(6615, '\0P\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:41:50'),
(6616, '', 0.00, 7, '2019-11-12 06:41:50'),
(6617, '', 0.00, 6, '2019-11-12 06:41:50'),
(6618, '', 0.00, 7, '2019-11-12 06:41:50'),
(6619, '\0Q\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:41:51'),
(6620, '\0\0\0', 0.00, 7, '2019-11-12 06:41:51'),
(6621, '', 0.00, 6, '2019-11-12 06:41:51'),
(6622, '\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:51'),
(6623, '\0R\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:41:51'),
(6624, '', 0.00, 7, '2019-11-12 06:41:51'),
(6625, '', 0.00, 6, '2019-11-12 06:41:51'),
(6626, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:51'),
(6627, '\0S\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:41:51'),
(6628, '', 0.00, 7, '2019-11-12 06:41:51'),
(6629, '', 0.00, 6, '2019-11-12 06:41:51'),
(6630, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:51'),
(6631, '\0T\0\0\0U\0\0~', 0.00, 6, '2019-11-12 06:41:51'),
(6632, '\0\0\0', 0.00, 7, '2019-11-12 06:41:51'),
(6633, '', 0.00, 6, '2019-11-12 06:41:51'),
(6634, '', 0.00, 7, '2019-11-12 06:41:51'),
(6635, '\0U\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:41:51'),
(6636, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:51'),
(6637, '', 0.00, 6, '2019-11-12 06:41:52'),
(6638, '', 0.00, 7, '2019-11-12 06:41:52'),
(6639, '\0V\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:41:52'),
(6640, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:52'),
(6641, '', 0.00, 6, '2019-11-12 06:41:52'),
(6642, '', 0.00, 7, '2019-11-12 06:41:52'),
(6643, '\0W\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:41:52'),
(6644, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:52'),
(6645, '', 0.00, 6, '2019-11-12 06:41:52'),
(6646, '', 0.00, 7, '2019-11-12 06:41:52'),
(6647, '\0X\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:41:52'),
(6648, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:52'),
(6649, '', 0.00, 6, '2019-11-12 06:41:52'),
(6650, '', 0.00, 7, '2019-11-12 06:41:52'),
(6651, '\0Y\0\0\0Z\0\0~', 0.00, 6, '2019-11-12 06:41:52'),
(6652, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:52'),
(6653, '', 0.00, 6, '2019-11-12 06:41:52'),
(6654, '', 0.00, 7, '2019-11-12 06:41:52'),
(6655, '\0Z\0\0\0[\0\0~', 0.00, 6, '2019-11-12 06:41:52'),
(6656, '\0\0\0\0	\0\0~', 0.00, 7, '2019-11-12 06:41:53'),
(6657, '', 0.00, 6, '2019-11-12 06:41:53'),
(6658, '', 0.00, 7, '2019-11-12 06:41:53'),
(6659, '\0[\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:41:53'),
(6660, '\0	\0\0\0', 0.00, 7, '2019-11-12 06:41:53'),
(6661, '', 0.00, 6, '2019-11-12 06:41:53'),
(6662, '\0\0~', 0.00, 7, '2019-11-12 06:41:53'),
(6663, '\0\\\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:41:53'),
(6664, '', 0.00, 7, '2019-11-12 06:41:53'),
(6665, '\0\\\0\0', 0.00, 6, '2019-11-12 06:41:53'),
(6666, '\0', 0.00, 7, '2019-11-12 06:41:53'),
(6667, '', 0.00, 6, '2019-11-12 06:41:53'),
(6668, '\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:54'),
(6669, '\0]\0\0\0^\0\0~', 0.00, 6, '2019-11-12 06:41:54'),
(6670, '\0', 0.00, 7, '2019-11-12 06:41:54'),
(6671, '', 0.00, 6, '2019-11-12 06:41:54'),
(6672, '', 0.00, 7, '2019-11-12 06:41:54'),
(6673, '\0^\0\0\0_\0\0~', 0.00, 6, '2019-11-12 06:41:54'),
(6674, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:54'),
(6675, '', 0.00, 6, '2019-11-12 06:41:54'),
(6676, '', 0.00, 7, '2019-11-12 06:41:54'),
(6677, '\0_\0\0\0`\0\0~', 0.00, 6, '2019-11-12 06:41:54'),
(6678, '\0\0\0\0\r\0\0~', 0.00, 7, '2019-11-12 06:41:54'),
(6679, '\0_\0\0B)\0\0\0`\0\0\0', 0.00, 6, '2019-11-12 06:41:54'),
(6680, '', 0.00, 7, '2019-11-12 06:41:54'),
(6681, '\0`\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:41:54'),
(6682, '\0\r\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:54'),
(6683, '', 0.00, 6, '2019-11-12 06:41:54'),
(6684, '', 0.00, 7, '2019-11-12 06:41:54'),
(6685, '\0a\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:41:54'),
(6686, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:54'),
(6687, '', 0.00, 6, '2019-11-12 06:41:54'),
(6688, '', 0.00, 7, '2019-11-12 06:41:55'),
(6689, '\0b\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:41:55'),
(6690, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:55'),
(6691, '', 0.00, 6, '2019-11-12 06:41:55'),
(6692, '', 0.00, 7, '2019-11-12 06:41:55'),
(6693, '\0c\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:41:55'),
(6694, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:55'),
(6695, '', 0.00, 6, '2019-11-12 06:41:55'),
(6696, '', 0.00, 7, '2019-11-12 06:41:55'),
(6697, '\0d\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:41:55'),
(6698, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:55'),
(6699, '', 0.00, 6, '2019-11-12 06:41:55'),
(6700, '', 0.00, 7, '2019-11-12 06:41:55'),
(6701, '\0e\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:41:55'),
(6702, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:55'),
(6703, '', 0.00, 6, '2019-11-12 06:41:55'),
(6704, '', 0.00, 7, '2019-11-12 06:41:55'),
(6705, '\0f\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:41:55'),
(6706, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:55'),
(6707, '', 0.00, 6, '2019-11-12 06:41:55'),
(6708, '', 0.00, 7, '2019-11-12 06:41:55'),
(6709, '\0g\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:41:55'),
(6710, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:55'),
(6711, '', 0.00, 6, '2019-11-12 06:41:55'),
(6712, '\0\0\0R', 0.00, 7, '2019-11-12 06:41:55'),
(6713, '\0h\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:41:55'),
(6714, '', 0.00, 7, '2019-11-12 06:41:56'),
(6715, '', 0.00, 6, '2019-11-12 06:41:56'),
(6716, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:56'),
(6717, '\0i\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:41:56'),
(6718, '', 0.00, 7, '2019-11-12 06:41:56'),
(6719, '', 0.00, 6, '2019-11-12 06:41:56'),
(6720, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:56'),
(6721, '\0j\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:41:56'),
(6722, '', 0.00, 7, '2019-11-12 06:41:56'),
(6723, '', 0.00, 6, '2019-11-12 06:41:56'),
(6724, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:56'),
(6725, '\0k\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:41:57'),
(6726, '', 0.00, 7, '2019-11-12 06:41:57'),
(6727, '', 0.00, 6, '2019-11-12 06:41:57'),
(6728, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:57'),
(6729, '\0l\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:41:57'),
(6730, '', 0.00, 7, '2019-11-12 06:41:57'),
(6731, '', 0.00, 6, '2019-11-12 06:41:57'),
(6732, '\0\0\0\0\Z\0\0~', 0.00, 7, '2019-11-12 06:41:57'),
(6733, '\0m\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:41:57'),
(6734, '', 0.00, 7, '2019-11-12 06:41:57'),
(6735, '', 0.00, 6, '2019-11-12 06:41:57'),
(6736, '\0\Z\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:57'),
(6737, '\0n\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:41:57'),
(6738, '\0\Z\0\0R', 0.00, 7, '2019-11-12 06:41:58'),
(6739, '', 0.00, 6, '2019-11-12 06:41:58'),
(6740, '', 0.00, 7, '2019-11-12 06:41:58'),
(6741, '\0o\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:41:58'),
(6742, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:58'),
(6743, '', 0.00, 6, '2019-11-12 06:41:58'),
(6744, '', 0.00, 7, '2019-11-12 06:41:58'),
(6745, '\0p\0\0\0q\0\0~', 0.00, 6, '2019-11-12 06:41:58'),
(6746, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:58'),
(6747, '', 0.00, 6, '2019-11-12 06:41:58'),
(6748, '', 0.00, 7, '2019-11-12 06:41:58'),
(6749, '\0q\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:41:58'),
(6750, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:59'),
(6751, '', 0.00, 6, '2019-11-12 06:41:59'),
(6752, '', 0.00, 7, '2019-11-12 06:41:59'),
(6753, '\0r\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:41:59'),
(6754, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:41:59'),
(6755, '', 0.00, 6, '2019-11-12 06:41:59'),
(6756, '', 0.00, 7, '2019-11-12 06:41:59'),
(6757, '\0s\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:41:59'),
(6758, '\0\0\0\0 \0\0~', 0.00, 7, '2019-11-12 06:41:59'),
(6759, '', 0.00, 6, '2019-11-12 06:41:59'),
(6760, '\0\0\0#\0\0\0 \0\0\0', 0.00, 7, '2019-11-12 06:41:59'),
(6761, '\0t\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:41:59'),
(6762, '\0 \0\0\0!\0\0~', 0.00, 7, '2019-11-12 06:41:59'),
(6763, '', 0.00, 6, '2019-11-12 06:41:59'),
(6764, '', 0.00, 7, '2019-11-12 06:41:59'),
(6765, '\0u\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:41:59'),
(6766, '', 0.00, 7, '2019-11-12 06:41:59'),
(6767, '', 0.00, 6, '2019-11-12 06:41:59'),
(6768, '\0&quot;\0\0\0#\0\0~', 0.00, 7, '2019-11-12 06:42:00'),
(6769, '\0v\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:42:00'),
(6770, '', 0.00, 7, '2019-11-12 06:42:00'),
(6771, '', 0.00, 6, '2019-11-12 06:42:00'),
(6772, '\0#\0\0\0$\0\0~', 0.00, 7, '2019-11-12 06:42:00'),
(6773, '\0w\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:42:00'),
(6774, '\0#\0\0', 0.00, 7, '2019-11-12 06:42:00'),
(6775, '', 0.00, 6, '2019-11-12 06:42:00'),
(6776, '', 0.00, 7, '2019-11-12 06:42:00'),
(6777, '\0x\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:42:00'),
(6778, '\0$\0\0\0%\0\0~', 0.00, 7, '2019-11-12 06:42:00'),
(6779, '', 0.00, 6, '2019-11-12 06:42:00'),
(6780, '', 0.00, 7, '2019-11-12 06:42:00'),
(6781, '\0y\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:42:00'),
(6782, '', 0.00, 7, '2019-11-12 06:42:00'),
(6783, '', 0.00, 6, '2019-11-12 06:42:00'),
(6784, '\0&amp;\0\0\0\'\0\0~', 0.00, 7, '2019-11-12 06:42:00'),
(6785, '\0z\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:42:00'),
(6786, '', 0.00, 7, '2019-11-12 06:42:00'),
(6787, '', 0.00, 6, '2019-11-12 06:42:00'),
(6788, '\0\'\0\0\0(\0\0~', 0.00, 7, '2019-11-12 06:42:00'),
(6789, '\0{\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:42:00'),
(6790, '', 0.00, 7, '2019-11-12 06:42:00'),
(6791, '\0{\0\0', 0.00, 6, '2019-11-12 06:42:01'),
(6792, '\0(\0\0\0)\0\0~', 0.00, 7, '2019-11-12 06:42:01'),
(6793, '', 0.00, 6, '2019-11-12 06:42:01'),
(6794, '', 0.00, 7, '2019-11-12 06:42:01'),
(6795, '\0|\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:42:01'),
(6796, '\0)\0\0\0*\0\0~', 0.00, 7, '2019-11-12 06:42:01'),
(6797, '', 0.00, 6, '2019-11-12 06:42:01'),
(6798, '', 0.00, 7, '2019-11-12 06:42:01'),
(6799, '\0}\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:42:01'),
(6800, '\0*\0\0\0+\0\0~', 0.00, 7, '2019-11-12 06:42:01'),
(6801, '', 0.00, 6, '2019-11-12 06:42:01'),
(6802, '', 0.00, 7, '2019-11-12 06:42:01'),
(6803, '\0~\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:01'),
(6804, '\0+\0\0\0', 0.00, 7, '2019-11-12 06:42:01'),
(6805, '', 0.00, 6, '2019-11-12 06:42:01'),
(6806, '', 0.00, 7, '2019-11-12 06:42:01'),
(6807, '', 0.00, 6, '2019-11-12 06:42:02'),
(6808, '\0', 0.00, 7, '2019-11-12 06:42:02'),
(6809, '', 0.00, 6, '2019-11-12 06:42:02'),
(6810, '\0', 0.00, 7, '2019-11-12 06:42:02'),
(6811, '', 0.00, 6, '2019-11-12 06:42:02'),
(6812, '\0-\0\0\0.\0\0~', 0.00, 7, '2019-11-12 06:42:02'),
(6813, '', 0.00, 6, '2019-11-12 06:42:03'),
(6814, '', 0.00, 7, '2019-11-12 06:42:03'),
(6815, '', 0.00, 6, '2019-11-12 06:42:03'),
(6816, '\0.\0\0\0/\0\0~', 0.00, 7, '2019-11-12 06:42:03'),
(6817, '', 0.00, 6, '2019-11-12 06:42:03'),
(6818, '', 0.00, 7, '2019-11-12 06:42:03'),
(6819, '', 0.00, 6, '2019-11-12 06:42:03'),
(6820, '\0/\0\0\00\0\0~', 0.00, 7, '2019-11-12 06:42:03'),
(6821, '', 0.00, 6, '2019-11-12 06:42:03'),
(6822, '', 0.00, 7, '2019-11-12 06:42:03'),
(6823, '', 0.00, 6, '2019-11-12 06:42:03'),
(6824, '', 0.00, 7, '2019-11-12 06:42:03'),
(6825, '', 0.00, 6, '2019-11-12 06:42:03'),
(6826, '\01\0\0\02\0\0~', 0.00, 7, '2019-11-12 06:42:04'),
(6827, '', 0.00, 6, '2019-11-12 06:42:04'),
(6828, '', 0.00, 7, '2019-11-12 06:42:04'),
(6829, '', 0.00, 6, '2019-11-12 06:42:04'),
(6830, '\02\0\0\03\0\0~', 0.00, 7, '2019-11-12 06:42:04'),
(6831, '', 0.00, 6, '2019-11-12 06:42:04'),
(6832, '', 0.00, 7, '2019-11-12 06:42:04'),
(6833, '', 0.00, 6, '2019-11-12 06:42:04'),
(6834, '\03\0\0\04\0\0~', 0.00, 7, '2019-11-12 06:42:04'),
(6835, '', 0.00, 6, '2019-11-12 06:42:04'),
(6836, '', 0.00, 7, '2019-11-12 06:42:04'),
(6837, '', 0.00, 6, '2019-11-12 06:42:04'),
(6838, '\04\0\0\05\0\0~', 0.00, 7, '2019-11-12 06:42:04'),
(6839, '', 0.00, 6, '2019-11-12 06:42:04'),
(6840, '', 0.00, 7, '2019-11-12 06:42:04'),
(6841, '', 0.00, 6, '2019-11-12 06:42:05'),
(6842, '\05\0\0\06\0\0~', 0.00, 7, '2019-11-12 06:42:05'),
(6843, '', 0.00, 6, '2019-11-12 06:42:05'),
(6844, '', 0.00, 7, '2019-11-12 06:42:05'),
(6845, '', 0.00, 6, '2019-11-12 06:42:05'),
(6846, '\06\0\0\07\0\0~', 0.00, 7, '2019-11-12 06:42:05'),
(6847, '', 0.00, 6, '2019-11-12 06:42:05'),
(6848, '', 0.00, 7, '2019-11-12 06:42:06'),
(6849, '', 0.00, 6, '2019-11-12 06:42:06'),
(6850, '\07\0\0\08\0\0~', 0.00, 7, '2019-11-12 06:42:06'),
(6851, '', 0.00, 6, '2019-11-12 06:42:06'),
(6852, '', 0.00, 7, '2019-11-12 06:42:06'),
(6853, '', 0.00, 6, '2019-11-12 06:42:06'),
(6854, '\08\0\0\09\0\0~', 0.00, 7, '2019-11-12 06:42:06'),
(6855, '', 0.00, 6, '2019-11-12 06:42:07'),
(6856, '', 0.00, 7, '2019-11-12 06:42:07'),
(6857, '', 0.00, 6, '2019-11-12 06:42:07'),
(6858, '\09\0\0\0:\0\0~', 0.00, 7, '2019-11-12 06:42:07'),
(6859, '', 0.00, 6, '2019-11-12 06:42:07'),
(6860, '', 0.00, 7, '2019-11-12 06:42:07'),
(6861, '', 0.00, 6, '2019-11-12 06:42:07'),
(6862, '\0:\0\0\0;\0\0~', 0.00, 7, '2019-11-12 06:42:07'),
(6863, '', 0.00, 6, '2019-11-12 06:42:07'),
(6864, '', 0.00, 7, '2019-11-12 06:42:07'),
(6865, '', 0.00, 6, '2019-11-12 06:42:07'),
(6866, '\0;\0\0\0&lt;\0\0~', 0.00, 7, '2019-11-12 06:42:07'),
(6867, '', 0.00, 6, '2019-11-12 06:42:07'),
(6868, '', 0.00, 7, '2019-11-12 06:42:07'),
(6869, '', 0.00, 6, '2019-11-12 06:42:07'),
(6870, '\0&lt;\0\0\0=\0\0~', 0.00, 7, '2019-11-12 06:42:07'),
(6871, '', 0.00, 6, '2019-11-12 06:42:08'),
(6872, '', 0.00, 7, '2019-11-12 06:42:08'),
(6873, '', 0.00, 6, '2019-11-12 06:42:08'),
(6874, '\0=\0\0\0&gt;\0\0~', 0.00, 7, '2019-11-12 06:42:08'),
(6875, '', 0.00, 6, '2019-11-12 06:42:08'),
(6876, '', 0.00, 7, '2019-11-12 06:42:08'),
(6877, '', 0.00, 6, '2019-11-12 06:42:08'),
(6878, '\0&gt;\0\0\0?\0\0~', 0.00, 7, '2019-11-12 06:42:08'),
(6879, '', 0.00, 6, '2019-11-12 06:42:08'),
(6880, '', 0.00, 7, '2019-11-12 06:42:08'),
(6881, '', 0.00, 6, '2019-11-12 06:42:08'),
(6882, '\0?\0\0\0@\0\0~', 0.00, 7, '2019-11-12 06:42:08'),
(6883, '', 0.00, 6, '2019-11-12 06:42:08'),
(6884, '\0?\0\02\0\0\0@\0\0\0', 0.00, 7, '2019-11-12 06:42:08'),
(6885, '', 0.00, 6, '2019-11-12 06:42:08'),
(6886, '\0@\0\0\0A\0\0~', 0.00, 7, '2019-11-12 06:42:09'),
(6887, '', 0.00, 6, '2019-11-12 06:42:09'),
(6888, '', 0.00, 7, '2019-11-12 06:42:09'),
(6889, '', 0.00, 6, '2019-11-12 06:42:09'),
(6890, '\0A\0\0\0B\0\0~', 0.00, 7, '2019-11-12 06:42:09'),
(6891, '', 0.00, 6, '2019-11-12 06:42:09'),
(6892, '', 0.00, 7, '2019-11-12 06:42:09'),
(6893, '', 0.00, 6, '2019-11-12 06:42:09'),
(6894, '\0B\0\0\0C\0\0~', 0.00, 7, '2019-11-12 06:42:09'),
(6895, '', 0.00, 6, '2019-11-12 06:42:09'),
(6896, '\0B\0\0R', 0.00, 7, '2019-11-12 06:42:09'),
(6897, '', 0.00, 6, '2019-11-12 06:42:09'),
(6898, '', 0.00, 7, '2019-11-12 06:42:09'),
(6899, '', 0.00, 6, '2019-11-12 06:42:10'),
(6900, '\0C\0\0\0D\0\0~', 0.00, 7, '2019-11-12 06:42:10'),
(6901, '', 0.00, 6, '2019-11-12 06:42:10'),
(6902, '', 0.00, 7, '2019-11-12 06:42:10'),
(6903, '', 0.00, 6, '2019-11-12 06:42:10'),
(6904, '\0D\0\0\0E\0\0~', 0.00, 7, '2019-11-12 06:42:10'),
(6905, '', 0.00, 6, '2019-11-12 06:42:10'),
(6906, '', 0.00, 7, '2019-11-12 06:42:10'),
(6907, '', 0.00, 6, '2019-11-12 06:42:10'),
(6908, '\0E\0\0\0F\0\0~', 0.00, 7, '2019-11-12 06:42:10'),
(6909, '', 0.00, 6, '2019-11-12 06:42:10'),
(6910, '', 0.00, 7, '2019-11-12 06:42:10'),
(6911, '', 0.00, 6, '2019-11-12 06:42:10'),
(6912, '\0F\0\0\0G\0\0~', 0.00, 7, '2019-11-12 06:42:10'),
(6913, '', 0.00, 6, '2019-11-12 06:42:10'),
(6914, '', 0.00, 7, '2019-11-12 06:42:10'),
(6915, '', 0.00, 6, '2019-11-12 06:42:10'),
(6916, '\0G\0\0\0H\0\0~', 0.00, 7, '2019-11-12 06:42:10'),
(6917, '', 0.00, 6, '2019-11-12 06:42:10'),
(6918, '', 0.00, 7, '2019-11-12 06:42:10'),
(6919, '', 0.00, 6, '2019-11-12 06:42:10'),
(6920, '\0H\0\0\0I\0\0~', 0.00, 7, '2019-11-12 06:42:10'),
(6921, '', 0.00, 6, '2019-11-12 06:42:10'),
(6922, '', 0.00, 7, '2019-11-12 06:42:10'),
(6923, '', 0.00, 6, '2019-11-12 06:42:10'),
(6924, '\0I\0\0\0J\0\0~', 0.00, 7, '2019-11-12 06:42:10'),
(6925, '', 0.00, 6, '2019-11-12 06:42:10'),
(6926, '', 0.00, 7, '2019-11-12 06:42:11'),
(6927, '', 0.00, 6, '2019-11-12 06:42:11'),
(6928, '\0J\0\0\0K\0\0~', 0.00, 7, '2019-11-12 06:42:11'),
(6929, '', 0.00, 6, '2019-11-12 06:42:11'),
(6930, '', 0.00, 7, '2019-11-12 06:42:11'),
(6931, '', 0.00, 6, '2019-11-12 06:42:11'),
(6932, '\0K\0\0\0L\0\0~', 0.00, 7, '2019-11-12 06:42:11'),
(6933, '', 0.00, 6, '2019-11-12 06:42:11'),
(6934, '\0K\0\0S', 0.00, 7, '2019-11-12 06:42:11'),
(6935, '', 0.00, 6, '2019-11-12 06:42:11'),
(6936, '', 0.00, 7, '2019-11-12 06:42:11'),
(6937, '', 0.00, 6, '2019-11-12 06:42:11'),
(6938, '\0L\0\0\0M\0\0~', 0.00, 7, '2019-11-12 06:42:11'),
(6939, '', 0.00, 6, '2019-11-12 06:42:11'),
(6940, '', 0.00, 7, '2019-11-12 06:42:12'),
(6941, '', 0.00, 6, '2019-11-12 06:42:12'),
(6942, '\0M\0\0\0N\0\0~', 0.00, 7, '2019-11-12 06:42:12'),
(6943, '', 0.00, 6, '2019-11-12 06:42:12'),
(6944, '', 0.00, 7, '2019-11-12 06:42:12'),
(6945, '', 0.00, 6, '2019-11-12 06:42:12'),
(6946, '\0N\0\0\0O\0\0~', 0.00, 7, '2019-11-12 06:42:12'),
(6947, '', 0.00, 6, '2019-11-12 06:42:12'),
(6948, '', 0.00, 7, '2019-11-12 06:42:12'),
(6949, '', 0.00, 6, '2019-11-12 06:42:12'),
(6950, '\0O\0\0\0P\0\0~', 0.00, 7, '2019-11-12 06:42:12'),
(6951, '', 0.00, 6, '2019-11-12 06:42:12'),
(6952, '', 0.00, 7, '2019-11-12 06:42:12'),
(6953, '', 0.00, 6, '2019-11-12 06:42:12'),
(6954, '\0P\0\0\0Q\0\0~', 0.00, 7, '2019-11-12 06:42:12'),
(6955, '', 0.00, 6, '2019-11-12 06:42:12'),
(6956, '\0P\0\0', 0.00, 7, '2019-11-12 06:42:12'),
(6957, '', 0.00, 6, '2019-11-12 06:42:12'),
(6958, '', 0.00, 7, '2019-11-12 06:42:13'),
(6959, '', 0.00, 6, '2019-11-12 06:42:13'),
(6960, '\0Q\0\0\0R\0\0~', 0.00, 7, '2019-11-12 06:42:13'),
(6961, '', 0.00, 6, '2019-11-12 06:42:13'),
(6962, '', 0.00, 7, '2019-11-12 06:42:13'),
(6963, '', 0.00, 6, '2019-11-12 06:42:13'),
(6964, '\0R\0\0\0S\0\0~', 0.00, 7, '2019-11-12 06:42:13'),
(6965, '', 0.00, 6, '2019-11-12 06:42:13'),
(6966, '', 0.00, 7, '2019-11-12 06:42:13'),
(6967, '', 0.00, 6, '2019-11-12 06:42:13'),
(6968, '\0S\0\0\0T\0\0~', 0.00, 7, '2019-11-12 06:42:13'),
(6969, '', 0.00, 6, '2019-11-12 06:42:13'),
(6970, '', 0.00, 7, '2019-11-12 06:42:13'),
(6971, '', 0.00, 6, '2019-11-12 06:42:13'),
(6972, '\0T\0\0\0U\0\0~', 0.00, 7, '2019-11-12 06:42:14'),
(6973, '', 0.00, 6, '2019-11-12 06:42:14'),
(6974, '', 0.00, 7, '2019-11-12 06:42:14'),
(6975, '', 0.00, 6, '2019-11-12 06:42:14'),
(6976, '\0U\0\0\0V\0\0~', 0.00, 7, '2019-11-12 06:42:14'),
(6977, '', 0.00, 6, '2019-11-12 06:42:14'),
(6978, '', 0.00, 7, '2019-11-12 06:42:14'),
(6979, '', 0.00, 6, '2019-11-12 06:42:14'),
(6980, '\0V\0\0\0W\0\0~', 0.00, 7, '2019-11-12 06:42:14'),
(6981, '', 0.00, 6, '2019-11-12 06:42:14'),
(6982, '', 0.00, 7, '2019-11-12 06:42:14'),
(6983, '', 0.00, 6, '2019-11-12 06:42:14'),
(6984, '\0W\0\0\0X\0\0~', 0.00, 7, '2019-11-12 06:42:14'),
(6985, '', 0.00, 6, '2019-11-12 06:42:14'),
(6986, '', 0.00, 7, '2019-11-12 06:42:14'),
(6987, '', 0.00, 6, '2019-11-12 06:42:14'),
(6988, '\0X\0\0\0Y\0\0~', 0.00, 7, '2019-11-12 06:42:14'),
(6989, '', 0.00, 6, '2019-11-12 06:42:14'),
(6990, '', 0.00, 7, '2019-11-12 06:42:14'),
(6991, '', 0.00, 6, '2019-11-12 06:42:14'),
(6992, '\0Y\0\0\0Z\0\0~', 0.00, 7, '2019-11-12 06:42:14'),
(6993, '', 0.00, 6, '2019-11-12 06:42:14'),
(6994, '', 0.00, 7, '2019-11-12 06:42:14'),
(6995, '', 0.00, 6, '2019-11-12 06:42:14'),
(6996, '\0Z\0\0\0[\0\0~', 0.00, 7, '2019-11-12 06:42:14'),
(6997, '', 0.00, 6, '2019-11-12 06:42:14'),
(6998, '', 0.00, 7, '2019-11-12 06:42:15'),
(6999, '', 0.00, 6, '2019-11-12 06:42:15'),
(7000, '\0[\0\0\0\\\0\0~', 0.00, 7, '2019-11-12 06:42:15'),
(7001, '', 0.00, 6, '2019-11-12 06:42:15'),
(7002, '', 0.00, 7, '2019-11-12 06:42:15'),
(7003, '', 0.00, 6, '2019-11-12 06:42:15'),
(7004, '\0\\\0\0\0]\0\0~', 0.00, 7, '2019-11-12 06:42:15'),
(7005, '', 0.00, 6, '2019-11-12 06:42:15'),
(7006, '', 0.00, 7, '2019-11-12 06:42:15'),
(7007, '', 0.00, 6, '2019-11-12 06:42:15'),
(7008, '\0]\0\0\0^\0\0~', 0.00, 7, '2019-11-12 06:42:15'),
(7009, '', 0.00, 6, '2019-11-12 06:42:15'),
(7010, '', 0.00, 7, '2019-11-12 06:42:15'),
(7011, '', 0.00, 6, '2019-11-12 06:42:15'),
(7012, '\0^\0\0\0_\0\0~', 0.00, 7, '2019-11-12 06:42:15'),
(7013, '', 0.00, 6, '2019-11-12 06:42:15'),
(7014, '', 0.00, 7, '2019-11-12 06:42:15'),
(7015, '', 0.00, 6, '2019-11-12 06:42:16'),
(7016, '\0_\0\0\0`\0\0\0_\0\0fffff&amp;r@\0`\0\0\0', 0.00, 7, '2019-11-12 06:42:16'),
(7017, '\0`\0\0\0a\0\0~', 0.00, 7, '2019-11-12 06:42:16'),
(7018, '', 0.00, 6, '2019-11-12 06:42:16'),
(7019, '', 0.00, 7, '2019-11-12 06:42:16'),
(7020, '', 0.00, 6, '2019-11-12 06:42:16'),
(7021, '\0a\0\0\0b\0\0~', 0.00, 7, '2019-11-12 06:42:17'),
(7022, '', 0.00, 6, '2019-11-12 06:42:17'),
(7023, '', 0.00, 7, '2019-11-12 06:42:17'),
(7024, '', 0.00, 6, '2019-11-12 06:42:17'),
(7025, '\0b\0\0\0c\0\0~', 0.00, 7, '2019-11-12 06:42:17'),
(7026, '', 0.00, 6, '2019-11-12 06:42:17'),
(7027, '', 0.00, 7, '2019-11-12 06:42:17'),
(7028, '', 0.00, 6, '2019-11-12 06:42:17'),
(7029, '\0c\0\0\0d\0\0~', 0.00, 7, '2019-11-12 06:42:17'),
(7030, '', 0.00, 6, '2019-11-12 06:42:17'),
(7031, '', 0.00, 7, '2019-11-12 06:42:18'),
(7032, '', 0.00, 6, '2019-11-12 06:42:18'),
(7033, '\0d\0\0\0e\0\0~', 0.00, 7, '2019-11-12 06:42:18'),
(7034, '', 0.00, 6, '2019-11-12 06:42:18'),
(7035, '', 0.00, 7, '2019-11-12 06:42:18'),
(7036, '', 0.00, 6, '2019-11-12 06:42:18'),
(7037, '\0e\0\0\0f\0\0~', 0.00, 7, '2019-11-12 06:42:18'),
(7038, '', 0.00, 6, '2019-11-12 06:42:18'),
(7039, '', 0.00, 7, '2019-11-12 06:42:18'),
(7040, '', 0.00, 6, '2019-11-12 06:42:18'),
(7041, '\0f\0\0\0g\0\0~', 0.00, 7, '2019-11-12 06:42:18'),
(7042, '', 0.00, 6, '2019-11-12 06:42:18'),
(7043, '', 0.00, 7, '2019-11-12 06:42:18'),
(7044, '', 0.00, 6, '2019-11-12 06:42:18'),
(7045, '\0g\0\0\0h\0\0~', 0.00, 7, '2019-11-12 06:42:18'),
(7046, '', 0.00, 6, '2019-11-12 06:42:18'),
(7047, '', 0.00, 7, '2019-11-12 06:42:18'),
(7048, '', 0.00, 6, '2019-11-12 06:42:19'),
(7049, '\0h\0\0\0i\0\0~', 0.00, 7, '2019-11-12 06:42:19'),
(7050, '', 0.00, 6, '2019-11-12 06:42:19'),
(7051, '', 0.00, 7, '2019-11-12 06:42:19'),
(7052, '', 0.00, 6, '2019-11-12 06:42:19'),
(7053, '\0i\0\0\0j\0\0~', 0.00, 7, '2019-11-12 06:42:19'),
(7054, '', 0.00, 6, '2019-11-12 06:42:19'),
(7055, '', 0.00, 7, '2019-11-12 06:42:19'),
(7056, '', 0.00, 6, '2019-11-12 06:42:19'),
(7057, '\0j\0\0\0k\0\0~', 0.00, 7, '2019-11-12 06:42:19'),
(7058, '', 0.00, 6, '2019-11-12 06:42:19'),
(7059, '', 0.00, 7, '2019-11-12 06:42:19'),
(7060, '', 0.00, 6, '2019-11-12 06:42:19'),
(7061, '\0k\0\0\0l\0\0~', 0.00, 7, '2019-11-12 06:42:19'),
(7062, '', 0.00, 6, '2019-11-12 06:42:19'),
(7063, '', 0.00, 7, '2019-11-12 06:42:19'),
(7064, '', 0.00, 6, '2019-11-12 06:42:19'),
(7065, '\0l\0\0\0m\0\0~', 0.00, 7, '2019-11-12 06:42:19'),
(7066, '', 0.00, 6, '2019-11-12 06:42:19'),
(7067, '', 0.00, 7, '2019-11-12 06:42:19'),
(7068, '', 0.00, 6, '2019-11-12 06:42:19'),
(7069, '\0m\0\0\0n\0\0~', 0.00, 7, '2019-11-12 06:42:20'),
(7070, '', 0.00, 6, '2019-11-12 06:42:20'),
(7071, '', 0.00, 7, '2019-11-12 06:42:20'),
(7072, '', 0.00, 6, '2019-11-12 06:42:20'),
(7073, '\0n\0\0\0o\0\0~', 0.00, 7, '2019-11-12 06:42:20'),
(7074, '', 0.00, 6, '2019-11-12 06:42:20'),
(7075, '', 0.00, 7, '2019-11-12 06:42:20'),
(7076, '', 0.00, 6, '2019-11-12 06:42:20'),
(7077, '\0o\0\0\0p\0\0~', 0.00, 7, '2019-11-12 06:42:20'),
(7078, '', 0.00, 6, '2019-11-12 06:42:20'),
(7079, '', 0.00, 7, '2019-11-12 06:42:20'),
(7080, '', 0.00, 6, '2019-11-12 06:42:20'),
(7081, '\0p\0\0\0q\0\0~', 0.00, 7, '2019-11-12 06:42:20'),
(7082, '', 0.00, 6, '2019-11-12 06:42:20'),
(7083, '\0p\0\0R', 0.00, 7, '2019-11-12 06:42:20'),
(7084, '', 0.00, 6, '2019-11-12 06:42:20'),
(7085, '', 0.00, 7, '2019-11-12 06:42:20'),
(7086, '', 0.00, 6, '2019-11-12 06:42:20'),
(7087, '\0q\0\0\0r\0\0~', 0.00, 7, '2019-11-12 06:42:20'),
(7088, '', 0.00, 7, '2019-11-12 06:42:20'),
(7089, '', 0.00, 6, '2019-11-12 06:42:20'),
(7090, '', 0.00, 6, '2019-11-12 06:42:20'),
(7091, '\0r\0\0\0s\0\0~', 0.00, 7, '2019-11-12 06:42:20'),
(7092, '', 0.00, 6, '2019-11-12 06:42:20'),
(7093, '', 0.00, 7, '2019-11-12 06:42:20'),
(7094, '\0s\0\0\0t\0\0~', 0.00, 7, '2019-11-12 06:42:20'),
(7095, '', 0.00, 6, '2019-11-12 06:42:20'),
(7096, '', 0.00, 7, '2019-11-12 06:42:20'),
(7097, '', 0.00, 6, '2019-11-12 06:42:21'),
(7098, '\0t\0\0\0u\0\0~', 0.00, 7, '2019-11-12 06:42:21'),
(7099, '', 0.00, 6, '2019-11-12 06:42:21'),
(7100, '', 0.00, 7, '2019-11-12 06:42:21'),
(7101, '', 0.00, 6, '2019-11-12 06:42:21'),
(7102, '\0u\0\0\0v\0\0~', 0.00, 7, '2019-11-12 06:42:21'),
(7103, '', 0.00, 6, '2019-11-12 06:42:21'),
(7104, '', 0.00, 7, '2019-11-12 06:42:21'),
(7105, '', 0.00, 6, '2019-11-12 06:42:21'),
(7106, '\0v\0\0\0w\0\0~', 0.00, 7, '2019-11-12 06:42:21'),
(7107, '', 0.00, 6, '2019-11-12 06:42:21'),
(7108, '', 0.00, 7, '2019-11-12 06:42:21'),
(7109, '', 0.00, 6, '2019-11-12 06:42:21'),
(7110, '\0w\0\0\0x\0\0~', 0.00, 7, '2019-11-12 06:42:21'),
(7111, '', 0.00, 6, '2019-11-12 06:42:21'),
(7112, '\0w\0\0', 0.00, 7, '2019-11-12 06:42:21'),
(7113, '', 0.00, 6, '2019-11-12 06:42:21'),
(7114, '', 0.00, 7, '2019-11-12 06:42:21'),
(7115, '', 0.00, 6, '2019-11-12 06:42:21'),
(7116, '\0x\0\0\0y\0\0~', 0.00, 7, '2019-11-12 06:42:22'),
(7117, '', 0.00, 6, '2019-11-12 06:42:22'),
(7118, '', 0.00, 7, '2019-11-12 06:42:22'),
(7119, '', 0.00, 6, '2019-11-12 06:42:22'),
(7120, '\0y\0\0\0z\0\0~', 0.00, 7, '2019-11-12 06:42:22'),
(7121, '', 0.00, 6, '2019-11-12 06:42:22'),
(7122, '', 0.00, 7, '2019-11-12 06:42:23'),
(7123, '', 0.00, 6, '2019-11-12 06:42:23'),
(7124, '\0z\0\0\0{\0\0~', 0.00, 7, '2019-11-12 06:42:23'),
(7125, '', 0.00, 6, '2019-11-12 06:42:23'),
(7126, '', 0.00, 7, '2019-11-12 06:42:23'),
(7127, '', 0.00, 6, '2019-11-12 06:42:23'),
(7128, '\0{\0\0\0|\0\0~', 0.00, 7, '2019-11-12 06:42:23'),
(7129, '', 0.00, 6, '2019-11-12 06:42:23'),
(7130, '', 0.00, 7, '2019-11-12 06:42:23'),
(7131, '', 0.00, 6, '2019-11-12 06:42:23'),
(7132, '\0|\0\0\0}\0\0~', 0.00, 7, '2019-11-12 06:42:23'),
(7133, '', 0.00, 6, '2019-11-12 06:42:23'),
(7134, '', 0.00, 7, '2019-11-12 06:42:24'),
(7135, '', 0.00, 6, '2019-11-12 06:42:24'),
(7136, '\0}\0\0\0~\0\0~', 0.00, 7, '2019-11-12 06:42:24'),
(7137, '', 0.00, 6, '2019-11-12 06:42:24'),
(7138, '', 0.00, 7, '2019-11-12 06:42:24'),
(7139, '', 0.00, 6, '2019-11-12 06:42:24'),
(7140, '\0~\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:42:24'),
(7141, '', 0.00, 6, '2019-11-12 06:42:24'),
(7142, '', 0.00, 7, '2019-11-12 06:42:24'),
(7143, '', 0.00, 6, '2019-11-12 06:42:24'),
(7144, '', 0.00, 7, '2019-11-12 06:42:24'),
(7145, '', 0.00, 6, '2019-11-12 06:42:24'),
(7146, '', 0.00, 7, '2019-11-12 06:42:24'),
(7147, '', 0.00, 6, '2019-11-12 06:42:24'),
(7148, '', 0.00, 7, '2019-11-12 06:42:24'),
(7149, '', 0.00, 6, '2019-11-12 06:42:24'),
(7150, '', 0.00, 7, '2019-11-12 06:42:24'),
(7151, '', 0.00, 6, '2019-11-12 06:42:24'),
(7152, '', 0.00, 7, '2019-11-12 06:42:24'),
(7153, '', 0.00, 6, '2019-11-12 06:42:24'),
(7154, '', 0.00, 7, '2019-11-12 06:42:24'),
(7155, '', 0.00, 6, '2019-11-12 06:42:24'),
(7156, '', 0.00, 7, '2019-11-12 06:42:25'),
(7157, '', 0.00, 6, '2019-11-12 06:42:25'),
(7158, '', 0.00, 7, '2019-11-12 06:42:25'),
(7159, '', 0.00, 6, '2019-11-12 06:42:25'),
(7160, '', 0.00, 7, '2019-11-12 06:42:25'),
(7161, '', 0.00, 6, '2019-11-12 06:42:25'),
(7162, '', 0.00, 7, '2019-11-12 06:42:25'),
(7163, '', 0.00, 6, '2019-11-12 06:42:25'),
(7164, '', 0.00, 7, '2019-11-12 06:42:25'),
(7165, '', 0.00, 6, '2019-11-12 06:42:25'),
(7166, '', 0.00, 7, '2019-11-12 06:42:25'),
(7167, '', 0.00, 6, '2019-11-12 06:42:25'),
(7168, '', 0.00, 7, '2019-11-12 06:42:25'),
(7169, '', 0.00, 6, '2019-11-12 06:42:26'),
(7170, '', 0.00, 7, '2019-11-12 06:42:26'),
(7171, '', 0.00, 6, '2019-11-12 06:42:26'),
(7172, '', 0.00, 7, '2019-11-12 06:42:26'),
(7173, '', 0.00, 6, '2019-11-12 06:42:26'),
(7174, '', 0.00, 7, '2019-11-12 06:42:26'),
(7175, '', 0.00, 6, '2019-11-12 06:42:26'),
(7176, '', 0.00, 7, '2019-11-12 06:42:26'),
(7177, '', 0.00, 6, '2019-11-12 06:42:26'),
(7178, '', 0.00, 7, '2019-11-12 06:42:26'),
(7179, '', 0.00, 6, '2019-11-12 06:42:26'),
(7180, '', 0.00, 7, '2019-11-12 06:42:26'),
(7181, '', 0.00, 6, '2019-11-12 06:42:26'),
(7182, '', 0.00, 7, '2019-11-12 06:42:26'),
(7183, '', 0.00, 6, '2019-11-12 06:42:26'),
(7184, '', 0.00, 7, '2019-11-12 06:42:26'),
(7185, '', 0.00, 6, '2019-11-12 06:42:27'),
(7186, '', 0.00, 7, '2019-11-12 06:42:27'),
(7187, '', 0.00, 6, '2019-11-12 06:42:27'),
(7188, '', 0.00, 7, '2019-11-12 06:42:27'),
(7189, '', 0.00, 6, '2019-11-12 06:42:27'),
(7190, '', 0.00, 7, '2019-11-12 06:42:28'),
(7191, '', 0.00, 6, '2019-11-12 06:42:28'),
(7192, '', 0.00, 7, '2019-11-12 06:42:28'),
(7193, '', 0.00, 6, '2019-11-12 06:42:28'),
(7194, '', 0.00, 7, '2019-11-12 06:42:28'),
(7195, '', 0.00, 6, '2019-11-12 06:42:29'),
(7196, '', 0.00, 7, '2019-11-12 06:42:29'),
(7197, '', 0.00, 6, '2019-11-12 06:42:29'),
(7198, '', 0.00, 7, '2019-11-12 06:42:29'),
(7199, '', 0.00, 6, '2019-11-12 06:42:29'),
(7200, '', 0.00, 7, '2019-11-12 06:42:29'),
(7201, '', 0.00, 6, '2019-11-12 06:42:29'),
(7202, '', 0.00, 7, '2019-11-12 06:42:29'),
(7203, '', 0.00, 6, '2019-11-12 06:42:29'),
(7204, '', 0.00, 7, '2019-11-12 06:42:29'),
(7205, '', 0.00, 6, '2019-11-12 06:42:29'),
(7206, '', 0.00, 7, '2019-11-12 06:42:29'),
(7207, '', 0.00, 6, '2019-11-12 06:42:29'),
(7208, '', 0.00, 7, '2019-11-12 06:42:29'),
(7209, '', 0.00, 6, '2019-11-12 06:42:29'),
(7210, '', 0.00, 7, '2019-11-12 06:42:29'),
(7211, '', 0.00, 6, '2019-11-12 06:42:29'),
(7212, '', 0.00, 7, '2019-11-12 06:42:29'),
(7213, '', 0.00, 6, '2019-11-12 06:42:29'),
(7214, '', 0.00, 7, '2019-11-12 06:42:29'),
(7215, '', 0.00, 6, '2019-11-12 06:42:29'),
(7216, '', 0.00, 7, '2019-11-12 06:42:29'),
(7217, '', 0.00, 6, '2019-11-12 06:42:30'),
(7218, '', 0.00, 7, '2019-11-12 06:42:30'),
(7219, '', 0.00, 6, '2019-11-12 06:42:30'),
(7220, '', 0.00, 7, '2019-11-12 06:42:30'),
(7221, '', 0.00, 6, '2019-11-12 06:42:30'),
(7222, '', 0.00, 7, '2019-11-12 06:42:30'),
(7223, '', 0.00, 6, '2019-11-12 06:42:30'),
(7224, '', 0.00, 7, '2019-11-12 06:42:30'),
(7225, '', 0.00, 6, '2019-11-12 06:42:30'),
(7226, '', 0.00, 7, '2019-11-12 06:42:30'),
(7227, '', 0.00, 6, '2019-11-12 06:42:30'),
(7228, '', 0.00, 7, '2019-11-12 06:42:30'),
(7229, '', 0.00, 6, '2019-11-12 06:42:30'),
(7230, '', 0.00, 7, '2019-11-12 06:42:30'),
(7231, '', 0.00, 6, '2019-11-12 06:42:30'),
(7232, '', 0.00, 7, '2019-11-12 06:42:30'),
(7233, '', 0.00, 6, '2019-11-12 06:42:30'),
(7234, '', 0.00, 7, '2019-11-12 06:42:31'),
(7235, '', 0.00, 6, '2019-11-12 06:42:31'),
(7236, '', 0.00, 7, '2019-11-12 06:42:31'),
(7237, '', 0.00, 6, '2019-11-12 06:42:31'),
(7238, '', 0.00, 7, '2019-11-12 06:42:31'),
(7239, '', 0.00, 6, '2019-11-12 06:42:31'),
(7240, '', 0.00, 7, '2019-11-12 06:42:31'),
(7241, '', 0.00, 6, '2019-11-12 06:42:31'),
(7242, '', 0.00, 7, '2019-11-12 06:42:32'),
(7243, '', 0.00, 6, '2019-11-12 06:42:32'),
(7244, '', 0.00, 7, '2019-11-12 06:42:32'),
(7245, '', 0.00, 6, '2019-11-12 06:42:32'),
(7246, '', 0.00, 7, '2019-11-12 06:42:32'),
(7247, '', 0.00, 7, '2019-11-12 06:42:32'),
(7248, '', 0.00, 6, '2019-11-12 06:42:32'),
(7249, '', 0.00, 7, '2019-11-12 06:42:32'),
(7250, '', 0.00, 6, '2019-11-12 06:42:32'),
(7251, '', 0.00, 7, '2019-11-12 06:42:32'),
(7252, '', 0.00, 6, '2019-11-12 06:42:32'),
(7253, '', 0.00, 7, '2019-11-12 06:42:32'),
(7254, '', 0.00, 6, '2019-11-12 06:42:32'),
(7255, '', 0.00, 7, '2019-11-12 06:42:33'),
(7256, '', 0.00, 6, '2019-11-12 06:42:33'),
(7257, '', 0.00, 7, '2019-11-12 06:42:33'),
(7258, '', 0.00, 6, '2019-11-12 06:42:33'),
(7259, '', 0.00, 7, '2019-11-12 06:42:33'),
(7260, '', 0.00, 6, '2019-11-12 06:42:33'),
(7261, '', 0.00, 7, '2019-11-12 06:42:33'),
(7262, '', 0.00, 6, '2019-11-12 06:42:33'),
(7263, '', 0.00, 7, '2019-11-12 06:42:33'),
(7264, '', 0.00, 6, '2019-11-12 06:42:33'),
(7265, '', 0.00, 7, '2019-11-12 06:42:33'),
(7266, '', 0.00, 6, '2019-11-12 06:42:33'),
(7267, '', 0.00, 7, '2019-11-12 06:42:34'),
(7268, '', 0.00, 6, '2019-11-12 06:42:34'),
(7269, '', 0.00, 7, '2019-11-12 06:42:34'),
(7270, '', 0.00, 6, '2019-11-12 06:42:34'),
(7271, '', 0.00, 7, '2019-11-12 06:42:34'),
(7272, '', 0.00, 6, '2019-11-12 06:42:34'),
(7273, '', 0.00, 7, '2019-11-12 06:42:34'),
(7274, '', 0.00, 6, '2019-11-12 06:42:34'),
(7275, '', 0.00, 7, '2019-11-12 06:42:34'),
(7276, '', 0.00, 6, '2019-11-12 06:42:34'),
(7277, '', 0.00, 7, '2019-11-12 06:42:34'),
(7278, '', 0.00, 6, '2019-11-12 06:42:34'),
(7279, '', 0.00, 7, '2019-11-12 06:42:34'),
(7280, '', 0.00, 6, '2019-11-12 06:42:35'),
(7281, '', 0.00, 7, '2019-11-12 06:42:35'),
(7282, '', 0.00, 6, '2019-11-12 06:42:35'),
(7283, '', 0.00, 7, '2019-11-12 06:42:35'),
(7284, '', 0.00, 6, '2019-11-12 06:42:35'),
(7285, '', 0.00, 7, '2019-11-12 06:42:35'),
(7286, '', 0.00, 6, '2019-11-12 06:42:35'),
(7287, '', 0.00, 7, '2019-11-12 06:42:35'),
(7288, '', 0.00, 6, '2019-11-12 06:42:35'),
(7289, '', 0.00, 7, '2019-11-12 06:42:35'),
(7290, '', 0.00, 6, '2019-11-12 06:42:35'),
(7291, '', 0.00, 7, '2019-11-12 06:42:35'),
(7292, '', 0.00, 6, '2019-11-12 06:42:36'),
(7293, '', 0.00, 7, '2019-11-12 06:42:36'),
(7294, '', 0.00, 6, '2019-11-12 06:42:36'),
(7295, '', 0.00, 7, '2019-11-12 06:42:36'),
(7296, '', 0.00, 6, '2019-11-12 06:42:36'),
(7297, '', 0.00, 7, '2019-11-12 06:42:36'),
(7298, '', 0.00, 6, '2019-11-12 06:42:37'),
(7299, '', 0.00, 7, '2019-11-12 06:42:37'),
(7300, '', 0.00, 6, '2019-11-12 06:42:37'),
(7301, '', 0.00, 7, '2019-11-12 06:42:37'),
(7302, '', 0.00, 6, '2019-11-12 06:42:37'),
(7303, '', 0.00, 7, '2019-11-12 06:42:37'),
(7304, '', 0.00, 6, '2019-11-12 06:42:37'),
(7305, '', 0.00, 7, '2019-11-12 06:42:37'),
(7306, '', 0.00, 6, '2019-11-12 06:42:37'),
(7307, '', 0.00, 7, '2019-11-12 06:42:37'),
(7308, '', 0.00, 6, '2019-11-12 06:42:37'),
(7309, '', 0.00, 7, '2019-11-12 06:42:37'),
(7310, '', 0.00, 6, '2019-11-12 06:42:37'),
(7311, '', 0.00, 7, '2019-11-12 06:42:38'),
(7312, '', 0.00, 6, '2019-11-12 06:42:38'),
(7313, '', 0.00, 7, '2019-11-12 06:42:38'),
(7314, '', 0.00, 6, '2019-11-12 06:42:38'),
(7315, '', 0.00, 7, '2019-11-12 06:42:38'),
(7316, '', 0.00, 6, '2019-11-12 06:42:38'),
(7317, '', 0.00, 7, '2019-11-12 06:42:38'),
(7318, '', 0.00, 6, '2019-11-12 06:42:38'),
(7319, '', 0.00, 7, '2019-11-12 06:42:38'),
(7320, '', 0.00, 6, '2019-11-12 06:42:38'),
(7321, '', 0.00, 7, '2019-11-12 06:42:38'),
(7322, '', 0.00, 6, '2019-11-12 06:42:38'),
(7323, '', 0.00, 7, '2019-11-12 06:42:38'),
(7324, '', 0.00, 6, '2019-11-12 06:42:38'),
(7325, '', 0.00, 7, '2019-11-12 06:42:38'),
(7326, '\0\0\0', 0.00, 6, '2019-11-12 06:42:38'),
(7327, '', 0.00, 7, '2019-11-12 06:42:39'),
(7328, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:39'),
(7329, '', 0.00, 6, '2019-11-12 06:42:39'),
(7330, '', 0.00, 7, '2019-11-12 06:42:39'),
(7331, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:39'),
(7332, '', 0.00, 7, '2019-11-12 06:42:39'),
(7333, '', 0.00, 6, '2019-11-12 06:42:39'),
(7334, '', 0.00, 7, '2019-11-12 06:42:39'),
(7335, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:39'),
(7336, '', 0.00, 7, '2019-11-12 06:42:39'),
(7337, '', 0.00, 6, '2019-11-12 06:42:39'),
(7338, '', 0.00, 7, '2019-11-12 06:42:39'),
(7339, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:40'),
(7340, '', 0.00, 7, '2019-11-12 06:42:40'),
(7341, '', 0.00, 6, '2019-11-12 06:42:40'),
(7342, '', 0.00, 7, '2019-11-12 06:42:40'),
(7343, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:40'),
(7344, '', 0.00, 7, '2019-11-12 06:42:40'),
(7345, '', 0.00, 6, '2019-11-12 06:42:40'),
(7346, '', 0.00, 7, '2019-11-12 06:42:40'),
(7347, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:40'),
(7348, '', 0.00, 7, '2019-11-12 06:42:40'),
(7349, '', 0.00, 6, '2019-11-12 06:42:40'),
(7350, '', 0.00, 7, '2019-11-12 06:42:40'),
(7351, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:40'),
(7352, '', 0.00, 7, '2019-11-12 06:42:40'),
(7353, '', 0.00, 6, '2019-11-12 06:42:40'),
(7354, '', 0.00, 7, '2019-11-12 06:42:40'),
(7355, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:40'),
(7356, '', 0.00, 7, '2019-11-12 06:42:40'),
(7357, '', 0.00, 6, '2019-11-12 06:42:40'),
(7358, '', 0.00, 7, '2019-11-12 06:42:41'),
(7359, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:41'),
(7360, '', 0.00, 7, '2019-11-12 06:42:41'),
(7361, '', 0.00, 6, '2019-11-12 06:42:41'),
(7362, '', 0.00, 7, '2019-11-12 06:42:41'),
(7363, '\0	\0\0\0	\0\0~', 0.00, 6, '2019-11-12 06:42:41'),
(7364, '', 0.00, 7, '2019-11-12 06:42:41'),
(7365, '', 0.00, 6, '2019-11-12 06:42:41'),
(7366, '', 0.00, 7, '2019-11-12 06:42:41'),
(7367, '\0', 0.00, 6, '2019-11-12 06:42:41'),
(7368, '', 0.00, 7, '2019-11-12 06:42:41'),
(7369, '\0\0\0', 0.00, 6, '2019-11-12 06:42:41'),
(7370, '', 0.00, 7, '2019-11-12 06:42:41'),
(7371, '\0\0~', 0.00, 6, '2019-11-12 06:42:41'),
(7372, '', 0.00, 7, '2019-11-12 06:42:41'),
(7373, '\0', 0.00, 6, '2019-11-12 06:42:41'),
(7374, '', 0.00, 7, '2019-11-12 06:42:41'),
(7375, '', 0.00, 6, '2019-11-12 06:42:41'),
(7376, '', 0.00, 7, '2019-11-12 06:42:41'),
(7377, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:41'),
(7378, '', 0.00, 7, '2019-11-12 06:42:41'),
(7379, '', 0.00, 6, '2019-11-12 06:42:41'),
(7380, '', 0.00, 7, '2019-11-12 06:42:41'),
(7381, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:41'),
(7382, '', 0.00, 7, '2019-11-12 06:42:42'),
(7383, '', 0.00, 6, '2019-11-12 06:42:42'),
(7384, '', 0.00, 7, '2019-11-12 06:42:42'),
(7385, '\0\r\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:42:42'),
(7386, '', 0.00, 7, '2019-11-12 06:42:42'),
(7387, '', 0.00, 6, '2019-11-12 06:42:42'),
(7388, '', 0.00, 7, '2019-11-12 06:42:42'),
(7389, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:42'),
(7390, '', 0.00, 7, '2019-11-12 06:42:42'),
(7391, '', 0.00, 6, '2019-11-12 06:42:42'),
(7392, '', 0.00, 7, '2019-11-12 06:42:42'),
(7393, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:42'),
(7394, '', 0.00, 7, '2019-11-12 06:42:42'),
(7395, '', 0.00, 6, '2019-11-12 06:42:42'),
(7396, '', 0.00, 7, '2019-11-12 06:42:43'),
(7397, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:43'),
(7398, '', 0.00, 7, '2019-11-12 06:42:43');
INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(7399, '', 0.00, 6, '2019-11-12 06:42:43'),
(7400, '', 0.00, 7, '2019-11-12 06:42:43'),
(7401, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:43'),
(7402, '', 0.00, 7, '2019-11-12 06:42:43'),
(7403, '', 0.00, 6, '2019-11-12 06:42:43'),
(7404, '', 0.00, 7, '2019-11-12 06:42:43'),
(7405, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:43'),
(7406, '', 0.00, 7, '2019-11-12 06:42:43'),
(7407, '\0\0\0', 0.00, 6, '2019-11-12 06:42:43'),
(7408, '', 0.00, 7, '2019-11-12 06:42:43'),
(7409, '', 0.00, 6, '2019-11-12 06:42:43'),
(7410, '', 0.00, 7, '2019-11-12 06:42:43'),
(7411, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:44'),
(7412, '', 0.00, 7, '2019-11-12 06:42:44'),
(7413, '', 0.00, 6, '2019-11-12 06:42:44'),
(7414, '', 0.00, 7, '2019-11-12 06:42:44'),
(7415, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:44'),
(7416, '', 0.00, 7, '2019-11-12 06:42:44'),
(7417, '', 0.00, 6, '2019-11-12 06:42:44'),
(7418, '', 0.00, 7, '2019-11-12 06:42:44'),
(7419, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:44'),
(7420, '', 0.00, 7, '2019-11-12 06:42:44'),
(7421, '', 0.00, 6, '2019-11-12 06:42:44'),
(7422, '', 0.00, 7, '2019-11-12 06:42:44'),
(7423, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:44'),
(7424, '', 0.00, 7, '2019-11-12 06:42:44'),
(7425, '', 0.00, 6, '2019-11-12 06:42:45'),
(7426, '', 0.00, 7, '2019-11-12 06:42:45'),
(7427, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:45'),
(7428, '', 0.00, 7, '2019-11-12 06:42:45'),
(7429, '', 0.00, 6, '2019-11-12 06:42:45'),
(7430, '', 0.00, 7, '2019-11-12 06:42:45'),
(7431, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:45'),
(7432, '', 0.00, 7, '2019-11-12 06:42:45'),
(7433, '', 0.00, 6, '2019-11-12 06:42:45'),
(7434, '', 0.00, 7, '2019-11-12 06:42:45'),
(7435, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:45'),
(7436, '', 0.00, 7, '2019-11-12 06:42:45'),
(7437, '', 0.00, 6, '2019-11-12 06:42:45'),
(7438, '', 0.00, 7, '2019-11-12 06:42:45'),
(7439, '\0\Z\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:42:45'),
(7440, '', 0.00, 7, '2019-11-12 06:42:46'),
(7441, '', 0.00, 6, '2019-11-12 06:42:46'),
(7442, '', 0.00, 7, '2019-11-12 06:42:46'),
(7443, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:46'),
(7444, '', 0.00, 7, '2019-11-12 06:42:46'),
(7445, '', 0.00, 6, '2019-11-12 06:42:46'),
(7446, '', 0.00, 7, '2019-11-12 06:42:46'),
(7447, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:46'),
(7448, '', 0.00, 7, '2019-11-12 06:42:46'),
(7449, '', 0.00, 6, '2019-11-12 06:42:46'),
(7450, '', 0.00, 7, '2019-11-12 06:42:46'),
(7451, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:46'),
(7452, '', 0.00, 7, '2019-11-12 06:42:46'),
(7453, '', 0.00, 6, '2019-11-12 06:42:46'),
(7454, '', 0.00, 7, '2019-11-12 06:42:46'),
(7455, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:46'),
(7456, '', 0.00, 7, '2019-11-12 06:42:47'),
(7457, '\0\0\0S', 0.00, 6, '2019-11-12 06:42:47'),
(7458, '', 0.00, 7, '2019-11-12 06:42:47'),
(7459, '', 0.00, 6, '2019-11-12 06:42:47'),
(7460, '', 0.00, 7, '2019-11-12 06:42:47'),
(7461, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:42:47'),
(7462, '', 0.00, 7, '2019-11-12 06:42:47'),
(7463, '\0\0\0S', 0.00, 6, '2019-11-12 06:42:47'),
(7464, '', 0.00, 7, '2019-11-12 06:42:47'),
(7465, '\0\0\0 \0\0\0', 0.00, 6, '2019-11-12 06:42:47'),
(7466, '', 0.00, 7, '2019-11-12 06:42:47'),
(7467, '\0 \0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:42:47'),
(7468, '', 0.00, 7, '2019-11-12 06:42:47'),
(7469, '', 0.00, 6, '2019-11-12 06:42:47'),
(7470, '', 0.00, 7, '2019-11-12 06:42:47'),
(7471, '\0!\0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:42:47'),
(7472, '', 0.00, 7, '2019-11-12 06:42:47'),
(7473, '', 0.00, 6, '2019-11-12 06:42:47'),
(7474, '', 0.00, 7, '2019-11-12 06:42:47'),
(7475, '\0&quot;\0\0\0&quot;\0\0~', 0.00, 6, '2019-11-12 06:42:47'),
(7476, '', 0.00, 7, '2019-11-12 06:42:48'),
(7477, '', 0.00, 6, '2019-11-12 06:42:48'),
(7478, '', 0.00, 7, '2019-11-12 06:42:48'),
(7479, '\0#\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:42:48'),
(7480, '', 0.00, 7, '2019-11-12 06:42:48'),
(7481, '', 0.00, 6, '2019-11-12 06:42:48'),
(7482, '', 0.00, 7, '2019-11-12 06:42:48'),
(7483, '\0$\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:42:48'),
(7484, '', 0.00, 7, '2019-11-12 06:42:48'),
(7485, '', 0.00, 6, '2019-11-12 06:42:48'),
(7486, '', 0.00, 7, '2019-11-12 06:42:48'),
(7487, '\0%\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:42:48'),
(7488, '', 0.00, 7, '2019-11-12 06:42:48'),
(7489, '', 0.00, 6, '2019-11-12 06:42:48'),
(7490, '', 0.00, 7, '2019-11-12 06:42:48'),
(7491, '\0&amp;\0\0\0&amp;\0\0~', 0.00, 6, '2019-11-12 06:42:48'),
(7492, '', 0.00, 7, '2019-11-12 06:42:48'),
(7493, '', 0.00, 6, '2019-11-12 06:42:49'),
(7494, '', 0.00, 7, '2019-11-12 06:42:49'),
(7495, '\0\'\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:42:49'),
(7496, '', 0.00, 7, '2019-11-12 06:42:49'),
(7497, '', 0.00, 6, '2019-11-12 06:42:49'),
(7498, '', 0.00, 7, '2019-11-12 06:42:49'),
(7499, '\0(\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:42:49'),
(7500, '', 0.00, 7, '2019-11-12 06:42:49'),
(7501, '', 0.00, 6, '2019-11-12 06:42:50'),
(7502, '', 0.00, 7, '2019-11-12 06:42:50'),
(7503, '\0)\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:42:50'),
(7504, '', 0.00, 7, '2019-11-12 06:42:50'),
(7505, '', 0.00, 6, '2019-11-12 06:42:50'),
(7506, '', 0.00, 7, '2019-11-12 06:42:50'),
(7507, '\0*\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:42:50'),
(7508, '', 0.00, 7, '2019-11-12 06:42:51'),
(7509, '', 0.00, 6, '2019-11-12 06:42:51'),
(7510, '', 0.00, 7, '2019-11-12 06:42:51'),
(7511, '\0+\0\0\0+\0\0~', 0.00, 6, '2019-11-12 06:42:51'),
(7512, '', 0.00, 7, '2019-11-12 06:42:51'),
(7513, '', 0.00, 6, '2019-11-12 06:42:51'),
(7514, '', 0.00, 7, '2019-11-12 06:42:51'),
(7515, '\0', 0.00, 6, '2019-11-12 06:42:51'),
(7516, '', 0.00, 7, '2019-11-12 06:42:51'),
(7517, '\0', 0.00, 6, '2019-11-12 06:42:51'),
(7518, '', 0.00, 7, '2019-11-12 06:42:51'),
(7519, '\0-\0\0\0-\0\0~', 0.00, 6, '2019-11-12 06:42:51'),
(7520, '', 0.00, 7, '2019-11-12 06:42:51'),
(7521, '', 0.00, 6, '2019-11-12 06:42:51'),
(7522, '', 0.00, 7, '2019-11-12 06:42:51'),
(7523, '\0.\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:42:52'),
(7524, '', 0.00, 7, '2019-11-12 06:42:52'),
(7525, '', 0.00, 6, '2019-11-12 06:42:52'),
(7526, '', 0.00, 7, '2019-11-12 06:42:52'),
(7527, '\0/\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:42:52'),
(7528, '', 0.00, 7, '2019-11-12 06:42:52'),
(7529, '', 0.00, 6, '2019-11-12 06:42:52'),
(7530, '', 0.00, 7, '2019-11-12 06:42:52'),
(7531, '\00\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:42:52'),
(7532, '', 0.00, 7, '2019-11-12 06:42:52'),
(7533, '', 0.00, 6, '2019-11-12 06:42:52'),
(7534, '', 0.00, 7, '2019-11-12 06:42:52'),
(7535, '\01\0\0\01\0\0~', 0.00, 6, '2019-11-12 06:42:52'),
(7536, '', 0.00, 7, '2019-11-12 06:42:52'),
(7537, '', 0.00, 6, '2019-11-12 06:42:52'),
(7538, '', 0.00, 7, '2019-11-12 06:42:53'),
(7539, '\02\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:42:53'),
(7540, '', 0.00, 7, '2019-11-12 06:42:53'),
(7541, '', 0.00, 6, '2019-11-12 06:42:53'),
(7542, '', 0.00, 7, '2019-11-12 06:42:53'),
(7543, '\03\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:42:53'),
(7544, '', 0.00, 7, '2019-11-12 06:42:53'),
(7545, '', 0.00, 6, '2019-11-12 06:42:53'),
(7546, '', 0.00, 7, '2019-11-12 06:42:53'),
(7547, '\04\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:42:53'),
(7548, '', 0.00, 7, '2019-11-12 06:42:53'),
(7549, '', 0.00, 6, '2019-11-12 06:42:53'),
(7550, '', 0.00, 7, '2019-11-12 06:42:53'),
(7551, '\05\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:42:53'),
(7552, '', 0.00, 7, '2019-11-12 06:42:53'),
(7553, '', 0.00, 6, '2019-11-12 06:42:53'),
(7554, '', 0.00, 7, '2019-11-12 06:42:53'),
(7555, '\06\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:42:53'),
(7556, '', 0.00, 7, '2019-11-12 06:42:53'),
(7557, '', 0.00, 6, '2019-11-12 06:42:53'),
(7558, '', 0.00, 7, '2019-11-12 06:42:53'),
(7559, '\07\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:42:54'),
(7560, '', 0.00, 7, '2019-11-12 06:42:54'),
(7561, '', 0.00, 6, '2019-11-12 06:42:54'),
(7562, '', 0.00, 7, '2019-11-12 06:42:54'),
(7563, '\08\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:42:54'),
(7564, '', 0.00, 7, '2019-11-12 06:42:54'),
(7565, '', 0.00, 6, '2019-11-12 06:42:54'),
(7566, '', 0.00, 7, '2019-11-12 06:42:54'),
(7567, '\09\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:42:54'),
(7568, '', 0.00, 7, '2019-11-12 06:42:54'),
(7569, '', 0.00, 6, '2019-11-12 06:42:54'),
(7570, '', 0.00, 7, '2019-11-12 06:42:54'),
(7571, '', 0.00, 6, '2019-11-12 06:42:54'),
(7572, '', 0.00, 7, '2019-11-12 06:42:54'),
(7573, '\0;\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:42:54'),
(7574, '', 0.00, 7, '2019-11-12 06:42:54'),
(7575, '', 0.00, 6, '2019-11-12 06:42:54'),
(7576, '', 0.00, 7, '2019-11-12 06:42:54'),
(7577, '\0&lt;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:42:54'),
(7578, '', 0.00, 7, '2019-11-12 06:42:54'),
(7579, '', 0.00, 6, '2019-11-12 06:42:54'),
(7580, '', 0.00, 7, '2019-11-12 06:42:54'),
(7581, '\0=\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:42:54'),
(7582, '', 0.00, 7, '2019-11-12 06:42:54'),
(7583, '\0=\0\0', 0.00, 6, '2019-11-12 06:42:54'),
(7584, '', 0.00, 7, '2019-11-12 06:42:55'),
(7585, '', 0.00, 6, '2019-11-12 06:42:55'),
(7586, '', 0.00, 7, '2019-11-12 06:42:55'),
(7587, '\0&gt;\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:42:55'),
(7588, '', 0.00, 7, '2019-11-12 06:42:55'),
(7589, '', 0.00, 6, '2019-11-12 06:42:55'),
(7590, '', 0.00, 7, '2019-11-12 06:42:55'),
(7591, '\0?\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:42:55'),
(7592, '', 0.00, 7, '2019-11-12 06:42:55'),
(7593, '\0?\0\0\0\0\0\0@\0\0\0', 0.00, 6, '2019-11-12 06:42:55'),
(7594, '', 0.00, 7, '2019-11-12 06:42:55'),
(7595, '\0@\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:42:56'),
(7596, '', 0.00, 7, '2019-11-12 06:42:56'),
(7597, '', 0.00, 6, '2019-11-12 06:42:56'),
(7598, '', 0.00, 7, '2019-11-12 06:42:56'),
(7599, '\0A\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:42:56'),
(7600, '', 0.00, 7, '2019-11-12 06:42:56'),
(7601, '', 0.00, 6, '2019-11-12 06:42:56'),
(7602, '', 0.00, 7, '2019-11-12 06:42:57'),
(7603, '\0B\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:42:57'),
(7604, '', 0.00, 7, '2019-11-12 06:42:57'),
(7605, '', 0.00, 6, '2019-11-12 06:42:57'),
(7606, '', 0.00, 7, '2019-11-12 06:42:57'),
(7607, '', 0.00, 6, '2019-11-12 06:42:58'),
(7608, '', 0.00, 7, '2019-11-12 06:42:58'),
(7609, '\0D\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:42:58'),
(7610, '', 0.00, 7, '2019-11-12 06:42:58'),
(7611, '', 0.00, 6, '2019-11-12 06:42:58'),
(7612, '', 0.00, 7, '2019-11-12 06:42:58'),
(7613, '\0E\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:42:58'),
(7614, '', 0.00, 7, '2019-11-12 06:42:58'),
(7615, '', 0.00, 6, '2019-11-12 06:42:59'),
(7616, '', 0.00, 7, '2019-11-12 06:42:59'),
(7617, '\0F\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:42:59'),
(7618, '', 0.00, 7, '2019-11-12 06:42:59'),
(7619, '', 0.00, 6, '2019-11-12 06:42:59'),
(7620, '', 0.00, 7, '2019-11-12 06:42:59'),
(7621, '\0G\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:42:59'),
(7622, '', 0.00, 7, '2019-11-12 06:42:59'),
(7623, '', 0.00, 6, '2019-11-12 06:42:59'),
(7624, '', 0.00, 7, '2019-11-12 06:42:59'),
(7625, '\0H\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:42:59'),
(7626, '', 0.00, 7, '2019-11-12 06:42:59'),
(7627, '', 0.00, 6, '2019-11-12 06:42:59'),
(7628, '', 0.00, 7, '2019-11-12 06:42:59'),
(7629, '\0I\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:42:59'),
(7630, '', 0.00, 7, '2019-11-12 06:42:59'),
(7631, '', 0.00, 6, '2019-11-12 06:42:59'),
(7632, '', 0.00, 7, '2019-11-12 06:42:59'),
(7633, '\0J\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:42:59'),
(7634, '', 0.00, 7, '2019-11-12 06:42:59'),
(7635, '', 0.00, 6, '2019-11-12 06:42:59'),
(7636, '', 0.00, 7, '2019-11-12 06:43:00'),
(7637, '\0K\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:43:00'),
(7638, '', 0.00, 7, '2019-11-12 06:43:00'),
(7639, '', 0.00, 6, '2019-11-12 06:43:00'),
(7640, '', 0.00, 7, '2019-11-12 06:43:00'),
(7641, '\0L\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:43:00'),
(7642, '', 0.00, 7, '2019-11-12 06:43:00'),
(7643, '', 0.00, 6, '2019-11-12 06:43:00'),
(7644, '', 0.00, 7, '2019-11-12 06:43:00'),
(7645, '\0M\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:43:00'),
(7646, '', 0.00, 7, '2019-11-12 06:43:00'),
(7647, '', 0.00, 6, '2019-11-12 06:43:00'),
(7648, '', 0.00, 7, '2019-11-12 06:43:00'),
(7649, '\0N\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:43:00'),
(7650, '', 0.00, 7, '2019-11-12 06:43:00'),
(7651, '', 0.00, 6, '2019-11-12 06:43:00'),
(7652, '', 0.00, 7, '2019-11-12 06:43:00'),
(7653, '\0O\0\0\0O\0\0~', 0.00, 6, '2019-11-12 06:43:00'),
(7654, '', 0.00, 7, '2019-11-12 06:43:00'),
(7655, '\0O\0\0', 0.00, 6, '2019-11-12 06:43:00'),
(7656, '', 0.00, 7, '2019-11-12 06:43:01'),
(7657, '', 0.00, 6, '2019-11-12 06:43:01'),
(7658, '', 0.00, 7, '2019-11-12 06:43:01'),
(7659, '\0P\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:43:01'),
(7660, '', 0.00, 7, '2019-11-12 06:43:01'),
(7661, '', 0.00, 6, '2019-11-12 06:43:01'),
(7662, '', 0.00, 7, '2019-11-12 06:43:01'),
(7663, '\0Q\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:43:02'),
(7664, '', 0.00, 7, '2019-11-12 06:43:02'),
(7665, '', 0.00, 6, '2019-11-12 06:43:02'),
(7666, '', 0.00, 7, '2019-11-12 06:43:02'),
(7667, '\0R\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:43:02'),
(7668, '\0\0\0', 0.00, 7, '2019-11-12 06:43:02'),
(7669, '', 0.00, 6, '2019-11-12 06:43:02'),
(7670, '\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:02'),
(7671, '\0S\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:43:02'),
(7672, '\0\0\0\0S', 0.00, 7, '2019-11-12 06:43:02'),
(7673, '', 0.00, 6, '2019-11-12 06:43:02'),
(7674, '', 0.00, 7, '2019-11-12 06:43:02'),
(7675, '\0T\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:43:02'),
(7676, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:02'),
(7677, '', 0.00, 6, '2019-11-12 06:43:03'),
(7678, '', 0.00, 7, '2019-11-12 06:43:03'),
(7679, '', 0.00, 6, '2019-11-12 06:43:03'),
(7680, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:03'),
(7681, '\0V\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:43:03'),
(7682, '', 0.00, 7, '2019-11-12 06:43:03'),
(7683, '', 0.00, 6, '2019-11-12 06:43:03'),
(7684, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:03'),
(7685, '\0W\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:43:03'),
(7686, '', 0.00, 7, '2019-11-12 06:43:03'),
(7687, '', 0.00, 6, '2019-11-12 06:43:03'),
(7688, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:03'),
(7689, '\0X\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:43:03'),
(7690, '', 0.00, 7, '2019-11-12 06:43:03'),
(7691, '', 0.00, 6, '2019-11-12 06:43:03'),
(7692, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:03'),
(7693, '\0Y\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:43:03'),
(7694, '', 0.00, 7, '2019-11-12 06:43:03'),
(7695, '', 0.00, 6, '2019-11-12 06:43:03'),
(7696, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:03'),
(7697, '', 0.00, 6, '2019-11-12 06:43:03'),
(7698, '\0\0\0R', 0.00, 7, '2019-11-12 06:43:04'),
(7699, '\0[\0\0\0[\0\0~', 0.00, 6, '2019-11-12 06:43:04'),
(7700, '', 0.00, 7, '2019-11-12 06:43:04'),
(7701, '', 0.00, 6, '2019-11-12 06:43:04'),
(7702, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:04'),
(7703, '\0\\\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:43:04'),
(7704, '\0\0\0R', 0.00, 7, '2019-11-12 06:43:04'),
(7705, '', 0.00, 6, '2019-11-12 06:43:04'),
(7706, '', 0.00, 7, '2019-11-12 06:43:04'),
(7707, '\0]\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:43:04'),
(7708, '\0\0\0\0	\0\0~', 0.00, 7, '2019-11-12 06:43:04'),
(7709, '', 0.00, 6, '2019-11-12 06:43:04'),
(7710, '', 0.00, 7, '2019-11-12 06:43:04'),
(7711, '\0^\0\0\0^\0\0~', 0.00, 6, '2019-11-12 06:43:04'),
(7712, '\0	\0\0\0', 0.00, 7, '2019-11-12 06:43:04'),
(7713, '', 0.00, 6, '2019-11-12 06:43:05'),
(7714, '\0\0~', 0.00, 7, '2019-11-12 06:43:05'),
(7715, '', 0.00, 6, '2019-11-12 06:43:05'),
(7716, '', 0.00, 7, '2019-11-12 06:43:05'),
(7717, '\0`\0\0\0`\0\0~', 0.00, 6, '2019-11-12 06:43:05'),
(7718, '\0', 0.00, 7, '2019-11-12 06:43:05'),
(7719, '', 0.00, 6, '2019-11-12 06:43:05'),
(7720, '\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:05'),
(7721, '\0a\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:43:05'),
(7722, '\0', 0.00, 7, '2019-11-12 06:43:05'),
(7723, '', 0.00, 6, '2019-11-12 06:43:05'),
(7724, '', 0.00, 7, '2019-11-12 06:43:05'),
(7725, '\0b\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:43:05'),
(7726, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:05'),
(7727, '\0b\0\0', 0.00, 6, '2019-11-12 06:43:05'),
(7728, '', 0.00, 7, '2019-11-12 06:43:05'),
(7729, '', 0.00, 6, '2019-11-12 06:43:06'),
(7730, '\0\0\0\0\r\0\0~', 0.00, 7, '2019-11-12 06:43:06'),
(7731, '\0c\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:43:06'),
(7732, '', 0.00, 7, '2019-11-12 06:43:06'),
(7733, '', 0.00, 6, '2019-11-12 06:43:06'),
(7734, '\0\r\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:06'),
(7735, '\0d\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:43:07'),
(7736, '', 0.00, 7, '2019-11-12 06:43:07'),
(7737, '', 0.00, 6, '2019-11-12 06:43:07'),
(7738, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:07'),
(7739, '\0e\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:43:07'),
(7740, '', 0.00, 7, '2019-11-12 06:43:07'),
(7741, '', 0.00, 6, '2019-11-12 06:43:07'),
(7742, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:07'),
(7743, '\0f\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:43:07'),
(7744, '', 0.00, 7, '2019-11-12 06:43:07'),
(7745, '', 0.00, 6, '2019-11-12 06:43:07'),
(7746, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:07'),
(7747, '\0g\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:43:07'),
(7748, '', 0.00, 7, '2019-11-12 06:43:07'),
(7749, '', 0.00, 6, '2019-11-12 06:43:07'),
(7750, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:07'),
(7751, '\0h\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:43:07'),
(7752, '', 0.00, 7, '2019-11-12 06:43:07'),
(7753, '', 0.00, 6, '2019-11-12 06:43:08'),
(7754, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:08'),
(7755, '\0i\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:43:08'),
(7756, '', 0.00, 7, '2019-11-12 06:43:08'),
(7757, '', 0.00, 6, '2019-11-12 06:43:08'),
(7758, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:08'),
(7759, '\0j\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:43:08'),
(7760, '', 0.00, 7, '2019-11-12 06:43:08'),
(7761, '', 0.00, 6, '2019-11-12 06:43:08'),
(7762, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:08'),
(7763, '\0k\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:43:08'),
(7764, '', 0.00, 7, '2019-11-12 06:43:08'),
(7765, '\0k\0\0', 0.00, 6, '2019-11-12 06:43:08'),
(7766, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:08'),
(7767, '', 0.00, 6, '2019-11-12 06:43:08'),
(7768, '', 0.00, 7, '2019-11-12 06:43:08'),
(7769, '\0l\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:43:08'),
(7770, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:08'),
(7771, '', 0.00, 6, '2019-11-12 06:43:08'),
(7772, '', 0.00, 7, '2019-11-12 06:43:08'),
(7773, '\0m\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:43:08'),
(7774, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:09'),
(7775, '', 0.00, 6, '2019-11-12 06:43:09'),
(7776, '', 0.00, 7, '2019-11-12 06:43:09'),
(7777, '\0n\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:43:09'),
(7778, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:09'),
(7779, '', 0.00, 6, '2019-11-12 06:43:09'),
(7780, '', 0.00, 7, '2019-11-12 06:43:09'),
(7781, '\0o\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:43:09'),
(7782, '\0\0\0\0\Z\0\0~', 0.00, 7, '2019-11-12 06:43:09'),
(7783, '', 0.00, 6, '2019-11-12 06:43:09'),
(7784, '', 0.00, 7, '2019-11-12 06:43:09'),
(7785, '\0p\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:43:09'),
(7786, '\0\Z\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:09'),
(7787, '', 0.00, 6, '2019-11-12 06:43:09'),
(7788, '', 0.00, 7, '2019-11-12 06:43:09'),
(7789, '', 0.00, 6, '2019-11-12 06:43:10'),
(7790, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:10'),
(7791, '\0r\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:43:10'),
(7792, '', 0.00, 7, '2019-11-12 06:43:10'),
(7793, '', 0.00, 6, '2019-11-12 06:43:10'),
(7794, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:10'),
(7795, '\0s\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:43:10'),
(7796, '', 0.00, 7, '2019-11-12 06:43:10'),
(7797, '', 0.00, 6, '2019-11-12 06:43:10'),
(7798, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:10'),
(7799, '\0t\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:43:10'),
(7800, '', 0.00, 7, '2019-11-12 06:43:10'),
(7801, '', 0.00, 6, '2019-11-12 06:43:10'),
(7802, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:10'),
(7803, '\0u\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:43:10'),
(7804, '', 0.00, 7, '2019-11-12 06:43:10'),
(7805, '', 0.00, 6, '2019-11-12 06:43:10'),
(7806, '\0\0\0\0 \0\0~', 0.00, 7, '2019-11-12 06:43:10'),
(7807, '\0v\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:43:10'),
(7808, '', 0.00, 7, '2019-11-12 06:43:10'),
(7809, '', 0.00, 6, '2019-11-12 06:43:11'),
(7810, '\0 \0\0\0!\0\0~', 0.00, 7, '2019-11-12 06:43:11'),
(7811, '\0w\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:43:11'),
(7812, '', 0.00, 7, '2019-11-12 06:43:11'),
(7813, '', 0.00, 6, '2019-11-12 06:43:11'),
(7814, '\0!\0\0\0&quot;\0\0~', 0.00, 7, '2019-11-12 06:43:11'),
(7815, '\0x\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:43:11'),
(7816, '', 0.00, 7, '2019-11-12 06:43:11'),
(7817, '', 0.00, 6, '2019-11-12 06:43:11'),
(7818, '\0&quot;\0\0\0#\0\0~', 0.00, 7, '2019-11-12 06:43:11'),
(7819, '\0y\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:43:11'),
(7820, '', 0.00, 7, '2019-11-12 06:43:11'),
(7821, '', 0.00, 6, '2019-11-12 06:43:11'),
(7822, '\0#\0\0\0$\0\0~', 0.00, 7, '2019-11-12 06:43:12'),
(7823, '\0z\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:43:12'),
(7824, '', 0.00, 7, '2019-11-12 06:43:12'),
(7825, '', 0.00, 6, '2019-11-12 06:43:12'),
(7826, '\0$\0\0\0%\0\0~', 0.00, 7, '2019-11-12 06:43:12'),
(7827, '\0{\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:43:12'),
(7828, '', 0.00, 7, '2019-11-12 06:43:13'),
(7829, '', 0.00, 6, '2019-11-12 06:43:13'),
(7830, '\0%\0\0\0&amp;\0\0~', 0.00, 7, '2019-11-12 06:43:13'),
(7831, '\0|\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:43:13'),
(7832, '', 0.00, 7, '2019-11-12 06:43:13'),
(7833, '', 0.00, 6, '2019-11-12 06:43:13'),
(7834, '\0&amp;\0\0\0\'\0\0~', 0.00, 7, '2019-11-12 06:43:13'),
(7835, '\0}\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:43:13'),
(7836, '', 0.00, 7, '2019-11-12 06:43:13'),
(7837, '\0}\0\0S', 0.00, 6, '2019-11-12 06:43:13'),
(7838, '\0\'\0\0\0(\0\0~', 0.00, 7, '2019-11-12 06:43:13'),
(7839, '', 0.00, 6, '2019-11-12 06:43:13'),
(7840, '\0\'\0\0R', 0.00, 7, '2019-11-12 06:43:13'),
(7841, '\0~\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:43:13'),
(7842, '', 0.00, 7, '2019-11-12 06:43:13'),
(7843, '', 0.00, 6, '2019-11-12 06:43:14'),
(7844, '\0(\0\0\0)\0\0~', 0.00, 7, '2019-11-12 06:43:14'),
(7845, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:14'),
(7846, '', 0.00, 7, '2019-11-12 06:43:14'),
(7847, '', 0.00, 6, '2019-11-12 06:43:14'),
(7848, '\0)\0\0\0*\0\0~', 0.00, 7, '2019-11-12 06:43:14'),
(7849, '', 0.00, 6, '2019-11-12 06:43:14'),
(7850, '', 0.00, 7, '2019-11-12 06:43:14'),
(7851, '', 0.00, 6, '2019-11-12 06:43:14'),
(7852, '', 0.00, 7, '2019-11-12 06:43:14'),
(7853, '', 0.00, 6, '2019-11-12 06:43:14'),
(7854, '\0+\0\0\0', 0.00, 7, '2019-11-12 06:43:14'),
(7855, '', 0.00, 6, '2019-11-12 06:43:14'),
(7856, '', 0.00, 7, '2019-11-12 06:43:14'),
(7857, '', 0.00, 6, '2019-11-12 06:43:14'),
(7858, '\0', 0.00, 7, '2019-11-12 06:43:14'),
(7859, '', 0.00, 6, '2019-11-12 06:43:15'),
(7860, '\0', 0.00, 7, '2019-11-12 06:43:15'),
(7861, '', 0.00, 6, '2019-11-12 06:43:15'),
(7862, '\0-\0\0\0.\0\0~', 0.00, 7, '2019-11-12 06:43:15'),
(7863, '', 0.00, 6, '2019-11-12 06:43:15'),
(7864, '', 0.00, 7, '2019-11-12 06:43:15'),
(7865, '', 0.00, 6, '2019-11-12 06:43:15'),
(7866, '\0.\0\0\0/\0\0~', 0.00, 7, '2019-11-12 06:43:15'),
(7867, '', 0.00, 6, '2019-11-12 06:43:15'),
(7868, '', 0.00, 7, '2019-11-12 06:43:15'),
(7869, '', 0.00, 6, '2019-11-12 06:43:15'),
(7870, '\0/\0\0\00\0\0~', 0.00, 7, '2019-11-12 06:43:15'),
(7871, '', 0.00, 6, '2019-11-12 06:43:16'),
(7872, '', 0.00, 7, '2019-11-12 06:43:16'),
(7873, '', 0.00, 6, '2019-11-12 06:43:16'),
(7874, '\00\0\0\01\0\0~', 0.00, 7, '2019-11-12 06:43:16'),
(7875, '', 0.00, 6, '2019-11-12 06:43:16'),
(7876, '', 0.00, 7, '2019-11-12 06:43:16'),
(7877, '', 0.00, 6, '2019-11-12 06:43:16'),
(7878, '\01\0\0\02\0\0~', 0.00, 7, '2019-11-12 06:43:16'),
(7879, '', 0.00, 6, '2019-11-12 06:43:16'),
(7880, '', 0.00, 7, '2019-11-12 06:43:16'),
(7881, '', 0.00, 6, '2019-11-12 06:43:16'),
(7882, '\02\0\0\03\0\0~', 0.00, 7, '2019-11-12 06:43:16'),
(7883, '', 0.00, 6, '2019-11-12 06:43:16'),
(7884, '', 0.00, 7, '2019-11-12 06:43:16'),
(7885, '', 0.00, 6, '2019-11-12 06:43:16'),
(7886, '\03\0\0\04\0\0~', 0.00, 7, '2019-11-12 06:43:16'),
(7887, '', 0.00, 6, '2019-11-12 06:43:16'),
(7888, '', 0.00, 7, '2019-11-12 06:43:16'),
(7889, '', 0.00, 6, '2019-11-12 06:43:16'),
(7890, '\04\0\0\05\0\0~', 0.00, 7, '2019-11-12 06:43:16'),
(7891, '', 0.00, 6, '2019-11-12 06:43:16'),
(7892, '', 0.00, 7, '2019-11-12 06:43:17'),
(7893, '', 0.00, 6, '2019-11-12 06:43:17'),
(7894, '\05\0\0\06\0\0~', 0.00, 7, '2019-11-12 06:43:17'),
(7895, '', 0.00, 6, '2019-11-12 06:43:17'),
(7896, '', 0.00, 7, '2019-11-12 06:43:17'),
(7897, '', 0.00, 6, '2019-11-12 06:43:17'),
(7898, '\06\0\0\07\0\0~', 0.00, 7, '2019-11-12 06:43:17'),
(7899, '', 0.00, 6, '2019-11-12 06:43:17'),
(7900, '', 0.00, 7, '2019-11-12 06:43:17'),
(7901, '', 0.00, 6, '2019-11-12 06:43:17'),
(7902, '\07\0\0\08\0\0~', 0.00, 7, '2019-11-12 06:43:17'),
(7903, '', 0.00, 6, '2019-11-12 06:43:17'),
(7904, '', 0.00, 7, '2019-11-12 06:43:17'),
(7905, '', 0.00, 6, '2019-11-12 06:43:17'),
(7906, '\08\0\0\09\0\0~', 0.00, 7, '2019-11-12 06:43:17'),
(7907, '', 0.00, 6, '2019-11-12 06:43:17'),
(7908, '', 0.00, 7, '2019-11-12 06:43:17'),
(7909, '', 0.00, 6, '2019-11-12 06:43:17'),
(7910, '\09\0\0\0:\0\0~', 0.00, 7, '2019-11-12 06:43:18'),
(7911, '', 0.00, 6, '2019-11-12 06:43:18'),
(7912, '', 0.00, 7, '2019-11-12 06:43:18'),
(7913, '', 0.00, 6, '2019-11-12 06:43:18'),
(7914, '\0:\0\0\0;\0\0~', 0.00, 7, '2019-11-12 06:43:18'),
(7915, '', 0.00, 6, '2019-11-12 06:43:18'),
(7916, '', 0.00, 7, '2019-11-12 06:43:18'),
(7917, '', 0.00, 6, '2019-11-12 06:43:18'),
(7918, '\0;\0\0\0&lt;\0\0~', 0.00, 7, '2019-11-12 06:43:18'),
(7919, '', 0.00, 6, '2019-11-12 06:43:18'),
(7920, '\0;\0\0', 0.00, 7, '2019-11-12 06:43:18'),
(7921, '', 0.00, 6, '2019-11-12 06:43:18'),
(7922, '', 0.00, 7, '2019-11-12 06:43:18'),
(7923, '', 0.00, 6, '2019-11-12 06:43:18'),
(7924, '\0&lt;\0\0\0=\0\0~', 0.00, 7, '2019-11-12 06:43:19'),
(7925, '', 0.00, 6, '2019-11-12 06:43:19'),
(7926, '', 0.00, 7, '2019-11-12 06:43:19'),
(7927, '', 0.00, 6, '2019-11-12 06:43:19'),
(7928, '\0=\0\0\0&gt;\0\0~', 0.00, 7, '2019-11-12 06:43:19'),
(7929, '', 0.00, 6, '2019-11-12 06:43:19'),
(7930, '', 0.00, 7, '2019-11-12 06:43:19'),
(7931, '', 0.00, 6, '2019-11-12 06:43:19'),
(7932, '\0&gt;\0\0\0?\0\0~', 0.00, 7, '2019-11-12 06:43:19'),
(7933, '', 0.00, 6, '2019-11-12 06:43:19'),
(7934, '', 0.00, 7, '2019-11-12 06:43:19'),
(7935, '', 0.00, 6, '2019-11-12 06:43:19'),
(7936, '\0?\0\0\0@\0\0~', 0.00, 7, '2019-11-12 06:43:19'),
(7937, '', 0.00, 6, '2019-11-12 06:43:19'),
(7938, '', 0.00, 7, '2019-11-12 06:43:19'),
(7939, '', 0.00, 6, '2019-11-12 06:43:19'),
(7940, '\0@\0\0\0A\0\0~', 0.00, 7, '2019-11-12 06:43:19'),
(7941, '', 0.00, 6, '2019-11-12 06:43:19'),
(7942, '', 0.00, 7, '2019-11-12 06:43:19'),
(7943, '', 0.00, 6, '2019-11-12 06:43:19'),
(7944, '\0A\0\0\0B\0\0~', 0.00, 7, '2019-11-12 06:43:19'),
(7945, '', 0.00, 6, '2019-11-12 06:43:19'),
(7946, '', 0.00, 7, '2019-11-12 06:43:19'),
(7947, '', 0.00, 6, '2019-11-12 06:43:19'),
(7948, '\0B\0\0\0C\0\0~', 0.00, 7, '2019-11-12 06:43:19'),
(7949, '', 0.00, 6, '2019-11-12 06:43:20'),
(7950, '', 0.00, 7, '2019-11-12 06:43:20'),
(7951, '', 0.00, 6, '2019-11-12 06:43:20'),
(7952, '\0C\0\0\0D\0\0~', 0.00, 7, '2019-11-12 06:43:20'),
(7953, '', 0.00, 6, '2019-11-12 06:43:20'),
(7954, '', 0.00, 7, '2019-11-12 06:43:20'),
(7955, '', 0.00, 6, '2019-11-12 06:43:20'),
(7956, '\0D\0\0\0E\0\0~', 0.00, 7, '2019-11-12 06:43:20'),
(7957, '', 0.00, 6, '2019-11-12 06:43:20'),
(7958, '', 0.00, 7, '2019-11-12 06:43:20'),
(7959, '', 0.00, 6, '2019-11-12 06:43:20'),
(7960, '\0E\0\0\0F\0\0~', 0.00, 7, '2019-11-12 06:43:20'),
(7961, '', 0.00, 6, '2019-11-12 06:43:20'),
(7962, '', 0.00, 7, '2019-11-12 06:43:20'),
(7963, '', 0.00, 6, '2019-11-12 06:43:21'),
(7964, '\0F\0\0\0G\0\0~', 0.00, 7, '2019-11-12 06:43:21'),
(7965, '', 0.00, 6, '2019-11-12 06:43:21'),
(7966, '', 0.00, 7, '2019-11-12 06:43:21'),
(7967, '', 0.00, 6, '2019-11-12 06:43:21'),
(7968, '\0G\0\0\0H\0\0~', 0.00, 7, '2019-11-12 06:43:21'),
(7969, '', 0.00, 6, '2019-11-12 06:43:21'),
(7970, '', 0.00, 7, '2019-11-12 06:43:21'),
(7971, '', 0.00, 6, '2019-11-12 06:43:21'),
(7972, '\0H\0\0\0I\0\0~', 0.00, 7, '2019-11-12 06:43:21'),
(7973, '', 0.00, 6, '2019-11-12 06:43:21'),
(7974, '', 0.00, 7, '2019-11-12 06:43:21'),
(7975, '', 0.00, 6, '2019-11-12 06:43:21'),
(7976, '\0I\0\0\0J\0\0~', 0.00, 7, '2019-11-12 06:43:21'),
(7977, '', 0.00, 6, '2019-11-12 06:43:21'),
(7978, '', 0.00, 7, '2019-11-12 06:43:21'),
(7979, '', 0.00, 6, '2019-11-12 06:43:21'),
(7980, '\0J\0\0\0K\0\0~', 0.00, 7, '2019-11-12 06:43:21'),
(7981, '', 0.00, 6, '2019-11-12 06:43:21'),
(7982, '', 0.00, 7, '2019-11-12 06:43:22'),
(7983, '', 0.00, 6, '2019-11-12 06:43:22'),
(7984, '\0K\0\0\0L\0\0~', 0.00, 7, '2019-11-12 06:43:22'),
(7985, '', 0.00, 6, '2019-11-12 06:43:22'),
(7986, '', 0.00, 7, '2019-11-12 06:43:22'),
(7987, '', 0.00, 6, '2019-11-12 06:43:22'),
(7988, '\0L\0\0\0M\0\0~', 0.00, 7, '2019-11-12 06:43:22'),
(7989, '', 0.00, 6, '2019-11-12 06:43:22'),
(7990, '', 0.00, 7, '2019-11-12 06:43:22'),
(7991, '', 0.00, 6, '2019-11-12 06:43:22'),
(7992, '\0M\0\0\0N\0\0~', 0.00, 7, '2019-11-12 06:43:22'),
(7993, '', 0.00, 6, '2019-11-12 06:43:23'),
(7994, '', 0.00, 7, '2019-11-12 06:43:23'),
(7995, '', 0.00, 6, '2019-11-12 06:43:23'),
(7996, '', 0.00, 7, '2019-11-12 06:43:23'),
(7997, '', 0.00, 6, '2019-11-12 06:43:23'),
(7998, '\0O\0\0\0P\0\0~', 0.00, 7, '2019-11-12 06:43:23'),
(7999, '', 0.00, 6, '2019-11-12 06:43:23'),
(8000, '', 0.00, 7, '2019-11-12 06:43:23'),
(8001, '', 0.00, 6, '2019-11-12 06:43:23'),
(8002, '\0P\0\0\0Q\0\0~', 0.00, 7, '2019-11-12 06:43:24'),
(8003, '', 0.00, 6, '2019-11-12 06:43:24'),
(8004, '', 0.00, 7, '2019-11-12 06:43:24'),
(8005, '', 0.00, 6, '2019-11-12 06:43:24'),
(8006, '\0Q\0\0\0R\0\0~', 0.00, 7, '2019-11-12 06:43:24'),
(8007, '', 0.00, 6, '2019-11-12 06:43:24'),
(8008, '', 0.00, 7, '2019-11-12 06:43:24'),
(8009, '', 0.00, 6, '2019-11-12 06:43:24'),
(8010, '\0R\0\0\0S\0\0~', 0.00, 7, '2019-11-12 06:43:24'),
(8011, '', 0.00, 6, '2019-11-12 06:43:24'),
(8012, '', 0.00, 7, '2019-11-12 06:43:24'),
(8013, '', 0.00, 6, '2019-11-12 06:43:24'),
(8014, '\0S\0\0\0T\0\0~', 0.00, 7, '2019-11-12 06:43:24'),
(8015, '', 0.00, 6, '2019-11-12 06:43:24'),
(8016, '', 0.00, 7, '2019-11-12 06:43:24'),
(8017, '', 0.00, 6, '2019-11-12 06:43:24'),
(8018, '\0T\0\0\0U\0\0~', 0.00, 7, '2019-11-12 06:43:24'),
(8019, '', 0.00, 6, '2019-11-12 06:43:25'),
(8020, '', 0.00, 7, '2019-11-12 06:43:25'),
(8021, '', 0.00, 6, '2019-11-12 06:43:25'),
(8022, '\0U\0\0\0V\0\0~', 0.00, 7, '2019-11-12 06:43:25'),
(8023, '', 0.00, 6, '2019-11-12 06:43:25'),
(8024, '', 0.00, 7, '2019-11-12 06:43:25'),
(8025, '', 0.00, 6, '2019-11-12 06:43:25'),
(8026, '\0V\0\0\0W\0\0~', 0.00, 7, '2019-11-12 06:43:25'),
(8027, '', 0.00, 6, '2019-11-12 06:43:25'),
(8028, '', 0.00, 7, '2019-11-12 06:43:25'),
(8029, '', 0.00, 6, '2019-11-12 06:43:25'),
(8030, '\0W\0\0\0X\0\0~', 0.00, 7, '2019-11-12 06:43:25'),
(8031, '', 0.00, 6, '2019-11-12 06:43:25'),
(8032, '', 0.00, 7, '2019-11-12 06:43:25'),
(8033, '', 0.00, 6, '2019-11-12 06:43:25'),
(8034, '\0X\0\0\0Y\0\0~', 0.00, 7, '2019-11-12 06:43:25'),
(8035, '', 0.00, 6, '2019-11-12 06:43:25'),
(8036, '', 0.00, 7, '2019-11-12 06:43:25'),
(8037, '', 0.00, 6, '2019-11-12 06:43:25'),
(8038, '\0Y\0\0\0Z\0\0~', 0.00, 7, '2019-11-12 06:43:25'),
(8039, '', 0.00, 6, '2019-11-12 06:43:25'),
(8040, '', 0.00, 7, '2019-11-12 06:43:25'),
(8041, '', 0.00, 6, '2019-11-12 06:43:25'),
(8042, '\0Z\0\0\0[\0\0~', 0.00, 7, '2019-11-12 06:43:26'),
(8043, '', 0.00, 6, '2019-11-12 06:43:26'),
(8044, '', 0.00, 7, '2019-11-12 06:43:26'),
(8045, '', 0.00, 6, '2019-11-12 06:43:26'),
(8046, '\0[\0\0\0\\\0\0~', 0.00, 7, '2019-11-12 06:43:26'),
(8047, '', 0.00, 6, '2019-11-12 06:43:26'),
(8048, '', 0.00, 7, '2019-11-12 06:43:26'),
(8049, '', 0.00, 6, '2019-11-12 06:43:26'),
(8050, '\0\\\0\0\0]\0\0~', 0.00, 7, '2019-11-12 06:43:26'),
(8051, '', 0.00, 6, '2019-11-12 06:43:26'),
(8052, '\0\\\0\0', 0.00, 7, '2019-11-12 06:43:26'),
(8053, '', 0.00, 6, '2019-11-12 06:43:26'),
(8054, '', 0.00, 7, '2019-11-12 06:43:26'),
(8055, '', 0.00, 6, '2019-11-12 06:43:26'),
(8056, '\0]\0\0\0^\0\0~', 0.00, 7, '2019-11-12 06:43:26'),
(8057, '', 0.00, 6, '2019-11-12 06:43:26'),
(8058, '', 0.00, 7, '2019-11-12 06:43:26'),
(8059, '', 0.00, 6, '2019-11-12 06:43:27'),
(8060, '\0^\0\0\0_\0\0~', 0.00, 7, '2019-11-12 06:43:27'),
(8061, '', 0.00, 6, '2019-11-12 06:43:27'),
(8062, '', 0.00, 7, '2019-11-12 06:43:28'),
(8063, '', 0.00, 6, '2019-11-12 06:43:28'),
(8064, '\0_\0\0\0`\0\0~', 0.00, 7, '2019-11-12 06:43:28'),
(8065, '', 0.00, 6, '2019-11-12 06:43:28'),
(8066, '\0_\0\0B)\0\0\0`\0\0\0', 0.00, 7, '2019-11-12 06:43:28'),
(8067, '', 0.00, 6, '2019-11-12 06:43:28'),
(8068, '\0`\0\0\0a\0\0~', 0.00, 7, '2019-11-12 06:43:28'),
(8069, '', 0.00, 6, '2019-11-12 06:43:28'),
(8070, '', 0.00, 7, '2019-11-12 06:43:28'),
(8071, '', 0.00, 6, '2019-11-12 06:43:28'),
(8072, '\0a\0\0\0b\0\0~', 0.00, 7, '2019-11-12 06:43:28'),
(8073, '', 0.00, 6, '2019-11-12 06:43:28'),
(8074, '', 0.00, 7, '2019-11-12 06:43:28'),
(8075, '', 0.00, 6, '2019-11-12 06:43:28'),
(8076, '\0b\0\0\0c\0\0~', 0.00, 7, '2019-11-12 06:43:28'),
(8077, '', 0.00, 6, '2019-11-12 06:43:28'),
(8078, '', 0.00, 7, '2019-11-12 06:43:29'),
(8079, '', 0.00, 6, '2019-11-12 06:43:29'),
(8080, '\0c\0\0\0d\0\0~', 0.00, 7, '2019-11-12 06:43:29'),
(8081, '', 0.00, 6, '2019-11-12 06:43:29'),
(8082, '', 0.00, 7, '2019-11-12 06:43:29'),
(8083, '', 0.00, 6, '2019-11-12 06:43:29'),
(8084, '\0d\0\0\0e\0\0~', 0.00, 7, '2019-11-12 06:43:29'),
(8085, '', 0.00, 6, '2019-11-12 06:43:29'),
(8086, '', 0.00, 7, '2019-11-12 06:43:29'),
(8087, '', 0.00, 6, '2019-11-12 06:43:29'),
(8088, '\0e\0\0\0f\0\0~', 0.00, 7, '2019-11-12 06:43:29'),
(8089, '', 0.00, 6, '2019-11-12 06:43:29'),
(8090, '', 0.00, 7, '2019-11-12 06:43:29'),
(8091, '', 0.00, 6, '2019-11-12 06:43:29'),
(8092, '\0f\0\0\0g\0\0~', 0.00, 7, '2019-11-12 06:43:29'),
(8093, '', 0.00, 6, '2019-11-12 06:43:29'),
(8094, '', 0.00, 7, '2019-11-12 06:43:29'),
(8095, '', 0.00, 6, '2019-11-12 06:43:29'),
(8096, '\0g\0\0\0h\0\0~', 0.00, 7, '2019-11-12 06:43:29'),
(8097, '', 0.00, 6, '2019-11-12 06:43:30'),
(8098, '', 0.00, 7, '2019-11-12 06:43:30'),
(8099, '', 0.00, 6, '2019-11-12 06:43:30'),
(8100, '\0h\0\0\0i\0\0~', 0.00, 7, '2019-11-12 06:43:30'),
(8101, '', 0.00, 6, '2019-11-12 06:43:30'),
(8102, '', 0.00, 7, '2019-11-12 06:43:30'),
(8103, '', 0.00, 6, '2019-11-12 06:43:30'),
(8104, '\0i\0\0\0j\0\0~', 0.00, 7, '2019-11-12 06:43:30'),
(8105, '', 0.00, 6, '2019-11-12 06:43:30'),
(8106, '', 0.00, 7, '2019-11-12 06:43:30'),
(8107, '', 0.00, 6, '2019-11-12 06:43:30'),
(8108, '\0j\0\0\0k\0\0~', 0.00, 7, '2019-11-12 06:43:30'),
(8109, '', 0.00, 6, '2019-11-12 06:43:30'),
(8110, '', 0.00, 7, '2019-11-12 06:43:30'),
(8111, '', 0.00, 6, '2019-11-12 06:43:30'),
(8112, '\0k\0\0\0l\0\0~', 0.00, 7, '2019-11-12 06:43:30'),
(8113, '', 0.00, 6, '2019-11-12 06:43:30'),
(8114, '', 0.00, 7, '2019-11-12 06:43:30'),
(8115, '', 0.00, 6, '2019-11-12 06:43:30'),
(8116, '\0l\0\0\0m\0\0~', 0.00, 7, '2019-11-12 06:43:30'),
(8117, '', 0.00, 6, '2019-11-12 06:43:30'),
(8118, '', 0.00, 7, '2019-11-12 06:43:31'),
(8119, '', 0.00, 6, '2019-11-12 06:43:31'),
(8120, '\0m\0\0\0n\0\0~', 0.00, 7, '2019-11-12 06:43:31'),
(8121, '', 0.00, 6, '2019-11-12 06:43:31'),
(8122, '', 0.00, 7, '2019-11-12 06:43:31'),
(8123, '', 0.00, 6, '2019-11-12 06:43:31'),
(8124, '\0n\0\0\0o\0\0~', 0.00, 7, '2019-11-12 06:43:31'),
(8125, '', 0.00, 6, '2019-11-12 06:43:31'),
(8126, '', 0.00, 7, '2019-11-12 06:43:31'),
(8127, '', 0.00, 6, '2019-11-12 06:43:31'),
(8128, '\0o\0\0\0p\0\0~', 0.00, 7, '2019-11-12 06:43:31'),
(8129, '', 0.00, 6, '2019-11-12 06:43:31'),
(8130, '', 0.00, 7, '2019-11-12 06:43:31'),
(8131, '', 0.00, 6, '2019-11-12 06:43:31'),
(8132, '\0p\0\0\0q\0\0~', 0.00, 7, '2019-11-12 06:43:31'),
(8133, '', 0.00, 6, '2019-11-12 06:43:31'),
(8134, '', 0.00, 7, '2019-11-12 06:43:31'),
(8135, '', 0.00, 6, '2019-11-12 06:43:31'),
(8136, '\0q\0\0\0r\0\0~', 0.00, 7, '2019-11-12 06:43:31'),
(8137, '', 0.00, 6, '2019-11-12 06:43:32'),
(8138, '', 0.00, 7, '2019-11-12 06:43:32'),
(8139, '', 0.00, 6, '2019-11-12 06:43:32'),
(8140, '\0r\0\0\0s\0\0~', 0.00, 7, '2019-11-12 06:43:32'),
(8141, '', 0.00, 6, '2019-11-12 06:43:32'),
(8142, '', 0.00, 7, '2019-11-12 06:43:32'),
(8143, '', 0.00, 6, '2019-11-12 06:43:32'),
(8144, '\0s\0\0\0t\0\0~', 0.00, 7, '2019-11-12 06:43:33'),
(8145, '', 0.00, 6, '2019-11-12 06:43:33'),
(8146, '', 0.00, 7, '2019-11-12 06:43:33'),
(8147, '', 0.00, 6, '2019-11-12 06:43:33'),
(8148, '\0t\0\0\0u\0\0~', 0.00, 7, '2019-11-12 06:43:33'),
(8149, '', 0.00, 6, '2019-11-12 06:43:33'),
(8150, '', 0.00, 7, '2019-11-12 06:43:33'),
(8151, '', 0.00, 6, '2019-11-12 06:43:33'),
(8152, '\0u\0\0\0v\0\0~', 0.00, 7, '2019-11-12 06:43:33'),
(8153, '', 0.00, 6, '2019-11-12 06:43:33'),
(8154, '', 0.00, 7, '2019-11-12 06:43:33'),
(8155, '', 0.00, 6, '2019-11-12 06:43:33'),
(8156, '\0v\0\0\0w\0\0~', 0.00, 7, '2019-11-12 06:43:34'),
(8157, '', 0.00, 6, '2019-11-12 06:43:34'),
(8158, '', 0.00, 7, '2019-11-12 06:43:34'),
(8159, '', 0.00, 6, '2019-11-12 06:43:34'),
(8160, '\0w\0\0\0x\0\0~', 0.00, 7, '2019-11-12 06:43:34'),
(8161, '', 0.00, 6, '2019-11-12 06:43:34'),
(8162, '', 0.00, 7, '2019-11-12 06:43:34'),
(8163, '', 0.00, 6, '2019-11-12 06:43:34'),
(8164, '\0x\0\0\0y\0\0~', 0.00, 7, '2019-11-12 06:43:34'),
(8165, '', 0.00, 6, '2019-11-12 06:43:35'),
(8166, '', 0.00, 7, '2019-11-12 06:43:35'),
(8167, '', 0.00, 6, '2019-11-12 06:43:35'),
(8168, '\0y\0\0\0z\0\0~', 0.00, 7, '2019-11-12 06:43:35'),
(8169, '', 0.00, 6, '2019-11-12 06:43:35'),
(8170, '', 0.00, 6, '2019-11-12 06:43:35'),
(8171, '', 0.00, 7, '2019-11-12 06:43:35'),
(8172, '', 0.00, 6, '2019-11-12 06:43:35'),
(8173, '\0z\0\0\0{\0\0~', 0.00, 7, '2019-11-12 06:43:35'),
(8174, '', 0.00, 6, '2019-11-12 06:43:36'),
(8175, '', 0.00, 7, '2019-11-12 06:43:36'),
(8176, '', 0.00, 6, '2019-11-12 06:43:36'),
(8177, '\0{\0\0\0|\0\0~', 0.00, 7, '2019-11-12 06:43:36'),
(8178, '', 0.00, 6, '2019-11-12 06:43:36'),
(8179, '\0{\0\0', 0.00, 7, '2019-11-12 06:43:36'),
(8180, '', 0.00, 6, '2019-11-12 06:43:36'),
(8181, '', 0.00, 7, '2019-11-12 06:43:36'),
(8182, '', 0.00, 6, '2019-11-12 06:43:36'),
(8183, '\0|\0\0\0}\0\0~', 0.00, 7, '2019-11-12 06:43:36'),
(8184, '', 0.00, 6, '2019-11-12 06:43:36'),
(8185, '', 0.00, 7, '2019-11-12 06:43:37'),
(8186, '', 0.00, 6, '2019-11-12 06:43:37'),
(8187, '\0}\0\0\0~\0\0~', 0.00, 7, '2019-11-12 06:43:37'),
(8188, '', 0.00, 6, '2019-11-12 06:43:37'),
(8189, '', 0.00, 7, '2019-11-12 06:43:37'),
(8190, '', 0.00, 6, '2019-11-12 06:43:37'),
(8191, '\0~\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:43:37'),
(8192, '', 0.00, 6, '2019-11-12 06:43:37'),
(8193, '', 0.00, 7, '2019-11-12 06:43:37'),
(8194, '', 0.00, 6, '2019-11-12 06:43:37'),
(8195, '', 0.00, 7, '2019-11-12 06:43:37'),
(8196, '', 0.00, 6, '2019-11-12 06:43:37'),
(8197, '', 0.00, 7, '2019-11-12 06:43:37'),
(8198, '', 0.00, 6, '2019-11-12 06:43:37'),
(8199, '', 0.00, 7, '2019-11-12 06:43:37'),
(8200, '', 0.00, 6, '2019-11-12 06:43:37'),
(8201, '', 0.00, 7, '2019-11-12 06:43:37'),
(8202, '', 0.00, 6, '2019-11-12 06:43:37'),
(8203, '', 0.00, 7, '2019-11-12 06:43:37'),
(8204, '', 0.00, 6, '2019-11-12 06:43:37'),
(8205, '', 0.00, 7, '2019-11-12 06:43:38'),
(8206, '', 0.00, 6, '2019-11-12 06:43:38'),
(8207, '', 0.00, 7, '2019-11-12 06:43:38'),
(8208, '', 0.00, 6, '2019-11-12 06:43:38'),
(8209, '', 0.00, 7, '2019-11-12 06:43:38'),
(8210, '', 0.00, 6, '2019-11-12 06:43:38'),
(8211, '', 0.00, 7, '2019-11-12 06:43:38'),
(8212, '', 0.00, 6, '2019-11-12 06:43:38'),
(8213, '', 0.00, 7, '2019-11-12 06:43:38'),
(8214, '', 0.00, 6, '2019-11-12 06:43:38'),
(8215, '', 0.00, 7, '2019-11-12 06:43:38'),
(8216, '', 0.00, 6, '2019-11-12 06:43:38'),
(8217, '', 0.00, 7, '2019-11-12 06:43:38'),
(8218, '', 0.00, 6, '2019-11-12 06:43:38'),
(8219, '', 0.00, 7, '2019-11-12 06:43:38'),
(8220, '', 0.00, 6, '2019-11-12 06:43:38'),
(8221, '', 0.00, 7, '2019-11-12 06:43:38'),
(8222, '', 0.00, 6, '2019-11-12 06:43:39'),
(8223, '', 0.00, 7, '2019-11-12 06:43:39'),
(8224, '', 0.00, 6, '2019-11-12 06:43:39'),
(8225, '', 0.00, 7, '2019-11-12 06:43:39'),
(8226, '', 0.00, 6, '2019-11-12 06:43:39'),
(8227, '', 0.00, 7, '2019-11-12 06:43:39'),
(8228, '', 0.00, 6, '2019-11-12 06:43:39'),
(8229, '', 0.00, 7, '2019-11-12 06:43:39'),
(8230, '', 0.00, 6, '2019-11-12 06:43:39'),
(8231, '', 0.00, 7, '2019-11-12 06:43:39'),
(8232, '', 0.00, 6, '2019-11-12 06:43:39'),
(8233, '', 0.00, 7, '2019-11-12 06:43:39'),
(8234, '', 0.00, 6, '2019-11-12 06:43:39'),
(8235, '', 0.00, 7, '2019-11-12 06:43:39'),
(8236, '', 0.00, 6, '2019-11-12 06:43:39'),
(8237, '', 0.00, 7, '2019-11-12 06:43:39'),
(8238, '', 0.00, 6, '2019-11-12 06:43:39'),
(8239, '', 0.00, 7, '2019-11-12 06:43:39'),
(8240, '', 0.00, 6, '2019-11-12 06:43:39'),
(8241, '', 0.00, 7, '2019-11-12 06:43:39'),
(8242, '', 0.00, 6, '2019-11-12 06:43:40'),
(8243, '', 0.00, 7, '2019-11-12 06:43:40'),
(8244, '', 0.00, 6, '2019-11-12 06:43:40'),
(8245, '', 0.00, 7, '2019-11-12 06:43:40'),
(8246, '', 0.00, 6, '2019-11-12 06:43:40'),
(8247, '', 0.00, 7, '2019-11-12 06:43:40'),
(8248, '', 0.00, 6, '2019-11-12 06:43:40'),
(8249, '', 0.00, 7, '2019-11-12 06:43:40'),
(8250, '', 0.00, 6, '2019-11-12 06:43:40'),
(8251, '', 0.00, 7, '2019-11-12 06:43:40'),
(8252, '', 0.00, 6, '2019-11-12 06:43:40'),
(8253, '', 0.00, 7, '2019-11-12 06:43:40'),
(8254, '', 0.00, 6, '2019-11-12 06:43:40'),
(8255, '', 0.00, 7, '2019-11-12 06:43:40'),
(8256, '', 0.00, 6, '2019-11-12 06:43:40'),
(8257, '', 0.00, 7, '2019-11-12 06:43:40'),
(8258, '', 0.00, 6, '2019-11-12 06:43:40'),
(8259, '', 0.00, 7, '2019-11-12 06:43:40'),
(8260, '', 0.00, 6, '2019-11-12 06:43:40'),
(8261, '', 0.00, 7, '2019-11-12 06:43:41'),
(8262, '', 0.00, 6, '2019-11-12 06:43:41'),
(8263, '', 0.00, 7, '2019-11-12 06:43:41'),
(8264, '', 0.00, 6, '2019-11-12 06:43:41'),
(8265, '', 0.00, 7, '2019-11-12 06:43:41'),
(8266, '', 0.00, 6, '2019-11-12 06:43:41'),
(8267, '', 0.00, 7, '2019-11-12 06:43:41'),
(8268, '', 0.00, 6, '2019-11-12 06:43:41'),
(8269, '', 0.00, 7, '2019-11-12 06:43:41'),
(8270, '', 0.00, 6, '2019-11-12 06:43:41'),
(8271, '', 0.00, 7, '2019-11-12 06:43:41'),
(8272, '', 0.00, 6, '2019-11-12 06:43:41'),
(8273, '', 0.00, 7, '2019-11-12 06:43:41'),
(8274, '', 0.00, 6, '2019-11-12 06:43:41'),
(8275, '', 0.00, 7, '2019-11-12 06:43:41'),
(8276, '', 0.00, 6, '2019-11-12 06:43:41'),
(8277, '', 0.00, 7, '2019-11-12 06:43:41'),
(8278, '', 0.00, 6, '2019-11-12 06:43:42'),
(8279, '', 0.00, 7, '2019-11-12 06:43:42'),
(8280, '', 0.00, 6, '2019-11-12 06:43:42'),
(8281, '', 0.00, 7, '2019-11-12 06:43:42'),
(8282, '', 0.00, 6, '2019-11-12 06:43:42'),
(8283, '', 0.00, 7, '2019-11-12 06:43:42'),
(8284, '', 0.00, 6, '2019-11-12 06:43:42'),
(8285, '', 0.00, 7, '2019-11-12 06:43:42'),
(8286, '', 0.00, 6, '2019-11-12 06:43:42'),
(8287, '', 0.00, 7, '2019-11-12 06:43:42'),
(8288, '', 0.00, 6, '2019-11-12 06:43:42'),
(8289, '', 0.00, 7, '2019-11-12 06:43:42'),
(8290, '', 0.00, 6, '2019-11-12 06:43:42'),
(8291, '', 0.00, 7, '2019-11-12 06:43:42'),
(8292, '', 0.00, 6, '2019-11-12 06:43:42'),
(8293, '', 0.00, 7, '2019-11-12 06:43:42'),
(8294, '', 0.00, 6, '2019-11-12 06:43:43'),
(8295, '', 0.00, 7, '2019-11-12 06:43:43'),
(8296, '', 0.00, 6, '2019-11-12 06:43:43'),
(8297, '', 0.00, 7, '2019-11-12 06:43:43'),
(8298, '', 0.00, 6, '2019-11-12 06:43:43'),
(8299, '', 0.00, 7, '2019-11-12 06:43:43'),
(8300, '', 0.00, 6, '2019-11-12 06:43:43'),
(8301, '', 0.00, 7, '2019-11-12 06:43:43'),
(8302, '', 0.00, 6, '2019-11-12 06:43:43'),
(8303, '', 0.00, 7, '2019-11-12 06:43:43'),
(8304, '', 0.00, 6, '2019-11-12 06:43:43'),
(8305, '', 0.00, 7, '2019-11-12 06:43:43'),
(8306, '', 0.00, 6, '2019-11-12 06:43:43'),
(8307, '', 0.00, 7, '2019-11-12 06:43:43'),
(8308, '', 0.00, 6, '2019-11-12 06:43:43'),
(8309, '', 0.00, 7, '2019-11-12 06:43:43'),
(8310, '', 0.00, 6, '2019-11-12 06:43:43'),
(8311, '', 0.00, 7, '2019-11-12 06:43:43'),
(8312, '', 0.00, 6, '2019-11-12 06:43:43'),
(8313, '', 0.00, 7, '2019-11-12 06:43:44'),
(8314, '', 0.00, 6, '2019-11-12 06:43:44'),
(8315, '', 0.00, 7, '2019-11-12 06:43:44'),
(8316, '', 0.00, 6, '2019-11-12 06:43:44'),
(8317, '', 0.00, 7, '2019-11-12 06:43:44'),
(8318, '', 0.00, 6, '2019-11-12 06:43:44'),
(8319, '', 0.00, 7, '2019-11-12 06:43:44'),
(8320, '', 0.00, 6, '2019-11-12 06:43:44'),
(8321, '', 0.00, 7, '2019-11-12 06:43:44'),
(8322, '', 0.00, 6, '2019-11-12 06:43:44'),
(8323, '', 0.00, 7, '2019-11-12 06:43:44'),
(8324, '', 0.00, 6, '2019-11-12 06:43:44'),
(8325, '', 0.00, 7, '2019-11-12 06:43:44'),
(8326, '', 0.00, 6, '2019-11-12 06:43:44'),
(8327, '', 0.00, 7, '2019-11-12 06:43:44'),
(8328, '', 0.00, 6, '2019-11-12 06:43:44'),
(8329, '', 0.00, 7, '2019-11-12 06:43:44'),
(8330, '', 0.00, 6, '2019-11-12 06:43:44'),
(8331, '', 0.00, 7, '2019-11-12 06:43:44'),
(8332, '', 0.00, 6, '2019-11-12 06:43:44'),
(8333, '', 0.00, 7, '2019-11-12 06:43:44'),
(8334, '', 0.00, 6, '2019-11-12 06:43:44'),
(8335, '', 0.00, 7, '2019-11-12 06:43:44'),
(8336, '', 0.00, 6, '2019-11-12 06:43:44'),
(8337, '', 0.00, 7, '2019-11-12 06:43:45'),
(8338, '', 0.00, 6, '2019-11-12 06:43:45'),
(8339, '', 0.00, 7, '2019-11-12 06:43:45'),
(8340, '', 0.00, 6, '2019-11-12 06:43:45'),
(8341, '', 0.00, 7, '2019-11-12 06:43:45'),
(8342, '', 0.00, 6, '2019-11-12 06:43:45'),
(8343, '', 0.00, 7, '2019-11-12 06:43:45'),
(8344, '', 0.00, 6, '2019-11-12 06:43:45'),
(8345, '', 0.00, 7, '2019-11-12 06:43:45'),
(8346, '', 0.00, 6, '2019-11-12 06:43:45'),
(8347, '', 0.00, 7, '2019-11-12 06:43:45'),
(8348, '\0\0\0', 0.00, 6, '2019-11-12 06:43:45'),
(8349, '', 0.00, 7, '2019-11-12 06:43:45'),
(8350, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:45'),
(8351, '', 0.00, 7, '2019-11-12 06:43:45'),
(8352, '', 0.00, 6, '2019-11-12 06:43:45'),
(8353, '', 0.00, 7, '2019-11-12 06:43:45'),
(8354, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:45'),
(8355, '', 0.00, 7, '2019-11-12 06:43:45'),
(8356, '', 0.00, 6, '2019-11-12 06:43:46'),
(8357, '', 0.00, 7, '2019-11-12 06:43:46'),
(8358, '', 0.00, 6, '2019-11-12 06:43:46'),
(8359, '', 0.00, 7, '2019-11-12 06:43:46'),
(8360, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:46'),
(8361, '', 0.00, 7, '2019-11-12 06:43:46'),
(8362, '', 0.00, 6, '2019-11-12 06:43:46'),
(8363, '', 0.00, 7, '2019-11-12 06:43:46'),
(8364, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:46'),
(8365, '', 0.00, 7, '2019-11-12 06:43:46'),
(8366, '', 0.00, 6, '2019-11-12 06:43:47'),
(8367, '', 0.00, 7, '2019-11-12 06:43:47'),
(8368, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:47'),
(8369, '', 0.00, 7, '2019-11-12 06:43:47'),
(8370, '', 0.00, 6, '2019-11-12 06:43:47'),
(8371, '', 0.00, 7, '2019-11-12 06:43:47'),
(8372, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:47'),
(8373, '', 0.00, 7, '2019-11-12 06:43:47'),
(8374, '', 0.00, 6, '2019-11-12 06:43:47'),
(8375, '', 0.00, 7, '2019-11-12 06:43:47'),
(8376, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:48'),
(8377, '', 0.00, 7, '2019-11-12 06:43:48'),
(8378, '', 0.00, 6, '2019-11-12 06:43:48'),
(8379, '', 0.00, 7, '2019-11-12 06:43:48'),
(8380, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:48'),
(8381, '', 0.00, 7, '2019-11-12 06:43:48'),
(8382, '', 0.00, 6, '2019-11-12 06:43:48'),
(8383, '', 0.00, 7, '2019-11-12 06:43:48'),
(8384, '', 0.00, 6, '2019-11-12 06:43:48'),
(8385, '', 0.00, 7, '2019-11-12 06:43:48'),
(8386, '\0', 0.00, 6, '2019-11-12 06:43:48'),
(8387, '', 0.00, 7, '2019-11-12 06:43:48'),
(8388, '\0\0\0', 0.00, 6, '2019-11-12 06:43:48'),
(8389, '', 0.00, 7, '2019-11-12 06:43:48'),
(8390, '\0\0~', 0.00, 6, '2019-11-12 06:43:48'),
(8391, '', 0.00, 7, '2019-11-12 06:43:49'),
(8392, '\0', 0.00, 6, '2019-11-12 06:43:49'),
(8393, '', 0.00, 7, '2019-11-12 06:43:49'),
(8394, '', 0.00, 6, '2019-11-12 06:43:49'),
(8395, '', 0.00, 7, '2019-11-12 06:43:49'),
(8396, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:49'),
(8397, '', 0.00, 7, '2019-11-12 06:43:49'),
(8398, '\0\0\0', 0.00, 6, '2019-11-12 06:43:49'),
(8399, '', 0.00, 7, '2019-11-12 06:43:49'),
(8400, '', 0.00, 6, '2019-11-12 06:43:49'),
(8401, '', 0.00, 7, '2019-11-12 06:43:49'),
(8402, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:49'),
(8403, '', 0.00, 7, '2019-11-12 06:43:49'),
(8404, '', 0.00, 6, '2019-11-12 06:43:49'),
(8405, '', 0.00, 7, '2019-11-12 06:43:49'),
(8406, '\0\r\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:43:49'),
(8407, '', 0.00, 7, '2019-11-12 06:43:49'),
(8408, '', 0.00, 6, '2019-11-12 06:43:49'),
(8409, '', 0.00, 7, '2019-11-12 06:43:49'),
(8410, '', 0.00, 6, '2019-11-12 06:43:50'),
(8411, '', 0.00, 7, '2019-11-12 06:43:50'),
(8412, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:50'),
(8413, '', 0.00, 7, '2019-11-12 06:43:50'),
(8414, '', 0.00, 6, '2019-11-12 06:43:50'),
(8415, '', 0.00, 7, '2019-11-12 06:43:50'),
(8416, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:50'),
(8417, '', 0.00, 7, '2019-11-12 06:43:50'),
(8418, '', 0.00, 6, '2019-11-12 06:43:50'),
(8419, '', 0.00, 7, '2019-11-12 06:43:50'),
(8420, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:50'),
(8421, '', 0.00, 7, '2019-11-12 06:43:50'),
(8422, '', 0.00, 6, '2019-11-12 06:43:50'),
(8423, '', 0.00, 7, '2019-11-12 06:43:50'),
(8424, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:50'),
(8425, '', 0.00, 7, '2019-11-12 06:43:50'),
(8426, '', 0.00, 6, '2019-11-12 06:43:50'),
(8427, '', 0.00, 7, '2019-11-12 06:43:50'),
(8428, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:50'),
(8429, '', 0.00, 7, '2019-11-12 06:43:50'),
(8430, '', 0.00, 6, '2019-11-12 06:43:51'),
(8431, '', 0.00, 7, '2019-11-12 06:43:51'),
(8432, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:51'),
(8433, '', 0.00, 7, '2019-11-12 06:43:51'),
(8434, '', 0.00, 6, '2019-11-12 06:43:51'),
(8435, '', 0.00, 7, '2019-11-12 06:43:51'),
(8436, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:51'),
(8437, '', 0.00, 7, '2019-11-12 06:43:51'),
(8438, '', 0.00, 6, '2019-11-12 06:43:51'),
(8439, '', 0.00, 7, '2019-11-12 06:43:51'),
(8440, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:51'),
(8441, '', 0.00, 7, '2019-11-12 06:43:51'),
(8442, '', 0.00, 6, '2019-11-12 06:43:51'),
(8443, '', 0.00, 7, '2019-11-12 06:43:52'),
(8444, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:52'),
(8445, '', 0.00, 7, '2019-11-12 06:43:52'),
(8446, '', 0.00, 6, '2019-11-12 06:43:52'),
(8447, '', 0.00, 7, '2019-11-12 06:43:52'),
(8448, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:52'),
(8449, '', 0.00, 7, '2019-11-12 06:43:52'),
(8450, '\0\0\0', 0.00, 6, '2019-11-12 06:43:52'),
(8451, '', 0.00, 7, '2019-11-12 06:43:52'),
(8452, '', 0.00, 6, '2019-11-12 06:43:52'),
(8453, '', 0.00, 7, '2019-11-12 06:43:52'),
(8454, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:52'),
(8455, '', 0.00, 7, '2019-11-12 06:43:52'),
(8456, '', 0.00, 6, '2019-11-12 06:43:52'),
(8457, '', 0.00, 7, '2019-11-12 06:43:52'),
(8458, '\0\Z\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:43:52'),
(8459, '', 0.00, 7, '2019-11-12 06:43:52'),
(8460, '', 0.00, 6, '2019-11-12 06:43:53'),
(8461, '', 0.00, 7, '2019-11-12 06:43:53'),
(8462, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:53');
INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(8463, '', 0.00, 7, '2019-11-12 06:43:53'),
(8464, '', 0.00, 6, '2019-11-12 06:43:53'),
(8465, '', 0.00, 7, '2019-11-12 06:43:53'),
(8466, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:53'),
(8467, '', 0.00, 7, '2019-11-12 06:43:53'),
(8468, '', 0.00, 6, '2019-11-12 06:43:53'),
(8469, '', 0.00, 7, '2019-11-12 06:43:53'),
(8470, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:54'),
(8471, '', 0.00, 7, '2019-11-12 06:43:54'),
(8472, '', 0.00, 6, '2019-11-12 06:43:54'),
(8473, '', 0.00, 7, '2019-11-12 06:43:54'),
(8474, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:54'),
(8475, '', 0.00, 7, '2019-11-12 06:43:54'),
(8476, '', 0.00, 6, '2019-11-12 06:43:54'),
(8477, '', 0.00, 7, '2019-11-12 06:43:54'),
(8478, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:43:54'),
(8479, '', 0.00, 7, '2019-11-12 06:43:54'),
(8480, '', 0.00, 6, '2019-11-12 06:43:54'),
(8481, '', 0.00, 7, '2019-11-12 06:43:54'),
(8482, '\0 \0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:43:54'),
(8483, '', 0.00, 7, '2019-11-12 06:43:54'),
(8484, '', 0.00, 6, '2019-11-12 06:43:54'),
(8485, '', 0.00, 7, '2019-11-12 06:43:54'),
(8486, '\0!\0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:43:54'),
(8487, '', 0.00, 7, '2019-11-12 06:43:54'),
(8488, '', 0.00, 6, '2019-11-12 06:43:55'),
(8489, '', 0.00, 7, '2019-11-12 06:43:55'),
(8490, '\0&quot;\0\0\0&quot;\0\0~', 0.00, 6, '2019-11-12 06:43:55'),
(8491, '', 0.00, 7, '2019-11-12 06:43:55'),
(8492, '', 0.00, 6, '2019-11-12 06:43:55'),
(8493, '', 0.00, 7, '2019-11-12 06:43:55'),
(8494, '\0#\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:43:55'),
(8495, '', 0.00, 7, '2019-11-12 06:43:55'),
(8496, '', 0.00, 6, '2019-11-12 06:43:55'),
(8497, '', 0.00, 7, '2019-11-12 06:43:55'),
(8498, '\0$\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:43:55'),
(8499, '', 0.00, 7, '2019-11-12 06:43:55'),
(8500, '', 0.00, 6, '2019-11-12 06:43:55'),
(8501, '', 0.00, 7, '2019-11-12 06:43:55'),
(8502, '\0%\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:43:55'),
(8503, '', 0.00, 7, '2019-11-12 06:43:55'),
(8504, '', 0.00, 6, '2019-11-12 06:43:55'),
(8505, '', 0.00, 7, '2019-11-12 06:43:55'),
(8506, '\0&amp;\0\0\0&amp;\0\0~', 0.00, 6, '2019-11-12 06:43:55'),
(8507, '', 0.00, 7, '2019-11-12 06:43:55'),
(8508, '', 0.00, 6, '2019-11-12 06:43:56'),
(8509, '', 0.00, 7, '2019-11-12 06:43:56'),
(8510, '\0\'\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:43:56'),
(8511, '', 0.00, 7, '2019-11-12 06:43:56'),
(8512, '', 0.00, 6, '2019-11-12 06:43:56'),
(8513, '', 0.00, 7, '2019-11-12 06:43:56'),
(8514, '\0(\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:43:56'),
(8515, '', 0.00, 7, '2019-11-12 06:43:56'),
(8516, '', 0.00, 6, '2019-11-12 06:43:56'),
(8517, '', 0.00, 7, '2019-11-12 06:43:56'),
(8518, '\0)\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:43:56'),
(8519, '', 0.00, 7, '2019-11-12 06:43:57'),
(8520, '', 0.00, 6, '2019-11-12 06:43:57'),
(8521, '', 0.00, 7, '2019-11-12 06:43:57'),
(8522, '\0*\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:43:57'),
(8523, '', 0.00, 7, '2019-11-12 06:43:57'),
(8524, '', 0.00, 6, '2019-11-12 06:43:57'),
(8525, '', 0.00, 7, '2019-11-12 06:43:57'),
(8526, '\0+\0\0\0+\0\0~', 0.00, 6, '2019-11-12 06:43:57'),
(8527, '', 0.00, 7, '2019-11-12 06:43:57'),
(8528, '', 0.00, 6, '2019-11-12 06:43:57'),
(8529, '', 0.00, 7, '2019-11-12 06:43:57'),
(8530, '\0', 0.00, 6, '2019-11-12 06:43:58'),
(8531, '', 0.00, 7, '2019-11-12 06:43:58'),
(8532, '\0', 0.00, 6, '2019-11-12 06:43:59'),
(8533, '', 0.00, 7, '2019-11-12 06:43:59'),
(8534, '\0-\0\0\0-\0\0~', 0.00, 6, '2019-11-12 06:43:59'),
(8535, '', 0.00, 7, '2019-11-12 06:43:59'),
(8536, '', 0.00, 6, '2019-11-12 06:43:59'),
(8537, '', 0.00, 7, '2019-11-12 06:43:59'),
(8538, '\0.\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:43:59'),
(8539, '', 0.00, 7, '2019-11-12 06:43:59'),
(8540, '', 0.00, 6, '2019-11-12 06:43:59'),
(8541, '', 0.00, 7, '2019-11-12 06:43:59'),
(8542, '\0/\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:43:59'),
(8543, '', 0.00, 7, '2019-11-12 06:43:59'),
(8544, '', 0.00, 6, '2019-11-12 06:43:59'),
(8545, '', 0.00, 7, '2019-11-12 06:43:59'),
(8546, '\00\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:43:59'),
(8547, '', 0.00, 7, '2019-11-12 06:43:59'),
(8548, '', 0.00, 6, '2019-11-12 06:43:59'),
(8549, '', 0.00, 7, '2019-11-12 06:43:59'),
(8550, '', 0.00, 6, '2019-11-12 06:44:00'),
(8551, '', 0.00, 7, '2019-11-12 06:44:01'),
(8552, '\02\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:44:01'),
(8553, '', 0.00, 7, '2019-11-12 06:44:01'),
(8554, '', 0.00, 6, '2019-11-12 06:44:01'),
(8555, '', 0.00, 7, '2019-11-12 06:44:01'),
(8556, '\03\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:44:01'),
(8557, '', 0.00, 7, '2019-11-12 06:44:01'),
(8558, '', 0.00, 6, '2019-11-12 06:44:01'),
(8559, '', 0.00, 7, '2019-11-12 06:44:01'),
(8560, '\04\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:44:01'),
(8561, '', 0.00, 7, '2019-11-12 06:44:01'),
(8562, '', 0.00, 6, '2019-11-12 06:44:01'),
(8563, '', 0.00, 7, '2019-11-12 06:44:01'),
(8564, '\05\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:44:02'),
(8565, '', 0.00, 7, '2019-11-12 06:44:02'),
(8566, '', 0.00, 6, '2019-11-12 06:44:02'),
(8567, '', 0.00, 7, '2019-11-12 06:44:02'),
(8568, '\06\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:44:02'),
(8569, '', 0.00, 7, '2019-11-12 06:44:02'),
(8570, '\06\0\0', 0.00, 6, '2019-11-12 06:44:03'),
(8571, '', 0.00, 7, '2019-11-12 06:44:03'),
(8572, '', 0.00, 6, '2019-11-12 06:44:03'),
(8573, '', 0.00, 7, '2019-11-12 06:44:03'),
(8574, '\07\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:44:03'),
(8575, '', 0.00, 7, '2019-11-12 06:44:03'),
(8576, '', 0.00, 6, '2019-11-12 06:44:03'),
(8577, '', 0.00, 7, '2019-11-12 06:44:03'),
(8578, '\08\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:44:03'),
(8579, '', 0.00, 7, '2019-11-12 06:44:03'),
(8580, '', 0.00, 6, '2019-11-12 06:44:03'),
(8581, '', 0.00, 7, '2019-11-12 06:44:03'),
(8582, '\09\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:44:03'),
(8583, '', 0.00, 7, '2019-11-12 06:44:03'),
(8584, '', 0.00, 6, '2019-11-12 06:44:03'),
(8585, '', 0.00, 7, '2019-11-12 06:44:04'),
(8586, '\0:\0\0\0:\0\0~', 0.00, 6, '2019-11-12 06:44:04'),
(8587, '', 0.00, 7, '2019-11-12 06:44:04'),
(8588, '', 0.00, 6, '2019-11-12 06:44:04'),
(8589, '', 0.00, 7, '2019-11-12 06:44:04'),
(8590, '\0;\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:44:04'),
(8591, '', 0.00, 7, '2019-11-12 06:44:04'),
(8592, '', 0.00, 6, '2019-11-12 06:44:04'),
(8593, '', 0.00, 7, '2019-11-12 06:44:04'),
(8594, '\0&lt;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:44:04'),
(8595, '', 0.00, 7, '2019-11-12 06:44:04'),
(8596, '', 0.00, 6, '2019-11-12 06:44:04'),
(8597, '', 0.00, 7, '2019-11-12 06:44:04'),
(8598, '\0=\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:44:04'),
(8599, '', 0.00, 7, '2019-11-12 06:44:04'),
(8600, '', 0.00, 6, '2019-11-12 06:44:04'),
(8601, '', 0.00, 7, '2019-11-12 06:44:04'),
(8602, '\0&gt;\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:44:04'),
(8603, '', 0.00, 7, '2019-11-12 06:44:04'),
(8604, '', 0.00, 6, '2019-11-12 06:44:04'),
(8605, '', 0.00, 7, '2019-11-12 06:44:04'),
(8606, '\0?\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:44:04'),
(8607, '', 0.00, 7, '2019-11-12 06:44:04'),
(8608, '\0?\0\0!\0\0\0@\0\0\0', 0.00, 6, '2019-11-12 06:44:04'),
(8609, '', 0.00, 7, '2019-11-12 06:44:04'),
(8610, '\0@\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:44:04'),
(8611, '', 0.00, 7, '2019-11-12 06:44:05'),
(8612, '', 0.00, 6, '2019-11-12 06:44:05'),
(8613, '', 0.00, 7, '2019-11-12 06:44:05'),
(8614, '\0A\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:44:05'),
(8615, '', 0.00, 7, '2019-11-12 06:44:05'),
(8616, '', 0.00, 6, '2019-11-12 06:44:05'),
(8617, '', 0.00, 7, '2019-11-12 06:44:05'),
(8618, '\0B\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:44:05'),
(8619, '', 0.00, 7, '2019-11-12 06:44:06'),
(8620, '', 0.00, 6, '2019-11-12 06:44:06'),
(8621, '', 0.00, 7, '2019-11-12 06:44:06'),
(8622, '\0C\0\0\0C\0\0~', 0.00, 6, '2019-11-12 06:44:06'),
(8623, '', 0.00, 7, '2019-11-12 06:44:06'),
(8624, '', 0.00, 6, '2019-11-12 06:44:06'),
(8625, '', 0.00, 7, '2019-11-12 06:44:07'),
(8626, '\0D\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:44:07'),
(8627, '', 0.00, 7, '2019-11-12 06:44:07'),
(8628, '', 0.00, 6, '2019-11-12 06:44:07'),
(8629, '', 0.00, 7, '2019-11-12 06:44:07'),
(8630, '\0E\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:44:07'),
(8631, '', 0.00, 7, '2019-11-12 06:44:07'),
(8632, '', 0.00, 6, '2019-11-12 06:44:07'),
(8633, '', 0.00, 7, '2019-11-12 06:44:07'),
(8634, '\0F\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:44:07'),
(8635, '', 0.00, 7, '2019-11-12 06:44:07'),
(8636, '', 0.00, 6, '2019-11-12 06:44:08'),
(8637, '', 0.00, 7, '2019-11-12 06:44:08'),
(8638, '\0G\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:44:08'),
(8639, '', 0.00, 7, '2019-11-12 06:44:08'),
(8640, '', 0.00, 6, '2019-11-12 06:44:08'),
(8641, '', 0.00, 7, '2019-11-12 06:44:08'),
(8642, '\0H\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:44:08'),
(8643, '', 0.00, 7, '2019-11-12 06:44:08'),
(8644, '', 0.00, 6, '2019-11-12 06:44:08'),
(8645, '', 0.00, 7, '2019-11-12 06:44:08'),
(8646, '\0I\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:44:08'),
(8647, '', 0.00, 7, '2019-11-12 06:44:08'),
(8648, '', 0.00, 6, '2019-11-12 06:44:08'),
(8649, '', 0.00, 7, '2019-11-12 06:44:09'),
(8650, '\0J\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:44:09'),
(8651, '', 0.00, 7, '2019-11-12 06:44:09'),
(8652, '', 0.00, 6, '2019-11-12 06:44:09'),
(8653, '', 0.00, 7, '2019-11-12 06:44:09'),
(8654, '\0K\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:44:09'),
(8655, '', 0.00, 7, '2019-11-12 06:44:09'),
(8656, '', 0.00, 6, '2019-11-12 06:44:09'),
(8657, '', 0.00, 7, '2019-11-12 06:44:09'),
(8658, '\0L\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:44:09'),
(8659, '', 0.00, 7, '2019-11-12 06:44:09'),
(8660, '', 0.00, 6, '2019-11-12 06:44:09'),
(8661, '', 0.00, 7, '2019-11-12 06:44:09'),
(8662, '\0M\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:44:09'),
(8663, '', 0.00, 7, '2019-11-12 06:44:09'),
(8664, '', 0.00, 6, '2019-11-12 06:44:09'),
(8665, '', 0.00, 7, '2019-11-12 06:44:10'),
(8666, '\0N\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:44:10'),
(8667, '', 0.00, 7, '2019-11-12 06:44:10'),
(8668, '', 0.00, 6, '2019-11-12 06:44:10'),
(8669, '', 0.00, 7, '2019-11-12 06:44:10'),
(8670, '\0O\0\0\0O\0\0~', 0.00, 6, '2019-11-12 06:44:10'),
(8671, '', 0.00, 7, '2019-11-12 06:44:10'),
(8672, '', 0.00, 6, '2019-11-12 06:44:10'),
(8673, '', 0.00, 7, '2019-11-12 06:44:10'),
(8674, '\0P\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:44:10'),
(8675, '', 0.00, 7, '2019-11-12 06:44:10'),
(8676, '', 0.00, 6, '2019-11-12 06:44:10'),
(8677, '', 0.00, 7, '2019-11-12 06:44:10'),
(8678, '\0Q\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:44:10'),
(8679, '', 0.00, 7, '2019-11-12 06:44:10'),
(8680, '', 0.00, 6, '2019-11-12 06:44:10'),
(8681, '', 0.00, 7, '2019-11-12 06:44:10'),
(8682, '\0R\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:44:10'),
(8683, '', 0.00, 7, '2019-11-12 06:44:11'),
(8684, '', 0.00, 6, '2019-11-12 06:44:11'),
(8685, '', 0.00, 7, '2019-11-12 06:44:11'),
(8686, '\0S\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:44:11'),
(8687, '', 0.00, 7, '2019-11-12 06:44:11'),
(8688, '', 0.00, 6, '2019-11-12 06:44:11'),
(8689, '', 0.00, 7, '2019-11-12 06:44:11'),
(8690, '\0T\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:44:11'),
(8691, '', 0.00, 7, '2019-11-12 06:44:11'),
(8692, '\0T\0\0R', 0.00, 6, '2019-11-12 06:44:11'),
(8693, '', 0.00, 7, '2019-11-12 06:44:11'),
(8694, '', 0.00, 6, '2019-11-12 06:44:11'),
(8695, '', 0.00, 7, '2019-11-12 06:44:11'),
(8696, '\0U\0\0\0U\0\0~', 0.00, 6, '2019-11-12 06:44:11'),
(8697, '', 0.00, 7, '2019-11-12 06:44:11'),
(8698, '', 0.00, 6, '2019-11-12 06:44:11'),
(8699, '', 0.00, 7, '2019-11-12 06:44:11'),
(8700, '\0V\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:44:11'),
(8701, '', 0.00, 7, '2019-11-12 06:44:11'),
(8702, '', 0.00, 6, '2019-11-12 06:44:11'),
(8703, '', 0.00, 7, '2019-11-12 06:44:12'),
(8704, '\0W\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:44:12'),
(8705, '', 0.00, 7, '2019-11-12 06:44:12'),
(8706, '', 0.00, 6, '2019-11-12 06:44:12'),
(8707, '', 0.00, 7, '2019-11-12 06:44:12'),
(8708, '\0X\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:44:12'),
(8709, '', 0.00, 7, '2019-11-12 06:44:12'),
(8710, '', 0.00, 6, '2019-11-12 06:44:12'),
(8711, '\0\0\0', 0.00, 7, '2019-11-12 06:44:12'),
(8712, '\0Y\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:44:12'),
(8713, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:12'),
(8714, '', 0.00, 6, '2019-11-12 06:44:12'),
(8715, '', 0.00, 7, '2019-11-12 06:44:12'),
(8716, '\0Z\0\0\0Z\0\0~', 0.00, 6, '2019-11-12 06:44:12'),
(8717, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:12'),
(8718, '', 0.00, 6, '2019-11-12 06:44:12'),
(8719, '', 0.00, 7, '2019-11-12 06:44:12'),
(8720, '', 0.00, 6, '2019-11-12 06:44:12'),
(8721, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:12'),
(8722, '\0\\\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:44:12'),
(8723, '', 0.00, 7, '2019-11-12 06:44:12'),
(8724, '', 0.00, 6, '2019-11-12 06:44:13'),
(8725, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:13'),
(8726, '\0]\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:44:13'),
(8727, '', 0.00, 7, '2019-11-12 06:44:13'),
(8728, '', 0.00, 6, '2019-11-12 06:44:13'),
(8729, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:13'),
(8730, '', 0.00, 6, '2019-11-12 06:44:13'),
(8731, '', 0.00, 7, '2019-11-12 06:44:13'),
(8732, '\0_\0\0\0_\0\0~', 0.00, 6, '2019-11-12 06:44:13'),
(8733, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:13'),
(8734, '\0_\0\0*\0\0\0`\0\0\0', 0.00, 6, '2019-11-12 06:44:13'),
(8735, '', 0.00, 7, '2019-11-12 06:44:13'),
(8736, '\0`\0\0\0`\0\0~', 0.00, 6, '2019-11-12 06:44:14'),
(8737, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:14'),
(8738, '', 0.00, 6, '2019-11-12 06:44:14'),
(8739, '', 0.00, 7, '2019-11-12 06:44:14'),
(8740, '\0a\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:44:14'),
(8741, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:14'),
(8742, '\0a\0\0', 0.00, 6, '2019-11-12 06:44:14'),
(8743, '', 0.00, 7, '2019-11-12 06:44:14'),
(8744, '', 0.00, 6, '2019-11-12 06:44:14'),
(8745, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:14'),
(8746, '\0b\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:44:14'),
(8747, '', 0.00, 7, '2019-11-12 06:44:14'),
(8748, '', 0.00, 6, '2019-11-12 06:44:14'),
(8749, '\0	\0\0\0	\0\0~', 0.00, 7, '2019-11-12 06:44:14'),
(8750, '\0c\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:44:14'),
(8751, '', 0.00, 7, '2019-11-12 06:44:14'),
(8752, '', 0.00, 6, '2019-11-12 06:44:14'),
(8753, '\0', 0.00, 7, '2019-11-12 06:44:14'),
(8754, '\0d\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:44:14'),
(8755, '\0\0\0', 0.00, 7, '2019-11-12 06:44:14'),
(8756, '', 0.00, 6, '2019-11-12 06:44:14'),
(8757, '\0\0~', 0.00, 7, '2019-11-12 06:44:15'),
(8758, '\0e\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:44:15'),
(8759, '\0', 0.00, 7, '2019-11-12 06:44:15'),
(8760, '', 0.00, 6, '2019-11-12 06:44:15'),
(8761, '', 0.00, 7, '2019-11-12 06:44:15'),
(8762, '\0f\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:44:15'),
(8763, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:15'),
(8764, '', 0.00, 6, '2019-11-12 06:44:15'),
(8765, '', 0.00, 7, '2019-11-12 06:44:15'),
(8766, '\0g\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:44:15'),
(8767, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:15'),
(8768, '', 0.00, 6, '2019-11-12 06:44:15'),
(8769, '', 0.00, 7, '2019-11-12 06:44:15'),
(8770, '\0h\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:44:15'),
(8771, '\0\r\0\0\0\r\0\0~', 0.00, 7, '2019-11-12 06:44:15'),
(8772, '', 0.00, 6, '2019-11-12 06:44:15'),
(8773, '', 0.00, 7, '2019-11-12 06:44:15'),
(8774, '\0i\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:44:15'),
(8775, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:15'),
(8776, '', 0.00, 6, '2019-11-12 06:44:16'),
(8777, '', 0.00, 7, '2019-11-12 06:44:16'),
(8778, '\0j\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:44:16'),
(8779, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:16'),
(8780, '', 0.00, 6, '2019-11-12 06:44:16'),
(8781, '', 0.00, 7, '2019-11-12 06:44:16'),
(8782, '\0k\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:44:16'),
(8783, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:16'),
(8784, '', 0.00, 6, '2019-11-12 06:44:16'),
(8785, '', 0.00, 7, '2019-11-12 06:44:16'),
(8786, '\0l\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:44:16'),
(8787, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:16'),
(8788, '', 0.00, 6, '2019-11-12 06:44:16'),
(8789, '', 0.00, 7, '2019-11-12 06:44:16'),
(8790, '\0m\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:44:16'),
(8791, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:16'),
(8792, '', 0.00, 6, '2019-11-12 06:44:16'),
(8793, '\0\0\0', 0.00, 7, '2019-11-12 06:44:16'),
(8794, '\0n\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:44:16'),
(8795, '', 0.00, 7, '2019-11-12 06:44:16'),
(8796, '', 0.00, 6, '2019-11-12 06:44:16'),
(8797, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:16'),
(8798, '\0o\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:44:16'),
(8799, '', 0.00, 7, '2019-11-12 06:44:17'),
(8800, '', 0.00, 6, '2019-11-12 06:44:17'),
(8801, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:17'),
(8802, '\0p\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:44:17'),
(8803, '', 0.00, 7, '2019-11-12 06:44:17'),
(8804, '', 0.00, 6, '2019-11-12 06:44:17'),
(8805, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:17'),
(8806, '', 0.00, 6, '2019-11-12 06:44:17'),
(8807, '', 0.00, 7, '2019-11-12 06:44:17'),
(8808, '\0r\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:44:17'),
(8809, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:17'),
(8810, '', 0.00, 6, '2019-11-12 06:44:17'),
(8811, '', 0.00, 7, '2019-11-12 06:44:17'),
(8812, '\0s\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:44:17'),
(8813, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:18'),
(8814, '', 0.00, 6, '2019-11-12 06:44:18'),
(8815, '', 0.00, 7, '2019-11-12 06:44:18'),
(8816, '\0t\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:44:18'),
(8817, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:18'),
(8818, '', 0.00, 6, '2019-11-12 06:44:18'),
(8819, '', 0.00, 7, '2019-11-12 06:44:18'),
(8820, '\0u\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:44:18'),
(8821, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:18'),
(8822, '', 0.00, 6, '2019-11-12 06:44:18'),
(8823, '', 0.00, 7, '2019-11-12 06:44:18'),
(8824, '\0v\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:44:18'),
(8825, '\0\Z\0\0\0\Z\0\0~', 0.00, 7, '2019-11-12 06:44:18'),
(8826, '', 0.00, 6, '2019-11-12 06:44:18'),
(8827, '', 0.00, 7, '2019-11-12 06:44:18'),
(8828, '\0w\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:44:19'),
(8829, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:19'),
(8830, '\0w\0\0', 0.00, 6, '2019-11-12 06:44:19'),
(8831, '', 0.00, 7, '2019-11-12 06:44:19'),
(8832, '', 0.00, 6, '2019-11-12 06:44:19'),
(8833, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:19'),
(8834, '\0x\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:44:19'),
(8835, '', 0.00, 7, '2019-11-12 06:44:19'),
(8836, '', 0.00, 6, '2019-11-12 06:44:19'),
(8837, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:19'),
(8838, '\0y\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:44:19'),
(8839, '', 0.00, 7, '2019-11-12 06:44:19'),
(8840, '', 0.00, 6, '2019-11-12 06:44:19'),
(8841, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:19'),
(8842, '\0z\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:44:19'),
(8843, '\0\0\0S', 0.00, 7, '2019-11-12 06:44:19'),
(8844, '', 0.00, 6, '2019-11-12 06:44:19'),
(8845, '', 0.00, 7, '2019-11-12 06:44:19'),
(8846, '\0{\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:44:19'),
(8847, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:19'),
(8848, '', 0.00, 6, '2019-11-12 06:44:19'),
(8849, '\0\0\0S', 0.00, 7, '2019-11-12 06:44:19'),
(8850, '\0|\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:44:19'),
(8851, '\0\0\0 \0\0\0', 0.00, 7, '2019-11-12 06:44:19'),
(8852, '', 0.00, 6, '2019-11-12 06:44:20'),
(8853, '\0 \0\0\0 \0\0~', 0.00, 7, '2019-11-12 06:44:20'),
(8854, '\0}\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:44:20'),
(8855, '', 0.00, 7, '2019-11-12 06:44:20'),
(8856, '', 0.00, 6, '2019-11-12 06:44:20'),
(8857, '\0!\0\0\0!\0\0~', 0.00, 7, '2019-11-12 06:44:20'),
(8858, '\0~\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:44:20'),
(8859, '', 0.00, 7, '2019-11-12 06:44:20'),
(8860, '', 0.00, 6, '2019-11-12 06:44:20'),
(8861, '\0&quot;\0\0\0&quot;\0\0~', 0.00, 7, '2019-11-12 06:44:20'),
(8862, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:21'),
(8863, '', 0.00, 7, '2019-11-12 06:44:21'),
(8864, '', 0.00, 6, '2019-11-12 06:44:21'),
(8865, '\0#\0\0\0#\0\0~', 0.00, 7, '2019-11-12 06:44:21'),
(8866, '', 0.00, 6, '2019-11-12 06:44:22'),
(8867, '', 0.00, 7, '2019-11-12 06:44:22'),
(8868, '', 0.00, 6, '2019-11-12 06:44:22'),
(8869, '\0$\0\0\0$\0\0~', 0.00, 7, '2019-11-12 06:44:22'),
(8870, '', 0.00, 6, '2019-11-12 06:44:22'),
(8871, '', 0.00, 7, '2019-11-12 06:44:22'),
(8872, '', 0.00, 6, '2019-11-12 06:44:22'),
(8873, '\0%\0\0\0%\0\0~', 0.00, 7, '2019-11-12 06:44:22'),
(8874, '', 0.00, 6, '2019-11-12 06:44:22'),
(8875, '', 0.00, 7, '2019-11-12 06:44:22'),
(8876, '', 0.00, 6, '2019-11-12 06:44:22'),
(8877, '\0&amp;\0\0\0&amp;\0\0~', 0.00, 7, '2019-11-12 06:44:22'),
(8878, '', 0.00, 6, '2019-11-12 06:44:22'),
(8879, '', 0.00, 7, '2019-11-12 06:44:22'),
(8880, '', 0.00, 6, '2019-11-12 06:44:22'),
(8881, '\0\'\0\0\0\'\0\0~', 0.00, 7, '2019-11-12 06:44:23'),
(8882, '', 0.00, 6, '2019-11-12 06:44:23'),
(8883, '', 0.00, 7, '2019-11-12 06:44:23'),
(8884, '', 0.00, 6, '2019-11-12 06:44:23'),
(8885, '\0(\0\0\0(\0\0~', 0.00, 7, '2019-11-12 06:44:23'),
(8886, '', 0.00, 6, '2019-11-12 06:44:23'),
(8887, '', 0.00, 7, '2019-11-12 06:44:23'),
(8888, '', 0.00, 6, '2019-11-12 06:44:23'),
(8889, '\0)\0\0\0)\0\0~', 0.00, 7, '2019-11-12 06:44:23'),
(8890, '', 0.00, 6, '2019-11-12 06:44:23'),
(8891, '', 0.00, 7, '2019-11-12 06:44:23'),
(8892, '', 0.00, 6, '2019-11-12 06:44:23'),
(8893, '\0*\0\0\0*\0\0~', 0.00, 7, '2019-11-12 06:44:24'),
(8894, '', 0.00, 6, '2019-11-12 06:44:24'),
(8895, '', 0.00, 7, '2019-11-12 06:44:24'),
(8896, '', 0.00, 6, '2019-11-12 06:44:24'),
(8897, '\0+\0\0\0+\0\0~', 0.00, 7, '2019-11-12 06:44:24'),
(8898, '', 0.00, 6, '2019-11-12 06:44:24'),
(8899, '', 0.00, 7, '2019-11-12 06:44:24'),
(8900, '', 0.00, 6, '2019-11-12 06:44:24'),
(8901, '\0', 0.00, 7, '2019-11-12 06:44:24'),
(8902, '', 0.00, 6, '2019-11-12 06:44:24'),
(8903, '\0', 0.00, 7, '2019-11-12 06:44:24'),
(8904, '', 0.00, 6, '2019-11-12 06:44:24'),
(8905, '\0-\0\0\0-\0\0~', 0.00, 7, '2019-11-12 06:44:24'),
(8906, '', 0.00, 6, '2019-11-12 06:44:24'),
(8907, '', 0.00, 7, '2019-11-12 06:44:24'),
(8908, '', 0.00, 6, '2019-11-12 06:44:24'),
(8909, '\0.\0\0\0.\0\0~', 0.00, 7, '2019-11-12 06:44:24'),
(8910, '', 0.00, 6, '2019-11-12 06:44:24'),
(8911, '', 0.00, 7, '2019-11-12 06:44:24'),
(8912, '', 0.00, 6, '2019-11-12 06:44:24'),
(8913, '\0/\0\0\0/\0\0~', 0.00, 7, '2019-11-12 06:44:24'),
(8914, '', 0.00, 6, '2019-11-12 06:44:24'),
(8915, '', 0.00, 7, '2019-11-12 06:44:25'),
(8916, '', 0.00, 6, '2019-11-12 06:44:25'),
(8917, '\00\0\0\00\0\0~', 0.00, 7, '2019-11-12 06:44:25'),
(8918, '', 0.00, 6, '2019-11-12 06:44:25'),
(8919, '', 0.00, 7, '2019-11-12 06:44:25'),
(8920, '', 0.00, 6, '2019-11-12 06:44:25'),
(8921, '\01\0\0\01\0\0~', 0.00, 7, '2019-11-12 06:44:25'),
(8922, '', 0.00, 6, '2019-11-12 06:44:25'),
(8923, '', 0.00, 7, '2019-11-12 06:44:25'),
(8924, '', 0.00, 6, '2019-11-12 06:44:25'),
(8925, '\02\0\0\02\0\0~', 0.00, 7, '2019-11-12 06:44:26'),
(8926, '', 0.00, 6, '2019-11-12 06:44:26'),
(8927, '', 0.00, 7, '2019-11-12 06:44:26'),
(8928, '', 0.00, 6, '2019-11-12 06:44:26'),
(8929, '\03\0\0\03\0\0~', 0.00, 7, '2019-11-12 06:44:26'),
(8930, '', 0.00, 6, '2019-11-12 06:44:26'),
(8931, '', 0.00, 7, '2019-11-12 06:44:26'),
(8932, '', 0.00, 6, '2019-11-12 06:44:26'),
(8933, '\04\0\0\04\0\0~', 0.00, 7, '2019-11-12 06:44:26'),
(8934, '', 0.00, 6, '2019-11-12 06:44:27'),
(8935, '', 0.00, 7, '2019-11-12 06:44:27'),
(8936, '', 0.00, 6, '2019-11-12 06:44:27'),
(8937, '\05\0\0\05\0\0~', 0.00, 7, '2019-11-12 06:44:27'),
(8938, '', 0.00, 6, '2019-11-12 06:44:27'),
(8939, '', 0.00, 7, '2019-11-12 06:44:27'),
(8940, '', 0.00, 6, '2019-11-12 06:44:27'),
(8941, '\06\0\0\06\0\0~', 0.00, 7, '2019-11-12 06:44:27'),
(8942, '', 0.00, 6, '2019-11-12 06:44:27'),
(8943, '', 0.00, 7, '2019-11-12 06:44:27'),
(8944, '', 0.00, 6, '2019-11-12 06:44:27'),
(8945, '\07\0\0\07\0\0~', 0.00, 7, '2019-11-12 06:44:27'),
(8946, '', 0.00, 6, '2019-11-12 06:44:27'),
(8947, '', 0.00, 7, '2019-11-12 06:44:27'),
(8948, '', 0.00, 6, '2019-11-12 06:44:27'),
(8949, '\08\0\0\08\0\0~', 0.00, 7, '2019-11-12 06:44:27'),
(8950, '', 0.00, 6, '2019-11-12 06:44:28'),
(8951, '', 0.00, 7, '2019-11-12 06:44:28'),
(8952, '', 0.00, 6, '2019-11-12 06:44:28'),
(8953, '\09\0\0\09\0\0~', 0.00, 7, '2019-11-12 06:44:28'),
(8954, '', 0.00, 6, '2019-11-12 06:44:28'),
(8955, '', 0.00, 7, '2019-11-12 06:44:28'),
(8956, '', 0.00, 6, '2019-11-12 06:44:28'),
(8957, '', 0.00, 7, '2019-11-12 06:44:29'),
(8958, '', 0.00, 6, '2019-11-12 06:44:29'),
(8959, '\0;\0\0\0;\0\0~', 0.00, 7, '2019-11-12 06:44:29'),
(8960, '', 0.00, 6, '2019-11-12 06:44:29'),
(8961, '', 0.00, 7, '2019-11-12 06:44:29'),
(8962, '', 0.00, 6, '2019-11-12 06:44:29'),
(8963, '\0&lt;\0\0\0&lt;\0\0~', 0.00, 7, '2019-11-12 06:44:29'),
(8964, '', 0.00, 6, '2019-11-12 06:44:30'),
(8965, '', 0.00, 7, '2019-11-12 06:44:30'),
(8966, '', 0.00, 6, '2019-11-12 06:44:30'),
(8967, '\0=\0\0\0=\0\0~', 0.00, 7, '2019-11-12 06:44:30'),
(8968, '', 0.00, 6, '2019-11-12 06:44:30'),
(8969, '\0=\0\0', 0.00, 7, '2019-11-12 06:44:30'),
(8970, '', 0.00, 6, '2019-11-12 06:44:30'),
(8971, '', 0.00, 7, '2019-11-12 06:44:31'),
(8972, '', 0.00, 6, '2019-11-12 06:44:31'),
(8973, '\0&gt;\0\0\0&gt;\0\0~', 0.00, 7, '2019-11-12 06:44:31'),
(8974, '', 0.00, 6, '2019-11-12 06:44:31'),
(8975, '', 0.00, 7, '2019-11-12 06:44:31'),
(8976, '', 0.00, 6, '2019-11-12 06:44:31'),
(8977, '\0?\0\0\0?\0\0~', 0.00, 7, '2019-11-12 06:44:31'),
(8978, '', 0.00, 6, '2019-11-12 06:44:31'),
(8979, '\0?\0\0\0\0\0\0@\0\0\0', 0.00, 7, '2019-11-12 06:44:31'),
(8980, '', 0.00, 6, '2019-11-12 06:44:31'),
(8981, '\0@\0\0\0@\0\0~', 0.00, 7, '2019-11-12 06:44:31'),
(8982, '', 0.00, 6, '2019-11-12 06:44:32'),
(8983, '', 0.00, 7, '2019-11-12 06:44:32'),
(8984, '', 0.00, 6, '2019-11-12 06:44:32'),
(8985, '\0A\0\0\0A\0\0~', 0.00, 7, '2019-11-12 06:44:32'),
(8986, '', 0.00, 6, '2019-11-12 06:44:33'),
(8987, '', 0.00, 7, '2019-11-12 06:44:33'),
(8988, '\0B\0\0\0B\0\0~', 0.00, 7, '2019-11-12 06:44:33'),
(8989, '', 0.00, 6, '2019-11-12 06:44:33'),
(8990, '', 0.00, 7, '2019-11-12 06:44:33'),
(8991, '', 0.00, 6, '2019-11-12 06:44:33'),
(8992, '', 0.00, 7, '2019-11-12 06:44:33'),
(8993, '', 0.00, 6, '2019-11-12 06:44:33'),
(8994, '\0D\0\0\0D\0\0~', 0.00, 7, '2019-11-12 06:44:33'),
(8995, '', 0.00, 6, '2019-11-12 06:44:33'),
(8996, '', 0.00, 7, '2019-11-12 06:44:33'),
(8997, '', 0.00, 6, '2019-11-12 06:44:33'),
(8998, '\0E\0\0\0E\0\0~', 0.00, 7, '2019-11-12 06:44:33'),
(8999, '', 0.00, 6, '2019-11-12 06:44:34'),
(9000, '', 0.00, 7, '2019-11-12 06:44:34'),
(9001, '', 0.00, 6, '2019-11-12 06:44:34'),
(9002, '\0F\0\0\0F\0\0~', 0.00, 7, '2019-11-12 06:44:35'),
(9003, '', 0.00, 6, '2019-11-12 06:44:35'),
(9004, '', 0.00, 7, '2019-11-12 06:44:35'),
(9005, '', 0.00, 6, '2019-11-12 06:44:35'),
(9006, '\0G\0\0\0G\0\0~', 0.00, 7, '2019-11-12 06:44:36'),
(9007, '', 0.00, 6, '2019-11-12 06:44:36'),
(9008, '', 0.00, 7, '2019-11-12 06:44:36'),
(9009, '', 0.00, 6, '2019-11-12 06:44:36'),
(9010, '\0H\0\0\0H\0\0~', 0.00, 7, '2019-11-12 06:44:36'),
(9011, '', 0.00, 6, '2019-11-12 06:44:37'),
(9012, '', 0.00, 7, '2019-11-12 06:44:37'),
(9013, '', 0.00, 6, '2019-11-12 06:44:37'),
(9014, '\0I\0\0\0I\0\0~', 0.00, 7, '2019-11-12 06:44:37'),
(9015, '', 0.00, 6, '2019-11-12 06:44:37'),
(9016, '', 0.00, 7, '2019-11-12 06:44:37'),
(9017, '', 0.00, 6, '2019-11-12 06:44:38'),
(9018, '\0J\0\0\0J\0\0~', 0.00, 7, '2019-11-12 06:44:38'),
(9019, '', 0.00, 6, '2019-11-12 06:44:38'),
(9020, '', 0.00, 7, '2019-11-12 06:44:39'),
(9021, '', 0.00, 6, '2019-11-12 06:44:39'),
(9022, '\0K\0\0\0K\0\0~', 0.00, 7, '2019-11-12 06:44:39'),
(9023, '', 0.00, 6, '2019-11-12 06:44:39'),
(9024, '', 0.00, 7, '2019-11-12 06:44:39'),
(9025, '', 0.00, 6, '2019-11-12 06:44:39'),
(9026, '\0L\0\0\0L\0\0~', 0.00, 7, '2019-11-12 06:44:39'),
(9027, '', 0.00, 6, '2019-11-12 06:44:39'),
(9028, '', 0.00, 7, '2019-11-12 06:44:39'),
(9029, '', 0.00, 6, '2019-11-12 06:44:39'),
(9030, '\0M\0\0\0M\0\0~', 0.00, 7, '2019-11-12 06:44:39'),
(9031, '', 0.00, 6, '2019-11-12 06:44:39'),
(9032, '', 0.00, 7, '2019-11-12 06:44:39'),
(9033, '', 0.00, 6, '2019-11-12 06:44:39'),
(9034, '\0N\0\0\0N\0\0~', 0.00, 7, '2019-11-12 06:44:39'),
(9035, '', 0.00, 6, '2019-11-12 06:44:39'),
(9036, '', 0.00, 7, '2019-11-12 06:44:39'),
(9037, '', 0.00, 6, '2019-11-12 06:44:39'),
(9038, '\0O\0\0\0O\0\0~', 0.00, 7, '2019-11-12 06:44:39'),
(9039, '', 0.00, 6, '2019-11-12 06:44:39'),
(9040, '\0O\0\0', 0.00, 7, '2019-11-12 06:44:40'),
(9041, '', 0.00, 6, '2019-11-12 06:44:40'),
(9042, '', 0.00, 7, '2019-11-12 06:44:40'),
(9043, '', 0.00, 6, '2019-11-12 06:44:40'),
(9044, '\0P\0\0\0P\0\0~', 0.00, 7, '2019-11-12 06:44:40'),
(9045, '', 0.00, 6, '2019-11-12 06:44:40'),
(9046, '', 0.00, 7, '2019-11-12 06:44:40'),
(9047, '', 0.00, 6, '2019-11-12 06:44:40'),
(9048, '\0Q\0\0\0Q\0\0~', 0.00, 7, '2019-11-12 06:44:40'),
(9049, '', 0.00, 6, '2019-11-12 06:44:40'),
(9050, '', 0.00, 7, '2019-11-12 06:44:40'),
(9051, '', 0.00, 6, '2019-11-12 06:44:40'),
(9052, '\0R\0\0\0R\0\0~', 0.00, 7, '2019-11-12 06:44:40'),
(9053, '', 0.00, 6, '2019-11-12 06:44:40'),
(9054, '', 0.00, 7, '2019-11-12 06:44:40'),
(9055, '', 0.00, 6, '2019-11-12 06:44:40'),
(9056, '\0S\0\0\0S\0\0~', 0.00, 7, '2019-11-12 06:44:41'),
(9057, '', 0.00, 6, '2019-11-12 06:44:41'),
(9058, '', 0.00, 7, '2019-11-12 06:44:41'),
(9059, '', 0.00, 6, '2019-11-12 06:44:41'),
(9060, '\0T\0\0\0T\0\0~', 0.00, 7, '2019-11-12 06:44:41'),
(9061, '', 0.00, 6, '2019-11-12 06:44:41'),
(9062, '', 0.00, 7, '2019-11-12 06:44:41'),
(9063, '', 0.00, 6, '2019-11-12 06:44:41'),
(9064, '', 0.00, 7, '2019-11-12 06:44:41'),
(9065, '', 0.00, 6, '2019-11-12 06:44:41'),
(9066, '\0V\0\0\0V\0\0~', 0.00, 7, '2019-11-12 06:44:41'),
(9067, '', 0.00, 6, '2019-11-12 06:44:41'),
(9068, '', 0.00, 7, '2019-11-12 06:44:41'),
(9069, '', 0.00, 6, '2019-11-12 06:44:41'),
(9070, '\0W\0\0\0W\0\0~', 0.00, 7, '2019-11-12 06:44:41'),
(9071, '', 0.00, 6, '2019-11-12 06:44:41'),
(9072, '', 0.00, 7, '2019-11-12 06:44:41'),
(9073, '', 0.00, 6, '2019-11-12 06:44:41'),
(9074, '\0X\0\0\0X\0\0~', 0.00, 7, '2019-11-12 06:44:41'),
(9075, '', 0.00, 6, '2019-11-12 06:44:41'),
(9076, '', 0.00, 7, '2019-11-12 06:44:41'),
(9077, '', 0.00, 6, '2019-11-12 06:44:41'),
(9078, '\0Y\0\0\0Y\0\0~', 0.00, 7, '2019-11-12 06:44:41'),
(9079, '', 0.00, 6, '2019-11-12 06:44:41'),
(9080, '', 0.00, 7, '2019-11-12 06:44:41'),
(9081, '', 0.00, 6, '2019-11-12 06:44:41'),
(9082, '', 0.00, 7, '2019-11-12 06:44:42'),
(9083, '', 0.00, 6, '2019-11-12 06:44:42'),
(9084, '\0[\0\0\0[\0\0~', 0.00, 7, '2019-11-12 06:44:42'),
(9085, '', 0.00, 6, '2019-11-12 06:44:42'),
(9086, '', 0.00, 7, '2019-11-12 06:44:42'),
(9087, '\0\0Ixime', 0.00, 6, '2019-11-12 06:44:42'),
(9088, '\0\\\0\0\0\\\0\0~', 0.00, 7, '2019-11-12 06:44:42'),
(9089, '\0\0Manix', 0.00, 6, '2019-11-12 06:44:42'),
(9090, '', 0.00, 7, '2019-11-12 06:44:42'),
(9091, '\0\0Pelox', 0.00, 6, '2019-11-12 06:44:42'),
(9092, '\0]\0\0\0]\0\0~', 0.00, 7, '2019-11-12 06:44:42'),
(9093, '\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0 \0\0!\0\0&quot;\0\0#\0\0$\0\0%\0\0&amp;\0\0\'\0\0(\0\0)\0\0*\0\0+\0\0', 0.00, 6, '2019-11-12 06:44:42'),
(9094, '', 0.00, 7, '2019-11-12 06:44:42'),
(9095, '\0\0Supracin', 0.00, 6, '2019-11-12 06:44:42'),
(9096, '\0^\0\0\0^\0\0~', 0.00, 7, '2019-11-12 06:44:43'),
(9097, '\0\0Tetacid ', 0.00, 6, '2019-11-12 06:44:43'),
(9098, '', 0.00, 7, '2019-11-12 06:44:43'),
(9099, '', 0.00, 6, '2019-11-12 06:44:43'),
(9100, '', 0.00, 7, '2019-11-12 06:44:43'),
(9101, '\0\0\0\0', 0.00, 6, '2019-11-12 06:44:43'),
(9102, '\0`\0\0\0`\0\0~', 0.00, 7, '2019-11-12 06:44:43'),
(9103, '', 0.00, 6, '2019-11-12 06:44:43'),
(9104, '', 0.00, 7, '2019-11-12 06:44:43'),
(9105, '', 0.00, 6, '2019-11-12 06:44:43'),
(9106, '\0a\0\0\0a\0\0~', 0.00, 7, '2019-11-12 06:44:43'),
(9107, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:43'),
(9108, '', 0.00, 7, '2019-11-12 06:44:43'),
(9109, '', 0.00, 6, '2019-11-12 06:44:44'),
(9110, '\0b\0\0\0b\0\0~', 0.00, 7, '2019-11-12 06:44:44'),
(9111, '', 0.00, 6, '2019-11-12 06:44:44'),
(9112, '\0b\0\0', 0.00, 7, '2019-11-12 06:44:44'),
(9113, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:44'),
(9114, '', 0.00, 7, '2019-11-12 06:44:44'),
(9115, '', 0.00, 6, '2019-11-12 06:44:44'),
(9116, '\0c\0\0\0c\0\0~', 0.00, 7, '2019-11-12 06:44:44'),
(9117, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:44'),
(9118, '', 0.00, 7, '2019-11-12 06:44:45'),
(9119, '', 0.00, 6, '2019-11-12 06:44:45'),
(9120, '\0d\0\0\0d\0\0~', 0.00, 7, '2019-11-12 06:44:45'),
(9121, '', 0.00, 6, '2019-11-12 06:44:45'),
(9122, '', 0.00, 7, '2019-11-12 06:44:45'),
(9123, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:45'),
(9124, '\0e\0\0\0e\0\0~', 0.00, 7, '2019-11-12 06:44:45'),
(9125, '', 0.00, 6, '2019-11-12 06:44:45'),
(9126, '', 0.00, 7, '2019-11-12 06:44:45'),
(9127, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:45'),
(9128, '\0f\0\0\0f\0\0~', 0.00, 7, '2019-11-12 06:44:45'),
(9129, '', 0.00, 6, '2019-11-12 06:44:45'),
(9130, '', 0.00, 7, '2019-11-12 06:44:45'),
(9131, '\0\0\0\0\0	\0\0\0~', 0.00, 6, '2019-11-12 06:44:45'),
(9132, '\0g\0\0\0g\0\0~', 0.00, 7, '2019-11-12 06:44:45'),
(9133, '', 0.00, 6, '2019-11-12 06:44:45'),
(9134, '', 0.00, 7, '2019-11-12 06:44:45'),
(9135, '\0	\0\0\0\0', 0.00, 6, '2019-11-12 06:44:45'),
(9136, '\0h\0\0\0h\0\0~', 0.00, 7, '2019-11-12 06:44:45'),
(9137, '\0\0\0~', 0.00, 6, '2019-11-12 06:44:45'),
(9138, '', 0.00, 7, '2019-11-12 06:44:45'),
(9139, '', 0.00, 6, '2019-11-12 06:44:45'),
(9140, '\0i\0\0\0i\0\0~', 0.00, 7, '2019-11-12 06:44:45'),
(9141, '\0', 0.00, 6, '2019-11-12 06:44:45'),
(9142, '', 0.00, 7, '2019-11-12 06:44:46'),
(9143, '\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:46'),
(9144, '\0j\0\0\0j\0\0~', 0.00, 7, '2019-11-12 06:44:46'),
(9145, '\0', 0.00, 6, '2019-11-12 06:44:46'),
(9146, '', 0.00, 7, '2019-11-12 06:44:46'),
(9147, '', 0.00, 6, '2019-11-12 06:44:46'),
(9148, '\0k\0\0\0k\0\0~', 0.00, 7, '2019-11-12 06:44:46'),
(9149, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:46'),
(9150, '\0k\0\0', 0.00, 7, '2019-11-12 06:44:46'),
(9151, '\0\0\0\0', 0.00, 6, '2019-11-12 06:44:46'),
(9152, '', 0.00, 7, '2019-11-12 06:44:47'),
(9153, '', 0.00, 6, '2019-11-12 06:44:47'),
(9154, '\0l\0\0\0l\0\0~', 0.00, 7, '2019-11-12 06:44:47'),
(9155, '\0\0\0\0\0\r\0\0\0~', 0.00, 6, '2019-11-12 06:44:47'),
(9156, '', 0.00, 7, '2019-11-12 06:44:47'),
(9157, '', 0.00, 6, '2019-11-12 06:44:47'),
(9158, '\0m\0\0\0m\0\0~', 0.00, 7, '2019-11-12 06:44:47'),
(9159, '\0\r\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:47'),
(9160, '', 0.00, 7, '2019-11-12 06:44:47'),
(9161, '', 0.00, 6, '2019-11-12 06:44:47'),
(9162, '\0n\0\0\0n\0\0~', 0.00, 7, '2019-11-12 06:44:47'),
(9163, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:47'),
(9164, '', 0.00, 7, '2019-11-12 06:44:47'),
(9165, '', 0.00, 6, '2019-11-12 06:44:47'),
(9166, '\0o\0\0\0o\0\0~', 0.00, 7, '2019-11-12 06:44:47'),
(9167, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:47'),
(9168, '', 0.00, 7, '2019-11-12 06:44:47'),
(9169, '', 0.00, 6, '2019-11-12 06:44:47'),
(9170, '\0p\0\0\0p\0\0~', 0.00, 7, '2019-11-12 06:44:48'),
(9171, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:48'),
(9172, '', 0.00, 7, '2019-11-12 06:44:48'),
(9173, '', 0.00, 6, '2019-11-12 06:44:48'),
(9174, '', 0.00, 7, '2019-11-12 06:44:48'),
(9175, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:48'),
(9176, '\0r\0\0\0r\0\0~', 0.00, 7, '2019-11-12 06:44:48'),
(9177, '\0\0\0\0P', 0.00, 6, '2019-11-12 06:44:48'),
(9178, '', 0.00, 7, '2019-11-12 06:44:48'),
(9179, '', 0.00, 6, '2019-11-12 06:44:48'),
(9180, '\0s\0\0\0s\0\0~', 0.00, 7, '2019-11-12 06:44:48'),
(9181, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:48'),
(9182, '', 0.00, 7, '2019-11-12 06:44:48'),
(9183, '', 0.00, 6, '2019-11-12 06:44:48'),
(9184, '\0t\0\0\0t\0\0~', 0.00, 7, '2019-11-12 06:44:48'),
(9185, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:48'),
(9186, '', 0.00, 7, '2019-11-12 06:44:49'),
(9187, '', 0.00, 6, '2019-11-12 06:44:49'),
(9188, '\0u\0\0\0u\0\0~', 0.00, 7, '2019-11-12 06:44:49'),
(9189, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:49'),
(9190, '', 0.00, 7, '2019-11-12 06:44:49'),
(9191, '\0\0\0\0', 0.00, 6, '2019-11-12 06:44:49'),
(9192, '\0v\0\0\0v\0\0~', 0.00, 7, '2019-11-12 06:44:49'),
(9193, '', 0.00, 6, '2019-11-12 06:44:49'),
(9194, '', 0.00, 7, '2019-11-12 06:44:49'),
(9195, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:49'),
(9196, '\0w\0\0\0w\0\0~', 0.00, 7, '2019-11-12 06:44:49'),
(9197, '', 0.00, 6, '2019-11-12 06:44:49'),
(9198, '', 0.00, 7, '2019-11-12 06:44:49'),
(9199, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:49'),
(9200, '\0x\0\0\0x\0\0~', 0.00, 7, '2019-11-12 06:44:49'),
(9201, '', 0.00, 6, '2019-11-12 06:44:49'),
(9202, '', 0.00, 7, '2019-11-12 06:44:49'),
(9203, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:50'),
(9204, '\0y\0\0\0y\0\0~', 0.00, 7, '2019-11-12 06:44:50'),
(9205, '', 0.00, 6, '2019-11-12 06:44:50'),
(9206, '', 0.00, 7, '2019-11-12 06:44:50'),
(9207, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:50'),
(9208, '\0z\0\0\0z\0\0~', 0.00, 7, '2019-11-12 06:44:50'),
(9209, '', 0.00, 6, '2019-11-12 06:44:50'),
(9210, '', 0.00, 7, '2019-11-12 06:44:50'),
(9211, '\0\0\0\0\0\Z\0\0\0~', 0.00, 6, '2019-11-12 06:44:50'),
(9212, '\0{\0\0\0{\0\0~', 0.00, 7, '2019-11-12 06:44:50'),
(9213, '', 0.00, 6, '2019-11-12 06:44:50'),
(9214, '', 0.00, 7, '2019-11-12 06:44:50'),
(9215, '\0\Z\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:50'),
(9216, '\0|\0\0\0|\0\0~', 0.00, 7, '2019-11-12 06:44:50'),
(9217, '', 0.00, 6, '2019-11-12 06:44:50'),
(9218, '', 0.00, 7, '2019-11-12 06:44:50'),
(9219, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:50'),
(9220, '\0}\0\0\0}\0\0~', 0.00, 7, '2019-11-12 06:44:50'),
(9221, '', 0.00, 6, '2019-11-12 06:44:51'),
(9222, '\0}\0\0S', 0.00, 7, '2019-11-12 06:44:51'),
(9223, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:51'),
(9224, '', 0.00, 7, '2019-11-12 06:44:51'),
(9225, '', 0.00, 6, '2019-11-12 06:44:51'),
(9226, '\0~\0\0\0~\0\0~', 0.00, 7, '2019-11-12 06:44:51'),
(9227, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:51'),
(9228, '', 0.00, 7, '2019-11-12 06:44:51'),
(9229, '', 0.00, 6, '2019-11-12 06:44:51'),
(9230, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:44:51'),
(9231, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:44:51'),
(9232, '', 0.00, 7, '2019-11-12 06:44:51'),
(9233, '', 0.00, 6, '2019-11-12 06:44:51'),
(9234, '', 0.00, 7, '2019-11-12 06:44:51'),
(9235, '\0\0\0\0\0 \0\0\0~', 0.00, 6, '2019-11-12 06:44:51'),
(9236, '', 0.00, 7, '2019-11-12 06:44:51'),
(9237, '\0\0\0\0#F\0\0\0 \0\0\0\0', 0.00, 6, '2019-11-12 06:44:51'),
(9238, '', 0.00, 7, '2019-11-12 06:44:51'),
(9239, '\0 \0\0\0\0!\0\0\0~', 0.00, 6, '2019-11-12 06:44:51'),
(9240, '', 0.00, 7, '2019-11-12 06:44:51'),
(9241, '', 0.00, 6, '2019-11-12 06:44:51'),
(9242, '', 0.00, 7, '2019-11-12 06:44:51'),
(9243, '\0!\0\0\0\0&quot;\0\0\0~', 0.00, 6, '2019-11-12 06:44:52'),
(9244, '', 0.00, 7, '2019-11-12 06:44:52'),
(9245, '', 0.00, 6, '2019-11-12 06:44:52'),
(9246, '', 0.00, 7, '2019-11-12 06:44:52'),
(9247, '\0&quot;\0\0\0\0#\0\0\0~', 0.00, 6, '2019-11-12 06:44:52'),
(9248, '', 0.00, 7, '2019-11-12 06:44:52'),
(9249, '', 0.00, 6, '2019-11-12 06:44:52'),
(9250, '', 0.00, 7, '2019-11-12 06:44:52'),
(9251, '\0#\0\0\0\0$\0\0\0~', 0.00, 6, '2019-11-12 06:44:52'),
(9252, '', 0.00, 7, '2019-11-12 06:44:52'),
(9253, '', 0.00, 6, '2019-11-12 06:44:52'),
(9254, '', 0.00, 7, '2019-11-12 06:44:52'),
(9255, '\0$\0\0\0\0%\0\0\0~', 0.00, 6, '2019-11-12 06:44:52'),
(9256, '', 0.00, 7, '2019-11-12 06:44:53'),
(9257, '', 0.00, 6, '2019-11-12 06:44:53'),
(9258, '', 0.00, 7, '2019-11-12 06:44:53'),
(9259, '\0%\0\0\0\0&amp;\0\0\0~', 0.00, 6, '2019-11-12 06:44:53'),
(9260, '', 0.00, 7, '2019-11-12 06:44:53'),
(9261, '', 0.00, 6, '2019-11-12 06:44:53'),
(9262, '', 0.00, 7, '2019-11-12 06:44:53'),
(9263, '\0&amp;\0\0\0\0\'\0\0\0~', 0.00, 6, '2019-11-12 06:44:53'),
(9264, '', 0.00, 7, '2019-11-12 06:44:54'),
(9265, '', 0.00, 6, '2019-11-12 06:44:54'),
(9266, '', 0.00, 7, '2019-11-12 06:44:54'),
(9267, '\0\'\0\0\0\0(\0\0\0~', 0.00, 6, '2019-11-12 06:44:54'),
(9268, '', 0.00, 7, '2019-11-12 06:44:54'),
(9269, '', 0.00, 6, '2019-11-12 06:44:54'),
(9270, '', 0.00, 7, '2019-11-12 06:44:54'),
(9271, '', 0.00, 6, '2019-11-12 06:44:54'),
(9272, '', 0.00, 7, '2019-11-12 06:44:54'),
(9273, '\0)\0\0\0\0*\0\0\0~', 0.00, 6, '2019-11-12 06:44:54'),
(9274, '', 0.00, 7, '2019-11-12 06:44:55'),
(9275, '', 0.00, 6, '2019-11-12 06:44:55'),
(9276, '', 0.00, 7, '2019-11-12 06:44:55'),
(9277, '\0*\0\0\0\0+\0\0\0~', 0.00, 6, '2019-11-12 06:44:55'),
(9278, '', 0.00, 7, '2019-11-12 06:44:55'),
(9279, '', 0.00, 6, '2019-11-12 06:44:56'),
(9280, '', 0.00, 7, '2019-11-12 06:44:56'),
(9281, '\0+\0\0\0\0', 0.00, 6, '2019-11-12 06:44:56'),
(9282, '', 0.00, 7, '2019-11-12 06:44:56'),
(9283, '', 0.00, 6, '2019-11-12 06:44:56'),
(9284, '', 0.00, 7, '2019-11-12 06:44:56'),
(9285, '\0', 0.00, 6, '2019-11-12 06:44:56'),
(9286, '', 0.00, 7, '2019-11-12 06:44:56'),
(9287, '\0', 0.00, 6, '2019-11-12 06:44:56'),
(9288, '', 0.00, 7, '2019-11-12 06:44:57'),
(9289, '\0-\0\0\0\0.\0\0\0~', 0.00, 6, '2019-11-12 06:44:57'),
(9290, '', 0.00, 7, '2019-11-12 06:44:57'),
(9291, '', 0.00, 6, '2019-11-12 06:44:57'),
(9292, '', 0.00, 7, '2019-11-12 06:44:57'),
(9293, '\0.\0\0\0\0/\0\0\0~', 0.00, 6, '2019-11-12 06:44:57'),
(9294, '', 0.00, 7, '2019-11-12 06:44:57'),
(9295, '', 0.00, 6, '2019-11-12 06:44:57'),
(9296, '', 0.00, 7, '2019-11-12 06:44:57'),
(9297, '\0/\0\0\0\00\0\0\0~', 0.00, 6, '2019-11-12 06:44:57'),
(9298, '', 0.00, 7, '2019-11-12 06:44:57'),
(9299, '', 0.00, 6, '2019-11-12 06:44:57'),
(9300, '', 0.00, 7, '2019-11-12 06:44:57'),
(9301, '\00\0\0\0\01\0\0\0~', 0.00, 6, '2019-11-12 06:44:57'),
(9302, '', 0.00, 7, '2019-11-12 06:44:57'),
(9303, '', 0.00, 6, '2019-11-12 06:44:57'),
(9304, '', 0.00, 7, '2019-11-12 06:44:57'),
(9305, '\01\0\0\0\02\0\0\0~', 0.00, 6, '2019-11-12 06:44:58'),
(9306, '', 0.00, 7, '2019-11-12 06:44:58'),
(9307, '', 0.00, 6, '2019-11-12 06:44:58'),
(9308, '', 0.00, 7, '2019-11-12 06:44:58'),
(9309, '\02\0\0\0\03\0\0\0~', 0.00, 6, '2019-11-12 06:44:58'),
(9310, '', 0.00, 7, '2019-11-12 06:44:59'),
(9311, '', 0.00, 6, '2019-11-12 06:44:59'),
(9312, '', 0.00, 7, '2019-11-12 06:44:59'),
(9313, '\03\0\0\0\04\0\0\0~', 0.00, 6, '2019-11-12 06:44:59'),
(9314, '', 0.00, 7, '2019-11-12 06:44:59'),
(9315, '\03\0\0\0', 0.00, 6, '2019-11-12 06:44:59'),
(9316, '', 0.00, 7, '2019-11-12 06:44:59'),
(9317, '', 0.00, 6, '2019-11-12 06:44:59'),
(9318, '', 0.00, 7, '2019-11-12 06:44:59'),
(9319, '\04\0\0\0\05\0\0\0~', 0.00, 6, '2019-11-12 06:44:59'),
(9320, '', 0.00, 7, '2019-11-12 06:44:59'),
(9321, '', 0.00, 6, '2019-11-12 06:44:59'),
(9322, '', 0.00, 7, '2019-11-12 06:44:59'),
(9323, '\05\0\0\0\06\0\0\0~', 0.00, 6, '2019-11-12 06:44:59'),
(9324, '', 0.00, 7, '2019-11-12 06:44:59'),
(9325, '', 0.00, 6, '2019-11-12 06:44:59'),
(9326, '', 0.00, 7, '2019-11-12 06:44:59'),
(9327, '\06\0\0\0\07\0\0\0~', 0.00, 6, '2019-11-12 06:44:59'),
(9328, '', 0.00, 7, '2019-11-12 06:44:59'),
(9329, '', 0.00, 6, '2019-11-12 06:45:00'),
(9330, '', 0.00, 7, '2019-11-12 06:45:00'),
(9331, '\07\0\0\0\08\0\0\0~', 0.00, 6, '2019-11-12 06:45:00'),
(9332, '', 0.00, 7, '2019-11-12 06:45:00'),
(9333, '', 0.00, 6, '2019-11-12 06:45:00'),
(9334, '', 0.00, 7, '2019-11-12 06:45:00'),
(9335, '', 0.00, 6, '2019-11-12 06:45:00'),
(9336, '', 0.00, 7, '2019-11-12 06:45:00'),
(9337, '\09\0\0\0\0:\0\0\0~', 0.00, 6, '2019-11-12 06:45:00'),
(9338, '', 0.00, 7, '2019-11-12 06:45:00'),
(9339, '', 0.00, 6, '2019-11-12 06:45:00'),
(9340, '', 0.00, 7, '2019-11-12 06:45:00'),
(9341, '\0:\0\0\0\0;\0\0\0~', 0.00, 6, '2019-11-12 06:45:00'),
(9342, '', 0.00, 7, '2019-11-12 06:45:00'),
(9343, '', 0.00, 6, '2019-11-12 06:45:00'),
(9344, '', 0.00, 7, '2019-11-12 06:45:00'),
(9345, '\0;\0\0\0\0&lt;\0\0\0~', 0.00, 6, '2019-11-12 06:45:01'),
(9346, '', 0.00, 7, '2019-11-12 06:45:01'),
(9347, '', 0.00, 6, '2019-11-12 06:45:01'),
(9348, '', 0.00, 7, '2019-11-12 06:45:01'),
(9349, '\0&lt;\0\0\0\0=\0\0\0~', 0.00, 6, '2019-11-12 06:45:01'),
(9350, '', 0.00, 7, '2019-11-12 06:45:01'),
(9351, '', 0.00, 6, '2019-11-12 06:45:01'),
(9352, '', 0.00, 7, '2019-11-12 06:45:01'),
(9353, '\0=\0\0\0\0&gt;\0\0\0~', 0.00, 6, '2019-11-12 06:45:02'),
(9354, '', 0.00, 7, '2019-11-12 06:45:02'),
(9355, '', 0.00, 6, '2019-11-12 06:45:02'),
(9356, '', 0.00, 7, '2019-11-12 06:45:02'),
(9357, '', 0.00, 6, '2019-11-12 06:45:02'),
(9358, '', 0.00, 7, '2019-11-12 06:45:02'),
(9359, '\0?\0\0\0\0@\0\0\0~', 0.00, 6, '2019-11-12 06:45:02'),
(9360, '', 0.00, 7, '2019-11-12 06:45:02'),
(9361, '', 0.00, 6, '2019-11-12 06:45:02'),
(9362, '', 0.00, 7, '2019-11-12 06:45:02'),
(9363, '\0@\0\0\0\0A\0\0\0~', 0.00, 6, '2019-11-12 06:45:03'),
(9364, '', 0.00, 7, '2019-11-12 06:45:03'),
(9365, '', 0.00, 6, '2019-11-12 06:45:03'),
(9366, '', 0.00, 7, '2019-11-12 06:45:03'),
(9367, '\0A\0\0\0\0B\0\0\0~', 0.00, 6, '2019-11-12 06:45:03'),
(9368, '', 0.00, 7, '2019-11-12 06:45:03'),
(9369, '', 0.00, 6, '2019-11-12 06:45:03'),
(9370, '', 0.00, 7, '2019-11-12 06:45:03'),
(9371, '\0B\0\0\0\0C\0\0\0~', 0.00, 6, '2019-11-12 06:45:03'),
(9372, '', 0.00, 7, '2019-11-12 06:45:03'),
(9373, '', 0.00, 6, '2019-11-12 06:45:03'),
(9374, '', 0.00, 7, '2019-11-12 06:45:03'),
(9375, '\0C\0\0\0\0D\0\0\0~', 0.00, 6, '2019-11-12 06:45:03'),
(9376, '', 0.00, 7, '2019-11-12 06:45:03'),
(9377, '', 0.00, 6, '2019-11-12 06:45:03'),
(9378, '', 0.00, 7, '2019-11-12 06:45:04'),
(9379, '\0D\0\0\0\0E\0\0\0~', 0.00, 6, '2019-11-12 06:45:04'),
(9380, '', 0.00, 7, '2019-11-12 06:45:04'),
(9381, '', 0.00, 6, '2019-11-12 06:45:04'),
(9382, '', 0.00, 7, '2019-11-12 06:45:04'),
(9383, '', 0.00, 6, '2019-11-12 06:45:04'),
(9384, '', 0.00, 7, '2019-11-12 06:45:04'),
(9385, '\0F\0\0\0\0G\0\0\0~', 0.00, 6, '2019-11-12 06:45:04'),
(9386, '', 0.00, 7, '2019-11-12 06:45:04'),
(9387, '', 0.00, 6, '2019-11-12 06:45:04'),
(9388, '', 0.00, 7, '2019-11-12 06:45:04'),
(9389, '\0G\0\0\0\0H\0\0\0~', 0.00, 6, '2019-11-12 06:45:04'),
(9390, '', 0.00, 7, '2019-11-12 06:45:04'),
(9391, '', 0.00, 6, '2019-11-12 06:45:04'),
(9392, '', 0.00, 7, '2019-11-12 06:45:04'),
(9393, '', 0.00, 6, '2019-11-12 06:45:04'),
(9394, '', 0.00, 7, '2019-11-12 06:45:04'),
(9395, '\0I\0\0\0\0J\0\0\0~', 0.00, 6, '2019-11-12 06:45:04'),
(9396, '', 0.00, 7, '2019-11-12 06:45:04'),
(9397, '', 0.00, 6, '2019-11-12 06:45:04'),
(9398, '', 0.00, 7, '2019-11-12 06:45:04'),
(9399, '\0J\0\0\0\0K\0\0\0~', 0.00, 6, '2019-11-12 06:45:04'),
(9400, '', 0.00, 7, '2019-11-12 06:45:04'),
(9401, '', 0.00, 6, '2019-11-12 06:45:04'),
(9402, '', 0.00, 7, '2019-11-12 06:45:04'),
(9403, '\0K\0\0\0\0L\0\0\0~', 0.00, 6, '2019-11-12 06:45:05'),
(9404, '', 0.00, 7, '2019-11-12 06:45:05'),
(9405, '', 0.00, 6, '2019-11-12 06:45:05'),
(9406, '', 0.00, 7, '2019-11-12 06:45:05'),
(9407, '\0L\0\0\0\0M\0\0\0~', 0.00, 6, '2019-11-12 06:45:05'),
(9408, '', 0.00, 7, '2019-11-12 06:45:05'),
(9409, '', 0.00, 6, '2019-11-12 06:45:05'),
(9410, '', 0.00, 7, '2019-11-12 06:45:05'),
(9411, '\0M\0\0\0\0N\0\0\0~', 0.00, 6, '2019-11-12 06:45:05'),
(9412, '', 0.00, 7, '2019-11-12 06:45:05'),
(9413, '', 0.00, 6, '2019-11-12 06:45:05'),
(9414, '', 0.00, 7, '2019-11-12 06:45:05'),
(9415, '\0N\0\0\0\0O\0\0\0~', 0.00, 6, '2019-11-12 06:45:05'),
(9416, '', 0.00, 7, '2019-11-12 06:45:05'),
(9417, '', 0.00, 6, '2019-11-12 06:45:05'),
(9418, '', 0.00, 7, '2019-11-12 06:45:05'),
(9419, '\0O\0\0\0\0P\0\0\0~', 0.00, 6, '2019-11-12 06:45:05'),
(9420, '', 0.00, 7, '2019-11-12 06:45:05'),
(9421, '', 0.00, 6, '2019-11-12 06:45:05'),
(9422, '', 0.00, 7, '2019-11-12 06:45:06'),
(9423, '\0P\0\0\0\0Q\0\0\0~', 0.00, 6, '2019-11-12 06:45:06'),
(9424, '', 0.00, 7, '2019-11-12 06:45:06'),
(9425, '', 0.00, 6, '2019-11-12 06:45:06'),
(9426, '', 0.00, 7, '2019-11-12 06:45:06'),
(9427, '\0Q\0\0\0\0R\0\0\0~', 0.00, 6, '2019-11-12 06:45:06'),
(9428, '', 0.00, 7, '2019-11-12 06:45:06'),
(9429, '', 0.00, 6, '2019-11-12 06:45:06'),
(9430, '', 0.00, 7, '2019-11-12 06:45:06'),
(9431, '\0R\0\0\0\0S\0\0\0~', 0.00, 6, '2019-11-12 06:45:06'),
(9432, '', 0.00, 7, '2019-11-12 06:45:06'),
(9433, '', 0.00, 6, '2019-11-12 06:45:06'),
(9434, '', 0.00, 7, '2019-11-12 06:45:07'),
(9435, '\0S\0\0\0\0T\0\0\0~', 0.00, 6, '2019-11-12 06:45:07'),
(9436, '', 0.00, 7, '2019-11-12 06:45:07'),
(9437, '', 0.00, 6, '2019-11-12 06:45:07'),
(9438, '', 0.00, 7, '2019-11-12 06:45:07'),
(9439, '\0T\0\0\0\0U\0\0\0~', 0.00, 6, '2019-11-12 06:45:07'),
(9440, '', 0.00, 7, '2019-11-12 06:45:07'),
(9441, '', 0.00, 6, '2019-11-12 06:45:07'),
(9442, '', 0.00, 7, '2019-11-12 06:45:07'),
(9443, '\0U\0\0\0\0V\0\0\0~', 0.00, 6, '2019-11-12 06:45:07'),
(9444, '', 0.00, 7, '2019-11-12 06:45:07'),
(9445, '', 0.00, 6, '2019-11-12 06:45:07'),
(9446, '', 0.00, 7, '2019-11-12 06:45:07'),
(9447, '\0V\0\0\0\0W\0\0\0~', 0.00, 6, '2019-11-12 06:45:07'),
(9448, '', 0.00, 7, '2019-11-12 06:45:08'),
(9449, '', 0.00, 6, '2019-11-12 06:45:08'),
(9450, '', 0.00, 7, '2019-11-12 06:45:08'),
(9451, '\0W\0\0\0\0X\0\0\0~', 0.00, 6, '2019-11-12 06:45:08'),
(9452, '', 0.00, 7, '2019-11-12 06:45:08'),
(9453, '', 0.00, 6, '2019-11-12 06:45:08'),
(9454, '', 0.00, 7, '2019-11-12 06:45:08'),
(9455, '\0X\0\0\0\0Y\0\0\0~', 0.00, 6, '2019-11-12 06:45:08'),
(9456, '', 0.00, 7, '2019-11-12 06:45:08'),
(9457, '', 0.00, 6, '2019-11-12 06:45:09'),
(9458, '', 0.00, 7, '2019-11-12 06:45:09'),
(9459, '\0Y\0\0\0\0Z\0\0\0~', 0.00, 6, '2019-11-12 06:45:09'),
(9460, '', 0.00, 7, '2019-11-12 06:45:09'),
(9461, '', 0.00, 6, '2019-11-12 06:45:09'),
(9462, '', 0.00, 7, '2019-11-12 06:45:09'),
(9463, '\0Z\0\0\0\0[\0\0\0~', 0.00, 6, '2019-11-12 06:45:09'),
(9464, '', 0.00, 7, '2019-11-12 06:45:09'),
(9465, '', 0.00, 6, '2019-11-12 06:45:09'),
(9466, '', 0.00, 7, '2019-11-12 06:45:09'),
(9467, '\0[\0\0\0\0\\\0\0\0~', 0.00, 6, '2019-11-12 06:45:09'),
(9468, '', 0.00, 7, '2019-11-12 06:45:09'),
(9469, '', 0.00, 6, '2019-11-12 06:45:09'),
(9470, '', 0.00, 7, '2019-11-12 06:45:09'),
(9471, '\0\\\0\0\0\0]\0\0\0~', 0.00, 6, '2019-11-12 06:45:09'),
(9472, '', 0.00, 7, '2019-11-12 06:45:09'),
(9473, '', 0.00, 6, '2019-11-12 06:45:09'),
(9474, '', 0.00, 7, '2019-11-12 06:45:09'),
(9475, '\0]\0\0\0\0^\0\0\0~', 0.00, 6, '2019-11-12 06:45:09'),
(9476, '', 0.00, 7, '2019-11-12 06:45:09'),
(9477, '', 0.00, 6, '2019-11-12 06:45:09'),
(9478, '', 0.00, 7, '2019-11-12 06:45:09'),
(9479, '\0^\0\0\0\0_\0\0\0~', 0.00, 6, '2019-11-12 06:45:09'),
(9480, '', 0.00, 7, '2019-11-12 06:45:09'),
(9481, '', 0.00, 6, '2019-11-12 06:45:09'),
(9482, '', 0.00, 7, '2019-11-12 06:45:09'),
(9483, '\0_\0\0\0\0`\0\0\0~', 0.00, 6, '2019-11-12 06:45:09'),
(9484, '', 0.00, 7, '2019-11-12 06:45:09'),
(9485, '\0_\0\0\0j\r\0\0\0`\0\0\0\0', 0.00, 6, '2019-11-12 06:45:09'),
(9486, '', 0.00, 7, '2019-11-12 06:45:10'),
(9487, '\0`\0\0\0\0a\0\0\0~', 0.00, 6, '2019-11-12 06:45:10'),
(9488, '', 0.00, 7, '2019-11-12 06:45:10'),
(9489, '', 0.00, 6, '2019-11-12 06:45:10'),
(9490, '', 0.00, 7, '2019-11-12 06:45:10');
INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(9491, '\0a\0\0\0\0b\0\0\0~', 0.00, 6, '2019-11-12 06:45:10'),
(9492, '', 0.00, 7, '2019-11-12 06:45:10'),
(9493, '\0a\0\0\0S', 0.00, 6, '2019-11-12 06:45:10'),
(9494, '', 0.00, 7, '2019-11-12 06:45:10'),
(9495, '', 0.00, 6, '2019-11-12 06:45:10'),
(9496, '', 0.00, 7, '2019-11-12 06:45:10'),
(9497, '\0b\0\0\0\0c\0\0\0~', 0.00, 6, '2019-11-12 06:45:10'),
(9498, '', 0.00, 7, '2019-11-12 06:45:10'),
(9499, '', 0.00, 6, '2019-11-12 06:45:10'),
(9500, '', 0.00, 7, '2019-11-12 06:45:10'),
(9501, '\0c\0\0\0\0d\0\0\0~', 0.00, 6, '2019-11-12 06:45:10'),
(9502, '', 0.00, 7, '2019-11-12 06:45:10'),
(9503, '', 0.00, 6, '2019-11-12 06:45:11'),
(9504, '', 0.00, 7, '2019-11-12 06:45:11'),
(9505, '', 0.00, 6, '2019-11-12 06:45:11'),
(9506, '', 0.00, 7, '2019-11-12 06:45:11'),
(9507, '\0e\0\0\0\0f\0\0\0~', 0.00, 6, '2019-11-12 06:45:11'),
(9508, '', 0.00, 7, '2019-11-12 06:45:11'),
(9509, '', 0.00, 6, '2019-11-12 06:45:11'),
(9510, '', 0.00, 7, '2019-11-12 06:45:11'),
(9511, '\0f\0\0\0\0g\0\0\0~', 0.00, 6, '2019-11-12 06:45:11'),
(9512, '', 0.00, 7, '2019-11-12 06:45:11'),
(9513, '', 0.00, 6, '2019-11-12 06:45:11'),
(9514, '', 0.00, 7, '2019-11-12 06:45:11'),
(9515, '\0g\0\0\0\0h\0\0\0~', 0.00, 6, '2019-11-12 06:45:11'),
(9516, '', 0.00, 7, '2019-11-12 06:45:11'),
(9517, '', 0.00, 6, '2019-11-12 06:45:11'),
(9518, '', 0.00, 7, '2019-11-12 06:45:11'),
(9519, '\0h\0\0\0\0i\0\0\0~', 0.00, 6, '2019-11-12 06:45:11'),
(9520, '', 0.00, 7, '2019-11-12 06:45:11'),
(9521, '', 0.00, 6, '2019-11-12 06:45:11'),
(9522, '', 0.00, 7, '2019-11-12 06:45:11'),
(9523, '\0i\0\0\0\0j\0\0\0~', 0.00, 6, '2019-11-12 06:45:11'),
(9524, '', 0.00, 7, '2019-11-12 06:45:11'),
(9525, '', 0.00, 6, '2019-11-12 06:45:11'),
(9526, '', 0.00, 7, '2019-11-12 06:45:11'),
(9527, '\0j\0\0\0\0k\0\0\0~', 0.00, 6, '2019-11-12 06:45:11'),
(9528, '', 0.00, 7, '2019-11-12 06:45:11'),
(9529, '', 0.00, 6, '2019-11-12 06:45:12'),
(9530, '', 0.00, 7, '2019-11-12 06:45:12'),
(9531, '\0k\0\0\0\0l\0\0\0~', 0.00, 6, '2019-11-12 06:45:12'),
(9532, '', 0.00, 7, '2019-11-12 06:45:12'),
(9533, '', 0.00, 6, '2019-11-12 06:45:12'),
(9534, '', 0.00, 7, '2019-11-12 06:45:12'),
(9535, '\0l\0\0\0\0m\0\0\0~', 0.00, 6, '2019-11-12 06:45:12'),
(9536, '', 0.00, 7, '2019-11-12 06:45:12'),
(9537, '', 0.00, 6, '2019-11-12 06:45:12'),
(9538, '', 0.00, 7, '2019-11-12 06:45:12'),
(9539, '\0m\0\0\0\0n\0\0\0~', 0.00, 6, '2019-11-12 06:45:12'),
(9540, '', 0.00, 7, '2019-11-12 06:45:12'),
(9541, '', 0.00, 6, '2019-11-12 06:45:12'),
(9542, '', 0.00, 7, '2019-11-12 06:45:12'),
(9543, '', 0.00, 6, '2019-11-12 06:45:12'),
(9544, '', 0.00, 7, '2019-11-12 06:45:12'),
(9545, '\0o\0\0\0\0p\0\0\0~', 0.00, 6, '2019-11-12 06:45:12'),
(9546, '', 0.00, 7, '2019-11-12 06:45:12'),
(9547, '', 0.00, 6, '2019-11-12 06:45:12'),
(9548, '', 0.00, 7, '2019-11-12 06:45:13'),
(9549, '\0p\0\0\0\0q\0\0\0~', 0.00, 6, '2019-11-12 06:45:13'),
(9550, '', 0.00, 7, '2019-11-12 06:45:13'),
(9551, '', 0.00, 6, '2019-11-12 06:45:13'),
(9552, '', 0.00, 7, '2019-11-12 06:45:13'),
(9553, '\0q\0\0\0\0r\0\0\0~', 0.00, 6, '2019-11-12 06:45:13'),
(9554, '', 0.00, 7, '2019-11-12 06:45:13'),
(9555, '\0q\0\0\0', 0.00, 6, '2019-11-12 06:45:13'),
(9556, '', 0.00, 7, '2019-11-12 06:45:13'),
(9557, '', 0.00, 6, '2019-11-12 06:45:13'),
(9558, '', 0.00, 7, '2019-11-12 06:45:13'),
(9559, '\0r\0\0\0\0s\0\0\0~', 0.00, 6, '2019-11-12 06:45:13'),
(9560, '', 0.00, 7, '2019-11-12 06:45:13'),
(9561, '', 0.00, 6, '2019-11-12 06:45:13'),
(9562, '', 0.00, 7, '2019-11-12 06:45:13'),
(9563, '\0s\0\0\0\0t\0\0\0~', 0.00, 6, '2019-11-12 06:45:13'),
(9564, '', 0.00, 7, '2019-11-12 06:45:13'),
(9565, '', 0.00, 6, '2019-11-12 06:45:13'),
(9566, '', 0.00, 7, '2019-11-12 06:45:13'),
(9567, '\0t\0\0\0\0u\0\0\0~', 0.00, 6, '2019-11-12 06:45:14'),
(9568, '', 0.00, 7, '2019-11-12 06:45:14'),
(9569, '', 0.00, 6, '2019-11-12 06:45:14'),
(9570, '', 0.00, 7, '2019-11-12 06:45:14'),
(9571, '\0u\0\0\0\0v\0\0\0~', 0.00, 6, '2019-11-12 06:45:14'),
(9572, '', 0.00, 7, '2019-11-12 06:45:14'),
(9573, '', 0.00, 6, '2019-11-12 06:45:14'),
(9574, '', 0.00, 7, '2019-11-12 06:45:14'),
(9575, '\0v\0\0\0\0w\0\0\0~', 0.00, 6, '2019-11-12 06:45:14'),
(9576, '', 0.00, 7, '2019-11-12 06:45:14'),
(9577, '', 0.00, 6, '2019-11-12 06:45:14'),
(9578, '', 0.00, 7, '2019-11-12 06:45:14'),
(9579, '\0w\0\0\0\0x\0\0\0~', 0.00, 6, '2019-11-12 06:45:14'),
(9580, '', 0.00, 7, '2019-11-12 06:45:14'),
(9581, '', 0.00, 6, '2019-11-12 06:45:14'),
(9582, '', 0.00, 7, '2019-11-12 06:45:15'),
(9583, '\0x\0\0\0\0y\0\0\0~', 0.00, 6, '2019-11-12 06:45:15'),
(9584, '', 0.00, 7, '2019-11-12 06:45:15'),
(9585, '', 0.00, 6, '2019-11-12 06:45:15'),
(9586, '', 0.00, 7, '2019-11-12 06:45:15'),
(9587, '\0y\0\0\0\0z\0\0\0~', 0.00, 6, '2019-11-12 06:45:15'),
(9588, '', 0.00, 7, '2019-11-12 06:45:15'),
(9589, '', 0.00, 6, '2019-11-12 06:45:15'),
(9590, '', 0.00, 7, '2019-11-12 06:45:15'),
(9591, '', 0.00, 6, '2019-11-12 06:45:15'),
(9592, '', 0.00, 7, '2019-11-12 06:45:15'),
(9593, '', 0.00, 6, '2019-11-12 06:45:15'),
(9594, '', 0.00, 7, '2019-11-12 06:45:15'),
(9595, '', 0.00, 6, '2019-11-12 06:45:15'),
(9596, '', 0.00, 7, '2019-11-12 06:45:15'),
(9597, '\0}\0\0\0\0~\0\0\0~', 0.00, 6, '2019-11-12 06:45:15'),
(9598, '', 0.00, 7, '2019-11-12 06:45:15'),
(9599, '', 0.00, 6, '2019-11-12 06:45:15'),
(9600, '', 0.00, 7, '2019-11-12 06:45:15'),
(9601, '', 0.00, 6, '2019-11-12 06:45:15'),
(9602, '', 0.00, 7, '2019-11-12 06:45:15'),
(9603, '', 0.00, 6, '2019-11-12 06:45:16'),
(9604, '', 0.00, 7, '2019-11-12 06:45:16'),
(9605, '', 0.00, 6, '2019-11-12 06:45:16'),
(9606, '', 0.00, 7, '2019-11-12 06:45:16'),
(9607, '', 0.00, 6, '2019-11-12 06:45:16'),
(9608, '', 0.00, 7, '2019-11-12 06:45:16'),
(9609, '', 0.00, 6, '2019-11-12 06:45:16'),
(9610, '', 0.00, 7, '2019-11-12 06:45:16'),
(9611, '', 0.00, 6, '2019-11-12 06:45:16'),
(9612, '', 0.00, 7, '2019-11-12 06:45:16'),
(9613, '', 0.00, 6, '2019-11-12 06:45:16'),
(9614, '', 0.00, 7, '2019-11-12 06:45:16'),
(9615, '', 0.00, 6, '2019-11-12 06:45:16'),
(9616, '', 0.00, 7, '2019-11-12 06:45:16'),
(9617, '', 0.00, 6, '2019-11-12 06:45:16'),
(9618, '', 0.00, 7, '2019-11-12 06:45:16'),
(9619, '', 0.00, 6, '2019-11-12 06:45:17'),
(9620, '', 0.00, 7, '2019-11-12 06:45:17'),
(9621, '', 0.00, 6, '2019-11-12 06:45:17'),
(9622, '', 0.00, 7, '2019-11-12 06:45:17'),
(9623, '', 0.00, 6, '2019-11-12 06:45:17'),
(9624, '', 0.00, 7, '2019-11-12 06:45:17'),
(9625, '', 0.00, 6, '2019-11-12 06:45:17'),
(9626, '', 0.00, 7, '2019-11-12 06:45:17'),
(9627, '', 0.00, 6, '2019-11-12 06:45:18'),
(9628, '', 0.00, 7, '2019-11-12 06:45:18'),
(9629, '', 0.00, 6, '2019-11-12 06:45:18'),
(9630, '', 0.00, 7, '2019-11-12 06:45:18'),
(9631, '', 0.00, 6, '2019-11-12 06:45:18'),
(9632, '', 0.00, 7, '2019-11-12 06:45:18'),
(9633, '', 0.00, 6, '2019-11-12 06:45:18'),
(9634, '', 0.00, 7, '2019-11-12 06:45:18'),
(9635, '', 0.00, 6, '2019-11-12 06:45:18'),
(9636, '', 0.00, 7, '2019-11-12 06:45:18'),
(9637, '', 0.00, 6, '2019-11-12 06:45:18'),
(9638, '', 0.00, 7, '2019-11-12 06:45:19'),
(9639, '', 0.00, 6, '2019-11-12 06:45:19'),
(9640, '', 0.00, 7, '2019-11-12 06:45:19'),
(9641, '', 0.00, 6, '2019-11-12 06:45:19'),
(9642, '', 0.00, 7, '2019-11-12 06:45:19'),
(9643, '', 0.00, 6, '2019-11-12 06:45:19'),
(9644, '', 0.00, 7, '2019-11-12 06:45:19'),
(9645, '', 0.00, 6, '2019-11-12 06:45:19'),
(9646, '', 0.00, 7, '2019-11-12 06:45:19'),
(9647, '', 0.00, 6, '2019-11-12 06:45:19'),
(9648, '', 0.00, 7, '2019-11-12 06:45:19'),
(9649, '', 0.00, 6, '2019-11-12 06:45:19'),
(9650, '', 0.00, 7, '2019-11-12 06:45:19'),
(9651, '', 0.00, 6, '2019-11-12 06:45:19'),
(9652, '', 0.00, 7, '2019-11-12 06:45:19'),
(9653, '', 0.00, 6, '2019-11-12 06:45:19'),
(9654, '', 0.00, 7, '2019-11-12 06:45:19'),
(9655, '', 0.00, 6, '2019-11-12 06:45:19'),
(9656, '', 0.00, 7, '2019-11-12 06:45:19'),
(9657, '', 0.00, 6, '2019-11-12 06:45:19'),
(9658, '', 0.00, 7, '2019-11-12 06:45:19'),
(9659, '', 0.00, 6, '2019-11-12 06:45:19'),
(9660, '', 0.00, 7, '2019-11-12 06:45:19'),
(9661, '', 0.00, 6, '2019-11-12 06:45:19'),
(9662, '', 0.00, 7, '2019-11-12 06:45:20'),
(9663, '', 0.00, 6, '2019-11-12 06:45:20'),
(9664, '', 0.00, 7, '2019-11-12 06:45:20'),
(9665, '', 0.00, 6, '2019-11-12 06:45:20'),
(9666, '', 0.00, 7, '2019-11-12 06:45:20'),
(9667, '', 0.00, 6, '2019-11-12 06:45:20'),
(9668, '', 0.00, 7, '2019-11-12 06:45:20'),
(9669, '', 0.00, 6, '2019-11-12 06:45:20'),
(9670, '', 0.00, 7, '2019-11-12 06:45:20'),
(9671, '', 0.00, 6, '2019-11-12 06:45:20'),
(9672, '', 0.00, 7, '2019-11-12 06:45:20'),
(9673, '', 0.00, 6, '2019-11-12 06:45:20'),
(9674, '', 0.00, 7, '2019-11-12 06:45:20'),
(9675, '', 0.00, 6, '2019-11-12 06:45:21'),
(9676, '', 0.00, 7, '2019-11-12 06:45:21'),
(9677, '', 0.00, 6, '2019-11-12 06:45:21'),
(9678, '', 0.00, 7, '2019-11-12 06:45:21'),
(9679, '', 0.00, 6, '2019-11-12 06:45:21'),
(9680, '', 0.00, 7, '2019-11-12 06:45:21'),
(9681, '', 0.00, 6, '2019-11-12 06:45:21'),
(9682, '', 0.00, 7, '2019-11-12 06:45:21'),
(9683, '', 0.00, 6, '2019-11-12 06:45:21'),
(9684, '', 0.00, 7, '2019-11-12 06:45:21'),
(9685, '', 0.00, 6, '2019-11-12 06:45:21'),
(9686, '', 0.00, 7, '2019-11-12 06:45:21'),
(9687, '', 0.00, 6, '2019-11-12 06:45:21'),
(9688, '', 0.00, 7, '2019-11-12 06:45:21'),
(9689, '', 0.00, 6, '2019-11-12 06:45:21'),
(9690, '', 0.00, 7, '2019-11-12 06:45:21'),
(9691, '', 0.00, 6, '2019-11-12 06:45:21'),
(9692, '', 0.00, 7, '2019-11-12 06:45:21'),
(9693, '', 0.00, 6, '2019-11-12 06:45:21'),
(9694, '', 0.00, 7, '2019-11-12 06:45:22'),
(9695, '', 0.00, 6, '2019-11-12 06:45:22'),
(9696, '', 0.00, 7, '2019-11-12 06:45:22'),
(9697, '', 0.00, 6, '2019-11-12 06:45:22'),
(9698, '', 0.00, 7, '2019-11-12 06:45:22'),
(9699, '', 0.00, 6, '2019-11-12 06:45:22'),
(9700, '', 0.00, 7, '2019-11-12 06:45:22'),
(9701, '', 0.00, 6, '2019-11-12 06:45:22'),
(9702, '', 0.00, 7, '2019-11-12 06:45:22'),
(9703, '', 0.00, 6, '2019-11-12 06:45:22'),
(9704, '', 0.00, 7, '2019-11-12 06:45:22'),
(9705, '', 0.00, 6, '2019-11-12 06:45:22'),
(9706, '', 0.00, 7, '2019-11-12 06:45:22'),
(9707, '', 0.00, 6, '2019-11-12 06:45:22'),
(9708, '', 0.00, 7, '2019-11-12 06:45:22'),
(9709, '', 0.00, 6, '2019-11-12 06:45:22'),
(9710, '', 0.00, 7, '2019-11-12 06:45:22'),
(9711, '', 0.00, 6, '2019-11-12 06:45:22'),
(9712, '', 0.00, 7, '2019-11-12 06:45:22'),
(9713, '', 0.00, 6, '2019-11-12 06:45:22'),
(9714, '', 0.00, 7, '2019-11-12 06:45:22'),
(9715, '', 0.00, 6, '2019-11-12 06:45:22'),
(9716, '', 0.00, 7, '2019-11-12 06:45:22'),
(9717, '', 0.00, 6, '2019-11-12 06:45:23'),
(9718, '', 0.00, 7, '2019-11-12 06:45:23'),
(9719, '', 0.00, 6, '2019-11-12 06:45:23'),
(9720, '', 0.00, 7, '2019-11-12 06:45:23'),
(9721, '', 0.00, 6, '2019-11-12 06:45:23'),
(9722, '', 0.00, 7, '2019-11-12 06:45:23'),
(9723, '', 0.00, 6, '2019-11-12 06:45:23'),
(9724, '', 0.00, 7, '2019-11-12 06:45:23'),
(9725, '', 0.00, 6, '2019-11-12 06:45:23'),
(9726, '', 0.00, 7, '2019-11-12 06:45:23'),
(9727, '', 0.00, 6, '2019-11-12 06:45:23'),
(9728, '', 0.00, 7, '2019-11-12 06:45:23'),
(9729, '', 0.00, 6, '2019-11-12 06:45:23'),
(9730, '', 0.00, 7, '2019-11-12 06:45:23'),
(9731, '', 0.00, 6, '2019-11-12 06:45:23'),
(9732, '', 0.00, 7, '2019-11-12 06:45:24'),
(9733, '', 0.00, 6, '2019-11-12 06:45:24'),
(9734, '\0\0\0', 0.00, 7, '2019-11-12 06:45:24'),
(9735, '', 0.00, 6, '2019-11-12 06:45:24'),
(9736, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:24'),
(9737, '', 0.00, 6, '2019-11-12 06:45:24'),
(9738, '', 0.00, 7, '2019-11-12 06:45:24'),
(9739, '', 0.00, 6, '2019-11-12 06:45:24'),
(9740, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:24'),
(9741, '', 0.00, 6, '2019-11-12 06:45:24'),
(9742, '', 0.00, 7, '2019-11-12 06:45:24'),
(9743, '', 0.00, 6, '2019-11-12 06:45:24'),
(9744, '', 0.00, 7, '2019-11-12 06:45:24'),
(9745, '', 0.00, 6, '2019-11-12 06:45:24'),
(9746, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:24'),
(9747, '', 0.00, 6, '2019-11-12 06:45:24'),
(9748, '', 0.00, 7, '2019-11-12 06:45:24'),
(9749, '', 0.00, 6, '2019-11-12 06:45:24'),
(9750, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:24'),
(9751, '', 0.00, 6, '2019-11-12 06:45:24'),
(9752, '', 0.00, 7, '2019-11-12 06:45:24'),
(9753, '', 0.00, 6, '2019-11-12 06:45:24'),
(9754, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:24'),
(9755, '', 0.00, 6, '2019-11-12 06:45:25'),
(9756, '', 0.00, 7, '2019-11-12 06:45:25'),
(9757, '', 0.00, 6, '2019-11-12 06:45:25'),
(9758, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:25'),
(9759, '', 0.00, 6, '2019-11-12 06:45:25'),
(9760, '', 0.00, 7, '2019-11-12 06:45:25'),
(9761, '', 0.00, 6, '2019-11-12 06:45:25'),
(9762, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:25'),
(9763, '', 0.00, 6, '2019-11-12 06:45:25'),
(9764, '', 0.00, 7, '2019-11-12 06:45:25'),
(9765, '', 0.00, 6, '2019-11-12 06:45:26'),
(9766, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:26'),
(9767, '', 0.00, 6, '2019-11-12 06:45:26'),
(9768, '', 0.00, 7, '2019-11-12 06:45:26'),
(9769, '', 0.00, 6, '2019-11-12 06:45:26'),
(9770, '', 0.00, 7, '2019-11-12 06:45:26'),
(9771, '', 0.00, 6, '2019-11-12 06:45:26'),
(9772, '\0', 0.00, 7, '2019-11-12 06:45:26'),
(9773, '', 0.00, 6, '2019-11-12 06:45:26'),
(9774, '\0\0\0', 0.00, 7, '2019-11-12 06:45:26'),
(9775, '', 0.00, 6, '2019-11-12 06:45:26'),
(9776, '\0\0~', 0.00, 7, '2019-11-12 06:45:26'),
(9777, '', 0.00, 6, '2019-11-12 06:45:27'),
(9778, '\0', 0.00, 7, '2019-11-12 06:45:27'),
(9779, '', 0.00, 6, '2019-11-12 06:45:27'),
(9780, '', 0.00, 7, '2019-11-12 06:45:27'),
(9781, '', 0.00, 6, '2019-11-12 06:45:27'),
(9782, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:27'),
(9783, '', 0.00, 6, '2019-11-12 06:45:27'),
(9784, '\0\0\0', 0.00, 7, '2019-11-12 06:45:27'),
(9785, '', 0.00, 6, '2019-11-12 06:45:27'),
(9786, '', 0.00, 7, '2019-11-12 06:45:27'),
(9787, '', 0.00, 6, '2019-11-12 06:45:27'),
(9788, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:27'),
(9789, '', 0.00, 6, '2019-11-12 06:45:27'),
(9790, '', 0.00, 7, '2019-11-12 06:45:27'),
(9791, '', 0.00, 6, '2019-11-12 06:45:27'),
(9792, '\0\r\0\0\0\r\0\0~', 0.00, 7, '2019-11-12 06:45:27'),
(9793, '', 0.00, 6, '2019-11-12 06:45:27'),
(9794, '', 0.00, 7, '2019-11-12 06:45:27'),
(9795, '', 0.00, 6, '2019-11-12 06:45:28'),
(9796, '', 0.00, 7, '2019-11-12 06:45:28'),
(9797, '', 0.00, 6, '2019-11-12 06:45:28'),
(9798, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:28'),
(9799, '', 0.00, 6, '2019-11-12 06:45:28'),
(9800, '', 0.00, 6, '2019-11-12 06:45:28'),
(9801, '', 0.00, 7, '2019-11-12 06:45:29'),
(9802, '', 0.00, 6, '2019-11-12 06:45:29'),
(9803, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:29'),
(9804, '', 0.00, 6, '2019-11-12 06:45:29'),
(9805, '', 0.00, 7, '2019-11-12 06:45:29'),
(9806, '', 0.00, 6, '2019-11-12 06:45:29'),
(9807, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:30'),
(9808, '', 0.00, 6, '2019-11-12 06:45:30'),
(9809, '', 0.00, 7, '2019-11-12 06:45:30'),
(9810, '', 0.00, 6, '2019-11-12 06:45:30'),
(9811, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:30'),
(9812, '', 0.00, 6, '2019-11-12 06:45:30'),
(9813, '', 0.00, 7, '2019-11-12 06:45:30'),
(9814, '', 0.00, 6, '2019-11-12 06:45:30'),
(9815, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:30'),
(9816, '', 0.00, 6, '2019-11-12 06:45:30'),
(9817, '', 0.00, 7, '2019-11-12 06:45:30'),
(9818, '', 0.00, 6, '2019-11-12 06:45:30'),
(9819, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:30'),
(9820, '', 0.00, 6, '2019-11-12 06:45:30'),
(9821, '', 0.00, 7, '2019-11-12 06:45:30'),
(9822, '', 0.00, 6, '2019-11-12 06:45:30'),
(9823, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:30'),
(9824, '', 0.00, 6, '2019-11-12 06:45:30'),
(9825, '', 0.00, 7, '2019-11-12 06:45:30'),
(9826, '', 0.00, 6, '2019-11-12 06:45:30'),
(9827, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:31'),
(9828, '', 0.00, 6, '2019-11-12 06:45:31'),
(9829, '', 0.00, 7, '2019-11-12 06:45:31'),
(9830, '', 0.00, 6, '2019-11-12 06:45:31'),
(9831, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:31'),
(9832, '', 0.00, 6, '2019-11-12 06:45:31'),
(9833, '', 0.00, 7, '2019-11-12 06:45:31'),
(9834, '', 0.00, 6, '2019-11-12 06:45:31'),
(9835, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:31'),
(9836, '', 0.00, 6, '2019-11-12 06:45:31'),
(9837, '\0\0\0', 0.00, 7, '2019-11-12 06:45:31'),
(9838, '', 0.00, 6, '2019-11-12 06:45:31'),
(9839, '', 0.00, 7, '2019-11-12 06:45:31'),
(9840, '', 0.00, 6, '2019-11-12 06:45:31'),
(9841, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:31'),
(9842, '', 0.00, 6, '2019-11-12 06:45:31'),
(9843, '', 0.00, 7, '2019-11-12 06:45:31'),
(9844, '', 0.00, 6, '2019-11-12 06:45:31'),
(9845, '\0\Z\0\0\0\Z\0\0~', 0.00, 7, '2019-11-12 06:45:31'),
(9846, '', 0.00, 6, '2019-11-12 06:45:32'),
(9847, '', 0.00, 7, '2019-11-12 06:45:32'),
(9848, '', 0.00, 6, '2019-11-12 06:45:32'),
(9849, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:32'),
(9850, '', 0.00, 6, '2019-11-12 06:45:32'),
(9851, '', 0.00, 7, '2019-11-12 06:45:32'),
(9852, '', 0.00, 6, '2019-11-12 06:45:32'),
(9853, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:32'),
(9854, '', 0.00, 6, '2019-11-12 06:45:32'),
(9855, '', 0.00, 7, '2019-11-12 06:45:32'),
(9856, '', 0.00, 6, '2019-11-12 06:45:32'),
(9857, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:32'),
(9858, '', 0.00, 6, '2019-11-12 06:45:32'),
(9859, '', 0.00, 7, '2019-11-12 06:45:32'),
(9860, '', 0.00, 6, '2019-11-12 06:45:32'),
(9861, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:32'),
(9862, '', 0.00, 6, '2019-11-12 06:45:32'),
(9863, '', 0.00, 7, '2019-11-12 06:45:32'),
(9864, '', 0.00, 6, '2019-11-12 06:45:32'),
(9865, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:33'),
(9866, '', 0.00, 6, '2019-11-12 06:45:33'),
(9867, '', 0.00, 7, '2019-11-12 06:45:33'),
(9868, '', 0.00, 6, '2019-11-12 06:45:33'),
(9869, '\0 \0\0\0 \0\0~', 0.00, 7, '2019-11-12 06:45:33'),
(9870, '', 0.00, 6, '2019-11-12 06:45:33'),
(9871, '', 0.00, 7, '2019-11-12 06:45:33'),
(9872, '', 0.00, 6, '2019-11-12 06:45:33'),
(9873, '\0!\0\0\0!\0\0~', 0.00, 7, '2019-11-12 06:45:33'),
(9874, '', 0.00, 6, '2019-11-12 06:45:33'),
(9875, '', 0.00, 7, '2019-11-12 06:45:33'),
(9876, '', 0.00, 6, '2019-11-12 06:45:33'),
(9877, '\0&quot;\0\0\0&quot;\0\0~', 0.00, 7, '2019-11-12 06:45:34'),
(9878, '', 0.00, 6, '2019-11-12 06:45:34'),
(9879, '', 0.00, 7, '2019-11-12 06:45:34'),
(9880, '', 0.00, 6, '2019-11-12 06:45:34'),
(9881, '\0#\0\0\0#\0\0~', 0.00, 7, '2019-11-12 06:45:34'),
(9882, '', 0.00, 6, '2019-11-12 06:45:34'),
(9883, '', 0.00, 7, '2019-11-12 06:45:34'),
(9884, '', 0.00, 6, '2019-11-12 06:45:34'),
(9885, '\0$\0\0\0$\0\0~', 0.00, 7, '2019-11-12 06:45:34'),
(9886, '', 0.00, 6, '2019-11-12 06:45:34'),
(9887, '', 0.00, 7, '2019-11-12 06:45:34'),
(9888, '', 0.00, 6, '2019-11-12 06:45:34'),
(9889, '\0%\0\0\0%\0\0~', 0.00, 7, '2019-11-12 06:45:34'),
(9890, '', 0.00, 6, '2019-11-12 06:45:34'),
(9891, '', 0.00, 7, '2019-11-12 06:45:34'),
(9892, '', 0.00, 6, '2019-11-12 06:45:34'),
(9893, '\0&amp;\0\0\0&amp;\0\0~', 0.00, 7, '2019-11-12 06:45:34'),
(9894, '', 0.00, 6, '2019-11-12 06:45:34'),
(9895, '', 0.00, 7, '2019-11-12 06:45:34'),
(9896, '', 0.00, 6, '2019-11-12 06:45:34'),
(9897, '\0\'\0\0\0\'\0\0~', 0.00, 7, '2019-11-12 06:45:34'),
(9898, '', 0.00, 6, '2019-11-12 06:45:34'),
(9899, '', 0.00, 7, '2019-11-12 06:45:34'),
(9900, '', 0.00, 6, '2019-11-12 06:45:35'),
(9901, '\0(\0\0\0(\0\0~', 0.00, 7, '2019-11-12 06:45:35'),
(9902, '', 0.00, 6, '2019-11-12 06:45:35'),
(9903, '', 0.00, 7, '2019-11-12 06:45:35'),
(9904, '', 0.00, 6, '2019-11-12 06:45:35'),
(9905, '\0)\0\0\0)\0\0~', 0.00, 7, '2019-11-12 06:45:35'),
(9906, '', 0.00, 6, '2019-11-12 06:45:35'),
(9907, '', 0.00, 7, '2019-11-12 06:45:35'),
(9908, '', 0.00, 6, '2019-11-12 06:45:35'),
(9909, '\0*\0\0\0*\0\0~', 0.00, 7, '2019-11-12 06:45:35'),
(9910, '', 0.00, 6, '2019-11-12 06:45:35'),
(9911, '', 0.00, 7, '2019-11-12 06:45:35'),
(9912, '', 0.00, 6, '2019-11-12 06:45:35'),
(9913, '\0+\0\0\0+\0\0~', 0.00, 7, '2019-11-12 06:45:35'),
(9914, '', 0.00, 6, '2019-11-12 06:45:35'),
(9915, '', 0.00, 7, '2019-11-12 06:45:35'),
(9916, '', 0.00, 6, '2019-11-12 06:45:35'),
(9917, '\0', 0.00, 7, '2019-11-12 06:45:35'),
(9918, '', 0.00, 6, '2019-11-12 06:45:36'),
(9919, '\0', 0.00, 7, '2019-11-12 06:45:36'),
(9920, '', 0.00, 6, '2019-11-12 06:45:36'),
(9921, '\0-\0\0\0-\0\0~', 0.00, 7, '2019-11-12 06:45:36'),
(9922, '', 0.00, 6, '2019-11-12 06:45:36'),
(9923, '', 0.00, 7, '2019-11-12 06:45:36'),
(9924, '', 0.00, 6, '2019-11-12 06:45:36'),
(9925, '\0.\0\0\0.\0\0~', 0.00, 7, '2019-11-12 06:45:36'),
(9926, '', 0.00, 6, '2019-11-12 06:45:36'),
(9927, '', 0.00, 7, '2019-11-12 06:45:36'),
(9928, '', 0.00, 6, '2019-11-12 06:45:36'),
(9929, '\0/\0\0\0/\0\0~', 0.00, 7, '2019-11-12 06:45:36'),
(9930, '', 0.00, 6, '2019-11-12 06:45:36'),
(9931, '', 0.00, 7, '2019-11-12 06:45:36'),
(9932, '', 0.00, 6, '2019-11-12 06:45:36'),
(9933, '\00\0\0\00\0\0~', 0.00, 7, '2019-11-12 06:45:36'),
(9934, '', 0.00, 6, '2019-11-12 06:45:37'),
(9935, '', 0.00, 7, '2019-11-12 06:45:37'),
(9936, '', 0.00, 6, '2019-11-12 06:45:37'),
(9937, '', 0.00, 7, '2019-11-12 06:45:37'),
(9938, '', 0.00, 6, '2019-11-12 06:45:37'),
(9939, '\02\0\0\02\0\0~', 0.00, 7, '2019-11-12 06:45:37'),
(9940, '', 0.00, 6, '2019-11-12 06:45:37'),
(9941, '', 0.00, 7, '2019-11-12 06:45:37'),
(9942, '', 0.00, 6, '2019-11-12 06:45:38'),
(9943, '\03\0\0\03\0\0~', 0.00, 7, '2019-11-12 06:45:38'),
(9944, '', 0.00, 6, '2019-11-12 06:45:38'),
(9945, '', 0.00, 7, '2019-11-12 06:45:38'),
(9946, '', 0.00, 6, '2019-11-12 06:45:38'),
(9947, '\04\0\0\04\0\0~', 0.00, 7, '2019-11-12 06:45:38'),
(9948, '', 0.00, 6, '2019-11-12 06:45:38'),
(9949, '', 0.00, 7, '2019-11-12 06:45:38'),
(9950, '', 0.00, 6, '2019-11-12 06:45:38'),
(9951, '\05\0\0\05\0\0~', 0.00, 7, '2019-11-12 06:45:38'),
(9952, '', 0.00, 6, '2019-11-12 06:45:38'),
(9953, '', 0.00, 7, '2019-11-12 06:45:38'),
(9954, '', 0.00, 6, '2019-11-12 06:45:38'),
(9955, '\06\0\0\06\0\0~', 0.00, 7, '2019-11-12 06:45:38'),
(9956, '', 0.00, 6, '2019-11-12 06:45:38'),
(9957, '\06\0\0', 0.00, 7, '2019-11-12 06:45:38'),
(9958, '', 0.00, 6, '2019-11-12 06:45:39'),
(9959, '', 0.00, 7, '2019-11-12 06:45:39'),
(9960, '', 0.00, 6, '2019-11-12 06:45:39'),
(9961, '\07\0\0\07\0\0~', 0.00, 7, '2019-11-12 06:45:39'),
(9962, '', 0.00, 6, '2019-11-12 06:45:39'),
(9963, '', 0.00, 7, '2019-11-12 06:45:39'),
(9964, '', 0.00, 6, '2019-11-12 06:45:39'),
(9965, '\08\0\0\08\0\0~', 0.00, 7, '2019-11-12 06:45:39'),
(9966, '', 0.00, 6, '2019-11-12 06:45:39'),
(9967, '', 0.00, 7, '2019-11-12 06:45:39'),
(9968, '', 0.00, 6, '2019-11-12 06:45:39'),
(9969, '\09\0\0\09\0\0~', 0.00, 7, '2019-11-12 06:45:39'),
(9970, '', 0.00, 6, '2019-11-12 06:45:39'),
(9971, '', 0.00, 7, '2019-11-12 06:45:39'),
(9972, '', 0.00, 6, '2019-11-12 06:45:39'),
(9973, '\0:\0\0\0:\0\0~', 0.00, 7, '2019-11-12 06:45:39'),
(9974, '', 0.00, 6, '2019-11-12 06:45:39'),
(9975, '', 0.00, 7, '2019-11-12 06:45:39'),
(9976, '', 0.00, 6, '2019-11-12 06:45:39'),
(9977, '\0;\0\0\0;\0\0~', 0.00, 7, '2019-11-12 06:45:40'),
(9978, '', 0.00, 6, '2019-11-12 06:45:40'),
(9979, '', 0.00, 7, '2019-11-12 06:45:40'),
(9980, '', 0.00, 6, '2019-11-12 06:45:40'),
(9981, '\0&lt;\0\0\0&lt;\0\0~', 0.00, 7, '2019-11-12 06:45:40'),
(9982, '', 0.00, 6, '2019-11-12 06:45:41'),
(9983, '', 0.00, 7, '2019-11-12 06:45:41'),
(9984, '', 0.00, 6, '2019-11-12 06:45:41'),
(9985, '\0=\0\0\0=\0\0~', 0.00, 7, '2019-11-12 06:45:42'),
(9986, '', 0.00, 6, '2019-11-12 06:45:42'),
(9987, '', 0.00, 7, '2019-11-12 06:45:42'),
(9988, '', 0.00, 6, '2019-11-12 06:45:42'),
(9989, '\0&gt;\0\0\0&gt;\0\0~', 0.00, 7, '2019-11-12 06:45:42'),
(9990, '', 0.00, 6, '2019-11-12 06:45:42'),
(9991, '', 0.00, 7, '2019-11-12 06:45:42'),
(9992, '', 0.00, 6, '2019-11-12 06:45:42'),
(9993, '\0?\0\0\0?\0\0~', 0.00, 7, '2019-11-12 06:45:42'),
(9994, '', 0.00, 6, '2019-11-12 06:45:42'),
(9995, '\0?\0\0!\0\0\0@\0\0\0', 0.00, 7, '2019-11-12 06:45:42'),
(9996, '', 0.00, 6, '2019-11-12 06:45:42'),
(9997, '\0@\0\0\0@\0\0~', 0.00, 7, '2019-11-12 06:45:42'),
(9998, '', 0.00, 6, '2019-11-12 06:45:42'),
(9999, '', 0.00, 7, '2019-11-12 06:45:42'),
(10000, '', 0.00, 6, '2019-11-12 06:45:42'),
(10001, '\0A\0\0\0A\0\0~', 0.00, 7, '2019-11-12 06:45:42'),
(10002, '', 0.00, 6, '2019-11-12 06:45:43'),
(10003, '', 0.00, 7, '2019-11-12 06:45:43'),
(10004, '', 0.00, 6, '2019-11-12 06:45:43'),
(10005, '\0B\0\0\0B\0\0~', 0.00, 7, '2019-11-12 06:45:43'),
(10006, '', 0.00, 6, '2019-11-12 06:45:43'),
(10007, '', 0.00, 7, '2019-11-12 06:45:43'),
(10008, '', 0.00, 6, '2019-11-12 06:45:43'),
(10009, '\0C\0\0\0C\0\0~', 0.00, 7, '2019-11-12 06:45:43'),
(10010, '', 0.00, 6, '2019-11-12 06:45:43'),
(10011, '', 0.00, 7, '2019-11-12 06:45:44'),
(10012, '', 0.00, 6, '2019-11-12 06:45:44'),
(10013, '\0D\0\0\0D\0\0~', 0.00, 7, '2019-11-12 06:45:44'),
(10014, '', 0.00, 6, '2019-11-12 06:45:44'),
(10015, '', 0.00, 7, '2019-11-12 06:45:44'),
(10016, '', 0.00, 6, '2019-11-12 06:45:44'),
(10017, '\0E\0\0\0E\0\0~', 0.00, 7, '2019-11-12 06:45:44'),
(10018, '', 0.00, 6, '2019-11-12 06:45:44'),
(10019, '', 0.00, 7, '2019-11-12 06:45:44'),
(10020, '', 0.00, 6, '2019-11-12 06:45:44'),
(10021, '\0F\0\0\0F\0\0~', 0.00, 7, '2019-11-12 06:45:44'),
(10022, '', 0.00, 6, '2019-11-12 06:45:44'),
(10023, '', 0.00, 7, '2019-11-12 06:45:44'),
(10024, '', 0.00, 6, '2019-11-12 06:45:44'),
(10025, '\0G\0\0\0G\0\0~', 0.00, 7, '2019-11-12 06:45:44'),
(10026, '', 0.00, 6, '2019-11-12 06:45:44'),
(10027, '', 0.00, 7, '2019-11-12 06:45:44'),
(10028, '', 0.00, 6, '2019-11-12 06:45:44'),
(10029, '\0H\0\0\0H\0\0~', 0.00, 7, '2019-11-12 06:45:44'),
(10030, '', 0.00, 6, '2019-11-12 06:45:44'),
(10031, '', 0.00, 7, '2019-11-12 06:45:44'),
(10032, '', 0.00, 6, '2019-11-12 06:45:44'),
(10033, '\0I\0\0\0I\0\0~', 0.00, 7, '2019-11-12 06:45:44'),
(10034, '', 0.00, 6, '2019-11-12 06:45:44'),
(10035, '', 0.00, 7, '2019-11-12 06:45:44'),
(10036, '', 0.00, 6, '2019-11-12 06:45:45'),
(10037, '\0J\0\0\0J\0\0~', 0.00, 7, '2019-11-12 06:45:45'),
(10038, '', 0.00, 6, '2019-11-12 06:45:45'),
(10039, '', 0.00, 7, '2019-11-12 06:45:45'),
(10040, '', 0.00, 6, '2019-11-12 06:45:45'),
(10041, '\0K\0\0\0K\0\0~', 0.00, 7, '2019-11-12 06:45:45'),
(10042, '', 0.00, 6, '2019-11-12 06:45:45'),
(10043, '', 0.00, 7, '2019-11-12 06:45:45'),
(10044, '', 0.00, 6, '2019-11-12 06:45:45'),
(10045, '\0L\0\0\0L\0\0~', 0.00, 7, '2019-11-12 06:45:45'),
(10046, '', 0.00, 6, '2019-11-12 06:45:45'),
(10047, '', 0.00, 7, '2019-11-12 06:45:45'),
(10048, '', 0.00, 6, '2019-11-12 06:45:45'),
(10049, '\0M\0\0\0M\0\0~', 0.00, 7, '2019-11-12 06:45:45'),
(10050, '', 0.00, 6, '2019-11-12 06:45:45'),
(10051, '', 0.00, 7, '2019-11-12 06:45:45'),
(10052, '', 0.00, 6, '2019-11-12 06:45:46'),
(10053, '\0N\0\0\0N\0\0~', 0.00, 7, '2019-11-12 06:45:46'),
(10054, '', 0.00, 6, '2019-11-12 06:45:46'),
(10055, '', 0.00, 7, '2019-11-12 06:45:46'),
(10056, '', 0.00, 6, '2019-11-12 06:45:46'),
(10057, '\0O\0\0\0O\0\0~', 0.00, 7, '2019-11-12 06:45:46'),
(10058, '', 0.00, 6, '2019-11-12 06:45:46'),
(10059, '', 0.00, 7, '2019-11-12 06:45:46'),
(10060, '', 0.00, 6, '2019-11-12 06:45:46'),
(10061, '\0P\0\0\0P\0\0~', 0.00, 7, '2019-11-12 06:45:46'),
(10062, '', 0.00, 6, '2019-11-12 06:45:46'),
(10063, '', 0.00, 7, '2019-11-12 06:45:46'),
(10064, '', 0.00, 6, '2019-11-12 06:45:46'),
(10065, '\0Q\0\0\0Q\0\0~', 0.00, 7, '2019-11-12 06:45:46'),
(10066, '', 0.00, 6, '2019-11-12 06:45:46'),
(10067, '', 0.00, 7, '2019-11-12 06:45:46'),
(10068, '', 0.00, 6, '2019-11-12 06:45:46'),
(10069, '\0R\0\0\0R\0\0~', 0.00, 7, '2019-11-12 06:45:46'),
(10070, '', 0.00, 6, '2019-11-12 06:45:46'),
(10071, '', 0.00, 7, '2019-11-12 06:45:46'),
(10072, '', 0.00, 6, '2019-11-12 06:45:46'),
(10073, '\0S\0\0\0S\0\0~', 0.00, 7, '2019-11-12 06:45:46'),
(10074, '', 0.00, 6, '2019-11-12 06:45:46'),
(10075, '', 0.00, 7, '2019-11-12 06:45:46'),
(10076, '', 0.00, 6, '2019-11-12 06:45:46'),
(10077, '\0T\0\0\0T\0\0~', 0.00, 7, '2019-11-12 06:45:46'),
(10078, '', 0.00, 6, '2019-11-12 06:45:46'),
(10079, '\0T\0\0R', 0.00, 7, '2019-11-12 06:45:47'),
(10080, '', 0.00, 6, '2019-11-12 06:45:47'),
(10081, '', 0.00, 7, '2019-11-12 06:45:47'),
(10082, '', 0.00, 6, '2019-11-12 06:45:47'),
(10083, '\0U\0\0\0U\0\0~', 0.00, 7, '2019-11-12 06:45:47'),
(10084, '', 0.00, 6, '2019-11-12 06:45:47'),
(10085, '', 0.00, 7, '2019-11-12 06:45:47'),
(10086, '', 0.00, 6, '2019-11-12 06:45:47'),
(10087, '\0V\0\0\0V\0\0~', 0.00, 7, '2019-11-12 06:45:47'),
(10088, '', 0.00, 6, '2019-11-12 06:45:47'),
(10089, '', 0.00, 7, '2019-11-12 06:45:47'),
(10090, '', 0.00, 6, '2019-11-12 06:45:47'),
(10091, '\0W\0\0\0W\0\0~', 0.00, 7, '2019-11-12 06:45:47'),
(10092, '', 0.00, 6, '2019-11-12 06:45:47'),
(10093, '', 0.00, 7, '2019-11-12 06:45:47'),
(10094, '', 0.00, 6, '2019-11-12 06:45:47'),
(10095, '\0X\0\0\0X\0\0~', 0.00, 7, '2019-11-12 06:45:47'),
(10096, '', 0.00, 6, '2019-11-12 06:45:47'),
(10097, '', 0.00, 7, '2019-11-12 06:45:47'),
(10098, '', 0.00, 6, '2019-11-12 06:45:48'),
(10099, '\0Y\0\0\0Y\0\0~', 0.00, 7, '2019-11-12 06:45:48'),
(10100, '', 0.00, 6, '2019-11-12 06:45:48'),
(10101, '', 0.00, 7, '2019-11-12 06:45:48'),
(10102, '', 0.00, 6, '2019-11-12 06:45:48'),
(10103, '\0Z\0\0\0Z\0\0~', 0.00, 7, '2019-11-12 06:45:48'),
(10104, '', 0.00, 6, '2019-11-12 06:45:48'),
(10105, '', 0.00, 7, '2019-11-12 06:45:48'),
(10106, '', 0.00, 6, '2019-11-12 06:45:48'),
(10107, '', 0.00, 7, '2019-11-12 06:45:48'),
(10108, '\0\0\0', 0.00, 6, '2019-11-12 06:45:48'),
(10109, '\0\\\0\0\0\\\0\0~', 0.00, 7, '2019-11-12 06:45:48'),
(10110, '\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:48'),
(10111, '', 0.00, 7, '2019-11-12 06:45:48'),
(10112, '', 0.00, 6, '2019-11-12 06:45:48'),
(10113, '\0]\0\0\0]\0\0~', 0.00, 7, '2019-11-12 06:45:48'),
(10114, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:48'),
(10115, '', 0.00, 7, '2019-11-12 06:45:48'),
(10116, '', 0.00, 6, '2019-11-12 06:45:48'),
(10117, '', 0.00, 7, '2019-11-12 06:45:49'),
(10118, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:49'),
(10119, '\0_\0\0\0_\0\0~', 0.00, 7, '2019-11-12 06:45:49'),
(10120, '\0\0\0', 0.00, 6, '2019-11-12 06:45:49'),
(10121, '\0_\0\0*\0\0\0`\0\0\0', 0.00, 7, '2019-11-12 06:45:49'),
(10122, '', 0.00, 6, '2019-11-12 06:45:49'),
(10123, '\0`\0\0\0`\0\0~', 0.00, 7, '2019-11-12 06:45:49'),
(10124, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:49'),
(10125, '', 0.00, 7, '2019-11-12 06:45:49'),
(10126, '', 0.00, 6, '2019-11-12 06:45:49'),
(10127, '\0a\0\0\0a\0\0~', 0.00, 7, '2019-11-12 06:45:49'),
(10128, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:49'),
(10129, '\0a\0\0', 0.00, 7, '2019-11-12 06:45:49'),
(10130, '', 0.00, 6, '2019-11-12 06:45:49'),
(10131, '', 0.00, 7, '2019-11-12 06:45:49'),
(10132, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:49'),
(10133, '\0b\0\0\0b\0\0~', 0.00, 7, '2019-11-12 06:45:49'),
(10134, '', 0.00, 6, '2019-11-12 06:45:49'),
(10135, '', 0.00, 7, '2019-11-12 06:45:49'),
(10136, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:49'),
(10137, '\0c\0\0\0c\0\0~', 0.00, 7, '2019-11-12 06:45:49'),
(10138, '', 0.00, 6, '2019-11-12 06:45:49'),
(10139, '', 0.00, 7, '2019-11-12 06:45:49'),
(10140, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:49'),
(10141, '\0d\0\0\0d\0\0~', 0.00, 7, '2019-11-12 06:45:49'),
(10142, '', 0.00, 6, '2019-11-12 06:45:49'),
(10143, '', 0.00, 7, '2019-11-12 06:45:49'),
(10144, '\0\0\0\0	\0\0~', 0.00, 6, '2019-11-12 06:45:50'),
(10145, '\0e\0\0\0e\0\0~', 0.00, 7, '2019-11-12 06:45:50'),
(10146, '', 0.00, 6, '2019-11-12 06:45:50'),
(10147, '', 0.00, 7, '2019-11-12 06:45:50'),
(10148, '\0	\0\0\0', 0.00, 6, '2019-11-12 06:45:50'),
(10149, '\0f\0\0\0f\0\0~', 0.00, 7, '2019-11-12 06:45:50'),
(10150, '\0\0~', 0.00, 6, '2019-11-12 06:45:50'),
(10151, '', 0.00, 7, '2019-11-12 06:45:50'),
(10152, '', 0.00, 6, '2019-11-12 06:45:50'),
(10153, '\0g\0\0\0g\0\0~', 0.00, 7, '2019-11-12 06:45:50'),
(10154, '\0', 0.00, 6, '2019-11-12 06:45:50'),
(10155, '', 0.00, 7, '2019-11-12 06:45:50'),
(10156, '\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:50'),
(10157, '\0h\0\0\0h\0\0~', 0.00, 7, '2019-11-12 06:45:50'),
(10158, '\0', 0.00, 6, '2019-11-12 06:45:50'),
(10159, '', 0.00, 7, '2019-11-12 06:45:50'),
(10160, '', 0.00, 6, '2019-11-12 06:45:50'),
(10161, '\0i\0\0\0i\0\0~', 0.00, 7, '2019-11-12 06:45:50'),
(10162, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:50'),
(10163, '', 0.00, 7, '2019-11-12 06:45:50'),
(10164, '', 0.00, 6, '2019-11-12 06:45:50'),
(10165, '\0j\0\0\0j\0\0~', 0.00, 7, '2019-11-12 06:45:50'),
(10166, '\0\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:45:50'),
(10167, '', 0.00, 7, '2019-11-12 06:45:50'),
(10168, '', 0.00, 6, '2019-11-12 06:45:51'),
(10169, '\0k\0\0\0k\0\0~', 0.00, 7, '2019-11-12 06:45:51'),
(10170, '\0\r\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:51'),
(10171, '', 0.00, 7, '2019-11-12 06:45:51'),
(10172, '', 0.00, 6, '2019-11-12 06:45:51'),
(10173, '\0l\0\0\0l\0\0~', 0.00, 7, '2019-11-12 06:45:51'),
(10174, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:51'),
(10175, '', 0.00, 7, '2019-11-12 06:45:51'),
(10176, '', 0.00, 6, '2019-11-12 06:45:51'),
(10177, '\0m\0\0\0m\0\0~', 0.00, 7, '2019-11-12 06:45:51'),
(10178, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:51'),
(10179, '', 0.00, 7, '2019-11-12 06:45:51'),
(10180, '', 0.00, 6, '2019-11-12 06:45:51'),
(10181, '\0n\0\0\0n\0\0~', 0.00, 7, '2019-11-12 06:45:51'),
(10182, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:51'),
(10183, '', 0.00, 7, '2019-11-12 06:45:51'),
(10184, '', 0.00, 6, '2019-11-12 06:45:51'),
(10185, '\0o\0\0\0o\0\0~', 0.00, 7, '2019-11-12 06:45:51'),
(10186, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:51'),
(10187, '', 0.00, 7, '2019-11-12 06:45:51'),
(10188, '', 0.00, 6, '2019-11-12 06:45:51'),
(10189, '\0p\0\0\0p\0\0~', 0.00, 7, '2019-11-12 06:45:52'),
(10190, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:52'),
(10191, '', 0.00, 7, '2019-11-12 06:45:52'),
(10192, '', 0.00, 6, '2019-11-12 06:45:52'),
(10193, '', 0.00, 7, '2019-11-12 06:45:52'),
(10194, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:52'),
(10195, '\0r\0\0\0r\0\0~', 0.00, 7, '2019-11-12 06:45:52'),
(10196, '', 0.00, 6, '2019-11-12 06:45:52'),
(10197, '', 0.00, 7, '2019-11-12 06:45:52'),
(10198, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:52'),
(10199, '\0s\0\0\0s\0\0~', 0.00, 7, '2019-11-12 06:45:52'),
(10200, '\0\0\0R', 0.00, 6, '2019-11-12 06:45:52'),
(10201, '', 0.00, 7, '2019-11-12 06:45:52'),
(10202, '', 0.00, 6, '2019-11-12 06:45:52'),
(10203, '\0t\0\0\0t\0\0~', 0.00, 7, '2019-11-12 06:45:52'),
(10204, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:52'),
(10205, '', 0.00, 7, '2019-11-12 06:45:52'),
(10206, '', 0.00, 6, '2019-11-12 06:45:52'),
(10207, '\0u\0\0\0u\0\0~', 0.00, 7, '2019-11-12 06:45:53'),
(10208, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:53'),
(10209, '', 0.00, 7, '2019-11-12 06:45:53'),
(10210, '', 0.00, 6, '2019-11-12 06:45:54'),
(10211, '\0v\0\0\0v\0\0~', 0.00, 7, '2019-11-12 06:45:54'),
(10212, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:54'),
(10213, '', 0.00, 7, '2019-11-12 06:45:54'),
(10214, '', 0.00, 6, '2019-11-12 06:45:54'),
(10215, '\0w\0\0\0w\0\0~', 0.00, 7, '2019-11-12 06:45:54'),
(10216, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:54'),
(10217, '\0w\0\0', 0.00, 7, '2019-11-12 06:45:54'),
(10218, '', 0.00, 6, '2019-11-12 06:45:54'),
(10219, '', 0.00, 7, '2019-11-12 06:45:55'),
(10220, '\0\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:45:55'),
(10221, '\0x\0\0\0x\0\0~', 0.00, 7, '2019-11-12 06:45:55'),
(10222, '', 0.00, 6, '2019-11-12 06:45:55'),
(10223, '', 0.00, 7, '2019-11-12 06:45:55'),
(10224, '\0\Z\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:55'),
(10225, '\0y\0\0\0y\0\0~', 0.00, 7, '2019-11-12 06:45:55'),
(10226, '\0\Z\0\0R', 0.00, 6, '2019-11-12 06:45:55'),
(10227, '', 0.00, 7, '2019-11-12 06:45:55'),
(10228, '', 0.00, 6, '2019-11-12 06:45:55'),
(10229, '\0z\0\0\0z\0\0~', 0.00, 7, '2019-11-12 06:45:55'),
(10230, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:55'),
(10231, '', 0.00, 7, '2019-11-12 06:45:55'),
(10232, '', 0.00, 6, '2019-11-12 06:45:55'),
(10233, '\0{\0\0\0{\0\0~', 0.00, 7, '2019-11-12 06:45:55'),
(10234, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:55'),
(10235, '', 0.00, 7, '2019-11-12 06:45:56'),
(10236, '', 0.00, 6, '2019-11-12 06:45:56'),
(10237, '\0|\0\0\0|\0\0~', 0.00, 7, '2019-11-12 06:45:56'),
(10238, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:56'),
(10239, '', 0.00, 7, '2019-11-12 06:45:56'),
(10240, '', 0.00, 6, '2019-11-12 06:45:56'),
(10241, '\0}\0\0\0}\0\0~', 0.00, 7, '2019-11-12 06:45:56'),
(10242, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:45:56'),
(10243, '', 0.00, 7, '2019-11-12 06:45:56'),
(10244, '', 0.00, 6, '2019-11-12 06:45:56'),
(10245, '\0~\0\0\0~\0\0~', 0.00, 7, '2019-11-12 06:45:57'),
(10246, '\0\0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:45:57'),
(10247, '', 0.00, 7, '2019-11-12 06:45:57'),
(10248, '\0\0\0#\0\0\0 \0\0\0', 0.00, 6, '2019-11-12 06:45:57'),
(10249, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:45:57'),
(10250, '\0 \0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:45:57'),
(10251, '', 0.00, 7, '2019-11-12 06:45:57'),
(10252, '', 0.00, 6, '2019-11-12 06:45:57'),
(10253, '', 0.00, 7, '2019-11-12 06:45:57'),
(10254, '', 0.00, 6, '2019-11-12 06:45:57'),
(10255, '', 0.00, 7, '2019-11-12 06:45:57'),
(10256, '\0&quot;\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:45:57'),
(10257, '', 0.00, 7, '2019-11-12 06:45:58'),
(10258, '', 0.00, 6, '2019-11-12 06:45:58'),
(10259, '', 0.00, 7, '2019-11-12 06:45:58'),
(10260, '\0#\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:45:58'),
(10261, '', 0.00, 7, '2019-11-12 06:45:58'),
(10262, '\0#\0\0', 0.00, 6, '2019-11-12 06:45:58'),
(10263, '', 0.00, 7, '2019-11-12 06:45:58'),
(10264, '', 0.00, 6, '2019-11-12 06:45:58'),
(10265, '', 0.00, 7, '2019-11-12 06:45:58'),
(10266, '\0$\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:45:58'),
(10267, '', 0.00, 7, '2019-11-12 06:45:58'),
(10268, '', 0.00, 6, '2019-11-12 06:45:58'),
(10269, '', 0.00, 7, '2019-11-12 06:45:58'),
(10270, '', 0.00, 6, '2019-11-12 06:45:58'),
(10271, '', 0.00, 7, '2019-11-12 06:45:58'),
(10272, '\0&amp;\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:45:58'),
(10273, '', 0.00, 7, '2019-11-12 06:45:58'),
(10274, '', 0.00, 6, '2019-11-12 06:45:58'),
(10275, '', 0.00, 7, '2019-11-12 06:45:59'),
(10276, '\0\'\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:45:59'),
(10277, '', 0.00, 7, '2019-11-12 06:45:59'),
(10278, '', 0.00, 6, '2019-11-12 06:45:59'),
(10279, '', 0.00, 7, '2019-11-12 06:45:59'),
(10280, '\0(\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:45:59'),
(10281, '', 0.00, 7, '2019-11-12 06:45:59'),
(10282, '', 0.00, 6, '2019-11-12 06:45:59'),
(10283, '', 0.00, 7, '2019-11-12 06:45:59'),
(10284, '\0)\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:45:59'),
(10285, '', 0.00, 7, '2019-11-12 06:45:59'),
(10286, '', 0.00, 6, '2019-11-12 06:45:59'),
(10287, '', 0.00, 7, '2019-11-12 06:45:59'),
(10288, '\0*\0\0\0+\0\0~', 0.00, 6, '2019-11-12 06:45:59'),
(10289, '', 0.00, 7, '2019-11-12 06:45:59'),
(10290, '', 0.00, 6, '2019-11-12 06:45:59'),
(10291, '', 0.00, 7, '2019-11-12 06:45:59'),
(10292, '\0+\0\0\0', 0.00, 6, '2019-11-12 06:46:00'),
(10293, '', 0.00, 7, '2019-11-12 06:46:00'),
(10294, '', 0.00, 6, '2019-11-12 06:46:00'),
(10295, '', 0.00, 7, '2019-11-12 06:46:00'),
(10296, '\0', 0.00, 6, '2019-11-12 06:46:00'),
(10297, '', 0.00, 7, '2019-11-12 06:46:00'),
(10298, '\0', 0.00, 6, '2019-11-12 06:46:00'),
(10299, '', 0.00, 7, '2019-11-12 06:46:00'),
(10300, '\0-\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:46:00'),
(10301, '', 0.00, 7, '2019-11-12 06:46:00'),
(10302, '', 0.00, 6, '2019-11-12 06:46:00'),
(10303, '', 0.00, 7, '2019-11-12 06:46:00'),
(10304, '\0.\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:46:00'),
(10305, '', 0.00, 7, '2019-11-12 06:46:00'),
(10306, '', 0.00, 6, '2019-11-12 06:46:00'),
(10307, '', 0.00, 7, '2019-11-12 06:46:00'),
(10308, '\0/\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:46:00'),
(10309, '', 0.00, 7, '2019-11-12 06:46:00'),
(10310, '', 0.00, 6, '2019-11-12 06:46:00'),
(10311, '', 0.00, 7, '2019-11-12 06:46:00'),
(10312, '', 0.00, 6, '2019-11-12 06:46:00'),
(10313, '', 0.00, 7, '2019-11-12 06:46:00'),
(10314, '\01\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:46:00'),
(10315, '', 0.00, 7, '2019-11-12 06:46:00'),
(10316, '', 0.00, 6, '2019-11-12 06:46:01'),
(10317, '', 0.00, 7, '2019-11-12 06:46:01'),
(10318, '\02\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:46:01'),
(10319, '', 0.00, 7, '2019-11-12 06:46:01'),
(10320, '', 0.00, 6, '2019-11-12 06:46:01'),
(10321, '', 0.00, 7, '2019-11-12 06:46:01'),
(10322, '\03\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:46:01'),
(10323, '', 0.00, 7, '2019-11-12 06:46:01'),
(10324, '', 0.00, 6, '2019-11-12 06:46:01'),
(10325, '', 0.00, 7, '2019-11-12 06:46:01'),
(10326, '\04\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:46:01'),
(10327, '', 0.00, 7, '2019-11-12 06:46:01'),
(10328, '', 0.00, 6, '2019-11-12 06:46:01'),
(10329, '', 0.00, 7, '2019-11-12 06:46:01'),
(10330, '\05\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:46:01'),
(10331, '', 0.00, 7, '2019-11-12 06:46:01'),
(10332, '', 0.00, 6, '2019-11-12 06:46:02'),
(10333, '', 0.00, 7, '2019-11-12 06:46:02'),
(10334, '\06\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:46:02'),
(10335, '', 0.00, 7, '2019-11-12 06:46:02'),
(10336, '', 0.00, 6, '2019-11-12 06:46:02'),
(10337, '', 0.00, 7, '2019-11-12 06:46:02'),
(10338, '\07\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:46:02'),
(10339, '', 0.00, 7, '2019-11-12 06:46:02'),
(10340, '', 0.00, 6, '2019-11-12 06:46:02'),
(10341, '', 0.00, 7, '2019-11-12 06:46:02'),
(10342, '\08\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:46:02'),
(10343, '', 0.00, 7, '2019-11-12 06:46:02'),
(10344, '', 0.00, 6, '2019-11-12 06:46:02'),
(10345, '', 0.00, 7, '2019-11-12 06:46:02'),
(10346, '\09\0\0\0:\0\0~', 0.00, 6, '2019-11-12 06:46:02'),
(10347, '', 0.00, 7, '2019-11-12 06:46:02'),
(10348, '', 0.00, 6, '2019-11-12 06:46:02'),
(10349, '', 0.00, 7, '2019-11-12 06:46:02'),
(10350, '\0:\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:46:02'),
(10351, '', 0.00, 7, '2019-11-12 06:46:03'),
(10352, '', 0.00, 6, '2019-11-12 06:46:03'),
(10353, '', 0.00, 7, '2019-11-12 06:46:03'),
(10354, '\0;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:46:03'),
(10355, '', 0.00, 7, '2019-11-12 06:46:03'),
(10356, '', 0.00, 6, '2019-11-12 06:46:03'),
(10357, '', 0.00, 7, '2019-11-12 06:46:03'),
(10358, '\0&lt;\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:46:03'),
(10359, '', 0.00, 7, '2019-11-12 06:46:03'),
(10360, '', 0.00, 6, '2019-11-12 06:46:03'),
(10361, '', 0.00, 7, '2019-11-12 06:46:03'),
(10362, '\0=\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:46:03'),
(10363, '', 0.00, 7, '2019-11-12 06:46:03'),
(10364, '', 0.00, 6, '2019-11-12 06:46:03'),
(10365, '', 0.00, 7, '2019-11-12 06:46:03'),
(10366, '\0&gt;\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:46:03'),
(10367, '', 0.00, 7, '2019-11-12 06:46:03'),
(10368, '', 0.00, 6, '2019-11-12 06:46:03'),
(10369, '', 0.00, 7, '2019-11-12 06:46:03'),
(10370, '\0?\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:46:03'),
(10371, '', 0.00, 7, '2019-11-12 06:46:03'),
(10372, '\0?\0\02\0\0\0@\0\0\0', 0.00, 6, '2019-11-12 06:46:04'),
(10373, '', 0.00, 7, '2019-11-12 06:46:04'),
(10374, '\0@\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:46:04'),
(10375, '', 0.00, 7, '2019-11-12 06:46:04'),
(10376, '', 0.00, 6, '2019-11-12 06:46:04'),
(10377, '', 0.00, 7, '2019-11-12 06:46:04'),
(10378, '\0A\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:46:04'),
(10379, '', 0.00, 7, '2019-11-12 06:46:04'),
(10380, '', 0.00, 6, '2019-11-12 06:46:04'),
(10381, '', 0.00, 7, '2019-11-12 06:46:04'),
(10382, '\0B\0\0\0C\0\0~', 0.00, 6, '2019-11-12 06:46:04'),
(10383, '', 0.00, 7, '2019-11-12 06:46:04'),
(10384, '\0B\0\0R', 0.00, 6, '2019-11-12 06:46:04'),
(10385, '', 0.00, 7, '2019-11-12 06:46:04'),
(10386, '', 0.00, 6, '2019-11-12 06:46:04'),
(10387, '', 0.00, 7, '2019-11-12 06:46:04'),
(10388, '\0C\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:46:04'),
(10389, '', 0.00, 7, '2019-11-12 06:46:05'),
(10390, '', 0.00, 6, '2019-11-12 06:46:05'),
(10391, '', 0.00, 7, '2019-11-12 06:46:05'),
(10392, '\0D\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:46:05'),
(10393, '', 0.00, 7, '2019-11-12 06:46:05'),
(10394, '', 0.00, 6, '2019-11-12 06:46:05'),
(10395, '', 0.00, 7, '2019-11-12 06:46:05'),
(10396, '\0E\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:46:05'),
(10397, '', 0.00, 7, '2019-11-12 06:46:05'),
(10398, '', 0.00, 6, '2019-11-12 06:46:05'),
(10399, '', 0.00, 7, '2019-11-12 06:46:05'),
(10400, '\0F\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:46:06'),
(10401, '', 0.00, 7, '2019-11-12 06:46:06'),
(10402, '', 0.00, 6, '2019-11-12 06:46:06'),
(10403, '', 0.00, 7, '2019-11-12 06:46:06'),
(10404, '\0G\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:46:06'),
(10405, '', 0.00, 7, '2019-11-12 06:46:06'),
(10406, '', 0.00, 6, '2019-11-12 06:46:06'),
(10407, '', 0.00, 7, '2019-11-12 06:46:06'),
(10408, '\0H\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:46:06'),
(10409, '', 0.00, 7, '2019-11-12 06:46:06'),
(10410, '', 0.00, 6, '2019-11-12 06:46:06'),
(10411, '', 0.00, 7, '2019-11-12 06:46:06'),
(10412, '\0I\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:46:06'),
(10413, '', 0.00, 7, '2019-11-12 06:46:06'),
(10414, '', 0.00, 6, '2019-11-12 06:46:06'),
(10415, '', 0.00, 7, '2019-11-12 06:46:06'),
(10416, '\0J\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:46:06'),
(10417, '', 0.00, 7, '2019-11-12 06:46:06'),
(10418, '', 0.00, 6, '2019-11-12 06:46:06'),
(10419, '', 0.00, 7, '2019-11-12 06:46:06'),
(10420, '\0K\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:46:06'),
(10421, '', 0.00, 7, '2019-11-12 06:46:06'),
(10422, '\0K\0\0S', 0.00, 6, '2019-11-12 06:46:06'),
(10423, '', 0.00, 7, '2019-11-12 06:46:07'),
(10424, '', 0.00, 6, '2019-11-12 06:46:07'),
(10425, '', 0.00, 7, '2019-11-12 06:46:07'),
(10426, '\0L\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:46:07'),
(10427, '', 0.00, 7, '2019-11-12 06:46:07'),
(10428, '', 0.00, 6, '2019-11-12 06:46:07'),
(10429, '', 0.00, 7, '2019-11-12 06:46:07'),
(10430, '\0M\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:46:07'),
(10431, '', 0.00, 7, '2019-11-12 06:46:07'),
(10432, '', 0.00, 6, '2019-11-12 06:46:07'),
(10433, '', 0.00, 7, '2019-11-12 06:46:07'),
(10434, '\0N\0\0\0O\0\0~', 0.00, 6, '2019-11-12 06:46:07'),
(10435, '', 0.00, 7, '2019-11-12 06:46:07'),
(10436, '', 0.00, 6, '2019-11-12 06:46:07'),
(10437, '', 0.00, 7, '2019-11-12 06:46:08'),
(10438, '\0O\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:46:08'),
(10439, '', 0.00, 7, '2019-11-12 06:46:08'),
(10440, '', 0.00, 6, '2019-11-12 06:46:08'),
(10441, '', 0.00, 7, '2019-11-12 06:46:08'),
(10442, '\0P\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:46:09'),
(10443, '', 0.00, 7, '2019-11-12 06:46:09'),
(10444, '\0P\0\0', 0.00, 6, '2019-11-12 06:46:09'),
(10445, '', 0.00, 7, '2019-11-12 06:46:09'),
(10446, '', 0.00, 6, '2019-11-12 06:46:09'),
(10447, '', 0.00, 7, '2019-11-12 06:46:09'),
(10448, '\0Q\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:46:09'),
(10449, '', 0.00, 7, '2019-11-12 06:46:09'),
(10450, '', 0.00, 6, '2019-11-12 06:46:09'),
(10451, '', 0.00, 7, '2019-11-12 06:46:09'),
(10452, '\0R\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:46:09'),
(10453, '', 0.00, 7, '2019-11-12 06:46:09'),
(10454, '', 0.00, 6, '2019-11-12 06:46:09'),
(10455, '', 0.00, 7, '2019-11-12 06:46:09'),
(10456, '\0S\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:46:09'),
(10457, '', 0.00, 7, '2019-11-12 06:46:10'),
(10458, '', 0.00, 6, '2019-11-12 06:46:10'),
(10459, '', 0.00, 7, '2019-11-12 06:46:10'),
(10460, '\0T\0\0\0U\0\0~', 0.00, 6, '2019-11-12 06:46:10'),
(10461, '', 0.00, 7, '2019-11-12 06:46:10'),
(10462, '', 0.00, 6, '2019-11-12 06:46:10'),
(10463, '', 0.00, 7, '2019-11-12 06:46:10'),
(10464, '\0U\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:46:10'),
(10465, '', 0.00, 7, '2019-11-12 06:46:10'),
(10466, '', 0.00, 6, '2019-11-12 06:46:10'),
(10467, '', 0.00, 7, '2019-11-12 06:46:10'),
(10468, '\0V\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:46:10'),
(10469, '', 0.00, 7, '2019-11-12 06:46:10'),
(10470, '', 0.00, 6, '2019-11-12 06:46:10'),
(10471, '', 0.00, 7, '2019-11-12 06:46:10'),
(10472, '\0W\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:46:10'),
(10473, '\0\0Ixime', 0.00, 7, '2019-11-12 06:46:10'),
(10474, '', 0.00, 6, '2019-11-12 06:46:11'),
(10475, '\0\0Manix', 0.00, 7, '2019-11-12 06:46:11'),
(10476, '\0X\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:46:11'),
(10477, '\0\0Pelox', 0.00, 7, '2019-11-12 06:46:11'),
(10478, '', 0.00, 6, '2019-11-12 06:46:11'),
(10479, '\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0 \0\0!\0\0&quot;\0\0#\0\0$\0\0%\0\0&amp;\0\0\'\0\0(\0\0)\0\0*\0\0+\0\0', 0.00, 7, '2019-11-12 06:46:11'),
(10480, '\0Y\0\0\0Z\0\0~', 0.00, 6, '2019-11-12 06:46:11'),
(10481, '\0\0Supracin', 0.00, 7, '2019-11-12 06:46:11'),
(10482, '', 0.00, 6, '2019-11-12 06:46:11'),
(10483, '\0\0Tetacid ', 0.00, 7, '2019-11-12 06:46:11'),
(10484, '\0Z\0\0\0[\0\0~', 0.00, 6, '2019-11-12 06:46:11'),
(10485, '', 0.00, 7, '2019-11-12 06:46:11'),
(10486, '', 0.00, 6, '2019-11-12 06:46:12'),
(10487, '\0\0\0\0', 0.00, 7, '2019-11-12 06:46:12'),
(10488, '\0[\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:46:12'),
(10489, '', 0.00, 7, '2019-11-12 06:46:12'),
(10490, '', 0.00, 6, '2019-11-12 06:46:12'),
(10491, '', 0.00, 7, '2019-11-12 06:46:12'),
(10492, '\0\\\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:46:12'),
(10493, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:12'),
(10494, '', 0.00, 6, '2019-11-12 06:46:12'),
(10495, '', 0.00, 7, '2019-11-12 06:46:12'),
(10496, '\0]\0\0\0^\0\0~', 0.00, 6, '2019-11-12 06:46:12'),
(10497, '', 0.00, 7, '2019-11-12 06:46:12'),
(10498, '', 0.00, 6, '2019-11-12 06:46:12'),
(10499, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:12'),
(10500, '\0^\0\0\0_\0\0~', 0.00, 6, '2019-11-12 06:46:12'),
(10501, '', 0.00, 7, '2019-11-12 06:46:12'),
(10502, '', 0.00, 6, '2019-11-12 06:46:12'),
(10503, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:13'),
(10504, '\0_\0\0\0`\0\0\0_\0\0fffff&amp;r@\0`\0\0\0', 0.00, 6, '2019-11-12 06:46:13'),
(10505, '', 0.00, 7, '2019-11-12 06:46:13'),
(10506, '\0`\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:46:13'),
(10507, '', 0.00, 7, '2019-11-12 06:46:13'),
(10508, '', 0.00, 6, '2019-11-12 06:46:13'),
(10509, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:14'),
(10510, '\0a\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:46:14'),
(10511, '', 0.00, 7, '2019-11-12 06:46:14'),
(10512, '', 0.00, 6, '2019-11-12 06:46:14'),
(10513, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:14'),
(10514, '\0b\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:46:14'),
(10515, '', 0.00, 7, '2019-11-12 06:46:14'),
(10516, '', 0.00, 6, '2019-11-12 06:46:15'),
(10517, '\0\0\0\0\0	\0\0\0~', 0.00, 7, '2019-11-12 06:46:15'),
(10518, '\0c\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:46:15'),
(10519, '', 0.00, 7, '2019-11-12 06:46:15'),
(10520, '', 0.00, 6, '2019-11-12 06:46:15'),
(10521, '\0	\0\0\0\0', 0.00, 7, '2019-11-12 06:46:15'),
(10522, '\0d\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:46:15'),
(10523, '\0\0\0~', 0.00, 7, '2019-11-12 06:46:15'),
(10524, '', 0.00, 6, '2019-11-12 06:46:15'),
(10525, '', 0.00, 7, '2019-11-12 06:46:16'),
(10526, '\0e\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:46:16'),
(10527, '\0', 0.00, 7, '2019-11-12 06:46:16'),
(10528, '', 0.00, 6, '2019-11-12 06:46:16'),
(10529, '\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:16'),
(10530, '\0f\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:46:17'),
(10531, '\0', 0.00, 7, '2019-11-12 06:46:17'),
(10532, '', 0.00, 6, '2019-11-12 06:46:17'),
(10533, '', 0.00, 7, '2019-11-12 06:46:17');
INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(10534, '\0g\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:46:17'),
(10535, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:17'),
(10536, '', 0.00, 6, '2019-11-12 06:46:17'),
(10537, '\0\0\0\0', 0.00, 7, '2019-11-12 06:46:17'),
(10538, '\0h\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:46:17'),
(10539, '', 0.00, 7, '2019-11-12 06:46:17'),
(10540, '', 0.00, 6, '2019-11-12 06:46:17'),
(10541, '\0\0\0\0\0\r\0\0\0~', 0.00, 7, '2019-11-12 06:46:17'),
(10542, '\0i\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:46:17'),
(10543, '', 0.00, 7, '2019-11-12 06:46:17'),
(10544, '', 0.00, 6, '2019-11-12 06:46:17'),
(10545, '\0\r\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:17'),
(10546, '\0j\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:46:17'),
(10547, '', 0.00, 7, '2019-11-12 06:46:17'),
(10548, '', 0.00, 6, '2019-11-12 06:46:17'),
(10549, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:17'),
(10550, '\0k\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:46:18'),
(10551, '', 0.00, 7, '2019-11-12 06:46:18'),
(10552, '', 0.00, 6, '2019-11-12 06:46:18'),
(10553, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:18'),
(10554, '\0l\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:46:18'),
(10555, '', 0.00, 7, '2019-11-12 06:46:18'),
(10556, '', 0.00, 6, '2019-11-12 06:46:18'),
(10557, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:18'),
(10558, '\0m\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:46:19'),
(10559, '', 0.00, 7, '2019-11-12 06:46:19'),
(10560, '', 0.00, 6, '2019-11-12 06:46:19'),
(10561, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:19'),
(10562, '\0n\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:46:19'),
(10563, '\0\0\0\0P', 0.00, 7, '2019-11-12 06:46:19'),
(10564, '', 0.00, 6, '2019-11-12 06:46:19'),
(10565, '', 0.00, 7, '2019-11-12 06:46:19'),
(10566, '\0o\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:46:19'),
(10567, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:19'),
(10568, '', 0.00, 6, '2019-11-12 06:46:19'),
(10569, '', 0.00, 7, '2019-11-12 06:46:19'),
(10570, '\0p\0\0\0q\0\0~', 0.00, 6, '2019-11-12 06:46:19'),
(10571, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:19'),
(10572, '\0p\0\0R', 0.00, 6, '2019-11-12 06:46:19'),
(10573, '', 0.00, 7, '2019-11-12 06:46:19'),
(10574, '', 0.00, 6, '2019-11-12 06:46:19'),
(10575, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:19'),
(10576, '\0q\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:46:19'),
(10577, '\0\0\0\0', 0.00, 7, '2019-11-12 06:46:19'),
(10578, '', 0.00, 6, '2019-11-12 06:46:19'),
(10579, '', 0.00, 7, '2019-11-12 06:46:19'),
(10580, '\0r\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:46:19'),
(10581, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:19'),
(10582, '', 0.00, 6, '2019-11-12 06:46:19'),
(10583, '', 0.00, 7, '2019-11-12 06:46:19'),
(10584, '\0s\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:46:20'),
(10585, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:20'),
(10586, '', 0.00, 6, '2019-11-12 06:46:20'),
(10587, '', 0.00, 7, '2019-11-12 06:46:20'),
(10588, '\0t\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:46:20'),
(10589, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:20'),
(10590, '', 0.00, 6, '2019-11-12 06:46:20'),
(10591, '', 0.00, 7, '2019-11-12 06:46:20'),
(10592, '\0u\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:46:20'),
(10593, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:20'),
(10594, '', 0.00, 6, '2019-11-12 06:46:20'),
(10595, '', 0.00, 7, '2019-11-12 06:46:20'),
(10596, '\0v\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:46:20'),
(10597, '\0\0\0\0\0\Z\0\0\0~', 0.00, 7, '2019-11-12 06:46:20'),
(10598, '', 0.00, 6, '2019-11-12 06:46:20'),
(10599, '', 0.00, 7, '2019-11-12 06:46:21'),
(10600, '\0w\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:46:21'),
(10601, '\0\Z\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:21'),
(10602, '\0w\0\0', 0.00, 6, '2019-11-12 06:46:21'),
(10603, '', 0.00, 7, '2019-11-12 06:46:21'),
(10604, '', 0.00, 6, '2019-11-12 06:46:21'),
(10605, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:21'),
(10606, '\0x\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:46:21'),
(10607, '', 0.00, 7, '2019-11-12 06:46:21'),
(10608, '', 0.00, 6, '2019-11-12 06:46:21'),
(10609, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:21'),
(10610, '\0y\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:46:21'),
(10611, '', 0.00, 7, '2019-11-12 06:46:21'),
(10612, '', 0.00, 6, '2019-11-12 06:46:21'),
(10613, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:21'),
(10614, '\0z\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:46:22'),
(10615, '', 0.00, 7, '2019-11-12 06:46:22'),
(10616, '', 0.00, 6, '2019-11-12 06:46:22'),
(10617, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:46:22'),
(10618, '\0{\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:46:22'),
(10619, '', 0.00, 7, '2019-11-12 06:46:22'),
(10620, '', 0.00, 6, '2019-11-12 06:46:22'),
(10621, '\0\0\0\0\0 \0\0\0~', 0.00, 7, '2019-11-12 06:46:22'),
(10622, '\0|\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:46:22'),
(10623, '\0\0\0\0#F\0\0\0 \0\0\0\0', 0.00, 7, '2019-11-12 06:46:22'),
(10624, '', 0.00, 6, '2019-11-12 06:46:22'),
(10625, '\0 \0\0\0\0!\0\0\0~', 0.00, 7, '2019-11-12 06:46:22'),
(10626, '\0}\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:46:22'),
(10627, '', 0.00, 7, '2019-11-12 06:46:22'),
(10628, '', 0.00, 6, '2019-11-12 06:46:22'),
(10629, '\0!\0\0\0\0&quot;\0\0\0~', 0.00, 7, '2019-11-12 06:46:22'),
(10630, '\0~\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:46:22'),
(10631, '', 0.00, 7, '2019-11-12 06:46:22'),
(10632, '', 0.00, 6, '2019-11-12 06:46:22'),
(10633, '\0&quot;\0\0\0\0#\0\0\0~', 0.00, 7, '2019-11-12 06:46:22'),
(10634, '', 0.00, 6, '2019-11-12 06:46:22'),
(10635, '', 0.00, 7, '2019-11-12 06:46:22'),
(10636, '', 0.00, 6, '2019-11-12 06:46:22'),
(10637, '\0#\0\0\0\0$\0\0\0~', 0.00, 7, '2019-11-12 06:46:22'),
(10638, '', 0.00, 6, '2019-11-12 06:46:22'),
(10639, '', 0.00, 7, '2019-11-12 06:46:23'),
(10640, '', 0.00, 6, '2019-11-12 06:46:23'),
(10641, '\0$\0\0\0\0%\0\0\0~', 0.00, 7, '2019-11-12 06:46:23'),
(10642, '', 0.00, 6, '2019-11-12 06:46:23'),
(10643, '', 0.00, 7, '2019-11-12 06:46:23'),
(10644, '', 0.00, 6, '2019-11-12 06:46:23'),
(10645, '\0%\0\0\0\0&amp;\0\0\0~', 0.00, 7, '2019-11-12 06:46:23'),
(10646, '', 0.00, 6, '2019-11-12 06:46:23'),
(10647, '', 0.00, 7, '2019-11-12 06:46:23'),
(10648, '', 0.00, 6, '2019-11-12 06:46:23'),
(10649, '\0&amp;\0\0\0\0\'\0\0\0~', 0.00, 7, '2019-11-12 06:46:23'),
(10650, '', 0.00, 6, '2019-11-12 06:46:23'),
(10651, '', 0.00, 7, '2019-11-12 06:46:23'),
(10652, '', 0.00, 6, '2019-11-12 06:46:23'),
(10653, '\0\'\0\0\0\0(\0\0\0~', 0.00, 7, '2019-11-12 06:46:23'),
(10654, '', 0.00, 6, '2019-11-12 06:46:24'),
(10655, '', 0.00, 7, '2019-11-12 06:46:24'),
(10656, '', 0.00, 6, '2019-11-12 06:46:24'),
(10657, '', 0.00, 7, '2019-11-12 06:46:24'),
(10658, '', 0.00, 6, '2019-11-12 06:46:24'),
(10659, '\0)\0\0\0\0*\0\0\0~', 0.00, 7, '2019-11-12 06:46:24'),
(10660, '', 0.00, 6, '2019-11-12 06:46:24'),
(10661, '', 0.00, 7, '2019-11-12 06:46:24'),
(10662, '', 0.00, 6, '2019-11-12 06:46:24'),
(10663, '\0*\0\0\0\0+\0\0\0~', 0.00, 7, '2019-11-12 06:46:24'),
(10664, '', 0.00, 6, '2019-11-12 06:46:24'),
(10665, '', 0.00, 7, '2019-11-12 06:46:24'),
(10666, '', 0.00, 6, '2019-11-12 06:46:24'),
(10667, '\0+\0\0\0\0', 0.00, 7, '2019-11-12 06:46:24'),
(10668, '', 0.00, 6, '2019-11-12 06:46:24'),
(10669, '', 0.00, 7, '2019-11-12 06:46:24'),
(10670, '', 0.00, 6, '2019-11-12 06:46:24'),
(10671, '\0', 0.00, 7, '2019-11-12 06:46:24'),
(10672, '', 0.00, 6, '2019-11-12 06:46:24'),
(10673, '\0', 0.00, 7, '2019-11-12 06:46:25'),
(10674, '', 0.00, 6, '2019-11-12 06:46:25'),
(10675, '\0-\0\0\0\0.\0\0\0~', 0.00, 7, '2019-11-12 06:46:25'),
(10676, '', 0.00, 6, '2019-11-12 06:46:25'),
(10677, '', 0.00, 7, '2019-11-12 06:46:25'),
(10678, '', 0.00, 6, '2019-11-12 06:46:25'),
(10679, '\0.\0\0\0\0/\0\0\0~', 0.00, 7, '2019-11-12 06:46:25'),
(10680, '', 0.00, 6, '2019-11-12 06:46:25'),
(10681, '', 0.00, 7, '2019-11-12 06:46:25'),
(10682, '', 0.00, 6, '2019-11-12 06:46:26'),
(10683, '\0/\0\0\0\00\0\0\0~', 0.00, 7, '2019-11-12 06:46:26'),
(10684, '', 0.00, 6, '2019-11-12 06:46:26'),
(10685, '', 0.00, 7, '2019-11-12 06:46:26'),
(10686, '', 0.00, 6, '2019-11-12 06:46:26'),
(10687, '\00\0\0\0\01\0\0\0~', 0.00, 7, '2019-11-12 06:46:26'),
(10688, '', 0.00, 6, '2019-11-12 06:46:26'),
(10689, '', 0.00, 7, '2019-11-12 06:46:26'),
(10690, '', 0.00, 6, '2019-11-12 06:46:26'),
(10691, '\01\0\0\0\02\0\0\0~', 0.00, 7, '2019-11-12 06:46:26'),
(10692, '', 0.00, 6, '2019-11-12 06:46:26'),
(10693, '', 0.00, 7, '2019-11-12 06:46:26'),
(10694, '', 0.00, 6, '2019-11-12 06:46:27'),
(10695, '\02\0\0\0\03\0\0\0~', 0.00, 7, '2019-11-12 06:46:27'),
(10696, '', 0.00, 6, '2019-11-12 06:46:27'),
(10697, '', 0.00, 7, '2019-11-12 06:46:27'),
(10698, '', 0.00, 6, '2019-11-12 06:46:27'),
(10699, '\03\0\0\0\04\0\0\0~', 0.00, 7, '2019-11-12 06:46:27'),
(10700, '', 0.00, 6, '2019-11-12 06:46:27'),
(10701, '\03\0\0\0', 0.00, 7, '2019-11-12 06:46:27'),
(10702, '', 0.00, 6, '2019-11-12 06:46:27'),
(10703, '', 0.00, 7, '2019-11-12 06:46:27'),
(10704, '', 0.00, 6, '2019-11-12 06:46:28'),
(10705, '\04\0\0\0\05\0\0\0~', 0.00, 7, '2019-11-12 06:46:28'),
(10706, '', 0.00, 6, '2019-11-12 06:46:28'),
(10707, '', 0.00, 7, '2019-11-12 06:46:28'),
(10708, '', 0.00, 6, '2019-11-12 06:46:28'),
(10709, '\05\0\0\0\06\0\0\0~', 0.00, 7, '2019-11-12 06:46:28'),
(10710, '', 0.00, 6, '2019-11-12 06:46:28'),
(10711, '', 0.00, 7, '2019-11-12 06:46:28'),
(10712, '', 0.00, 6, '2019-11-12 06:46:28'),
(10713, '\06\0\0\0\07\0\0\0~', 0.00, 7, '2019-11-12 06:46:28'),
(10714, '', 0.00, 6, '2019-11-12 06:46:28'),
(10715, '', 0.00, 7, '2019-11-12 06:46:28'),
(10716, '', 0.00, 6, '2019-11-12 06:46:29'),
(10717, '\07\0\0\0\08\0\0\0~', 0.00, 7, '2019-11-12 06:46:29'),
(10718, '', 0.00, 6, '2019-11-12 06:46:29'),
(10719, '', 0.00, 7, '2019-11-12 06:46:29'),
(10720, '', 0.00, 6, '2019-11-12 06:46:29'),
(10721, '', 0.00, 7, '2019-11-12 06:46:29'),
(10722, '', 0.00, 6, '2019-11-12 06:46:29'),
(10723, '\09\0\0\0\0:\0\0\0~', 0.00, 7, '2019-11-12 06:46:29'),
(10724, '', 0.00, 6, '2019-11-12 06:46:29'),
(10725, '', 0.00, 7, '2019-11-12 06:46:29'),
(10726, '', 0.00, 6, '2019-11-12 06:46:29'),
(10727, '\0:\0\0\0\0;\0\0\0~', 0.00, 7, '2019-11-12 06:46:29'),
(10728, '', 0.00, 6, '2019-11-12 06:46:29'),
(10729, '', 0.00, 7, '2019-11-12 06:46:29'),
(10730, '', 0.00, 6, '2019-11-12 06:46:29'),
(10731, '\0;\0\0\0\0&lt;\0\0\0~', 0.00, 7, '2019-11-12 06:46:29'),
(10732, '', 0.00, 6, '2019-11-12 06:46:29'),
(10733, '', 0.00, 7, '2019-11-12 06:46:29'),
(10734, '', 0.00, 6, '2019-11-12 06:46:29'),
(10735, '\0&lt;\0\0\0\0=\0\0\0~', 0.00, 7, '2019-11-12 06:46:29'),
(10736, '', 0.00, 6, '2019-11-12 06:46:29'),
(10737, '', 0.00, 7, '2019-11-12 06:46:29'),
(10738, '', 0.00, 6, '2019-11-12 06:46:29'),
(10739, '\0=\0\0\0\0&gt;\0\0\0~', 0.00, 7, '2019-11-12 06:46:29'),
(10740, '', 0.00, 6, '2019-11-12 06:46:29'),
(10741, '', 0.00, 7, '2019-11-12 06:46:30'),
(10742, '', 0.00, 6, '2019-11-12 06:46:30'),
(10743, '', 0.00, 7, '2019-11-12 06:46:30'),
(10744, '', 0.00, 6, '2019-11-12 06:46:30'),
(10745, '\0?\0\0\0\0@\0\0\0~', 0.00, 7, '2019-11-12 06:46:30'),
(10746, '', 0.00, 6, '2019-11-12 06:46:30'),
(10747, '', 0.00, 7, '2019-11-12 06:46:30'),
(10748, '', 0.00, 6, '2019-11-12 06:46:30'),
(10749, '\0@\0\0\0\0A\0\0\0~', 0.00, 7, '2019-11-12 06:46:30'),
(10750, '', 0.00, 6, '2019-11-12 06:46:30'),
(10751, '', 0.00, 7, '2019-11-12 06:46:30'),
(10752, '', 0.00, 6, '2019-11-12 06:46:30'),
(10753, '\0A\0\0\0\0B\0\0\0~', 0.00, 7, '2019-11-12 06:46:30'),
(10754, '', 0.00, 6, '2019-11-12 06:46:30'),
(10755, '', 0.00, 7, '2019-11-12 06:46:30'),
(10756, '', 0.00, 6, '2019-11-12 06:46:30'),
(10757, '\0B\0\0\0\0C\0\0\0~', 0.00, 7, '2019-11-12 06:46:30'),
(10758, '', 0.00, 6, '2019-11-12 06:46:30'),
(10759, '', 0.00, 7, '2019-11-12 06:46:31'),
(10760, '', 0.00, 6, '2019-11-12 06:46:31'),
(10761, '\0C\0\0\0\0D\0\0\0~', 0.00, 7, '2019-11-12 06:46:31'),
(10762, '', 0.00, 6, '2019-11-12 06:46:31'),
(10763, '', 0.00, 7, '2019-11-12 06:46:31'),
(10764, '', 0.00, 6, '2019-11-12 06:46:31'),
(10765, '\0D\0\0\0\0E\0\0\0~', 0.00, 7, '2019-11-12 06:46:31'),
(10766, '', 0.00, 6, '2019-11-12 06:46:31'),
(10767, '', 0.00, 7, '2019-11-12 06:46:31'),
(10768, '', 0.00, 6, '2019-11-12 06:46:31'),
(10769, '', 0.00, 7, '2019-11-12 06:46:31'),
(10770, '', 0.00, 6, '2019-11-12 06:46:31'),
(10771, '\0F\0\0\0\0G\0\0\0~', 0.00, 7, '2019-11-12 06:46:31'),
(10772, '', 0.00, 6, '2019-11-12 06:46:31'),
(10773, '', 0.00, 7, '2019-11-12 06:46:32'),
(10774, '', 0.00, 6, '2019-11-12 06:46:32'),
(10775, '\0G\0\0\0\0H\0\0\0~', 0.00, 7, '2019-11-12 06:46:32'),
(10776, '', 0.00, 6, '2019-11-12 06:46:32'),
(10777, '', 0.00, 7, '2019-11-12 06:46:32'),
(10778, '', 0.00, 6, '2019-11-12 06:46:32'),
(10779, '', 0.00, 7, '2019-11-12 06:46:32'),
(10780, '', 0.00, 6, '2019-11-12 06:46:32'),
(10781, '\0I\0\0\0\0J\0\0\0~', 0.00, 7, '2019-11-12 06:46:32'),
(10782, '', 0.00, 6, '2019-11-12 06:46:32'),
(10783, '', 0.00, 7, '2019-11-12 06:46:33'),
(10784, '', 0.00, 6, '2019-11-12 06:46:33'),
(10785, '\0J\0\0\0\0K\0\0\0~', 0.00, 7, '2019-11-12 06:46:33'),
(10786, '', 0.00, 6, '2019-11-12 06:46:33'),
(10787, '', 0.00, 7, '2019-11-12 06:46:33'),
(10788, '', 0.00, 6, '2019-11-12 06:46:33'),
(10789, '\0K\0\0\0\0L\0\0\0~', 0.00, 7, '2019-11-12 06:46:33'),
(10790, '', 0.00, 6, '2019-11-12 06:46:33'),
(10791, '', 0.00, 7, '2019-11-12 06:46:33'),
(10792, '', 0.00, 6, '2019-11-12 06:46:33'),
(10793, '\0L\0\0\0\0M\0\0\0~', 0.00, 7, '2019-11-12 06:46:33'),
(10794, '', 0.00, 6, '2019-11-12 06:46:33'),
(10795, '', 0.00, 7, '2019-11-12 06:46:33'),
(10796, '', 0.00, 6, '2019-11-12 06:46:33'),
(10797, '\0M\0\0\0\0N\0\0\0~', 0.00, 7, '2019-11-12 06:46:33'),
(10798, '', 0.00, 6, '2019-11-12 06:46:33'),
(10799, '', 0.00, 7, '2019-11-12 06:46:33'),
(10800, '', 0.00, 6, '2019-11-12 06:46:34'),
(10801, '\0N\0\0\0\0O\0\0\0~', 0.00, 7, '2019-11-12 06:46:34'),
(10802, '', 0.00, 6, '2019-11-12 06:46:34'),
(10803, '', 0.00, 7, '2019-11-12 06:46:34'),
(10804, '', 0.00, 6, '2019-11-12 06:46:34'),
(10805, '\0O\0\0\0\0P\0\0\0~', 0.00, 7, '2019-11-12 06:46:34'),
(10806, '', 0.00, 6, '2019-11-12 06:46:34'),
(10807, '', 0.00, 7, '2019-11-12 06:46:34'),
(10808, '', 0.00, 6, '2019-11-12 06:46:34'),
(10809, '\0P\0\0\0\0Q\0\0\0~', 0.00, 7, '2019-11-12 06:46:34'),
(10810, '', 0.00, 6, '2019-11-12 06:46:34'),
(10811, '', 0.00, 7, '2019-11-12 06:46:34'),
(10812, '', 0.00, 6, '2019-11-12 06:46:34'),
(10813, '\0Q\0\0\0\0R\0\0\0~', 0.00, 7, '2019-11-12 06:46:34'),
(10814, '', 0.00, 6, '2019-11-12 06:46:34'),
(10815, '', 0.00, 7, '2019-11-12 06:46:34'),
(10816, '', 0.00, 6, '2019-11-12 06:46:34'),
(10817, '\0R\0\0\0\0S\0\0\0~', 0.00, 7, '2019-11-12 06:46:34'),
(10818, '', 0.00, 6, '2019-11-12 06:46:35'),
(10819, '', 0.00, 7, '2019-11-12 06:46:35'),
(10820, '', 0.00, 6, '2019-11-12 06:46:35'),
(10821, '\0S\0\0\0\0T\0\0\0~', 0.00, 7, '2019-11-12 06:46:35'),
(10822, '', 0.00, 6, '2019-11-12 06:46:35'),
(10823, '', 0.00, 7, '2019-11-12 06:46:35'),
(10824, '', 0.00, 6, '2019-11-12 06:46:35'),
(10825, '\0T\0\0\0\0U\0\0\0~', 0.00, 7, '2019-11-12 06:46:35'),
(10826, '', 0.00, 6, '2019-11-12 06:46:35'),
(10827, '', 0.00, 7, '2019-11-12 06:46:35'),
(10828, '', 0.00, 6, '2019-11-12 06:46:35'),
(10829, '\0U\0\0\0\0V\0\0\0~', 0.00, 7, '2019-11-12 06:46:35'),
(10830, '', 0.00, 6, '2019-11-12 06:46:35'),
(10831, '', 0.00, 7, '2019-11-12 06:46:35'),
(10832, '', 0.00, 6, '2019-11-12 06:46:35'),
(10833, '\0V\0\0\0\0W\0\0\0~', 0.00, 7, '2019-11-12 06:46:35'),
(10834, '', 0.00, 6, '2019-11-12 06:46:35'),
(10835, '', 0.00, 7, '2019-11-12 06:46:35'),
(10836, '', 0.00, 6, '2019-11-12 06:46:35'),
(10837, '\0W\0\0\0\0X\0\0\0~', 0.00, 7, '2019-11-12 06:46:35'),
(10838, '', 0.00, 6, '2019-11-12 06:46:35'),
(10839, '', 0.00, 7, '2019-11-12 06:46:35'),
(10840, '', 0.00, 6, '2019-11-12 06:46:35'),
(10841, '\0X\0\0\0\0Y\0\0\0~', 0.00, 7, '2019-11-12 06:46:36'),
(10842, '', 0.00, 6, '2019-11-12 06:46:36'),
(10843, '', 0.00, 7, '2019-11-12 06:46:36'),
(10844, '', 0.00, 6, '2019-11-12 06:46:36'),
(10845, '\0Y\0\0\0\0Z\0\0\0~', 0.00, 7, '2019-11-12 06:46:36'),
(10846, '', 0.00, 6, '2019-11-12 06:46:36'),
(10847, '', 0.00, 7, '2019-11-12 06:46:36'),
(10848, '', 0.00, 6, '2019-11-12 06:46:36'),
(10849, '\0Z\0\0\0\0[\0\0\0~', 0.00, 7, '2019-11-12 06:46:36'),
(10850, '', 0.00, 6, '2019-11-12 06:46:36'),
(10851, '', 0.00, 7, '2019-11-12 06:46:36'),
(10852, '', 0.00, 6, '2019-11-12 06:46:36'),
(10853, '\0[\0\0\0\0\\\0\0\0~', 0.00, 7, '2019-11-12 06:46:36'),
(10854, '', 0.00, 6, '2019-11-12 06:46:36'),
(10855, '', 0.00, 7, '2019-11-12 06:46:36'),
(10856, '', 0.00, 6, '2019-11-12 06:46:36'),
(10857, '\0\\\0\0\0\0]\0\0\0~', 0.00, 7, '2019-11-12 06:46:36'),
(10858, '', 0.00, 6, '2019-11-12 06:46:36'),
(10859, '', 0.00, 7, '2019-11-12 06:46:36'),
(10860, '', 0.00, 6, '2019-11-12 06:46:36'),
(10861, '\0]\0\0\0\0^\0\0\0~', 0.00, 7, '2019-11-12 06:46:36'),
(10862, '', 0.00, 6, '2019-11-12 06:46:37'),
(10863, '', 0.00, 7, '2019-11-12 06:46:37'),
(10864, '', 0.00, 6, '2019-11-12 06:46:37'),
(10865, '\0^\0\0\0\0_\0\0\0~', 0.00, 7, '2019-11-12 06:46:37'),
(10866, '', 0.00, 6, '2019-11-12 06:46:37'),
(10867, '', 0.00, 7, '2019-11-12 06:46:37'),
(10868, '', 0.00, 6, '2019-11-12 06:46:37'),
(10869, '\0_\0\0\0\0`\0\0\0~', 0.00, 7, '2019-11-12 06:46:37'),
(10870, '', 0.00, 6, '2019-11-12 06:46:37'),
(10871, '\0_\0\0\0j\r\0\0\0`\0\0\0\0', 0.00, 7, '2019-11-12 06:46:37'),
(10872, '', 0.00, 6, '2019-11-12 06:46:37'),
(10873, '\0`\0\0\0\0a\0\0\0~', 0.00, 7, '2019-11-12 06:46:37'),
(10874, '', 0.00, 6, '2019-11-12 06:46:37'),
(10875, '', 0.00, 7, '2019-11-12 06:46:37'),
(10876, '', 0.00, 6, '2019-11-12 06:46:37'),
(10877, '\0a\0\0\0\0b\0\0\0~', 0.00, 7, '2019-11-12 06:46:37'),
(10878, '', 0.00, 6, '2019-11-12 06:46:38'),
(10879, '\0a\0\0\0S', 0.00, 7, '2019-11-12 06:46:38'),
(10880, '', 0.00, 6, '2019-11-12 06:46:38'),
(10881, '', 0.00, 7, '2019-11-12 06:46:38'),
(10882, '', 0.00, 6, '2019-11-12 06:46:38'),
(10883, '\0b\0\0\0\0c\0\0\0~', 0.00, 7, '2019-11-12 06:46:38'),
(10884, '', 0.00, 6, '2019-11-12 06:46:38'),
(10885, '', 0.00, 7, '2019-11-12 06:46:38'),
(10886, '', 0.00, 6, '2019-11-12 06:46:39'),
(10887, '\0c\0\0\0\0d\0\0\0~', 0.00, 7, '2019-11-12 06:46:39'),
(10888, '', 0.00, 7, '2019-11-12 06:46:39'),
(10889, '', 0.00, 6, '2019-11-12 06:46:39'),
(10890, '', 0.00, 7, '2019-11-12 06:46:39'),
(10891, '', 0.00, 6, '2019-11-12 06:46:39'),
(10892, '\0e\0\0\0\0f\0\0\0~', 0.00, 7, '2019-11-12 06:46:39'),
(10893, '', 0.00, 6, '2019-11-12 06:46:40'),
(10894, '', 0.00, 7, '2019-11-12 06:46:40'),
(10895, '', 0.00, 6, '2019-11-12 06:46:40'),
(10896, '\0f\0\0\0\0g\0\0\0~', 0.00, 7, '2019-11-12 06:46:40'),
(10897, '', 0.00, 6, '2019-11-12 06:46:40'),
(10898, '', 0.00, 7, '2019-11-12 06:46:40'),
(10899, '', 0.00, 6, '2019-11-12 06:46:40'),
(10900, '\0g\0\0\0\0h\0\0\0~', 0.00, 7, '2019-11-12 06:46:40'),
(10901, '', 0.00, 6, '2019-11-12 06:46:40'),
(10902, '', 0.00, 7, '2019-11-12 06:46:40'),
(10903, '', 0.00, 6, '2019-11-12 06:46:40'),
(10904, '\0h\0\0\0\0i\0\0\0~', 0.00, 7, '2019-11-12 06:46:40'),
(10905, '', 0.00, 6, '2019-11-12 06:46:40'),
(10906, '', 0.00, 7, '2019-11-12 06:46:40'),
(10907, '', 0.00, 6, '2019-11-12 06:46:40'),
(10908, '\0i\0\0\0\0j\0\0\0~', 0.00, 7, '2019-11-12 06:46:40'),
(10909, '', 0.00, 6, '2019-11-12 06:46:40'),
(10910, '', 0.00, 7, '2019-11-12 06:46:40'),
(10911, '', 0.00, 6, '2019-11-12 06:46:40'),
(10912, '\0j\0\0\0\0k\0\0\0~', 0.00, 7, '2019-11-12 06:46:40'),
(10913, '', 0.00, 6, '2019-11-12 06:46:40'),
(10914, '', 0.00, 7, '2019-11-12 06:46:40'),
(10915, '', 0.00, 6, '2019-11-12 06:46:40'),
(10916, '\0k\0\0\0\0l\0\0\0~', 0.00, 7, '2019-11-12 06:46:40'),
(10917, '', 0.00, 6, '2019-11-12 06:46:40'),
(10918, '', 0.00, 7, '2019-11-12 06:46:40'),
(10919, '', 0.00, 6, '2019-11-12 06:46:40'),
(10920, '\0l\0\0\0\0m\0\0\0~', 0.00, 7, '2019-11-12 06:46:41'),
(10921, '', 0.00, 6, '2019-11-12 06:46:41'),
(10922, '', 0.00, 7, '2019-11-12 06:46:41'),
(10923, '', 0.00, 6, '2019-11-12 06:46:41'),
(10924, '\0m\0\0\0\0n\0\0\0~', 0.00, 7, '2019-11-12 06:46:41'),
(10925, '', 0.00, 6, '2019-11-12 06:46:41'),
(10926, '', 0.00, 7, '2019-11-12 06:46:41'),
(10927, '', 0.00, 6, '2019-11-12 06:46:41'),
(10928, '', 0.00, 7, '2019-11-12 06:46:41'),
(10929, '', 0.00, 6, '2019-11-12 06:46:41'),
(10930, '\0o\0\0\0\0p\0\0\0~', 0.00, 7, '2019-11-12 06:46:41'),
(10931, '', 0.00, 6, '2019-11-12 06:46:41'),
(10932, '', 0.00, 7, '2019-11-12 06:46:41'),
(10933, '', 0.00, 6, '2019-11-12 06:46:41'),
(10934, '\0p\0\0\0\0q\0\0\0~', 0.00, 7, '2019-11-12 06:46:41'),
(10935, '', 0.00, 6, '2019-11-12 06:46:41'),
(10936, '', 0.00, 7, '2019-11-12 06:46:42'),
(10937, '', 0.00, 6, '2019-11-12 06:46:42'),
(10938, '\0q\0\0\0\0r\0\0\0~', 0.00, 7, '2019-11-12 06:46:42'),
(10939, '', 0.00, 6, '2019-11-12 06:46:42'),
(10940, '\0q\0\0\0', 0.00, 7, '2019-11-12 06:46:42'),
(10941, '', 0.00, 6, '2019-11-12 06:46:42'),
(10942, '', 0.00, 7, '2019-11-12 06:46:42'),
(10943, '', 0.00, 6, '2019-11-12 06:46:42'),
(10944, '\0r\0\0\0\0s\0\0\0~', 0.00, 7, '2019-11-12 06:46:42'),
(10945, '', 0.00, 6, '2019-11-12 06:46:42'),
(10946, '', 0.00, 7, '2019-11-12 06:46:42'),
(10947, '', 0.00, 6, '2019-11-12 06:46:42'),
(10948, '\0s\0\0\0\0t\0\0\0~', 0.00, 7, '2019-11-12 06:46:42'),
(10949, '', 0.00, 6, '2019-11-12 06:46:42'),
(10950, '', 0.00, 7, '2019-11-12 06:46:43'),
(10951, '', 0.00, 6, '2019-11-12 06:46:43'),
(10952, '\0t\0\0\0\0u\0\0\0~', 0.00, 7, '2019-11-12 06:46:43'),
(10953, '', 0.00, 6, '2019-11-12 06:46:43'),
(10954, '', 0.00, 7, '2019-11-12 06:46:43'),
(10955, '', 0.00, 6, '2019-11-12 06:46:43'),
(10956, '\0u\0\0\0\0v\0\0\0~', 0.00, 7, '2019-11-12 06:46:43'),
(10957, '', 0.00, 6, '2019-11-12 06:46:44'),
(10958, '', 0.00, 7, '2019-11-12 06:46:44'),
(10959, '', 0.00, 6, '2019-11-12 06:46:44'),
(10960, '\0v\0\0\0\0w\0\0\0~', 0.00, 7, '2019-11-12 06:46:44'),
(10961, '', 0.00, 6, '2019-11-12 06:46:44'),
(10962, '', 0.00, 7, '2019-11-12 06:46:44'),
(10963, '', 0.00, 6, '2019-11-12 06:46:44'),
(10964, '\0w\0\0\0\0x\0\0\0~', 0.00, 7, '2019-11-12 06:46:44'),
(10965, '', 0.00, 6, '2019-11-12 06:46:44'),
(10966, '', 0.00, 7, '2019-11-12 06:46:44'),
(10967, '', 0.00, 6, '2019-11-12 06:46:44'),
(10968, '\0x\0\0\0\0y\0\0\0~', 0.00, 7, '2019-11-12 06:46:44'),
(10969, '', 0.00, 6, '2019-11-12 06:46:44'),
(10970, '', 0.00, 7, '2019-11-12 06:46:44'),
(10971, '', 0.00, 6, '2019-11-12 06:46:44'),
(10972, '\0y\0\0\0\0z\0\0\0~', 0.00, 7, '2019-11-12 06:46:44'),
(10973, '', 0.00, 6, '2019-11-12 06:46:44'),
(10974, '', 0.00, 7, '2019-11-12 06:46:44'),
(10975, '', 0.00, 6, '2019-11-12 06:46:44'),
(10976, '', 0.00, 7, '2019-11-12 06:46:44'),
(10977, '', 0.00, 6, '2019-11-12 06:46:44'),
(10978, '', 0.00, 7, '2019-11-12 06:46:45'),
(10979, '', 0.00, 6, '2019-11-12 06:46:45'),
(10980, '', 0.00, 7, '2019-11-12 06:46:45'),
(10981, '', 0.00, 6, '2019-11-12 06:46:45'),
(10982, '\0}\0\0\0\0~\0\0\0~', 0.00, 7, '2019-11-12 06:46:45'),
(10983, '', 0.00, 6, '2019-11-12 06:46:45'),
(10984, '', 0.00, 7, '2019-11-12 06:46:45'),
(10985, '', 0.00, 6, '2019-11-12 06:46:45'),
(10986, '', 0.00, 7, '2019-11-12 06:46:45'),
(10987, '', 0.00, 6, '2019-11-12 06:46:45'),
(10988, '', 0.00, 7, '2019-11-12 06:46:45'),
(10989, '', 0.00, 6, '2019-11-12 06:46:46'),
(10990, '', 0.00, 7, '2019-11-12 06:46:46'),
(10991, '', 0.00, 6, '2019-11-12 06:46:46'),
(10992, '', 0.00, 7, '2019-11-12 06:46:46'),
(10993, '', 0.00, 6, '2019-11-12 06:46:46'),
(10994, '', 0.00, 7, '2019-11-12 06:46:46'),
(10995, '', 0.00, 6, '2019-11-12 06:46:46'),
(10996, '', 0.00, 7, '2019-11-12 06:46:46'),
(10997, '', 0.00, 6, '2019-11-12 06:46:46'),
(10998, '', 0.00, 7, '2019-11-12 06:46:46'),
(10999, '', 0.00, 6, '2019-11-12 06:46:47'),
(11000, '', 0.00, 7, '2019-11-12 06:46:47'),
(11001, '', 0.00, 6, '2019-11-12 06:46:47'),
(11002, '', 0.00, 7, '2019-11-12 06:46:47'),
(11003, '', 0.00, 6, '2019-11-12 06:46:47'),
(11004, '', 0.00, 7, '2019-11-12 06:46:47'),
(11005, '', 0.00, 6, '2019-11-12 06:46:47'),
(11006, '', 0.00, 7, '2019-11-12 06:46:47'),
(11007, '', 0.00, 6, '2019-11-12 06:46:47'),
(11008, '', 0.00, 7, '2019-11-12 06:46:47'),
(11009, '', 0.00, 6, '2019-11-12 06:46:47'),
(11010, '', 0.00, 7, '2019-11-12 06:46:47'),
(11011, '', 0.00, 6, '2019-11-12 06:46:47'),
(11012, '', 0.00, 7, '2019-11-12 06:46:47'),
(11013, '', 0.00, 6, '2019-11-12 06:46:47'),
(11014, '', 0.00, 7, '2019-11-12 06:46:47'),
(11015, '', 0.00, 6, '2019-11-12 06:46:47'),
(11016, '', 0.00, 7, '2019-11-12 06:46:47'),
(11017, '', 0.00, 6, '2019-11-12 06:46:47'),
(11018, '', 0.00, 7, '2019-11-12 06:46:47'),
(11019, '', 0.00, 6, '2019-11-12 06:46:47'),
(11020, '', 0.00, 7, '2019-11-12 06:46:47'),
(11021, '', 0.00, 6, '2019-11-12 06:46:47'),
(11022, '', 0.00, 7, '2019-11-12 06:46:47'),
(11023, '', 0.00, 6, '2019-11-12 06:46:47'),
(11024, '', 0.00, 7, '2019-11-12 06:46:47'),
(11025, '', 0.00, 6, '2019-11-12 06:46:47'),
(11026, '', 0.00, 7, '2019-11-12 06:46:48'),
(11027, '', 0.00, 7, '2019-11-12 06:46:48'),
(11028, '', 0.00, 6, '2019-11-12 06:46:48'),
(11029, '', 0.00, 7, '2019-11-12 06:46:48'),
(11030, '', 0.00, 6, '2019-11-12 06:46:48'),
(11031, '', 0.00, 7, '2019-11-12 06:46:48'),
(11032, '', 0.00, 6, '2019-11-12 06:46:48'),
(11033, '', 0.00, 7, '2019-11-12 06:46:48'),
(11034, '', 0.00, 6, '2019-11-12 06:46:48'),
(11035, '', 0.00, 7, '2019-11-12 06:46:48'),
(11036, '', 0.00, 6, '2019-11-12 06:46:48'),
(11037, '', 0.00, 7, '2019-11-12 06:46:48'),
(11038, '', 0.00, 6, '2019-11-12 06:46:48'),
(11039, '', 0.00, 7, '2019-11-12 06:46:49'),
(11040, '', 0.00, 6, '2019-11-12 06:46:49'),
(11041, '', 0.00, 7, '2019-11-12 06:46:49'),
(11042, '', 0.00, 6, '2019-11-12 06:46:49'),
(11043, '', 0.00, 7, '2019-11-12 06:46:49'),
(11044, '', 0.00, 6, '2019-11-12 06:46:49'),
(11045, '', 0.00, 7, '2019-11-12 06:46:49'),
(11046, '', 0.00, 6, '2019-11-12 06:46:49'),
(11047, '', 0.00, 7, '2019-11-12 06:46:49'),
(11048, '', 0.00, 6, '2019-11-12 06:46:49'),
(11049, '', 0.00, 7, '2019-11-12 06:46:49'),
(11050, '', 0.00, 6, '2019-11-12 06:46:49'),
(11051, '', 0.00, 7, '2019-11-12 06:46:49'),
(11052, '', 0.00, 6, '2019-11-12 06:46:49'),
(11053, '', 0.00, 7, '2019-11-12 06:46:50'),
(11054, '', 0.00, 6, '2019-11-12 06:46:50'),
(11055, '', 0.00, 7, '2019-11-12 06:46:50'),
(11056, '', 0.00, 6, '2019-11-12 06:46:50'),
(11057, '', 0.00, 7, '2019-11-12 06:46:50'),
(11058, '', 0.00, 6, '2019-11-12 06:46:51'),
(11059, '', 0.00, 7, '2019-11-12 06:46:51'),
(11060, '', 0.00, 6, '2019-11-12 06:46:51'),
(11061, '', 0.00, 7, '2019-11-12 06:46:51'),
(11062, '', 0.00, 6, '2019-11-12 06:46:51'),
(11063, '', 0.00, 7, '2019-11-12 06:46:51'),
(11064, '', 0.00, 6, '2019-11-12 06:46:51'),
(11065, '', 0.00, 7, '2019-11-12 06:46:51'),
(11066, '', 0.00, 6, '2019-11-12 06:46:52'),
(11067, '', 0.00, 7, '2019-11-12 06:46:52'),
(11068, '', 0.00, 6, '2019-11-12 06:46:52'),
(11069, '', 0.00, 7, '2019-11-12 06:46:52'),
(11070, '', 0.00, 6, '2019-11-12 06:46:52'),
(11071, '', 0.00, 7, '2019-11-12 06:46:52'),
(11072, '', 0.00, 6, '2019-11-12 06:46:52'),
(11073, '', 0.00, 7, '2019-11-12 06:46:52'),
(11074, '', 0.00, 6, '2019-11-12 06:46:52'),
(11075, '', 0.00, 7, '2019-11-12 06:46:52'),
(11076, '', 0.00, 6, '2019-11-12 06:46:52'),
(11077, '', 0.00, 7, '2019-11-12 06:46:52'),
(11078, '', 0.00, 6, '2019-11-12 06:46:52'),
(11079, '', 0.00, 7, '2019-11-12 06:46:52'),
(11080, '', 0.00, 6, '2019-11-12 06:46:52'),
(11081, '', 0.00, 7, '2019-11-12 06:46:52'),
(11082, '', 0.00, 6, '2019-11-12 06:46:52'),
(11083, '', 0.00, 7, '2019-11-12 06:46:52'),
(11084, '', 0.00, 6, '2019-11-12 06:46:53'),
(11085, '', 0.00, 7, '2019-11-12 06:46:53'),
(11086, '', 0.00, 6, '2019-11-12 06:46:53'),
(11087, '', 0.00, 7, '2019-11-12 06:46:53'),
(11088, '', 0.00, 6, '2019-11-12 06:46:53'),
(11089, '', 0.00, 7, '2019-11-12 06:46:53'),
(11090, '', 0.00, 6, '2019-11-12 06:46:53'),
(11091, '', 0.00, 7, '2019-11-12 06:46:54'),
(11092, '', 0.00, 6, '2019-11-12 06:46:54'),
(11093, '', 0.00, 7, '2019-11-12 06:46:54'),
(11094, '', 0.00, 6, '2019-11-12 06:46:54'),
(11095, '', 0.00, 7, '2019-11-12 06:46:54'),
(11096, '', 0.00, 6, '2019-11-12 06:46:54'),
(11097, '', 0.00, 7, '2019-11-12 06:46:54'),
(11098, '', 0.00, 6, '2019-11-12 06:46:54'),
(11099, '', 0.00, 7, '2019-11-12 06:46:54'),
(11100, '', 0.00, 6, '2019-11-12 06:46:54'),
(11101, '', 0.00, 7, '2019-11-12 06:46:54'),
(11102, '', 0.00, 6, '2019-11-12 06:46:54'),
(11103, '', 0.00, 7, '2019-11-12 06:46:54'),
(11104, '', 0.00, 6, '2019-11-12 06:46:54'),
(11105, '', 0.00, 7, '2019-11-12 06:46:54'),
(11106, '', 0.00, 6, '2019-11-12 06:46:54'),
(11107, '', 0.00, 7, '2019-11-12 06:46:54'),
(11108, '', 0.00, 6, '2019-11-12 06:46:54'),
(11109, '', 0.00, 7, '2019-11-12 06:46:54'),
(11110, '', 0.00, 6, '2019-11-12 06:46:54'),
(11111, '', 0.00, 7, '2019-11-12 06:46:54'),
(11112, '', 0.00, 6, '2019-11-12 06:46:54'),
(11113, '', 0.00, 7, '2019-11-12 06:46:55'),
(11114, '', 0.00, 6, '2019-11-12 06:46:55'),
(11115, '', 0.00, 7, '2019-11-12 06:46:55'),
(11116, '', 0.00, 6, '2019-11-12 06:46:55'),
(11117, '', 0.00, 7, '2019-11-12 06:46:55'),
(11118, '', 0.00, 6, '2019-11-12 06:46:55'),
(11119, '', 0.00, 7, '2019-11-12 06:46:55'),
(11120, '', 0.00, 6, '2019-11-12 06:46:55'),
(11121, '', 0.00, 7, '2019-11-12 06:46:55'),
(11122, '', 0.00, 6, '2019-11-12 06:46:55'),
(11123, '', 0.00, 7, '2019-11-12 06:46:55'),
(11124, '', 0.00, 6, '2019-11-12 06:46:55'),
(11125, '', 0.00, 7, '2019-11-12 06:46:55'),
(11126, '', 0.00, 6, '2019-11-12 06:46:55'),
(11127, '', 0.00, 7, '2019-11-12 06:46:55'),
(11128, '', 0.00, 6, '2019-11-12 06:46:55'),
(11129, '', 0.00, 7, '2019-11-12 06:46:55'),
(11130, '', 0.00, 6, '2019-11-12 06:46:55'),
(11131, '', 0.00, 7, '2019-11-12 06:46:55'),
(11132, '', 0.00, 6, '2019-11-12 06:46:55'),
(11133, '', 0.00, 7, '2019-11-12 06:46:56'),
(11134, '', 0.00, 6, '2019-11-12 06:46:56'),
(11135, '', 0.00, 7, '2019-11-12 06:46:56'),
(11136, '', 0.00, 6, '2019-11-12 06:46:56'),
(11137, '', 0.00, 7, '2019-11-12 06:46:56'),
(11138, '', 0.00, 6, '2019-11-12 06:46:56'),
(11139, '', 0.00, 7, '2019-11-12 06:46:56'),
(11140, '', 0.00, 6, '2019-11-12 06:46:56'),
(11141, '', 0.00, 7, '2019-11-12 06:46:56'),
(11142, '', 0.00, 6, '2019-11-12 06:46:56'),
(11143, '', 0.00, 7, '2019-11-12 06:46:56'),
(11144, '', 0.00, 6, '2019-11-12 06:46:56'),
(11145, '', 0.00, 7, '2019-11-12 06:46:56'),
(11146, '', 0.00, 6, '2019-11-12 06:46:57'),
(11147, '', 0.00, 7, '2019-11-12 06:46:57'),
(11148, '', 0.00, 6, '2019-11-12 06:46:57'),
(11149, '', 0.00, 7, '2019-11-12 06:46:57'),
(11150, '', 0.00, 6, '2019-11-12 06:46:57'),
(11151, '', 0.00, 7, '2019-11-12 06:46:57'),
(11152, '', 0.00, 6, '2019-11-12 06:46:57'),
(11153, '', 0.00, 7, '2019-11-12 06:46:57'),
(11154, '', 0.00, 6, '2019-11-12 06:46:57'),
(11155, '', 0.00, 7, '2019-11-12 06:46:57'),
(11156, '', 0.00, 6, '2019-11-12 06:46:57'),
(11157, '', 0.00, 7, '2019-11-12 06:46:57'),
(11158, '', 0.00, 6, '2019-11-12 06:46:58'),
(11159, '', 0.00, 7, '2019-11-12 06:46:58'),
(11160, '\0\0\0', 0.00, 6, '2019-11-12 06:46:58'),
(11161, '', 0.00, 7, '2019-11-12 06:46:58'),
(11162, '\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:46:58'),
(11163, '', 0.00, 7, '2019-11-12 06:46:58'),
(11164, '\0\0\0\0S', 0.00, 6, '2019-11-12 06:46:58'),
(11165, '', 0.00, 7, '2019-11-12 06:46:58'),
(11166, '', 0.00, 6, '2019-11-12 06:46:58'),
(11167, '', 0.00, 7, '2019-11-12 06:46:58'),
(11168, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:46:58'),
(11169, '', 0.00, 7, '2019-11-12 06:46:58'),
(11170, '', 0.00, 6, '2019-11-12 06:46:59'),
(11171, '', 0.00, 7, '2019-11-12 06:46:59'),
(11172, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:46:59'),
(11173, '', 0.00, 7, '2019-11-12 06:46:59'),
(11174, '', 0.00, 6, '2019-11-12 06:46:59'),
(11175, '', 0.00, 7, '2019-11-12 06:46:59'),
(11176, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:46:59'),
(11177, '', 0.00, 7, '2019-11-12 06:46:59'),
(11178, '', 0.00, 6, '2019-11-12 06:46:59'),
(11179, '', 0.00, 7, '2019-11-12 06:46:59'),
(11180, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:00'),
(11181, '', 0.00, 7, '2019-11-12 06:47:00'),
(11182, '', 0.00, 6, '2019-11-12 06:47:00'),
(11183, '', 0.00, 7, '2019-11-12 06:47:00'),
(11184, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:00'),
(11185, '', 0.00, 7, '2019-11-12 06:47:00'),
(11186, '', 0.00, 6, '2019-11-12 06:47:00'),
(11187, '', 0.00, 7, '2019-11-12 06:47:00'),
(11188, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:00'),
(11189, '', 0.00, 7, '2019-11-12 06:47:00'),
(11190, '\0\0\0R', 0.00, 6, '2019-11-12 06:47:00'),
(11191, '', 0.00, 7, '2019-11-12 06:47:00'),
(11192, '', 0.00, 6, '2019-11-12 06:47:00'),
(11193, '', 0.00, 7, '2019-11-12 06:47:00'),
(11194, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:00'),
(11195, '', 0.00, 7, '2019-11-12 06:47:00'),
(11196, '\0\0\0R', 0.00, 6, '2019-11-12 06:47:00'),
(11197, '', 0.00, 7, '2019-11-12 06:47:00'),
(11198, '', 0.00, 6, '2019-11-12 06:47:00'),
(11199, '', 0.00, 7, '2019-11-12 06:47:00'),
(11200, '\0\0\0\0	\0\0~', 0.00, 6, '2019-11-12 06:47:00'),
(11201, '', 0.00, 7, '2019-11-12 06:47:00'),
(11202, '', 0.00, 6, '2019-11-12 06:47:01'),
(11203, '', 0.00, 7, '2019-11-12 06:47:01'),
(11204, '\0	\0\0\0', 0.00, 6, '2019-11-12 06:47:01'),
(11205, '', 0.00, 7, '2019-11-12 06:47:01'),
(11206, '\0\0~', 0.00, 6, '2019-11-12 06:47:01'),
(11207, '', 0.00, 7, '2019-11-12 06:47:01'),
(11208, '', 0.00, 6, '2019-11-12 06:47:01'),
(11209, '', 0.00, 7, '2019-11-12 06:47:01'),
(11210, '\0', 0.00, 6, '2019-11-12 06:47:01'),
(11211, '', 0.00, 7, '2019-11-12 06:47:01'),
(11212, '\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:01'),
(11213, '', 0.00, 7, '2019-11-12 06:47:02'),
(11214, '\0', 0.00, 6, '2019-11-12 06:47:02'),
(11215, '', 0.00, 7, '2019-11-12 06:47:02'),
(11216, '', 0.00, 6, '2019-11-12 06:47:02'),
(11217, '', 0.00, 7, '2019-11-12 06:47:02'),
(11218, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:02'),
(11219, '', 0.00, 7, '2019-11-12 06:47:02'),
(11220, '', 0.00, 6, '2019-11-12 06:47:02'),
(11221, '', 0.00, 7, '2019-11-12 06:47:03'),
(11222, '\0\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:47:03'),
(11223, '', 0.00, 7, '2019-11-12 06:47:03'),
(11224, '', 0.00, 6, '2019-11-12 06:47:03'),
(11225, '', 0.00, 7, '2019-11-12 06:47:03'),
(11226, '\0\r\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:03'),
(11227, '', 0.00, 7, '2019-11-12 06:47:03'),
(11228, '', 0.00, 6, '2019-11-12 06:47:03'),
(11229, '', 0.00, 7, '2019-11-12 06:47:03'),
(11230, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:03'),
(11231, '', 0.00, 7, '2019-11-12 06:47:03'),
(11232, '', 0.00, 6, '2019-11-12 06:47:03'),
(11233, '', 0.00, 7, '2019-11-12 06:47:03'),
(11234, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:03'),
(11235, '', 0.00, 7, '2019-11-12 06:47:03'),
(11236, '', 0.00, 6, '2019-11-12 06:47:03'),
(11237, '', 0.00, 7, '2019-11-12 06:47:03'),
(11238, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:03'),
(11239, '', 0.00, 7, '2019-11-12 06:47:04'),
(11240, '', 0.00, 6, '2019-11-12 06:47:04'),
(11241, '', 0.00, 7, '2019-11-12 06:47:04'),
(11242, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:04'),
(11243, '', 0.00, 7, '2019-11-12 06:47:04'),
(11244, '', 0.00, 6, '2019-11-12 06:47:04'),
(11245, '', 0.00, 7, '2019-11-12 06:47:04'),
(11246, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:04'),
(11247, '', 0.00, 7, '2019-11-12 06:47:04'),
(11248, '', 0.00, 6, '2019-11-12 06:47:04'),
(11249, '', 0.00, 7, '2019-11-12 06:47:04'),
(11250, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:04'),
(11251, '', 0.00, 7, '2019-11-12 06:47:04'),
(11252, '', 0.00, 6, '2019-11-12 06:47:04'),
(11253, '', 0.00, 7, '2019-11-12 06:47:04'),
(11254, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:04'),
(11255, '', 0.00, 7, '2019-11-12 06:47:04'),
(11256, '', 0.00, 6, '2019-11-12 06:47:04'),
(11257, '', 0.00, 7, '2019-11-12 06:47:04'),
(11258, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:04'),
(11259, '', 0.00, 7, '2019-11-12 06:47:04'),
(11260, '', 0.00, 6, '2019-11-12 06:47:04'),
(11261, '', 0.00, 7, '2019-11-12 06:47:04'),
(11262, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:04'),
(11263, '', 0.00, 7, '2019-11-12 06:47:04'),
(11264, '', 0.00, 6, '2019-11-12 06:47:05'),
(11265, '', 0.00, 7, '2019-11-12 06:47:05'),
(11266, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:05'),
(11267, '', 0.00, 7, '2019-11-12 06:47:05'),
(11268, '', 0.00, 6, '2019-11-12 06:47:05'),
(11269, '', 0.00, 7, '2019-11-12 06:47:05'),
(11270, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:05'),
(11271, '', 0.00, 7, '2019-11-12 06:47:05'),
(11272, '', 0.00, 6, '2019-11-12 06:47:05'),
(11273, '', 0.00, 7, '2019-11-12 06:47:05'),
(11274, '\0\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:47:05'),
(11275, '', 0.00, 7, '2019-11-12 06:47:05'),
(11276, '', 0.00, 6, '2019-11-12 06:47:05'),
(11277, '', 0.00, 7, '2019-11-12 06:47:05'),
(11278, '\0\Z\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:05'),
(11279, '', 0.00, 7, '2019-11-12 06:47:05'),
(11280, '', 0.00, 6, '2019-11-12 06:47:05'),
(11281, '', 0.00, 7, '2019-11-12 06:47:05'),
(11282, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:05'),
(11283, '', 0.00, 7, '2019-11-12 06:47:05'),
(11284, '', 0.00, 6, '2019-11-12 06:47:05'),
(11285, '', 0.00, 7, '2019-11-12 06:47:06'),
(11286, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:06'),
(11287, '', 0.00, 7, '2019-11-12 06:47:06'),
(11288, '', 0.00, 6, '2019-11-12 06:47:06'),
(11289, '', 0.00, 7, '2019-11-12 06:47:06'),
(11290, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:06'),
(11291, '', 0.00, 7, '2019-11-12 06:47:06'),
(11292, '', 0.00, 6, '2019-11-12 06:47:06'),
(11293, '', 0.00, 7, '2019-11-12 06:47:06'),
(11294, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:06'),
(11295, '', 0.00, 7, '2019-11-12 06:47:06'),
(11296, '', 0.00, 6, '2019-11-12 06:47:06'),
(11297, '', 0.00, 7, '2019-11-12 06:47:06'),
(11298, '\0\0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:47:06'),
(11299, '', 0.00, 7, '2019-11-12 06:47:06'),
(11300, '', 0.00, 6, '2019-11-12 06:47:06'),
(11301, '', 0.00, 7, '2019-11-12 06:47:06'),
(11302, '\0 \0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:47:06'),
(11303, '', 0.00, 7, '2019-11-12 06:47:06'),
(11304, '', 0.00, 6, '2019-11-12 06:47:06'),
(11305, '', 0.00, 7, '2019-11-12 06:47:06'),
(11306, '\0!\0\0\0&quot;\0\0~', 0.00, 6, '2019-11-12 06:47:07'),
(11307, '', 0.00, 7, '2019-11-12 06:47:07'),
(11308, '', 0.00, 6, '2019-11-12 06:47:07'),
(11309, '', 0.00, 7, '2019-11-12 06:47:07'),
(11310, '\0&quot;\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:47:07'),
(11311, '', 0.00, 7, '2019-11-12 06:47:07'),
(11312, '', 0.00, 6, '2019-11-12 06:47:07'),
(11313, '', 0.00, 7, '2019-11-12 06:47:07'),
(11314, '\0#\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:47:07'),
(11315, '', 0.00, 7, '2019-11-12 06:47:07'),
(11316, '', 0.00, 6, '2019-11-12 06:47:07'),
(11317, '', 0.00, 7, '2019-11-12 06:47:07'),
(11318, '\0$\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:47:07'),
(11319, '', 0.00, 7, '2019-11-12 06:47:07'),
(11320, '', 0.00, 6, '2019-11-12 06:47:07'),
(11321, '', 0.00, 7, '2019-11-12 06:47:08'),
(11322, '\0%\0\0\0&amp;\0\0~', 0.00, 6, '2019-11-12 06:47:08'),
(11323, '', 0.00, 7, '2019-11-12 06:47:08'),
(11324, '', 0.00, 6, '2019-11-12 06:47:08'),
(11325, '', 0.00, 7, '2019-11-12 06:47:08'),
(11326, '\0&amp;\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:47:08'),
(11327, '', 0.00, 7, '2019-11-12 06:47:08'),
(11328, '', 0.00, 6, '2019-11-12 06:47:08'),
(11329, '', 0.00, 7, '2019-11-12 06:47:08'),
(11330, '\0\'\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:47:08'),
(11331, '', 0.00, 7, '2019-11-12 06:47:08'),
(11332, '\0\'\0\0R', 0.00, 6, '2019-11-12 06:47:08'),
(11333, '', 0.00, 7, '2019-11-12 06:47:08'),
(11334, '', 0.00, 6, '2019-11-12 06:47:08'),
(11335, '', 0.00, 7, '2019-11-12 06:47:08'),
(11336, '\0(\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:47:08'),
(11337, '', 0.00, 7, '2019-11-12 06:47:08'),
(11338, '', 0.00, 6, '2019-11-12 06:47:09'),
(11339, '', 0.00, 7, '2019-11-12 06:47:09'),
(11340, '\0)\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:47:09'),
(11341, '', 0.00, 7, '2019-11-12 06:47:09'),
(11342, '', 0.00, 6, '2019-11-12 06:47:09'),
(11343, '', 0.00, 7, '2019-11-12 06:47:09'),
(11344, '', 0.00, 6, '2019-11-12 06:47:09'),
(11345, '', 0.00, 7, '2019-11-12 06:47:09'),
(11346, '\0+\0\0\0', 0.00, 6, '2019-11-12 06:47:09'),
(11347, '', 0.00, 7, '2019-11-12 06:47:09'),
(11348, '', 0.00, 6, '2019-11-12 06:47:09'),
(11349, '', 0.00, 7, '2019-11-12 06:47:09'),
(11350, '\0', 0.00, 6, '2019-11-12 06:47:09'),
(11351, '', 0.00, 7, '2019-11-12 06:47:09'),
(11352, '\0', 0.00, 6, '2019-11-12 06:47:09'),
(11353, '', 0.00, 7, '2019-11-12 06:47:09'),
(11354, '\0-\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:47:09'),
(11355, '', 0.00, 7, '2019-11-12 06:47:09'),
(11356, '', 0.00, 6, '2019-11-12 06:47:09'),
(11357, '', 0.00, 7, '2019-11-12 06:47:10'),
(11358, '\0.\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:47:10'),
(11359, '', 0.00, 7, '2019-11-12 06:47:10'),
(11360, '', 0.00, 6, '2019-11-12 06:47:11'),
(11361, '', 0.00, 7, '2019-11-12 06:47:11'),
(11362, '\0/\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:47:11'),
(11363, '', 0.00, 7, '2019-11-12 06:47:11'),
(11364, '', 0.00, 6, '2019-11-12 06:47:11'),
(11365, '', 0.00, 7, '2019-11-12 06:47:11'),
(11366, '\00\0\0\01\0\0~', 0.00, 6, '2019-11-12 06:47:11'),
(11367, '', 0.00, 7, '2019-11-12 06:47:11'),
(11368, '', 0.00, 6, '2019-11-12 06:47:11'),
(11369, '', 0.00, 7, '2019-11-12 06:47:11'),
(11370, '\01\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:47:11'),
(11371, '', 0.00, 7, '2019-11-12 06:47:11'),
(11372, '', 0.00, 6, '2019-11-12 06:47:11'),
(11373, '', 0.00, 7, '2019-11-12 06:47:11'),
(11374, '\02\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:47:11'),
(11375, '', 0.00, 7, '2019-11-12 06:47:11'),
(11376, '', 0.00, 6, '2019-11-12 06:47:11'),
(11377, '', 0.00, 7, '2019-11-12 06:47:11'),
(11378, '\03\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:47:11'),
(11379, '', 0.00, 7, '2019-11-12 06:47:11'),
(11380, '', 0.00, 6, '2019-11-12 06:47:11'),
(11381, '', 0.00, 7, '2019-11-12 06:47:11'),
(11382, '\04\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:47:11'),
(11383, '', 0.00, 7, '2019-11-12 06:47:11'),
(11384, '', 0.00, 6, '2019-11-12 06:47:12'),
(11385, '', 0.00, 7, '2019-11-12 06:47:12'),
(11386, '\05\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:47:12'),
(11387, '', 0.00, 7, '2019-11-12 06:47:12'),
(11388, '', 0.00, 6, '2019-11-12 06:47:12'),
(11389, '', 0.00, 7, '2019-11-12 06:47:12'),
(11390, '\06\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:47:12'),
(11391, '', 0.00, 7, '2019-11-12 06:47:12'),
(11392, '', 0.00, 6, '2019-11-12 06:47:12'),
(11393, '', 0.00, 7, '2019-11-12 06:47:12'),
(11394, '\07\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:47:12'),
(11395, '', 0.00, 7, '2019-11-12 06:47:12'),
(11396, '', 0.00, 6, '2019-11-12 06:47:12'),
(11397, '', 0.00, 7, '2019-11-12 06:47:12'),
(11398, '\08\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:47:12'),
(11399, '', 0.00, 7, '2019-11-12 06:47:12'),
(11400, '', 0.00, 6, '2019-11-12 06:47:12'),
(11401, '', 0.00, 7, '2019-11-12 06:47:12'),
(11402, '\09\0\0\0:\0\0~', 0.00, 6, '2019-11-12 06:47:12'),
(11403, '', 0.00, 7, '2019-11-12 06:47:12'),
(11404, '', 0.00, 6, '2019-11-12 06:47:12'),
(11405, '', 0.00, 7, '2019-11-12 06:47:13'),
(11406, '\0:\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:47:13'),
(11407, '', 0.00, 7, '2019-11-12 06:47:13'),
(11408, '', 0.00, 6, '2019-11-12 06:47:13'),
(11409, '', 0.00, 7, '2019-11-12 06:47:13'),
(11410, '\0;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:47:14'),
(11411, '', 0.00, 7, '2019-11-12 06:47:14'),
(11412, '\0;\0\0', 0.00, 6, '2019-11-12 06:47:14'),
(11413, '', 0.00, 7, '2019-11-12 06:47:14'),
(11414, '', 0.00, 6, '2019-11-12 06:47:14'),
(11415, '', 0.00, 7, '2019-11-12 06:47:14'),
(11416, '\0&lt;\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:47:14'),
(11417, '', 0.00, 7, '2019-11-12 06:47:14'),
(11418, '', 0.00, 6, '2019-11-12 06:47:14'),
(11419, '', 0.00, 7, '2019-11-12 06:47:14'),
(11420, '\0=\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:47:14'),
(11421, '', 0.00, 7, '2019-11-12 06:47:14'),
(11422, '', 0.00, 6, '2019-11-12 06:47:15'),
(11423, '', 0.00, 7, '2019-11-12 06:47:15'),
(11424, '\0&gt;\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:47:15'),
(11425, '', 0.00, 7, '2019-11-12 06:47:15'),
(11426, '', 0.00, 6, '2019-11-12 06:47:15'),
(11427, '', 0.00, 7, '2019-11-12 06:47:15'),
(11428, '\0?\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:47:15'),
(11429, '', 0.00, 7, '2019-11-12 06:47:15'),
(11430, '', 0.00, 6, '2019-11-12 06:47:15'),
(11431, '', 0.00, 7, '2019-11-12 06:47:15'),
(11432, '\0@\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:47:15'),
(11433, '', 0.00, 7, '2019-11-12 06:47:15'),
(11434, '', 0.00, 6, '2019-11-12 06:47:15'),
(11435, '', 0.00, 7, '2019-11-12 06:47:15'),
(11436, '\0A\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:47:15'),
(11437, '', 0.00, 7, '2019-11-12 06:47:15'),
(11438, '', 0.00, 6, '2019-11-12 06:47:15'),
(11439, '', 0.00, 7, '2019-11-12 06:47:15'),
(11440, '\0B\0\0\0C\0\0~', 0.00, 6, '2019-11-12 06:47:15'),
(11441, '', 0.00, 7, '2019-11-12 06:47:15'),
(11442, '', 0.00, 6, '2019-11-12 06:47:15'),
(11443, '', 0.00, 7, '2019-11-12 06:47:16'),
(11444, '\0C\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:47:16'),
(11445, '', 0.00, 7, '2019-11-12 06:47:16'),
(11446, '', 0.00, 6, '2019-11-12 06:47:16'),
(11447, '', 0.00, 7, '2019-11-12 06:47:16'),
(11448, '\0D\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:47:16'),
(11449, '', 0.00, 7, '2019-11-12 06:47:16'),
(11450, '', 0.00, 6, '2019-11-12 06:47:16'),
(11451, '', 0.00, 7, '2019-11-12 06:47:16'),
(11452, '\0E\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:47:16'),
(11453, '', 0.00, 7, '2019-11-12 06:47:16'),
(11454, '', 0.00, 6, '2019-11-12 06:47:16'),
(11455, '', 0.00, 7, '2019-11-12 06:47:16'),
(11456, '\0F\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:47:16'),
(11457, '', 0.00, 7, '2019-11-12 06:47:16'),
(11458, '', 0.00, 6, '2019-11-12 06:47:16'),
(11459, '', 0.00, 7, '2019-11-12 06:47:16'),
(11460, '\0G\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:47:16'),
(11461, '', 0.00, 7, '2019-11-12 06:47:16'),
(11462, '', 0.00, 6, '2019-11-12 06:47:16'),
(11463, '', 0.00, 7, '2019-11-12 06:47:17'),
(11464, '\0H\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:47:17'),
(11465, '', 0.00, 7, '2019-11-12 06:47:17'),
(11466, '', 0.00, 6, '2019-11-12 06:47:17'),
(11467, '', 0.00, 7, '2019-11-12 06:47:17'),
(11468, '\0I\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:47:17'),
(11469, '', 0.00, 7, '2019-11-12 06:47:17'),
(11470, '', 0.00, 6, '2019-11-12 06:47:17'),
(11471, '', 0.00, 7, '2019-11-12 06:47:17'),
(11472, '\0J\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:47:17'),
(11473, '', 0.00, 7, '2019-11-12 06:47:17'),
(11474, '', 0.00, 6, '2019-11-12 06:47:17'),
(11475, '', 0.00, 7, '2019-11-12 06:47:17'),
(11476, '\0K\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:47:17'),
(11477, '', 0.00, 7, '2019-11-12 06:47:17'),
(11478, '', 0.00, 6, '2019-11-12 06:47:17'),
(11479, '', 0.00, 7, '2019-11-12 06:47:17'),
(11480, '\0L\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:47:17'),
(11481, '', 0.00, 7, '2019-11-12 06:47:18'),
(11482, '', 0.00, 6, '2019-11-12 06:47:18'),
(11483, '', 0.00, 7, '2019-11-12 06:47:18'),
(11484, '\0M\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:47:18'),
(11485, '', 0.00, 7, '2019-11-12 06:47:18'),
(11486, '', 0.00, 6, '2019-11-12 06:47:18'),
(11487, '', 0.00, 7, '2019-11-12 06:47:18'),
(11488, '', 0.00, 6, '2019-11-12 06:47:18'),
(11489, '', 0.00, 7, '2019-11-12 06:47:19'),
(11490, '\0O\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:47:19'),
(11491, '', 0.00, 7, '2019-11-12 06:47:19'),
(11492, '', 0.00, 6, '2019-11-12 06:47:19'),
(11493, '\0\0\0', 0.00, 7, '2019-11-12 06:47:19'),
(11494, '\0P\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:47:19'),
(11495, '\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:19'),
(11496, '', 0.00, 6, '2019-11-12 06:47:19'),
(11497, '', 0.00, 7, '2019-11-12 06:47:19'),
(11498, '\0Q\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:47:19'),
(11499, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:19'),
(11500, '', 0.00, 6, '2019-11-12 06:47:19'),
(11501, '', 0.00, 7, '2019-11-12 06:47:19'),
(11502, '\0R\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:47:19'),
(11503, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:19'),
(11504, '', 0.00, 6, '2019-11-12 06:47:19'),
(11505, '\0\0\0', 0.00, 7, '2019-11-12 06:47:19'),
(11506, '\0S\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:47:19'),
(11507, '', 0.00, 7, '2019-11-12 06:47:19'),
(11508, '', 0.00, 6, '2019-11-12 06:47:20'),
(11509, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:20'),
(11510, '\0T\0\0\0U\0\0~', 0.00, 6, '2019-11-12 06:47:20'),
(11511, '', 0.00, 7, '2019-11-12 06:47:20'),
(11512, '', 0.00, 6, '2019-11-12 06:47:20'),
(11513, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:20'),
(11514, '\0U\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:47:20'),
(11515, '', 0.00, 7, '2019-11-12 06:47:20'),
(11516, '', 0.00, 6, '2019-11-12 06:47:20'),
(11517, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:20'),
(11518, '\0V\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:47:20'),
(11519, '', 0.00, 7, '2019-11-12 06:47:20'),
(11520, '', 0.00, 6, '2019-11-12 06:47:20'),
(11521, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:20'),
(11522, '\0W\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:47:21'),
(11523, '', 0.00, 7, '2019-11-12 06:47:21'),
(11524, '', 0.00, 6, '2019-11-12 06:47:21'),
(11525, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:21'),
(11526, '\0X\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:47:21'),
(11527, '', 0.00, 7, '2019-11-12 06:47:21'),
(11528, '', 0.00, 6, '2019-11-12 06:47:21'),
(11529, '\0\0\0\0	\0\0~', 0.00, 7, '2019-11-12 06:47:21'),
(11530, '\0Y\0\0\0Z\0\0~', 0.00, 6, '2019-11-12 06:47:21'),
(11531, '', 0.00, 7, '2019-11-12 06:47:21'),
(11532, '', 0.00, 6, '2019-11-12 06:47:21'),
(11533, '\0	\0\0\0', 0.00, 7, '2019-11-12 06:47:21'),
(11534, '\0Z\0\0\0[\0\0~', 0.00, 6, '2019-11-12 06:47:22'),
(11535, '\0\0~', 0.00, 7, '2019-11-12 06:47:22'),
(11536, '', 0.00, 6, '2019-11-12 06:47:22'),
(11537, '', 0.00, 7, '2019-11-12 06:47:22'),
(11538, '\0[\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:47:22'),
(11539, '\0', 0.00, 7, '2019-11-12 06:47:22'),
(11540, '', 0.00, 6, '2019-11-12 06:47:22'),
(11541, '\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:22'),
(11542, '\0\\\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:47:22'),
(11543, '\0', 0.00, 7, '2019-11-12 06:47:22'),
(11544, '\0\\\0\0', 0.00, 6, '2019-11-12 06:47:22'),
(11545, '', 0.00, 7, '2019-11-12 06:47:22'),
(11546, '', 0.00, 6, '2019-11-12 06:47:22'),
(11547, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:23'),
(11548, '\0]\0\0\0^\0\0~', 0.00, 6, '2019-11-12 06:47:23'),
(11549, '', 0.00, 7, '2019-11-12 06:47:23'),
(11550, '', 0.00, 6, '2019-11-12 06:47:23'),
(11551, '\0\0\0\0\r\0\0~', 0.00, 7, '2019-11-12 06:47:24'),
(11552, '\0^\0\0\0_\0\0~', 0.00, 6, '2019-11-12 06:47:24'),
(11553, '', 0.00, 7, '2019-11-12 06:47:24'),
(11554, '', 0.00, 6, '2019-11-12 06:47:24'),
(11555, '\0\r\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:24'),
(11556, '\0_\0\0\0`\0\0~', 0.00, 6, '2019-11-12 06:47:24'),
(11557, '', 0.00, 7, '2019-11-12 06:47:24'),
(11558, '\0_\0\0B)\0\0\0`\0\0\0', 0.00, 6, '2019-11-12 06:47:24'),
(11559, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:24'),
(11560, '\0`\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:47:24'),
(11561, '', 0.00, 7, '2019-11-12 06:47:24'),
(11562, '', 0.00, 6, '2019-11-12 06:47:24'),
(11563, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:24'),
(11564, '\0a\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:47:25'),
(11565, '', 0.00, 7, '2019-11-12 06:47:25'),
(11566, '', 0.00, 6, '2019-11-12 06:47:25'),
(11567, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:25'),
(11568, '\0b\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:47:25'),
(11569, '', 0.00, 7, '2019-11-12 06:47:25'),
(11570, '', 0.00, 6, '2019-11-12 06:47:25'),
(11571, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:25'),
(11572, '\0c\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:47:25'),
(11573, '', 0.00, 7, '2019-11-12 06:47:25');
INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(11574, '', 0.00, 6, '2019-11-12 06:47:26'),
(11575, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:26'),
(11576, '\0d\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:47:26'),
(11577, '', 0.00, 7, '2019-11-12 06:47:26'),
(11578, '', 0.00, 6, '2019-11-12 06:47:26'),
(11579, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:26'),
(11580, '\0e\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:47:26'),
(11581, '', 0.00, 7, '2019-11-12 06:47:26'),
(11582, '', 0.00, 6, '2019-11-12 06:47:27'),
(11583, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:27'),
(11584, '\0f\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:47:27'),
(11585, '\0\0\0R', 0.00, 7, '2019-11-12 06:47:27'),
(11586, '', 0.00, 6, '2019-11-12 06:47:27'),
(11587, '', 0.00, 7, '2019-11-12 06:47:27'),
(11588, '\0g\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:47:27'),
(11589, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:27'),
(11590, '', 0.00, 6, '2019-11-12 06:47:27'),
(11591, '', 0.00, 7, '2019-11-12 06:47:27'),
(11592, '\0h\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:47:28'),
(11593, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:28'),
(11594, '', 0.00, 6, '2019-11-12 06:47:28'),
(11595, '', 0.00, 7, '2019-11-12 06:47:28'),
(11596, '\0i\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:47:29'),
(11597, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:29'),
(11598, '', 0.00, 6, '2019-11-12 06:47:29'),
(11599, '', 0.00, 7, '2019-11-12 06:47:29'),
(11600, '\0j\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:47:29'),
(11601, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:29'),
(11602, '', 0.00, 6, '2019-11-12 06:47:29'),
(11603, '', 0.00, 7, '2019-11-12 06:47:29'),
(11604, '\0k\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:47:29'),
(11605, '\0\0\0\0\Z\0\0~', 0.00, 7, '2019-11-12 06:47:30'),
(11606, '', 0.00, 6, '2019-11-12 06:47:30'),
(11607, '', 0.00, 7, '2019-11-12 06:47:30'),
(11608, '\0l\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:47:30'),
(11609, '\0\Z\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:30'),
(11610, '', 0.00, 6, '2019-11-12 06:47:30'),
(11611, '\0\Z\0\0R', 0.00, 7, '2019-11-12 06:47:30'),
(11612, '\0m\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:47:30'),
(11613, '', 0.00, 7, '2019-11-12 06:47:30'),
(11614, '', 0.00, 6, '2019-11-12 06:47:30'),
(11615, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:30'),
(11616, '\0n\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:47:30'),
(11617, '', 0.00, 7, '2019-11-12 06:47:30'),
(11618, '', 0.00, 6, '2019-11-12 06:47:30'),
(11619, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:30'),
(11620, '\0o\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:47:30'),
(11621, '', 0.00, 7, '2019-11-12 06:47:30'),
(11622, '', 0.00, 6, '2019-11-12 06:47:30'),
(11623, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:30'),
(11624, '\0p\0\0\0q\0\0~', 0.00, 6, '2019-11-12 06:47:30'),
(11625, '', 0.00, 7, '2019-11-12 06:47:30'),
(11626, '', 0.00, 6, '2019-11-12 06:47:31'),
(11627, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:31'),
(11628, '\0q\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:47:31'),
(11629, '', 0.00, 7, '2019-11-12 06:47:31'),
(11630, '', 0.00, 6, '2019-11-12 06:47:31'),
(11631, '\0\0\0\0 \0\0~', 0.00, 7, '2019-11-12 06:47:31'),
(11632, '\0r\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:47:31'),
(11633, '\0\0\0#\0\0\0 \0\0\0', 0.00, 7, '2019-11-12 06:47:31'),
(11634, '', 0.00, 6, '2019-11-12 06:47:31'),
(11635, '\0 \0\0\0!\0\0~', 0.00, 7, '2019-11-12 06:47:31'),
(11636, '\0s\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:47:31'),
(11637, '', 0.00, 7, '2019-11-12 06:47:31'),
(11638, '', 0.00, 6, '2019-11-12 06:47:31'),
(11639, '', 0.00, 7, '2019-11-12 06:47:31'),
(11640, '\0t\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:47:31'),
(11641, '\0&quot;\0\0\0#\0\0~', 0.00, 7, '2019-11-12 06:47:32'),
(11642, '', 0.00, 6, '2019-11-12 06:47:32'),
(11643, '', 0.00, 7, '2019-11-12 06:47:32'),
(11644, '\0u\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:47:32'),
(11645, '\0#\0\0\0$\0\0~', 0.00, 7, '2019-11-12 06:47:32'),
(11646, '', 0.00, 6, '2019-11-12 06:47:32'),
(11647, '\0#\0\0', 0.00, 7, '2019-11-12 06:47:32'),
(11648, '\0v\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:47:32'),
(11649, '', 0.00, 7, '2019-11-12 06:47:32'),
(11650, '', 0.00, 6, '2019-11-12 06:47:32'),
(11651, '\0$\0\0\0%\0\0~', 0.00, 7, '2019-11-12 06:47:32'),
(11652, '\0w\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:47:32'),
(11653, '', 0.00, 7, '2019-11-12 06:47:32'),
(11654, '', 0.00, 6, '2019-11-12 06:47:32'),
(11655, '', 0.00, 7, '2019-11-12 06:47:32'),
(11656, '\0x\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:47:32'),
(11657, '\0&amp;\0\0\0\'\0\0~', 0.00, 7, '2019-11-12 06:47:32'),
(11658, '', 0.00, 6, '2019-11-12 06:47:33'),
(11659, '', 0.00, 7, '2019-11-12 06:47:33'),
(11660, '\0y\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:47:33'),
(11661, '\0\'\0\0\0(\0\0~', 0.00, 7, '2019-11-12 06:47:33'),
(11662, '', 0.00, 6, '2019-11-12 06:47:33'),
(11663, '', 0.00, 7, '2019-11-12 06:47:33'),
(11664, '\0z\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:47:33'),
(11665, '\0(\0\0\0)\0\0~', 0.00, 7, '2019-11-12 06:47:33'),
(11666, '', 0.00, 6, '2019-11-12 06:47:33'),
(11667, '', 0.00, 7, '2019-11-12 06:47:33'),
(11668, '\0{\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:47:33'),
(11669, '\0)\0\0\0*\0\0~', 0.00, 7, '2019-11-12 06:47:33'),
(11670, '\0{\0\0', 0.00, 6, '2019-11-12 06:47:33'),
(11671, '', 0.00, 7, '2019-11-12 06:47:33'),
(11672, '', 0.00, 6, '2019-11-12 06:47:33'),
(11673, '\0*\0\0\0+\0\0~', 0.00, 7, '2019-11-12 06:47:33'),
(11674, '\0|\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:47:34'),
(11675, '', 0.00, 7, '2019-11-12 06:47:34'),
(11676, '', 0.00, 6, '2019-11-12 06:47:34'),
(11677, '\0+\0\0\0', 0.00, 7, '2019-11-12 06:47:34'),
(11678, '\0}\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:47:34'),
(11679, '', 0.00, 7, '2019-11-12 06:47:34'),
(11680, '', 0.00, 6, '2019-11-12 06:47:34'),
(11681, '\0', 0.00, 7, '2019-11-12 06:47:34'),
(11682, '\0~\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:47:34'),
(11683, '\0', 0.00, 7, '2019-11-12 06:47:34'),
(11684, '', 0.00, 6, '2019-11-12 06:47:34'),
(11685, '\0-\0\0\0.\0\0~', 0.00, 7, '2019-11-12 06:47:34'),
(11686, '', 0.00, 6, '2019-11-12 06:47:34'),
(11687, '', 0.00, 7, '2019-11-12 06:47:34'),
(11688, '', 0.00, 6, '2019-11-12 06:47:34'),
(11689, '\0.\0\0\0/\0\0~', 0.00, 7, '2019-11-12 06:47:34'),
(11690, '', 0.00, 6, '2019-11-12 06:47:34'),
(11691, '', 0.00, 7, '2019-11-12 06:47:34'),
(11692, '', 0.00, 6, '2019-11-12 06:47:35'),
(11693, '\0/\0\0\00\0\0~', 0.00, 7, '2019-11-12 06:47:35'),
(11694, '', 0.00, 6, '2019-11-12 06:47:35'),
(11695, '', 0.00, 7, '2019-11-12 06:47:35'),
(11696, '', 0.00, 6, '2019-11-12 06:47:35'),
(11697, '', 0.00, 7, '2019-11-12 06:47:35'),
(11698, '', 0.00, 6, '2019-11-12 06:47:35'),
(11699, '\01\0\0\02\0\0~', 0.00, 7, '2019-11-12 06:47:35'),
(11700, '', 0.00, 6, '2019-11-12 06:47:35'),
(11701, '', 0.00, 7, '2019-11-12 06:47:35'),
(11702, '', 0.00, 6, '2019-11-12 06:47:35'),
(11703, '\02\0\0\03\0\0~', 0.00, 7, '2019-11-12 06:47:35'),
(11704, '', 0.00, 7, '2019-11-12 06:47:36'),
(11705, '', 0.00, 6, '2019-11-12 06:47:36'),
(11706, '', 0.00, 6, '2019-11-12 06:47:36'),
(11707, '\03\0\0\04\0\0~', 0.00, 7, '2019-11-12 06:47:36'),
(11708, '', 0.00, 7, '2019-11-12 06:47:36'),
(11709, '', 0.00, 6, '2019-11-12 06:47:36'),
(11710, '\04\0\0\05\0\0~', 0.00, 7, '2019-11-12 06:47:36'),
(11711, '', 0.00, 6, '2019-11-12 06:47:36'),
(11712, '', 0.00, 7, '2019-11-12 06:47:36'),
(11713, '', 0.00, 6, '2019-11-12 06:47:36'),
(11714, '\05\0\0\06\0\0~', 0.00, 7, '2019-11-12 06:47:37'),
(11715, '', 0.00, 6, '2019-11-12 06:47:37'),
(11716, '', 0.00, 7, '2019-11-12 06:47:37'),
(11717, '', 0.00, 6, '2019-11-12 06:47:37'),
(11718, '\06\0\0\07\0\0~', 0.00, 7, '2019-11-12 06:47:37'),
(11719, '', 0.00, 6, '2019-11-12 06:47:37'),
(11720, '', 0.00, 7, '2019-11-12 06:47:37'),
(11721, '', 0.00, 6, '2019-11-12 06:47:37'),
(11722, '\07\0\0\08\0\0~', 0.00, 7, '2019-11-12 06:47:37'),
(11723, '', 0.00, 6, '2019-11-12 06:47:37'),
(11724, '', 0.00, 7, '2019-11-12 06:47:37'),
(11725, '', 0.00, 6, '2019-11-12 06:47:37'),
(11726, '\08\0\0\09\0\0~', 0.00, 7, '2019-11-12 06:47:37'),
(11727, '', 0.00, 6, '2019-11-12 06:47:37'),
(11728, '', 0.00, 7, '2019-11-12 06:47:37'),
(11729, '', 0.00, 6, '2019-11-12 06:47:37'),
(11730, '\09\0\0\0:\0\0~', 0.00, 7, '2019-11-12 06:47:37'),
(11731, '', 0.00, 6, '2019-11-12 06:47:37'),
(11732, '', 0.00, 7, '2019-11-12 06:47:37'),
(11733, '', 0.00, 6, '2019-11-12 06:47:38'),
(11734, '\0:\0\0\0;\0\0~', 0.00, 7, '2019-11-12 06:47:38'),
(11735, '', 0.00, 6, '2019-11-12 06:47:38'),
(11736, '', 0.00, 7, '2019-11-12 06:47:38'),
(11737, '', 0.00, 6, '2019-11-12 06:47:38'),
(11738, '\0;\0\0\0&lt;\0\0~', 0.00, 7, '2019-11-12 06:47:38'),
(11739, '', 0.00, 6, '2019-11-12 06:47:38'),
(11740, '', 0.00, 7, '2019-11-12 06:47:38'),
(11741, '', 0.00, 6, '2019-11-12 06:47:38'),
(11742, '\0&lt;\0\0\0=\0\0~', 0.00, 7, '2019-11-12 06:47:38'),
(11743, '', 0.00, 6, '2019-11-12 06:47:38'),
(11744, '', 0.00, 7, '2019-11-12 06:47:38'),
(11745, '', 0.00, 6, '2019-11-12 06:47:38'),
(11746, '\0=\0\0\0&gt;\0\0~', 0.00, 7, '2019-11-12 06:47:38'),
(11747, '', 0.00, 6, '2019-11-12 06:47:38'),
(11748, '', 0.00, 7, '2019-11-12 06:47:39'),
(11749, '', 0.00, 6, '2019-11-12 06:47:39'),
(11750, '\0&gt;\0\0\0?\0\0~', 0.00, 7, '2019-11-12 06:47:39'),
(11751, '', 0.00, 6, '2019-11-12 06:47:39'),
(11752, '', 0.00, 7, '2019-11-12 06:47:39'),
(11753, '', 0.00, 6, '2019-11-12 06:47:39'),
(11754, '\0?\0\0\0@\0\0~', 0.00, 7, '2019-11-12 06:47:39'),
(11755, '', 0.00, 6, '2019-11-12 06:47:39'),
(11756, '\0?\0\02\0\0\0@\0\0\0', 0.00, 7, '2019-11-12 06:47:39'),
(11757, '', 0.00, 6, '2019-11-12 06:47:39'),
(11758, '\0@\0\0\0A\0\0~', 0.00, 7, '2019-11-12 06:47:39'),
(11759, '', 0.00, 6, '2019-11-12 06:47:39'),
(11760, '', 0.00, 7, '2019-11-12 06:47:39'),
(11761, '', 0.00, 6, '2019-11-12 06:47:39'),
(11762, '\0A\0\0\0B\0\0~', 0.00, 7, '2019-11-12 06:47:39'),
(11763, '', 0.00, 6, '2019-11-12 06:47:39'),
(11764, '', 0.00, 7, '2019-11-12 06:47:39'),
(11765, '', 0.00, 6, '2019-11-12 06:47:39'),
(11766, '\0B\0\0\0C\0\0~', 0.00, 7, '2019-11-12 06:47:39'),
(11767, '', 0.00, 6, '2019-11-12 06:47:39'),
(11768, '\0B\0\0R', 0.00, 7, '2019-11-12 06:47:39'),
(11769, '', 0.00, 6, '2019-11-12 06:47:39'),
(11770, '', 0.00, 7, '2019-11-12 06:47:40'),
(11771, '', 0.00, 6, '2019-11-12 06:47:40'),
(11772, '\0C\0\0\0D\0\0~', 0.00, 7, '2019-11-12 06:47:40'),
(11773, '', 0.00, 6, '2019-11-12 06:47:40'),
(11774, '', 0.00, 7, '2019-11-12 06:47:40'),
(11775, '', 0.00, 6, '2019-11-12 06:47:40'),
(11776, '\0D\0\0\0E\0\0~', 0.00, 7, '2019-11-12 06:47:40'),
(11777, '', 0.00, 6, '2019-11-12 06:47:40'),
(11778, '', 0.00, 7, '2019-11-12 06:47:40'),
(11779, '', 0.00, 6, '2019-11-12 06:47:40'),
(11780, '\0E\0\0\0F\0\0~', 0.00, 7, '2019-11-12 06:47:40'),
(11781, '', 0.00, 6, '2019-11-12 06:47:40'),
(11782, '', 0.00, 7, '2019-11-12 06:47:40'),
(11783, '', 0.00, 6, '2019-11-12 06:47:40'),
(11784, '\0F\0\0\0G\0\0~', 0.00, 7, '2019-11-12 06:47:40'),
(11785, '', 0.00, 6, '2019-11-12 06:47:40'),
(11786, '', 0.00, 7, '2019-11-12 06:47:40'),
(11787, '', 0.00, 6, '2019-11-12 06:47:40'),
(11788, '\0G\0\0\0H\0\0~', 0.00, 7, '2019-11-12 06:47:40'),
(11789, '', 0.00, 6, '2019-11-12 06:47:40'),
(11790, '', 0.00, 7, '2019-11-12 06:47:40'),
(11791, '', 0.00, 6, '2019-11-12 06:47:41'),
(11792, '\0H\0\0\0I\0\0~', 0.00, 7, '2019-11-12 06:47:41'),
(11793, '', 0.00, 6, '2019-11-12 06:47:41'),
(11794, '', 0.00, 7, '2019-11-12 06:47:41'),
(11795, '', 0.00, 6, '2019-11-12 06:47:41'),
(11796, '\0I\0\0\0J\0\0~', 0.00, 7, '2019-11-12 06:47:41'),
(11797, '', 0.00, 6, '2019-11-12 06:47:41'),
(11798, '', 0.00, 7, '2019-11-12 06:47:41'),
(11799, '', 0.00, 6, '2019-11-12 06:47:41'),
(11800, '\0J\0\0\0K\0\0~', 0.00, 7, '2019-11-12 06:47:41'),
(11801, '', 0.00, 6, '2019-11-12 06:47:41'),
(11802, '', 0.00, 7, '2019-11-12 06:47:41'),
(11803, '', 0.00, 6, '2019-11-12 06:47:41'),
(11804, '\0K\0\0\0L\0\0~', 0.00, 7, '2019-11-12 06:47:41'),
(11805, '', 0.00, 6, '2019-11-12 06:47:41'),
(11806, '\0K\0\0S', 0.00, 7, '2019-11-12 06:47:41'),
(11807, '', 0.00, 6, '2019-11-12 06:47:41'),
(11808, '', 0.00, 7, '2019-11-12 06:47:41'),
(11809, '', 0.00, 6, '2019-11-12 06:47:41'),
(11810, '\0L\0\0\0M\0\0~', 0.00, 7, '2019-11-12 06:47:41'),
(11811, '', 0.00, 6, '2019-11-12 06:47:41'),
(11812, '', 0.00, 7, '2019-11-12 06:47:41'),
(11813, '', 0.00, 6, '2019-11-12 06:47:41'),
(11814, '\0M\0\0\0N\0\0~', 0.00, 7, '2019-11-12 06:47:42'),
(11815, '', 0.00, 6, '2019-11-12 06:47:42'),
(11816, '', 0.00, 7, '2019-11-12 06:47:42'),
(11817, '', 0.00, 6, '2019-11-12 06:47:42'),
(11818, '\0N\0\0\0O\0\0~', 0.00, 7, '2019-11-12 06:47:42'),
(11819, '', 0.00, 6, '2019-11-12 06:47:42'),
(11820, '', 0.00, 7, '2019-11-12 06:47:42'),
(11821, '', 0.00, 6, '2019-11-12 06:47:42'),
(11822, '\0O\0\0\0P\0\0~', 0.00, 7, '2019-11-12 06:47:42'),
(11823, '', 0.00, 6, '2019-11-12 06:47:42'),
(11824, '', 0.00, 7, '2019-11-12 06:47:42'),
(11825, '', 0.00, 6, '2019-11-12 06:47:42'),
(11826, '\0P\0\0\0Q\0\0~', 0.00, 7, '2019-11-12 06:47:42'),
(11827, '', 0.00, 6, '2019-11-12 06:47:42'),
(11828, '\0P\0\0', 0.00, 7, '2019-11-12 06:47:42'),
(11829, '', 0.00, 6, '2019-11-12 06:47:42'),
(11830, '', 0.00, 7, '2019-11-12 06:47:42'),
(11831, '', 0.00, 6, '2019-11-12 06:47:42'),
(11832, '\0Q\0\0\0R\0\0~', 0.00, 7, '2019-11-12 06:47:42'),
(11833, '', 0.00, 6, '2019-11-12 06:47:42'),
(11834, '', 0.00, 7, '2019-11-12 06:47:42'),
(11835, '', 0.00, 6, '2019-11-12 06:47:43'),
(11836, '\0R\0\0\0S\0\0~', 0.00, 7, '2019-11-12 06:47:43'),
(11837, '', 0.00, 6, '2019-11-12 06:47:43'),
(11838, '', 0.00, 7, '2019-11-12 06:47:43'),
(11839, '', 0.00, 6, '2019-11-12 06:47:43'),
(11840, '\0S\0\0\0T\0\0~', 0.00, 7, '2019-11-12 06:47:43'),
(11841, '', 0.00, 6, '2019-11-12 06:47:43'),
(11842, '', 0.00, 7, '2019-11-12 06:47:43'),
(11843, '', 0.00, 6, '2019-11-12 06:47:43'),
(11844, '\0T\0\0\0U\0\0~', 0.00, 7, '2019-11-12 06:47:43'),
(11845, '', 0.00, 6, '2019-11-12 06:47:43'),
(11846, '', 0.00, 7, '2019-11-12 06:47:43'),
(11847, '', 0.00, 6, '2019-11-12 06:47:43'),
(11848, '\0U\0\0\0V\0\0~', 0.00, 7, '2019-11-12 06:47:43'),
(11849, '', 0.00, 6, '2019-11-12 06:47:43'),
(11850, '', 0.00, 7, '2019-11-12 06:47:43'),
(11851, '', 0.00, 6, '2019-11-12 06:47:43'),
(11852, '\0V\0\0\0W\0\0~', 0.00, 7, '2019-11-12 06:47:43'),
(11853, '', 0.00, 6, '2019-11-12 06:47:43'),
(11854, '', 0.00, 7, '2019-11-12 06:47:43'),
(11855, '', 0.00, 6, '2019-11-12 06:47:44'),
(11856, '\0W\0\0\0X\0\0~', 0.00, 7, '2019-11-12 06:47:44'),
(11857, '', 0.00, 6, '2019-11-12 06:47:44'),
(11858, '', 0.00, 7, '2019-11-12 06:47:44'),
(11859, '', 0.00, 6, '2019-11-12 06:47:44'),
(11860, '\0X\0\0\0Y\0\0~', 0.00, 7, '2019-11-12 06:47:44'),
(11861, '', 0.00, 6, '2019-11-12 06:47:44'),
(11862, '', 0.00, 7, '2019-11-12 06:47:44'),
(11863, '', 0.00, 6, '2019-11-12 06:47:44'),
(11864, '\0Y\0\0\0Z\0\0~', 0.00, 7, '2019-11-12 06:47:44'),
(11865, '', 0.00, 6, '2019-11-12 06:47:44'),
(11866, '', 0.00, 7, '2019-11-12 06:47:44'),
(11867, '', 0.00, 6, '2019-11-12 06:47:44'),
(11868, '\0Z\0\0\0[\0\0~', 0.00, 7, '2019-11-12 06:47:44'),
(11869, '', 0.00, 6, '2019-11-12 06:47:44'),
(11870, '', 0.00, 7, '2019-11-12 06:47:45'),
(11871, '', 0.00, 6, '2019-11-12 06:47:45'),
(11872, '\0[\0\0\0\\\0\0~', 0.00, 7, '2019-11-12 06:47:45'),
(11873, '', 0.00, 6, '2019-11-12 06:47:45'),
(11874, '', 0.00, 7, '2019-11-12 06:47:45'),
(11875, '', 0.00, 6, '2019-11-12 06:47:45'),
(11876, '\0\\\0\0\0]\0\0~', 0.00, 7, '2019-11-12 06:47:45'),
(11877, '', 0.00, 6, '2019-11-12 06:47:45'),
(11878, '', 0.00, 7, '2019-11-12 06:47:45'),
(11879, '', 0.00, 6, '2019-11-12 06:47:45'),
(11880, '\0]\0\0\0^\0\0~', 0.00, 7, '2019-11-12 06:47:45'),
(11881, '', 0.00, 6, '2019-11-12 06:47:46'),
(11882, '', 0.00, 7, '2019-11-12 06:47:46'),
(11883, '', 0.00, 6, '2019-11-12 06:47:46'),
(11884, '\0^\0\0\0_\0\0~', 0.00, 7, '2019-11-12 06:47:46'),
(11885, '', 0.00, 6, '2019-11-12 06:47:46'),
(11886, '', 0.00, 7, '2019-11-12 06:47:46'),
(11887, '', 0.00, 6, '2019-11-12 06:47:46'),
(11888, '\0_\0\0\0`\0\0\0_\0\0fffff&amp;r@\0`\0\0\0', 0.00, 7, '2019-11-12 06:47:46'),
(11889, '', 0.00, 6, '2019-11-12 06:47:46'),
(11890, '\0`\0\0\0a\0\0~', 0.00, 7, '2019-11-12 06:47:46'),
(11891, '', 0.00, 6, '2019-11-12 06:47:46'),
(11892, '', 0.00, 7, '2019-11-12 06:47:46'),
(11893, '', 0.00, 6, '2019-11-12 06:47:46'),
(11894, '\0a\0\0\0b\0\0~', 0.00, 7, '2019-11-12 06:47:46'),
(11895, '', 0.00, 6, '2019-11-12 06:47:46'),
(11896, '', 0.00, 7, '2019-11-12 06:47:47'),
(11897, '', 0.00, 6, '2019-11-12 06:47:47'),
(11898, '\0b\0\0\0c\0\0~', 0.00, 7, '2019-11-12 06:47:47'),
(11899, '', 0.00, 6, '2019-11-12 06:47:47'),
(11900, '', 0.00, 7, '2019-11-12 06:47:47'),
(11901, '', 0.00, 6, '2019-11-12 06:47:47'),
(11902, '\0c\0\0\0d\0\0~', 0.00, 7, '2019-11-12 06:47:47'),
(11903, '', 0.00, 6, '2019-11-12 06:47:47'),
(11904, '', 0.00, 7, '2019-11-12 06:47:47'),
(11905, '', 0.00, 6, '2019-11-12 06:47:47'),
(11906, '\0d\0\0\0e\0\0~', 0.00, 7, '2019-11-12 06:47:47'),
(11907, '', 0.00, 6, '2019-11-12 06:47:48'),
(11908, '', 0.00, 7, '2019-11-12 06:47:48'),
(11909, '', 0.00, 6, '2019-11-12 06:47:48'),
(11910, '\0e\0\0\0f\0\0~', 0.00, 7, '2019-11-12 06:47:48'),
(11911, '', 0.00, 6, '2019-11-12 06:47:48'),
(11912, '', 0.00, 7, '2019-11-12 06:47:48'),
(11913, '', 0.00, 6, '2019-11-12 06:47:48'),
(11914, '\0f\0\0\0g\0\0~', 0.00, 7, '2019-11-12 06:47:48'),
(11915, '', 0.00, 6, '2019-11-12 06:47:48'),
(11916, '', 0.00, 7, '2019-11-12 06:47:48'),
(11917, '', 0.00, 6, '2019-11-12 06:47:48'),
(11918, '\0g\0\0\0h\0\0~', 0.00, 7, '2019-11-12 06:47:48'),
(11919, '', 0.00, 6, '2019-11-12 06:47:48'),
(11920, '', 0.00, 7, '2019-11-12 06:47:49'),
(11921, '', 0.00, 6, '2019-11-12 06:47:49'),
(11922, '\0h\0\0\0i\0\0~', 0.00, 7, '2019-11-12 06:47:49'),
(11923, '', 0.00, 6, '2019-11-12 06:47:49'),
(11924, '', 0.00, 7, '2019-11-12 06:47:49'),
(11925, '', 0.00, 6, '2019-11-12 06:47:49'),
(11926, '\0i\0\0\0j\0\0~', 0.00, 7, '2019-11-12 06:47:49'),
(11927, '', 0.00, 6, '2019-11-12 06:47:49'),
(11928, '', 0.00, 7, '2019-11-12 06:47:49'),
(11929, '', 0.00, 6, '2019-11-12 06:47:49'),
(11930, '\0j\0\0\0k\0\0~', 0.00, 7, '2019-11-12 06:47:49'),
(11931, '', 0.00, 6, '2019-11-12 06:47:49'),
(11932, '', 0.00, 7, '2019-11-12 06:47:49'),
(11933, '', 0.00, 6, '2019-11-12 06:47:49'),
(11934, '\0k\0\0\0l\0\0~', 0.00, 7, '2019-11-12 06:47:49'),
(11935, '', 0.00, 6, '2019-11-12 06:47:49'),
(11936, '', 0.00, 7, '2019-11-12 06:47:49'),
(11937, '', 0.00, 6, '2019-11-12 06:47:49'),
(11938, '\0l\0\0\0m\0\0~', 0.00, 7, '2019-11-12 06:47:49'),
(11939, '', 0.00, 6, '2019-11-12 06:47:49'),
(11940, '', 0.00, 7, '2019-11-12 06:47:49'),
(11941, '', 0.00, 6, '2019-11-12 06:47:49'),
(11942, '\0m\0\0\0n\0\0~', 0.00, 7, '2019-11-12 06:47:50'),
(11943, '', 0.00, 6, '2019-11-12 06:47:50'),
(11944, '', 0.00, 7, '2019-11-12 06:47:50'),
(11945, '', 0.00, 6, '2019-11-12 06:47:50'),
(11946, '\0n\0\0\0o\0\0~', 0.00, 7, '2019-11-12 06:47:50'),
(11947, '', 0.00, 6, '2019-11-12 06:47:50'),
(11948, '', 0.00, 7, '2019-11-12 06:47:50'),
(11949, '', 0.00, 6, '2019-11-12 06:47:50'),
(11950, '\0o\0\0\0p\0\0~', 0.00, 7, '2019-11-12 06:47:50'),
(11951, '', 0.00, 6, '2019-11-12 06:47:50'),
(11952, '', 0.00, 7, '2019-11-12 06:47:50'),
(11953, '', 0.00, 6, '2019-11-12 06:47:50'),
(11954, '\0p\0\0\0q\0\0~', 0.00, 7, '2019-11-12 06:47:50'),
(11955, '', 0.00, 6, '2019-11-12 06:47:50'),
(11956, '\0p\0\0R', 0.00, 7, '2019-11-12 06:47:50'),
(11957, '', 0.00, 6, '2019-11-12 06:47:50'),
(11958, '', 0.00, 7, '2019-11-12 06:47:50'),
(11959, '', 0.00, 6, '2019-11-12 06:47:50'),
(11960, '\0q\0\0\0r\0\0~', 0.00, 7, '2019-11-12 06:47:50'),
(11961, '', 0.00, 6, '2019-11-12 06:47:50'),
(11962, '', 0.00, 7, '2019-11-12 06:47:50'),
(11963, '', 0.00, 6, '2019-11-12 06:47:50'),
(11964, '\0r\0\0\0s\0\0~', 0.00, 7, '2019-11-12 06:47:50'),
(11965, '', 0.00, 6, '2019-11-12 06:47:50'),
(11966, '', 0.00, 7, '2019-11-12 06:47:50'),
(11967, '', 0.00, 6, '2019-11-12 06:47:51'),
(11968, '\0s\0\0\0t\0\0~', 0.00, 7, '2019-11-12 06:47:51'),
(11969, '', 0.00, 6, '2019-11-12 06:47:51'),
(11970, '', 0.00, 7, '2019-11-12 06:47:51'),
(11971, '', 0.00, 6, '2019-11-12 06:47:51'),
(11972, '\0t\0\0\0u\0\0~', 0.00, 7, '2019-11-12 06:47:51'),
(11973, '', 0.00, 6, '2019-11-12 06:47:51'),
(11974, '', 0.00, 7, '2019-11-12 06:47:51'),
(11975, '', 0.00, 6, '2019-11-12 06:47:51'),
(11976, '\0u\0\0\0v\0\0~', 0.00, 7, '2019-11-12 06:47:51'),
(11977, '', 0.00, 6, '2019-11-12 06:47:51'),
(11978, '', 0.00, 7, '2019-11-12 06:47:51'),
(11979, '', 0.00, 6, '2019-11-12 06:47:51'),
(11980, '\0v\0\0\0w\0\0~', 0.00, 7, '2019-11-12 06:47:51'),
(11981, '', 0.00, 6, '2019-11-12 06:47:51'),
(11982, '', 0.00, 7, '2019-11-12 06:47:51'),
(11983, '', 0.00, 6, '2019-11-12 06:47:51'),
(11984, '\0w\0\0\0x\0\0~', 0.00, 7, '2019-11-12 06:47:51'),
(11985, '', 0.00, 6, '2019-11-12 06:47:52'),
(11986, '\0w\0\0', 0.00, 7, '2019-11-12 06:47:52'),
(11987, '', 0.00, 6, '2019-11-12 06:47:52'),
(11988, '', 0.00, 7, '2019-11-12 06:47:52'),
(11989, '', 0.00, 6, '2019-11-12 06:47:52'),
(11990, '\0x\0\0\0y\0\0~', 0.00, 7, '2019-11-12 06:47:52'),
(11991, '', 0.00, 6, '2019-11-12 06:47:52'),
(11992, '', 0.00, 7, '2019-11-12 06:47:52'),
(11993, '', 0.00, 6, '2019-11-12 06:47:52'),
(11994, '\0y\0\0\0z\0\0~', 0.00, 7, '2019-11-12 06:47:52'),
(11995, '', 0.00, 6, '2019-11-12 06:47:52'),
(11996, '', 0.00, 7, '2019-11-12 06:47:52'),
(11997, '', 0.00, 6, '2019-11-12 06:47:52'),
(11998, '\0z\0\0\0{\0\0~', 0.00, 7, '2019-11-12 06:47:52'),
(11999, '', 0.00, 6, '2019-11-12 06:47:52'),
(12000, '', 0.00, 7, '2019-11-12 06:47:53'),
(12001, '', 0.00, 6, '2019-11-12 06:47:53'),
(12002, '\0{\0\0\0|\0\0~', 0.00, 7, '2019-11-12 06:47:53'),
(12003, '', 0.00, 6, '2019-11-12 06:47:53'),
(12004, '', 0.00, 7, '2019-11-12 06:47:53'),
(12005, '', 0.00, 6, '2019-11-12 06:47:54'),
(12006, '\0|\0\0\0}\0\0~', 0.00, 7, '2019-11-12 06:47:54'),
(12007, '', 0.00, 6, '2019-11-12 06:47:54'),
(12008, '', 0.00, 7, '2019-11-12 06:47:54'),
(12009, '', 0.00, 6, '2019-11-12 06:47:54'),
(12010, '\0}\0\0\0~\0\0~', 0.00, 7, '2019-11-12 06:47:54'),
(12011, '', 0.00, 6, '2019-11-12 06:47:54'),
(12012, '', 0.00, 7, '2019-11-12 06:47:54'),
(12013, '', 0.00, 6, '2019-11-12 06:47:54'),
(12014, '\0~\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:47:54'),
(12015, '', 0.00, 6, '2019-11-12 06:47:54'),
(12016, '', 0.00, 7, '2019-11-12 06:47:54'),
(12017, '', 0.00, 6, '2019-11-12 06:47:54'),
(12018, '', 0.00, 7, '2019-11-12 06:47:54'),
(12019, '', 0.00, 6, '2019-11-12 06:47:54'),
(12020, '', 0.00, 7, '2019-11-12 06:47:54'),
(12021, '', 0.00, 6, '2019-11-12 06:47:54'),
(12022, '', 0.00, 7, '2019-11-12 06:47:54'),
(12023, '', 0.00, 6, '2019-11-12 06:47:54'),
(12024, '', 0.00, 7, '2019-11-12 06:47:54'),
(12025, '', 0.00, 6, '2019-11-12 06:47:54'),
(12026, '', 0.00, 7, '2019-11-12 06:47:54'),
(12027, '', 0.00, 6, '2019-11-12 06:47:55'),
(12028, '', 0.00, 7, '2019-11-12 06:47:55'),
(12029, '', 0.00, 6, '2019-11-12 06:47:55'),
(12030, '', 0.00, 7, '2019-11-12 06:47:55'),
(12031, '', 0.00, 6, '2019-11-12 06:47:55'),
(12032, '', 0.00, 7, '2019-11-12 06:47:55'),
(12033, '', 0.00, 6, '2019-11-12 06:47:55'),
(12034, '', 0.00, 7, '2019-11-12 06:47:55'),
(12035, '', 0.00, 6, '2019-11-12 06:47:55'),
(12036, '', 0.00, 7, '2019-11-12 06:47:55'),
(12037, '', 0.00, 6, '2019-11-12 06:47:55'),
(12038, '', 0.00, 7, '2019-11-12 06:47:55'),
(12039, '', 0.00, 6, '2019-11-12 06:47:55'),
(12040, '', 0.00, 7, '2019-11-12 06:47:55'),
(12041, '', 0.00, 6, '2019-11-12 06:47:55'),
(12042, '', 0.00, 7, '2019-11-12 06:47:55'),
(12043, '', 0.00, 6, '2019-11-12 06:47:55'),
(12044, '', 0.00, 7, '2019-11-12 06:47:56'),
(12045, '', 0.00, 6, '2019-11-12 06:47:56'),
(12046, '', 0.00, 7, '2019-11-12 06:47:56'),
(12047, '', 0.00, 6, '2019-11-12 06:47:56'),
(12048, '', 0.00, 7, '2019-11-12 06:47:56'),
(12049, '', 0.00, 6, '2019-11-12 06:47:56'),
(12050, '', 0.00, 7, '2019-11-12 06:47:56'),
(12051, '', 0.00, 6, '2019-11-12 06:47:56'),
(12052, '', 0.00, 7, '2019-11-12 06:47:56'),
(12053, '', 0.00, 6, '2019-11-12 06:47:56'),
(12054, '', 0.00, 7, '2019-11-12 06:47:56'),
(12055, '', 0.00, 6, '2019-11-12 06:47:56'),
(12056, '', 0.00, 7, '2019-11-12 06:47:56'),
(12057, '', 0.00, 6, '2019-11-12 06:47:56'),
(12058, '', 0.00, 7, '2019-11-12 06:47:57'),
(12059, '', 0.00, 6, '2019-11-12 06:47:57'),
(12060, '', 0.00, 7, '2019-11-12 06:47:57'),
(12061, '', 0.00, 6, '2019-11-12 06:47:57'),
(12062, '', 0.00, 7, '2019-11-12 06:47:57'),
(12063, '', 0.00, 6, '2019-11-12 06:47:57'),
(12064, '', 0.00, 7, '2019-11-12 06:47:57'),
(12065, '', 0.00, 6, '2019-11-12 06:47:57'),
(12066, '', 0.00, 7, '2019-11-12 06:47:57'),
(12067, '', 0.00, 6, '2019-11-12 06:47:57'),
(12068, '', 0.00, 7, '2019-11-12 06:47:57'),
(12069, '', 0.00, 6, '2019-11-12 06:47:58'),
(12070, '', 0.00, 7, '2019-11-12 06:47:58'),
(12071, '', 0.00, 6, '2019-11-12 06:47:58'),
(12072, '', 0.00, 7, '2019-11-12 06:47:59'),
(12073, '', 0.00, 6, '2019-11-12 06:47:59'),
(12074, '', 0.00, 7, '2019-11-12 06:47:59'),
(12075, '', 0.00, 6, '2019-11-12 06:47:59'),
(12076, '', 0.00, 7, '2019-11-12 06:47:59'),
(12077, '', 0.00, 6, '2019-11-12 06:47:59'),
(12078, '', 0.00, 7, '2019-11-12 06:47:59'),
(12079, '', 0.00, 6, '2019-11-12 06:47:59'),
(12080, '', 0.00, 7, '2019-11-12 06:48:00'),
(12081, '', 0.00, 6, '2019-11-12 06:48:00'),
(12082, '', 0.00, 7, '2019-11-12 06:48:00'),
(12083, '', 0.00, 6, '2019-11-12 06:48:00'),
(12084, '', 0.00, 7, '2019-11-12 06:48:00'),
(12085, '', 0.00, 6, '2019-11-12 06:48:00'),
(12086, '', 0.00, 7, '2019-11-12 06:48:00'),
(12087, '', 0.00, 6, '2019-11-12 06:48:00'),
(12088, '', 0.00, 7, '2019-11-12 06:48:00'),
(12089, '', 0.00, 6, '2019-11-12 06:48:00'),
(12090, '', 0.00, 7, '2019-11-12 06:48:00'),
(12091, '', 0.00, 6, '2019-11-12 06:48:00'),
(12092, '', 0.00, 7, '2019-11-12 06:48:00'),
(12093, '', 0.00, 6, '2019-11-12 06:48:00'),
(12094, '', 0.00, 7, '2019-11-12 06:48:00'),
(12095, '', 0.00, 6, '2019-11-12 06:48:00'),
(12096, '', 0.00, 7, '2019-11-12 06:48:00'),
(12097, '', 0.00, 6, '2019-11-12 06:48:00'),
(12098, '', 0.00, 7, '2019-11-12 06:48:01'),
(12099, '', 0.00, 6, '2019-11-12 06:48:01'),
(12100, '', 0.00, 7, '2019-11-12 06:48:01'),
(12101, '', 0.00, 6, '2019-11-12 06:48:01'),
(12102, '', 0.00, 7, '2019-11-12 06:48:01'),
(12103, '', 0.00, 6, '2019-11-12 06:48:01'),
(12104, '', 0.00, 7, '2019-11-12 06:48:01'),
(12105, '', 0.00, 6, '2019-11-12 06:48:01'),
(12106, '', 0.00, 7, '2019-11-12 06:48:01'),
(12107, '', 0.00, 6, '2019-11-12 06:48:02'),
(12108, '', 0.00, 7, '2019-11-12 06:48:02'),
(12109, '', 0.00, 6, '2019-11-12 06:48:02'),
(12110, '', 0.00, 7, '2019-11-12 06:48:02'),
(12111, '', 0.00, 6, '2019-11-12 06:48:02'),
(12112, '', 0.00, 7, '2019-11-12 06:48:02'),
(12113, '', 0.00, 6, '2019-11-12 06:48:02'),
(12114, '', 0.00, 7, '2019-11-12 06:48:03'),
(12115, '', 0.00, 6, '2019-11-12 06:48:03'),
(12116, '', 0.00, 7, '2019-11-12 06:48:03'),
(12117, '', 0.00, 6, '2019-11-12 06:48:03'),
(12118, '', 0.00, 7, '2019-11-12 06:48:03'),
(12119, '', 0.00, 6, '2019-11-12 06:48:04'),
(12120, '', 0.00, 7, '2019-11-12 06:48:04'),
(12121, '', 0.00, 6, '2019-11-12 06:48:04'),
(12122, '', 0.00, 7, '2019-11-12 06:48:04'),
(12123, '', 0.00, 6, '2019-11-12 06:48:04'),
(12124, '', 0.00, 7, '2019-11-12 06:48:04'),
(12125, '', 0.00, 6, '2019-11-12 06:48:04'),
(12126, '', 0.00, 7, '2019-11-12 06:48:04'),
(12127, '', 0.00, 6, '2019-11-12 06:48:04'),
(12128, '', 0.00, 7, '2019-11-12 06:48:04'),
(12129, '', 0.00, 6, '2019-11-12 06:48:04'),
(12130, '', 0.00, 7, '2019-11-12 06:48:04'),
(12131, '', 0.00, 6, '2019-11-12 06:48:04'),
(12132, '', 0.00, 7, '2019-11-12 06:48:04'),
(12133, '', 0.00, 6, '2019-11-12 06:48:05'),
(12134, '', 0.00, 7, '2019-11-12 06:48:05'),
(12135, '', 0.00, 6, '2019-11-12 06:48:05'),
(12136, '', 0.00, 7, '2019-11-12 06:48:05'),
(12137, '', 0.00, 6, '2019-11-12 06:48:05'),
(12138, '', 0.00, 7, '2019-11-12 06:48:05'),
(12139, '', 0.00, 6, '2019-11-12 06:48:05'),
(12140, '', 0.00, 7, '2019-11-12 06:48:05'),
(12141, '', 0.00, 6, '2019-11-12 06:48:05'),
(12142, '', 0.00, 7, '2019-11-12 06:48:05'),
(12143, '', 0.00, 6, '2019-11-12 06:48:05'),
(12144, '', 0.00, 7, '2019-11-12 06:48:05'),
(12145, '', 0.00, 6, '2019-11-12 06:48:05'),
(12146, '', 0.00, 7, '2019-11-12 06:48:05'),
(12147, '', 0.00, 6, '2019-11-12 06:48:05'),
(12148, '', 0.00, 7, '2019-11-12 06:48:05'),
(12149, '', 0.00, 6, '2019-11-12 06:48:05'),
(12150, '', 0.00, 7, '2019-11-12 06:48:05'),
(12151, '', 0.00, 6, '2019-11-12 06:48:05'),
(12152, '', 0.00, 7, '2019-11-12 06:48:05'),
(12153, '', 0.00, 6, '2019-11-12 06:48:05'),
(12154, '', 0.00, 7, '2019-11-12 06:48:05'),
(12155, '', 0.00, 6, '2019-11-12 06:48:05'),
(12156, '', 0.00, 7, '2019-11-12 06:48:05'),
(12157, '', 0.00, 6, '2019-11-12 06:48:05'),
(12158, '', 0.00, 7, '2019-11-12 06:48:05'),
(12159, '', 0.00, 6, '2019-11-12 06:48:06'),
(12160, '', 0.00, 7, '2019-11-12 06:48:06'),
(12161, '', 0.00, 6, '2019-11-12 06:48:06'),
(12162, '', 0.00, 7, '2019-11-12 06:48:06'),
(12163, '', 0.00, 6, '2019-11-12 06:48:06'),
(12164, '', 0.00, 7, '2019-11-12 06:48:06'),
(12165, '', 0.00, 6, '2019-11-12 06:48:06'),
(12166, '', 0.00, 7, '2019-11-12 06:48:06'),
(12167, '', 0.00, 6, '2019-11-12 06:48:06'),
(12168, '', 0.00, 7, '2019-11-12 06:48:06'),
(12169, '', 0.00, 6, '2019-11-12 06:48:06'),
(12170, '', 0.00, 7, '2019-11-12 06:48:06'),
(12171, '', 0.00, 6, '2019-11-12 06:48:06'),
(12172, '', 0.00, 7, '2019-11-12 06:48:06'),
(12173, '', 0.00, 6, '2019-11-12 06:48:06'),
(12174, '', 0.00, 7, '2019-11-12 06:48:06'),
(12175, '', 0.00, 6, '2019-11-12 06:48:07'),
(12176, '', 0.00, 7, '2019-11-12 06:48:07'),
(12177, '', 0.00, 6, '2019-11-12 06:48:07'),
(12178, '', 0.00, 7, '2019-11-12 06:48:07'),
(12179, '', 0.00, 6, '2019-11-12 06:48:07'),
(12180, '', 0.00, 7, '2019-11-12 06:48:07'),
(12181, '', 0.00, 6, '2019-11-12 06:48:07'),
(12182, '', 0.00, 7, '2019-11-12 06:48:07'),
(12183, '', 0.00, 6, '2019-11-12 06:48:07'),
(12184, '', 0.00, 7, '2019-11-12 06:48:07'),
(12185, '', 0.00, 6, '2019-11-12 06:48:07'),
(12186, '', 0.00, 7, '2019-11-12 06:48:07'),
(12187, '', 0.00, 6, '2019-11-12 06:48:07'),
(12188, '', 0.00, 7, '2019-11-12 06:48:08'),
(12189, '', 0.00, 6, '2019-11-12 06:48:08'),
(12190, '', 0.00, 7, '2019-11-12 06:48:08'),
(12191, '', 0.00, 6, '2019-11-12 06:48:08'),
(12192, '', 0.00, 7, '2019-11-12 06:48:08'),
(12193, '', 0.00, 6, '2019-11-12 06:48:08'),
(12194, '', 0.00, 7, '2019-11-12 06:48:08'),
(12195, '', 0.00, 6, '2019-11-12 06:48:08'),
(12196, '', 0.00, 7, '2019-11-12 06:48:08'),
(12197, '', 0.00, 6, '2019-11-12 06:48:08'),
(12198, '', 0.00, 7, '2019-11-12 06:48:08'),
(12199, '', 0.00, 6, '2019-11-12 06:48:08'),
(12200, '', 0.00, 7, '2019-11-12 06:48:08'),
(12201, '', 0.00, 6, '2019-11-12 06:48:08'),
(12202, '', 0.00, 7, '2019-11-12 06:48:08'),
(12203, '\0\0\0', 0.00, 6, '2019-11-12 06:48:08'),
(12204, '', 0.00, 7, '2019-11-12 06:48:08'),
(12205, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:08'),
(12206, '', 0.00, 7, '2019-11-12 06:48:09'),
(12207, '', 0.00, 6, '2019-11-12 06:48:09'),
(12208, '', 0.00, 7, '2019-11-12 06:48:09'),
(12209, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:09'),
(12210, '', 0.00, 7, '2019-11-12 06:48:09'),
(12211, '', 0.00, 6, '2019-11-12 06:48:09'),
(12212, '', 0.00, 7, '2019-11-12 06:48:09'),
(12213, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:09'),
(12214, '', 0.00, 7, '2019-11-12 06:48:09'),
(12215, '', 0.00, 6, '2019-11-12 06:48:09'),
(12216, '', 0.00, 7, '2019-11-12 06:48:09'),
(12217, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:09'),
(12218, '', 0.00, 7, '2019-11-12 06:48:09'),
(12219, '', 0.00, 6, '2019-11-12 06:48:09'),
(12220, '', 0.00, 7, '2019-11-12 06:48:09'),
(12221, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:09'),
(12222, '', 0.00, 7, '2019-11-12 06:48:09'),
(12223, '', 0.00, 6, '2019-11-12 06:48:09'),
(12224, '', 0.00, 7, '2019-11-12 06:48:10'),
(12225, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:10'),
(12226, '', 0.00, 7, '2019-11-12 06:48:10'),
(12227, '', 0.00, 6, '2019-11-12 06:48:10'),
(12228, '', 0.00, 7, '2019-11-12 06:48:10'),
(12229, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:10'),
(12230, '', 0.00, 7, '2019-11-12 06:48:10'),
(12231, '', 0.00, 6, '2019-11-12 06:48:11'),
(12232, '', 0.00, 7, '2019-11-12 06:48:11'),
(12233, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:11'),
(12234, '', 0.00, 7, '2019-11-12 06:48:11'),
(12235, '', 0.00, 6, '2019-11-12 06:48:11'),
(12236, '', 0.00, 7, '2019-11-12 06:48:11'),
(12237, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:11'),
(12238, '', 0.00, 7, '2019-11-12 06:48:11'),
(12239, '', 0.00, 6, '2019-11-12 06:48:11'),
(12240, '', 0.00, 7, '2019-11-12 06:48:11'),
(12241, '\0	\0\0\0	\0\0~', 0.00, 6, '2019-11-12 06:48:11'),
(12242, '', 0.00, 7, '2019-11-12 06:48:11'),
(12243, '', 0.00, 6, '2019-11-12 06:48:11'),
(12244, '', 0.00, 7, '2019-11-12 06:48:11'),
(12245, '\0', 0.00, 6, '2019-11-12 06:48:11'),
(12246, '', 0.00, 7, '2019-11-12 06:48:11'),
(12247, '\0\0\0', 0.00, 6, '2019-11-12 06:48:11'),
(12248, '', 0.00, 7, '2019-11-12 06:48:11'),
(12249, '\0\0~', 0.00, 6, '2019-11-12 06:48:12'),
(12250, '', 0.00, 7, '2019-11-12 06:48:12'),
(12251, '\0', 0.00, 6, '2019-11-12 06:48:12'),
(12252, '', 0.00, 7, '2019-11-12 06:48:12'),
(12253, '', 0.00, 6, '2019-11-12 06:48:12'),
(12254, '', 0.00, 7, '2019-11-12 06:48:12'),
(12255, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:12'),
(12256, '', 0.00, 7, '2019-11-12 06:48:12'),
(12257, '', 0.00, 6, '2019-11-12 06:48:12'),
(12258, '', 0.00, 7, '2019-11-12 06:48:12'),
(12259, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:12'),
(12260, '', 0.00, 7, '2019-11-12 06:48:12'),
(12261, '', 0.00, 6, '2019-11-12 06:48:12'),
(12262, '', 0.00, 7, '2019-11-12 06:48:12'),
(12263, '\0\r\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:48:12'),
(12264, '', 0.00, 7, '2019-11-12 06:48:12'),
(12265, '', 0.00, 6, '2019-11-12 06:48:12'),
(12266, '', 0.00, 7, '2019-11-12 06:48:13'),
(12267, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:13'),
(12268, '', 0.00, 7, '2019-11-12 06:48:13'),
(12269, '', 0.00, 6, '2019-11-12 06:48:13'),
(12270, '', 0.00, 7, '2019-11-12 06:48:13'),
(12271, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:13'),
(12272, '', 0.00, 7, '2019-11-12 06:48:13'),
(12273, '', 0.00, 6, '2019-11-12 06:48:13'),
(12274, '', 0.00, 7, '2019-11-12 06:48:13'),
(12275, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:13'),
(12276, '', 0.00, 7, '2019-11-12 06:48:13'),
(12277, '', 0.00, 6, '2019-11-12 06:48:13'),
(12278, '', 0.00, 7, '2019-11-12 06:48:13'),
(12279, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:13'),
(12280, '', 0.00, 7, '2019-11-12 06:48:13'),
(12281, '', 0.00, 6, '2019-11-12 06:48:13'),
(12282, '', 0.00, 7, '2019-11-12 06:48:14'),
(12283, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:14'),
(12284, '', 0.00, 7, '2019-11-12 06:48:14'),
(12285, '\0\0\0', 0.00, 6, '2019-11-12 06:48:14'),
(12286, '', 0.00, 7, '2019-11-12 06:48:14'),
(12287, '', 0.00, 6, '2019-11-12 06:48:14'),
(12288, '', 0.00, 7, '2019-11-12 06:48:14'),
(12289, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:14'),
(12290, '', 0.00, 7, '2019-11-12 06:48:14'),
(12291, '', 0.00, 6, '2019-11-12 06:48:14'),
(12292, '', 0.00, 7, '2019-11-12 06:48:14'),
(12293, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:14'),
(12294, '', 0.00, 7, '2019-11-12 06:48:14'),
(12295, '', 0.00, 6, '2019-11-12 06:48:14'),
(12296, '', 0.00, 7, '2019-11-12 06:48:14'),
(12297, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:14'),
(12298, '', 0.00, 7, '2019-11-12 06:48:14'),
(12299, '', 0.00, 6, '2019-11-12 06:48:15'),
(12300, '', 0.00, 7, '2019-11-12 06:48:15'),
(12301, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:15'),
(12302, '', 0.00, 7, '2019-11-12 06:48:15'),
(12303, '', 0.00, 6, '2019-11-12 06:48:15'),
(12304, '', 0.00, 7, '2019-11-12 06:48:15'),
(12305, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:15'),
(12306, '', 0.00, 7, '2019-11-12 06:48:15'),
(12307, '', 0.00, 6, '2019-11-12 06:48:15'),
(12308, '', 0.00, 7, '2019-11-12 06:48:15'),
(12309, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:16'),
(12310, '', 0.00, 7, '2019-11-12 06:48:16'),
(12311, '', 0.00, 6, '2019-11-12 06:48:16'),
(12312, '', 0.00, 7, '2019-11-12 06:48:16'),
(12313, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:16'),
(12314, '', 0.00, 7, '2019-11-12 06:48:16'),
(12315, '', 0.00, 6, '2019-11-12 06:48:16'),
(12316, '', 0.00, 7, '2019-11-12 06:48:16'),
(12317, '\0\Z\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:48:16'),
(12318, '', 0.00, 7, '2019-11-12 06:48:16'),
(12319, '', 0.00, 6, '2019-11-12 06:48:16'),
(12320, '', 0.00, 7, '2019-11-12 06:48:16'),
(12321, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:16'),
(12322, '', 0.00, 7, '2019-11-12 06:48:16'),
(12323, '', 0.00, 6, '2019-11-12 06:48:16'),
(12324, '', 0.00, 7, '2019-11-12 06:48:16'),
(12325, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:16'),
(12326, '', 0.00, 7, '2019-11-12 06:48:16'),
(12327, '', 0.00, 6, '2019-11-12 06:48:16'),
(12328, '', 0.00, 7, '2019-11-12 06:48:16'),
(12329, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:16'),
(12330, '', 0.00, 7, '2019-11-12 06:48:16'),
(12331, '', 0.00, 6, '2019-11-12 06:48:16'),
(12332, '', 0.00, 7, '2019-11-12 06:48:17'),
(12333, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:17'),
(12334, '', 0.00, 7, '2019-11-12 06:48:17'),
(12335, '\0\0\0S', 0.00, 6, '2019-11-12 06:48:17'),
(12336, '', 0.00, 7, '2019-11-12 06:48:17'),
(12337, '', 0.00, 6, '2019-11-12 06:48:17'),
(12338, '', 0.00, 7, '2019-11-12 06:48:17'),
(12339, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:17'),
(12340, '', 0.00, 7, '2019-11-12 06:48:17'),
(12341, '\0\0\0S', 0.00, 6, '2019-11-12 06:48:17'),
(12342, '', 0.00, 7, '2019-11-12 06:48:17'),
(12343, '\0\0\0 \0\0\0', 0.00, 6, '2019-11-12 06:48:17'),
(12344, '', 0.00, 7, '2019-11-12 06:48:17'),
(12345, '\0 \0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:48:17'),
(12346, '', 0.00, 7, '2019-11-12 06:48:17'),
(12347, '', 0.00, 6, '2019-11-12 06:48:17'),
(12348, '', 0.00, 7, '2019-11-12 06:48:17'),
(12349, '\0!\0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:48:17'),
(12350, '', 0.00, 7, '2019-11-12 06:48:17'),
(12351, '', 0.00, 6, '2019-11-12 06:48:17'),
(12352, '', 0.00, 7, '2019-11-12 06:48:18'),
(12353, '\0&quot;\0\0\0&quot;\0\0~', 0.00, 6, '2019-11-12 06:48:18'),
(12354, '', 0.00, 7, '2019-11-12 06:48:18'),
(12355, '', 0.00, 6, '2019-11-12 06:48:18'),
(12356, '', 0.00, 7, '2019-11-12 06:48:18'),
(12357, '\0#\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:48:18'),
(12358, '', 0.00, 7, '2019-11-12 06:48:18'),
(12359, '', 0.00, 6, '2019-11-12 06:48:18'),
(12360, '', 0.00, 7, '2019-11-12 06:48:18'),
(12361, '\0$\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:48:18'),
(12362, '', 0.00, 7, '2019-11-12 06:48:18'),
(12363, '', 0.00, 6, '2019-11-12 06:48:18'),
(12364, '', 0.00, 7, '2019-11-12 06:48:18'),
(12365, '\0%\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:48:18'),
(12366, '', 0.00, 7, '2019-11-12 06:48:18'),
(12367, '', 0.00, 6, '2019-11-12 06:48:18'),
(12368, '', 0.00, 7, '2019-11-12 06:48:18'),
(12369, '\0&amp;\0\0\0&amp;\0\0~', 0.00, 6, '2019-11-12 06:48:18'),
(12370, '', 0.00, 7, '2019-11-12 06:48:18'),
(12371, '', 0.00, 6, '2019-11-12 06:48:18'),
(12372, '', 0.00, 7, '2019-11-12 06:48:18'),
(12373, '\0\'\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:48:18'),
(12374, '', 0.00, 7, '2019-11-12 06:48:19'),
(12375, '', 0.00, 6, '2019-11-12 06:48:19'),
(12376, '', 0.00, 7, '2019-11-12 06:48:19'),
(12377, '\0(\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:48:19'),
(12378, '', 0.00, 7, '2019-11-12 06:48:19'),
(12379, '', 0.00, 6, '2019-11-12 06:48:19'),
(12380, '', 0.00, 7, '2019-11-12 06:48:19'),
(12381, '\0)\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:48:19'),
(12382, '', 0.00, 7, '2019-11-12 06:48:19'),
(12383, '', 0.00, 6, '2019-11-12 06:48:19'),
(12384, '', 0.00, 7, '2019-11-12 06:48:19'),
(12385, '\0*\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:48:19'),
(12386, '', 0.00, 7, '2019-11-12 06:48:19'),
(12387, '', 0.00, 6, '2019-11-12 06:48:19'),
(12388, '', 0.00, 7, '2019-11-12 06:48:20'),
(12389, '\0+\0\0\0+\0\0~', 0.00, 6, '2019-11-12 06:48:20'),
(12390, '', 0.00, 7, '2019-11-12 06:48:20'),
(12391, '', 0.00, 6, '2019-11-12 06:48:20'),
(12392, '', 0.00, 7, '2019-11-12 06:48:20'),
(12393, '\0', 0.00, 6, '2019-11-12 06:48:20'),
(12394, '', 0.00, 7, '2019-11-12 06:48:20'),
(12395, '\0', 0.00, 6, '2019-11-12 06:48:20'),
(12396, '', 0.00, 7, '2019-11-12 06:48:20'),
(12397, '\0-\0\0\0-\0\0~', 0.00, 6, '2019-11-12 06:48:20'),
(12398, '', 0.00, 7, '2019-11-12 06:48:20'),
(12399, '', 0.00, 6, '2019-11-12 06:48:20'),
(12400, '', 0.00, 7, '2019-11-12 06:48:20'),
(12401, '\0.\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:48:20'),
(12402, '', 0.00, 7, '2019-11-12 06:48:20'),
(12403, '', 0.00, 6, '2019-11-12 06:48:20'),
(12404, '', 0.00, 7, '2019-11-12 06:48:20'),
(12405, '\0/\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:48:20'),
(12406, '', 0.00, 7, '2019-11-12 06:48:20'),
(12407, '', 0.00, 6, '2019-11-12 06:48:20'),
(12408, '', 0.00, 7, '2019-11-12 06:48:20'),
(12409, '\00\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:48:20'),
(12410, '', 0.00, 7, '2019-11-12 06:48:20'),
(12411, '', 0.00, 6, '2019-11-12 06:48:20'),
(12412, '', 0.00, 7, '2019-11-12 06:48:21'),
(12413, '\01\0\0\01\0\0~', 0.00, 6, '2019-11-12 06:48:21'),
(12414, '', 0.00, 7, '2019-11-12 06:48:21'),
(12415, '', 0.00, 6, '2019-11-12 06:48:21'),
(12416, '', 0.00, 7, '2019-11-12 06:48:21'),
(12417, '\02\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:48:21'),
(12418, '', 0.00, 7, '2019-11-12 06:48:21'),
(12419, '', 0.00, 6, '2019-11-12 06:48:21'),
(12420, '', 0.00, 7, '2019-11-12 06:48:21'),
(12421, '\03\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:48:21'),
(12422, '', 0.00, 7, '2019-11-12 06:48:22'),
(12423, '', 0.00, 6, '2019-11-12 06:48:22'),
(12424, '', 0.00, 7, '2019-11-12 06:48:22'),
(12425, '\04\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:48:22'),
(12426, '', 0.00, 7, '2019-11-12 06:48:22'),
(12427, '', 0.00, 6, '2019-11-12 06:48:22'),
(12428, '', 0.00, 7, '2019-11-12 06:48:22'),
(12429, '\05\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:48:22'),
(12430, '', 0.00, 7, '2019-11-12 06:48:22'),
(12431, '', 0.00, 6, '2019-11-12 06:48:22'),
(12432, '', 0.00, 7, '2019-11-12 06:48:22'),
(12433, '\06\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:48:22'),
(12434, '', 0.00, 7, '2019-11-12 06:48:22'),
(12435, '', 0.00, 6, '2019-11-12 06:48:22'),
(12436, '', 0.00, 7, '2019-11-12 06:48:22'),
(12437, '\07\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:48:22'),
(12438, '', 0.00, 7, '2019-11-12 06:48:22'),
(12439, '', 0.00, 6, '2019-11-12 06:48:22'),
(12440, '', 0.00, 7, '2019-11-12 06:48:22'),
(12441, '\08\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:48:22'),
(12442, '', 0.00, 7, '2019-11-12 06:48:23'),
(12443, '', 0.00, 6, '2019-11-12 06:48:23'),
(12444, '', 0.00, 7, '2019-11-12 06:48:23'),
(12445, '\09\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:48:23'),
(12446, '', 0.00, 7, '2019-11-12 06:48:23'),
(12447, '', 0.00, 6, '2019-11-12 06:48:23'),
(12448, '', 0.00, 7, '2019-11-12 06:48:23'),
(12449, '', 0.00, 6, '2019-11-12 06:48:23'),
(12450, '', 0.00, 7, '2019-11-12 06:48:23'),
(12451, '\0;\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:48:23'),
(12452, '', 0.00, 7, '2019-11-12 06:48:23'),
(12453, '', 0.00, 6, '2019-11-12 06:48:24'),
(12454, '', 0.00, 7, '2019-11-12 06:48:24'),
(12455, '\0&lt;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:48:24'),
(12456, '', 0.00, 7, '2019-11-12 06:48:24'),
(12457, '', 0.00, 6, '2019-11-12 06:48:24'),
(12458, '', 0.00, 7, '2019-11-12 06:48:24'),
(12459, '\0=\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:48:24'),
(12460, '', 0.00, 7, '2019-11-12 06:48:24'),
(12461, '\0=\0\0', 0.00, 6, '2019-11-12 06:48:24'),
(12462, '', 0.00, 7, '2019-11-12 06:48:24'),
(12463, '', 0.00, 6, '2019-11-12 06:48:24'),
(12464, '', 0.00, 7, '2019-11-12 06:48:24'),
(12465, '\0&gt;\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:48:24'),
(12466, '', 0.00, 7, '2019-11-12 06:48:24'),
(12467, '', 0.00, 6, '2019-11-12 06:48:24'),
(12468, '', 0.00, 7, '2019-11-12 06:48:24'),
(12469, '\0?\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:48:24'),
(12470, '', 0.00, 7, '2019-11-12 06:48:24'),
(12471, '\0?\0\0\0\0\0\0@\0\0\0', 0.00, 6, '2019-11-12 06:48:24'),
(12472, '', 0.00, 7, '2019-11-12 06:48:24'),
(12473, '\0@\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:48:24'),
(12474, '', 0.00, 7, '2019-11-12 06:48:24'),
(12475, '', 0.00, 6, '2019-11-12 06:48:24'),
(12476, '', 0.00, 7, '2019-11-12 06:48:24'),
(12477, '\0A\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:48:24'),
(12478, '', 0.00, 7, '2019-11-12 06:48:25'),
(12479, '', 0.00, 6, '2019-11-12 06:48:25'),
(12480, '', 0.00, 7, '2019-11-12 06:48:25'),
(12481, '\0B\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:48:25'),
(12482, '', 0.00, 7, '2019-11-12 06:48:25'),
(12483, '', 0.00, 6, '2019-11-12 06:48:25'),
(12484, '', 0.00, 7, '2019-11-12 06:48:25'),
(12485, '', 0.00, 6, '2019-11-12 06:48:25'),
(12486, '', 0.00, 7, '2019-11-12 06:48:25'),
(12487, '\0D\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:48:25'),
(12488, '', 0.00, 7, '2019-11-12 06:48:25'),
(12489, '', 0.00, 6, '2019-11-12 06:48:25'),
(12490, '', 0.00, 7, '2019-11-12 06:48:25'),
(12491, '\0E\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:48:25'),
(12492, '', 0.00, 7, '2019-11-12 06:48:25'),
(12493, '', 0.00, 6, '2019-11-12 06:48:25'),
(12494, '', 0.00, 7, '2019-11-12 06:48:25'),
(12495, '\0F\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:48:25'),
(12496, '', 0.00, 7, '2019-11-12 06:48:25'),
(12497, '', 0.00, 6, '2019-11-12 06:48:25'),
(12498, '', 0.00, 7, '2019-11-12 06:48:26'),
(12499, '\0G\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:48:26'),
(12500, '', 0.00, 7, '2019-11-12 06:48:26'),
(12501, '', 0.00, 6, '2019-11-12 06:48:26'),
(12502, '', 0.00, 7, '2019-11-12 06:48:26'),
(12503, '\0H\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:48:26'),
(12504, '', 0.00, 7, '2019-11-12 06:48:26'),
(12505, '', 0.00, 6, '2019-11-12 06:48:26'),
(12506, '', 0.00, 7, '2019-11-12 06:48:26'),
(12507, '\0I\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:48:26'),
(12508, '', 0.00, 7, '2019-11-12 06:48:26'),
(12509, '', 0.00, 6, '2019-11-12 06:48:26'),
(12510, '', 0.00, 7, '2019-11-12 06:48:26'),
(12511, '\0J\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:48:26'),
(12512, '', 0.00, 7, '2019-11-12 06:48:26'),
(12513, '', 0.00, 6, '2019-11-12 06:48:26'),
(12514, '', 0.00, 7, '2019-11-12 06:48:26'),
(12515, '\0K\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:48:26'),
(12516, '', 0.00, 7, '2019-11-12 06:48:26'),
(12517, '', 0.00, 6, '2019-11-12 06:48:26'),
(12518, '', 0.00, 7, '2019-11-12 06:48:26'),
(12519, '\0L\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:48:27'),
(12520, '', 0.00, 7, '2019-11-12 06:48:27'),
(12521, '', 0.00, 6, '2019-11-12 06:48:27'),
(12522, '', 0.00, 7, '2019-11-12 06:48:27'),
(12523, '\0M\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:48:27'),
(12524, '', 0.00, 7, '2019-11-12 06:48:27'),
(12525, '', 0.00, 6, '2019-11-12 06:48:27'),
(12526, '', 0.00, 7, '2019-11-12 06:48:27'),
(12527, '\0N\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:48:27'),
(12528, '', 0.00, 7, '2019-11-12 06:48:27'),
(12529, '', 0.00, 6, '2019-11-12 06:48:27'),
(12530, '', 0.00, 7, '2019-11-12 06:48:27'),
(12531, '\0O\0\0\0O\0\0~', 0.00, 6, '2019-11-12 06:48:27'),
(12532, '', 0.00, 7, '2019-11-12 06:48:27'),
(12533, '\0O\0\0', 0.00, 6, '2019-11-12 06:48:27'),
(12534, '', 0.00, 7, '2019-11-12 06:48:27'),
(12535, '', 0.00, 6, '2019-11-12 06:48:28'),
(12536, '', 0.00, 7, '2019-11-12 06:48:28'),
(12537, '\0P\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:48:28'),
(12538, '', 0.00, 7, '2019-11-12 06:48:28'),
(12539, '', 0.00, 6, '2019-11-12 06:48:28'),
(12540, '', 0.00, 7, '2019-11-12 06:48:28'),
(12541, '\0Q\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:48:28'),
(12542, '\0\0\0', 0.00, 7, '2019-11-12 06:48:29'),
(12543, '', 0.00, 6, '2019-11-12 06:48:29'),
(12544, '\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:29'),
(12545, '\0R\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:48:29'),
(12546, '\0\0\0\0S', 0.00, 7, '2019-11-12 06:48:29'),
(12547, '', 0.00, 6, '2019-11-12 06:48:29'),
(12548, '', 0.00, 7, '2019-11-12 06:48:29'),
(12549, '\0S\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:48:29'),
(12550, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:29'),
(12551, '', 0.00, 6, '2019-11-12 06:48:29'),
(12552, '', 0.00, 7, '2019-11-12 06:48:29'),
(12553, '\0T\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:48:29'),
(12554, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:29'),
(12555, '', 0.00, 6, '2019-11-12 06:48:29'),
(12556, '', 0.00, 7, '2019-11-12 06:48:30'),
(12557, '', 0.00, 6, '2019-11-12 06:48:30'),
(12558, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:30'),
(12559, '\0V\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:48:30'),
(12560, '', 0.00, 7, '2019-11-12 06:48:30'),
(12561, '', 0.00, 6, '2019-11-12 06:48:30'),
(12562, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:30'),
(12563, '\0W\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:48:30'),
(12564, '', 0.00, 7, '2019-11-12 06:48:30'),
(12565, '', 0.00, 6, '2019-11-12 06:48:30'),
(12566, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:30'),
(12567, '\0X\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:48:30'),
(12568, '', 0.00, 7, '2019-11-12 06:48:30'),
(12569, '', 0.00, 6, '2019-11-12 06:48:30'),
(12570, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:30'),
(12571, '\0Y\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:48:30'),
(12572, '\0\0\0R', 0.00, 7, '2019-11-12 06:48:30'),
(12573, '', 0.00, 6, '2019-11-12 06:48:30'),
(12574, '', 0.00, 7, '2019-11-12 06:48:31'),
(12575, '', 0.00, 6, '2019-11-12 06:48:31'),
(12576, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:31'),
(12577, '\0[\0\0\0[\0\0~', 0.00, 6, '2019-11-12 06:48:31'),
(12578, '\0\0\0R', 0.00, 7, '2019-11-12 06:48:31'),
(12579, '', 0.00, 6, '2019-11-12 06:48:31'),
(12580, '', 0.00, 7, '2019-11-12 06:48:31'),
(12581, '\0\\\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:48:31'),
(12582, '\0\0\0\0	\0\0~', 0.00, 7, '2019-11-12 06:48:31'),
(12583, '', 0.00, 6, '2019-11-12 06:48:31'),
(12584, '', 0.00, 7, '2019-11-12 06:48:31'),
(12585, '\0]\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:48:31'),
(12586, '\0	\0\0\0', 0.00, 7, '2019-11-12 06:48:31'),
(12587, '', 0.00, 6, '2019-11-12 06:48:31'),
(12588, '\0\0~', 0.00, 7, '2019-11-12 06:48:31'),
(12589, '\0^\0\0\0^\0\0~', 0.00, 6, '2019-11-12 06:48:32'),
(12590, '', 0.00, 7, '2019-11-12 06:48:32'),
(12591, '\0', 0.00, 7, '2019-11-12 06:48:33'),
(12592, '', 0.00, 6, '2019-11-12 06:48:33'),
(12593, '\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:33'),
(12594, '', 0.00, 6, '2019-11-12 06:48:34'),
(12595, '\0', 0.00, 7, '2019-11-12 06:48:34'),
(12596, '', 0.00, 7, '2019-11-12 06:48:34'),
(12597, '\0`\0\0\0`\0\0~', 0.00, 6, '2019-11-12 06:48:34'),
(12598, '', 0.00, 6, '2019-11-12 06:48:34'),
(12599, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:34'),
(12600, '\0a\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:48:34'),
(12601, '', 0.00, 7, '2019-11-12 06:48:34'),
(12602, '', 0.00, 6, '2019-11-12 06:48:34'),
(12603, '\0\0\0\0\r\0\0~', 0.00, 7, '2019-11-12 06:48:34'),
(12604, '\0b\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:48:34'),
(12605, '', 0.00, 7, '2019-11-12 06:48:34'),
(12606, '\0b\0\0', 0.00, 6, '2019-11-12 06:48:35'),
(12607, '\0\r\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:35'),
(12608, '', 0.00, 6, '2019-11-12 06:48:35'),
(12609, '', 0.00, 7, '2019-11-12 06:48:35'),
(12610, '\0c\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:48:35'),
(12611, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:35'),
(12612, '', 0.00, 6, '2019-11-12 06:48:35'),
(12613, '', 0.00, 7, '2019-11-12 06:48:35'),
(12614, '\0d\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:48:36'),
(12615, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:36'),
(12616, '', 0.00, 6, '2019-11-12 06:48:36');
INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(12617, '', 0.00, 7, '2019-11-12 06:48:36'),
(12618, '\0e\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:48:36'),
(12619, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:36'),
(12620, '', 0.00, 6, '2019-11-12 06:48:36'),
(12621, '', 0.00, 7, '2019-11-12 06:48:36'),
(12622, '\0f\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:48:36'),
(12623, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:37'),
(12624, '', 0.00, 6, '2019-11-12 06:48:37'),
(12625, '', 0.00, 7, '2019-11-12 06:48:37'),
(12626, '\0g\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:48:37'),
(12627, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:37'),
(12628, '', 0.00, 6, '2019-11-12 06:48:37'),
(12629, '', 0.00, 7, '2019-11-12 06:48:37'),
(12630, '\0h\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:48:37'),
(12631, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:37'),
(12632, '', 0.00, 6, '2019-11-12 06:48:37'),
(12633, '', 0.00, 7, '2019-11-12 06:48:37'),
(12634, '\0i\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:48:37'),
(12635, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:38'),
(12636, '', 0.00, 6, '2019-11-12 06:48:38'),
(12637, '', 0.00, 7, '2019-11-12 06:48:38'),
(12638, '\0j\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:48:38'),
(12639, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:38'),
(12640, '', 0.00, 6, '2019-11-12 06:48:38'),
(12641, '', 0.00, 7, '2019-11-12 06:48:38'),
(12642, '\0k\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:48:38'),
(12643, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:38'),
(12644, '\0k\0\0', 0.00, 6, '2019-11-12 06:48:38'),
(12645, '', 0.00, 7, '2019-11-12 06:48:38'),
(12646, '', 0.00, 6, '2019-11-12 06:48:38'),
(12647, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:38'),
(12648, '\0l\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:48:38'),
(12649, '', 0.00, 7, '2019-11-12 06:48:38'),
(12650, '', 0.00, 6, '2019-11-12 06:48:38'),
(12651, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:38'),
(12652, '\0m\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:48:39'),
(12653, '', 0.00, 7, '2019-11-12 06:48:39'),
(12654, '', 0.00, 6, '2019-11-12 06:48:39'),
(12655, '\0\0\0\0\Z\0\0~', 0.00, 7, '2019-11-12 06:48:39'),
(12656, '\0n\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:48:39'),
(12657, '', 0.00, 7, '2019-11-12 06:48:39'),
(12658, '', 0.00, 6, '2019-11-12 06:48:39'),
(12659, '\0\Z\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:39'),
(12660, '\0o\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:48:39'),
(12661, '', 0.00, 7, '2019-11-12 06:48:39'),
(12662, '', 0.00, 6, '2019-11-12 06:48:39'),
(12663, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:39'),
(12664, '\0p\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:48:39'),
(12665, '', 0.00, 7, '2019-11-12 06:48:39'),
(12666, '', 0.00, 6, '2019-11-12 06:48:39'),
(12667, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:39'),
(12668, '', 0.00, 6, '2019-11-12 06:48:39'),
(12669, '', 0.00, 7, '2019-11-12 06:48:39'),
(12670, '\0r\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:48:39'),
(12671, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:39'),
(12672, '', 0.00, 6, '2019-11-12 06:48:39'),
(12673, '', 0.00, 7, '2019-11-12 06:48:39'),
(12674, '\0s\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:48:40'),
(12675, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:48:40'),
(12676, '', 0.00, 6, '2019-11-12 06:48:40'),
(12677, '', 0.00, 7, '2019-11-12 06:48:40'),
(12678, '\0t\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:48:40'),
(12679, '\0\0\0\0 \0\0~', 0.00, 7, '2019-11-12 06:48:40'),
(12680, '', 0.00, 6, '2019-11-12 06:48:40'),
(12681, '', 0.00, 7, '2019-11-12 06:48:40'),
(12682, '\0u\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:48:40'),
(12683, '\0 \0\0\0!\0\0~', 0.00, 7, '2019-11-12 06:48:40'),
(12684, '', 0.00, 6, '2019-11-12 06:48:40'),
(12685, '', 0.00, 7, '2019-11-12 06:48:40'),
(12686, '\0v\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:48:40'),
(12687, '\0!\0\0\0&quot;\0\0~', 0.00, 7, '2019-11-12 06:48:40'),
(12688, '', 0.00, 6, '2019-11-12 06:48:40'),
(12689, '', 0.00, 7, '2019-11-12 06:48:40'),
(12690, '\0w\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:48:40'),
(12691, '\0&quot;\0\0\0#\0\0~', 0.00, 7, '2019-11-12 06:48:40'),
(12692, '', 0.00, 6, '2019-11-12 06:48:40'),
(12693, '', 0.00, 7, '2019-11-12 06:48:41'),
(12694, '\0x\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:48:41'),
(12695, '\0#\0\0\0$\0\0~', 0.00, 7, '2019-11-12 06:48:41'),
(12696, '', 0.00, 6, '2019-11-12 06:48:41'),
(12697, '', 0.00, 7, '2019-11-12 06:48:41'),
(12698, '\0y\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:48:41'),
(12699, '\0$\0\0\0%\0\0~', 0.00, 7, '2019-11-12 06:48:41'),
(12700, '', 0.00, 6, '2019-11-12 06:48:41'),
(12701, '', 0.00, 7, '2019-11-12 06:48:41'),
(12702, '\0z\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:48:41'),
(12703, '\0%\0\0\0&amp;\0\0~', 0.00, 7, '2019-11-12 06:48:41'),
(12704, '', 0.00, 6, '2019-11-12 06:48:41'),
(12705, '', 0.00, 7, '2019-11-12 06:48:41'),
(12706, '\0{\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:48:41'),
(12707, '\0&amp;\0\0\0\'\0\0~', 0.00, 7, '2019-11-12 06:48:42'),
(12708, '', 0.00, 6, '2019-11-12 06:48:42'),
(12709, '', 0.00, 7, '2019-11-12 06:48:42'),
(12710, '\0|\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:48:42'),
(12711, '\0\'\0\0\0(\0\0~', 0.00, 7, '2019-11-12 06:48:42'),
(12712, '', 0.00, 6, '2019-11-12 06:48:42'),
(12713, '\0\'\0\0R', 0.00, 7, '2019-11-12 06:48:42'),
(12714, '\0}\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:48:42'),
(12715, '', 0.00, 7, '2019-11-12 06:48:42'),
(12716, '\0}\0\0S', 0.00, 6, '2019-11-12 06:48:42'),
(12717, '\0(\0\0\0)\0\0~', 0.00, 7, '2019-11-12 06:48:42'),
(12718, '', 0.00, 6, '2019-11-12 06:48:42'),
(12719, '', 0.00, 7, '2019-11-12 06:48:42'),
(12720, '\0~\0\0\0~\0\0~', 0.00, 6, '2019-11-12 06:48:42'),
(12721, '\0)\0\0\0*\0\0~', 0.00, 7, '2019-11-12 06:48:42'),
(12722, '', 0.00, 6, '2019-11-12 06:48:42'),
(12723, '', 0.00, 7, '2019-11-12 06:48:42'),
(12724, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:48:42'),
(12725, '', 0.00, 7, '2019-11-12 06:48:42'),
(12726, '', 0.00, 6, '2019-11-12 06:48:43'),
(12727, '\0+\0\0\0', 0.00, 7, '2019-11-12 06:48:43'),
(12728, '', 0.00, 6, '2019-11-12 06:48:43'),
(12729, '', 0.00, 7, '2019-11-12 06:48:43'),
(12730, '', 0.00, 6, '2019-11-12 06:48:43'),
(12731, '\0', 0.00, 7, '2019-11-12 06:48:43'),
(12732, '', 0.00, 6, '2019-11-12 06:48:43'),
(12733, '\0', 0.00, 7, '2019-11-12 06:48:43'),
(12734, '', 0.00, 6, '2019-11-12 06:48:43'),
(12735, '\0-\0\0\0.\0\0~', 0.00, 7, '2019-11-12 06:48:43'),
(12736, '', 0.00, 6, '2019-11-12 06:48:43'),
(12737, '', 0.00, 7, '2019-11-12 06:48:43'),
(12738, '', 0.00, 6, '2019-11-12 06:48:43'),
(12739, '\0.\0\0\0/\0\0~', 0.00, 7, '2019-11-12 06:48:43'),
(12740, '', 0.00, 6, '2019-11-12 06:48:43'),
(12741, '', 0.00, 7, '2019-11-12 06:48:43'),
(12742, '', 0.00, 6, '2019-11-12 06:48:43'),
(12743, '\0/\0\0\00\0\0~', 0.00, 7, '2019-11-12 06:48:44'),
(12744, '', 0.00, 6, '2019-11-12 06:48:44'),
(12745, '', 0.00, 7, '2019-11-12 06:48:44'),
(12746, '', 0.00, 6, '2019-11-12 06:48:44'),
(12747, '\00\0\0\01\0\0~', 0.00, 7, '2019-11-12 06:48:44'),
(12748, '', 0.00, 6, '2019-11-12 06:48:44'),
(12749, '', 0.00, 7, '2019-11-12 06:48:44'),
(12750, '', 0.00, 6, '2019-11-12 06:48:44'),
(12751, '\01\0\0\02\0\0~', 0.00, 7, '2019-11-12 06:48:44'),
(12752, '', 0.00, 6, '2019-11-12 06:48:44'),
(12753, '', 0.00, 7, '2019-11-12 06:48:44'),
(12754, '', 0.00, 6, '2019-11-12 06:48:44'),
(12755, '\02\0\0\03\0\0~', 0.00, 7, '2019-11-12 06:48:45'),
(12756, '', 0.00, 6, '2019-11-12 06:48:45'),
(12757, '', 0.00, 7, '2019-11-12 06:48:45'),
(12758, '', 0.00, 6, '2019-11-12 06:48:45'),
(12759, '\03\0\0\04\0\0~', 0.00, 7, '2019-11-12 06:48:45'),
(12760, '', 0.00, 6, '2019-11-12 06:48:45'),
(12761, '', 0.00, 7, '2019-11-12 06:48:46'),
(12762, '', 0.00, 6, '2019-11-12 06:48:46'),
(12763, '\04\0\0\05\0\0~', 0.00, 7, '2019-11-12 06:48:46'),
(12764, '', 0.00, 6, '2019-11-12 06:48:46'),
(12765, '', 0.00, 7, '2019-11-12 06:48:46'),
(12766, '', 0.00, 6, '2019-11-12 06:48:46'),
(12767, '\05\0\0\06\0\0~', 0.00, 7, '2019-11-12 06:48:46'),
(12768, '', 0.00, 6, '2019-11-12 06:48:46'),
(12769, '', 0.00, 7, '2019-11-12 06:48:46'),
(12770, '', 0.00, 6, '2019-11-12 06:48:46'),
(12771, '\06\0\0\07\0\0~', 0.00, 7, '2019-11-12 06:48:46'),
(12772, '', 0.00, 6, '2019-11-12 06:48:46'),
(12773, '', 0.00, 7, '2019-11-12 06:48:46'),
(12774, '', 0.00, 6, '2019-11-12 06:48:46'),
(12775, '\07\0\0\08\0\0~', 0.00, 7, '2019-11-12 06:48:46'),
(12776, '', 0.00, 6, '2019-11-12 06:48:46'),
(12777, '', 0.00, 7, '2019-11-12 06:48:47'),
(12778, '', 0.00, 6, '2019-11-12 06:48:47'),
(12779, '\08\0\0\09\0\0~', 0.00, 7, '2019-11-12 06:48:47'),
(12780, '', 0.00, 6, '2019-11-12 06:48:47'),
(12781, '', 0.00, 7, '2019-11-12 06:48:47'),
(12782, '', 0.00, 6, '2019-11-12 06:48:47'),
(12783, '\09\0\0\0:\0\0~', 0.00, 7, '2019-11-12 06:48:47'),
(12784, '', 0.00, 6, '2019-11-12 06:48:47'),
(12785, '', 0.00, 7, '2019-11-12 06:48:47'),
(12786, '', 0.00, 6, '2019-11-12 06:48:47'),
(12787, '\0:\0\0\0;\0\0~', 0.00, 7, '2019-11-12 06:48:47'),
(12788, '', 0.00, 6, '2019-11-12 06:48:47'),
(12789, '', 0.00, 7, '2019-11-12 06:48:47'),
(12790, '', 0.00, 6, '2019-11-12 06:48:47'),
(12791, '\0;\0\0\0&lt;\0\0~', 0.00, 7, '2019-11-12 06:48:47'),
(12792, '', 0.00, 6, '2019-11-12 06:48:47'),
(12793, '\0;\0\0', 0.00, 7, '2019-11-12 06:48:47'),
(12794, '', 0.00, 6, '2019-11-12 06:48:47'),
(12795, '', 0.00, 7, '2019-11-12 06:48:47'),
(12796, '', 0.00, 6, '2019-11-12 06:48:47'),
(12797, '\0&lt;\0\0\0=\0\0~', 0.00, 7, '2019-11-12 06:48:48'),
(12798, '', 0.00, 6, '2019-11-12 06:48:48'),
(12799, '', 0.00, 7, '2019-11-12 06:48:48'),
(12800, '', 0.00, 6, '2019-11-12 06:48:48'),
(12801, '\0=\0\0\0&gt;\0\0~', 0.00, 7, '2019-11-12 06:48:48'),
(12802, '', 0.00, 6, '2019-11-12 06:48:48'),
(12803, '', 0.00, 7, '2019-11-12 06:48:48'),
(12804, '', 0.00, 6, '2019-11-12 06:48:48'),
(12805, '\0&gt;\0\0\0?\0\0~', 0.00, 7, '2019-11-12 06:48:48'),
(12806, '', 0.00, 6, '2019-11-12 06:48:48'),
(12807, '', 0.00, 7, '2019-11-12 06:48:48'),
(12808, '', 0.00, 6, '2019-11-12 06:48:48'),
(12809, '\0?\0\0\0@\0\0~', 0.00, 7, '2019-11-12 06:48:48'),
(12810, '', 0.00, 6, '2019-11-12 06:48:48'),
(12811, '', 0.00, 7, '2019-11-12 06:48:48'),
(12812, '', 0.00, 6, '2019-11-12 06:48:48'),
(12813, '\0@\0\0\0A\0\0~', 0.00, 7, '2019-11-12 06:48:48'),
(12814, '', 0.00, 6, '2019-11-12 06:48:49'),
(12815, '', 0.00, 7, '2019-11-12 06:48:49'),
(12816, '', 0.00, 6, '2019-11-12 06:48:49'),
(12817, '\0A\0\0\0B\0\0~', 0.00, 7, '2019-11-12 06:48:49'),
(12818, '', 0.00, 6, '2019-11-12 06:48:49'),
(12819, '', 0.00, 7, '2019-11-12 06:48:49'),
(12820, '', 0.00, 6, '2019-11-12 06:48:49'),
(12821, '\0B\0\0\0C\0\0~', 0.00, 7, '2019-11-12 06:48:49'),
(12822, '', 0.00, 6, '2019-11-12 06:48:49'),
(12823, '', 0.00, 7, '2019-11-12 06:48:49'),
(12824, '', 0.00, 6, '2019-11-12 06:48:49'),
(12825, '\0C\0\0\0D\0\0~', 0.00, 7, '2019-11-12 06:48:49'),
(12826, '', 0.00, 6, '2019-11-12 06:48:49'),
(12827, '', 0.00, 7, '2019-11-12 06:48:49'),
(12828, '', 0.00, 6, '2019-11-12 06:48:50'),
(12829, '\0D\0\0\0E\0\0~', 0.00, 7, '2019-11-12 06:48:50'),
(12830, '', 0.00, 6, '2019-11-12 06:48:50'),
(12831, '', 0.00, 7, '2019-11-12 06:48:50'),
(12832, '', 0.00, 6, '2019-11-12 06:48:50'),
(12833, '\0E\0\0\0F\0\0~', 0.00, 7, '2019-11-12 06:48:50'),
(12834, '', 0.00, 6, '2019-11-12 06:48:50'),
(12835, '', 0.00, 7, '2019-11-12 06:48:50'),
(12836, '', 0.00, 6, '2019-11-12 06:48:50'),
(12837, '\0F\0\0\0G\0\0~', 0.00, 7, '2019-11-12 06:48:50'),
(12838, '', 0.00, 6, '2019-11-12 06:48:50'),
(12839, '', 0.00, 7, '2019-11-12 06:48:50'),
(12840, '', 0.00, 6, '2019-11-12 06:48:50'),
(12841, '\0G\0\0\0H\0\0~', 0.00, 7, '2019-11-12 06:48:50'),
(12842, '', 0.00, 6, '2019-11-12 06:48:50'),
(12843, '', 0.00, 7, '2019-11-12 06:48:50'),
(12844, '', 0.00, 6, '2019-11-12 06:48:50'),
(12845, '\0H\0\0\0I\0\0~', 0.00, 7, '2019-11-12 06:48:50'),
(12846, '', 0.00, 6, '2019-11-12 06:48:50'),
(12847, '', 0.00, 7, '2019-11-12 06:48:50'),
(12848, '', 0.00, 6, '2019-11-12 06:48:50'),
(12849, '\0I\0\0\0J\0\0~', 0.00, 7, '2019-11-12 06:48:50'),
(12850, '', 0.00, 6, '2019-11-12 06:48:50'),
(12851, '', 0.00, 7, '2019-11-12 06:48:51'),
(12852, '', 0.00, 6, '2019-11-12 06:48:51'),
(12853, '\0J\0\0\0K\0\0~', 0.00, 7, '2019-11-12 06:48:51'),
(12854, '', 0.00, 6, '2019-11-12 06:48:51'),
(12855, '', 0.00, 7, '2019-11-12 06:48:51'),
(12856, '', 0.00, 6, '2019-11-12 06:48:51'),
(12857, '\0K\0\0\0L\0\0~', 0.00, 7, '2019-11-12 06:48:51'),
(12858, '', 0.00, 6, '2019-11-12 06:48:51'),
(12859, '', 0.00, 7, '2019-11-12 06:48:51'),
(12860, '', 0.00, 6, '2019-11-12 06:48:51'),
(12861, '\0L\0\0\0M\0\0~', 0.00, 7, '2019-11-12 06:48:51'),
(12862, '', 0.00, 6, '2019-11-12 06:48:51'),
(12863, '', 0.00, 7, '2019-11-12 06:48:51'),
(12864, '', 0.00, 6, '2019-11-12 06:48:51'),
(12865, '\0M\0\0\0N\0\0~', 0.00, 7, '2019-11-12 06:48:51'),
(12866, '', 0.00, 6, '2019-11-12 06:48:51'),
(12867, '', 0.00, 7, '2019-11-12 06:48:51'),
(12868, '', 0.00, 6, '2019-11-12 06:48:51'),
(12869, '', 0.00, 7, '2019-11-12 06:48:51'),
(12870, '', 0.00, 6, '2019-11-12 06:48:52'),
(12871, '\0O\0\0\0P\0\0~', 0.00, 7, '2019-11-12 06:48:52'),
(12872, '', 0.00, 6, '2019-11-12 06:48:52'),
(12873, '', 0.00, 7, '2019-11-12 06:48:52'),
(12874, '', 0.00, 6, '2019-11-12 06:48:52'),
(12875, '\0P\0\0\0Q\0\0~', 0.00, 7, '2019-11-12 06:48:52'),
(12876, '', 0.00, 6, '2019-11-12 06:48:52'),
(12877, '', 0.00, 7, '2019-11-12 06:48:52'),
(12878, '', 0.00, 6, '2019-11-12 06:48:52'),
(12879, '\0Q\0\0\0R\0\0~', 0.00, 7, '2019-11-12 06:48:52'),
(12880, '', 0.00, 6, '2019-11-12 06:48:52'),
(12881, '', 0.00, 7, '2019-11-12 06:48:52'),
(12882, '', 0.00, 6, '2019-11-12 06:48:52'),
(12883, '\0R\0\0\0S\0\0~', 0.00, 7, '2019-11-12 06:48:52'),
(12884, '', 0.00, 6, '2019-11-12 06:48:52'),
(12885, '', 0.00, 7, '2019-11-12 06:48:52'),
(12886, '', 0.00, 6, '2019-11-12 06:48:52'),
(12887, '\0S\0\0\0T\0\0~', 0.00, 7, '2019-11-12 06:48:53'),
(12888, '', 0.00, 6, '2019-11-12 06:48:53'),
(12889, '', 0.00, 7, '2019-11-12 06:48:53'),
(12890, '', 0.00, 6, '2019-11-12 06:48:53'),
(12891, '\0T\0\0\0U\0\0~', 0.00, 7, '2019-11-12 06:48:53'),
(12892, '', 0.00, 6, '2019-11-12 06:48:53'),
(12893, '', 0.00, 7, '2019-11-12 06:48:53'),
(12894, '', 0.00, 6, '2019-11-12 06:48:53'),
(12895, '\0U\0\0\0V\0\0~', 0.00, 7, '2019-11-12 06:48:53'),
(12896, '', 0.00, 6, '2019-11-12 06:48:53'),
(12897, '', 0.00, 7, '2019-11-12 06:48:53'),
(12898, '', 0.00, 6, '2019-11-12 06:48:53'),
(12899, '\0V\0\0\0W\0\0~', 0.00, 7, '2019-11-12 06:48:53'),
(12900, '', 0.00, 6, '2019-11-12 06:48:53'),
(12901, '', 0.00, 7, '2019-11-12 06:48:54'),
(12902, '', 0.00, 6, '2019-11-12 06:48:54'),
(12903, '\0W\0\0\0X\0\0~', 0.00, 7, '2019-11-12 06:48:54'),
(12904, '', 0.00, 6, '2019-11-12 06:48:54'),
(12905, '', 0.00, 7, '2019-11-12 06:48:54'),
(12906, '', 0.00, 6, '2019-11-12 06:48:54'),
(12907, '\0X\0\0\0Y\0\0~', 0.00, 7, '2019-11-12 06:48:54'),
(12908, '', 0.00, 6, '2019-11-12 06:48:54'),
(12909, '', 0.00, 7, '2019-11-12 06:48:54'),
(12910, '', 0.00, 6, '2019-11-12 06:48:54'),
(12911, '\0Y\0\0\0Z\0\0~', 0.00, 7, '2019-11-12 06:48:54'),
(12912, '', 0.00, 6, '2019-11-12 06:48:54'),
(12913, '', 0.00, 7, '2019-11-12 06:48:54'),
(12914, '', 0.00, 6, '2019-11-12 06:48:54'),
(12915, '\0Z\0\0\0[\0\0~', 0.00, 7, '2019-11-12 06:48:54'),
(12916, '', 0.00, 6, '2019-11-12 06:48:54'),
(12917, '', 0.00, 7, '2019-11-12 06:48:54'),
(12918, '', 0.00, 6, '2019-11-12 06:48:54'),
(12919, '\0[\0\0\0\\\0\0~', 0.00, 7, '2019-11-12 06:48:54'),
(12920, '', 0.00, 6, '2019-11-12 06:48:54'),
(12921, '', 0.00, 7, '2019-11-12 06:48:55'),
(12922, '', 0.00, 6, '2019-11-12 06:48:55'),
(12923, '\0\\\0\0\0]\0\0~', 0.00, 7, '2019-11-12 06:48:55'),
(12924, '', 0.00, 6, '2019-11-12 06:48:55'),
(12925, '\0\\\0\0', 0.00, 7, '2019-11-12 06:48:55'),
(12926, '', 0.00, 6, '2019-11-12 06:48:55'),
(12927, '', 0.00, 7, '2019-11-12 06:48:55'),
(12928, '', 0.00, 6, '2019-11-12 06:48:55'),
(12929, '\0]\0\0\0^\0\0~', 0.00, 7, '2019-11-12 06:48:55'),
(12930, '', 0.00, 6, '2019-11-12 06:48:55'),
(12931, '', 0.00, 7, '2019-11-12 06:48:55'),
(12932, '', 0.00, 6, '2019-11-12 06:48:55'),
(12933, '\0^\0\0\0_\0\0~', 0.00, 7, '2019-11-12 06:48:55'),
(12934, '', 0.00, 6, '2019-11-12 06:48:55'),
(12935, '', 0.00, 7, '2019-11-12 06:48:55'),
(12936, '', 0.00, 6, '2019-11-12 06:48:55'),
(12937, '\0_\0\0\0`\0\0~', 0.00, 7, '2019-11-12 06:48:55'),
(12938, '', 0.00, 6, '2019-11-12 06:48:55'),
(12939, '\0_\0\0B)\0\0\0`\0\0\0', 0.00, 7, '2019-11-12 06:48:55'),
(12940, '', 0.00, 6, '2019-11-12 06:48:55'),
(12941, '\0`\0\0\0a\0\0~', 0.00, 7, '2019-11-12 06:48:55'),
(12942, '', 0.00, 6, '2019-11-12 06:48:55'),
(12943, '', 0.00, 7, '2019-11-12 06:48:56'),
(12944, '', 0.00, 6, '2019-11-12 06:48:56'),
(12945, '\0a\0\0\0b\0\0~', 0.00, 7, '2019-11-12 06:48:56'),
(12946, '', 0.00, 6, '2019-11-12 06:48:56'),
(12947, '', 0.00, 7, '2019-11-12 06:48:56'),
(12948, '', 0.00, 6, '2019-11-12 06:48:56'),
(12949, '\0b\0\0\0c\0\0~', 0.00, 7, '2019-11-12 06:48:56'),
(12950, '', 0.00, 6, '2019-11-12 06:48:56'),
(12951, '', 0.00, 7, '2019-11-12 06:48:56'),
(12952, '', 0.00, 6, '2019-11-12 06:48:57'),
(12953, '\0c\0\0\0d\0\0~', 0.00, 7, '2019-11-12 06:48:57'),
(12954, '', 0.00, 6, '2019-11-12 06:48:57'),
(12955, '', 0.00, 7, '2019-11-12 06:48:57'),
(12956, '', 0.00, 6, '2019-11-12 06:48:57'),
(12957, '\0d\0\0\0e\0\0~', 0.00, 7, '2019-11-12 06:48:57'),
(12958, '', 0.00, 6, '2019-11-12 06:48:57'),
(12959, '', 0.00, 7, '2019-11-12 06:48:57'),
(12960, '', 0.00, 6, '2019-11-12 06:48:57'),
(12961, '\0e\0\0\0f\0\0~', 0.00, 7, '2019-11-12 06:48:57'),
(12962, '', 0.00, 6, '2019-11-12 06:48:58'),
(12963, '', 0.00, 7, '2019-11-12 06:48:58'),
(12964, '', 0.00, 6, '2019-11-12 06:48:58'),
(12965, '\0f\0\0\0g\0\0~', 0.00, 7, '2019-11-12 06:48:58'),
(12966, '', 0.00, 6, '2019-11-12 06:48:58'),
(12967, '', 0.00, 7, '2019-11-12 06:48:58'),
(12968, '', 0.00, 6, '2019-11-12 06:48:58'),
(12969, '\0g\0\0\0h\0\0~', 0.00, 7, '2019-11-12 06:48:58'),
(12970, '', 0.00, 6, '2019-11-12 06:48:58'),
(12971, '', 0.00, 7, '2019-11-12 06:48:58'),
(12972, '', 0.00, 6, '2019-11-12 06:48:58'),
(12973, '\0h\0\0\0i\0\0~', 0.00, 7, '2019-11-12 06:48:58'),
(12974, '', 0.00, 6, '2019-11-12 06:48:59'),
(12975, '', 0.00, 7, '2019-11-12 06:48:59'),
(12976, '', 0.00, 6, '2019-11-12 06:48:59'),
(12977, '\0i\0\0\0j\0\0~', 0.00, 7, '2019-11-12 06:48:59'),
(12978, '', 0.00, 6, '2019-11-12 06:48:59'),
(12979, '', 0.00, 7, '2019-11-12 06:48:59'),
(12980, '', 0.00, 6, '2019-11-12 06:48:59'),
(12981, '\0j\0\0\0k\0\0~', 0.00, 7, '2019-11-12 06:48:59'),
(12982, '', 0.00, 6, '2019-11-12 06:48:59'),
(12983, '', 0.00, 7, '2019-11-12 06:48:59'),
(12984, '', 0.00, 6, '2019-11-12 06:48:59'),
(12985, '\0k\0\0\0l\0\0~', 0.00, 7, '2019-11-12 06:48:59'),
(12986, '', 0.00, 6, '2019-11-12 06:48:59'),
(12987, '', 0.00, 7, '2019-11-12 06:48:59'),
(12988, '', 0.00, 6, '2019-11-12 06:48:59'),
(12989, '\0l\0\0\0m\0\0~', 0.00, 7, '2019-11-12 06:48:59'),
(12990, '', 0.00, 6, '2019-11-12 06:48:59'),
(12991, '', 0.00, 7, '2019-11-12 06:48:59'),
(12992, '', 0.00, 6, '2019-11-12 06:48:59'),
(12993, '\0m\0\0\0n\0\0~', 0.00, 7, '2019-11-12 06:48:59'),
(12994, '', 0.00, 6, '2019-11-12 06:49:00'),
(12995, '', 0.00, 7, '2019-11-12 06:49:00'),
(12996, '', 0.00, 6, '2019-11-12 06:49:00'),
(12997, '\0n\0\0\0o\0\0~', 0.00, 7, '2019-11-12 06:49:00'),
(12998, '', 0.00, 6, '2019-11-12 06:49:00'),
(12999, '', 0.00, 7, '2019-11-12 06:49:00'),
(13000, '', 0.00, 6, '2019-11-12 06:49:00'),
(13001, '\0o\0\0\0p\0\0~', 0.00, 7, '2019-11-12 06:49:00'),
(13002, '', 0.00, 6, '2019-11-12 06:49:00'),
(13003, '', 0.00, 7, '2019-11-12 06:49:00'),
(13004, '', 0.00, 6, '2019-11-12 06:49:00'),
(13005, '\0p\0\0\0q\0\0~', 0.00, 7, '2019-11-12 06:49:00'),
(13006, '', 0.00, 6, '2019-11-12 06:49:00'),
(13007, '', 0.00, 7, '2019-11-12 06:49:00'),
(13008, '', 0.00, 6, '2019-11-12 06:49:00'),
(13009, '\0q\0\0\0r\0\0~', 0.00, 7, '2019-11-12 06:49:00'),
(13010, '', 0.00, 6, '2019-11-12 06:49:00'),
(13011, '', 0.00, 7, '2019-11-12 06:49:00'),
(13012, '', 0.00, 6, '2019-11-12 06:49:00'),
(13013, '\0r\0\0\0s\0\0~', 0.00, 7, '2019-11-12 06:49:00'),
(13014, '', 0.00, 6, '2019-11-12 06:49:00'),
(13015, '', 0.00, 7, '2019-11-12 06:49:00'),
(13016, '', 0.00, 6, '2019-11-12 06:49:01'),
(13017, '\0s\0\0\0t\0\0~', 0.00, 7, '2019-11-12 06:49:01'),
(13018, '', 0.00, 6, '2019-11-12 06:49:01'),
(13019, '', 0.00, 7, '2019-11-12 06:49:01'),
(13020, '', 0.00, 6, '2019-11-12 06:49:01'),
(13021, '\0t\0\0\0u\0\0~', 0.00, 7, '2019-11-12 06:49:01'),
(13022, '', 0.00, 6, '2019-11-12 06:49:01'),
(13023, '', 0.00, 7, '2019-11-12 06:49:01'),
(13024, '', 0.00, 6, '2019-11-12 06:49:01'),
(13025, '\0u\0\0\0v\0\0~', 0.00, 7, '2019-11-12 06:49:01'),
(13026, '', 0.00, 6, '2019-11-12 06:49:01'),
(13027, '', 0.00, 7, '2019-11-12 06:49:01'),
(13028, '', 0.00, 6, '2019-11-12 06:49:01'),
(13029, '\0v\0\0\0w\0\0~', 0.00, 7, '2019-11-12 06:49:01'),
(13030, '', 0.00, 6, '2019-11-12 06:49:02'),
(13031, '', 0.00, 7, '2019-11-12 06:49:02'),
(13032, '', 0.00, 6, '2019-11-12 06:49:02'),
(13033, '\0w\0\0\0x\0\0~', 0.00, 7, '2019-11-12 06:49:02'),
(13034, '', 0.00, 6, '2019-11-12 06:49:02'),
(13035, '', 0.00, 7, '2019-11-12 06:49:02'),
(13036, '', 0.00, 6, '2019-11-12 06:49:02'),
(13037, '\0x\0\0\0y\0\0~', 0.00, 7, '2019-11-12 06:49:02'),
(13038, '', 0.00, 6, '2019-11-12 06:49:02'),
(13039, '', 0.00, 7, '2019-11-12 06:49:02'),
(13040, '', 0.00, 6, '2019-11-12 06:49:02'),
(13041, '\0y\0\0\0z\0\0~', 0.00, 7, '2019-11-12 06:49:02'),
(13042, '', 0.00, 6, '2019-11-12 06:49:02'),
(13043, '', 0.00, 7, '2019-11-12 06:49:02'),
(13044, '', 0.00, 6, '2019-11-12 06:49:02'),
(13045, '\0z\0\0\0{\0\0~', 0.00, 7, '2019-11-12 06:49:02'),
(13046, '', 0.00, 6, '2019-11-12 06:49:02'),
(13047, '', 0.00, 7, '2019-11-12 06:49:02'),
(13048, '', 0.00, 6, '2019-11-12 06:49:03'),
(13049, '\0{\0\0\0|\0\0~', 0.00, 7, '2019-11-12 06:49:03'),
(13050, '', 0.00, 6, '2019-11-12 06:49:03'),
(13051, '\0{\0\0', 0.00, 7, '2019-11-12 06:49:03'),
(13052, '', 0.00, 6, '2019-11-12 06:49:03'),
(13053, '', 0.00, 7, '2019-11-12 06:49:03'),
(13054, '', 0.00, 6, '2019-11-12 06:49:03'),
(13055, '\0|\0\0\0}\0\0~', 0.00, 7, '2019-11-12 06:49:04'),
(13056, '', 0.00, 6, '2019-11-12 06:49:04'),
(13057, '', 0.00, 7, '2019-11-12 06:49:04'),
(13058, '', 0.00, 6, '2019-11-12 06:49:04'),
(13059, '\0}\0\0\0~\0\0~', 0.00, 7, '2019-11-12 06:49:04'),
(13060, '', 0.00, 6, '2019-11-12 06:49:04'),
(13061, '', 0.00, 7, '2019-11-12 06:49:04'),
(13062, '', 0.00, 6, '2019-11-12 06:49:04'),
(13063, '\0~\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:05'),
(13064, '', 0.00, 6, '2019-11-12 06:49:05'),
(13065, '', 0.00, 7, '2019-11-12 06:49:05'),
(13066, '', 0.00, 6, '2019-11-12 06:49:05'),
(13067, '', 0.00, 7, '2019-11-12 06:49:05'),
(13068, '', 0.00, 6, '2019-11-12 06:49:05'),
(13069, '', 0.00, 7, '2019-11-12 06:49:05'),
(13070, '', 0.00, 6, '2019-11-12 06:49:05'),
(13071, '', 0.00, 7, '2019-11-12 06:49:05'),
(13072, '', 0.00, 6, '2019-11-12 06:49:05'),
(13073, '', 0.00, 7, '2019-11-12 06:49:05'),
(13074, '', 0.00, 6, '2019-11-12 06:49:05'),
(13075, '', 0.00, 7, '2019-11-12 06:49:06'),
(13076, '', 0.00, 6, '2019-11-12 06:49:06'),
(13077, '', 0.00, 7, '2019-11-12 06:49:06'),
(13078, '', 0.00, 6, '2019-11-12 06:49:06'),
(13079, '', 0.00, 7, '2019-11-12 06:49:06'),
(13080, '', 0.00, 6, '2019-11-12 06:49:06'),
(13081, '', 0.00, 7, '2019-11-12 06:49:06'),
(13082, '', 0.00, 6, '2019-11-12 06:49:06'),
(13083, '', 0.00, 7, '2019-11-12 06:49:06'),
(13084, '', 0.00, 6, '2019-11-12 06:49:06'),
(13085, '', 0.00, 7, '2019-11-12 06:49:06'),
(13086, '', 0.00, 6, '2019-11-12 06:49:06'),
(13087, '', 0.00, 7, '2019-11-12 06:49:06'),
(13088, '', 0.00, 6, '2019-11-12 06:49:06'),
(13089, '', 0.00, 7, '2019-11-12 06:49:06'),
(13090, '', 0.00, 6, '2019-11-12 06:49:06'),
(13091, '', 0.00, 7, '2019-11-12 06:49:06'),
(13092, '', 0.00, 6, '2019-11-12 06:49:06'),
(13093, '', 0.00, 7, '2019-11-12 06:49:06'),
(13094, '', 0.00, 6, '2019-11-12 06:49:06'),
(13095, '', 0.00, 7, '2019-11-12 06:49:06'),
(13096, '', 0.00, 6, '2019-11-12 06:49:06'),
(13097, '', 0.00, 7, '2019-11-12 06:49:06'),
(13098, '', 0.00, 6, '2019-11-12 06:49:06'),
(13099, '', 0.00, 7, '2019-11-12 06:49:06'),
(13100, '', 0.00, 6, '2019-11-12 06:49:07'),
(13101, '', 0.00, 7, '2019-11-12 06:49:07'),
(13102, '', 0.00, 6, '2019-11-12 06:49:07'),
(13103, '', 0.00, 7, '2019-11-12 06:49:07'),
(13104, '', 0.00, 6, '2019-11-12 06:49:07'),
(13105, '', 0.00, 7, '2019-11-12 06:49:07'),
(13106, '', 0.00, 6, '2019-11-12 06:49:07'),
(13107, '', 0.00, 7, '2019-11-12 06:49:07'),
(13108, '', 0.00, 6, '2019-11-12 06:49:07'),
(13109, '', 0.00, 7, '2019-11-12 06:49:07'),
(13110, '', 0.00, 6, '2019-11-12 06:49:07'),
(13111, '', 0.00, 7, '2019-11-12 06:49:07'),
(13112, '', 0.00, 6, '2019-11-12 06:49:07'),
(13113, '', 0.00, 7, '2019-11-12 06:49:07'),
(13114, '', 0.00, 6, '2019-11-12 06:49:08'),
(13115, '', 0.00, 7, '2019-11-12 06:49:08'),
(13116, '', 0.00, 6, '2019-11-12 06:49:09'),
(13117, '', 0.00, 7, '2019-11-12 06:49:09'),
(13118, '', 0.00, 6, '2019-11-12 06:49:09'),
(13119, '', 0.00, 7, '2019-11-12 06:49:09'),
(13120, '', 0.00, 6, '2019-11-12 06:49:09'),
(13121, '', 0.00, 7, '2019-11-12 06:49:09'),
(13122, '', 0.00, 6, '2019-11-12 06:49:09'),
(13123, '', 0.00, 7, '2019-11-12 06:49:09'),
(13124, '', 0.00, 6, '2019-11-12 06:49:09'),
(13125, '', 0.00, 7, '2019-11-12 06:49:10'),
(13126, '', 0.00, 6, '2019-11-12 06:49:10'),
(13127, '', 0.00, 7, '2019-11-12 06:49:10'),
(13128, '', 0.00, 6, '2019-11-12 06:49:10'),
(13129, '', 0.00, 7, '2019-11-12 06:49:10'),
(13130, '', 0.00, 6, '2019-11-12 06:49:10'),
(13131, '', 0.00, 7, '2019-11-12 06:49:10'),
(13132, '', 0.00, 6, '2019-11-12 06:49:10'),
(13133, '', 0.00, 7, '2019-11-12 06:49:10'),
(13134, '', 0.00, 6, '2019-11-12 06:49:10'),
(13135, '', 0.00, 7, '2019-11-12 06:49:10'),
(13136, '', 0.00, 6, '2019-11-12 06:49:10'),
(13137, '', 0.00, 7, '2019-11-12 06:49:10'),
(13138, '', 0.00, 6, '2019-11-12 06:49:10'),
(13139, '', 0.00, 7, '2019-11-12 06:49:10'),
(13140, '', 0.00, 6, '2019-11-12 06:49:10'),
(13141, '', 0.00, 7, '2019-11-12 06:49:11'),
(13142, '', 0.00, 6, '2019-11-12 06:49:11'),
(13143, '', 0.00, 7, '2019-11-12 06:49:11'),
(13144, '', 0.00, 6, '2019-11-12 06:49:11'),
(13145, '', 0.00, 7, '2019-11-12 06:49:11'),
(13146, '', 0.00, 6, '2019-11-12 06:49:11'),
(13147, '', 0.00, 7, '2019-11-12 06:49:11'),
(13148, '', 0.00, 6, '2019-11-12 06:49:11'),
(13149, '', 0.00, 7, '2019-11-12 06:49:11'),
(13150, '', 0.00, 6, '2019-11-12 06:49:11'),
(13151, '', 0.00, 7, '2019-11-12 06:49:11'),
(13152, '', 0.00, 6, '2019-11-12 06:49:11'),
(13153, '', 0.00, 7, '2019-11-12 06:49:11'),
(13154, '', 0.00, 6, '2019-11-12 06:49:11'),
(13155, '', 0.00, 7, '2019-11-12 06:49:11'),
(13156, '', 0.00, 6, '2019-11-12 06:49:11'),
(13157, '', 0.00, 7, '2019-11-12 06:49:11'),
(13158, '', 0.00, 6, '2019-11-12 06:49:12'),
(13159, '', 0.00, 7, '2019-11-12 06:49:12'),
(13160, '', 0.00, 6, '2019-11-12 06:49:12'),
(13161, '', 0.00, 7, '2019-11-12 06:49:12'),
(13162, '', 0.00, 6, '2019-11-12 06:49:12'),
(13163, '', 0.00, 7, '2019-11-12 06:49:12'),
(13164, '', 0.00, 6, '2019-11-12 06:49:12'),
(13165, '', 0.00, 7, '2019-11-12 06:49:12'),
(13166, '', 0.00, 6, '2019-11-12 06:49:12'),
(13167, '', 0.00, 7, '2019-11-12 06:49:13'),
(13168, '', 0.00, 6, '2019-11-12 06:49:13'),
(13169, '', 0.00, 7, '2019-11-12 06:49:13'),
(13170, '', 0.00, 6, '2019-11-12 06:49:13'),
(13171, '', 0.00, 7, '2019-11-12 06:49:13'),
(13172, '', 0.00, 6, '2019-11-12 06:49:13'),
(13173, '', 0.00, 7, '2019-11-12 06:49:13'),
(13174, '', 0.00, 6, '2019-11-12 06:49:13'),
(13175, '', 0.00, 7, '2019-11-12 06:49:13'),
(13176, '', 0.00, 6, '2019-11-12 06:49:13'),
(13177, '', 0.00, 7, '2019-11-12 06:49:13'),
(13178, '', 0.00, 6, '2019-11-12 06:49:13'),
(13179, '', 0.00, 7, '2019-11-12 06:49:13'),
(13180, '', 0.00, 6, '2019-11-12 06:49:14'),
(13181, '', 0.00, 7, '2019-11-12 06:49:14'),
(13182, '', 0.00, 6, '2019-11-12 06:49:14'),
(13183, '', 0.00, 7, '2019-11-12 06:49:14'),
(13184, '', 0.00, 6, '2019-11-12 06:49:14'),
(13185, '', 0.00, 7, '2019-11-12 06:49:14'),
(13186, '', 0.00, 6, '2019-11-12 06:49:14'),
(13187, '', 0.00, 7, '2019-11-12 06:49:14'),
(13188, '', 0.00, 6, '2019-11-12 06:49:14'),
(13189, '', 0.00, 7, '2019-11-12 06:49:14'),
(13190, '', 0.00, 6, '2019-11-12 06:49:14'),
(13191, '', 0.00, 7, '2019-11-12 06:49:15'),
(13192, '', 0.00, 6, '2019-11-12 06:49:15'),
(13193, '', 0.00, 7, '2019-11-12 06:49:15'),
(13194, '', 0.00, 6, '2019-11-12 06:49:15'),
(13195, '', 0.00, 7, '2019-11-12 06:49:15'),
(13196, '', 0.00, 6, '2019-11-12 06:49:15'),
(13197, '', 0.00, 7, '2019-11-12 06:49:15'),
(13198, '', 0.00, 6, '2019-11-12 06:49:15'),
(13199, '', 0.00, 7, '2019-11-12 06:49:15'),
(13200, '', 0.00, 6, '2019-11-12 06:49:15'),
(13201, '', 0.00, 7, '2019-11-12 06:49:15'),
(13202, '', 0.00, 6, '2019-11-12 06:49:15'),
(13203, '', 0.00, 7, '2019-11-12 06:49:15'),
(13204, '', 0.00, 6, '2019-11-12 06:49:16'),
(13205, '', 0.00, 7, '2019-11-12 06:49:16'),
(13206, '', 0.00, 6, '2019-11-12 06:49:16'),
(13207, '', 0.00, 7, '2019-11-12 06:49:16'),
(13208, '', 0.00, 6, '2019-11-12 06:49:16'),
(13209, '', 0.00, 7, '2019-11-12 06:49:16'),
(13210, '', 0.00, 6, '2019-11-12 06:49:16'),
(13211, '', 0.00, 7, '2019-11-12 06:49:16'),
(13212, '', 0.00, 6, '2019-11-12 06:49:16'),
(13213, '', 0.00, 7, '2019-11-12 06:49:16'),
(13214, '', 0.00, 6, '2019-11-12 06:49:16'),
(13215, '', 0.00, 7, '2019-11-12 06:49:16'),
(13216, '', 0.00, 6, '2019-11-12 06:49:16'),
(13217, '', 0.00, 7, '2019-11-12 06:49:16'),
(13218, '', 0.00, 6, '2019-11-12 06:49:16'),
(13219, '', 0.00, 7, '2019-11-12 06:49:16'),
(13220, '', 0.00, 6, '2019-11-12 06:49:17'),
(13221, '', 0.00, 7, '2019-11-12 06:49:17'),
(13222, '', 0.00, 6, '2019-11-12 06:49:17'),
(13223, '', 0.00, 7, '2019-11-12 06:49:17'),
(13224, '', 0.00, 6, '2019-11-12 06:49:17'),
(13225, '', 0.00, 7, '2019-11-12 06:49:17'),
(13226, '', 0.00, 6, '2019-11-12 06:49:17'),
(13227, '', 0.00, 7, '2019-11-12 06:49:17'),
(13228, '\0\0\0', 0.00, 6, '2019-11-12 06:49:17'),
(13229, '', 0.00, 7, '2019-11-12 06:49:17'),
(13230, '\0\0\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:17'),
(13231, '', 0.00, 7, '2019-11-12 06:49:17'),
(13232, '', 0.00, 6, '2019-11-12 06:49:17'),
(13233, '', 0.00, 7, '2019-11-12 06:49:17'),
(13234, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:17'),
(13235, '', 0.00, 7, '2019-11-12 06:49:17'),
(13236, '', 0.00, 6, '2019-11-12 06:49:17'),
(13237, '', 0.00, 7, '2019-11-12 06:49:17'),
(13238, '', 0.00, 6, '2019-11-12 06:49:18'),
(13239, '', 0.00, 7, '2019-11-12 06:49:18'),
(13240, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:18'),
(13241, '', 0.00, 7, '2019-11-12 06:49:18'),
(13242, '', 0.00, 6, '2019-11-12 06:49:18'),
(13243, '', 0.00, 7, '2019-11-12 06:49:18'),
(13244, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:18'),
(13245, '', 0.00, 7, '2019-11-12 06:49:18'),
(13246, '', 0.00, 6, '2019-11-12 06:49:18'),
(13247, '', 0.00, 7, '2019-11-12 06:49:18'),
(13248, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:18'),
(13249, '', 0.00, 7, '2019-11-12 06:49:18'),
(13250, '', 0.00, 6, '2019-11-12 06:49:18'),
(13251, '', 0.00, 7, '2019-11-12 06:49:18'),
(13252, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:18'),
(13253, '', 0.00, 7, '2019-11-12 06:49:18'),
(13254, '', 0.00, 6, '2019-11-12 06:49:18'),
(13255, '', 0.00, 7, '2019-11-12 06:49:18'),
(13256, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:18'),
(13257, '', 0.00, 7, '2019-11-12 06:49:18'),
(13258, '', 0.00, 6, '2019-11-12 06:49:18'),
(13259, '', 0.00, 7, '2019-11-12 06:49:18'),
(13260, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:19'),
(13261, '', 0.00, 7, '2019-11-12 06:49:19'),
(13262, '', 0.00, 6, '2019-11-12 06:49:19'),
(13263, '', 0.00, 7, '2019-11-12 06:49:19'),
(13264, '', 0.00, 6, '2019-11-12 06:49:19'),
(13265, '', 0.00, 7, '2019-11-12 06:49:19'),
(13266, '\0', 0.00, 6, '2019-11-12 06:49:19'),
(13267, '', 0.00, 7, '2019-11-12 06:49:19'),
(13268, '\0\0\0', 0.00, 6, '2019-11-12 06:49:19'),
(13269, '', 0.00, 7, '2019-11-12 06:49:19'),
(13270, '\0\0~', 0.00, 6, '2019-11-12 06:49:19'),
(13271, '', 0.00, 7, '2019-11-12 06:49:19'),
(13272, '\0', 0.00, 6, '2019-11-12 06:49:19'),
(13273, '', 0.00, 7, '2019-11-12 06:49:19'),
(13274, '', 0.00, 6, '2019-11-12 06:49:19'),
(13275, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:20'),
(13276, '', 0.00, 7, '2019-11-12 06:49:20'),
(13277, '\0\0\0', 0.00, 6, '2019-11-12 06:49:20'),
(13278, '', 0.00, 7, '2019-11-12 06:49:20'),
(13279, '', 0.00, 6, '2019-11-12 06:49:20'),
(13280, '', 0.00, 7, '2019-11-12 06:49:20'),
(13281, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:20'),
(13282, '', 0.00, 7, '2019-11-12 06:49:20'),
(13283, '', 0.00, 6, '2019-11-12 06:49:21'),
(13284, '', 0.00, 7, '2019-11-12 06:49:21'),
(13285, '\0\r\0\0\0\r\0\0~', 0.00, 6, '2019-11-12 06:49:21'),
(13286, '', 0.00, 7, '2019-11-12 06:49:21'),
(13287, '', 0.00, 6, '2019-11-12 06:49:21'),
(13288, '', 0.00, 7, '2019-11-12 06:49:21'),
(13289, '', 0.00, 6, '2019-11-12 06:49:21'),
(13290, '', 0.00, 7, '2019-11-12 06:49:21'),
(13291, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:21'),
(13292, '', 0.00, 7, '2019-11-12 06:49:21'),
(13293, '', 0.00, 6, '2019-11-12 06:49:21'),
(13294, '', 0.00, 7, '2019-11-12 06:49:22'),
(13295, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:22'),
(13296, '', 0.00, 7, '2019-11-12 06:49:22'),
(13297, '', 0.00, 6, '2019-11-12 06:49:22'),
(13298, '', 0.00, 7, '2019-11-12 06:49:22'),
(13299, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:22'),
(13300, '', 0.00, 7, '2019-11-12 06:49:22'),
(13301, '', 0.00, 6, '2019-11-12 06:49:22'),
(13302, '', 0.00, 7, '2019-11-12 06:49:22'),
(13303, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:22'),
(13304, '', 0.00, 7, '2019-11-12 06:49:22'),
(13305, '', 0.00, 6, '2019-11-12 06:49:22'),
(13306, '', 0.00, 7, '2019-11-12 06:49:22'),
(13307, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:23'),
(13308, '', 0.00, 7, '2019-11-12 06:49:23'),
(13309, '', 0.00, 6, '2019-11-12 06:49:23'),
(13310, '', 0.00, 7, '2019-11-12 06:49:23'),
(13311, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:23'),
(13312, '', 0.00, 7, '2019-11-12 06:49:23'),
(13313, '', 0.00, 6, '2019-11-12 06:49:23'),
(13314, '', 0.00, 7, '2019-11-12 06:49:23'),
(13315, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:23'),
(13316, '', 0.00, 7, '2019-11-12 06:49:23'),
(13317, '', 0.00, 6, '2019-11-12 06:49:23'),
(13318, '', 0.00, 7, '2019-11-12 06:49:23'),
(13319, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:23'),
(13320, '', 0.00, 7, '2019-11-12 06:49:23'),
(13321, '', 0.00, 6, '2019-11-12 06:49:23'),
(13322, '', 0.00, 7, '2019-11-12 06:49:23'),
(13323, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:23'),
(13324, '', 0.00, 7, '2019-11-12 06:49:23'),
(13325, '', 0.00, 6, '2019-11-12 06:49:23'),
(13326, '', 0.00, 7, '2019-11-12 06:49:23'),
(13327, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:23'),
(13328, '', 0.00, 7, '2019-11-12 06:49:23'),
(13329, '\0\0\0', 0.00, 6, '2019-11-12 06:49:24'),
(13330, '', 0.00, 7, '2019-11-12 06:49:24'),
(13331, '', 0.00, 6, '2019-11-12 06:49:24'),
(13332, '', 0.00, 7, '2019-11-12 06:49:24'),
(13333, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:24'),
(13334, '', 0.00, 7, '2019-11-12 06:49:24'),
(13335, '', 0.00, 6, '2019-11-12 06:49:24'),
(13336, '', 0.00, 7, '2019-11-12 06:49:24'),
(13337, '\0\Z\0\0\0\Z\0\0~', 0.00, 6, '2019-11-12 06:49:24'),
(13338, '', 0.00, 7, '2019-11-12 06:49:24'),
(13339, '', 0.00, 6, '2019-11-12 06:49:24'),
(13340, '', 0.00, 7, '2019-11-12 06:49:24'),
(13341, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:24'),
(13342, '', 0.00, 7, '2019-11-12 06:49:24'),
(13343, '', 0.00, 6, '2019-11-12 06:49:25'),
(13344, '', 0.00, 7, '2019-11-12 06:49:25'),
(13345, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:25'),
(13346, '', 0.00, 7, '2019-11-12 06:49:25'),
(13347, '', 0.00, 6, '2019-11-12 06:49:25'),
(13348, '', 0.00, 7, '2019-11-12 06:49:25'),
(13349, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:25'),
(13350, '', 0.00, 7, '2019-11-12 06:49:25'),
(13351, '', 0.00, 6, '2019-11-12 06:49:25'),
(13352, '', 0.00, 7, '2019-11-12 06:49:25'),
(13353, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:25'),
(13354, '', 0.00, 7, '2019-11-12 06:49:25'),
(13355, '', 0.00, 6, '2019-11-12 06:49:25'),
(13356, '', 0.00, 7, '2019-11-12 06:49:25'),
(13357, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:25'),
(13358, '', 0.00, 7, '2019-11-12 06:49:25'),
(13359, '', 0.00, 6, '2019-11-12 06:49:25'),
(13360, '', 0.00, 7, '2019-11-12 06:49:25'),
(13361, '\0 \0\0\0 \0\0~', 0.00, 6, '2019-11-12 06:49:25'),
(13362, '', 0.00, 7, '2019-11-12 06:49:26'),
(13363, '', 0.00, 6, '2019-11-12 06:49:26'),
(13364, '', 0.00, 7, '2019-11-12 06:49:26'),
(13365, '\0!\0\0\0!\0\0~', 0.00, 6, '2019-11-12 06:49:26'),
(13366, '', 0.00, 7, '2019-11-12 06:49:26'),
(13367, '', 0.00, 6, '2019-11-12 06:49:26'),
(13368, '', 0.00, 7, '2019-11-12 06:49:26'),
(13369, '\0&quot;\0\0\0&quot;\0\0~', 0.00, 6, '2019-11-12 06:49:26'),
(13370, '', 0.00, 7, '2019-11-12 06:49:26'),
(13371, '', 0.00, 6, '2019-11-12 06:49:26'),
(13372, '', 0.00, 7, '2019-11-12 06:49:26'),
(13373, '\0#\0\0\0#\0\0~', 0.00, 6, '2019-11-12 06:49:26'),
(13374, '', 0.00, 7, '2019-11-12 06:49:26'),
(13375, '', 0.00, 6, '2019-11-12 06:49:26'),
(13376, '', 0.00, 7, '2019-11-12 06:49:26'),
(13377, '\0$\0\0\0$\0\0~', 0.00, 6, '2019-11-12 06:49:26'),
(13378, '', 0.00, 7, '2019-11-12 06:49:26'),
(13379, '', 0.00, 6, '2019-11-12 06:49:26'),
(13380, '', 0.00, 7, '2019-11-12 06:49:26'),
(13381, '\0%\0\0\0%\0\0~', 0.00, 6, '2019-11-12 06:49:26'),
(13382, '', 0.00, 7, '2019-11-12 06:49:27'),
(13383, '', 0.00, 6, '2019-11-12 06:49:27'),
(13384, '', 0.00, 7, '2019-11-12 06:49:27'),
(13385, '\0&amp;\0\0\0&amp;\0\0~', 0.00, 6, '2019-11-12 06:49:27'),
(13386, '', 0.00, 7, '2019-11-12 06:49:27'),
(13387, '', 0.00, 6, '2019-11-12 06:49:27'),
(13388, '', 0.00, 7, '2019-11-12 06:49:27'),
(13389, '\0\'\0\0\0\'\0\0~', 0.00, 6, '2019-11-12 06:49:27'),
(13390, '', 0.00, 7, '2019-11-12 06:49:27'),
(13391, '', 0.00, 6, '2019-11-12 06:49:27'),
(13392, '', 0.00, 7, '2019-11-12 06:49:27'),
(13393, '\0(\0\0\0(\0\0~', 0.00, 6, '2019-11-12 06:49:27'),
(13394, '', 0.00, 7, '2019-11-12 06:49:27'),
(13395, '', 0.00, 6, '2019-11-12 06:49:27'),
(13396, '', 0.00, 7, '2019-11-12 06:49:27'),
(13397, '\0)\0\0\0)\0\0~', 0.00, 6, '2019-11-12 06:49:27'),
(13398, '', 0.00, 7, '2019-11-12 06:49:27'),
(13399, '', 0.00, 6, '2019-11-12 06:49:27'),
(13400, '', 0.00, 7, '2019-11-12 06:49:27'),
(13401, '\0*\0\0\0*\0\0~', 0.00, 6, '2019-11-12 06:49:27'),
(13402, '', 0.00, 7, '2019-11-12 06:49:27'),
(13403, '', 0.00, 6, '2019-11-12 06:49:27'),
(13404, '', 0.00, 7, '2019-11-12 06:49:28'),
(13405, '\0+\0\0\0+\0\0~', 0.00, 6, '2019-11-12 06:49:28'),
(13406, '', 0.00, 7, '2019-11-12 06:49:28'),
(13407, '', 0.00, 6, '2019-11-12 06:49:28'),
(13408, '', 0.00, 7, '2019-11-12 06:49:28'),
(13409, '\0', 0.00, 6, '2019-11-12 06:49:28'),
(13410, '', 0.00, 7, '2019-11-12 06:49:28'),
(13411, '\0', 0.00, 6, '2019-11-12 06:49:28'),
(13412, '', 0.00, 7, '2019-11-12 06:49:28'),
(13413, '\0-\0\0\0-\0\0~', 0.00, 6, '2019-11-12 06:49:28'),
(13414, '', 0.00, 7, '2019-11-12 06:49:28'),
(13415, '', 0.00, 6, '2019-11-12 06:49:28'),
(13416, '', 0.00, 7, '2019-11-12 06:49:28'),
(13417, '\0.\0\0\0.\0\0~', 0.00, 6, '2019-11-12 06:49:28'),
(13418, '', 0.00, 7, '2019-11-12 06:49:28'),
(13419, '', 0.00, 6, '2019-11-12 06:49:28'),
(13420, '', 0.00, 7, '2019-11-12 06:49:28'),
(13421, '\0/\0\0\0/\0\0~', 0.00, 6, '2019-11-12 06:49:28'),
(13422, '', 0.00, 7, '2019-11-12 06:49:28'),
(13423, '', 0.00, 6, '2019-11-12 06:49:28'),
(13424, '', 0.00, 7, '2019-11-12 06:49:29'),
(13425, '\00\0\0\00\0\0~', 0.00, 6, '2019-11-12 06:49:29'),
(13426, '', 0.00, 7, '2019-11-12 06:49:29'),
(13427, '', 0.00, 6, '2019-11-12 06:49:29'),
(13428, '', 0.00, 7, '2019-11-12 06:49:29'),
(13429, '', 0.00, 6, '2019-11-12 06:49:29'),
(13430, '', 0.00, 7, '2019-11-12 06:49:29'),
(13431, '\02\0\0\02\0\0~', 0.00, 6, '2019-11-12 06:49:29'),
(13432, '', 0.00, 7, '2019-11-12 06:49:29'),
(13433, '', 0.00, 6, '2019-11-12 06:49:29'),
(13434, '', 0.00, 7, '2019-11-12 06:49:29'),
(13435, '\03\0\0\03\0\0~', 0.00, 6, '2019-11-12 06:49:29'),
(13436, '', 0.00, 7, '2019-11-12 06:49:29'),
(13437, '', 0.00, 6, '2019-11-12 06:49:29'),
(13438, '', 0.00, 7, '2019-11-12 06:49:29'),
(13439, '\04\0\0\04\0\0~', 0.00, 6, '2019-11-12 06:49:29'),
(13440, '', 0.00, 7, '2019-11-12 06:49:29'),
(13441, '', 0.00, 6, '2019-11-12 06:49:29'),
(13442, '', 0.00, 7, '2019-11-12 06:49:29'),
(13443, '\05\0\0\05\0\0~', 0.00, 6, '2019-11-12 06:49:29'),
(13444, '', 0.00, 7, '2019-11-12 06:49:30'),
(13445, '', 0.00, 6, '2019-11-12 06:49:30'),
(13446, '', 0.00, 7, '2019-11-12 06:49:30'),
(13447, '\06\0\0\06\0\0~', 0.00, 6, '2019-11-12 06:49:30'),
(13448, '', 0.00, 7, '2019-11-12 06:49:30'),
(13449, '\06\0\0', 0.00, 6, '2019-11-12 06:49:30'),
(13450, '', 0.00, 7, '2019-11-12 06:49:30'),
(13451, '', 0.00, 6, '2019-11-12 06:49:30'),
(13452, '', 0.00, 7, '2019-11-12 06:49:30'),
(13453, '\07\0\0\07\0\0~', 0.00, 6, '2019-11-12 06:49:30'),
(13454, '', 0.00, 7, '2019-11-12 06:49:30'),
(13455, '', 0.00, 6, '2019-11-12 06:49:30'),
(13456, '', 0.00, 7, '2019-11-12 06:49:30'),
(13457, '\08\0\0\08\0\0~', 0.00, 6, '2019-11-12 06:49:30'),
(13458, '', 0.00, 7, '2019-11-12 06:49:30'),
(13459, '', 0.00, 6, '2019-11-12 06:49:30'),
(13460, '', 0.00, 7, '2019-11-12 06:49:31'),
(13461, '\09\0\0\09\0\0~', 0.00, 6, '2019-11-12 06:49:31'),
(13462, '', 0.00, 7, '2019-11-12 06:49:31'),
(13463, '', 0.00, 6, '2019-11-12 06:49:31'),
(13464, '', 0.00, 7, '2019-11-12 06:49:31'),
(13465, '\0:\0\0\0:\0\0~', 0.00, 6, '2019-11-12 06:49:31'),
(13466, '', 0.00, 7, '2019-11-12 06:49:31'),
(13467, '', 0.00, 6, '2019-11-12 06:49:32'),
(13468, '', 0.00, 7, '2019-11-12 06:49:32'),
(13469, '\0;\0\0\0;\0\0~', 0.00, 6, '2019-11-12 06:49:32'),
(13470, '', 0.00, 7, '2019-11-12 06:49:32'),
(13471, '', 0.00, 6, '2019-11-12 06:49:32'),
(13472, '', 0.00, 7, '2019-11-12 06:49:32'),
(13473, '\0&lt;\0\0\0&lt;\0\0~', 0.00, 6, '2019-11-12 06:49:32'),
(13474, '', 0.00, 7, '2019-11-12 06:49:32'),
(13475, '', 0.00, 6, '2019-11-12 06:49:32'),
(13476, '', 0.00, 7, '2019-11-12 06:49:32'),
(13477, '\0=\0\0\0=\0\0~', 0.00, 6, '2019-11-12 06:49:32'),
(13478, '', 0.00, 7, '2019-11-12 06:49:32'),
(13479, '', 0.00, 6, '2019-11-12 06:49:32'),
(13480, '', 0.00, 7, '2019-11-12 06:49:32'),
(13481, '\0&gt;\0\0\0&gt;\0\0~', 0.00, 6, '2019-11-12 06:49:32'),
(13482, '', 0.00, 7, '2019-11-12 06:49:32'),
(13483, '', 0.00, 6, '2019-11-12 06:49:33'),
(13484, '', 0.00, 7, '2019-11-12 06:49:33'),
(13485, '\0?\0\0\0?\0\0~', 0.00, 6, '2019-11-12 06:49:33'),
(13486, '', 0.00, 7, '2019-11-12 06:49:33'),
(13487, '\0?\0\0!\0\0\0@\0\0\0', 0.00, 6, '2019-11-12 06:49:33'),
(13488, '', 0.00, 7, '2019-11-12 06:49:33'),
(13489, '\0@\0\0\0@\0\0~', 0.00, 6, '2019-11-12 06:49:33'),
(13490, '', 0.00, 7, '2019-11-12 06:49:33'),
(13491, '', 0.00, 6, '2019-11-12 06:49:33'),
(13492, '', 0.00, 7, '2019-11-12 06:49:33'),
(13493, '\0A\0\0\0A\0\0~', 0.00, 6, '2019-11-12 06:49:34'),
(13494, '', 0.00, 7, '2019-11-12 06:49:34'),
(13495, '', 0.00, 6, '2019-11-12 06:49:34'),
(13496, '', 0.00, 7, '2019-11-12 06:49:34'),
(13497, '\0B\0\0\0B\0\0~', 0.00, 6, '2019-11-12 06:49:34'),
(13498, '', 0.00, 7, '2019-11-12 06:49:34'),
(13499, '', 0.00, 6, '2019-11-12 06:49:34'),
(13500, '', 0.00, 7, '2019-11-12 06:49:34'),
(13501, '\0C\0\0\0C\0\0~', 0.00, 6, '2019-11-12 06:49:34'),
(13502, '', 0.00, 7, '2019-11-12 06:49:34'),
(13503, '', 0.00, 6, '2019-11-12 06:49:34'),
(13504, '', 0.00, 7, '2019-11-12 06:49:34'),
(13505, '\0D\0\0\0D\0\0~', 0.00, 6, '2019-11-12 06:49:34'),
(13506, '', 0.00, 7, '2019-11-12 06:49:34'),
(13507, '', 0.00, 6, '2019-11-12 06:49:34'),
(13508, '', 0.00, 7, '2019-11-12 06:49:34'),
(13509, '\0E\0\0\0E\0\0~', 0.00, 6, '2019-11-12 06:49:34'),
(13510, '', 0.00, 7, '2019-11-12 06:49:34'),
(13511, '', 0.00, 6, '2019-11-12 06:49:34'),
(13512, '', 0.00, 7, '2019-11-12 06:49:34'),
(13513, '\0F\0\0\0F\0\0~', 0.00, 6, '2019-11-12 06:49:34'),
(13514, '', 0.00, 7, '2019-11-12 06:49:34'),
(13515, '', 0.00, 6, '2019-11-12 06:49:34'),
(13516, '', 0.00, 7, '2019-11-12 06:49:34'),
(13517, '\0G\0\0\0G\0\0~', 0.00, 6, '2019-11-12 06:49:34'),
(13518, '', 0.00, 7, '2019-11-12 06:49:35'),
(13519, '', 0.00, 6, '2019-11-12 06:49:35'),
(13520, '', 0.00, 7, '2019-11-12 06:49:35'),
(13521, '\0H\0\0\0H\0\0~', 0.00, 6, '2019-11-12 06:49:35'),
(13522, '', 0.00, 7, '2019-11-12 06:49:35'),
(13523, '', 0.00, 6, '2019-11-12 06:49:35'),
(13524, '', 0.00, 7, '2019-11-12 06:49:35'),
(13525, '\0I\0\0\0I\0\0~', 0.00, 6, '2019-11-12 06:49:35'),
(13526, '', 0.00, 7, '2019-11-12 06:49:35'),
(13527, '', 0.00, 6, '2019-11-12 06:49:35'),
(13528, '', 0.00, 7, '2019-11-12 06:49:35'),
(13529, '\0J\0\0\0J\0\0~', 0.00, 6, '2019-11-12 06:49:35'),
(13530, '', 0.00, 7, '2019-11-12 06:49:35'),
(13531, '', 0.00, 6, '2019-11-12 06:49:35'),
(13532, '', 0.00, 7, '2019-11-12 06:49:35'),
(13533, '\0K\0\0\0K\0\0~', 0.00, 6, '2019-11-12 06:49:35'),
(13534, '', 0.00, 7, '2019-11-12 06:49:35'),
(13535, '', 0.00, 6, '2019-11-12 06:49:36'),
(13536, '', 0.00, 7, '2019-11-12 06:49:36'),
(13537, '\0L\0\0\0L\0\0~', 0.00, 6, '2019-11-12 06:49:36'),
(13538, '', 0.00, 7, '2019-11-12 06:49:36'),
(13539, '', 0.00, 6, '2019-11-12 06:49:36'),
(13540, '', 0.00, 7, '2019-11-12 06:49:36'),
(13541, '\0M\0\0\0M\0\0~', 0.00, 6, '2019-11-12 06:49:36'),
(13542, '', 0.00, 7, '2019-11-12 06:49:36'),
(13543, '', 0.00, 6, '2019-11-12 06:49:36'),
(13544, '', 0.00, 7, '2019-11-12 06:49:36'),
(13545, '\0N\0\0\0N\0\0~', 0.00, 6, '2019-11-12 06:49:36'),
(13546, '', 0.00, 7, '2019-11-12 06:49:36'),
(13547, '', 0.00, 6, '2019-11-12 06:49:37'),
(13548, '', 0.00, 7, '2019-11-12 06:49:37'),
(13549, '\0O\0\0\0O\0\0~', 0.00, 6, '2019-11-12 06:49:37'),
(13550, '', 0.00, 7, '2019-11-12 06:49:37'),
(13551, '', 0.00, 6, '2019-11-12 06:49:37'),
(13552, '', 0.00, 7, '2019-11-12 06:49:37'),
(13553, '\0P\0\0\0P\0\0~', 0.00, 6, '2019-11-12 06:49:37'),
(13554, '', 0.00, 7, '2019-11-12 06:49:37'),
(13555, '', 0.00, 6, '2019-11-12 06:49:37'),
(13556, '', 0.00, 7, '2019-11-12 06:49:38'),
(13557, '\0Q\0\0\0Q\0\0~', 0.00, 6, '2019-11-12 06:49:38'),
(13558, '', 0.00, 7, '2019-11-12 06:49:38'),
(13559, '', 0.00, 6, '2019-11-12 06:49:38'),
(13560, '', 0.00, 7, '2019-11-12 06:49:38'),
(13561, '\0R\0\0\0R\0\0~', 0.00, 6, '2019-11-12 06:49:38'),
(13562, '', 0.00, 7, '2019-11-12 06:49:39'),
(13563, '', 0.00, 6, '2019-11-12 06:49:39'),
(13564, '', 0.00, 7, '2019-11-12 06:49:39'),
(13565, '\0S\0\0\0S\0\0~', 0.00, 6, '2019-11-12 06:49:39'),
(13566, '', 0.00, 7, '2019-11-12 06:49:40'),
(13567, '', 0.00, 6, '2019-11-12 06:49:40'),
(13568, '', 0.00, 7, '2019-11-12 06:49:40'),
(13569, '\0T\0\0\0T\0\0~', 0.00, 6, '2019-11-12 06:49:40'),
(13570, '', 0.00, 7, '2019-11-12 06:49:40'),
(13571, '\0T\0\0R', 0.00, 6, '2019-11-12 06:49:40'),
(13572, '', 0.00, 7, '2019-11-12 06:49:40'),
(13573, '', 0.00, 6, '2019-11-12 06:49:40'),
(13574, '', 0.00, 7, '2019-11-12 06:49:40'),
(13575, '\0U\0\0\0U\0\0~', 0.00, 6, '2019-11-12 06:49:40'),
(13576, '', 0.00, 7, '2019-11-12 06:49:40'),
(13577, '', 0.00, 6, '2019-11-12 06:49:40'),
(13578, '', 0.00, 7, '2019-11-12 06:49:40'),
(13579, '\0V\0\0\0V\0\0~', 0.00, 6, '2019-11-12 06:49:40'),
(13580, '', 0.00, 7, '2019-11-12 06:49:40'),
(13581, '', 0.00, 6, '2019-11-12 06:49:40'),
(13582, '', 0.00, 7, '2019-11-12 06:49:41'),
(13583, '\0W\0\0\0W\0\0~', 0.00, 6, '2019-11-12 06:49:41'),
(13584, '\0\0\0', 0.00, 7, '2019-11-12 06:49:41'),
(13585, '', 0.00, 6, '2019-11-12 06:49:41'),
(13586, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:41'),
(13587, '\0X\0\0\0X\0\0~', 0.00, 6, '2019-11-12 06:49:41'),
(13588, '', 0.00, 7, '2019-11-12 06:49:41'),
(13589, '', 0.00, 6, '2019-11-12 06:49:41'),
(13590, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:41'),
(13591, '\0Y\0\0\0Y\0\0~', 0.00, 6, '2019-11-12 06:49:41'),
(13592, '', 0.00, 7, '2019-11-12 06:49:41'),
(13593, '', 0.00, 6, '2019-11-12 06:49:41'),
(13594, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:41'),
(13595, '\0Z\0\0\0Z\0\0~', 0.00, 6, '2019-11-12 06:49:41'),
(13596, '', 0.00, 7, '2019-11-12 06:49:41'),
(13597, '', 0.00, 6, '2019-11-12 06:49:41'),
(13598, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:41'),
(13599, '', 0.00, 6, '2019-11-12 06:49:41'),
(13600, '', 0.00, 7, '2019-11-12 06:49:41'),
(13601, '\0\\\0\0\0\\\0\0~', 0.00, 6, '2019-11-12 06:49:42'),
(13602, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:42'),
(13603, '', 0.00, 6, '2019-11-12 06:49:42'),
(13604, '', 0.00, 7, '2019-11-12 06:49:42'),
(13605, '\0]\0\0\0]\0\0~', 0.00, 6, '2019-11-12 06:49:42'),
(13606, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:42'),
(13607, '', 0.00, 6, '2019-11-12 06:49:42'),
(13608, '', 0.00, 7, '2019-11-12 06:49:42'),
(13609, '', 0.00, 6, '2019-11-12 06:49:43'),
(13610, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:43'),
(13611, '\0_\0\0\0_\0\0~', 0.00, 6, '2019-11-12 06:49:43'),
(13612, '', 0.00, 7, '2019-11-12 06:49:43'),
(13613, '\0_\0\0*\0\0\0`\0\0\0', 0.00, 6, '2019-11-12 06:49:43'),
(13614, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:43'),
(13615, '\0`\0\0\0`\0\0~', 0.00, 6, '2019-11-12 06:49:43'),
(13616, '', 0.00, 7, '2019-11-12 06:49:43'),
(13617, '', 0.00, 6, '2019-11-12 06:49:43'),
(13618, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:43'),
(13619, '\0a\0\0\0a\0\0~', 0.00, 6, '2019-11-12 06:49:43'),
(13620, '', 0.00, 7, '2019-11-12 06:49:43'),
(13621, '\0a\0\0', 0.00, 6, '2019-11-12 06:49:43'),
(13622, '\0	\0\0\0	\0\0~', 0.00, 7, '2019-11-12 06:49:43'),
(13623, '', 0.00, 6, '2019-11-12 06:49:43'),
(13624, '', 0.00, 7, '2019-11-12 06:49:43'),
(13625, '\0b\0\0\0b\0\0~', 0.00, 6, '2019-11-12 06:49:43'),
(13626, '\0', 0.00, 7, '2019-11-12 06:49:44'),
(13627, '', 0.00, 6, '2019-11-12 06:49:44'),
(13628, '\0\0\0', 0.00, 7, '2019-11-12 06:49:44'),
(13629, '\0c\0\0\0c\0\0~', 0.00, 6, '2019-11-12 06:49:44'),
(13630, '\0\0~', 0.00, 7, '2019-11-12 06:49:44'),
(13631, '', 0.00, 6, '2019-11-12 06:49:44'),
(13632, '\0', 0.00, 7, '2019-11-12 06:49:44'),
(13633, '\0d\0\0\0d\0\0~', 0.00, 6, '2019-11-12 06:49:45'),
(13634, '', 0.00, 7, '2019-11-12 06:49:45'),
(13635, '', 0.00, 6, '2019-11-12 06:49:45'),
(13636, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:45'),
(13637, '\0e\0\0\0e\0\0~', 0.00, 6, '2019-11-12 06:49:45'),
(13638, '', 0.00, 7, '2019-11-12 06:49:45'),
(13639, '', 0.00, 6, '2019-11-12 06:49:45'),
(13640, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:45'),
(13641, '\0f\0\0\0f\0\0~', 0.00, 6, '2019-11-12 06:49:45'),
(13642, '', 0.00, 7, '2019-11-12 06:49:45'),
(13643, '', 0.00, 6, '2019-11-12 06:49:45'),
(13644, '\0\r\0\0\0\r\0\0~', 0.00, 7, '2019-11-12 06:49:45'),
(13645, '\0g\0\0\0g\0\0~', 0.00, 6, '2019-11-12 06:49:45'),
(13646, '', 0.00, 7, '2019-11-12 06:49:45'),
(13647, '', 0.00, 6, '2019-11-12 06:49:45'),
(13648, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:45'),
(13649, '\0h\0\0\0h\0\0~', 0.00, 6, '2019-11-12 06:49:45'),
(13650, '', 0.00, 7, '2019-11-12 06:49:46'),
(13651, '', 0.00, 6, '2019-11-12 06:49:46'),
(13652, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:46'),
(13653, '\0i\0\0\0i\0\0~', 0.00, 6, '2019-11-12 06:49:46'),
(13654, '', 0.00, 7, '2019-11-12 06:49:46'),
(13655, '', 0.00, 6, '2019-11-12 06:49:46'),
(13656, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:46'),
(13657, '\0j\0\0\0j\0\0~', 0.00, 6, '2019-11-12 06:49:46'),
(13658, '', 0.00, 7, '2019-11-12 06:49:46');
INSERT INTO `xin_hospital_drugs` (`drug_id`, `drug_name`, `drug_price`, `hospital_id`, `created_on`) VALUES
(13659, '', 0.00, 6, '2019-11-12 06:49:46'),
(13660, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:46'),
(13661, '\0k\0\0\0k\0\0~', 0.00, 6, '2019-11-12 06:49:47'),
(13662, '', 0.00, 7, '2019-11-12 06:49:47'),
(13663, '', 0.00, 6, '2019-11-12 06:49:47'),
(13664, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:47'),
(13665, '\0l\0\0\0l\0\0~', 0.00, 6, '2019-11-12 06:49:47'),
(13666, '\0\0\0', 0.00, 7, '2019-11-12 06:49:47'),
(13667, '', 0.00, 6, '2019-11-12 06:49:47'),
(13668, '', 0.00, 7, '2019-11-12 06:49:47'),
(13669, '\0m\0\0\0m\0\0~', 0.00, 6, '2019-11-12 06:49:47'),
(13670, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:47'),
(13671, '', 0.00, 6, '2019-11-12 06:49:47'),
(13672, '', 0.00, 7, '2019-11-12 06:49:47'),
(13673, '\0n\0\0\0n\0\0~', 0.00, 6, '2019-11-12 06:49:47'),
(13674, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:47'),
(13675, '', 0.00, 6, '2019-11-12 06:49:47'),
(13676, '', 0.00, 7, '2019-11-12 06:49:48'),
(13677, '\0o\0\0\0o\0\0~', 0.00, 6, '2019-11-12 06:49:48'),
(13678, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:48'),
(13679, '', 0.00, 6, '2019-11-12 06:49:48'),
(13680, '', 0.00, 7, '2019-11-12 06:49:48'),
(13681, '\0p\0\0\0p\0\0~', 0.00, 6, '2019-11-12 06:49:48'),
(13682, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:48'),
(13683, '', 0.00, 6, '2019-11-12 06:49:48'),
(13684, '', 0.00, 7, '2019-11-12 06:49:48'),
(13685, '', 0.00, 6, '2019-11-12 06:49:48'),
(13686, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:48'),
(13687, '\0r\0\0\0r\0\0~', 0.00, 6, '2019-11-12 06:49:48'),
(13688, '', 0.00, 7, '2019-11-12 06:49:48'),
(13689, '', 0.00, 6, '2019-11-12 06:49:48'),
(13690, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:48'),
(13691, '\0s\0\0\0s\0\0~', 0.00, 6, '2019-11-12 06:49:48'),
(13692, '', 0.00, 7, '2019-11-12 06:49:48'),
(13693, '', 0.00, 6, '2019-11-12 06:49:48'),
(13694, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:49'),
(13695, '\0t\0\0\0t\0\0~', 0.00, 6, '2019-11-12 06:49:49'),
(13696, '', 0.00, 7, '2019-11-12 06:49:49'),
(13697, '', 0.00, 6, '2019-11-12 06:49:49'),
(13698, '\0\Z\0\0\0\Z\0\0~', 0.00, 7, '2019-11-12 06:49:49'),
(13699, '\0u\0\0\0u\0\0~', 0.00, 6, '2019-11-12 06:49:49'),
(13700, '', 0.00, 7, '2019-11-12 06:49:49'),
(13701, '', 0.00, 6, '2019-11-12 06:49:49'),
(13702, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:49'),
(13703, '\0v\0\0\0v\0\0~', 0.00, 6, '2019-11-12 06:49:49'),
(13704, '', 0.00, 7, '2019-11-12 06:49:49'),
(13705, '', 0.00, 6, '2019-11-12 06:49:49'),
(13706, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:49'),
(13707, '\0w\0\0\0w\0\0~', 0.00, 6, '2019-11-12 06:49:49'),
(13708, '', 0.00, 7, '2019-11-12 06:49:49'),
(13709, '\0w\0\0', 0.00, 6, '2019-11-12 06:49:49'),
(13710, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:50'),
(13711, '', 0.00, 6, '2019-11-12 06:49:50'),
(13712, '', 0.00, 7, '2019-11-12 06:49:50'),
(13713, '\0x\0\0\0x\0\0~', 0.00, 6, '2019-11-12 06:49:50'),
(13714, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:50'),
(13715, '', 0.00, 6, '2019-11-12 06:49:50'),
(13716, '\0\0\0S', 0.00, 7, '2019-11-12 06:49:50'),
(13717, '\0y\0\0\0y\0\0~', 0.00, 6, '2019-11-12 06:49:50'),
(13718, '', 0.00, 7, '2019-11-12 06:49:50'),
(13719, '', 0.00, 6, '2019-11-12 06:49:50'),
(13720, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:49:50'),
(13721, '\0z\0\0\0z\0\0~', 0.00, 6, '2019-11-12 06:49:50'),
(13722, '\0\0\0S', 0.00, 7, '2019-11-12 06:49:50'),
(13723, '', 0.00, 6, '2019-11-12 06:49:50'),
(13724, '\0\0\0 \0\0\0', 0.00, 7, '2019-11-12 06:49:50'),
(13725, '\0{\0\0\0{\0\0~', 0.00, 6, '2019-11-12 06:49:50'),
(13726, '\0 \0\0\0 \0\0~', 0.00, 7, '2019-11-12 06:49:50'),
(13727, '', 0.00, 6, '2019-11-12 06:49:50'),
(13728, '', 0.00, 7, '2019-11-12 06:49:50'),
(13729, '\0|\0\0\0|\0\0~', 0.00, 6, '2019-11-12 06:49:51'),
(13730, '\0!\0\0\0!\0\0~', 0.00, 7, '2019-11-12 06:49:51'),
(13731, '', 0.00, 6, '2019-11-12 06:49:51'),
(13732, '', 0.00, 7, '2019-11-12 06:49:51'),
(13733, '\0}\0\0\0}\0\0~', 0.00, 6, '2019-11-12 06:49:51'),
(13734, '\0&quot;\0\0\0&quot;\0\0~', 0.00, 7, '2019-11-12 06:49:51'),
(13735, '', 0.00, 6, '2019-11-12 06:49:51'),
(13736, '', 0.00, 7, '2019-11-12 06:49:51'),
(13737, '', 0.00, 6, '2019-11-12 06:49:51'),
(13738, '\0#\0\0\0#\0\0~', 0.00, 7, '2019-11-12 06:49:51'),
(13739, '', 0.00, 6, '2019-11-12 06:49:51'),
(13740, '', 0.00, 7, '2019-11-12 06:49:51'),
(13741, '\0\0\0\0\0\0~', 0.00, 6, '2019-11-12 06:49:51'),
(13742, '\0$\0\0\0$\0\0~', 0.00, 7, '2019-11-12 06:49:51'),
(13743, '', 0.00, 6, '2019-11-12 06:49:52'),
(13744, '', 0.00, 7, '2019-11-12 06:49:52'),
(13745, '', 0.00, 6, '2019-11-12 06:49:52'),
(13746, '\0%\0\0\0%\0\0~', 0.00, 7, '2019-11-12 06:49:52'),
(13747, '', 0.00, 6, '2019-11-12 06:49:52'),
(13748, '', 0.00, 7, '2019-11-12 06:49:52'),
(13749, '', 0.00, 6, '2019-11-12 06:49:52'),
(13750, '\0&amp;\0\0\0&amp;\0\0~', 0.00, 7, '2019-11-12 06:49:52'),
(13751, '', 0.00, 6, '2019-11-12 06:49:52'),
(13752, '', 0.00, 7, '2019-11-12 06:49:52'),
(13753, '', 0.00, 6, '2019-11-12 06:49:52'),
(13754, '\0\'\0\0\0\'\0\0~', 0.00, 7, '2019-11-12 06:49:52'),
(13755, '', 0.00, 6, '2019-11-12 06:49:52'),
(13756, '', 0.00, 7, '2019-11-12 06:49:52'),
(13757, '', 0.00, 6, '2019-11-12 06:49:52'),
(13758, '\0(\0\0\0(\0\0~', 0.00, 7, '2019-11-12 06:49:52'),
(13759, '', 0.00, 6, '2019-11-12 06:49:52'),
(13760, '', 0.00, 7, '2019-11-12 06:49:52'),
(13761, '', 0.00, 6, '2019-11-12 06:49:52'),
(13762, '\0)\0\0\0)\0\0~', 0.00, 7, '2019-11-12 06:49:52'),
(13763, '', 0.00, 6, '2019-11-12 06:49:52'),
(13764, '', 0.00, 7, '2019-11-12 06:49:52'),
(13765, '', 0.00, 6, '2019-11-12 06:49:53'),
(13766, '\0*\0\0\0*\0\0~', 0.00, 7, '2019-11-12 06:49:53'),
(13767, '', 0.00, 6, '2019-11-12 06:49:53'),
(13768, '', 0.00, 7, '2019-11-12 06:49:53'),
(13769, '', 0.00, 6, '2019-11-12 06:49:53'),
(13770, '\0+\0\0\0+\0\0~', 0.00, 7, '2019-11-12 06:49:53'),
(13771, '', 0.00, 7, '2019-11-12 06:49:53'),
(13772, '', 0.00, 6, '2019-11-12 06:49:53'),
(13773, '\0', 0.00, 7, '2019-11-12 06:49:54'),
(13774, '', 0.00, 6, '2019-11-12 06:49:54'),
(13775, '\0', 0.00, 7, '2019-11-12 06:49:54'),
(13776, '', 0.00, 6, '2019-11-12 06:49:54'),
(13777, '\0-\0\0\0-\0\0~', 0.00, 7, '2019-11-12 06:49:55'),
(13778, '', 0.00, 6, '2019-11-12 06:49:55'),
(13779, '', 0.00, 7, '2019-11-12 06:49:55'),
(13780, '', 0.00, 6, '2019-11-12 06:49:55'),
(13781, '\0.\0\0\0.\0\0~', 0.00, 7, '2019-11-12 06:49:55'),
(13782, '', 0.00, 6, '2019-11-12 06:49:55'),
(13783, '', 0.00, 7, '2019-11-12 06:49:55'),
(13784, '', 0.00, 6, '2019-11-12 06:49:55'),
(13785, '\0/\0\0\0/\0\0~', 0.00, 7, '2019-11-12 06:49:56'),
(13786, '', 0.00, 6, '2019-11-12 06:49:56'),
(13787, '', 0.00, 7, '2019-11-12 06:49:56'),
(13788, '', 0.00, 6, '2019-11-12 06:49:56'),
(13789, '\00\0\0\00\0\0~', 0.00, 7, '2019-11-12 06:49:56'),
(13790, '', 0.00, 6, '2019-11-12 06:49:56'),
(13791, '', 0.00, 7, '2019-11-12 06:49:56'),
(13792, '', 0.00, 6, '2019-11-12 06:49:56'),
(13793, '\01\0\0\01\0\0~', 0.00, 7, '2019-11-12 06:49:56'),
(13794, '', 0.00, 6, '2019-11-12 06:49:57'),
(13795, '', 0.00, 7, '2019-11-12 06:49:57'),
(13796, '', 0.00, 6, '2019-11-12 06:49:57'),
(13797, '\02\0\0\02\0\0~', 0.00, 7, '2019-11-12 06:49:57'),
(13798, '', 0.00, 6, '2019-11-12 06:49:57'),
(13799, '', 0.00, 7, '2019-11-12 06:49:57'),
(13800, '', 0.00, 6, '2019-11-12 06:49:57'),
(13801, '\03\0\0\03\0\0~', 0.00, 7, '2019-11-12 06:49:57'),
(13802, '', 0.00, 6, '2019-11-12 06:49:57'),
(13803, '', 0.00, 7, '2019-11-12 06:49:57'),
(13804, '', 0.00, 6, '2019-11-12 06:49:57'),
(13805, '\04\0\0\04\0\0~', 0.00, 7, '2019-11-12 06:49:57'),
(13806, '', 0.00, 6, '2019-11-12 06:49:57'),
(13807, '', 0.00, 7, '2019-11-12 06:49:57'),
(13808, '', 0.00, 6, '2019-11-12 06:49:57'),
(13809, '\05\0\0\05\0\0~', 0.00, 7, '2019-11-12 06:49:58'),
(13810, '', 0.00, 6, '2019-11-12 06:49:58'),
(13811, '', 0.00, 7, '2019-11-12 06:49:58'),
(13812, '', 0.00, 6, '2019-11-12 06:49:58'),
(13813, '\06\0\0\06\0\0~', 0.00, 7, '2019-11-12 06:49:58'),
(13814, '', 0.00, 6, '2019-11-12 06:49:58'),
(13815, '', 0.00, 7, '2019-11-12 06:49:58'),
(13816, '', 0.00, 6, '2019-11-12 06:49:58'),
(13817, '\07\0\0\07\0\0~', 0.00, 7, '2019-11-12 06:49:58'),
(13818, '', 0.00, 6, '2019-11-12 06:49:58'),
(13819, '', 0.00, 7, '2019-11-12 06:49:58'),
(13820, '', 0.00, 6, '2019-11-12 06:49:58'),
(13821, '\08\0\0\08\0\0~', 0.00, 7, '2019-11-12 06:49:58'),
(13822, '', 0.00, 6, '2019-11-12 06:49:58'),
(13823, '', 0.00, 7, '2019-11-12 06:49:58'),
(13824, '', 0.00, 6, '2019-11-12 06:49:58'),
(13825, '\09\0\0\09\0\0~', 0.00, 7, '2019-11-12 06:49:58'),
(13826, '', 0.00, 6, '2019-11-12 06:49:58'),
(13827, '', 0.00, 7, '2019-11-12 06:49:58'),
(13828, '', 0.00, 6, '2019-11-12 06:49:59'),
(13829, '', 0.00, 7, '2019-11-12 06:49:59'),
(13830, '', 0.00, 6, '2019-11-12 06:49:59'),
(13831, '\0;\0\0\0;\0\0~', 0.00, 7, '2019-11-12 06:49:59'),
(13832, '', 0.00, 6, '2019-11-12 06:49:59'),
(13833, '', 0.00, 7, '2019-11-12 06:49:59'),
(13834, '', 0.00, 6, '2019-11-12 06:49:59'),
(13835, '\0&lt;\0\0\0&lt;\0\0~', 0.00, 7, '2019-11-12 06:49:59'),
(13836, '', 0.00, 6, '2019-11-12 06:49:59'),
(13837, '', 0.00, 7, '2019-11-12 06:49:59'),
(13838, '', 0.00, 6, '2019-11-12 06:49:59'),
(13839, '\0=\0\0\0=\0\0~', 0.00, 7, '2019-11-12 06:49:59'),
(13840, '', 0.00, 6, '2019-11-12 06:49:59'),
(13841, '\0=\0\0', 0.00, 7, '2019-11-12 06:50:00'),
(13842, '', 0.00, 6, '2019-11-12 06:50:00'),
(13843, '', 0.00, 7, '2019-11-12 06:50:00'),
(13844, '', 0.00, 6, '2019-11-12 06:50:00'),
(13845, '\0&gt;\0\0\0&gt;\0\0~', 0.00, 7, '2019-11-12 06:50:00'),
(13846, '', 0.00, 6, '2019-11-12 06:50:00'),
(13847, '', 0.00, 7, '2019-11-12 06:50:00'),
(13848, '', 0.00, 6, '2019-11-12 06:50:00'),
(13849, '\0?\0\0\0?\0\0~', 0.00, 7, '2019-11-12 06:50:00'),
(13850, '', 0.00, 6, '2019-11-12 06:50:00'),
(13851, '\0?\0\0\0\0\0\0@\0\0\0', 0.00, 7, '2019-11-12 06:50:00'),
(13852, '', 0.00, 6, '2019-11-12 06:50:00'),
(13853, '\0@\0\0\0@\0\0~', 0.00, 7, '2019-11-12 06:50:00'),
(13854, '', 0.00, 6, '2019-11-12 06:50:00'),
(13855, '', 0.00, 7, '2019-11-12 06:50:00'),
(13856, '', 0.00, 6, '2019-11-12 06:50:00'),
(13857, '\0A\0\0\0A\0\0~', 0.00, 7, '2019-11-12 06:50:00'),
(13858, '', 0.00, 6, '2019-11-12 06:50:00'),
(13859, '', 0.00, 7, '2019-11-12 06:50:00'),
(13860, '', 0.00, 6, '2019-11-12 06:50:00'),
(13861, '\0B\0\0\0B\0\0~', 0.00, 7, '2019-11-12 06:50:00'),
(13862, '', 0.00, 6, '2019-11-12 06:50:00'),
(13863, '', 0.00, 7, '2019-11-12 06:50:00'),
(13864, '', 0.00, 6, '2019-11-12 06:50:01'),
(13865, '', 0.00, 7, '2019-11-12 06:50:01'),
(13866, '', 0.00, 6, '2019-11-12 06:50:01'),
(13867, '\0D\0\0\0D\0\0~', 0.00, 7, '2019-11-12 06:50:01'),
(13868, '', 0.00, 6, '2019-11-12 06:50:01'),
(13869, '', 0.00, 7, '2019-11-12 06:50:01'),
(13870, '', 0.00, 6, '2019-11-12 06:50:01'),
(13871, '\0E\0\0\0E\0\0~', 0.00, 7, '2019-11-12 06:50:01'),
(13872, '', 0.00, 6, '2019-11-12 06:50:01'),
(13873, '', 0.00, 7, '2019-11-12 06:50:01'),
(13874, '', 0.00, 6, '2019-11-12 06:50:01'),
(13875, '\0F\0\0\0F\0\0~', 0.00, 7, '2019-11-12 06:50:01'),
(13876, '', 0.00, 6, '2019-11-12 06:50:01'),
(13877, '', 0.00, 7, '2019-11-12 06:50:01'),
(13878, '', 0.00, 6, '2019-11-12 06:50:02'),
(13879, '\0G\0\0\0G\0\0~', 0.00, 7, '2019-11-12 06:50:02'),
(13880, '', 0.00, 6, '2019-11-12 06:50:02'),
(13881, '', 0.00, 7, '2019-11-12 06:50:02'),
(13882, '', 0.00, 6, '2019-11-12 06:50:02'),
(13883, '\0H\0\0\0H\0\0~', 0.00, 7, '2019-11-12 06:50:02'),
(13884, '', 0.00, 6, '2019-11-12 06:50:02'),
(13885, '', 0.00, 7, '2019-11-12 06:50:02'),
(13886, '', 0.00, 6, '2019-11-12 06:50:02'),
(13887, '\0I\0\0\0I\0\0~', 0.00, 7, '2019-11-12 06:50:02'),
(13888, '', 0.00, 6, '2019-11-12 06:50:02'),
(13889, '', 0.00, 7, '2019-11-12 06:50:02'),
(13890, '', 0.00, 6, '2019-11-12 06:50:02'),
(13891, '\0J\0\0\0J\0\0~', 0.00, 7, '2019-11-12 06:50:02'),
(13892, '', 0.00, 6, '2019-11-12 06:50:02'),
(13893, '', 0.00, 7, '2019-11-12 06:50:02'),
(13894, '', 0.00, 6, '2019-11-12 06:50:02'),
(13895, '\0K\0\0\0K\0\0~', 0.00, 7, '2019-11-12 06:50:02'),
(13896, '', 0.00, 6, '2019-11-12 06:50:02'),
(13897, '', 0.00, 7, '2019-11-12 06:50:03'),
(13898, '', 0.00, 6, '2019-11-12 06:50:03'),
(13899, '\0L\0\0\0L\0\0~', 0.00, 7, '2019-11-12 06:50:03'),
(13900, '', 0.00, 6, '2019-11-12 06:50:03'),
(13901, '', 0.00, 7, '2019-11-12 06:50:03'),
(13902, '', 0.00, 6, '2019-11-12 06:50:03'),
(13903, '\0M\0\0\0M\0\0~', 0.00, 7, '2019-11-12 06:50:03'),
(13904, '', 0.00, 6, '2019-11-12 06:50:03'),
(13905, '', 0.00, 7, '2019-11-12 06:50:03'),
(13906, '', 0.00, 6, '2019-11-12 06:50:03'),
(13907, '\0N\0\0\0N\0\0~', 0.00, 7, '2019-11-12 06:50:03'),
(13908, '', 0.00, 6, '2019-11-12 06:50:03'),
(13909, '', 0.00, 7, '2019-11-12 06:50:03'),
(13910, '', 0.00, 6, '2019-11-12 06:50:03'),
(13911, '\0O\0\0\0O\0\0~', 0.00, 7, '2019-11-12 06:50:03'),
(13912, '', 0.00, 6, '2019-11-12 06:50:03'),
(13913, '\0O\0\0', 0.00, 7, '2019-11-12 06:50:03'),
(13914, '', 0.00, 6, '2019-11-12 06:50:03'),
(13915, '', 0.00, 7, '2019-11-12 06:50:03'),
(13916, '', 0.00, 6, '2019-11-12 06:50:03'),
(13917, '\0P\0\0\0P\0\0~', 0.00, 7, '2019-11-12 06:50:03'),
(13918, '', 0.00, 6, '2019-11-12 06:50:04'),
(13919, '', 0.00, 7, '2019-11-12 06:50:04'),
(13920, '', 0.00, 6, '2019-11-12 06:50:04'),
(13921, '\0Q\0\0\0Q\0\0~', 0.00, 7, '2019-11-12 06:50:04'),
(13922, '', 0.00, 6, '2019-11-12 06:50:04'),
(13923, '', 0.00, 7, '2019-11-12 06:50:04'),
(13924, '', 0.00, 6, '2019-11-12 06:50:04'),
(13925, '\0R\0\0\0R\0\0~', 0.00, 7, '2019-11-12 06:50:04'),
(13926, '', 0.00, 6, '2019-11-12 06:50:04'),
(13927, '', 0.00, 7, '2019-11-12 06:50:05'),
(13928, '', 0.00, 6, '2019-11-12 06:50:05'),
(13929, '\0S\0\0\0S\0\0~', 0.00, 7, '2019-11-12 06:50:05'),
(13930, '', 0.00, 6, '2019-11-12 06:50:05'),
(13931, '', 0.00, 7, '2019-11-12 06:50:05'),
(13932, '', 0.00, 6, '2019-11-12 06:50:05'),
(13933, '\0T\0\0\0T\0\0~', 0.00, 7, '2019-11-12 06:50:05'),
(13934, '', 0.00, 6, '2019-11-12 06:50:05'),
(13935, '', 0.00, 7, '2019-11-12 06:50:05'),
(13936, '', 0.00, 6, '2019-11-12 06:50:05'),
(13937, '', 0.00, 7, '2019-11-12 06:50:06'),
(13938, '', 0.00, 6, '2019-11-12 06:50:06'),
(13939, '\0V\0\0\0V\0\0~', 0.00, 7, '2019-11-12 06:50:06'),
(13940, '', 0.00, 6, '2019-11-12 06:50:06'),
(13941, '', 0.00, 7, '2019-11-12 06:50:06'),
(13942, '', 0.00, 6, '2019-11-12 06:50:06'),
(13943, '\0W\0\0\0W\0\0~', 0.00, 7, '2019-11-12 06:50:06'),
(13944, '', 0.00, 6, '2019-11-12 06:50:06'),
(13945, '', 0.00, 7, '2019-11-12 06:50:06'),
(13946, '', 0.00, 6, '2019-11-12 06:50:06'),
(13947, '\0X\0\0\0X\0\0~', 0.00, 7, '2019-11-12 06:50:06'),
(13948, '', 0.00, 6, '2019-11-12 06:50:06'),
(13949, '', 0.00, 7, '2019-11-12 06:50:06'),
(13950, '', 0.00, 6, '2019-11-12 06:50:06'),
(13951, '\0Y\0\0\0Y\0\0~', 0.00, 7, '2019-11-12 06:50:06'),
(13952, '', 0.00, 6, '2019-11-12 06:50:06'),
(13953, '', 0.00, 7, '2019-11-12 06:50:07'),
(13954, '', 0.00, 6, '2019-11-12 06:50:07'),
(13955, '', 0.00, 7, '2019-11-12 06:50:07'),
(13956, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', 0.00, 6, '2019-11-12 06:50:07'),
(13957, '\0[\0\0\0[\0\0~', 0.00, 7, '2019-11-12 06:50:07'),
(13958, '', 0.00, 7, '2019-11-12 06:50:07'),
(13959, '\0\\\0\0\0\\\0\0~', 0.00, 7, '2019-11-12 06:50:07'),
(13960, '', 0.00, 7, '2019-11-12 06:50:07'),
(13961, '\0]\0\0\0]\0\0~', 0.00, 7, '2019-11-12 06:50:07'),
(13962, '', 0.00, 7, '2019-11-12 06:50:07'),
(13963, '\0^\0\0\0^\0\0~', 0.00, 7, '2019-11-12 06:50:07'),
(13964, '', 0.00, 7, '2019-11-12 06:50:08'),
(13965, '', 0.00, 7, '2019-11-12 06:50:08'),
(13966, '\0`\0\0\0`\0\0~', 0.00, 7, '2019-11-12 06:50:08'),
(13967, '', 0.00, 7, '2019-11-12 06:50:08'),
(13968, '\0a\0\0\0a\0\0~', 0.00, 7, '2019-11-12 06:50:08'),
(13969, '', 0.00, 7, '2019-11-12 06:50:08'),
(13970, '\0b\0\0\0b\0\0~', 0.00, 7, '2019-11-12 06:50:08'),
(13971, '\0b\0\0', 0.00, 7, '2019-11-12 06:50:08'),
(13972, '', 0.00, 7, '2019-11-12 06:50:08'),
(13973, '\0c\0\0\0c\0\0~', 0.00, 7, '2019-11-12 06:50:08'),
(13974, '', 0.00, 7, '2019-11-12 06:50:08'),
(13975, '\0d\0\0\0d\0\0~', 0.00, 7, '2019-11-12 06:50:08'),
(13976, '', 0.00, 7, '2019-11-12 06:50:09'),
(13977, '\0e\0\0\0e\0\0~', 0.00, 7, '2019-11-12 06:50:09'),
(13978, '', 0.00, 7, '2019-11-12 06:50:09'),
(13979, '\0f\0\0\0f\0\0~', 0.00, 7, '2019-11-12 06:50:09'),
(13980, '', 0.00, 7, '2019-11-12 06:50:09'),
(13981, '\0g\0\0\0g\0\0~', 0.00, 7, '2019-11-12 06:50:09'),
(13982, '', 0.00, 7, '2019-11-12 06:50:09'),
(13983, '\0h\0\0\0h\0\0~', 0.00, 7, '2019-11-12 06:50:09'),
(13984, '', 0.00, 7, '2019-11-12 06:50:09'),
(13985, '\0i\0\0\0i\0\0~', 0.00, 7, '2019-11-12 06:50:09'),
(13986, '', 0.00, 7, '2019-11-12 06:50:09'),
(13987, '\0j\0\0\0j\0\0~', 0.00, 7, '2019-11-12 06:50:09'),
(13988, '', 0.00, 7, '2019-11-12 06:50:09'),
(13989, '\0k\0\0\0k\0\0~', 0.00, 7, '2019-11-12 06:50:09'),
(13990, '\0k\0\0', 0.00, 7, '2019-11-12 06:50:09'),
(13991, '', 0.00, 7, '2019-11-12 06:50:09'),
(13992, '\0l\0\0\0l\0\0~', 0.00, 7, '2019-11-12 06:50:09'),
(13993, '', 0.00, 7, '2019-11-12 06:50:10'),
(13994, '\0m\0\0\0m\0\0~', 0.00, 7, '2019-11-12 06:50:10'),
(13995, '', 0.00, 7, '2019-11-12 06:50:10'),
(13996, '\0n\0\0\0n\0\0~', 0.00, 7, '2019-11-12 06:50:10'),
(13997, '', 0.00, 7, '2019-11-12 06:50:10'),
(13998, '\0o\0\0\0o\0\0~', 0.00, 7, '2019-11-12 06:50:10'),
(13999, '', 0.00, 7, '2019-11-12 06:50:10'),
(14000, '\0p\0\0\0p\0\0~', 0.00, 7, '2019-11-12 06:50:10'),
(14001, '', 0.00, 7, '2019-11-12 06:50:10'),
(14002, '', 0.00, 7, '2019-11-12 06:50:10'),
(14003, '\0r\0\0\0r\0\0~', 0.00, 7, '2019-11-12 06:50:10'),
(14004, '', 0.00, 7, '2019-11-12 06:50:10'),
(14005, '\0s\0\0\0s\0\0~', 0.00, 7, '2019-11-12 06:50:10'),
(14006, '', 0.00, 7, '2019-11-12 06:50:10'),
(14007, '\0t\0\0\0t\0\0~', 0.00, 7, '2019-11-12 06:50:10'),
(14008, '', 0.00, 7, '2019-11-12 06:50:10'),
(14009, '\0u\0\0\0u\0\0~', 0.00, 7, '2019-11-12 06:50:11'),
(14010, '', 0.00, 7, '2019-11-12 06:50:11'),
(14011, '\0v\0\0\0v\0\0~', 0.00, 7, '2019-11-12 06:50:11'),
(14012, '', 0.00, 7, '2019-11-12 06:50:11'),
(14013, '\0w\0\0\0w\0\0~', 0.00, 7, '2019-11-12 06:50:11'),
(14014, '', 0.00, 7, '2019-11-12 06:50:11'),
(14015, '\0x\0\0\0x\0\0~', 0.00, 7, '2019-11-12 06:50:11'),
(14016, '', 0.00, 7, '2019-11-12 06:50:11'),
(14017, '\0y\0\0\0y\0\0~', 0.00, 7, '2019-11-12 06:50:11'),
(14018, '', 0.00, 7, '2019-11-12 06:50:11'),
(14019, '\0z\0\0\0z\0\0~', 0.00, 7, '2019-11-12 06:50:11'),
(14020, '', 0.00, 7, '2019-11-12 06:50:11'),
(14021, '\0{\0\0\0{\0\0~', 0.00, 7, '2019-11-12 06:50:11'),
(14022, '', 0.00, 7, '2019-11-12 06:50:11'),
(14023, '\0|\0\0\0|\0\0~', 0.00, 7, '2019-11-12 06:50:11'),
(14024, '', 0.00, 7, '2019-11-12 06:50:12'),
(14025, '\0}\0\0\0}\0\0~', 0.00, 7, '2019-11-12 06:50:12'),
(14026, '\0}\0\0S', 0.00, 7, '2019-11-12 06:50:12'),
(14027, '', 0.00, 7, '2019-11-12 06:50:12'),
(14028, '\0~\0\0\0~\0\0~', 0.00, 7, '2019-11-12 06:50:12'),
(14029, '', 0.00, 7, '2019-11-12 06:50:12'),
(14030, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:12'),
(14031, '', 0.00, 7, '2019-11-12 06:50:12'),
(14032, '', 0.00, 7, '2019-11-12 06:50:12'),
(14033, '', 0.00, 7, '2019-11-12 06:50:12'),
(14034, '', 0.00, 7, '2019-11-12 06:50:12'),
(14035, '', 0.00, 7, '2019-11-12 06:50:13'),
(14036, '', 0.00, 7, '2019-11-12 06:50:13'),
(14037, '', 0.00, 7, '2019-11-12 06:50:13'),
(14038, '', 0.00, 7, '2019-11-12 06:50:13'),
(14039, '', 0.00, 7, '2019-11-12 06:50:13'),
(14040, '', 0.00, 7, '2019-11-12 06:50:13'),
(14041, '', 0.00, 7, '2019-11-12 06:50:13'),
(14042, '', 0.00, 7, '2019-11-12 06:50:13'),
(14043, '', 0.00, 7, '2019-11-12 06:50:13'),
(14044, '', 0.00, 7, '2019-11-12 06:50:13'),
(14045, '', 0.00, 7, '2019-11-12 06:50:13'),
(14046, '', 0.00, 7, '2019-11-12 06:50:13'),
(14047, '', 0.00, 7, '2019-11-12 06:50:13'),
(14048, '', 0.00, 7, '2019-11-12 06:50:13'),
(14049, '', 0.00, 7, '2019-11-12 06:50:13'),
(14050, '', 0.00, 7, '2019-11-12 06:50:13'),
(14051, '', 0.00, 7, '2019-11-12 06:50:14'),
(14052, '', 0.00, 7, '2019-11-12 06:50:14'),
(14053, '', 0.00, 7, '2019-11-12 06:50:14'),
(14054, '', 0.00, 7, '2019-11-12 06:50:14'),
(14055, '', 0.00, 7, '2019-11-12 06:50:14'),
(14056, '', 0.00, 7, '2019-11-12 06:50:14'),
(14057, '', 0.00, 7, '2019-11-12 06:50:14'),
(14058, '', 0.00, 7, '2019-11-12 06:50:15'),
(14059, '', 0.00, 7, '2019-11-12 06:50:15'),
(14060, '', 0.00, 7, '2019-11-12 06:50:15'),
(14061, '', 0.00, 7, '2019-11-12 06:50:15'),
(14062, '', 0.00, 7, '2019-11-12 06:50:15'),
(14063, '', 0.00, 7, '2019-11-12 06:50:15'),
(14064, '', 0.00, 7, '2019-11-12 06:50:15'),
(14065, '', 0.00, 7, '2019-11-12 06:50:15'),
(14066, '', 0.00, 7, '2019-11-12 06:50:15'),
(14067, '', 0.00, 7, '2019-11-12 06:50:15'),
(14068, '', 0.00, 7, '2019-11-12 06:50:15'),
(14069, '', 0.00, 7, '2019-11-12 06:50:15'),
(14070, '', 0.00, 7, '2019-11-12 06:50:15'),
(14071, '', 0.00, 7, '2019-11-12 06:50:16'),
(14072, '', 0.00, 7, '2019-11-12 06:50:16'),
(14073, '', 0.00, 7, '2019-11-12 06:50:16'),
(14074, '', 0.00, 7, '2019-11-12 06:50:16'),
(14075, '', 0.00, 7, '2019-11-12 06:50:16'),
(14076, '', 0.00, 7, '2019-11-12 06:50:16'),
(14077, '', 0.00, 7, '2019-11-12 06:50:17'),
(14078, '', 0.00, 7, '2019-11-12 06:50:17'),
(14079, '', 0.00, 7, '2019-11-12 06:50:17'),
(14080, '', 0.00, 7, '2019-11-12 06:50:17'),
(14081, '', 0.00, 7, '2019-11-12 06:50:17'),
(14082, '', 0.00, 7, '2019-11-12 06:50:17'),
(14083, '', 0.00, 7, '2019-11-12 06:50:17'),
(14084, '', 0.00, 7, '2019-11-12 06:50:17'),
(14085, '', 0.00, 7, '2019-11-12 06:50:17'),
(14086, '', 0.00, 7, '2019-11-12 06:50:17'),
(14087, '', 0.00, 7, '2019-11-12 06:50:17'),
(14088, '', 0.00, 7, '2019-11-12 06:50:17'),
(14089, '', 0.00, 7, '2019-11-12 06:50:17'),
(14090, '', 0.00, 7, '2019-11-12 06:50:17'),
(14091, '', 0.00, 7, '2019-11-12 06:50:17'),
(14092, '', 0.00, 7, '2019-11-12 06:50:17'),
(14093, '', 0.00, 7, '2019-11-12 06:50:17'),
(14094, '', 0.00, 7, '2019-11-12 06:50:17'),
(14095, '', 0.00, 7, '2019-11-12 06:50:17'),
(14096, '', 0.00, 7, '2019-11-12 06:50:17'),
(14097, '', 0.00, 7, '2019-11-12 06:50:17'),
(14098, '', 0.00, 7, '2019-11-12 06:50:17'),
(14099, '', 0.00, 7, '2019-11-12 06:50:17'),
(14100, '', 0.00, 7, '2019-11-12 06:50:18'),
(14101, '', 0.00, 7, '2019-11-12 06:50:18'),
(14102, '', 0.00, 7, '2019-11-12 06:50:18'),
(14103, '', 0.00, 7, '2019-11-12 06:50:18'),
(14104, '', 0.00, 7, '2019-11-12 06:50:18'),
(14105, '', 0.00, 7, '2019-11-12 06:50:18'),
(14106, '', 0.00, 7, '2019-11-12 06:50:18'),
(14107, '', 0.00, 7, '2019-11-12 06:50:18'),
(14108, '', 0.00, 7, '2019-11-12 06:50:18'),
(14109, '', 0.00, 7, '2019-11-12 06:50:18'),
(14110, '', 0.00, 7, '2019-11-12 06:50:18'),
(14111, '', 0.00, 7, '2019-11-12 06:50:18'),
(14112, '', 0.00, 7, '2019-11-12 06:50:18'),
(14113, '', 0.00, 7, '2019-11-12 06:50:18'),
(14114, '', 0.00, 7, '2019-11-12 06:50:19'),
(14115, '', 0.00, 7, '2019-11-12 06:50:19'),
(14116, '', 0.00, 7, '2019-11-12 06:50:19'),
(14117, '', 0.00, 7, '2019-11-12 06:50:19'),
(14118, '', 0.00, 7, '2019-11-12 06:50:19'),
(14119, '', 0.00, 7, '2019-11-12 06:50:19'),
(14120, '', 0.00, 7, '2019-11-12 06:50:19'),
(14121, '', 0.00, 7, '2019-11-12 06:50:19'),
(14122, '', 0.00, 7, '2019-11-12 06:50:19'),
(14123, '', 0.00, 7, '2019-11-12 06:50:19'),
(14124, '', 0.00, 7, '2019-11-12 06:50:20'),
(14125, '', 0.00, 7, '2019-11-12 06:50:20'),
(14126, '', 0.00, 7, '2019-11-12 06:50:20'),
(14127, '', 0.00, 7, '2019-11-12 06:50:21'),
(14128, '', 0.00, 7, '2019-11-12 06:50:21'),
(14129, '', 0.00, 7, '2019-11-12 06:50:21'),
(14130, '', 0.00, 7, '2019-11-12 06:50:21'),
(14131, '', 0.00, 7, '2019-11-12 06:50:21'),
(14132, '', 0.00, 7, '2019-11-12 06:50:21'),
(14133, '', 0.00, 7, '2019-11-12 06:50:21'),
(14134, '', 0.00, 7, '2019-11-12 06:50:21'),
(14135, '', 0.00, 7, '2019-11-12 06:50:21'),
(14136, '', 0.00, 7, '2019-11-12 06:50:21'),
(14137, '', 0.00, 7, '2019-11-12 06:50:21'),
(14138, '', 0.00, 7, '2019-11-12 06:50:22'),
(14139, '', 0.00, 7, '2019-11-12 06:50:22'),
(14140, '', 0.00, 7, '2019-11-12 06:50:22'),
(14141, '', 0.00, 7, '2019-11-12 06:50:22'),
(14142, '', 0.00, 7, '2019-11-12 06:50:22'),
(14143, '', 0.00, 7, '2019-11-12 06:50:22'),
(14144, '', 0.00, 7, '2019-11-12 06:50:22'),
(14145, '', 0.00, 7, '2019-11-12 06:50:22'),
(14146, '', 0.00, 7, '2019-11-12 06:50:22'),
(14147, '', 0.00, 7, '2019-11-12 06:50:22'),
(14148, '', 0.00, 7, '2019-11-12 06:50:22'),
(14149, '', 0.00, 7, '2019-11-12 06:50:22'),
(14150, '', 0.00, 7, '2019-11-12 06:50:22'),
(14151, '', 0.00, 7, '2019-11-12 06:50:22'),
(14152, '', 0.00, 7, '2019-11-12 06:50:22'),
(14153, '', 0.00, 7, '2019-11-12 06:50:23'),
(14154, '', 0.00, 7, '2019-11-12 06:50:23'),
(14155, '', 0.00, 7, '2019-11-12 06:50:23'),
(14156, '', 0.00, 7, '2019-11-12 06:50:23'),
(14157, '', 0.00, 7, '2019-11-12 06:50:23'),
(14158, '', 0.00, 7, '2019-11-12 06:50:23'),
(14159, '', 0.00, 7, '2019-11-12 06:50:23'),
(14160, '', 0.00, 7, '2019-11-12 06:50:23'),
(14161, '', 0.00, 7, '2019-11-12 06:50:23'),
(14162, '', 0.00, 7, '2019-11-12 06:50:23'),
(14163, '', 0.00, 7, '2019-11-12 06:50:23'),
(14164, '', 0.00, 7, '2019-11-12 06:50:23'),
(14165, '', 0.00, 7, '2019-11-12 06:50:24'),
(14166, '', 0.00, 7, '2019-11-12 06:50:24'),
(14167, '', 0.00, 7, '2019-11-12 06:50:24'),
(14168, '', 0.00, 7, '2019-11-12 06:50:24'),
(14169, '', 0.00, 7, '2019-11-12 06:50:24'),
(14170, '', 0.00, 7, '2019-11-12 06:50:24'),
(14171, '', 0.00, 7, '2019-11-12 06:50:24'),
(14172, '', 0.00, 7, '2019-11-12 06:50:24'),
(14173, '', 0.00, 7, '2019-11-12 06:50:24'),
(14174, '', 0.00, 7, '2019-11-12 06:50:24'),
(14175, '', 0.00, 7, '2019-11-12 06:50:24'),
(14176, '', 0.00, 7, '2019-11-12 06:50:24'),
(14177, '', 0.00, 7, '2019-11-12 06:50:24'),
(14178, '', 0.00, 7, '2019-11-12 06:50:25'),
(14179, '', 0.00, 7, '2019-11-12 06:50:25'),
(14180, '', 0.00, 7, '2019-11-12 06:50:25'),
(14181, '', 0.00, 7, '2019-11-12 06:50:25'),
(14182, '', 0.00, 7, '2019-11-12 06:50:25'),
(14183, '', 0.00, 7, '2019-11-12 06:50:25'),
(14184, '', 0.00, 7, '2019-11-12 06:50:25'),
(14185, '', 0.00, 7, '2019-11-12 06:50:25'),
(14186, '', 0.00, 7, '2019-11-12 06:50:25'),
(14187, '', 0.00, 7, '2019-11-12 06:50:25'),
(14188, '', 0.00, 7, '2019-11-12 06:50:25'),
(14189, '', 0.00, 7, '2019-11-12 06:50:25'),
(14190, '', 0.00, 7, '2019-11-12 06:50:25'),
(14191, '', 0.00, 7, '2019-11-12 06:50:25'),
(14192, '', 0.00, 7, '2019-11-12 06:50:26'),
(14193, '', 0.00, 7, '2019-11-12 06:50:26'),
(14194, '', 0.00, 7, '2019-11-12 06:50:26'),
(14195, '', 0.00, 7, '2019-11-12 06:50:26'),
(14196, '', 0.00, 7, '2019-11-12 06:50:26'),
(14197, '', 0.00, 7, '2019-11-12 06:50:26'),
(14198, '', 0.00, 7, '2019-11-12 06:50:26'),
(14199, '', 0.00, 7, '2019-11-12 06:50:26'),
(14200, '', 0.00, 7, '2019-11-12 06:50:26'),
(14201, '', 0.00, 7, '2019-11-12 06:50:26'),
(14202, '', 0.00, 7, '2019-11-12 06:50:26'),
(14203, '', 0.00, 7, '2019-11-12 06:50:26'),
(14204, '', 0.00, 7, '2019-11-12 06:50:26'),
(14205, '', 0.00, 7, '2019-11-12 06:50:27'),
(14206, '', 0.00, 7, '2019-11-12 06:50:27'),
(14207, '', 0.00, 7, '2019-11-12 06:50:27'),
(14208, '', 0.00, 7, '2019-11-12 06:50:27'),
(14209, '', 0.00, 7, '2019-11-12 06:50:27'),
(14210, '', 0.00, 7, '2019-11-12 06:50:27'),
(14211, '', 0.00, 7, '2019-11-12 06:50:27'),
(14212, '', 0.00, 7, '2019-11-12 06:50:28'),
(14213, '', 0.00, 7, '2019-11-12 06:50:28'),
(14214, '', 0.00, 7, '2019-11-12 06:50:28'),
(14215, '', 0.00, 7, '2019-11-12 06:50:28'),
(14216, '', 0.00, 7, '2019-11-12 06:50:28'),
(14217, '', 0.00, 7, '2019-11-12 06:50:28'),
(14218, '', 0.00, 7, '2019-11-12 06:50:28'),
(14219, '', 0.00, 7, '2019-11-12 06:50:28'),
(14220, '', 0.00, 7, '2019-11-12 06:50:28'),
(14221, '', 0.00, 7, '2019-11-12 06:50:28'),
(14222, '', 0.00, 7, '2019-11-12 06:50:28'),
(14223, '', 0.00, 7, '2019-11-12 06:50:28'),
(14224, '', 0.00, 7, '2019-11-12 06:50:29'),
(14225, '', 0.00, 7, '2019-11-12 06:50:29'),
(14226, '', 0.00, 7, '2019-11-12 06:50:29'),
(14227, '', 0.00, 7, '2019-11-12 06:50:29'),
(14228, '', 0.00, 7, '2019-11-12 06:50:29'),
(14229, '', 0.00, 7, '2019-11-12 06:50:29'),
(14230, '', 0.00, 7, '2019-11-12 06:50:29'),
(14231, '', 0.00, 7, '2019-11-12 06:50:29'),
(14232, '', 0.00, 7, '2019-11-12 06:50:29'),
(14233, '', 0.00, 7, '2019-11-12 06:50:29'),
(14234, '', 0.00, 7, '2019-11-12 06:50:29'),
(14235, '', 0.00, 7, '2019-11-12 06:50:29'),
(14236, '', 0.00, 7, '2019-11-12 06:50:29'),
(14237, '', 0.00, 7, '2019-11-12 06:50:29'),
(14238, '', 0.00, 7, '2019-11-12 06:50:29'),
(14239, '', 0.00, 7, '2019-11-12 06:50:29'),
(14240, '', 0.00, 7, '2019-11-12 06:50:29'),
(14241, '', 0.00, 7, '2019-11-12 06:50:29'),
(14242, '', 0.00, 7, '2019-11-12 06:50:30'),
(14243, '', 0.00, 7, '2019-11-12 06:50:30'),
(14244, '', 0.00, 7, '2019-11-12 06:50:30'),
(14245, '', 0.00, 7, '2019-11-12 06:50:30'),
(14246, '', 0.00, 7, '2019-11-12 06:50:30'),
(14247, '', 0.00, 7, '2019-11-12 06:50:30'),
(14248, '', 0.00, 7, '2019-11-12 06:50:30'),
(14249, '', 0.00, 7, '2019-11-12 06:50:30'),
(14250, '', 0.00, 7, '2019-11-12 06:50:30'),
(14251, '', 0.00, 7, '2019-11-12 06:50:31'),
(14252, '', 0.00, 7, '2019-11-12 06:50:31'),
(14253, '', 0.00, 7, '2019-11-12 06:50:31'),
(14254, '', 0.00, 7, '2019-11-12 06:50:31'),
(14255, '', 0.00, 7, '2019-11-12 06:50:31'),
(14256, '', 0.00, 7, '2019-11-12 06:50:31'),
(14257, '', 0.00, 7, '2019-11-12 06:50:31'),
(14258, '', 0.00, 7, '2019-11-12 06:50:31'),
(14259, '', 0.00, 7, '2019-11-12 06:50:31'),
(14260, '', 0.00, 7, '2019-11-12 06:50:31'),
(14261, '', 0.00, 7, '2019-11-12 06:50:31'),
(14262, '', 0.00, 7, '2019-11-12 06:50:31'),
(14263, '', 0.00, 7, '2019-11-12 06:50:31'),
(14264, '', 0.00, 7, '2019-11-12 06:50:31'),
(14265, '', 0.00, 7, '2019-11-12 06:50:31'),
(14266, '', 0.00, 7, '2019-11-12 06:50:31'),
(14267, '', 0.00, 7, '2019-11-12 06:50:31'),
(14268, '', 0.00, 7, '2019-11-12 06:50:31'),
(14269, '', 0.00, 7, '2019-11-12 06:50:31'),
(14270, '', 0.00, 7, '2019-11-12 06:50:32'),
(14271, '', 0.00, 7, '2019-11-12 06:50:32'),
(14272, '', 0.00, 7, '2019-11-12 06:50:32'),
(14273, '', 0.00, 7, '2019-11-12 06:50:32'),
(14274, '', 0.00, 7, '2019-11-12 06:50:32'),
(14275, '', 0.00, 7, '2019-11-12 06:50:32'),
(14276, '', 0.00, 7, '2019-11-12 06:50:32'),
(14277, '', 0.00, 7, '2019-11-12 06:50:32'),
(14278, '', 0.00, 7, '2019-11-12 06:50:32'),
(14279, '', 0.00, 7, '2019-11-12 06:50:32'),
(14280, '', 0.00, 7, '2019-11-12 06:50:32'),
(14281, '', 0.00, 7, '2019-11-12 06:50:32'),
(14282, '\0\0\0', 0.00, 7, '2019-11-12 06:50:32'),
(14283, '\0\0\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:32'),
(14284, '', 0.00, 7, '2019-11-12 06:50:32'),
(14285, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:32'),
(14286, '', 0.00, 7, '2019-11-12 06:50:33'),
(14287, '', 0.00, 7, '2019-11-12 06:50:33'),
(14288, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:33'),
(14289, '', 0.00, 7, '2019-11-12 06:50:33'),
(14290, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:33'),
(14291, '', 0.00, 7, '2019-11-12 06:50:33'),
(14292, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:33'),
(14293, '', 0.00, 7, '2019-11-12 06:50:33'),
(14294, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:33'),
(14295, '', 0.00, 7, '2019-11-12 06:50:33'),
(14296, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:33'),
(14297, '', 0.00, 7, '2019-11-12 06:50:33'),
(14298, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:33'),
(14299, '', 0.00, 7, '2019-11-12 06:50:33'),
(14300, '', 0.00, 7, '2019-11-12 06:50:33'),
(14301, '\0', 0.00, 7, '2019-11-12 06:50:33'),
(14302, '\0\0\0', 0.00, 7, '2019-11-12 06:50:33'),
(14303, '\0\0~', 0.00, 7, '2019-11-12 06:50:33'),
(14304, '\0', 0.00, 7, '2019-11-12 06:50:33'),
(14305, '', 0.00, 7, '2019-11-12 06:50:34'),
(14306, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:34'),
(14307, '\0\0\0', 0.00, 7, '2019-11-12 06:50:34'),
(14308, '', 0.00, 7, '2019-11-12 06:50:34'),
(14309, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:34'),
(14310, '', 0.00, 7, '2019-11-12 06:50:34'),
(14311, '\0\r\0\0\0\r\0\0~', 0.00, 7, '2019-11-12 06:50:34'),
(14312, '', 0.00, 7, '2019-11-12 06:50:34'),
(14313, '', 0.00, 7, '2019-11-12 06:50:34'),
(14314, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:34'),
(14315, '', 0.00, 7, '2019-11-12 06:50:34'),
(14316, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:34'),
(14317, '', 0.00, 7, '2019-11-12 06:50:34'),
(14318, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:34'),
(14319, '', 0.00, 7, '2019-11-12 06:50:34'),
(14320, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:34'),
(14321, '', 0.00, 7, '2019-11-12 06:50:34'),
(14322, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:34'),
(14323, '', 0.00, 7, '2019-11-12 06:50:34'),
(14324, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:35'),
(14325, '', 0.00, 7, '2019-11-12 06:50:35'),
(14326, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:35'),
(14327, '', 0.00, 7, '2019-11-12 06:50:35'),
(14328, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:35'),
(14329, '', 0.00, 7, '2019-11-12 06:50:35'),
(14330, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:35'),
(14331, '', 0.00, 7, '2019-11-12 06:50:35'),
(14332, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:35'),
(14333, '\0\0\0', 0.00, 7, '2019-11-12 06:50:35'),
(14334, '', 0.00, 7, '2019-11-12 06:50:35'),
(14335, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:35'),
(14336, '', 0.00, 7, '2019-11-12 06:50:35'),
(14337, '\0\Z\0\0\0\Z\0\0~', 0.00, 7, '2019-11-12 06:50:35'),
(14338, '', 0.00, 7, '2019-11-12 06:50:35'),
(14339, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:35'),
(14340, '', 0.00, 7, '2019-11-12 06:50:35'),
(14341, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:35'),
(14342, '', 0.00, 7, '2019-11-12 06:50:35'),
(14343, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:35'),
(14344, '', 0.00, 7, '2019-11-12 06:50:35'),
(14345, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:35'),
(14346, '', 0.00, 7, '2019-11-12 06:50:35'),
(14347, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:36'),
(14348, '', 0.00, 7, '2019-11-12 06:50:36'),
(14349, '\0 \0\0\0 \0\0~', 0.00, 7, '2019-11-12 06:50:36'),
(14350, '', 0.00, 7, '2019-11-12 06:50:36'),
(14351, '\0!\0\0\0!\0\0~', 0.00, 7, '2019-11-12 06:50:36'),
(14352, '', 0.00, 7, '2019-11-12 06:50:36'),
(14353, '\0&quot;\0\0\0&quot;\0\0~', 0.00, 7, '2019-11-12 06:50:36'),
(14354, '', 0.00, 7, '2019-11-12 06:50:36'),
(14355, '\0#\0\0\0#\0\0~', 0.00, 7, '2019-11-12 06:50:36'),
(14356, '', 0.00, 7, '2019-11-12 06:50:36'),
(14357, '\0$\0\0\0$\0\0~', 0.00, 7, '2019-11-12 06:50:36'),
(14358, '', 0.00, 7, '2019-11-12 06:50:36'),
(14359, '\0%\0\0\0%\0\0~', 0.00, 7, '2019-11-12 06:50:36'),
(14360, '', 0.00, 7, '2019-11-12 06:50:36'),
(14361, '\0&amp;\0\0\0&amp;\0\0~', 0.00, 7, '2019-11-12 06:50:36'),
(14362, '', 0.00, 7, '2019-11-12 06:50:36'),
(14363, '\0\'\0\0\0\'\0\0~', 0.00, 7, '2019-11-12 06:50:36'),
(14364, '', 0.00, 7, '2019-11-12 06:50:36'),
(14365, '\0(\0\0\0(\0\0~', 0.00, 7, '2019-11-12 06:50:37'),
(14366, '', 0.00, 7, '2019-11-12 06:50:37'),
(14367, '\0)\0\0\0)\0\0~', 0.00, 7, '2019-11-12 06:50:37'),
(14368, '', 0.00, 7, '2019-11-12 06:50:37'),
(14369, '\0*\0\0\0*\0\0~', 0.00, 7, '2019-11-12 06:50:37'),
(14370, '', 0.00, 7, '2019-11-12 06:50:37'),
(14371, '\0+\0\0\0+\0\0~', 0.00, 7, '2019-11-12 06:50:37'),
(14372, '', 0.00, 7, '2019-11-12 06:50:37'),
(14373, '\0', 0.00, 7, '2019-11-12 06:50:37'),
(14374, '\0', 0.00, 7, '2019-11-12 06:50:37'),
(14375, '\0-\0\0\0-\0\0~', 0.00, 7, '2019-11-12 06:50:38'),
(14376, '', 0.00, 7, '2019-11-12 06:50:38'),
(14377, '\0.\0\0\0.\0\0~', 0.00, 7, '2019-11-12 06:50:39'),
(14378, '', 0.00, 7, '2019-11-12 06:50:39'),
(14379, '\0/\0\0\0/\0\0~', 0.00, 7, '2019-11-12 06:50:39'),
(14380, '', 0.00, 7, '2019-11-12 06:50:39'),
(14381, '\00\0\0\00\0\0~', 0.00, 7, '2019-11-12 06:50:39'),
(14382, '', 0.00, 7, '2019-11-12 06:50:40'),
(14383, '', 0.00, 7, '2019-11-12 06:50:40'),
(14384, '\02\0\0\02\0\0~', 0.00, 7, '2019-11-12 06:50:40'),
(14385, '', 0.00, 7, '2019-11-12 06:50:40'),
(14386, '\03\0\0\03\0\0~', 0.00, 7, '2019-11-12 06:50:40'),
(14387, '', 0.00, 7, '2019-11-12 06:50:40'),
(14388, '\04\0\0\04\0\0~', 0.00, 7, '2019-11-12 06:50:40'),
(14389, '', 0.00, 7, '2019-11-12 06:50:40'),
(14390, '\05\0\0\05\0\0~', 0.00, 7, '2019-11-12 06:50:40'),
(14391, '', 0.00, 7, '2019-11-12 06:50:40'),
(14392, '\06\0\0\06\0\0~', 0.00, 7, '2019-11-12 06:50:40'),
(14393, '\06\0\0', 0.00, 7, '2019-11-12 06:50:40'),
(14394, '', 0.00, 7, '2019-11-12 06:50:40'),
(14395, '\07\0\0\07\0\0~', 0.00, 7, '2019-11-12 06:50:40'),
(14396, '', 0.00, 7, '2019-11-12 06:50:40'),
(14397, '\08\0\0\08\0\0~', 0.00, 7, '2019-11-12 06:50:40'),
(14398, '', 0.00, 7, '2019-11-12 06:50:40'),
(14399, '\09\0\0\09\0\0~', 0.00, 7, '2019-11-12 06:50:40'),
(14400, '', 0.00, 7, '2019-11-12 06:50:40'),
(14401, '\0:\0\0\0:\0\0~', 0.00, 7, '2019-11-12 06:50:40'),
(14402, '', 0.00, 7, '2019-11-12 06:50:40'),
(14403, '\0;\0\0\0;\0\0~', 0.00, 7, '2019-11-12 06:50:40'),
(14404, '', 0.00, 7, '2019-11-12 06:50:40'),
(14405, '\0&lt;\0\0\0&lt;\0\0~', 0.00, 7, '2019-11-12 06:50:40'),
(14406, '', 0.00, 7, '2019-11-12 06:50:40'),
(14407, '\0=\0\0\0=\0\0~', 0.00, 7, '2019-11-12 06:50:41'),
(14408, '', 0.00, 7, '2019-11-12 06:50:41'),
(14409, '\0&gt;\0\0\0&gt;\0\0~', 0.00, 7, '2019-11-12 06:50:41'),
(14410, '', 0.00, 7, '2019-11-12 06:50:41'),
(14411, '\0?\0\0\0?\0\0~', 0.00, 7, '2019-11-12 06:50:41'),
(14412, '\0?\0\0!\0\0\0@\0\0\0', 0.00, 7, '2019-11-12 06:50:41'),
(14413, '\0@\0\0\0@\0\0~', 0.00, 7, '2019-11-12 06:50:41'),
(14414, '', 0.00, 7, '2019-11-12 06:50:41'),
(14415, '\0A\0\0\0A\0\0~', 0.00, 7, '2019-11-12 06:50:41'),
(14416, '', 0.00, 7, '2019-11-12 06:50:41'),
(14417, '\0B\0\0\0B\0\0~', 0.00, 7, '2019-11-12 06:50:41'),
(14418, '', 0.00, 7, '2019-11-12 06:50:41'),
(14419, '\0C\0\0\0C\0\0~', 0.00, 7, '2019-11-12 06:50:41'),
(14420, '', 0.00, 7, '2019-11-12 06:50:41'),
(14421, '\0D\0\0\0D\0\0~', 0.00, 7, '2019-11-12 06:50:41'),
(14422, '', 0.00, 7, '2019-11-12 06:50:41'),
(14423, '\0E\0\0\0E\0\0~', 0.00, 7, '2019-11-12 06:50:41'),
(14424, '', 0.00, 7, '2019-11-12 06:50:42'),
(14425, '\0F\0\0\0F\0\0~', 0.00, 7, '2019-11-12 06:50:42'),
(14426, '', 0.00, 7, '2019-11-12 06:50:42'),
(14427, '\0G\0\0\0G\0\0~', 0.00, 7, '2019-11-12 06:50:42'),
(14428, '', 0.00, 7, '2019-11-12 06:50:42'),
(14429, '\0H\0\0\0H\0\0~', 0.00, 7, '2019-11-12 06:50:42'),
(14430, '', 0.00, 7, '2019-11-12 06:50:42'),
(14431, '\0I\0\0\0I\0\0~', 0.00, 7, '2019-11-12 06:50:42'),
(14432, '', 0.00, 7, '2019-11-12 06:50:42'),
(14433, '\0J\0\0\0J\0\0~', 0.00, 7, '2019-11-12 06:50:42'),
(14434, '', 0.00, 7, '2019-11-12 06:50:42'),
(14435, '\0K\0\0\0K\0\0~', 0.00, 7, '2019-11-12 06:50:42'),
(14436, '', 0.00, 7, '2019-11-12 06:50:42'),
(14437, '\0L\0\0\0L\0\0~', 0.00, 7, '2019-11-12 06:50:42'),
(14438, '', 0.00, 7, '2019-11-12 06:50:42'),
(14439, '\0M\0\0\0M\0\0~', 0.00, 7, '2019-11-12 06:50:42'),
(14440, '', 0.00, 7, '2019-11-12 06:50:42'),
(14441, '\0N\0\0\0N\0\0~', 0.00, 7, '2019-11-12 06:50:42'),
(14442, '', 0.00, 7, '2019-11-12 06:50:42'),
(14443, '\0O\0\0\0O\0\0~', 0.00, 7, '2019-11-12 06:50:42'),
(14444, '', 0.00, 7, '2019-11-12 06:50:42'),
(14445, '\0P\0\0\0P\0\0~', 0.00, 7, '2019-11-12 06:50:42'),
(14446, '', 0.00, 7, '2019-11-12 06:50:43'),
(14447, '\0Q\0\0\0Q\0\0~', 0.00, 7, '2019-11-12 06:50:43'),
(14448, '', 0.00, 7, '2019-11-12 06:50:43'),
(14449, '\0R\0\0\0R\0\0~', 0.00, 7, '2019-11-12 06:50:43'),
(14450, '', 0.00, 7, '2019-11-12 06:50:43'),
(14451, '\0S\0\0\0S\0\0~', 0.00, 7, '2019-11-12 06:50:43'),
(14452, '', 0.00, 7, '2019-11-12 06:50:43'),
(14453, '\0T\0\0\0T\0\0~', 0.00, 7, '2019-11-12 06:50:43'),
(14454, '\0T\0\0R', 0.00, 7, '2019-11-12 06:50:43'),
(14455, '', 0.00, 7, '2019-11-12 06:50:43'),
(14456, '\0U\0\0\0U\0\0~', 0.00, 7, '2019-11-12 06:50:43'),
(14457, '', 0.00, 7, '2019-11-12 06:50:43'),
(14458, '\0V\0\0\0V\0\0~', 0.00, 7, '2019-11-12 06:50:43'),
(14459, '', 0.00, 7, '2019-11-12 06:50:43'),
(14460, '\0W\0\0\0W\0\0~', 0.00, 7, '2019-11-12 06:50:44'),
(14461, '', 0.00, 7, '2019-11-12 06:50:44'),
(14462, '\0X\0\0\0X\0\0~', 0.00, 7, '2019-11-12 06:50:44'),
(14463, '', 0.00, 7, '2019-11-12 06:50:44'),
(14464, '\0Y\0\0\0Y\0\0~', 0.00, 7, '2019-11-12 06:50:44'),
(14465, '', 0.00, 7, '2019-11-12 06:50:44'),
(14466, '\0Z\0\0\0Z\0\0~', 0.00, 7, '2019-11-12 06:50:44'),
(14467, '', 0.00, 7, '2019-11-12 06:50:44'),
(14468, '', 0.00, 7, '2019-11-12 06:50:44'),
(14469, '\0\\\0\0\0\\\0\0~', 0.00, 7, '2019-11-12 06:50:44'),
(14470, '', 0.00, 7, '2019-11-12 06:50:44'),
(14471, '\0]\0\0\0]\0\0~', 0.00, 7, '2019-11-12 06:50:44'),
(14472, '', 0.00, 7, '2019-11-12 06:50:44'),
(14473, '', 0.00, 7, '2019-11-12 06:50:44'),
(14474, '\0_\0\0\0_\0\0~', 0.00, 7, '2019-11-12 06:50:44'),
(14475, '\0_\0\0*\0\0\0`\0\0\0', 0.00, 7, '2019-11-12 06:50:44'),
(14476, '\0`\0\0\0`\0\0~', 0.00, 7, '2019-11-12 06:50:44'),
(14477, '', 0.00, 7, '2019-11-12 06:50:44'),
(14478, '\0a\0\0\0a\0\0~', 0.00, 7, '2019-11-12 06:50:44'),
(14479, '\0a\0\0', 0.00, 7, '2019-11-12 06:50:44'),
(14480, '', 0.00, 7, '2019-11-12 06:50:45'),
(14481, '\0b\0\0\0b\0\0~', 0.00, 7, '2019-11-12 06:50:45'),
(14482, '', 0.00, 7, '2019-11-12 06:50:45'),
(14483, '\0c\0\0\0c\0\0~', 0.00, 7, '2019-11-12 06:50:45'),
(14484, '', 0.00, 7, '2019-11-12 06:50:45'),
(14485, '\0d\0\0\0d\0\0~', 0.00, 7, '2019-11-12 06:50:45'),
(14486, '', 0.00, 7, '2019-11-12 06:50:45'),
(14487, '\0e\0\0\0e\0\0~', 0.00, 7, '2019-11-12 06:50:45'),
(14488, '', 0.00, 7, '2019-11-12 06:50:45'),
(14489, '\0f\0\0\0f\0\0~', 0.00, 7, '2019-11-12 06:50:45'),
(14490, '', 0.00, 7, '2019-11-12 06:50:45'),
(14491, '\0g\0\0\0g\0\0~', 0.00, 7, '2019-11-12 06:50:45'),
(14492, '', 0.00, 7, '2019-11-12 06:50:45'),
(14493, '\0h\0\0\0h\0\0~', 0.00, 7, '2019-11-12 06:50:45'),
(14494, '', 0.00, 7, '2019-11-12 06:50:46'),
(14495, '\0i\0\0\0i\0\0~', 0.00, 7, '2019-11-12 06:50:46'),
(14496, '', 0.00, 7, '2019-11-12 06:50:46'),
(14497, '\0j\0\0\0j\0\0~', 0.00, 7, '2019-11-12 06:50:46'),
(14498, '', 0.00, 7, '2019-11-12 06:50:46'),
(14499, '\0k\0\0\0k\0\0~', 0.00, 7, '2019-11-12 06:50:46'),
(14500, '', 0.00, 7, '2019-11-12 06:50:46'),
(14501, '\0l\0\0\0l\0\0~', 0.00, 7, '2019-11-12 06:50:46'),
(14502, '', 0.00, 7, '2019-11-12 06:50:46'),
(14503, '\0m\0\0\0m\0\0~', 0.00, 7, '2019-11-12 06:50:46'),
(14504, '', 0.00, 7, '2019-11-12 06:50:46'),
(14505, '\0n\0\0\0n\0\0~', 0.00, 7, '2019-11-12 06:50:46'),
(14506, '', 0.00, 7, '2019-11-12 06:50:46'),
(14507, '\0o\0\0\0o\0\0~', 0.00, 7, '2019-11-12 06:50:46'),
(14508, '', 0.00, 7, '2019-11-12 06:50:46'),
(14509, '\0p\0\0\0p\0\0~', 0.00, 7, '2019-11-12 06:50:46'),
(14510, '', 0.00, 7, '2019-11-12 06:50:46'),
(14511, '', 0.00, 7, '2019-11-12 06:50:46'),
(14512, '\0r\0\0\0r\0\0~', 0.00, 7, '2019-11-12 06:50:46'),
(14513, '', 0.00, 7, '2019-11-12 06:50:46'),
(14514, '\0s\0\0\0s\0\0~', 0.00, 7, '2019-11-12 06:50:47'),
(14515, '', 0.00, 7, '2019-11-12 06:50:47'),
(14516, '\0t\0\0\0t\0\0~', 0.00, 7, '2019-11-12 06:50:47'),
(14517, '', 0.00, 7, '2019-11-12 06:50:47'),
(14518, '\0u\0\0\0u\0\0~', 0.00, 7, '2019-11-12 06:50:47'),
(14519, '', 0.00, 7, '2019-11-12 06:50:47'),
(14520, '\0v\0\0\0v\0\0~', 0.00, 7, '2019-11-12 06:50:47'),
(14521, '', 0.00, 7, '2019-11-12 06:50:47'),
(14522, '\0w\0\0\0w\0\0~', 0.00, 7, '2019-11-12 06:50:47'),
(14523, '\0w\0\0', 0.00, 7, '2019-11-12 06:50:47'),
(14524, '', 0.00, 7, '2019-11-12 06:50:47'),
(14525, '\0x\0\0\0x\0\0~', 0.00, 7, '2019-11-12 06:50:47'),
(14526, '', 0.00, 7, '2019-11-12 06:50:47'),
(14527, '\0y\0\0\0y\0\0~', 0.00, 7, '2019-11-12 06:50:47'),
(14528, '', 0.00, 7, '2019-11-12 06:50:47'),
(14529, '\0z\0\0\0z\0\0~', 0.00, 7, '2019-11-12 06:50:48'),
(14530, '', 0.00, 7, '2019-11-12 06:50:48'),
(14531, '\0{\0\0\0{\0\0~', 0.00, 7, '2019-11-12 06:50:48'),
(14532, '', 0.00, 7, '2019-11-12 06:50:48'),
(14533, '\0|\0\0\0|\0\0~', 0.00, 7, '2019-11-12 06:50:48'),
(14534, '', 0.00, 7, '2019-11-12 06:50:48'),
(14535, '\0}\0\0\0}\0\0~', 0.00, 7, '2019-11-12 06:50:48'),
(14536, '', 0.00, 7, '2019-11-12 06:50:48'),
(14537, '', 0.00, 7, '2019-11-12 06:50:48'),
(14538, '', 0.00, 7, '2019-11-12 06:50:48'),
(14539, '\0\0\0\0\0\0~', 0.00, 7, '2019-11-12 06:50:48'),
(14540, '', 0.00, 7, '2019-11-12 06:50:48'),
(14541, '', 0.00, 7, '2019-11-12 06:50:48'),
(14542, '', 0.00, 7, '2019-11-12 06:50:48'),
(14543, '', 0.00, 7, '2019-11-12 06:50:48'),
(14544, '', 0.00, 7, '2019-11-12 06:50:48'),
(14545, '', 0.00, 7, '2019-11-12 06:50:48'),
(14546, '', 0.00, 7, '2019-11-12 06:50:48'),
(14547, '', 0.00, 7, '2019-11-12 06:50:48'),
(14548, '', 0.00, 7, '2019-11-12 06:50:48'),
(14549, '', 0.00, 7, '2019-11-12 06:50:48'),
(14550, '', 0.00, 7, '2019-11-12 06:50:48'),
(14551, '', 0.00, 7, '2019-11-12 06:50:48'),
(14552, '', 0.00, 7, '2019-11-12 06:50:48'),
(14553, '', 0.00, 7, '2019-11-12 06:50:48'),
(14554, '', 0.00, 7, '2019-11-12 06:50:49'),
(14555, '', 0.00, 7, '2019-11-12 06:50:49'),
(14556, '', 0.00, 7, '2019-11-12 06:50:50'),
(14557, '', 0.00, 7, '2019-11-12 06:50:50'),
(14558, '', 0.00, 7, '2019-11-12 06:50:50'),
(14559, '', 0.00, 7, '2019-11-12 06:50:50'),
(14560, '', 0.00, 7, '2019-11-12 06:50:50'),
(14561, '', 0.00, 7, '2019-11-12 06:50:50'),
(14562, '', 0.00, 7, '2019-11-12 06:50:50'),
(14563, '', 0.00, 7, '2019-11-12 06:50:50'),
(14564, '', 0.00, 7, '2019-11-12 06:50:51'),
(14565, '', 0.00, 7, '2019-11-12 06:50:51'),
(14566, '', 0.00, 7, '2019-11-12 06:50:51'),
(14567, '', 0.00, 7, '2019-11-12 06:50:51'),
(14568, '', 0.00, 7, '2019-11-12 06:50:51'),
(14569, '', 0.00, 7, '2019-11-12 06:50:51'),
(14570, '', 0.00, 7, '2019-11-12 06:50:51'),
(14571, '', 0.00, 7, '2019-11-12 06:50:51'),
(14572, '', 0.00, 7, '2019-11-12 06:50:51'),
(14573, '', 0.00, 7, '2019-11-12 06:50:51'),
(14574, '', 0.00, 7, '2019-11-12 06:50:51'),
(14575, '', 0.00, 7, '2019-11-12 06:50:51'),
(14576, '', 0.00, 7, '2019-11-12 06:50:51'),
(14577, '', 0.00, 7, '2019-11-12 06:50:51'),
(14578, '', 0.00, 7, '2019-11-12 06:50:51'),
(14579, '', 0.00, 7, '2019-11-12 06:50:51'),
(14580, '', 0.00, 7, '2019-11-12 06:50:51'),
(14581, '', 0.00, 7, '2019-11-12 06:50:51'),
(14582, '', 0.00, 7, '2019-11-12 06:50:51'),
(14583, '', 0.00, 7, '2019-11-12 06:50:51'),
(14584, '', 0.00, 7, '2019-11-12 06:50:52'),
(14585, '', 0.00, 7, '2019-11-12 06:50:52'),
(14586, '', 0.00, 7, '2019-11-12 06:50:52'),
(14587, '', 0.00, 7, '2019-11-12 06:50:52'),
(14588, '', 0.00, 7, '2019-11-12 06:50:52'),
(14589, '', 0.00, 7, '2019-11-12 06:50:52'),
(14590, '', 0.00, 7, '2019-11-12 06:50:52'),
(14591, '', 0.00, 7, '2019-11-12 06:50:52'),
(14592, '', 0.00, 7, '2019-11-12 06:50:52'),
(14593, '', 0.00, 7, '2019-11-12 06:50:52'),
(14594, '', 0.00, 7, '2019-11-12 06:50:52'),
(14595, '', 0.00, 7, '2019-11-12 06:50:52'),
(14596, '', 0.00, 7, '2019-11-12 06:50:52'),
(14597, '', 0.00, 7, '2019-11-12 06:50:52'),
(14598, '', 0.00, 7, '2019-11-12 06:50:52'),
(14599, '', 0.00, 7, '2019-11-12 06:50:52'),
(14600, '', 0.00, 7, '2019-11-12 06:50:53'),
(14601, '', 0.00, 7, '2019-11-12 06:50:53'),
(14602, '', 0.00, 7, '2019-11-12 06:50:53'),
(14603, '', 0.00, 7, '2019-11-12 06:50:53'),
(14604, '', 0.00, 7, '2019-11-12 06:50:53'),
(14605, '', 0.00, 7, '2019-11-12 06:50:53'),
(14606, '', 0.00, 7, '2019-11-12 06:50:53'),
(14607, '', 0.00, 7, '2019-11-12 06:50:53'),
(14608, '', 0.00, 7, '2019-11-12 06:50:53'),
(14609, '', 0.00, 7, '2019-11-12 06:50:53'),
(14610, '', 0.00, 7, '2019-11-12 06:50:53'),
(14611, '', 0.00, 7, '2019-11-12 06:50:53'),
(14612, '', 0.00, 7, '2019-11-12 06:50:54'),
(14613, '', 0.00, 7, '2019-11-12 06:50:54'),
(14614, '', 0.00, 7, '2019-11-12 06:50:54'),
(14615, '', 0.00, 7, '2019-11-12 06:50:54'),
(14616, '', 0.00, 7, '2019-11-12 06:50:54'),
(14617, '', 0.00, 7, '2019-11-12 06:50:55'),
(14618, '', 0.00, 7, '2019-11-12 06:50:55'),
(14619, '', 0.00, 7, '2019-11-12 06:50:55'),
(14620, '', 0.00, 7, '2019-11-12 06:50:55'),
(14621, '', 0.00, 7, '2019-11-12 06:50:55'),
(14622, '', 0.00, 7, '2019-11-12 06:50:55'),
(14623, '', 0.00, 7, '2019-11-12 06:50:55'),
(14624, '', 0.00, 7, '2019-11-12 06:50:55'),
(14625, '', 0.00, 7, '2019-11-12 06:50:55'),
(14626, '', 0.00, 7, '2019-11-12 06:50:55'),
(14627, '', 0.00, 7, '2019-11-12 06:50:55'),
(14628, '', 0.00, 7, '2019-11-12 06:50:55'),
(14629, '', 0.00, 7, '2019-11-12 06:50:55'),
(14630, '', 0.00, 7, '2019-11-12 06:50:56'),
(14631, '', 0.00, 7, '2019-11-12 06:50:56'),
(14632, '', 0.00, 7, '2019-11-12 06:50:56'),
(14633, '', 0.00, 7, '2019-11-12 06:50:56'),
(14634, '', 0.00, 7, '2019-11-12 06:50:56'),
(14635, '', 0.00, 7, '2019-11-12 06:50:56'),
(14636, '', 0.00, 7, '2019-11-12 06:50:56'),
(14637, '', 0.00, 7, '2019-11-12 06:50:56'),
(14638, '', 0.00, 7, '2019-11-12 06:50:56'),
(14639, '', 0.00, 7, '2019-11-12 06:50:56'),
(14640, '', 0.00, 7, '2019-11-12 06:50:56'),
(14641, '', 0.00, 7, '2019-11-12 06:50:56'),
(14642, '', 0.00, 7, '2019-11-12 06:50:56'),
(14643, '', 0.00, 7, '2019-11-12 06:50:56'),
(14644, '', 0.00, 7, '2019-11-12 06:50:56'),
(14645, '', 0.00, 7, '2019-11-12 06:50:56'),
(14646, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', 0.00, 7, '2019-11-12 06:50:56'),
(14647, 'test', 12.00, 10, '2019-11-21 12:44:20'),
(14648, 'test', 2323.00, 10, '2019-11-21 12:44:20'),
(14649, 'tt', 222.00, 10, '2019-11-21 12:44:20'),
(14650, 'test', 12.00, 9, '2019-12-03 04:03:29'),
(14651, 'test', 2323.00, 9, '2019-12-03 04:03:29'),
(14652, 'tt', 222.00, 9, '2019-12-03 04:03:29');

-- --------------------------------------------------------

--
-- Table structure for table `xin_hospital_services`
--

CREATE TABLE `xin_hospital_services` (
  `id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `service_price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `xin_hourly_templates`
--

CREATE TABLE `xin_hourly_templates` (
  `hourly_rate_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `hourly_grade` varchar(255) NOT NULL,
  `hourly_rate` varchar(255) NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_hrsale_invoices`
--

CREATE TABLE `xin_hrsale_invoices` (
  `invoice_id` int(111) NOT NULL,
  `invoice_number` varchar(255) NOT NULL,
  `client_id` int(11) NOT NULL,
  `project_id` int(111) NOT NULL,
  `invoice_date` varchar(255) NOT NULL,
  `invoice_due_date` varchar(255) NOT NULL,
  `sub_total_amount` varchar(255) NOT NULL,
  `discount_type` varchar(11) NOT NULL,
  `discount_figure` varchar(255) NOT NULL,
  `total_tax` varchar(255) NOT NULL,
  `total_discount` varchar(255) NOT NULL,
  `grand_total` varchar(255) NOT NULL,
  `invoice_note` mediumtext NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT 'null',
  `company_name` varchar(200) NOT NULL DEFAULT 'null',
  `client_profile` varchar(200) NOT NULL DEFAULT 'null',
  `email` varchar(200) NOT NULL DEFAULT 'null',
  `contact_number` varchar(200) NOT NULL DEFAULT 'null',
  `website_url` varchar(200) NOT NULL DEFAULT 'null',
  `address_1` text NOT NULL,
  `address_2` text NOT NULL,
  `city` varchar(200) NOT NULL DEFAULT 'null',
  `state` varchar(200) NOT NULL DEFAULT 'null',
  `zipcode` varchar(200) NOT NULL DEFAULT 'null',
  `countryid` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_hrsale_invoices`
--

INSERT INTO `xin_hrsale_invoices` (`invoice_id`, `invoice_number`, `client_id`, `project_id`, `invoice_date`, `invoice_due_date`, `sub_total_amount`, `discount_type`, `discount_figure`, `total_tax`, `total_discount`, `grand_total`, `invoice_note`, `name`, `company_name`, `client_profile`, `email`, `contact_number`, `website_url`, `address_1`, `address_2`, `city`, `state`, `zipcode`, `countryid`, `status`, `created_at`) VALUES
(1, 'INV-0001', 4, 1, '2019-11-23', '2019-11-23', '10000.00', '1', '0', '0.00', '0', '10000.00', '', 'Muhammad', '5', 'client_photo_1572692867.png', 'John1122@gmail.com', '32434343', '', 'sss', '', '', '18', '', 0, 0, '23-11-2019 18:04:04');

-- --------------------------------------------------------

--
-- Table structure for table `xin_hrsale_invoices_items`
--

CREATE TABLE `xin_hrsale_invoices_items` (
  `invoice_item_id` int(111) NOT NULL,
  `invoice_id` int(111) NOT NULL,
  `project_id` int(111) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_tax_type` varchar(255) NOT NULL,
  `item_tax_rate` varchar(255) NOT NULL,
  `item_qty` varchar(255) NOT NULL,
  `item_unit_price` varchar(255) NOT NULL,
  `item_sub_total` varchar(255) NOT NULL,
  `sub_total_amount` varchar(255) NOT NULL,
  `total_tax` varchar(255) NOT NULL,
  `discount_type` int(11) NOT NULL,
  `discount_figure` varchar(255) NOT NULL,
  `total_discount` varchar(255) NOT NULL,
  `grand_total` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_hrsale_invoices_items`
--

INSERT INTO `xin_hrsale_invoices_items` (`invoice_item_id`, `invoice_id`, `project_id`, `item_name`, `item_tax_type`, `item_tax_rate`, `item_qty`, `item_unit_price`, `item_sub_total`, `sub_total_amount`, `total_tax`, `discount_type`, `discount_figure`, `total_discount`, `grand_total`, `created_at`) VALUES
(1, 1, 1, 'test', '1', '0', '1', '10000', '10000.00', '10000.00', '0.00', 1, '0', '0', '10000.00', '23-11-2019 18:04:04');

-- --------------------------------------------------------

--
-- Table structure for table `xin_hrsale_module_attributes`
--

CREATE TABLE `xin_hrsale_module_attributes` (
  `custom_field_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `attribute` varchar(255) NOT NULL,
  `attribute_label` varchar(255) NOT NULL,
  `attribute_type` varchar(255) NOT NULL,
  `validation` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `xin_hrsale_module_attributes_select_value`
--

CREATE TABLE `xin_hrsale_module_attributes_select_value` (
  `attributes_select_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `select_label` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `xin_hrsale_module_attributes_values`
--

CREATE TABLE `xin_hrsale_module_attributes_values` (
  `attributes_value_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `module_attributes_id` int(11) NOT NULL,
  `attribute_value` text NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `xin_income_categories`
--

CREATE TABLE `xin_income_categories` (
  `category_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_income_categories`
--

INSERT INTO `xin_income_categories` (`category_id`, `name`, `status`, `created_at`) VALUES
(1, 'Envato', 1, '25-03-2018 09:36:20'),
(2, 'Salary', 1, '25-03-2018 09:36:28'),
(3, 'Other Income', 1, '25-03-2018 09:36:32'),
(4, 'Interest Income', 1, '25-03-2018 09:36:53'),
(5, 'Part Time Work', 1, '25-03-2018 09:37:13'),
(6, 'Regular Income', 1, '25-03-2018 09:37:17');

-- --------------------------------------------------------

--
-- Table structure for table `xin_jobs`
--

CREATE TABLE `xin_jobs` (
  `job_id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `designation_id` int(111) NOT NULL,
  `job_url` varchar(255) NOT NULL,
  `job_type` int(225) NOT NULL,
  `category_url` varchar(255) NOT NULL,
  `is_featured` int(11) NOT NULL,
  `type_url` varchar(255) NOT NULL,
  `job_vacancy` int(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `minimum_experience` varchar(255) NOT NULL,
  `date_of_closing` varchar(200) NOT NULL,
  `short_description` mediumtext NOT NULL,
  `long_description` mediumtext NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_jobs`
--

INSERT INTO `xin_jobs` (`job_id`, `employer_id`, `category_id`, `company_id`, `job_title`, `designation_id`, `job_url`, `job_type`, `category_url`, `is_featured`, `type_url`, `job_vacancy`, `gender`, `minimum_experience`, `date_of_closing`, `short_description`, `long_description`, `status`, `created_at`) VALUES
(1, 2, 5, 0, 'Business Development', 0, 'pUYSGerwflsHDVTaChPngmb79kivLt2M1OFARjKq', 2, '', 1, '', 2, '0', '0', '2019-09-22', 'Business Development', '&lt;p&gt;Business Development&lt;/p&gt;', 1, '2019-09-22 07:55:00');

-- --------------------------------------------------------

--
-- Table structure for table `xin_job_applications`
--

CREATE TABLE `xin_job_applications` (
  `application_id` int(111) NOT NULL,
  `job_id` int(111) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `user_id` int(111) NOT NULL,
  `message` mediumtext NOT NULL,
  `job_resume` mediumtext NOT NULL,
  `application_status` varchar(200) NOT NULL,
  `application_remarks` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_job_applications`
--

INSERT INTO `xin_job_applications` (`application_id`, `job_id`, `full_name`, `email`, `user_id`, `message`, `job_resume`, `application_status`, `application_remarks`, `created_at`) VALUES
(1, 1, 'Kennedy Job', 'kennedyjob@yahoo.com', 2, '<p>thanks for letting me apply for Business Development</p>', 'resume_1569175005.pdf', 'Applied', '', '2019-09-22 07:56:44');

-- --------------------------------------------------------

--
-- Table structure for table `xin_job_categories`
--

CREATE TABLE `xin_job_categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_url` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_job_categories`
--

INSERT INTO `xin_job_categories` (`category_id`, `category_name`, `category_url`, `created_at`) VALUES
(1, 'PHP', 'q7VJh5xWwr56ycN0mAou4266iOY8l1BbMd6H2D3rkFnjU9LgC', '2018-04-15'),
(2, 'Android', 'q7VJh5xWwr56ycN0m34Aou4266iOY8l1BbMd6H2D3rkFnjU9LgC', '2018-04-15'),
(3, 'WordPress', 'q2327VJh5xWwr56ycN0mAou4266iOY8l1BbMd6H2D3rkFnjU9LgC', '2018-04-15'),
(4, 'Design', '0oplfq7VJh5xWwr56ycN0mAou4266iOY8l1BbMd6H2D3rkFnjU9LgC', '2018-04-15'),
(5, 'Developer', '34e6zyr56ycN0mAou4266iOY8l1BbMd6H2D3rkFnjU9LgC', '2018-04-15'),
(6, 'iOS', 'l1BbMd6H2D3rkFnjU9LgCq7VJh5xWwr56ycN0mAou4266iOY8', '2018-04-15'),
(7, 'Mobile', 'l1BbMd6H2D3rkFnjU9LgCH2D3rkFnjU9BbMd6H2D3r', '2018-04-15'),
(8, 'MySQL', '2D3rkFnjU9LgCq7VJh5xWwl1BbMd6H2D3rkFnjU9LgCq7VJh5xWwr56ycN0mAou4266iOY8', '2018-04-15'),
(9, 'JavaScript', 'gCq7VJh5xWwl1BbMd6H2D3rkFnjU9LgCq7VJh5xWwl1BbMd6H2D3rkFnjU9LgCq7VJh5xWwr56ycN0mAou4266iOY8', '2018-04-15'),
(10, 'Software', 'zyr56ycN0mAou42634e6zyr56ycN0mAou4266iOY8l1BbMd6H2D3rkFnjU9LgC', '2018-04-15'),
(11, 'Website Design', '6iOY8l1BbMd6H2D3rkFnjU9LgCzyr56ycN0mAou42634e6zyr56ycN0mAou426', '2018-04-15'),
(12, 'Programming', 'jU9LgCzyr56ycN0mAou4266iOY8l1BbMd6H2D3rkFn34e6zyr56ycN0mAou426', '2018-04-15'),
(13, 'SEO', 'cN0mAou4266iOY8l1Bq2327VJh5xWwr56ybMd6H2D3rkFnjU9LgC', '2018-04-15'),
(14, 'Java', 'VJh5xWwr56ybMd6H2DcN0mAou4266iOY8l1Bq23273rkFnjU9LgC', '2018-04-15'),
(15, 'CSS', 'VJh5xWwr56ybMd6H2Dsdfkkj58234ksklEr6ybMd6H2D', '2018-04-15'),
(16, 'HTML5', '0349324k0434r23ksodfkpsodkfposakfkpww3MsH2Dei30ks', '2018-04-15'),
(17, 'Web Development', 'sdfj0rkskfskdfj329FLE34LFMsH2Dei30ks0349324k0434', '2018-04-15'),
(18, 'Web Design', 'MsH2Deiee30ks0349324k0434klEr6ybMd6234b5ksddif0k33', '2018-04-15'),
(19, 'eCommerce', 'klEr6ybMd6234bMsH2Dei30ks0349324k04345ksddif0k33', '2018-04-15'),
(20, 'Design', '234bMsklEr6ybMd6H2Dssdk5yy98ooVJh5xWwr56y435gghhole93lfkkj58', '2018-04-15'),
(21, 'Logo Design', 'k5yy98ooVJh5xWw45456y435gghhole93lfkkj58234bMsklEr6ybMd6H2D', '2018-04-15'),
(22, 'Graphic Design', 'k5yy98ooVJh5xWwr56y435gghhole93lfkkj58234bMsklEr6ybMd6H2D', '2018-04-15'),
(23, 'Video', 'k98ooVJh5xWwr56y435gghhole93lfkkj58234bMsklEr6ybMd6H2D', '2018-04-15'),
(24, 'Animation', 'ole93lfkkj58234k98ooVJh5xWwr56ybMsklEr6ybMd6H2D', '2018-04-15'),
(25, 'Adobe Photoshop', 'd6H2Dsdfkkj58234k98ooVJh5xWwr56ybMsklEr6ybMd6H2D', '2018-04-15'),
(26, 'Illustration', 'xWwr56ybMd6H2DcN0mA3405kfVJh5ou4266iOY8l1Bq23273rkFnjU9LgC', '2018-04-15'),
(27, 'Art', '3405kfVJh5ou4266iOY8l1Bq23273rk3ok3xWwr56ybMd6H2DcN0mAFnjU9LgC', '2018-04-15'),
(28, '3D', 'Md6H2DcN0mAFnjU9LfVJh5ou4266iOY8l1Bq23273rk3ok3xWwr56ybgC', '2018-04-15'),
(29, 'Adobe Illustrator', '5ou4266iOY8l1Bq23273rkMd6H2DcN0mAFnjU9LfVJh3ok3xWwr56ybgCww', '2018-04-15'),
(30, 'Drawing', '6iOY8l1Bq23273rk0234KILR23492034ksfpd456yslfdsf5ou426', '2018-04-15'),
(31, 'Web Design', '3l34l432fo232l3223DhssdfRKLl5434lsdfl3l3sfs3lllblp23D', '2018-04-15'),
(32, 'Cartoon', 'sdfowerewl567lflsdfl3l3sf3l34l432fo232l3223Dhs3lllblp23D', '2018-04-15'),
(33, 'Graphics', '3232l32hsfo23lllblp23D9LfVJkfo394s5ou42at5sd20cNsolof3llsblp23DcN', '2018-04-15'),
(34, 'Fashion Design', '9LfVJkfo394s5ou42at5sd203232l32hsfo23lllblp23DcNsolof3llsblp23DcN', '2018-04-15'),
(35, 'WordPress', 'hsfo23lllblp23DcNsolof3llsblp23DcN9LfVJkfo394s5ou42', '2018-04-15'),
(36, 'Editing', 'lof3llsblp23DcN9LfVJhsfo23lllblp23DcNsokfo394s5ou42', '2018-04-15'),
(37, 'Writing', 'blp23DcNsokfo394slof3llsblp23DcN9LfVJh5ou42', '2018-04-15'),
(38, 'T-Shirt Design', '9LfVJh5ou42blp23DcNsdf329LfVJh5ou42bsokjfwpoek0mAFnjU', '2018-04-15'),
(39, 'Display Advertising', '9LfVJh5ou42bsokjfwpoek9LfVJh5ou42blp23DcN0mAFnjU', '2018-04-15'),
(40, 'Email Marketing', 'DcN0mAFnjU9LfVJh5ou42bs66iOY8l1Bq23273rk3ok3xWwr56yMd6H2gC', '2018-04-15'),
(41, 'Lead Generation', '66iOY8l1Bq23273rk3ok3xWwr56yMd6H2DcN0mAFnjU9LfVJh5ou42bgC', '2018-04-15'),
(42, 'Market & Customer Research', 'Aou42Eou42iOY800Ke3klAou42066iOY800fklAou42', '2018-04-15'),
(43, 'Marketing Strategy', 'EKe3000fklAou4266iOY8l1kkadwlsdfk20323rlsKh4KadlLL', '2018-04-15'),
(44, 'Public Relations', 'l1kkadwlsdfk20323rlsKh4KadlLLEKe3000fklAou4266iOY8', '2018-04-15'),
(45, 'Telemarketing & Telesales', 'fklAou4266iOY8l1kkadwlsfBf329k3249owe923ksd324odLL2DcN0m', '2018-04-15'),
(46, 'Other - Sales & Marketing', 'Bf329k3249owe923ksd324odfklAou4266iOY8l1kkadwlLL2DcN0m', '2018-04-15'),
(47, 'SEM - Search Engine Marketing', 'Aou4266iOY8l1Bf329k3249owe923ksdkkadwlLL2DcN0m', '2018-04-15'),
(48, 'SEO - Search Engine Optimization', 'rk0234KILR23492034ksfpd456y6iOY8l1Bq23273slfdsf5ou426', '2018-04-15'),
(49, 'SMM - Social Media Marketing', '2DcN0mAou4266iOY8l1BVJh5xWwr56ybMd6Hq23273rkFnjU9LgC', '2018-04-15');

-- --------------------------------------------------------

--
-- Table structure for table `xin_job_interviews`
--

CREATE TABLE `xin_job_interviews` (
  `job_interview_id` int(111) NOT NULL,
  `job_id` int(111) NOT NULL,
  `interviewers_id` varchar(255) NOT NULL,
  `interview_place` varchar(255) NOT NULL,
  `interview_date` varchar(255) NOT NULL,
  `interview_time` varchar(255) NOT NULL,
  `interviewees_id` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_job_pages`
--

CREATE TABLE `xin_job_pages` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_url` varchar(255) NOT NULL,
  `page_details` text NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_job_pages`
--

INSERT INTO `xin_job_pages` (`page_id`, `page_title`, `page_url`, `page_details`, `created_at`) VALUES
(1, 'About Us', 'xl9wkRy7tqOehBo6YCDjFG2JTucpKI4gMNsn8Zdf', 'About Ussss', '2018-04-15'),
(2, 'Communications', '5uk4EUc3V9FYTbBQz7PWgKM6qCajfAipvhOJnZHl', 'Communications', '2018-04-15'),
(3, 'Lending Licenses', '5r6OCsUoHQFiRwI17W0eT38jbvpxEGuLhzgmt9lZ', 'Lending Licenses', '2018-04-15'),
(4, 'Terms of Service', 'QrfbMOUWpdYNxjLFz8G1m6t3wi0X2RKEZVC9ySka', 'Terms of Service', '2018-04-15'),
(5, 'Privacy Policy', 'rjHKhmsNezT2OJBAoQq0yU1tL5F34MCwgIiZEc7x', 'Privacy Policy', '2018-04-15'),
(6, 'Support', 'gZbBVMxnfzYLlC2AOk609Q7yWpaSjmJHuRXosr58', 'Support', '2018-04-15'),
(7, 'How It Works', 'l1BbMd6H2D3rkFnjU9LgCH2D3rkFnjU9BbMd6H2D3r', 'How It Works', '2018-04-15'),
(8, 'Disclaimers', 'CTbzS9IrWkNU7VM3HGZYjp6iwmfyXDOQgtsP8FEc', 'Disclaimers', '2018-04-15');

-- --------------------------------------------------------

--
-- Table structure for table `xin_job_type`
--

CREATE TABLE `xin_job_type` (
  `job_type_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `type_url` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_job_type`
--

INSERT INTO `xin_job_type` (`job_type_id`, `company_id`, `type`, `type_url`, `created_at`) VALUES
(1, 1, 'Full Time', 'full-time', '22-03-2018 02:18:48'),
(2, 1, 'Part Time', 'part-time', '16-04-2018 06:29:45'),
(3, 1, 'Internship', 'internship', '16-04-2018 06:30:06'),
(4, 1, 'Freelance', 'freelance', '16-04-2018 06:30:21');

-- --------------------------------------------------------

--
-- Table structure for table `xin_kpi_incidental`
--

CREATE TABLE `xin_kpi_incidental` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `incidental_kpi` text NOT NULL,
  `targeted_date` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `quarter` int(11) NOT NULL,
  `result` varchar(200) NOT NULL,
  `feedback` text NOT NULL,
  `year_created` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  `updated_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `xin_kpi_maingoals`
--

CREATE TABLE `xin_kpi_maingoals` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `main_kpi` varchar(255) NOT NULL,
  `year_created` varchar(200) NOT NULL,
  `status` varchar(100) NOT NULL,
  `approve_status` varchar(100) NOT NULL,
  `q1` varchar(100) NOT NULL,
  `q2` varchar(100) NOT NULL,
  `q3` varchar(100) NOT NULL,
  `q4` varchar(100) NOT NULL,
  `feedback` varchar(255) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  `updated_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `xin_kpi_variable`
--

CREATE TABLE `xin_kpi_variable` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `variable_kpi` varchar(200) NOT NULL,
  `targeted_date` varchar(200) NOT NULL,
  `result` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `approve_status` varchar(200) NOT NULL,
  `feedback` text NOT NULL,
  `quarter` varchar(200) NOT NULL,
  `year_created` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  `updated_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `xin_languages`
--

CREATE TABLE `xin_languages` (
  `language_id` int(111) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `language_flag` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_languages`
--

INSERT INTO `xin_languages` (`language_id`, `language_name`, `language_code`, `language_flag`, `is_active`, `created_at`) VALUES
(1, 'English', 'english', 'language_flag_1520564355.gif', 1, ''),
(4, 'PortuguÃªs', 'portuguese', 'language_flag_1526420518.gif', 0, '16-05-2018 12:41:57'),
(5, 'Tiáº¿ng Viá»‡t', 'vietnamese', 'language_flag_1526728529.gif', 0, '19-05-2018 02:15:28'),
(6, 'EspaÃ±ol', 'spanish', 'language_flag_1563906920.gif', 0, '23-07-2019 11:35:20'),
(7, 'Svenska', 'swedish', 'language_flag_1564007195.gif', 0, '25-07-2019 03:26:35'),
(8, 'Thailand', 'thailand', 'language_flag_1564007402.gif', 0, '25-07-2019 03:30:02'),
(9, 'Indonesian', 'indonesian', 'language_flag_1564054894.gif', 0, '25-07-2019 04:41:33'),
(10, 'Italiano', 'italian', 'language_flag_1564058198.gif', 0, '25-07-2019 05:36:37'),
(11, 'Deutsch', 'dutch', 'language_flag_1564058280.gif', 0, '25-07-2019 05:37:59'),
(12, 'TÃ¼rk', 'turkish', 'language_flag_1564058565.gif', 0, '25-07-2019 05:42:44'),
(13, 'FranÃ§ais', 'french', 'language_flag_1564058638.gif', 0, '25-07-2019 05:43:58'),
(14, 'Ð ÑƒÑÑÐºÐ¸Ð¹', 'russian', 'language_flag_1564058661.gif', 0, '25-07-2019 05:44:20'),
(15, 'RomÃ¢nÄƒ', 'romanian', 'language_flag_1564058689.gif', 0, '25-07-2019 05:44:49'),
(16, 'Irish', 'irish', 'language_flag_1564171301.gif', 0, '27-07-2019 01:01:41');

-- --------------------------------------------------------

--
-- Table structure for table `xin_leave_applications`
--

CREATE TABLE `xin_leave_applications` (
  `leave_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(222) NOT NULL,
  `leave_type_id` int(222) NOT NULL,
  `from_date` varchar(200) NOT NULL,
  `to_date` varchar(200) NOT NULL,
  `applied_on` varchar(200) NOT NULL,
  `reason` mediumtext NOT NULL,
  `remarks` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_notify` int(11) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_leave_type`
--

CREATE TABLE `xin_leave_type` (
  `leave_type_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `type_name` varchar(200) NOT NULL,
  `days_per_year` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_leave_type`
--

INSERT INTO `xin_leave_type` (`leave_type_id`, `company_id`, `type_name`, `days_per_year`, `status`, `created_at`) VALUES
(1, 1, 'Casual Leave', '3', 1, '19-03-2018 07:52:20'),
(2, 1, 'Medical', '2', 1, '19-03-2018 07:52:30');

-- --------------------------------------------------------

--
-- Table structure for table `xin_location`
--

CREATE TABLE `xin_location` (
  `location_id` int(11) NOT NULL,
  `location_name` varchar(255) NOT NULL,
  `created_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_location`
--

INSERT INTO `xin_location` (`location_id`, `location_name`, `created_on`) VALUES
(10, 'Abia State', '2019-11-01 11:42:04'),
(11, 'Adamawa', '2019-11-01 11:44:06'),
(12, 'Akwa Ibom', '2019-11-01 11:44:27'),
(13, 'Anambra', '2019-11-01 11:44:41'),
(14, 'Bauchi', '2019-11-01 11:44:56'),
(15, 'FCT Abuja', '2019-11-01 11:45:14'),
(16, 'Lagos', '2019-11-01 11:45:30'),
(17, 'Bayelsa', '2019-11-02 05:06:02'),
(18, 'Enugu', '2019-11-02 05:05:46');

-- --------------------------------------------------------

--
-- Table structure for table `xin_make_payment`
--

CREATE TABLE `xin_make_payment` (
  `make_payment_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `department_id` int(111) NOT NULL,
  `company_id` int(111) NOT NULL,
  `location_id` int(111) NOT NULL,
  `designation_id` int(111) NOT NULL,
  `payment_date` varchar(200) NOT NULL,
  `basic_salary` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `gross_salary` varchar(255) NOT NULL,
  `total_allowances` varchar(255) NOT NULL,
  `total_deductions` varchar(255) NOT NULL,
  `net_salary` varchar(255) NOT NULL,
  `house_rent_allowance` varchar(255) NOT NULL,
  `medical_allowance` varchar(255) NOT NULL,
  `travelling_allowance` varchar(255) NOT NULL,
  `dearness_allowance` varchar(255) NOT NULL,
  `provident_fund` varchar(255) NOT NULL,
  `tax_deduction` varchar(255) NOT NULL,
  `security_deposit` varchar(255) NOT NULL,
  `overtime_rate` varchar(255) NOT NULL,
  `is_advance_salary_deduct` int(11) NOT NULL,
  `advance_salary_amount` varchar(255) NOT NULL,
  `is_payment` tinyint(1) NOT NULL,
  `payment_method` int(11) NOT NULL,
  `hourly_rate` varchar(255) NOT NULL,
  `total_hours_work` varchar(255) NOT NULL,
  `comments` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_meetings`
--

CREATE TABLE `xin_meetings` (
  `meeting_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `meeting_title` varchar(255) NOT NULL,
  `meeting_date` varchar(255) NOT NULL,
  `meeting_time` varchar(255) NOT NULL,
  `meeting_note` mediumtext NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_meetings`
--

INSERT INTO `xin_meetings` (`meeting_id`, `company_id`, `employee_id`, `meeting_title`, `meeting_date`, `meeting_time`, `meeting_note`, `created_at`) VALUES
(1, 1, 5, 'LionTech Presentation', '2019-11-03', '12:05', 'LionTech Presentation for HMO SOftware', '2019-11-02');

-- --------------------------------------------------------

--
-- Table structure for table `xin_office_location`
--

CREATE TABLE `xin_office_location` (
  `location_id` int(11) NOT NULL,
  `company_id` int(111) NOT NULL,
  `location_head` int(111) NOT NULL,
  `location_manager` int(111) NOT NULL,
  `location_name` varchar(200) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `address_1` mediumtext NOT NULL,
  `address_2` mediumtext NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `country` int(111) NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_office_location`
--

INSERT INTO `xin_office_location` (`location_id`, `company_id`, `location_head`, `location_manager`, `location_name`, `email`, `phone`, `fax`, `address_1`, `address_2`, `city`, `state`, `zipcode`, `country`, `added_by`, `created_at`, `status`) VALUES
(1, 1, 5, 0, 'Riyadh Branch', 'mainoffice@hrsale.com', '1234567890', '1234567890', 'Address Line 1', 'Address Line 2', 'City', 'State', '12345', 190, 1, '28-02-2018', 1),
(2, 1, 5, 0, 'FCT Abuja', 'info@liontech.com.ng', '7031806085', '', 'Test', 'Test2', 'Abuja', 'FCT', '11461', 161, 1, '02-11-2019', 1),
(3, 1, 5, 0, 'Anambra', 'info@liontech.com.ng', '7031806085', '', 'Test', 'Test2', 'Abuja', 'FCT', '11461', 161, 1, '02-11-2019', 1);

-- --------------------------------------------------------

--
-- Table structure for table `xin_office_shift`
--

CREATE TABLE `xin_office_shift` (
  `office_shift_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `shift_name` varchar(255) NOT NULL,
  `default_shift` int(111) NOT NULL,
  `monday_in_time` varchar(222) NOT NULL,
  `monday_out_time` varchar(222) NOT NULL,
  `tuesday_in_time` varchar(222) NOT NULL,
  `tuesday_out_time` varchar(222) NOT NULL,
  `wednesday_in_time` varchar(222) NOT NULL,
  `wednesday_out_time` varchar(222) NOT NULL,
  `thursday_in_time` varchar(222) NOT NULL,
  `thursday_out_time` varchar(222) NOT NULL,
  `friday_in_time` varchar(222) NOT NULL,
  `friday_out_time` varchar(222) NOT NULL,
  `saturday_in_time` varchar(222) NOT NULL,
  `saturday_out_time` varchar(222) NOT NULL,
  `sunday_in_time` varchar(222) NOT NULL,
  `sunday_out_time` varchar(222) NOT NULL,
  `created_at` varchar(222) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_office_shift`
--

INSERT INTO `xin_office_shift` (`office_shift_id`, `company_id`, `shift_name`, `default_shift`, `monday_in_time`, `monday_out_time`, `tuesday_in_time`, `tuesday_out_time`, `wednesday_in_time`, `wednesday_out_time`, `thursday_in_time`, `thursday_out_time`, `friday_in_time`, `friday_out_time`, `saturday_in_time`, `saturday_out_time`, `sunday_in_time`, `sunday_out_time`, `created_at`) VALUES
(1, 1, 'Morning Shift', 1, '08:00', '18:00', '03:00', '18:00', '08:00', '18:00', '08:00', '18:00', '08:00', '18:00', '', '', '', '', '2018-02-28');

-- --------------------------------------------------------

--
-- Table structure for table `xin_organization`
--

CREATE TABLE `xin_organization` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `location_id` int(11) NOT NULL,
  `contact_person` varchar(255) NOT NULL,
  `logo_name` varchar(255) NOT NULL,
  `rc_number` varchar(255) NOT NULL,
  `type_business` int(11) NOT NULL,
  `created_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_organization`
--

INSERT INTO `xin_organization` (`id`, `name`, `location_id`, `contact_person`, `logo_name`, `rc_number`, `type_business`, `created_on`) VALUES
(4, 'Testing', 11, 'Muhammad Zeeshan', 'logo_1572674437.jpg', '3334', 5, '2019-11-03 08:03:52'),
(5, 'LionTech', 15, 'Kennedy Job', 'logo_1573153412.png', '232323', 2, '2019-11-07 08:03:31'),
(6, 'NIRSAL', 15, 'Yarima Albashir', 'logo_1573474516.jpg', '232323', 4, '2019-11-11 01:15:15'),
(7, 'India Tech', 16, 'test test', 'logo_1574335550.png', '23323', 2, '2019-11-21 12:25:49');

-- --------------------------------------------------------

--
-- Table structure for table `xin_payment_method`
--

CREATE TABLE `xin_payment_method` (
  `payment_method_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `method_name` varchar(255) NOT NULL,
  `payment_percentage` varchar(200) DEFAULT NULL,
  `account_number` varchar(200) DEFAULT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_payment_method`
--

INSERT INTO `xin_payment_method` (`payment_method_id`, `company_id`, `method_name`, `payment_percentage`, `account_number`, `created_at`) VALUES
(1, 1, 'Cash', '30', '', '23-04-2018 05:13:52'),
(2, 1, 'Paypal', '40', '1', '12-08-2018 02:18:50'),
(3, 1, 'Bank', '30', '1231232', '12-08-2018 02:18:57'),
(4, 0, 'Transfer', '20', '232323233', '02-11-2019 01:12:14');

-- --------------------------------------------------------

--
-- Table structure for table `xin_payroll_custom_fields`
--

CREATE TABLE `xin_payroll_custom_fields` (
  `payroll_custom_id` int(11) NOT NULL,
  `allow_custom_1` varchar(255) NOT NULL,
  `is_active_allow_1` int(11) NOT NULL,
  `allow_custom_2` varchar(255) NOT NULL,
  `is_active_allow_2` int(11) NOT NULL,
  `allow_custom_3` varchar(255) NOT NULL,
  `is_active_allow_3` int(11) NOT NULL,
  `allow_custom_4` varchar(255) NOT NULL,
  `is_active_allow_4` int(11) NOT NULL,
  `allow_custom_5` varchar(255) NOT NULL,
  `is_active_allow_5` int(111) NOT NULL,
  `deduct_custom_1` varchar(255) NOT NULL,
  `is_active_deduct_1` int(11) NOT NULL,
  `deduct_custom_2` varchar(255) NOT NULL,
  `is_active_deduct_2` int(11) NOT NULL,
  `deduct_custom_3` varchar(255) NOT NULL,
  `is_active_deduct_3` int(11) NOT NULL,
  `deduct_custom_4` varchar(255) NOT NULL,
  `is_active_deduct_4` int(11) NOT NULL,
  `deduct_custom_5` varchar(255) NOT NULL,
  `is_active_deduct_5` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_performance_appraisal`
--

CREATE TABLE `xin_performance_appraisal` (
  `performance_appraisal_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `appraisal_year_month` varchar(255) NOT NULL,
  `customer_experience` int(111) NOT NULL,
  `marketing` int(111) NOT NULL,
  `management` int(111) NOT NULL,
  `administration` int(111) NOT NULL,
  `presentation_skill` int(111) NOT NULL,
  `quality_of_work` int(111) NOT NULL,
  `efficiency` int(111) NOT NULL,
  `integrity` int(111) NOT NULL,
  `professionalism` int(111) NOT NULL,
  `team_work` int(111) NOT NULL,
  `critical_thinking` int(111) NOT NULL,
  `conflict_management` int(111) NOT NULL,
  `attendance` int(111) NOT NULL,
  `ability_to_meet_deadline` int(111) NOT NULL,
  `remarks` mediumtext NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_performance_indicator`
--

CREATE TABLE `xin_performance_indicator` (
  `performance_indicator_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `designation_id` int(111) NOT NULL,
  `customer_experience` int(111) NOT NULL,
  `marketing` int(111) NOT NULL,
  `management` int(111) NOT NULL,
  `administration` int(111) NOT NULL,
  `presentation_skill` int(111) NOT NULL,
  `quality_of_work` int(111) NOT NULL,
  `efficiency` int(111) NOT NULL,
  `integrity` int(111) NOT NULL,
  `professionalism` int(111) NOT NULL,
  `team_work` int(111) NOT NULL,
  `critical_thinking` int(111) NOT NULL,
  `conflict_management` int(111) NOT NULL,
  `attendance` int(111) NOT NULL,
  `ability_to_meet_deadline` int(111) NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_projects`
--

CREATE TABLE `xin_projects` (
  `project_id` int(111) NOT NULL,
  `title` varchar(255) NOT NULL,
  `client_id` int(100) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `company_id` int(111) NOT NULL,
  `assigned_to` mediumtext NOT NULL,
  `priority` varchar(255) NOT NULL,
  `summary` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `added_by` int(111) NOT NULL,
  `project_progress` varchar(255) NOT NULL,
  `project_note` longtext NOT NULL,
  `status` tinyint(2) NOT NULL,
  `is_notify` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_projects`
--

INSERT INTO `xin_projects` (`project_id`, `title`, `client_id`, `start_date`, `end_date`, `company_id`, `assigned_to`, `priority`, `summary`, `description`, `added_by`, `project_progress`, `project_note`, `status`, `is_notify`, `created_at`) VALUES
(1, 'test', 4, '2019-11-23', '2020-11-20', 1, '5', '1', '100000', 'testing', 1, '0', '', 0, 0, '23-11-2019');

-- --------------------------------------------------------

--
-- Table structure for table `xin_projects_attachment`
--

CREATE TABLE `xin_projects_attachment` (
  `project_attachment_id` int(11) NOT NULL,
  `project_id` int(200) NOT NULL,
  `upload_by` int(255) NOT NULL,
  `file_title` varchar(255) NOT NULL,
  `file_description` mediumtext NOT NULL,
  `attachment_file` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_projects_bugs`
--

CREATE TABLE `xin_projects_bugs` (
  `bug_id` int(11) NOT NULL,
  `project_id` int(111) NOT NULL,
  `user_id` int(200) NOT NULL,
  `attachment_file` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_projects_discussion`
--

CREATE TABLE `xin_projects_discussion` (
  `discussion_id` int(11) NOT NULL,
  `project_id` int(111) NOT NULL,
  `user_id` int(200) NOT NULL,
  `client_id` int(11) NOT NULL,
  `attachment_file` varchar(255) NOT NULL,
  `message` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_qualification_education_level`
--

CREATE TABLE `xin_qualification_education_level` (
  `education_level_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_qualification_education_level`
--

INSERT INTO `xin_qualification_education_level` (`education_level_id`, `company_id`, `name`, `created_at`) VALUES
(1, 1, 'High School Diploma / GED', '09-05-2018 03:11:59');

-- --------------------------------------------------------

--
-- Table structure for table `xin_qualification_language`
--

CREATE TABLE `xin_qualification_language` (
  `language_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_qualification_language`
--

INSERT INTO `xin_qualification_language` (`language_id`, `company_id`, `name`, `created_at`) VALUES
(1, 1, 'English', '09-05-2018 03:12:03');

-- --------------------------------------------------------

--
-- Table structure for table `xin_qualification_skill`
--

CREATE TABLE `xin_qualification_skill` (
  `skill_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_qualification_skill`
--

INSERT INTO `xin_qualification_skill` (`skill_id`, `company_id`, `name`, `created_at`) VALUES
(1, 1, 'jQuery', '09-05-2018 03:12:08');

-- --------------------------------------------------------

--
-- Table structure for table `xin_recruitment_pages`
--

CREATE TABLE `xin_recruitment_pages` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_details` mediumtext NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_recruitment_pages`
--

INSERT INTO `xin_recruitment_pages` (`page_id`, `page_title`, `page_details`, `status`, `created_at`) VALUES
(1, 'Pages', 'Nulla dignissim gravida\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultricies dictum ex, nec ullamcorper orci luctus eget. Integer mauris arcu, pretium eget elit vel, posuere consectetur massa. Etiam non fermentum augue, vel posuere sapien. \n\nVivamus aliquet eros bibendum ipsum euismod, non interdum dui elementum. Morbi facilisis hendrerit nisi, a volutpat velit. Donec sed malesuada felis. Nulla facilisi. Vivamus a velit vel orci euismod maximus. Praesent ut blandit orci, eget suscipit lorem. Aenean dignissim, augue at porta suscipit, est enim euismod mi, a rhoncus mi lacus ac nibh. Ut pharetra ligula sed tortor congue, pellentesque ultricies augue tincidunt.', 1, ''),
(2, 'About Us', 'Nulla dignissim gravida\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultricies dictum ex, nec ullamcorper orci luctus eget. Integer mauris arcu, pretium eget elit vel, posuere consectetur massa. Etiam non fermentum augue, vel posuere sapien. \n\nVivamus aliquet eros bibendum ipsum euismod, non interdum dui elementum. Morbi facilisis hendrerit nisi, a volutpat velit. Donec sed malesuada felis. Nulla facilisi. Vivamus a velit vel orci euismod maximus. Praesent ut blandit orci, eget suscipit lorem. Aenean dignissim, augue at porta suscipit, est enim euismod mi, a rhoncus mi lacus ac nibh. Ut pharetra ligula sed tortor congue, pellentesque ultricies augue tincidunt.', 1, ''),
(3, 'Career Services', 'Career Services', 1, ''),
(4, 'Success Stories', 'Success Stories', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `xin_recruitment_subpages`
--

CREATE TABLE `xin_recruitment_subpages` (
  `subpages_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `sub_page_title` varchar(255) NOT NULL,
  `sub_page_details` mediumtext NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_recruitment_subpages`
--

INSERT INTO `xin_recruitment_subpages` (`subpages_id`, `page_id`, `sub_page_title`, `sub_page_details`, `status`, `created_at`) VALUES
(1, 1, 'Sub Menu 1', 'Nulla dignissim gravida\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultricies dictum ex, nec ullamcorper orci luctus eget. Integer mauris arcu, pretium eget elit vel, posuere consectetur massa. Etiam non fermentum augue, vel posuere sapien. \r\n\r\nVivamus aliquet eros bibendum ipsum euismod, non interdum dui elementum. Morbi facilisis hendrerit nisi, a volutpat velit. Donec sed malesuada felis. Nulla facilisi. Vivamus a velit vel orci euismod maximus. Praesent ut blandit orci, eget suscipit lorem. Aenean dignissim, augue at porta suscipit, est enim euismod mi, a rhoncus mi lacus ac nibh. Ut pharetra ligula sed tortor congue, pellentesque ultricies augue tincidunt.', 1, ''),
(2, 1, 'Sub Menu 2', 'Nulla dignissim gravida\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultricies dictum ex, nec ullamcorper orci luctus eget. Integer mauris arcu, pretium eget elit vel, posuere consectetur massa. Etiam non fermentum augue, vel posuere sapien. \r\n\r\nVivamus aliquet eros bibendum ipsum euismod, non interdum dui elementum. Morbi facilisis hendrerit nisi, a volutpat velit. Donec sed malesuada felis. Nulla facilisi. Vivamus a velit vel orci euismod maximus. Praesent ut blandit orci, eget suscipit lorem. Aenean dignissim, augue at porta suscipit, est enim euismod mi, a rhoncus mi lacus ac nibh. Ut pharetra ligula sed tortor congue, pellentesque ultricies augue tincidunt.', 1, ''),
(3, 1, 'Sub Menu 3', 'Nulla dignissim gravida\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultricies dictum ex, nec ullamcorper orci luctus eget. Integer mauris arcu, pretium eget elit vel, posuere consectetur massa. Etiam non fermentum augue, vel posuere sapien. \r\n\r\nVivamus aliquet eros bibendum ipsum euismod, non interdum dui elementum. Morbi facilisis hendrerit nisi, a volutpat velit. Donec sed malesuada felis. Nulla facilisi. Vivamus a velit vel orci euismod maximus. Praesent ut blandit orci, eget suscipit lorem. Aenean dignissim, augue at porta suscipit, est enim euismod mi, a rhoncus mi lacus ac nibh. Ut pharetra ligula sed tortor congue, pellentesque ultricies augue tincidunt.', 1, ''),
(4, 1, 'Sub Menu 4', 'Nulla dignissim gravida\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultricies dictum ex, nec ullamcorper orci luctus eget. Integer mauris arcu, pretium eget elit vel, posuere consectetur massa. Etiam non fermentum augue, vel posuere sapien. \r\n\r\nVivamus aliquet eros bibendum ipsum euismod, non interdum dui elementum. Morbi facilisis hendrerit nisi, a volutpat velit. Donec sed malesuada felis. Nulla facilisi. Vivamus a velit vel orci euismod maximus. Praesent ut blandit orci, eget suscipit lorem. Aenean dignissim, augue at porta suscipit, est enim euismod mi, a rhoncus mi lacus ac nibh. Ut pharetra ligula sed tortor congue, pellentesque ultricies augue tincidunt.', 1, ''),
(5, 1, 'Sub Menu 5', 'Nulla dignissim gravida\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultricies dictum ex, nec ullamcorper orci luctus eget. Integer mauris arcu, pretium eget elit vel, posuere consectetur massa. Etiam non fermentum augue, vel posuere sapien. \r\n\r\nVivamus aliquet eros bibendum ipsum euismod, non interdum dui elementum. Morbi facilisis hendrerit nisi, a volutpat velit. Donec sed malesuada felis. Nulla facilisi. Vivamus a velit vel orci euismod maximus. Praesent ut blandit orci, eget suscipit lorem. Aenean dignissim, augue at porta suscipit, est enim euismod mi, a rhoncus mi lacus ac nibh. Ut pharetra ligula sed tortor congue, pellentesque ultricies augue tincidunt.', 1, ''),
(6, 1, 'Sub Menu 6', 'Nulla dignissim gravida\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultricies dictum ex, nec ullamcorper orci luctus eget. Integer mauris arcu, pretium eget elit vel, posuere consectetur massa. Etiam non fermentum augue, vel posuere sapien. \r\n\r\nVivamus aliquet eros bibendum ipsum euismod, non interdum dui elementum. Morbi facilisis hendrerit nisi, a volutpat velit. Donec sed malesuada felis. Nulla facilisi. Vivamus a velit vel orci euismod maximus. Praesent ut blandit orci, eget suscipit lorem. Aenean dignissim, augue at porta suscipit, est enim euismod mi, a rhoncus mi lacus ac nibh. Ut pharetra ligula sed tortor congue, pellentesque ultricies augue tincidunt.', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_allowances`
--

CREATE TABLE `xin_salary_allowances` (
  `allowance_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `is_allowance_taxable` int(11) NOT NULL,
  `allowance_title` varchar(200) DEFAULT NULL,
  `allowance_amount` varchar(200) DEFAULT NULL,
  `created_at` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_salary_allowances`
--

INSERT INTO `xin_salary_allowances` (`allowance_id`, `employee_id`, `is_allowance_taxable`, `allowance_title`, `allowance_amount`, `created_at`) VALUES
(1, 1, 0, 'Cost of Living Allowance', '100', NULL),
(2, 1, 0, 'Housing Allowance', '200', NULL),
(3, 1, 0, 'Market Adjustment', '200', NULL),
(4, 1, 0, 'Meal Allowance', '100', NULL),
(5, 1, 0, 'Transportation Allowance', '200', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_bank_allocation`
--

CREATE TABLE `xin_salary_bank_allocation` (
  `bank_allocation_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `pay_percent` varchar(200) NOT NULL,
  `acc_number` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_commissions`
--

CREATE TABLE `xin_salary_commissions` (
  `salary_commissions_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `commission_title` varchar(200) DEFAULT NULL,
  `commission_amount` varchar(200) DEFAULT NULL,
  `created_at` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_loan_deductions`
--

CREATE TABLE `xin_salary_loan_deductions` (
  `loan_deduction_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `loan_options` int(11) NOT NULL,
  `loan_deduction_title` varchar(200) NOT NULL,
  `start_date` varchar(200) NOT NULL,
  `end_date` varchar(200) NOT NULL,
  `monthly_installment` varchar(200) NOT NULL,
  `loan_time` varchar(200) NOT NULL,
  `loan_deduction_amount` varchar(200) NOT NULL,
  `total_paid` varchar(200) NOT NULL,
  `reason` text NOT NULL,
  `status` int(11) NOT NULL,
  `is_deducted_from_salary` int(11) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_other_payments`
--

CREATE TABLE `xin_salary_other_payments` (
  `other_payments_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `payments_title` varchar(200) DEFAULT NULL,
  `payments_amount` varchar(200) DEFAULT NULL,
  `created_at` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_overtime`
--

CREATE TABLE `xin_salary_overtime` (
  `salary_overtime_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `overtime_type` varchar(200) NOT NULL,
  `no_of_days` varchar(100) NOT NULL DEFAULT '0',
  `overtime_hours` varchar(100) NOT NULL DEFAULT '0',
  `overtime_rate` varchar(100) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_payslips`
--

CREATE TABLE `xin_salary_payslips` (
  `payslip_id` int(11) NOT NULL,
  `payslip_key` varchar(200) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `salary_month` varchar(200) NOT NULL,
  `wages_type` int(11) NOT NULL,
  `payslip_type` varchar(50) NOT NULL,
  `basic_salary` varchar(200) NOT NULL,
  `daily_wages` varchar(200) NOT NULL,
  `is_half_monthly_payroll` tinyint(1) NOT NULL,
  `hours_worked` varchar(50) NOT NULL DEFAULT '0',
  `total_allowances` varchar(200) NOT NULL,
  `total_commissions` varchar(200) NOT NULL,
  `total_statutory_deductions` varchar(200) NOT NULL,
  `total_other_payments` varchar(200) NOT NULL,
  `total_loan` varchar(200) NOT NULL,
  `total_overtime` varchar(200) NOT NULL,
  `statutory_deductions` varchar(200) NOT NULL,
  `net_salary` varchar(200) NOT NULL,
  `grand_net_salary` varchar(200) NOT NULL,
  `other_payment` varchar(200) NOT NULL,
  `payment_method` int(11) NOT NULL,
  `pay_comments` mediumtext NOT NULL,
  `is_payment` int(11) NOT NULL,
  `year_to_date` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_payslip_allowances`
--

CREATE TABLE `xin_salary_payslip_allowances` (
  `payslip_allowances_id` int(11) NOT NULL,
  `payslip_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `allowance_title` varchar(200) NOT NULL,
  `allowance_amount` varchar(200) NOT NULL,
  `salary_month` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_payslip_commissions`
--

CREATE TABLE `xin_salary_payslip_commissions` (
  `payslip_commissions_id` int(11) NOT NULL,
  `payslip_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `commission_title` varchar(200) NOT NULL,
  `commission_amount` varchar(200) NOT NULL,
  `salary_month` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_payslip_loan`
--

CREATE TABLE `xin_salary_payslip_loan` (
  `payslip_loan_id` int(11) NOT NULL,
  `payslip_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `loan_title` varchar(200) NOT NULL,
  `loan_amount` varchar(200) NOT NULL,
  `salary_month` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_payslip_other_payments`
--

CREATE TABLE `xin_salary_payslip_other_payments` (
  `payslip_other_payment_id` int(11) NOT NULL,
  `payslip_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `payments_title` varchar(200) NOT NULL,
  `payments_amount` varchar(200) NOT NULL,
  `salary_month` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_payslip_overtime`
--

CREATE TABLE `xin_salary_payslip_overtime` (
  `payslip_overtime_id` int(11) NOT NULL,
  `payslip_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `overtime_title` varchar(200) NOT NULL,
  `overtime_salary_month` varchar(200) NOT NULL,
  `overtime_no_of_days` varchar(200) NOT NULL,
  `overtime_hours` varchar(200) NOT NULL,
  `overtime_rate` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_payslip_statutory_deductions`
--

CREATE TABLE `xin_salary_payslip_statutory_deductions` (
  `payslip_deduction_id` int(11) NOT NULL,
  `payslip_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `deduction_title` varchar(200) NOT NULL,
  `deduction_amount` varchar(200) NOT NULL,
  `salary_month` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_statutory_deductions`
--

CREATE TABLE `xin_salary_statutory_deductions` (
  `statutory_deductions_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `statutory_options` int(11) NOT NULL,
  `deduction_title` varchar(200) DEFAULT NULL,
  `deduction_amount` varchar(200) DEFAULT NULL,
  `created_at` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_salary_templates`
--

CREATE TABLE `xin_salary_templates` (
  `salary_template_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `salary_grades` varchar(255) NOT NULL,
  `basic_salary` varchar(255) NOT NULL,
  `overtime_rate` varchar(255) NOT NULL,
  `house_rent_allowance` varchar(255) NOT NULL,
  `medical_allowance` varchar(255) NOT NULL,
  `travelling_allowance` varchar(255) NOT NULL,
  `dearness_allowance` varchar(255) NOT NULL,
  `security_deposit` varchar(255) NOT NULL,
  `provident_fund` varchar(255) NOT NULL,
  `tax_deduction` varchar(255) NOT NULL,
  `gross_salary` varchar(255) NOT NULL,
  `total_allowance` varchar(255) NOT NULL,
  `total_deduction` varchar(255) NOT NULL,
  `net_salary` varchar(255) NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_salary_templates`
--

INSERT INTO `xin_salary_templates` (`salary_template_id`, `company_id`, `salary_grades`, `basic_salary`, `overtime_rate`, `house_rent_allowance`, `medical_allowance`, `travelling_allowance`, `dearness_allowance`, `security_deposit`, `provident_fund`, `tax_deduction`, `gross_salary`, `total_allowance`, `total_deduction`, `net_salary`, `added_by`, `created_at`) VALUES
(1, 1, 'Monthly', '2500', '', '50', '60', '70', '80', '40', '20', '30', '2760', '260', '90', '2670', 1, '22-03-2018 01:40:06');

-- --------------------------------------------------------

--
-- Table structure for table `xin_services_hospital`
--

CREATE TABLE `xin_services_hospital` (
  `id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `service_price` decimal(10,2) NOT NULL,
  `hospital_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_services_hospital`
--

INSERT INTO `xin_services_hospital` (`id`, `service_name`, `service_price`, `hospital_id`, `created_on`) VALUES
(2, 'teste', 2323.00, 3, '2019-11-06 04:39:49'),
(3, 'tt', 222.00, 3, '2019-11-06 04:39:49'),
(4, 'test', 12.00, 8, '2019-11-07 06:51:19'),
(5, 'test', 2323.00, 8, '2019-11-07 06:51:19'),
(6, 'tt', 222.00, 8, '2019-11-07 06:51:19'),
(7, 'test', 12.00, 4, '2019-11-11 12:41:25'),
(8, 'test', 2323.00, 4, '2019-11-11 12:41:25'),
(9, 'tt', 222.00, 4, '2019-11-11 12:41:25'),
(10, 'sdklsdkldkl', 2323.00, 4, '2019-11-11 12:41:25'),
(11, 'scsdc', 232.00, 4, '2019-11-11 12:41:26'),
(12, 'xcasxas', 21.00, 4, '2019-11-11 12:41:26'),
(13, 'klklk', 454.00, 4, '2019-11-11 12:41:26'),
(14, 'llkkl', 566.00, 4, '2019-11-11 12:41:26'),
(15, 'gfgfg', 232.00, 4, '2019-11-11 12:41:26'),
(16, 'panadol', 12.00, 10, '2019-11-21 12:47:14'),
(17, 'test', 2323.00, 10, '2019-11-21 12:47:14'),
(18, 'tt', 222.00, 10, '2019-11-21 12:47:14'),
(19, 'test', 12.00, 9, '2019-12-03 04:03:19'),
(20, 'test', 2323.00, 9, '2019-12-03 04:03:19'),
(21, 'tt', 222.00, 9, '2019-12-03 04:03:19');

-- --------------------------------------------------------

--
-- Table structure for table `xin_subscription`
--

CREATE TABLE `xin_subscription` (
  `subscription_id` int(11) NOT NULL,
  `plan_name` varchar(255) NOT NULL,
  `plan_cost` decimal(10,2) NOT NULL,
  `created_on` datetime DEFAULT NULL,
  `band_types` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_subscription`
--

INSERT INTO `xin_subscription` (`subscription_id`, `plan_name`, `plan_cost`, `created_on`, `band_types`) VALUES
(11, 'Precious', 29000.00, '2019-11-01 11:54:01', '8,7'),
(13, 'Diamond', 120000.00, '2019-11-20 07:22:14', '9,8,7'),
(14, 'Olive', 330000.00, '2019-11-21 12:28:55', '8'),
(15, 'Olive Executive', 45000.00, '2019-11-11 01:08:27', '9,8,7');

-- --------------------------------------------------------

--
-- Table structure for table `xin_subscription_detail`
--

CREATE TABLE `xin_subscription_detail` (
  `subscription_detail_id` int(11) NOT NULL,
  `subscription_detail_master_id` int(11) NOT NULL,
  `subscription_benifit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xin_subscription_detail`
--

INSERT INTO `xin_subscription_detail` (`subscription_detail_id`, `subscription_detail_master_id`, `subscription_benifit`) VALUES
(2, 11, 'Eye Surgery'),
(3, 11, 'Fever Treatment'),
(4, 11, 'Malaria Treatment'),
(5, 11, 'Headache'),
(22, 15, 'test'),
(24, 13, 'PREVENTIVE MEDICINE'),
(25, 14, 'sdf');

-- --------------------------------------------------------

--
-- Table structure for table `xin_sub_departments`
--

CREATE TABLE `xin_sub_departments` (
  `sub_department_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `department_name` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_sub_departments`
--

INSERT INTO `xin_sub_departments` (`sub_department_id`, `department_id`, `department_name`, `created_at`) VALUES
(8, 1, 'Manager', '2019-02-15 00:22:13'),
(9, 1, 'Lead Manager', '2019-02-15 00:22:21'),
(10, 2, 'Accountant', '2019-02-15 00:22:26');

-- --------------------------------------------------------

--
-- Table structure for table `xin_support_tickets`
--

CREATE TABLE `xin_support_tickets` (
  `ticket_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `ticket_code` varchar(200) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `ticket_priority` varchar(255) NOT NULL,
  `department_id` int(111) NOT NULL,
  `assigned_to` mediumtext NOT NULL,
  `message` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `ticket_remarks` mediumtext NOT NULL,
  `ticket_status` varchar(200) NOT NULL,
  `ticket_note` mediumtext NOT NULL,
  `is_notify` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_support_tickets`
--

INSERT INTO `xin_support_tickets` (`ticket_id`, `company_id`, `ticket_code`, `subject`, `employee_id`, `ticket_priority`, `department_id`, `assigned_to`, `message`, `description`, `ticket_remarks`, `ticket_status`, `ticket_note`, `is_notify`, `created_at`) VALUES
(1, 1, 'TQCQMAT', 'tt', 5, '1', 0, '', '', 'tt', '', '1', '', 0, '01-11-2019 04:00:07'),
(2, 1, 'XLA14F0', 'ffffff', 5, '2', 0, '', '', 'f', '', '1', '', 0, '01-11-2019 04:04:13');

-- --------------------------------------------------------

--
-- Table structure for table `xin_support_ticket_files`
--

CREATE TABLE `xin_support_ticket_files` (
  `ticket_file_id` int(111) NOT NULL,
  `ticket_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `ticket_files` varchar(255) NOT NULL,
  `file_size` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_system_setting`
--

CREATE TABLE `xin_system_setting` (
  `setting_id` int(111) NOT NULL,
  `application_name` varchar(255) NOT NULL,
  `default_currency` varchar(255) NOT NULL,
  `default_currency_id` int(11) NOT NULL,
  `default_currency_symbol` varchar(255) NOT NULL,
  `show_currency` varchar(255) NOT NULL,
  `currency_position` varchar(255) NOT NULL,
  `notification_position` varchar(255) NOT NULL,
  `notification_close_btn` varchar(255) NOT NULL,
  `notification_bar` varchar(255) NOT NULL,
  `enable_registration` varchar(255) NOT NULL,
  `login_with` varchar(255) NOT NULL,
  `date_format_xi` varchar(255) NOT NULL,
  `employee_manage_own_contact` varchar(255) NOT NULL,
  `employee_manage_own_profile` varchar(255) NOT NULL,
  `employee_manage_own_qualification` varchar(255) NOT NULL,
  `employee_manage_own_work_experience` varchar(255) NOT NULL,
  `employee_manage_own_document` varchar(255) NOT NULL,
  `employee_manage_own_picture` varchar(255) NOT NULL,
  `employee_manage_own_social` varchar(255) NOT NULL,
  `employee_manage_own_bank_account` varchar(255) NOT NULL,
  `enable_attendance` varchar(255) NOT NULL,
  `enable_clock_in_btn` varchar(255) NOT NULL,
  `enable_email_notification` varchar(255) NOT NULL,
  `payroll_include_day_summary` varchar(255) NOT NULL,
  `payroll_include_hour_summary` varchar(255) NOT NULL,
  `payroll_include_leave_summary` varchar(255) NOT NULL,
  `enable_job_application_candidates` varchar(255) NOT NULL,
  `job_logo` varchar(255) NOT NULL,
  `payroll_logo` varchar(255) NOT NULL,
  `is_payslip_password_generate` int(11) NOT NULL,
  `payslip_password_format` varchar(255) NOT NULL,
  `enable_profile_background` varchar(255) NOT NULL,
  `enable_policy_link` varchar(255) NOT NULL,
  `enable_layout` varchar(255) NOT NULL,
  `job_application_format` mediumtext NOT NULL,
  `project_email` varchar(255) NOT NULL,
  `holiday_email` varchar(255) NOT NULL,
  `leave_email` varchar(255) NOT NULL,
  `payslip_email` varchar(255) NOT NULL,
  `award_email` varchar(255) NOT NULL,
  `recruitment_email` varchar(255) NOT NULL,
  `announcement_email` varchar(255) NOT NULL,
  `training_email` varchar(255) NOT NULL,
  `task_email` varchar(255) NOT NULL,
  `compact_sidebar` varchar(255) NOT NULL,
  `fixed_header` varchar(255) NOT NULL,
  `fixed_sidebar` varchar(255) NOT NULL,
  `boxed_wrapper` varchar(255) NOT NULL,
  `layout_static` varchar(255) NOT NULL,
  `system_skin` varchar(255) NOT NULL,
  `animation_effect` varchar(255) NOT NULL,
  `animation_effect_modal` varchar(255) NOT NULL,
  `animation_effect_topmenu` varchar(255) NOT NULL,
  `footer_text` varchar(255) NOT NULL,
  `is_ssl_available` varchar(50) NOT NULL,
  `is_active_sub_departments` varchar(10) NOT NULL,
  `default_language` varchar(200) NOT NULL,
  `statutory_fixed` varchar(100) NOT NULL,
  `system_timezone` varchar(200) NOT NULL,
  `system_ip_address` varchar(255) NOT NULL,
  `system_ip_restriction` varchar(200) NOT NULL,
  `google_maps_api_key` mediumtext NOT NULL,
  `module_recruitment` varchar(100) NOT NULL,
  `module_travel` varchar(100) NOT NULL,
  `module_performance` varchar(100) NOT NULL,
  `module_payroll` varchar(10) NOT NULL,
  `module_files` varchar(100) NOT NULL,
  `module_awards` varchar(100) NOT NULL,
  `module_training` varchar(100) NOT NULL,
  `module_inquiry` varchar(100) NOT NULL,
  `module_language` varchar(100) NOT NULL,
  `module_orgchart` varchar(100) NOT NULL,
  `module_accounting` varchar(111) NOT NULL,
  `module_events` varchar(100) NOT NULL,
  `module_goal_tracking` varchar(100) NOT NULL,
  `module_assets` varchar(100) NOT NULL,
  `module_projects_tasks` varchar(100) NOT NULL,
  `module_chat_box` varchar(100) NOT NULL,
  `enable_page_rendered` varchar(255) NOT NULL,
  `enable_current_year` varchar(255) NOT NULL,
  `employee_login_id` varchar(200) NOT NULL,
  `paypal_email` varchar(100) NOT NULL,
  `paypal_sandbox` varchar(10) NOT NULL,
  `paypal_active` varchar(10) NOT NULL,
  `stripe_secret_key` varchar(200) NOT NULL,
  `stripe_publishable_key` varchar(200) NOT NULL,
  `stripe_active` varchar(10) NOT NULL,
  `online_payment_account` int(11) NOT NULL,
  `is_half_monthly` tinyint(1) NOT NULL,
  `half_deduct_month` tinyint(1) NOT NULL,
  `enable_auth_background` varchar(11) NOT NULL,
  `hr_version` varchar(200) NOT NULL,
  `hr_release_date` varchar(100) NOT NULL,
  `updated_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_system_setting`
--

INSERT INTO `xin_system_setting` (`setting_id`, `application_name`, `default_currency`, `default_currency_id`, `default_currency_symbol`, `show_currency`, `currency_position`, `notification_position`, `notification_close_btn`, `notification_bar`, `enable_registration`, `login_with`, `date_format_xi`, `employee_manage_own_contact`, `employee_manage_own_profile`, `employee_manage_own_qualification`, `employee_manage_own_work_experience`, `employee_manage_own_document`, `employee_manage_own_picture`, `employee_manage_own_social`, `employee_manage_own_bank_account`, `enable_attendance`, `enable_clock_in_btn`, `enable_email_notification`, `payroll_include_day_summary`, `payroll_include_hour_summary`, `payroll_include_leave_summary`, `enable_job_application_candidates`, `job_logo`, `payroll_logo`, `is_payslip_password_generate`, `payslip_password_format`, `enable_profile_background`, `enable_policy_link`, `enable_layout`, `job_application_format`, `project_email`, `holiday_email`, `leave_email`, `payslip_email`, `award_email`, `recruitment_email`, `announcement_email`, `training_email`, `task_email`, `compact_sidebar`, `fixed_header`, `fixed_sidebar`, `boxed_wrapper`, `layout_static`, `system_skin`, `animation_effect`, `animation_effect_modal`, `animation_effect_topmenu`, `footer_text`, `is_ssl_available`, `is_active_sub_departments`, `default_language`, `statutory_fixed`, `system_timezone`, `system_ip_address`, `system_ip_restriction`, `google_maps_api_key`, `module_recruitment`, `module_travel`, `module_performance`, `module_payroll`, `module_files`, `module_awards`, `module_training`, `module_inquiry`, `module_language`, `module_orgchart`, `module_accounting`, `module_events`, `module_goal_tracking`, `module_assets`, `module_projects_tasks`, `module_chat_box`, `enable_page_rendered`, `enable_current_year`, `employee_login_id`, `paypal_email`, `paypal_sandbox`, `paypal_active`, `stripe_secret_key`, `stripe_publishable_key`, `stripe_active`, `online_payment_account`, `is_half_monthly`, `half_deduct_month`, `enable_auth_background`, `hr_version`, `hr_release_date`, `updated_at`) VALUES
(1, 'Brave HMO Suite', 'NGN - ₦', 1, 'NGN - ₦', 'symbol', 'Prefix', 'toast-top-right', 'true', 'true', 'no', 'username', 'M-d-Y', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', '', 'yes', 'yes', 'yes', 'yes', '', 'job_logo_1573560863.png', 'payroll_logo_1568650221.png', 1, 'employee_id', 'yes', 'yes', 'yes', 'doc,docx,pdf', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', 'sidebar_layout_hrsale', '', 'fixed-sidebar', 'boxed_layout_hrsale', '', 'skin-default', 'fadeInDown', 'tada', 'tada', 'Brave HealthCare Suite ...Powered by LionTech', 'yes', '', 'english', '', 'Europe/Brussels', '::1', '', 'AIzaSyB3gP8H3eypotNeoEtezbRiF_f8Zh_p4ck', '', 'true', 'yes', 'yes', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', '', 'yes', 'email', 'hrsalesoft-facilitator@gmail.com', 'yes', 'yes', 'sk_test_2XEyr1hQFGByITfQjSwFqNtm', 'pk_test_zVFISCqeQPnniD0ywHBHikMd', 'yes', 1, 0, 1, 'yes', '1.0.3', '2018-03-28', '2018-03-28 04:27:32');

-- --------------------------------------------------------

--
-- Table structure for table `xin_tasks`
--

CREATE TABLE `xin_tasks` (
  `task_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `project_id` int(111) NOT NULL,
  `created_by` int(111) NOT NULL,
  `task_name` varchar(255) NOT NULL,
  `assigned_to` varchar(255) NOT NULL,
  `start_date` varchar(200) NOT NULL,
  `end_date` varchar(200) NOT NULL,
  `task_hour` varchar(200) NOT NULL,
  `task_progress` varchar(200) NOT NULL,
  `description` mediumtext NOT NULL,
  `task_status` int(5) NOT NULL,
  `task_note` mediumtext NOT NULL,
  `is_notify` int(11) NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_tasks_attachment`
--

CREATE TABLE `xin_tasks_attachment` (
  `task_attachment_id` int(11) NOT NULL,
  `task_id` int(200) NOT NULL,
  `upload_by` int(255) NOT NULL,
  `file_title` varchar(255) NOT NULL,
  `file_description` mediumtext NOT NULL,
  `attachment_file` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_tasks_comments`
--

CREATE TABLE `xin_tasks_comments` (
  `comment_id` int(11) NOT NULL,
  `task_id` int(200) NOT NULL,
  `user_id` int(200) NOT NULL,
  `task_comments` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_tax_types`
--

CREATE TABLE `xin_tax_types` (
  `tax_id` int(111) NOT NULL,
  `name` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_tax_types`
--

INSERT INTO `xin_tax_types` (`tax_id`, `name`, `rate`, `type`, `description`, `created_at`) VALUES
(1, 'No Tax', '0', 'fixed', 'test', '25-05-2018'),
(2, 'IVU', '2', 'fixed', 'test', '25-05-2018'),
(3, 'VAT', '5', 'percentage', 'testttt', '25-05-2018');

-- --------------------------------------------------------

--
-- Table structure for table `xin_termination_type`
--

CREATE TABLE `xin_termination_type` (
  `termination_type_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_termination_type`
--

INSERT INTO `xin_termination_type` (`termination_type_id`, `company_id`, `type`, `created_at`) VALUES
(1, 1, 'Voluntary Termination', '22-03-2018 01:38:41');

-- --------------------------------------------------------

--
-- Table structure for table `xin_theme_settings`
--

CREATE TABLE `xin_theme_settings` (
  `theme_settings_id` int(11) NOT NULL,
  `fixed_layout` varchar(200) NOT NULL,
  `fixed_footer` varchar(200) NOT NULL,
  `boxed_layout` varchar(200) NOT NULL,
  `page_header` varchar(200) NOT NULL,
  `footer_layout` varchar(200) NOT NULL,
  `statistics_cards` varchar(200) NOT NULL,
  `animation_style` varchar(100) NOT NULL,
  `theme_option` varchar(100) NOT NULL,
  `dashboard_option` varchar(100) NOT NULL,
  `dashboard_calendar` varchar(100) NOT NULL,
  `login_page_options` varchar(100) NOT NULL,
  `sub_menu_icons` varchar(100) NOT NULL,
  `statistics_cards_background` varchar(200) NOT NULL,
  `employee_cards` varchar(200) NOT NULL,
  `card_border_color` varchar(200) NOT NULL,
  `compact_menu` varchar(200) NOT NULL,
  `flipped_menu` varchar(200) NOT NULL,
  `right_side_icons` varchar(200) NOT NULL,
  `bordered_menu` varchar(200) NOT NULL,
  `form_design` varchar(200) NOT NULL,
  `is_semi_dark` int(11) NOT NULL,
  `semi_dark_color` varchar(200) NOT NULL,
  `top_nav_dark_color` varchar(200) NOT NULL,
  `menu_color_option` varchar(200) NOT NULL,
  `export_orgchart` varchar(100) NOT NULL,
  `export_file_title` mediumtext NOT NULL,
  `org_chart_layout` varchar(200) NOT NULL,
  `org_chart_zoom` varchar(100) NOT NULL,
  `org_chart_pan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_theme_settings`
--

INSERT INTO `xin_theme_settings` (`theme_settings_id`, `fixed_layout`, `fixed_footer`, `boxed_layout`, `page_header`, `footer_layout`, `statistics_cards`, `animation_style`, `theme_option`, `dashboard_option`, `dashboard_calendar`, `login_page_options`, `sub_menu_icons`, `statistics_cards_background`, `employee_cards`, `card_border_color`, `compact_menu`, `flipped_menu`, `right_side_icons`, `bordered_menu`, `form_design`, `is_semi_dark`, `semi_dark_color`, `top_nav_dark_color`, `menu_color_option`, `export_orgchart`, `export_file_title`, `org_chart_layout`, `org_chart_zoom`, `org_chart_pan`) VALUES
(1, 'false', 'true', 'false', 'breadcrumb-light', 'footer-dark', '8', 'fadeInDown', 'template_1', 'dashboard_2', 'true', 'login_page_2', 'fa-check-circle-o', '', '', '', 'true', 'false', 'false', 'false', 'modern_form', 1, 'bg-primary', 'bg-blue-grey', 'menu-dark', 'true', 'LionTech', 't2b', 'true', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `xin_tickets_attachment`
--

CREATE TABLE `xin_tickets_attachment` (
  `ticket_attachment_id` int(11) NOT NULL,
  `ticket_id` int(200) NOT NULL,
  `upload_by` int(255) NOT NULL,
  `file_title` varchar(255) NOT NULL,
  `file_description` mediumtext NOT NULL,
  `attachment_file` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_tickets_comments`
--

CREATE TABLE `xin_tickets_comments` (
  `comment_id` int(11) NOT NULL,
  `ticket_id` int(200) NOT NULL,
  `user_id` int(200) NOT NULL,
  `ticket_comments` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_tickets_comments`
--

INSERT INTO `xin_tickets_comments` (`comment_id`, `ticket_id`, `user_id`, `ticket_comments`, `created_at`) VALUES
(1, 1, 1, 'test', '23-11-2019 06:44:53');

-- --------------------------------------------------------

--
-- Table structure for table `xin_trainers`
--

CREATE TABLE `xin_trainers` (
  `trainer_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `designation_id` int(111) NOT NULL,
  `expertise` mediumtext NOT NULL,
  `address` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_training`
--

CREATE TABLE `xin_training` (
  `training_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` varchar(200) NOT NULL,
  `training_type_id` int(200) NOT NULL,
  `trainer_id` int(200) NOT NULL,
  `start_date` varchar(200) NOT NULL,
  `finish_date` varchar(200) NOT NULL,
  `training_cost` varchar(200) NOT NULL,
  `training_status` int(200) NOT NULL,
  `description` mediumtext NOT NULL,
  `performance` varchar(200) NOT NULL,
  `remarks` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xin_training_types`
--

CREATE TABLE `xin_training_types` (
  `training_type_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_training_types`
--

INSERT INTO `xin_training_types` (`training_type_id`, `company_id`, `type`, `created_at`, `status`) VALUES
(1, 1, 'Job Training', '19-03-2018 06:45:47', 1),
(2, 1, 'Workshop', '19-03-2018 06:45:51', 1);

-- --------------------------------------------------------

--
-- Table structure for table `xin_travel_arrangement_type`
--

CREATE TABLE `xin_travel_arrangement_type` (
  `arrangement_type_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_travel_arrangement_type`
--

INSERT INTO `xin_travel_arrangement_type` (`arrangement_type_id`, `company_id`, `type`, `status`, `created_at`) VALUES
(1, 1, 'Corporation', 1, '19-03-2018 08:45:17'),
(2, 1, 'Guest House', 1, '19-03-2018 08:45:27');

-- --------------------------------------------------------

--
-- Table structure for table `xin_users`
--

CREATE TABLE `xin_users` (
  `user_id` int(11) NOT NULL,
  `user_role` varchar(30) NOT NULL DEFAULT 'administrator',
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_logo` varchar(255) NOT NULL,
  `user_type` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile_photo` varchar(255) NOT NULL,
  `profile_background` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `address_1` text NOT NULL,
  `address_2` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `country` int(11) NOT NULL,
  `last_login_date` varchar(255) NOT NULL,
  `last_login_ip` varchar(255) NOT NULL,
  `is_logged_in` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_users`
--

INSERT INTO `xin_users` (`user_id`, `user_role`, `first_name`, `last_name`, `company_name`, `company_logo`, `user_type`, `email`, `username`, `password`, `profile_photo`, `profile_background`, `contact_number`, `gender`, `address_1`, `address_2`, `city`, `state`, `zipcode`, `country`, `last_login_date`, `last_login_ip`, `is_logged_in`, `is_active`, `created_at`) VALUES
(1, 'administrator', 'Thomas', 'Fleming', '', '', 2, 'test1@test.com', 'admin', 'test123', 'user_1520720863.jpg', 'profile_background_1505458640.jpg', '12333332', 'Male', 'Address Line 1', 'Address Line 2', 'City', 'State', '12345', 230, '15-04-2018 07:36:12', '::1', 0, 1, '14-09-2017 10:02:54'),
(2, 'administrator', 'Main', 'Office', 'LionTech', 'employer_1569175127.png', 2, 'hello@liontech.com.ng', 'test', 'test123', 'user_1523821315.jpg', '', '7031806085', 'Male', 'Address Line 1', 'Address Line 2', 'City', 'State', '11461', 190, '23-04-2018 05:34:47', '::1', 0, 1, '15-04-2018 06:13:08'),
(4, 'administrator', 'Fiona', 'Grace', 'test', 'employer_1524025572.jpg', 1, 'employer@test.com', '', 'test123', '', '', '1234567890', 'Male', 'Address Line 1', 'Address Line 2', 'City', 'State', '11461', 190, '23-04-2018 05:34:54', '::1', 0, 1, '18-04-2018 07:26:12');

-- --------------------------------------------------------

--
-- Table structure for table `xin_user_roles`
--

CREATE TABLE `xin_user_roles` (
  `role_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `role_name` varchar(200) NOT NULL,
  `role_access` varchar(200) NOT NULL,
  `role_resources` text NOT NULL,
  `created_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_user_roles`
--

INSERT INTO `xin_user_roles` (`role_id`, `company_id`, `role_name`, `role_access`, `role_resources`, `created_at`) VALUES
(1, 1, 'Super Admin', '1', '0,103,13,13,201,202,203,393,393,394,395,396,351,92,88,23,23,204,205,206,231,400,22,12,14,14,207,208,209,232,15,15,210,211,212,233,16,16,213,214,215,234,17,17,216,217,218,235,18,18,219,220,221,236,19,19,222,223,224,237,20,20,225,226,227,238,21,21,228,229,230,239,2,3,3,240,241,242,4,4,243,244,245,249,5,5,246,247,248,6,6,250,251,252,11,11,254,255,256,257,9,9,258,259,260,96,24,25,25,262,263,264,265,26,26,266,267,268,97,98,98,269,270,271,272,99,99,273,274,275,276,27,28,28,397,10,10,253,261,29,29,381,30,30,277,278,279,310,401,401,402,403,31,7,7,280,281,282,2822,311,8,8,283,284,285,46,46,287,288,289,290,286,312,48,49,49,291,292,293,50,51,51,294,295,387,52,52,296,297,388,32,36,36,313,314,37,37,391,40,41,41,298,299,300,301,42,42,302,303,304,305,372,373,43,43,306,307,308,309,104,44,44,315,316,317,318,45,45,319,320,321,322,119,119,323,324,325,121,120,121,328,329,330,122,122,331,332,333,106,107,107,334,335,336,108,108,338,339,340,47,53,54,54,341,342,343,344,55,55,345,346,347,56,56,348,349,350,57,60,61,118,62,63,93,71,72,72,352,353,354,73,74,75,75,355,356,357,76,76,358,359,360,77,77,361,362,363,78,79,80,80,364,365,366,81,81,367,368,369,82,83,84,85,86,87,89,89,370,371,90,91,94,95,110,111,112,113,114,115,116,117', '28-02-2018'),
(2, 1, 'Employee', '2', '0,13,201,92,88,22,12,14,14,207,208,209,232,15,15,210,211,212,233,16,16,213,214,215,234,17,17,216,217,218,235,18,18,219,220,221,236,19,19,222,223,224,237,20,20,225,226,227,238,21,21,228,229,230,239,2,3,3,240,241,242,4,4,243,244,245,249,5,5,246,247,248,6,6,250,251,252,11,11,254,255,256,257,9,9,258,259,260,96,25,262,263,264,27,28,28,397,10,10,253,261,29,29,381,30,30,277,278,279,310,401,401,402,403,31,7,7,280,281,282,2822,311,8,8,283,284,285,46,46,287,288,289,290,286,312,48,49,49,291,292,293,50,51,51,294,295,387,52,52,296,297,388,32,36,36,313,314,37,37,391,43,43,306,307,308,309,106,107,107,334,335,336,108,108,338,339,340,47,54,341,342,343,95', '21-03-2018');

-- --------------------------------------------------------

--
-- Table structure for table `xin_warning_type`
--

CREATE TABLE `xin_warning_type` (
  `warning_type_id` int(111) NOT NULL,
  `company_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xin_warning_type`
--

INSERT INTO `xin_warning_type` (`warning_type_id`, `company_id`, `type`, `created_at`) VALUES
(1, 1, 'First Written Warning', '22-03-2018 01:38:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business_type`
--
ALTER TABLE `business_type`
  ADD PRIMARY KEY (`business_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `xin_advance_salaries`
--
ALTER TABLE `xin_advance_salaries`
  ADD PRIMARY KEY (`advance_salary_id`);

--
-- Indexes for table `xin_announcements`
--
ALTER TABLE `xin_announcements`
  ADD PRIMARY KEY (`announcement_id`);

--
-- Indexes for table `xin_assets`
--
ALTER TABLE `xin_assets`
  ADD PRIMARY KEY (`assets_id`);

--
-- Indexes for table `xin_assets_categories`
--
ALTER TABLE `xin_assets_categories`
  ADD PRIMARY KEY (`assets_category_id`);

--
-- Indexes for table `xin_attendance_time`
--
ALTER TABLE `xin_attendance_time`
  ADD PRIMARY KEY (`time_attendance_id`);

--
-- Indexes for table `xin_attendance_time_request`
--
ALTER TABLE `xin_attendance_time_request`
  ADD PRIMARY KEY (`time_request_id`);

--
-- Indexes for table `xin_awards`
--
ALTER TABLE `xin_awards`
  ADD PRIMARY KEY (`award_id`);

--
-- Indexes for table `xin_award_type`
--
ALTER TABLE `xin_award_type`
  ADD PRIMARY KEY (`award_type_id`);

--
-- Indexes for table `xin_bands`
--
ALTER TABLE `xin_bands`
  ADD PRIMARY KEY (`band_id`);

--
-- Indexes for table `xin_change_hospital_request`
--
ALTER TABLE `xin_change_hospital_request`
  ADD PRIMARY KEY (`change_hospital_request_id`);

--
-- Indexes for table `xin_chat_messages`
--
ALTER TABLE `xin_chat_messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `xin_clients`
--
ALTER TABLE `xin_clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `xin_clients_diagnose`
--
ALTER TABLE `xin_clients_diagnose`
  ADD PRIMARY KEY (`diagnose_id`);

--
-- Indexes for table `xin_clients_diagnose_drugs`
--
ALTER TABLE `xin_clients_diagnose_drugs`
  ADD PRIMARY KEY (`diagnose_autod_id`);

--
-- Indexes for table `xin_clients_diagnose_services`
--
ALTER TABLE `xin_clients_diagnose_services`
  ADD PRIMARY KEY (`diagnose_autos_id`);

--
-- Indexes for table `xin_clients_family`
--
ALTER TABLE `xin_clients_family`
  ADD PRIMARY KEY (`clients_family_id`);

--
-- Indexes for table `xin_clients_family_req`
--
ALTER TABLE `xin_clients_family_req`
  ADD PRIMARY KEY (`clients_family_id`);

--
-- Indexes for table `xin_companies`
--
ALTER TABLE `xin_companies`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `xin_company_documents`
--
ALTER TABLE `xin_company_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `xin_company_info`
--
ALTER TABLE `xin_company_info`
  ADD PRIMARY KEY (`company_info_id`);

--
-- Indexes for table `xin_company_policy`
--
ALTER TABLE `xin_company_policy`
  ADD PRIMARY KEY (`policy_id`);

--
-- Indexes for table `xin_company_type`
--
ALTER TABLE `xin_company_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `xin_contract_type`
--
ALTER TABLE `xin_contract_type`
  ADD PRIMARY KEY (`contract_type_id`);

--
-- Indexes for table `xin_countries`
--
ALTER TABLE `xin_countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `xin_currencies`
--
ALTER TABLE `xin_currencies`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `xin_currency_converter`
--
ALTER TABLE `xin_currency_converter`
  ADD PRIMARY KEY (`currency_converter_id`);

--
-- Indexes for table `xin_database_backup`
--
ALTER TABLE `xin_database_backup`
  ADD PRIMARY KEY (`backup_id`);

--
-- Indexes for table `xin_departments`
--
ALTER TABLE `xin_departments`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `xin_designations`
--
ALTER TABLE `xin_designations`
  ADD PRIMARY KEY (`designation_id`);

--
-- Indexes for table `xin_document_type`
--
ALTER TABLE `xin_document_type`
  ADD PRIMARY KEY (`document_type_id`);

--
-- Indexes for table `xin_email_template`
--
ALTER TABLE `xin_email_template`
  ADD PRIMARY KEY (`template_id`);

--
-- Indexes for table `xin_employees`
--
ALTER TABLE `xin_employees`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `xin_employee_bankaccount`
--
ALTER TABLE `xin_employee_bankaccount`
  ADD PRIMARY KEY (`bankaccount_id`);

--
-- Indexes for table `xin_employee_complaints`
--
ALTER TABLE `xin_employee_complaints`
  ADD PRIMARY KEY (`complaint_id`);

--
-- Indexes for table `xin_employee_contacts`
--
ALTER TABLE `xin_employee_contacts`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `xin_employee_contract`
--
ALTER TABLE `xin_employee_contract`
  ADD PRIMARY KEY (`contract_id`);

--
-- Indexes for table `xin_employee_documents`
--
ALTER TABLE `xin_employee_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `xin_employee_exit`
--
ALTER TABLE `xin_employee_exit`
  ADD PRIMARY KEY (`exit_id`);

--
-- Indexes for table `xin_employee_exit_type`
--
ALTER TABLE `xin_employee_exit_type`
  ADD PRIMARY KEY (`exit_type_id`);

--
-- Indexes for table `xin_employee_immigration`
--
ALTER TABLE `xin_employee_immigration`
  ADD PRIMARY KEY (`immigration_id`);

--
-- Indexes for table `xin_employee_leave`
--
ALTER TABLE `xin_employee_leave`
  ADD PRIMARY KEY (`leave_id`);

--
-- Indexes for table `xin_employee_location`
--
ALTER TABLE `xin_employee_location`
  ADD PRIMARY KEY (`office_location_id`);

--
-- Indexes for table `xin_employee_promotions`
--
ALTER TABLE `xin_employee_promotions`
  ADD PRIMARY KEY (`promotion_id`);

--
-- Indexes for table `xin_employee_qualification`
--
ALTER TABLE `xin_employee_qualification`
  ADD PRIMARY KEY (`qualification_id`);

--
-- Indexes for table `xin_employee_resignations`
--
ALTER TABLE `xin_employee_resignations`
  ADD PRIMARY KEY (`resignation_id`);

--
-- Indexes for table `xin_employee_shift`
--
ALTER TABLE `xin_employee_shift`
  ADD PRIMARY KEY (`emp_shift_id`);

--
-- Indexes for table `xin_employee_terminations`
--
ALTER TABLE `xin_employee_terminations`
  ADD PRIMARY KEY (`termination_id`);

--
-- Indexes for table `xin_employee_transfer`
--
ALTER TABLE `xin_employee_transfer`
  ADD PRIMARY KEY (`transfer_id`);

--
-- Indexes for table `xin_employee_travels`
--
ALTER TABLE `xin_employee_travels`
  ADD PRIMARY KEY (`travel_id`);

--
-- Indexes for table `xin_employee_warnings`
--
ALTER TABLE `xin_employee_warnings`
  ADD PRIMARY KEY (`warning_id`);

--
-- Indexes for table `xin_employee_work_experience`
--
ALTER TABLE `xin_employee_work_experience`
  ADD PRIMARY KEY (`work_experience_id`);

--
-- Indexes for table `xin_events`
--
ALTER TABLE `xin_events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `xin_expenses`
--
ALTER TABLE `xin_expenses`
  ADD PRIMARY KEY (`expense_id`);

--
-- Indexes for table `xin_expense_type`
--
ALTER TABLE `xin_expense_type`
  ADD PRIMARY KEY (`expense_type_id`);

--
-- Indexes for table `xin_file_manager`
--
ALTER TABLE `xin_file_manager`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `xin_file_manager_settings`
--
ALTER TABLE `xin_file_manager_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `xin_finance_bankcash`
--
ALTER TABLE `xin_finance_bankcash`
  ADD PRIMARY KEY (`bankcash_id`);

--
-- Indexes for table `xin_finance_deposit`
--
ALTER TABLE `xin_finance_deposit`
  ADD PRIMARY KEY (`deposit_id`);

--
-- Indexes for table `xin_finance_expense`
--
ALTER TABLE `xin_finance_expense`
  ADD PRIMARY KEY (`expense_id`);

--
-- Indexes for table `xin_finance_payees`
--
ALTER TABLE `xin_finance_payees`
  ADD PRIMARY KEY (`payee_id`);

--
-- Indexes for table `xin_finance_payers`
--
ALTER TABLE `xin_finance_payers`
  ADD PRIMARY KEY (`payer_id`);

--
-- Indexes for table `xin_finance_transaction`
--
ALTER TABLE `xin_finance_transaction`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `xin_finance_transactions`
--
ALTER TABLE `xin_finance_transactions`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `xin_finance_transfer`
--
ALTER TABLE `xin_finance_transfer`
  ADD PRIMARY KEY (`transfer_id`);

--
-- Indexes for table `xin_goal_tracking`
--
ALTER TABLE `xin_goal_tracking`
  ADD PRIMARY KEY (`tracking_id`);

--
-- Indexes for table `xin_goal_tracking_type`
--
ALTER TABLE `xin_goal_tracking_type`
  ADD PRIMARY KEY (`tracking_type_id`);

--
-- Indexes for table `xin_holidays`
--
ALTER TABLE `xin_holidays`
  ADD PRIMARY KEY (`holiday_id`);

--
-- Indexes for table `xin_hospital`
--
ALTER TABLE `xin_hospital`
  ADD PRIMARY KEY (`hospital_id`);

--
-- Indexes for table `xin_hospital_drugs`
--
ALTER TABLE `xin_hospital_drugs`
  ADD PRIMARY KEY (`drug_id`);

--
-- Indexes for table `xin_hospital_services`
--
ALTER TABLE `xin_hospital_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `xin_hourly_templates`
--
ALTER TABLE `xin_hourly_templates`
  ADD PRIMARY KEY (`hourly_rate_id`);

--
-- Indexes for table `xin_hrsale_invoices`
--
ALTER TABLE `xin_hrsale_invoices`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `xin_hrsale_invoices_items`
--
ALTER TABLE `xin_hrsale_invoices_items`
  ADD PRIMARY KEY (`invoice_item_id`);

--
-- Indexes for table `xin_hrsale_module_attributes`
--
ALTER TABLE `xin_hrsale_module_attributes`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Indexes for table `xin_hrsale_module_attributes_select_value`
--
ALTER TABLE `xin_hrsale_module_attributes_select_value`
  ADD PRIMARY KEY (`attributes_select_value_id`);

--
-- Indexes for table `xin_hrsale_module_attributes_values`
--
ALTER TABLE `xin_hrsale_module_attributes_values`
  ADD PRIMARY KEY (`attributes_value_id`);

--
-- Indexes for table `xin_income_categories`
--
ALTER TABLE `xin_income_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `xin_jobs`
--
ALTER TABLE `xin_jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `xin_job_applications`
--
ALTER TABLE `xin_job_applications`
  ADD PRIMARY KEY (`application_id`);

--
-- Indexes for table `xin_job_categories`
--
ALTER TABLE `xin_job_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `xin_job_interviews`
--
ALTER TABLE `xin_job_interviews`
  ADD PRIMARY KEY (`job_interview_id`);

--
-- Indexes for table `xin_job_pages`
--
ALTER TABLE `xin_job_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `xin_job_type`
--
ALTER TABLE `xin_job_type`
  ADD PRIMARY KEY (`job_type_id`);

--
-- Indexes for table `xin_kpi_incidental`
--
ALTER TABLE `xin_kpi_incidental`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `xin_kpi_maingoals`
--
ALTER TABLE `xin_kpi_maingoals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `xin_kpi_variable`
--
ALTER TABLE `xin_kpi_variable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `xin_languages`
--
ALTER TABLE `xin_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `xin_leave_applications`
--
ALTER TABLE `xin_leave_applications`
  ADD PRIMARY KEY (`leave_id`);

--
-- Indexes for table `xin_leave_type`
--
ALTER TABLE `xin_leave_type`
  ADD PRIMARY KEY (`leave_type_id`);

--
-- Indexes for table `xin_location`
--
ALTER TABLE `xin_location`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `xin_make_payment`
--
ALTER TABLE `xin_make_payment`
  ADD PRIMARY KEY (`make_payment_id`);

--
-- Indexes for table `xin_meetings`
--
ALTER TABLE `xin_meetings`
  ADD PRIMARY KEY (`meeting_id`);

--
-- Indexes for table `xin_office_location`
--
ALTER TABLE `xin_office_location`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `xin_office_shift`
--
ALTER TABLE `xin_office_shift`
  ADD PRIMARY KEY (`office_shift_id`);

--
-- Indexes for table `xin_organization`
--
ALTER TABLE `xin_organization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `xin_payment_method`
--
ALTER TABLE `xin_payment_method`
  ADD PRIMARY KEY (`payment_method_id`);

--
-- Indexes for table `xin_payroll_custom_fields`
--
ALTER TABLE `xin_payroll_custom_fields`
  ADD PRIMARY KEY (`payroll_custom_id`);

--
-- Indexes for table `xin_performance_appraisal`
--
ALTER TABLE `xin_performance_appraisal`
  ADD PRIMARY KEY (`performance_appraisal_id`);

--
-- Indexes for table `xin_performance_indicator`
--
ALTER TABLE `xin_performance_indicator`
  ADD PRIMARY KEY (`performance_indicator_id`);

--
-- Indexes for table `xin_projects`
--
ALTER TABLE `xin_projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `xin_projects_attachment`
--
ALTER TABLE `xin_projects_attachment`
  ADD PRIMARY KEY (`project_attachment_id`);

--
-- Indexes for table `xin_projects_bugs`
--
ALTER TABLE `xin_projects_bugs`
  ADD PRIMARY KEY (`bug_id`);

--
-- Indexes for table `xin_projects_discussion`
--
ALTER TABLE `xin_projects_discussion`
  ADD PRIMARY KEY (`discussion_id`);

--
-- Indexes for table `xin_qualification_education_level`
--
ALTER TABLE `xin_qualification_education_level`
  ADD PRIMARY KEY (`education_level_id`);

--
-- Indexes for table `xin_qualification_language`
--
ALTER TABLE `xin_qualification_language`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `xin_qualification_skill`
--
ALTER TABLE `xin_qualification_skill`
  ADD PRIMARY KEY (`skill_id`);

--
-- Indexes for table `xin_recruitment_pages`
--
ALTER TABLE `xin_recruitment_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `xin_recruitment_subpages`
--
ALTER TABLE `xin_recruitment_subpages`
  ADD PRIMARY KEY (`subpages_id`);

--
-- Indexes for table `xin_salary_allowances`
--
ALTER TABLE `xin_salary_allowances`
  ADD PRIMARY KEY (`allowance_id`);

--
-- Indexes for table `xin_salary_bank_allocation`
--
ALTER TABLE `xin_salary_bank_allocation`
  ADD PRIMARY KEY (`bank_allocation_id`);

--
-- Indexes for table `xin_salary_commissions`
--
ALTER TABLE `xin_salary_commissions`
  ADD PRIMARY KEY (`salary_commissions_id`);

--
-- Indexes for table `xin_salary_loan_deductions`
--
ALTER TABLE `xin_salary_loan_deductions`
  ADD PRIMARY KEY (`loan_deduction_id`);

--
-- Indexes for table `xin_salary_other_payments`
--
ALTER TABLE `xin_salary_other_payments`
  ADD PRIMARY KEY (`other_payments_id`);

--
-- Indexes for table `xin_salary_overtime`
--
ALTER TABLE `xin_salary_overtime`
  ADD PRIMARY KEY (`salary_overtime_id`);

--
-- Indexes for table `xin_salary_payslips`
--
ALTER TABLE `xin_salary_payslips`
  ADD PRIMARY KEY (`payslip_id`);

--
-- Indexes for table `xin_salary_payslip_allowances`
--
ALTER TABLE `xin_salary_payslip_allowances`
  ADD PRIMARY KEY (`payslip_allowances_id`);

--
-- Indexes for table `xin_salary_payslip_commissions`
--
ALTER TABLE `xin_salary_payslip_commissions`
  ADD PRIMARY KEY (`payslip_commissions_id`);

--
-- Indexes for table `xin_salary_payslip_loan`
--
ALTER TABLE `xin_salary_payslip_loan`
  ADD PRIMARY KEY (`payslip_loan_id`);

--
-- Indexes for table `xin_salary_payslip_other_payments`
--
ALTER TABLE `xin_salary_payslip_other_payments`
  ADD PRIMARY KEY (`payslip_other_payment_id`);

--
-- Indexes for table `xin_salary_payslip_overtime`
--
ALTER TABLE `xin_salary_payslip_overtime`
  ADD PRIMARY KEY (`payslip_overtime_id`);

--
-- Indexes for table `xin_salary_payslip_statutory_deductions`
--
ALTER TABLE `xin_salary_payslip_statutory_deductions`
  ADD PRIMARY KEY (`payslip_deduction_id`);

--
-- Indexes for table `xin_salary_statutory_deductions`
--
ALTER TABLE `xin_salary_statutory_deductions`
  ADD PRIMARY KEY (`statutory_deductions_id`);

--
-- Indexes for table `xin_salary_templates`
--
ALTER TABLE `xin_salary_templates`
  ADD PRIMARY KEY (`salary_template_id`);

--
-- Indexes for table `xin_services_hospital`
--
ALTER TABLE `xin_services_hospital`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `xin_subscription`
--
ALTER TABLE `xin_subscription`
  ADD PRIMARY KEY (`subscription_id`);

--
-- Indexes for table `xin_subscription_detail`
--
ALTER TABLE `xin_subscription_detail`
  ADD PRIMARY KEY (`subscription_detail_id`);

--
-- Indexes for table `xin_sub_departments`
--
ALTER TABLE `xin_sub_departments`
  ADD PRIMARY KEY (`sub_department_id`);

--
-- Indexes for table `xin_support_tickets`
--
ALTER TABLE `xin_support_tickets`
  ADD PRIMARY KEY (`ticket_id`);

--
-- Indexes for table `xin_support_ticket_files`
--
ALTER TABLE `xin_support_ticket_files`
  ADD PRIMARY KEY (`ticket_file_id`);

--
-- Indexes for table `xin_system_setting`
--
ALTER TABLE `xin_system_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `xin_tasks`
--
ALTER TABLE `xin_tasks`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `xin_tasks_attachment`
--
ALTER TABLE `xin_tasks_attachment`
  ADD PRIMARY KEY (`task_attachment_id`);

--
-- Indexes for table `xin_tasks_comments`
--
ALTER TABLE `xin_tasks_comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `xin_tax_types`
--
ALTER TABLE `xin_tax_types`
  ADD PRIMARY KEY (`tax_id`);

--
-- Indexes for table `xin_termination_type`
--
ALTER TABLE `xin_termination_type`
  ADD PRIMARY KEY (`termination_type_id`);

--
-- Indexes for table `xin_theme_settings`
--
ALTER TABLE `xin_theme_settings`
  ADD PRIMARY KEY (`theme_settings_id`);

--
-- Indexes for table `xin_tickets_attachment`
--
ALTER TABLE `xin_tickets_attachment`
  ADD PRIMARY KEY (`ticket_attachment_id`);

--
-- Indexes for table `xin_tickets_comments`
--
ALTER TABLE `xin_tickets_comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `xin_trainers`
--
ALTER TABLE `xin_trainers`
  ADD PRIMARY KEY (`trainer_id`);

--
-- Indexes for table `xin_training`
--
ALTER TABLE `xin_training`
  ADD PRIMARY KEY (`training_id`);

--
-- Indexes for table `xin_training_types`
--
ALTER TABLE `xin_training_types`
  ADD PRIMARY KEY (`training_type_id`);

--
-- Indexes for table `xin_travel_arrangement_type`
--
ALTER TABLE `xin_travel_arrangement_type`
  ADD PRIMARY KEY (`arrangement_type_id`);

--
-- Indexes for table `xin_users`
--
ALTER TABLE `xin_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `xin_user_roles`
--
ALTER TABLE `xin_user_roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `xin_warning_type`
--
ALTER TABLE `xin_warning_type`
  ADD PRIMARY KEY (`warning_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business_type`
--
ALTER TABLE `business_type`
  MODIFY `business_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `xin_advance_salaries`
--
ALTER TABLE `xin_advance_salaries`
  MODIFY `advance_salary_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_announcements`
--
ALTER TABLE `xin_announcements`
  MODIFY `announcement_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_assets`
--
ALTER TABLE `xin_assets`
  MODIFY `assets_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_assets_categories`
--
ALTER TABLE `xin_assets_categories`
  MODIFY `assets_category_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_attendance_time`
--
ALTER TABLE `xin_attendance_time`
  MODIFY `time_attendance_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_attendance_time_request`
--
ALTER TABLE `xin_attendance_time_request`
  MODIFY `time_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_awards`
--
ALTER TABLE `xin_awards`
  MODIFY `award_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_award_type`
--
ALTER TABLE `xin_award_type`
  MODIFY `award_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_bands`
--
ALTER TABLE `xin_bands`
  MODIFY `band_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `xin_change_hospital_request`
--
ALTER TABLE `xin_change_hospital_request`
  MODIFY `change_hospital_request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `xin_chat_messages`
--
ALTER TABLE `xin_chat_messages`
  MODIFY `message_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `xin_clients`
--
ALTER TABLE `xin_clients`
  MODIFY `client_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `xin_clients_diagnose`
--
ALTER TABLE `xin_clients_diagnose`
  MODIFY `diagnose_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `xin_clients_diagnose_drugs`
--
ALTER TABLE `xin_clients_diagnose_drugs`
  MODIFY `diagnose_autod_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `xin_clients_diagnose_services`
--
ALTER TABLE `xin_clients_diagnose_services`
  MODIFY `diagnose_autos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `xin_clients_family`
--
ALTER TABLE `xin_clients_family`
  MODIFY `clients_family_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `xin_clients_family_req`
--
ALTER TABLE `xin_clients_family_req`
  MODIFY `clients_family_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `xin_companies`
--
ALTER TABLE `xin_companies`
  MODIFY `company_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_company_documents`
--
ALTER TABLE `xin_company_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_company_info`
--
ALTER TABLE `xin_company_info`
  MODIFY `company_info_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_company_policy`
--
ALTER TABLE `xin_company_policy`
  MODIFY `policy_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_company_type`
--
ALTER TABLE `xin_company_type`
  MODIFY `type_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `xin_contract_type`
--
ALTER TABLE `xin_contract_type`
  MODIFY `contract_type_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_countries`
--
ALTER TABLE `xin_countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=246;

--
-- AUTO_INCREMENT for table `xin_currencies`
--
ALTER TABLE `xin_currencies`
  MODIFY `currency_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `xin_currency_converter`
--
ALTER TABLE `xin_currency_converter`
  MODIFY `currency_converter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_database_backup`
--
ALTER TABLE `xin_database_backup`
  MODIFY `backup_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_departments`
--
ALTER TABLE `xin_departments`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `xin_designations`
--
ALTER TABLE `xin_designations`
  MODIFY `designation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `xin_document_type`
--
ALTER TABLE `xin_document_type`
  MODIFY `document_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_email_template`
--
ALTER TABLE `xin_email_template`
  MODIFY `template_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `xin_employees`
--
ALTER TABLE `xin_employees`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `xin_employee_bankaccount`
--
ALTER TABLE `xin_employee_bankaccount`
  MODIFY `bankaccount_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_complaints`
--
ALTER TABLE `xin_employee_complaints`
  MODIFY `complaint_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_contacts`
--
ALTER TABLE `xin_employee_contacts`
  MODIFY `contact_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_contract`
--
ALTER TABLE `xin_employee_contract`
  MODIFY `contract_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_documents`
--
ALTER TABLE `xin_employee_documents`
  MODIFY `document_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_exit`
--
ALTER TABLE `xin_employee_exit`
  MODIFY `exit_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_exit_type`
--
ALTER TABLE `xin_employee_exit_type`
  MODIFY `exit_type_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_employee_immigration`
--
ALTER TABLE `xin_employee_immigration`
  MODIFY `immigration_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_leave`
--
ALTER TABLE `xin_employee_leave`
  MODIFY `leave_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_location`
--
ALTER TABLE `xin_employee_location`
  MODIFY `office_location_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_promotions`
--
ALTER TABLE `xin_employee_promotions`
  MODIFY `promotion_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_qualification`
--
ALTER TABLE `xin_employee_qualification`
  MODIFY `qualification_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_resignations`
--
ALTER TABLE `xin_employee_resignations`
  MODIFY `resignation_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_shift`
--
ALTER TABLE `xin_employee_shift`
  MODIFY `emp_shift_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_terminations`
--
ALTER TABLE `xin_employee_terminations`
  MODIFY `termination_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_transfer`
--
ALTER TABLE `xin_employee_transfer`
  MODIFY `transfer_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_travels`
--
ALTER TABLE `xin_employee_travels`
  MODIFY `travel_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_warnings`
--
ALTER TABLE `xin_employee_warnings`
  MODIFY `warning_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_employee_work_experience`
--
ALTER TABLE `xin_employee_work_experience`
  MODIFY `work_experience_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_events`
--
ALTER TABLE `xin_events`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_expenses`
--
ALTER TABLE `xin_expenses`
  MODIFY `expense_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_expense_type`
--
ALTER TABLE `xin_expense_type`
  MODIFY `expense_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `xin_file_manager`
--
ALTER TABLE `xin_file_manager`
  MODIFY `file_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_file_manager_settings`
--
ALTER TABLE `xin_file_manager_settings`
  MODIFY `setting_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_finance_bankcash`
--
ALTER TABLE `xin_finance_bankcash`
  MODIFY `bankcash_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `xin_finance_deposit`
--
ALTER TABLE `xin_finance_deposit`
  MODIFY `deposit_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_finance_expense`
--
ALTER TABLE `xin_finance_expense`
  MODIFY `expense_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_finance_payees`
--
ALTER TABLE `xin_finance_payees`
  MODIFY `payee_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_finance_payers`
--
ALTER TABLE `xin_finance_payers`
  MODIFY `payer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_finance_transaction`
--
ALTER TABLE `xin_finance_transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_finance_transactions`
--
ALTER TABLE `xin_finance_transactions`
  MODIFY `transaction_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_finance_transfer`
--
ALTER TABLE `xin_finance_transfer`
  MODIFY `transfer_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_goal_tracking`
--
ALTER TABLE `xin_goal_tracking`
  MODIFY `tracking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_goal_tracking_type`
--
ALTER TABLE `xin_goal_tracking_type`
  MODIFY `tracking_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `xin_holidays`
--
ALTER TABLE `xin_holidays`
  MODIFY `holiday_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_hospital`
--
ALTER TABLE `xin_hospital`
  MODIFY `hospital_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `xin_hospital_drugs`
--
ALTER TABLE `xin_hospital_drugs`
  MODIFY `drug_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14653;

--
-- AUTO_INCREMENT for table `xin_hospital_services`
--
ALTER TABLE `xin_hospital_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_hourly_templates`
--
ALTER TABLE `xin_hourly_templates`
  MODIFY `hourly_rate_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_hrsale_invoices`
--
ALTER TABLE `xin_hrsale_invoices`
  MODIFY `invoice_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_hrsale_invoices_items`
--
ALTER TABLE `xin_hrsale_invoices_items`
  MODIFY `invoice_item_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_hrsale_module_attributes`
--
ALTER TABLE `xin_hrsale_module_attributes`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_hrsale_module_attributes_select_value`
--
ALTER TABLE `xin_hrsale_module_attributes_select_value`
  MODIFY `attributes_select_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_hrsale_module_attributes_values`
--
ALTER TABLE `xin_hrsale_module_attributes_values`
  MODIFY `attributes_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_income_categories`
--
ALTER TABLE `xin_income_categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `xin_jobs`
--
ALTER TABLE `xin_jobs`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_job_applications`
--
ALTER TABLE `xin_job_applications`
  MODIFY `application_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_job_categories`
--
ALTER TABLE `xin_job_categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `xin_job_interviews`
--
ALTER TABLE `xin_job_interviews`
  MODIFY `job_interview_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_job_pages`
--
ALTER TABLE `xin_job_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `xin_job_type`
--
ALTER TABLE `xin_job_type`
  MODIFY `job_type_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `xin_kpi_incidental`
--
ALTER TABLE `xin_kpi_incidental`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_kpi_maingoals`
--
ALTER TABLE `xin_kpi_maingoals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_kpi_variable`
--
ALTER TABLE `xin_kpi_variable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_languages`
--
ALTER TABLE `xin_languages`
  MODIFY `language_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `xin_leave_applications`
--
ALTER TABLE `xin_leave_applications`
  MODIFY `leave_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_leave_type`
--
ALTER TABLE `xin_leave_type`
  MODIFY `leave_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `xin_location`
--
ALTER TABLE `xin_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `xin_make_payment`
--
ALTER TABLE `xin_make_payment`
  MODIFY `make_payment_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_meetings`
--
ALTER TABLE `xin_meetings`
  MODIFY `meeting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_office_location`
--
ALTER TABLE `xin_office_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `xin_office_shift`
--
ALTER TABLE `xin_office_shift`
  MODIFY `office_shift_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_organization`
--
ALTER TABLE `xin_organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `xin_payment_method`
--
ALTER TABLE `xin_payment_method`
  MODIFY `payment_method_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `xin_payroll_custom_fields`
--
ALTER TABLE `xin_payroll_custom_fields`
  MODIFY `payroll_custom_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_performance_appraisal`
--
ALTER TABLE `xin_performance_appraisal`
  MODIFY `performance_appraisal_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_performance_indicator`
--
ALTER TABLE `xin_performance_indicator`
  MODIFY `performance_indicator_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_projects`
--
ALTER TABLE `xin_projects`
  MODIFY `project_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_projects_attachment`
--
ALTER TABLE `xin_projects_attachment`
  MODIFY `project_attachment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_projects_bugs`
--
ALTER TABLE `xin_projects_bugs`
  MODIFY `bug_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_projects_discussion`
--
ALTER TABLE `xin_projects_discussion`
  MODIFY `discussion_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_qualification_education_level`
--
ALTER TABLE `xin_qualification_education_level`
  MODIFY `education_level_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_qualification_language`
--
ALTER TABLE `xin_qualification_language`
  MODIFY `language_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_qualification_skill`
--
ALTER TABLE `xin_qualification_skill`
  MODIFY `skill_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_recruitment_pages`
--
ALTER TABLE `xin_recruitment_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `xin_recruitment_subpages`
--
ALTER TABLE `xin_recruitment_subpages`
  MODIFY `subpages_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `xin_salary_allowances`
--
ALTER TABLE `xin_salary_allowances`
  MODIFY `allowance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `xin_salary_bank_allocation`
--
ALTER TABLE `xin_salary_bank_allocation`
  MODIFY `bank_allocation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_salary_commissions`
--
ALTER TABLE `xin_salary_commissions`
  MODIFY `salary_commissions_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_salary_loan_deductions`
--
ALTER TABLE `xin_salary_loan_deductions`
  MODIFY `loan_deduction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_salary_other_payments`
--
ALTER TABLE `xin_salary_other_payments`
  MODIFY `other_payments_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_salary_overtime`
--
ALTER TABLE `xin_salary_overtime`
  MODIFY `salary_overtime_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_salary_payslips`
--
ALTER TABLE `xin_salary_payslips`
  MODIFY `payslip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_salary_payslip_allowances`
--
ALTER TABLE `xin_salary_payslip_allowances`
  MODIFY `payslip_allowances_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_salary_payslip_commissions`
--
ALTER TABLE `xin_salary_payslip_commissions`
  MODIFY `payslip_commissions_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_salary_payslip_loan`
--
ALTER TABLE `xin_salary_payslip_loan`
  MODIFY `payslip_loan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_salary_payslip_other_payments`
--
ALTER TABLE `xin_salary_payslip_other_payments`
  MODIFY `payslip_other_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_salary_payslip_overtime`
--
ALTER TABLE `xin_salary_payslip_overtime`
  MODIFY `payslip_overtime_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_salary_payslip_statutory_deductions`
--
ALTER TABLE `xin_salary_payslip_statutory_deductions`
  MODIFY `payslip_deduction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_salary_statutory_deductions`
--
ALTER TABLE `xin_salary_statutory_deductions`
  MODIFY `statutory_deductions_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_salary_templates`
--
ALTER TABLE `xin_salary_templates`
  MODIFY `salary_template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_services_hospital`
--
ALTER TABLE `xin_services_hospital`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `xin_subscription`
--
ALTER TABLE `xin_subscription`
  MODIFY `subscription_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `xin_subscription_detail`
--
ALTER TABLE `xin_subscription_detail`
  MODIFY `subscription_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `xin_sub_departments`
--
ALTER TABLE `xin_sub_departments`
  MODIFY `sub_department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `xin_support_tickets`
--
ALTER TABLE `xin_support_tickets`
  MODIFY `ticket_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `xin_support_ticket_files`
--
ALTER TABLE `xin_support_ticket_files`
  MODIFY `ticket_file_id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_system_setting`
--
ALTER TABLE `xin_system_setting`
  MODIFY `setting_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_tasks`
--
ALTER TABLE `xin_tasks`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_tasks_attachment`
--
ALTER TABLE `xin_tasks_attachment`
  MODIFY `task_attachment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_tasks_comments`
--
ALTER TABLE `xin_tasks_comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_tax_types`
--
ALTER TABLE `xin_tax_types`
  MODIFY `tax_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `xin_termination_type`
--
ALTER TABLE `xin_termination_type`
  MODIFY `termination_type_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_theme_settings`
--
ALTER TABLE `xin_theme_settings`
  MODIFY `theme_settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_tickets_attachment`
--
ALTER TABLE `xin_tickets_attachment`
  MODIFY `ticket_attachment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_tickets_comments`
--
ALTER TABLE `xin_tickets_comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `xin_trainers`
--
ALTER TABLE `xin_trainers`
  MODIFY `trainer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_training`
--
ALTER TABLE `xin_training`
  MODIFY `training_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xin_training_types`
--
ALTER TABLE `xin_training_types`
  MODIFY `training_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `xin_travel_arrangement_type`
--
ALTER TABLE `xin_travel_arrangement_type`
  MODIFY `arrangement_type_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `xin_users`
--
ALTER TABLE `xin_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `xin_user_roles`
--
ALTER TABLE `xin_user_roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `xin_warning_type`
--
ALTER TABLE `xin_warning_type`
  MODIFY `warning_type_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
